module.exports = {
    "env": {
        "browser": true,
        "es2021": true
    },
    "extends": [
        "standard-with-typescript",
        "plugin:react/recommended"
    ],
    "overrides": [
        {
            "env": {
                "node": true
            },
            "files": [
                ".eslintrc.{js,cjs}"
            ],
            "parserOptions": {
                "sourceType": "script"
            }
        }
    ],
    "ignorePatterns": [".eslintrc.js",],
    "parserOptions": {
        "ecmaVersion": "latest",
        "sourceType": "module"
    },
    "plugins": [
        "react"
    ],
    "rules": {
       "quotes": "off",
       "@typescript-eslint/quotes": ["error", "double", { "avoidEscape": true, "allowTemplateLiterals": true }],
        //  References https://github.com/orgs/react-hook-form/discussions/10965
        //  References https://github.com/orgs/react-hook-form/discussions/9325#discussioncomment-4060566
        //  References https://github.com/orgs/react-hook-form/discussions/8020
       "@typescript-eslint/no-misused-promises": [2, {
        "checksVoidReturn": {
          "attributes": false
        }
      }]
    },
    "settings": {
      "import/resolver": {
        "node": {
          "extensions": [".ts", ".mjs",".tsx"],
          "paths": ["src",]
        }
      },
      "react": {
        "version": "detect"
      }
    }
}
