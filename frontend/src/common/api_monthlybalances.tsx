import type * as Interfaces from "./interfaces"
import { getAuthHeaders, credentialsOption } from "./api_helpers"
import Cookies from "universal-cookie"
const cookies = new Cookies()

export async function createMonthlyBalanceCategory (data: Interfaces.CategoryFormInputs, signal?: AbortSignal): Promise<Interfaces.MonthlyBalanceCategoryResponse> {
  return await fetch("/api/v1/monthly_balance_categories", {
    method: "POST",
    headers: getAuthHeaders(),
    ...credentialsOption,
    body: JSON.stringify(data),
    signal
  }).then(async response => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err): Promise<Interfaces.MonthlyBalanceCategoryResponse> => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const empty: Interfaces.MonthlyBalanceCategory = {
        id: 0,
        text: "",
        is_foreign_currency: false
      }
      return await Promise.resolve({ response, data: empty })
    })
  })
}

export async function getMonthlyBalanceCategory (id: number, signal?: AbortSignal): Promise<Interfaces.MonthlyBalanceCategoryResponse> {
  return await fetch(`/api/v1/monthly_balance_category/${id}`, { signal })
    .then(async (response): Promise<Interfaces.MonthlyBalanceCategoryResponse> => {
      return await response.json().then(parsedJson => {
        return { response, data: parsedJson }
      }).catch(async (err) => {
        // This catch handles the case where there is no json body in the response
        console.log(err)
        const empty: Interfaces.MonthlyBalanceCategory = {
          id: 0,
          text: "",
          is_foreign_currency: false
        }
        return await Promise.resolve({ response, data: empty })
      })
    })
}

export async function putMonthlyBalanceCategory (id: number, data: Interfaces.CategoryFormInputs, signal?: AbortSignal): Promise<Interfaces.MonthlyBalanceCategoryResponse> {
  return await fetch(`/api/v1/monthly_balance_category/${id}`, {
    method: "PUT",
    headers: getAuthHeaders(),
    ...credentialsOption,
    body: JSON.stringify(data),
    signal
  }).then(async (response): Promise<Interfaces.MonthlyBalanceCategoryResponse> => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err) => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const category: Interfaces.MonthlyBalanceCategory = {
        id: 0,
        text: "",
        is_foreign_currency: false
      }
      return await Promise.resolve({ response, data: category })
    })
  })
}

export async function patchMonthlyBalanceCategory (id: number, data: Interfaces.CategoryFormInputs, signal?: AbortSignal): Promise<Interfaces.MonthlyBalanceCategoryResponse> {
  return await fetch(`/api/v1/monthly_balance_category/${id}/`, {
    method: "PATCH",
    headers: getAuthHeaders(),
    ...credentialsOption,
    body: JSON.stringify(data),
    signal
  }).then(async (response): Promise<Interfaces.MonthlyBalanceCategoryResponse> => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err) => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const category: Interfaces.MonthlyBalanceCategory = {
        id: 0,
        text: "",
        is_foreign_currency: false
      }
      return await Promise.resolve({ response, data: category })
    })
  })
}

export async function getMonthlyBalanceCategories (signal?: AbortSignal): Promise<Interfaces.MonthlyBalanceCategoriesResponse> {
  return await fetch("/api/v1/monthly_balance_categories", { signal })
    .then(async response => {
      return await response.json().then(async (parsedJson): Promise<Interfaces.MonthlyBalanceCategoriesResponse> => {
        return { response, data: parsedJson }
      }).catch(async (err) => {
        // This catch handles the case where there is no json body in the response
        console.log(err)
        const emptyRes = [] as Interfaces.MonthlyBalanceCategory[]
        const empty = { count: 0, results: emptyRes }
        return await Promise.resolve({ response, data: empty })
      })
    })
}

export async function getMonthlyBalances (date?: string, signal?: AbortSignal): Promise<Interfaces.MonthlyBalancesPagedResponse> {
  let url = "/api/v1/monthly_balances"
  // TODO: validate parameters!
  if (date !== null && date !== undefined) {
    url += `?date=${date}`
  }
  return await fetch(url, { signal }).then(async (response): Promise<Interfaces.MonthlyBalancesPagedResponse> => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err) => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const emptyRes = [] as Interfaces.MonthlyBalanceWithNestedCategory[]
      const empty = { count: 0, results: emptyRes }
      return await Promise.resolve({ response, data: empty })
    })
  })
}

export async function getMonthlyBalance (id: string, signal?: AbortSignal): Promise<Interfaces.MonthlyBalanceResponse> {
  const url = `/api/v1/monthly_balances/${id}/`
  // TODO: validate parameters!
  return await fetch(url, { signal }).then(async (response) => {
    return await response.json().then(async (parsedJson): Promise<Interfaces.MonthlyBalanceResponse> => {
      return { response, data: parsedJson }
    }).catch(async (err) => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const empty: Interfaces.MonthlyBalanceWithNestedCategory = {
        id: 0,
        amount: 0,
        date: "",
        category: {
          id: 0,
          text: "",
          is_foreign_currency: false
        }
      }
      return await Promise.resolve({ response, data: empty })
    })
  })
}

export async function patchMonthlyBalances (id: number, formInfo: Interfaces.MonthlyBalanceForm, signal?: AbortSignal): Promise<Interfaces.MonthlyBalanceResponse> {
  const url = `/api/v1/monthly_balances/${id}/`
  // TODO: validate parameters!
  return await fetch(url, {
    method: "PATCH",
    headers: getAuthHeaders(),
    ...credentialsOption,
    body: JSON.stringify(formInfo),
    signal
  }).then(async (response): Promise<Interfaces.MonthlyBalanceResponse> => {
    return await response.json().then(async (parsedJson) => {
      return { response, data: parsedJson }
    }).catch(async (err) => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const empty: Interfaces.MonthlyBalanceWithNestedCategory = {
        id: 0,
        amount: 0,
        date: "",
        category: {
          id: 0,
          text: "",
          is_foreign_currency: false
        }
      }
      return await Promise.resolve({ response, data: empty })
    })
  })
}

export async function createOrUpdateMonthlyBalance (formInfo: Interfaces.MonthlyBalanceCreateUpdateForm, signal?: AbortSignal): Promise<Interfaces.MonthlyBalanceResponse> {
  const url = "/api/v1/monthly_balances/create-update"
  // TODO: validate parameters!
  return await fetch(url, {
    method: "POST",
    headers: {
      "Content-Type": "application/json",
      "X-CSRFToken": cookies.get("csrftoken")
    },
    credentials: "same-origin",
    body: JSON.stringify(formInfo),
    signal
  }).then(async (response): Promise<Interfaces.MonthlyBalanceResponse> => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err) => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const empty: Interfaces.MonthlyBalanceWithNestedCategory = {
        id: 0,
        amount: 0,
        date: "",
        category: {
          id: 0,
          text: "",
          is_foreign_currency: false
        }
      }
      return await Promise.resolve({ response, data: empty })
    })
  })
}
