import type { AlertDependencies, Asset, Category, Dividend, Expense, ExpenseFormInputs, ExpenseResponse, EndUserAsset, SimplifiedDividend, SubmissionDependencies, YearMonth } from "./interfaces"
import Alert from "react-bootstrap/Alert"
import React from "react"
/**
 * Creates an Intl.NumberFormat object based on the provided currency.
 *
 * @param {string} currency - The currency code (e.g., "USD", "EUR", "JPY").
 * @param {boolean} [forceYenDecimals=false] - Optional parameter to force specific decimal settings for Japanese Yen.
 * @returns {Intl.NumberFormat} - The Intl.NumberFormat object configured based on the provided currency.
 */
export const SmartFormatter = (currency: string, forceYenDecimals: boolean = false): Intl.NumberFormat => {
  switch (currency) {
    case "USD":
      return new Intl.NumberFormat("en-US", { style: "currency", currency: "USD" })
    case "EUR":
      return new Intl.NumberFormat("de-DE", { style: "currency", currency: "EUR" })
    default:
      return new Intl.NumberFormat("ja-JP",
        {
          style: "currency",
          minimumFractionDigits: 0,
          maximumFractionDigits: forceYenDecimals ? 4 : 0,
          currency: "JPY",
          // JPY does not round to the nearest value, but always rounds down
          // Technically by law any rounding method is allowed, as per the NTA
          // https://www.nta.go.jp/taxes/shiraberu/taxanswer/shohi/6383.htm
          // But it's common to truncate the demicals
          // https://www.freee.co.jp/kb/kb-invoice/consumption_tax_fraction/
          // Especially since the tax fraction is 1/8, so it's not a power of 2
          // And the NTA required to drop the fraction for some cases
          // See: https://www.nta.go.jp/taxes/shiraberu/taxanswer/shohi/6371.htm
          // （3）還付金に相当する消費税額に1円未満の端数があるときは、その端数を切り捨て、
          // 還付金に相当する消費税額が1円未満であるときは、1円とします。
          roundingMode: "floor"
        })
  }
}

/**
 * FormatterToTwoDecimals is an Intl.NumberFormat object configured for formatting numbers with a maximum of two decimal places in the "en-IN" locale.
 *
 * @type {Intl.NumberFormat}
 */
export const FormatterToTwoDecimals = new Intl.NumberFormat("en-IN", { maximumFractionDigits: 2 })

/**
 * Aggregates assets based on the 'text' property and calculates the sum of corresponding values.
 *
 * @param {Asset[]} assets - An array of Asset objects.
 * @returns {Asset[]} - An array of aggregated Asset objects.
 */
export const aggregateAssets = (assets: Asset[]): Asset[] => {
  const res = assets.reduce((acc: any, asset: any) => {
    if (acc[asset.text] === undefined) {
      acc[asset.text] = asset
    } else {
      const prev = acc[asset.text]
      acc[asset.text] = {
        account: {
          // We need an id to let react handle tables: which ID is not important as long as it's unique (*)
          id: prev.account.id,
          text: "Multiple accounts"
        },
        amount: prev.amount + asset.amount,
        bought_price_foreign: prev.bought_price_foreign + asset.bought_price_foreign,
        bought_price_local: prev.bought_price_local + asset.bought_price_local,
        current_foreign_value: prev.current_foreign_value + asset.current_foreign_value,
        current_local_value: prev.current_local_value + asset.current_local_value,
        // We assume a given asset is always bought with the same foreign currency among all accounts
        foreign_currency: prev.foreign_currency,
        // See above (*)
        id: prev.id,
        net_loss_foreign: prev.net_loss_foreign + asset.net_loss_foreign,
        net_loss_local: prev.net_loss_local + asset.net_loss_local,
        // We assume we'll only display assets with the same date
        record_date: prev.record_date,
        text: prev.text
      }
    }
    return acc
  }, {})
  return Object.values(res)
}

// FIXME: Confirm return type and write JSdocs
export const reduceAssets = (assets: Asset[]): Asset => {
  const res = assets.reduce((acc: any, asset: Asset) => {
    acc.bought_price_foreign = acc.bought_price_foreign + asset.bought_price_foreign
    acc.bought_price_local = acc.bought_price_local + asset.bought_price_local
    acc.current_foreign_value = acc.current_foreign_value + asset.current_foreign_value
    acc.current_local_value = acc.current_local_value + asset.current_local_value
    acc.net_loss_foreign = acc.net_loss_foreign + asset.net_loss_foreign
    acc.net_loss_local = acc.net_loss_local + asset.net_loss_local
    acc.record_date = asset.record_date
    return acc
  }, {
    bought_price_foreign: 0,
    bought_price_local: 0,
    current_foreign_value: 0,
    current_local_value: 0,
    net_loss_foreign: 0,
    net_loss_local: 0,
    record_date: 0
  })

  // We need IDs to let react handle tables: which ID do we use it not important, as long as they're unique within the table
  const propertiesToAdd = {
    account: {
      id: "-",
      text: "Total"
    },
    amount: "-",
    foreign_currency: "USD",
    id: "-",
    text: "Total"
  }

  return Object.assign(res, propertiesToAdd)
}

/**
 * Returns today's date in "YYYY-MM-DD" format.
 *
 * @deprecated
 * @returns {string} - The formatted current date string.
 */
export const formattedToday = (): string => {
  const options: Intl.DateTimeFormatOptions = { year: "numeric", month: "2-digit", day: "2-digit" }
  return new Date().toLocaleDateString("en-CA", options)
}

/**
 * Checks if the provided date string is in the format "YYYY-MM-01".
 *
 * @deprecated
 * @param {string} date - The input date string to be checked.
 * @returns {boolean} - True if the date is in the correct format, false otherwise.
 * @example
 * const isValid = isCorrectFormatDate("2022-01-01")
 */
export const isCorrectFormatDate = (date: string): boolean => {
  // NOTE: Many apis only accept YYYY-MM-01 format
  // Only accepts dates between 1000 and 2999: edit this regex if using historical data, of if you happen to be in year 3000+
  const dateRegex = /^(1|2)\d{3}-(0[1-9]|1[0-2])-01$/g
  return dateRegex.test(date)
}

/**
 * Adds the current value to the accumulator.
 *
 * @param {number} acc - The accumulator value.
 * @param {number} curr - The current value to be added to the accumulator.
 * @returns {number} - The result of adding the current value to the accumulator.
 */
export const accumulator = (acc: number, curr: number): number => acc + curr

/**
 * Compare two Asset objects based on their 'current_foreign_value' properties for sorting in descending order.
 *
 * @deprecated
 * @param {Asset} a - The first Asset object.
 * @param {Asset} b - The second Asset object.
 * @returns {number} - A negative, zero, or positive number indicating the sorting order.
 * @example
 * const assetsArray: Asset[] = [...]
 * const sortedAssets: Asset[] = assetsArray.sort(sortByCurrForeignValue)
 */
export const sortByCurrForeignValue = (a: Asset, b: Asset): number => b.current_foreign_value - a.current_foreign_value

/**
 * Compare two Asset objects based on their 'current_local_value' properties for sorting in descending order.
 *
 * @param {Asset} a - The first Asset object.
 * @param {Asset} b - The second Asset object.
 * @returns {number} - A negative, zero, or positive number indicating the sorting order.
 * @example
 * const assetsArray: Asset[] = [...]
 * const sortedAssets: Asset[] = assetsArray.sort(sortByCurrentLocalValue)
 */
export const sortByCurrentLocalValue = (a: Asset, b: Asset): number => b.current_local_value - a.current_local_value

/**
 * Compare two Expense objects based on their 'local_amount' properties for sorting in descending order.
 *
 * @deprecated
 * @param {Dividend} a - The first Dividend object.
 * @param {Dividend} b - The second Dividend object.
 * @returns {number} - A negative, zero, or positive number indicating the sorting order.
 * @example
 * const dividendsArray: Dividend[] = [...]
 * const sortedDividends: Dividend[] = dividendsArray.sort(sortByLocalAmount);
 */
export const sortByLocalAmount = (a: Dividend, b: Dividend): number => {
  const bValue = b.currency === "USD" ? b.local_amount : (b.foreign_amount / b.rate)
  const aValue = a.currency === "USD" ? a.local_amount : (a.foreign_amount / a.rate)
  return bValue - aValue
}

/**
 * Compare two Expense objects based on their 'date' properties for sorting in descending order.
 *
 * @param {Expense} a - The first Expense object.
 * @param {Expense} b - The second Expense object.
 * @returns {number} - A negative, zero, or positive number indicating the sorting order.
 * @example
 * const expensesArray: Expense[] = [...]
 * const sortedExpenses: Expense[] = expensesArray.sort(sortByDateValue)
 */
export const sortByDateValue = (a: Expense, b: Expense): number => (b.date.localeCompare(a.date))

/**
 * Calculate the date of the first day of the previous month based on the provided date string.
 *
 * @param {string} dateStr - The input date string in the format "YYYY-MM-dd".
 * @returns {string | null} - The date string of the first day of the previous month in the format "YYYY-MM-dd" or null if the input date is invalid.
 */
export const getPreviousYYYYMMdd = (dateStr: string): string | null => {
  if (!yearMonthIsValidDate(dateStr, true)) {
    return null
  }

  // Credits https://stackoverflow.com/a/66821884
  const date01 = `${dateStr}T00:00:00`
  const d = new Date(date01)
  let m = d.getMonth() - 1
  let y = d.getFullYear()

  // Handle the case when we've substracted one month from January, and moved to the previous year
  if (m === -1) {
    m = 11
    y -= 1
  }
  const prevMonth = new Date(y, m, 1)
  return `${prevMonth.getFullYear()}-${String(prevMonth.getMonth() + 1).padStart(2, "0")}-01`
}

/**
 * Calculate the date of the first day of the following month based on the provided date string.
 *
 * @param {string} dateStr - The input date string in the format "YYYY-MM-dd".
 * @returns {string | null} - The date string of the first day of the following month in the format "YYYY-MM-dd" or null if the input date is invalid.
 */
export const getNextYYYYMMdd = (dateStr: string): string | null => {
  if (!yearMonthIsValidDate(dateStr, true)) {
    return null
  }

  // Credits https://stackoverflow.com/a/66821884
  const date01 = `${dateStr}T00:00:00`
  const d = new Date(date01)
  let m = d.getMonth() + 1
  let y = d.getFullYear()

  if (m === 12) {
    m = 0
    y += 1
  }
  const prevMonth = new Date(y, m, 1)
  return `${prevMonth.getFullYear()}-${String(prevMonth.getMonth() + 1).padStart(2, "0")}-01`
}

/**
 * This function returns a YYYY-mm-dd date string that's previous month, today's day of the month
 * NOTE: In case the current day is greater than the total number of days in the previous month
 * it will return the last day of the previous month
 * e.g. 2023-03-31 will return 2023-02-28
 * @returns {String} The YYYY-mm-dd formatted date
*/
export const getAMonthAgoDate = (): string => {
  const currentDate = new Date()
  const currentDay = currentDate.getDate()
  const currentMonth = currentDate.getMonth()
  const currentYear = currentDate.getFullYear()

  let lastMonthDate = new Date(currentYear, currentMonth - 1, currentDay)

  // FIXME: The condition (lastMonthDate.getMonth() !== currentMonth - 1) is likely never false
  // so we might not need it anymore. However, we have no test coverage for this function
  // so let's leave the code as it is, and add an horrible patch on it, to avoid introducing regressions
  // Let's clean up the frontend code after we've delivered all the half done features that are waiting to be shipped
  if (lastMonthDate.getMonth() !== currentMonth - 1 && currentMonth !== 0) {
    lastMonthDate = new Date(currentYear, currentMonth, 0)
  }

  // "en-CA" format for dates is "YYYY-mm-dd"
  return lastMonthDate.toLocaleDateString("en-CA")
}

/**
 * Calculate the following year and month based on the provided YearMonth object.
 *
 * @param {YearMonth} target - The target YearMonth object containing 'year' and 'month' properties.
 * @returns {YearMonth} - The YearMonth object representing the following year and month.
 */
export const getFollowingYearMonth = (target: YearMonth): YearMonth => {
  const { year, month } = target

  if (year === null || year === undefined) {
    return {
      year: null,
      month: null
    }
  }

  let yearNum: number = parseInt(String(year), 10)
  if (month === null || month === undefined) {
    return {
      year: yearNum + 1,
      month: null
    }
  }

  let monthNum: number = parseInt(String(month), 10)
  monthNum += 1
  // NOTE: these are not javascript months, starting at 0, ending at 11.
  // These are human months, starting from 1 ending in 12
  if (monthNum === 13) {
    monthNum = 1
    yearNum += 1
  }

  return {
    year: yearNum,
    month: monthNum
  }
}

/**
 * Calculate the previous year and month based on the provided YearMonth object.
 *
 * @param {YearMonth} target - The target YearMonth object containing 'year' and 'month' properties.
 * @returns {YearMonth} - The YearMonth object representing the previous year and month.
 */
export const getPreviousYearMonth = (target: YearMonth): YearMonth => {
  const { year, month } = target

  if (year === null || year === undefined) {
    return {
      year: null,
      month: null
    }
  }

  let yearNum: number = parseInt(year, 10)
  if (month === null || month === undefined) {
    return {
      year: year - 1,
      month: null
    }
  }

  let monthNum: number = parseInt(month, 10)
  monthNum -= 1
  // NOTE: these are not javascript months, starting at 0, ending at 11.
  // These are human months, starting from 1 ending in 12
  if (monthNum < 1) {
    monthNum = 12
    yearNum -= 1
  }

  return {
    year: yearNum,
    month: monthNum
  }
}

// Credits https://bobbyhadz.com/blog/javascript-validate-date-yyyy-mm-dd
export const yearMonthIsValidDate = (dateStr: string, completeDate = true): boolean => {
  // Check if the string is a valid YYYY-mm-dd date
  //       if completeDate is true, checks for YYYY-mm instead
  if (typeof (dateStr) !== "string") {
    return false
  }

  // YYYY-mm
  let regex = /^\d{4}-\d{2}$/
  if (completeDate) {
    // YYYY-mm-dd
    regex = /^\d{4}-\d{2}-\d{2}$/
  }

  if (dateStr.match(regex) === null) {
    return false
  }

  // const dateStrWithDay = dateStr + "-01" // CHECK ME: Is it OK not to use this?
  // FIXME: add "-01" to the dateStr, to make it a valid date if needed
  const date = new Date(dateStr)
  const timestamp = date.getTime()

  if (typeof timestamp !== "number" || Number.isNaN(timestamp)) {
    return false
  }

  return date.toISOString().startsWith(dateStr)
}

/**
 * Validates JSON data against the EndUserAsset interface (i.e. Assets third party API return value)
 * @param {any} data - The JSON data to validate.
 * @returns {boolean} - True if the data matches the EndUserAsset format, otherwise false.
 */
export const isValidAssetFormat = (data: any): data is EndUserAsset => {
  // Array of top-level keys to check
  const keysToCheck = [
    "ticker",
    "amount",
    "currency",
    "sum_current",
    "sum_bought",
    "sum_net",
    "rate"
  ]

  for (const key of keysToCheck) {
    if (!(key in data)) {
      return false
    }
  }

  // Check top-level keys type
  if (
    typeof data.ticker !== "string" ||
    typeof data.amount !== "number" ||
    typeof data.rate !== "number" ||
    typeof data.currency !== "string" || data.currency.length !== 3
  ) {
    return false
  }

  // Array of nested keys to check
  const nestedKeysToCheck = ["foreign", "local"]

  for (const set of ["sum_current", "sum_bought", "sum_net"]) {
    if (typeof data[set] !== "object") {
      return false
    }

    for (const key of nestedKeysToCheck) {
      if (typeof data[set][key] !== "number") {
        return false
      }
    }

    // Check for extra properties within the nested object
    const nestedExtraKeys = Object.keys(data[set]).filter(
      key => !nestedKeysToCheck.includes(key)
    )
    if (nestedExtraKeys.length > 0) {
      return false
    }
  }

  // Check for extra properties in the top-level object
  const extraKeys = Object.keys(data).filter(key => !keysToCheck.includes(key))
  if (extraKeys.length > 0) {
    return false
  }

  return true
}

/**
 * Validates JSON data against the SimplifiedDividend interface. (i.e. Dividends third party API return value)
 * @param {any} data - The JSON data to validate.
 * @returns {boolean} - True if the data matches the SimplifiedDividend format, otherwise false.
 */
export const isDataValidForDividend = (data: any): data is SimplifiedDividend => {
  const keysToCheck = ["amount", "currency", "date", "rate", "ticker"]

  for (const key of keysToCheck) {
    if (!(key in data)) {
      return false
    }
  }

  // Check for extra properties in the top-level object
  const extraKeys = Object.keys(data).filter(key => !keysToCheck.includes(key))
  if (extraKeys.length > 0) {
    return false
  }

  // Check top-level keys type
  if (
    typeof data.amount !== "number" ||
    typeof data.currency !== "string" || data.currency.length !== 3 ||
    typeof data.date !== "string" ||
    !/^\d{4}-\d{2}-\d{2}$/.test(data.date) || // Check date format (YYYY-mm-dd)
    typeof data.rate !== "number" ||
    typeof data.ticker !== "string" // Corrected from 'number'
  ) {
    return false
  }

  return true
}

/**
 * Prevents the default form submission behavior.
 *
 * @param {React.FormEvent} e - The React FormEvent object.
 * @returns {void}
 */
export const preventFormSubmit = (e: React.FormEvent): void => {
  e.preventDefault()
}

/**
 * Creates a handler function for submitting expense forms.
 *
 * @param {Object} dependencies - An object containing functions and state setters required for form submission.
 * @param {Function} dependencies.setShowDismissable - Function to set the visibility of the dismissable alert.
 * @param {Function} dependencies.setIsSending - Function to set the sending state of the form.
 * @param {Function} dependencies.getIsSending - Function to get the current sending state of the form.
 * @param {Function} dependencies.createExpense - Function to create an expense based on the form values.
 * @param {Function} dependencies.setGenericFormError - Function to set generic form error messages.
 * @param {Function} dependencies.setIsUpdateSuccess - Function to set the success state of the form submission.
 * @param {Function} dependencies.setError - Function to set form field errors.
 * @param {Function} dependencies.reset - Function to reset the form fields.
 * @param {Object} dependencies.defaultValues - The default values for the form fields.
 *
 * @returns {Function} An async function that handles the expense form submission process.
 *                     This returned function takes the form values as a parameter and manages the entire
 *                     submission flow, including error handling and success/failure states.
 *
 * @example
 * const onSubmit = createExpenseSubmissionHandler({
 *   setShowDismissable,
 *   setIsSending,
 *   getIsSending: () => isSending,
 *   createExpense,
 *   setGenericFormError,
 *   setIsUpdateSuccess,
 *   setError,
 *   reset,
 *   defaultValues,
 * });
 *
 * // Later in your component
 * <form onSubmit={handleSubmit(onSubmit)}>
 *   // form fields
 * </form>
 */
export const createExpenseSubmissionHandler = (dependencies: SubmissionDependencies, signal?: AbortSignal): ((formValues: ExpenseFormInputs) => Promise<(() => void)>) => {
  const {
    setShowDismissable,
    setIsSending,
    getIsSending,
    createExpense,
    setGenericFormError,
    setIsUpdateSuccess,
    setError,
    reset,
    defaultValues
  } = dependencies

  return async (formValues: ExpenseFormInputs) => {
    if (getIsSending()) return () => {}

    setShowDismissable(false)
    setIsSending(true)

    let timer: ReturnType<typeof setTimeout> | null = null
    const cleanupFunction = (): void => {
      if (timer !== null) clearTimeout(timer)
    }

    try {
      const res: ExpenseResponse = await createExpense(formValues, signal)

      if (!res?.response?.ok) {
        if (res?.data !== undefined && typeof res.data === "object") {
          for (const [key, value] of Object.entries(res.data)) {
            console.log("Expense creation form error(s):", key, value)
            // Set error for existing fields
            if (key in formValues) {
              setError(key as keyof ExpenseFormInputs, { message: value[0] })
            } else {
              // Other validation errors
              setGenericFormError(value[0])
            }
          }
        } else {
          // Handle 40X,50X errors in here
          setGenericFormError("Something is wrong with the server, try again later!")
        }
        setIsUpdateSuccess(false)
      } else {
        reset(defaultValues)
        setGenericFormError("")
        setIsUpdateSuccess(true)
      }
    } catch (error) {
      console.log("Expense creation error:", error)
      setIsUpdateSuccess(false)
    } finally {
      setIsSending(false)
      setShowDismissable(true)
      timer = setTimeout(() => {
        setShowDismissable(false)
      }, 1500)
    }

    return cleanupFunction
  }
}

/**
 * Renders a dismissable alert
 *
 * @param {AlertDependencies} dependencies - An object containing functions and state setters required for rendering the alert.
 * @returns {JSX.Element | null} - The JSX element representing the dismissable alert or null if the alert is not visible.
 */
export const renderDismissableAlert = (dependencies: AlertDependencies): JSX.Element | null => {
  const {
    setShowDismissable,
    getIsShowDismissable,
    successMessage,
    failureMessage,
    getIsUpdateSuccess
  } = dependencies

  if (getIsShowDismissable()) {
    return <Alert variant={getIsUpdateSuccess() ? "success" : "danger"} onClose={() => { setShowDismissable(false) }} dismissible>
        <Alert.Heading>{getIsUpdateSuccess() ? successMessage : failureMessage }</Alert.Heading>
      </Alert>
  } else {
    return null
  }
}

const isLocalhost = (): boolean => {
  const host = window.location.hostname
  // Do not compare === "localhost" as host contains the port number as well
  return host.includes("localhost") || host.includes("127.0.0.1")
}

const isHttps = (): boolean => {
  return window.location.protocol === "https:"
}

export const checkEnvironment = (): { isLocalhost: boolean, isHttps: boolean } => {
  const hots = isLocalhost()
  const protocol = isHttps()

  return {
    isLocalhost: hots,
    isHttps: protocol
  }
}

export const locallyLookUpCategory = (categoryId: number, categories: Category[]): string => {
  const found = categories.find((category: Category, index: number) => {
    if (category.id === categoryId) {
      return true
    }
    return false
  })
  if (found === undefined) {
    // FIXME: decide how to properly handle when the API returns garbage
    return "ERROR"
  }
  return found.text
}
