import Cookies from "universal-cookie"
const cookies = new Cookies()

// Common fetch headers with CSRF token
export const getAuthHeaders = (): Record<string, string> => ({
  "Content-Type": "application/json",
  "X-CSRFToken": cookies.get("csrftoken")
})

// Common credentials option
export const credentialsOption = { credentials: "same-origin" as const }

// Generic error handler for empty responses
export const handleEmptyResponse = (err: Error, emptyData: any): any => {
  console.log(err)
  return { response: null, data: emptyData }
}
