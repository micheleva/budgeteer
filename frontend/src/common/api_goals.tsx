import type * as Interfaces from "./interfaces"
import { getAuthHeaders, credentialsOption } from "./api_helpers"

export async function createGoal (data: any, signal?: AbortSignal): Promise<Interfaces.GoalResponse> {
  return await fetch("/api/v1/goals", {
    method: "POST",
    headers: getAuthHeaders(),
    ...credentialsOption,
    body: JSON.stringify(data),
    signal
  }).then(async (response): Promise<Interfaces.GoalResponse> => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err): Promise<Interfaces.GoalResponse> => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const empty: Interfaces.Goal = {
        id: 0,
        text: "",
        amount: 0,
        note: "",
        is_archived: false
      }
      return await Promise.resolve({ response, data: empty })
    })
  })
}

export async function getGoals (signal?: AbortSignal): Promise<Interfaces.GoalsResponse> {
  return await fetch("/api/v1/goals", { signal })
    .then(async (response): Promise<Interfaces.GoalsResponse> => {
      return await response.json().then(parsedJson => {
        return { response, data: parsedJson }
      }).catch(async (err) => {
        // This catch handles the case where there is no json body in the response
        console.log(err)
        const emptyRes = [] as Interfaces.Goal[]
        const empty = { count: 0, results: emptyRes }
        return await Promise.resolve({ response, data: empty })
      })
    })
}

export async function getGoal (id: number, signal?: AbortSignal): Promise<Interfaces.GoalResponse> {
  return await fetch(`/api/v1/goals/${id}/`, { signal })
    .then(async (response): Promise<Interfaces.GoalResponse> => {
      return await response.json().then(parsedJson => {
        return { response, data: parsedJson }
      }).catch(async (err): Promise<Interfaces.GoalResponse> => {
        // This catch handles the case where there is no json body in the response
        console.log(err)
        const empty: Interfaces.Goal = {
          id: 0,
          text: "",
          amount: 0,
          note: "",
          is_archived: false
        }
        return await Promise.resolve({ response, data: empty })
      })
    })
}

export async function patchGoal (id: number, data: Interfaces.GoalFormInputs, signal?: AbortSignal): Promise<Interfaces.GoalResponse> {
  return await fetch(`/api/v1/goals/${id}/`, {
    method: "PATCH",
    headers: getAuthHeaders(),
    ...credentialsOption,
    body: JSON.stringify(data),
    signal
  }).then(async (response): Promise<Interfaces.GoalResponse> => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err): Promise<Interfaces.GoalResponse> => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const goal: Interfaces.Goal = {
        id: 0,
        text: "",
        amount: 0,
        note: "",
        is_archived: false
      }
      return await Promise.resolve({ response, data: goal })
    })
  })
}

// This API does not return JSON, hence it's a bit different from the other funcs
export async function deleteGoal (id: number, signal?: AbortSignal): Promise<Interfaces.HTTPResponse> {
  return await fetch(`/api/v1/goals/${id}/`, {
    method: "DELETE",
    headers: getAuthHeaders(),
    ...credentialsOption,
    signal
  })
}
