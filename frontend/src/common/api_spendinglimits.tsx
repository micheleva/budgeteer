import type * as Interfaces from "./interfaces"
import { getAuthHeaders, credentialsOption } from "./api_helpers"

export async function createSpendingLimit (data: Interfaces.SpendingLimitFormInputs, signal?: AbortSignal): Promise<Interfaces.SpendingLimitResponse> {
  return await fetch("/api/v1/spending-limits", {
    method: "POST",
    headers: getAuthHeaders(),
    ...credentialsOption,
    body: JSON.stringify(data),
    signal
  }).then(async (response): Promise<Interfaces.SpendingLimitResponse> => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err) => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const empty: Interfaces.SpendingLimit = {
        id: 0,
        category: 0,
        category_text: "",
        amount: 0,
        text: "",
        currency: "",
        days: 0
      }
      return await Promise.resolve({ response, data: empty })
    })
  })
}

export async function getSpendingLimits (signal?: AbortSignal): Promise<Interfaces.SpendingLimitsResponse> {
  return await fetch("/api/v1/spending-limits", { signal })
    .then(async (response): Promise<Interfaces.SpendingLimitsResponse> => {
      return await response.json().then(parsedJson => {
        return { response, data: parsedJson }
      }).catch(async (err) => {
        // This catch handles the case where there is no json body in the response
        console.log(err)
        const emptyRes = [] as Interfaces.SpendingLimit[]
        const empty = { count: 0, results: emptyRes }
        return await Promise.resolve({ response, data: empty })
      })
    })
}

export async function getSpendingLimit (id: number, signal?: AbortSignal): Promise<Interfaces.SpendingLimitResponse> {
  return await fetch(`/api/v1/spending-limits/${id}`, { signal })
    .then(async (response): Promise<Interfaces.SpendingLimitResponse> => {
      return await response.json().then(parsedJson => {
        return { response, data: parsedJson }
      }).catch(async (err): Promise<Interfaces.SpendingLimitResponse> => {
        // This catch handles the case where there is no json body in the response
        console.log(err)
        const empty: Interfaces.SpendingLimit = {
          id: 0,
          category: 0,
          category_text: "",
          amount: 0,
          text: "",
          currency: "",
          days: 0
        }
        return await Promise.resolve({ response, data: empty })
      })
    })
}

export async function updateSpendingLimit (id: number, payload: Interfaces.UpdateSpendingLimitPayload, signal?: AbortSignal): Promise<Interfaces.SpendingLimitResponse> {
  const response = await fetch(`/api/v1/spending-limits/${id}`, {
    method: "PATCH",
    headers: getAuthHeaders(),
    ...credentialsOption,
    body: JSON.stringify(payload),
    signal
  })

  if (!response.ok) {
    throw new Error(`Failed to update SpendingLimit with ID ${id}`)
  }

  try {
    const resJson = await response.json()
    return { response, data: resJson }
  } catch (err) {
    console.log(err)
    const empty: Interfaces.SpendingLimit = {
      id: 0,
      amount: 0,
      currency: "",
      days: 0,
      text: "",
      category: 0,
      category_text: ""
    }
    return await Promise.resolve({ response, data: empty })
  }
}

export async function deleteSpendingLimit (id: number, signal?: AbortSignal): Promise<Interfaces.HTTPResponse> {
  return await fetch(`/api/v1/spending-limits/${id}/`, {
    method: "DELETE",
    headers: getAuthHeaders(),
    ...credentialsOption,
    signal
  })
}
