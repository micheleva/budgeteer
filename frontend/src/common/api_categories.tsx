import type * as Interfaces from "./interfaces"
import { getAuthHeaders, credentialsOption, handleEmptyResponse } from "./api_helpers"

export async function createCategory (data: any, signal?: AbortSignal): Promise<Interfaces.CategoryResponse> {
  return await fetch("/api/v1/categories", {
    method: "POST",
    headers: getAuthHeaders(),
    ...credentialsOption,
    body: JSON.stringify(data),
    signal
  }).then(async (response): Promise<Interfaces.CategoryResponse> => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err): Promise<Interfaces.CategoryResponse> => {
      // This catch handles the case where there is no json body in the response
      const empty: Interfaces.Category = {
        id: 0,
        text: ""
      }
      return handleEmptyResponse(err, empty)
    })
  })
}

export async function getCategories (signal?: AbortSignal): Promise<Interfaces.CategoriesResponse> {
  return await fetch("/api/v1/categories", {
    signal
  }).then(async (response): Promise<Interfaces.CategoriesResponse> => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err): Promise<Interfaces.CategoriesResponse> => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const emptyRes = [] as Interfaces.Category[]
      const empty = { count: 0, results: emptyRes }
      return await Promise.resolve({ response, data: empty })
    })
  })
}

export async function getCategory (id: number, signal?: AbortSignal): Promise<Interfaces.CategoryResponse> {
  return await fetch(`/api/v1/category/${id}`, {
    signal
  }).then(async (response): Promise<Interfaces.CategoryResponse> => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err): Promise<Interfaces.CategoryResponse> => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const empty: Interfaces.Category = {
        id: 0,
        text: ""
      }
      return await Promise.resolve({ response, data: empty })
    })
  })
}

export async function putCategory (id: number, data: Interfaces.CategoryFormInputs, signal?: AbortSignal): Promise<Interfaces.CategoryResponse> {
  return await fetch(`/api/v1/category/${id}`, {
    method: "PUT",
    headers: getAuthHeaders(),
    ...credentialsOption,
    body: JSON.stringify(data),
    signal
  }).then(async (response): Promise<Interfaces.CategoryResponse> => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err) => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const category: Interfaces.Category = {
        id: 0,
        text: ""
      }
      return await Promise.resolve({ response, data: category })
    })
  })
}

export async function patchCategory (id: number, data: Interfaces.CategoryFormInputs, signal?: AbortSignal): Promise<Interfaces.CategoryResponse> {
  return await fetch(`/api/v1/category/${id}`, {
    method: "PATCH",
    headers: getAuthHeaders(),
    ...credentialsOption,
    body: JSON.stringify(data),
    signal
  }).then(async (response): Promise<Interfaces.CategoryResponse> => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err): Promise<Interfaces.CategoryResponse> => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const category: Interfaces.Category = {
        id: 0,
        text: ""
      }
      return await Promise.resolve({ response, data: category })
    })
  })
}
