import type * as Interfaces from "./interfaces"
import { getAuthHeaders, credentialsOption } from "./api_helpers"

export async function getAssets (filters: Interfaces.AssetFormFilter | null, signal?: AbortSignal): Promise<Interfaces.AssetsResponse> {
  let url = "/api/v1/assets"
  const queryParams = new URLSearchParams()
  if (filters?.name !== null && filters?.name !== undefined) queryParams.append("name", filters.name)
  if (filters?.date !== null && filters?.date !== undefined) queryParams.append("date", filters.date)
  if (filters?.account !== null && filters?.account !== undefined) queryParams.append("account_id", String(filters.account))

  url = `${url}?${queryParams.toString()}`
  return await fetch(url, { signal }).then(async (response) => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err) => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const emptyRes = [] as Interfaces.Asset[]
      const empty = { count: 0, results: emptyRes }
      return await Promise.resolve({ response, data: empty })
    })
  })
}

export async function createAsset (data: any, signal?: AbortSignal): Promise<Interfaces.AssetResponse> {
  return await fetch("/api/v1/assets/create", {
    method: "POST",
    headers: getAuthHeaders(),
    ...credentialsOption,
    body: JSON.stringify(data),
    signal
  }).then(async (response): Promise<Interfaces.AssetResponse> => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err) => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const emptyRes: Interfaces.Asset = {
        id: 0,
        account: 0,
        amount: 0,
        text: "",
        foreign_currency: "",
        current_foreign_value: 0,
        current_local_value: 0,
        bought_price_foreign: 0,
        bought_price_local: 0,
        net_loss_foreign: 0,
        net_loss_local: 0,
        record_date: ""
      }
      return await Promise.resolve({ response, data: emptyRes })
    })
  })
}

// Yes, method is delete, but post. DELETE should be idempodent, but we can pass multiple IDs here
export async function deleteAssets (data: any, signal?: AbortSignal): Promise<Interfaces.DeletedAssetsResponse> {
  return await fetch("/api/v1/assets/delete", {
    method: "POST",
    headers: getAuthHeaders(),
    ...credentialsOption,
    body: JSON.stringify(data),
    signal
  }).then(async (response): Promise<Interfaces.DeletedAssetsResponse> => {
    console.log(response)
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err) => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const data = { records_deleted: 0 }
      const emptyRes: Interfaces.DeletedAssetsResponse = { response, data }
      return await Promise.resolve(emptyRes)
    })
  })
}

export async function getAssetsData (account: Interfaces.Account, date: string, url: string, signal?: AbortSignal): Promise<Interfaces.EndUserAssetResponse> {
  // TODO: fail if url is empty
  const finalurl = `${url}/${account.text}/${date}.json`
  return await fetch(finalurl, { signal }).then(async (response) => {
    if (response.status !== 200) {
      console.log(`HTTP error! status: ${response.status} from ${url}`)
      // console.log(err)
      const emptyAssets = [] as Interfaces.EndUserAsset[]
      const emptyRes: Interfaces.EndUserAssetPagedData = { account, assets: emptyAssets }
      return await Promise.resolve({ response, data: emptyRes })
    }
    return await response.json().then(async (parsedJson): Promise<Interfaces.EndUserAssetResponse> => {
      const formattedData: Interfaces.EndUserAssetPagedData = { account, assets: parsedJson }
      return { response, data: formattedData }
    }).catch(async (err): Promise<Interfaces.EndUserAssetResponse> => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const emptyAssets = [] as Interfaces.EndUserAsset[]
      const emptyRes: Interfaces.EndUserAssetPagedData = { account, assets: emptyAssets }
      return await Promise.resolve({ response, data: emptyRes })
    })
  })
}

export async function getAssetsDates (signal?: AbortSignal): Promise<Interfaces.AssetsDate> {
  return await fetch("/api/v1/assets/list_dates", { signal }).then(async (response) => {
    if (response.status !== 200) {
      console.log(`HTTP error! status: ${response.status} from "/api/v1/assets/list_dates"`)
      const emptyDates = [] as string[]
      return await Promise.resolve({ response, data: emptyDates })
    }
    return await response.json().then(async (parsedJson): Promise<Interfaces.AssetsDate> => {
      return { response, data: parsedJson }
    }).catch(async (err): Promise<Interfaces.AssetsDate> => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const empty = [] as string[]
      return await Promise.resolve({ response, data: empty })
    })
  })
}

export async function getAllMonthlyBalancesAndAssets (signal?: AbortSignal): Promise<Interfaces.MonthlyBalancesWithFlatCategoryPagedResponse> {
  const url = "/api/v1/monthly_balances_and_assets"
  return await fetch(url, { signal }).then(async (response): Promise<Interfaces.MonthlyBalancesWithFlatCategoryPagedResponse> => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err) => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const emptyRes = [] as Interfaces.MonthlyBalanceWithFlatCategory[]
      const empty = { count: 0, results: emptyRes }
      return await Promise.resolve({ response, data: empty })
    })
  })
}