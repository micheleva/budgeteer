import { getAuthHeaders, credentialsOption } from "./api_helpers"

export async function isAsrEnabled (signal?: AbortSignal): Promise<boolean> {
  return await fetch("/api/alpha/asr-enabled", { signal })
    .then(async (response) => {
      return await response.json().then(parsedJson => {
        return parsedJson.enabled
      }).catch(async (err) => {
        console.log(err)
        // In the backend we assume ASR is enabled by default, however, to avoid
        // building extra logic in the frontend, we just return false if the request fails
        return await Promise.resolve(false)
      })
    })
}

export async function postAudio (audioBlob: any, signal?: AbortSignal): Promise<any> { // FIX ME: do not use any :O <===
  const formData = new FormData()
  formData.append("audio", audioBlob, "recording.wav")

  try {
    const response = await fetch("/api/alpha/speech-to-text", {
      method: "POST",
      headers: getAuthHeaders(),
      ...credentialsOption,
      body: formData,
      signal
    })

    if (!response.ok) {
      throw new Error(`HTTP error! status: ${response.status}`)
    }

    const result = await response.json()
    console.log("Speech to text result:", result)
    return result
  } catch (error) {
    console.error("Error sending audio:", error)
    throw error
  }
}

export async function dummyPostAudio (audioBlob: any, signal?: AbortSignal): Promise<any> {
  const formData = new FormData()
  formData.append("audio", audioBlob, "recording.wav")

  try {
    const response = await fetch("/api/alpha/speech-to-text/dummy", {
      method: "GET",
      headers: getAuthHeaders(),
      ...credentialsOption,
      signal
    })

    if (!response.ok) {
      throw new Error(`HTTP error! status: ${response.status}`)
    }

    const result = await response.json()
    console.log("Speech to text result:", result)
    return result
  } catch (error) {
    console.error("Error sending audio:", error)
    throw error
  }
}
