import type * as Interfaces from "./interfaces"
import { getAuthHeaders, credentialsOption } from "./api_helpers"
import Cookies from "universal-cookie"
const cookies = new Cookies()

export async function createBudget (data: Interfaces.BudgetFormInput, signal?: AbortSignal): Promise<Interfaces.BudgetResponse> {
  return await fetch("/api/v1/budgets", {
    method: "POST",
    headers: getAuthHeaders(),
    ...credentialsOption,
    body: JSON.stringify(data),
    signal
  }).then(async (response): Promise<Interfaces.BudgetResponse> => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err): Promise<Interfaces.BudgetResponse> => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const emptyRes: Interfaces.Budget = {
        year: 0,
        amount: 0
      }
      return await Promise.resolve({ response, data: emptyRes })
    })
  })
}

export async function getBudgets (target: Interfaces.YearMonth, signal?: AbortSignal): Promise<Interfaces.BudgetsResponse> {
  // TODO: validate parameters!
  const { year, month } = target
  let url = `/api/v1/budgets?year=${year}`
  if (month !== null) {
    url += `&month=${month}`
  }
  return await fetch(url, {
    method: "GET",
    headers: getAuthHeaders(),
    ...credentialsOption,
    signal
  }).then(async (response): Promise<Interfaces.BudgetsResponse> => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err): Promise<Interfaces.BudgetsResponse> => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const emptyRes: Interfaces.BudgetWithCategory[] = []
      const empty = { count: 0, results: emptyRes }
      return await Promise.resolve({ response, data: empty })
    })
  })
}

export async function getBudget (id: number, signal?: AbortSignal): Promise<Interfaces.BudgetWithIdResponse> {
  // TODO: validate parameters!
  const url = `/api/v1/budgets/${id}`
  return await fetch(url, {
    method: "GET",
    headers: getAuthHeaders(),
    ...credentialsOption,
    signal
  }).then(async (response): Promise<Interfaces.BudgetWithIdResponse> => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err): Promise<Interfaces.BudgetWithIdResponse> => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const emptyRes: Interfaces.BudgetWithId = {
        year: 0,
        amount: 0
      }
      return await Promise.resolve({ response, data: emptyRes })
    })
  })
}

export async function patchBudget (id: number, data: Interfaces.BudgetEditFormInputs, signal?: AbortSignal): Promise<Interfaces.BudgetPatchResponse> {
  // TODO: validate parameters!
  const url = `/api/v1/budgets/${id}/`
  return await fetch(url, {
    method: "PATCH",
    headers: getAuthHeaders(),
    ...credentialsOption,
    body: JSON.stringify(data),
    signal
  }).then(async (response) => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err): Promise<Interfaces.BudgetPatchResponse> => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const category: Interfaces.BudgetPatchFormResponse = {
        amount: 0,
        allocated_amount: null
      }
      return await Promise.resolve({ response, data: category })
    })
  })
}

export async function getYearlyBudgetAggregate (target: Interfaces.YearMonth, signal?: AbortSignal): Promise<Interfaces.BudgetYearlyAggregateResponse> {
  // TODO: validate parameters!
  const { year } = target
  const url = `/api/v1/budgets/yearly-aggregate/${year}`
  return await fetch(url, {
    method: "GET",
    headers: {
      "Content-Type": "application/json",
      "X-CSRFToken": cookies.get("csrftoken")
    },
    credentials: "same-origin",
    signal
  }).then(async (response): Promise<Interfaces.BudgetYearlyAggregateResponse> => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err): Promise<Interfaces.BudgetYearlyAggregateResponse> => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const emptyRes: Interfaces.BudgetYearlyAggregatePagedData = {
        category: 0,
        sum_amount: 0
      }
      return await Promise.resolve({ response, data: emptyRes })
    })
  })
}
