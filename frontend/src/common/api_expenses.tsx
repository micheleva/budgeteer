import type * as Interfaces from "./interfaces"
import { getAuthHeaders, credentialsOption } from "./api_helpers"

export async function getExpensesByCategoryId (categoryId: number, start: string, end: string, note?: string | null, signal?: AbortSignal): Promise<Interfaces.ExpensesResponse> {
  // TODO: validate parameters!
  let url = `/api/v1/expenses/create-list?category_id=${categoryId}&format=json&huge_page=yes&start=${start}&end=${end}`
  if (note !== null && note !== undefined) {
    url += `&note=${note}`
  }
  return await fetch(encodeURI(url), { signal })
    .then(async (response): Promise<Interfaces.ExpensesResponse> => {
      return await response.json().then(parsedJson => {
        return { response, data: parsedJson }
      }).catch(async (err): Promise<Interfaces.ExpensesResponse> => {
        // This catch handles the case where there is no json body in the response
        console.log(err)
        const emptyRes: Interfaces.Expense[] = []
        const empty = { count: 0, results: emptyRes }
        return await Promise.resolve({ response, data: empty })
      })
    })
}

export async function getExpensesByDates (start: string, end: string, signal?: AbortSignal): Promise<Interfaces.ExpensesResponse> {
  // TODO: validate parameters!
  const url = `/api/v1/expenses/create-list?format=json&huge_page=yes&start=${start}&end=${end}`
  return await fetch(encodeURI(url), { signal })
    .then(async (response): Promise<Interfaces.ExpensesResponse> => {
      return await response.json().then(parsedJson => {
        return { response, data: parsedJson }
      }).catch(async (err): Promise<Interfaces.ExpensesResponse> => {
        // This catch handles the case where there is no json body in the response
        console.log(err)
        const emptyRes: Interfaces.Expense[] = []
        const empty = { count: 0, results: emptyRes }
        return await Promise.resolve({ response, data: empty })
      })
    })
}

export async function createExpense (data: any, signal?: AbortSignal): Promise<Interfaces.ExpenseResponse> {
  return await fetch("/api/v1/expenses/create-list", {
    method: "POST",
    headers: getAuthHeaders(),
    ...credentialsOption,
    body: JSON.stringify(data),
    signal
  }).then(async (response): Promise<Interfaces.ExpenseResponse> => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err): Promise<Interfaces.ExpenseResponse> => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const empty: Interfaces.Expense = {
        id: 0,
        amount: "0",
        category_id: 0,
        category_text: "",
        note: "",
        date: ""
      }
      return await Promise.resolve({ response, data: empty })
    })
  })
}

export async function getExpense (id: number, signal?: AbortSignal): Promise<Interfaces.ExpenseWithCategoryResponse> {
  return await fetch(`/api/v1/expenses/${id}/`, { signal })
    .then(async (response) => {
      return await response.json().then(parsedJson => {
        console.log("Expense data:", parsedJson)
        return { response, data: parsedJson }
      }).catch(async (err: Error): Promise<Interfaces.ExpenseWithCategoryResponse> => {
        // This catch handles the case where there is no json body in the response
        console.log(err)
        const empty: Interfaces.ExpenseWithCategoryId = {
          id: 0,
          amount: "0",
          category: 0,
          note: "",
          date: ""
        }
        return await Promise.resolve({ response, data: empty })
      })
    })
}

// This API does not return JSON, hence it's a bit different from the other funcs
export async function deleteExpense (id: number, signal?: AbortSignal): Promise<Interfaces.HTTPResponse> {
  return await fetch(`/api/v1/expenses/${id}/`, {
    method: "DELETE",
    headers: getAuthHeaders(),
    ...credentialsOption,
    signal
  })
}

export async function patchExpense (id: number, data: Interfaces.PatchExpenseFormInputs, signal?: AbortSignal): Promise<Interfaces.ExpensePatchResponse> {
  return await fetch(`/api/v1/expenses/${id}/`, {
    method: "PATCH",
    headers: getAuthHeaders(),
    ...credentialsOption,
    body: JSON.stringify(data),
    signal
  }).then(async (response): Promise<Interfaces.ExpensePatchResponse> => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err) => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const empty: Interfaces.ExpenseWihtoutID = {
        amount: "0",
        category: 0,
        note: "",
        date: ""
      }
      return await Promise.resolve({ response, data: empty })
    })
  })
}

export async function getExpensesAggregateByMonth (signal?: AbortSignal): Promise<Interfaces.AggregateExpensesByMonthResponse> {
  const url = "/api/v1/expenses/aggregate/by_month"
  return await fetch(encodeURI(url), { signal })
    .then(async response => {
      return await response.json().then(parsedJson => {
        return { response, data: parsedJson }
      }).catch(async (err): Promise<Interfaces.AggregateExpensesByMonthResponse> => {
        // This catch handles the case where there is no json body in the response
        console.log(err)
        const emptyRes = [] as Interfaces.AggregateExpenseByMonth[]
        const empty = { count: 0, results: emptyRes }
        return await Promise.resolve({ response, data: empty })
      })
    })
}

export async function getExpensesAggregateByCategory (start: string, end: string, signal?: AbortSignal): Promise<Interfaces.AggregateExpensesByCategoryResponse> {
  // TODO: validate parameters!
  const url = `/api/v1/expenses/aggregate/by_category?start=${start}&end=${end}`
  return await fetch(encodeURI(url), { signal })
    .then(async (response) => {
      return await response.json().then(parsedJson => {
        return { response, data: parsedJson }
      }).catch(async (err): Promise<Interfaces.AggregateExpensesByCategoryResponse> => {
        // This catch handles the case where there is no json body in the response
        console.log(err)
        const emptyRes = [] as Interfaces.AggregateExpenseByCategory[]
        const empty = { count: 0, results: emptyRes }
        return await Promise.resolve({ response, data: empty })
      })
    })
}

export async function getExpenseCandiates (signal?: AbortSignal): Promise<Interfaces.FastExpenseCandidateResponse> {
  return await fetch("/api/v1/expenses/made_often", { signal })
    .then(async response => {
      return await response.json().then(parsedJson => {
        return { response, data: parsedJson }
      }).catch(async (err): Promise<Interfaces.FastExpenseCandidateResponse> => {
        // This catch handles the case where there is no json body in the response
        console.error(err)
        const emptyRes = [] as Interfaces.FastExpenseCandidate[]
        const empty = { count: 0, results: emptyRes }
        return await Promise.resolve({ response, data: empty })
      })
    })
}
