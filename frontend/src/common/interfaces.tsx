import type React from "react"

// Models, entities

/**
 * Represents a user expense transaction with category and date information.
 * Used for displaying and managing expense records.
 * Maps to the Expense model in the backend.
 */
export interface Expense {
  id: number
  // The backend is returning a string, as the amount is stored as Decmial
  amount: string
  category_id: number
  category_text: string
  note: string
  date: string
}

/**
 * Represents a user expense transaction with category and date information.
 * Used for displaying and managing expense records.
 * Maps to the Expense model in the backend.
 * The category is not in a nested object, but on the same level as other properties.
 */
export interface ExpenseWithCategoryId extends Omit<Expense, "category_id" | "category_text"> {
  category: number
}

export interface ExpenseWithCategoryResponse {
  response: HTTPResponse
  data: ExpenseWithCategoryId
}

/**
 * Represents an expense without an ID, typically used for creating new expenses.
 * Contains the minimal fields needed for expense creation.
 * Note the typo in the interface name (should be "WithoutID").
 */
export interface ExpenseWihtoutID {
  // The backend is returning a string, as the amount is stored as Decmial
  amount: string
  // NOTE: this 'category' is an int, not an Category object
  category: number
  note: string
  date: string
}

/**
 * Form inputs for creating an expense.
 * Contains the fields needed for the expense creation form.
 */
export interface ExpenseFormInputs {
  amount: number | null
  category?: number // This will make the linter shut up about errors.category not existing
  category_id: number
  note: string
  date: string
}

/**
 * Form inputs for updating an existing expense.
 * Used in the expense edit form.
 */
export interface PatchExpenseFormInputs {
  amount: number
  category: number
  note: string
  date: string
}

/**
 * Represents a goal to save towards, such as a financial target.
 * Maps to the Goal model in the backend.
 * Goals have an amount to reach and can be archived when completed.
 */
export interface Goal {
  id: number
  text: string
  amount: number
  note: string
  is_archived: boolean
}

/**
 * Represents a category for expenses.
 * Maps to the Category model in the backend.
 * Categories can be archived to hide them from active use.
 */
export interface Category {
  id: number
  text: string
  is_archived?: boolean
}

/**
 * Represents a category for savings.
 * Maps to the SavingCategory model in the backend.
 * Similar to regular categories but used specifically for savings.
 */
export interface SavingCategory {
  id: number
  text: string
  is_archived?: boolean
}

/**
 * Form inputs for creating or updating a category.
 * Used in the category creation and edit forms.
 */
export interface CategoryFormInputs {
  // On creation we do not allow to set 'is_archived' to True
  text: string
  is_archived?: boolean
}

/**
 * Form inputs for creating or updating a saving category.
 * Used in the saving category creation and edit forms.
 */
export interface SavingCategoryFormInputs {
  text: string
  is_archived?: boolean
}

/**
 * Represents a budget allocation for either a regular category or a saving category.
 * Maps to the Budget model in the backend.
 * Note: A budget can be associated with either a category OR a savingcategory, but not both.
 * For saving categories, allocated_amount represents the actual amount allocated.
 */
export interface Budget {
  category?: number
  savingcategory?: number
  year: number
  month?: number | null
  amount: number
  allocated_amount?: number | null
}

/**
 * Form inputs for creating or updating a budget.
 * Contains fields for both regular and saving category budgets.
 */
export interface BudgetFormInput {
  category?: number
  savingcategory?: number
  date?: string
  year: number
  month?: number | null
  amount: number
  allocated_amount?: number | null
  isYearlyBudget?: boolean | null
  errors?: { string: { message: string[] } }
}

/**
 * Form inputs for editing a budget.
 * Simplified form for updating just the amount.
 */
export interface BudgetEditFormInputs {
  amount: number
}

/**
 * Form inputs for editing a saving budget.
 * Includes both amount and allocated_amount fields.
 */
export interface SavingBudgetEditFormInputs {
  amount: number
  allocated_amount: number
}

/**
 * Represents a monthly balance record.
 * Maps to the MonthlyBalance model in the backend.
 * Tracks balances for different categories on specific dates.
 */
export interface MonthlyBalance {
  id: number
  amount: number
  category_id: number
  category_text: string
  date: string
}

/**
 * Form inputs for creating or updating a monthly balance.
 * Used for the monthly balance creation and edit forms.
 */
export interface MonthlyBalanceCreateUpdateForm {
  amount: number
  date: string
  category: number
}

/**
 * Represents a monthly balance with category fields flattened at the top level.
 * Used in API responses where category data is not nested.
 */
export interface MonthlyBalanceWithFlatCategory {
  id: number
  amount: number
  category_id: number // category's fields are not in a nested object, but on the same leve as other properties
  category_text: string // category's fields are not in a nested object, but on the same leve as other properties
  date: string
}

/**
 * Represents a monthly balance with category as a nested object.
 * Used in UI components that expect structured category data.
 */
export interface MonthlyBalanceWithNestedCategory {
  id: number
  amount: number
  date: string
  category: MonthlyBalanceCategory // category is a nested object
}

/**
 * Form inputs for editing a monthly balance.
 * Simplified form for updating just the amount.
 */
export interface MonthlyBalanceForm {
  // Since monthlybanace can not have duplicates for category/date/user,
  // in the edit form we only allow users to update the amount
  // (otherwise we'd need some dynamic logic _in the UI_ to prevent users from creating duplicated MonthlyBalance
  //  with the same date and category)
  amount: number
}

/**
 * Represents a category for monthly balances.
 * Maps to the MonthlyBalanceCategory model in the backend.
 * Can be used for tracking balances in foreign currencies.
 */
export interface MonthlyBalanceCategory {
  id: number
  text: string
  is_foreign_currency: boolean
}

/**
 * Represents an income record.
 * Maps to the Income model in the backend.
 * Tracks income amounts with categories and dates.
 */
export interface Income {
  id: number
  amount: number
  category?: number // This will make the linter shut up about errors.category not existing
  category_id: number
  category_text: string
  note: string
  date: string
}

export interface AggregateIncome {
  year: number
  month: number
  total_amount: number
}

/**
 * Represents an income without an ID, typically used for creating new income records.
 * Contains the minimal fields needed for income creation.
 * Note the typo in the interface name (should be "WithoutID").
 */
export interface IncomeWihtoutID {
  amount: number
  // NOTE: this 'category' is an int, not an IncomeCategory object
  category: number
  note: string
  date: string
}

/**
 * Form inputs for creating or updating an income.
 * Contains the fields needed for the income creation form.
 */
export interface IncomeFormInputs {
  amount: number
  category?: number // This will make the linter shut up about errors.category not existing
  category_id: number
  note: string
  date: string
}

/**
 * Form inputs for updating an existing income.
 * Used in the income edit form.
 */
export interface PatchIncomeFormInputs {
  amount: number
  category: number
  note: string
  date: string
}

/**
 * Form inputs for creating or updating an income category.
 * Used in the income category creation and edit forms.
 */
export interface IncomeCategoryFormInputs {
  text: string
}

/**
 * Represents a category for income.
 * Maps to the IncomeCategory model in the backend.
 */
export interface IncomeCategory {
  id: number
  text: string
}

export interface AggregateIncomePagedData {
// NOTE: this API is not paginated, no need for next or previous
  count: number
  results: AggregateIncome[]
}

export interface IncomesResponse {
  response: HTTPResponse
  data: IncomePagedData
}

export interface AggregateIncomesResponse {
  response: HTTPResponse
  data: AggregateIncomePagedData
}

export interface MonthlyBalanceCategoryResponse {
  response: HTTPResponse
  data: MonthlyBalanceCategory
}

export interface IncomeCategoryResponse {
  response: HTTPResponse
  data: IncomeCategory
}

export interface IncomeCategoryPagedData {
  count: number
  // next: ??,
  // previous: ??,
  results: IncomeCategory[]
}

export interface IncomeCategoriesResponse {
  response: HTTPResponse
  data: IncomeCategoryPagedData
}

/**
 * Represents a financial account.
 * Maps to the Account model in the backend.
 * Can be used for tracking assets in foreign currencies.
 */
export interface Account {
  id: number
  text: string
  credit_entity: string
  is_foreign_currency: boolean
}

/**
 * Form inputs for creating or updating an account.
 * Used in the account creation and edit forms.
 */
export interface AccountFormInputs {
  text: string
  credit_entity: string
  is_foreign_currency: boolean
}

/**
 * Represents a financial asset.
 * Maps to the Asset model in the backend.
 * Tracks assets with their values in both local and foreign currencies.
 */
export interface Asset {
  id: number
  account: number
  amount: number
  text: string
  // Length 3 as per ISO 4217
  foreign_currency: string
  current_foreign_value: number
  current_local_value: number
  bought_price_foreign: number
  bought_price_local: number
  net_loss_foreign: number
  net_loss_local: number
  record_date: string
}

/**
 * Form inputs for creating or updating an asset.
 * Contains all fields needed for asset management.
 */
export interface AssetFormInput {
  account: number
  amount: number
  text: string
  // Length 3 as per ISO 4217
  foreign_currency: string
  current_foreign_value: number
  current_local_value: number
  bought_price_foreign: number
  bought_price_local: number
  net_loss_foreign: number
  net_loss_local: number
  record_date: string
}

/**
 * Form inputs for filtering assets in the asset list view.
 */
export interface AssetFormFilter {
  name?: string
  date?: string
  account?: number | string
}

export interface AssetsDate {
  response: HTTPResponse
  data: string[]
}

// This is what we expect the EndUser to return from their json endpoint
// when we automatically import assets data for a given date
export interface EndUserAsset {
  ticker: string
  amount: number
  currency: string
  sum_current: {
    foreign: number
    local: number
  }
  sum_bought: {
    foreign: number
    local: number
  }
  sum_net: {
    foreign: number
    local: number
  }
  rate: number
  date: string
}

// This is what we expect the EndUser to return from their json endpoint
// when we automatically import dividends data for a given date
export interface EndUserDividends {
  date: string
  currency: string
  ticker: string
  amount: number
  rate: number
}

/**
 * Represents a dividend or income from an asset.
 * Maps to the Dividend model in the backend.
 * Tracks dividend amounts in both local and foreign currencies.
 */
export interface Dividend {
  id: number
  account: number
  asset_text: string
  // Length 3 as per ISO 4217
  local_amount: number
  foreign_amount: number
  currency: string
  rate: number
  record_date: string
}

export interface SimplifiedDividend {
  amount: number
  currency: string
  date: string
  rate: number
  ticker: string
}

/**
 * Form inputs for creating or updating a dividend.
 * Contains all fields needed for dividend management.
 */
export interface DividendFormInput {
  account: number
  asset_text: string
  local_amount: number
  foreign_amount: number
  // Length 3 as per ISO 4217
  currency: string
  rate: number
  record_date: string
}

export interface YearlyAggregateDividend {
  year: number
  local_total: number
  local_per_day: number
}

export interface SymbolAggregateDividend {
  asset_text: string
  total_local_amount: number
  total_foreign_amount: number
}

/**
 * Represents a spending limit for a category.
 * Maps to the SpendingLimit model in the backend.
 * Used to set budget constraints for specific categories over a period of days.
 */
export interface SpendingLimit {
  id: number
  amount: number
  currency: string
  days: number
  category: number
  category_text: string
  text: string
}

/**
 * Form inputs for creating or updating a spending limit.
 * Used in the spending limit creation and edit forms.
 */
export interface SpendingLimitFormInputs {
  amount: number
  currency: string
  days: number
  category: number
  text: string
}

export interface SpendingLimitResponse {
  response: HTTPResponse
  data: SpendingLimit
}

/**
 * Represents a financial obligation like a loan or recurring payment.
 * Maps to the Obligation model in the backend.
 * Can be either a fixed or variable obligation with payment schedule.
 */
export interface Obligation {
  id: number
  name: string
  total_amount?: string // Decimal 6 digits, 2 decimals
  obligation_type: string
  monthly_payment: number // Decimal 6 digits, 2 decimals
  payment_day: number
  start_date: string
  end_date: string
  changes?: ObligationPaymentChange[]
}

/**
 * Represents a change in payment amount for an obligation.
 * Maps to the ObligationPaymentChange model in the backend.
 * Used to track changes in payment amounts over time.
 */
export interface ObligationPaymentChange {
  id: number
  obligation: number
  new_monthly_payment: number
  change_date: string
}

export interface ObligationPaymentChangeResponse {
  response: HTTPResponse
  data: ObligationPaymentChange
}

/**
 * Form inputs for creating or updating a payment change for an obligation.
 * Used to record changes in payment amounts.
 */
export interface PaymentChangeFormInput {
  id?: number
  obligation: number
  new_monthly_payment: number
  change_date: string
}

/**
 * Form inputs for creating or updating an obligation.
 * Contains all fields needed for obligation management.
 */
export interface ObligationFormInputs {
  name: string
  total_amount?: number
  obligation_type: string
  monthly_payment: number
  payment_day: number
  start_date: string
  end_date: string
  changes?: ObligationPaymentChange[]
}

export type DeleteObligationFunction = (id: number) => Promise<DeletedObligationResponse>
export type DeletePaymentChangeFunction = (id: number) => Promise<HTTPResponseWithMessage>

export interface ObligationsTableProps {
  obligations: Obligation[]
  id: string
  deleteOblFunc: DeleteObligationFunction
  deletePaymentFunc: DeletePaymentChangeFunction
  updateParentData: cleanCacheFunction
  updateParentPaymentChangeData: cleanCacheFunction
}

export interface ChartData {
  monthlyBalances: MonthlyBalanceWithFlatCategoryPagedData[]
  goals: Goal[]
  specialGoals: Goal[]
  obligations?: Obligation[]
}

export interface simpleBarChart {
  monthlyBalances: MonthlyBalanceWithFlatCategoryPagedData[]
}

export interface expensesChart {
  expenses: AggregateExpensesByCategoryPagedData
}

export interface PieGraph {
  labels: string[]
  datasets: [
    {
      label: string
      data: number[]
      hoverOffset: number
    }
  ]
}

// Function interfaces
export type DeleteFunction = (id: number) => Promise<HTTPResponse>

export type cleanCacheFunction = (id: number) => void

export type getAssetsFunction = (account: Account, date: string, url: string) => Promise<EndUserAssetResponse>

export type getDividendDataFunc = (account: Account, date: string, url: string) => Promise<EndUserDividendsResponse>

// API response interfaces
/**
 * Standard HTTP response wrapper used throughout the application.
 * Contains basic information about the HTTP response status.
 */
export interface HTTPResponse {
  ok: boolean
  status: number
  statusText: string
}

// NOTE: This interface is duplicated in order to make handleDeleteObligation and handleDeletePaymentChange
// functions in obligations.tsx work without extra logic
export interface HTTPResponseWithMessage {
  response: HTTPResponse
}

export interface ExpenseResponse {
  response: HTTPResponse
  data: Expense
}

export interface ExpensesPagedData {
  count: number
  // next: ??,
  // previous: ??,
  results: Expense[]
}

export interface IncomesPagedData {
  count: number
  // next: ??,
  // previous: ??,
  results: Income[]
}

export interface ExpensePatchResponse {
  response: HTTPResponse
  data: ExpenseWihtoutID
}

export interface ExpensesResponse {
  response: HTTPResponse
  data: ExpensesPagedData
}

export interface AggregateExpenseByMonth {
  year: number
  month: number
  total_amount: number
}

export interface AggregateExpensesByMonthPagedData {
// NOTE: this API is not paginated, no need for next or previous
  count: number
  results: AggregateExpenseByMonth[]
}

export interface AggregateExpensesByMonthResponse {
  response: HTTPResponse
  data: AggregateExpensesByMonthPagedData
}

export interface AggregateExpenseByCategory {
  category_text: string
  total_amount: number
}

export interface AggregateExpensesByCategoryPagedData {
// NOTE: this API is not paginated, no need for next or previous
  count: number
  results: AggregateExpenseByCategory[]
}

export interface AggregateExpensesByCategoryResponse {
  response: HTTPResponse
  data: AggregateExpensesByCategoryPagedData
}

export interface CategoryResponse {
  response: HTTPResponse
  data: Category
}

export interface SavingCategoryResponse {
  response: HTTPResponse
  data: SavingCategory
}

export interface CategoriesPagedData {
  count: number
  // next: ??,
  // previous: ??,
  results: Category[]
}

export interface CategoriesResponse {
  response: HTTPResponse
  data: CategoriesPagedData
}

export interface SavingCategoriesPagedData {
  count: number
  // next: ??,
  // previous: ??,
  results: SavingCategory[]
}

export interface SavingCategoriesResponse {
  response: HTTPResponse
  data: SavingCategoriesPagedData
}

export interface BudgetsPagedData {
  count: number
  // This view is not paginated, no need for next or previous
  results: BudgetWithCategory[]
}

export interface BudgetResponse {
  response: HTTPResponse
  data: Budget
}

export interface BudgetWithIdResponse {
  response: HTTPResponse
  data: BudgetWithId
}

export interface BudgetPatchResponse {
  response: HTTPResponse
  data: BudgetPatchFormResponse
}

export interface BudgetsResponse {
  response: HTTPResponse
  data: BudgetsPagedData
}

export interface BudgetYearlyAggregatePagedData {
  category: number
  sum_amount: number
}

export interface BudgetYearlyAggregateResponse {
  response: HTTPResponse
  data: BudgetYearlyAggregatePagedData
}

export interface overBudgetedCategory {
  categoryId: number
  name: string
  overbudgetedAmount: number
}

export interface GoalResponse {
  response: HTTPResponse
  data: Goal
}

export interface ObligationResponse {
  response: HTTPResponse
  data: Obligation
}

export interface GoalPagedData {
  count: number
  // next: ??,
  // previous: ??,
  results: Goal[]
}

export interface ObligationPagedData {
  count: number
  // next: ??,
  // previous: ??,
  results: Obligation[]
}

export interface GoalsResponse {
  response: HTTPResponse
  data: GoalPagedData
}

export interface ObligationsResponse {
  response: HTTPResponse
  data: ObligationPagedData
}

export interface MonthlyBalanceWithFlatCategoryPagedData {
  count: number
  // next: ??,
  // previous: ??,
  results: MonthlyBalanceWithFlatCategory[]
}

export interface MonthlyBalancesWithFlatCategoryPagedResponse {
  response: HTTPResponse
  data: MonthlyBalanceWithFlatCategoryPagedData
}

export interface MonthlyBalanceResponse {
  response: HTTPResponse
  data: MonthlyBalanceWithNestedCategory
}

export interface MonthlyBalancesPagedData {
  count: number
  // next: ??,
  // previous: ??,
  results: MonthlyBalanceWithNestedCategory[]
}

export interface MonthlyBalancesPagedResponse {
  response: HTTPResponse
  data: MonthlyBalancesPagedData
}

export interface MonthlyBalanceCategoriesPagedData {
  count: number
  // next: ??,
  // previous: ??,
  results: MonthlyBalanceCategory[]
}

export interface MonthlyBalanceCategoriesResponse {
  response: HTTPResponse
  data: MonthlyBalanceCategoriesPagedData
}

export interface MonthlyBalanceCategoryFormInputs {
  id?: number
  text: string
  is_foreign_currency: boolean
}

// Most logged expenses per user, used to create expense with a click
export interface FastExpenseCandidate {
  // The backend is returning a string, as the amount is stored as Decmial
  amount: string
  categoryId: number
  categoryName: string
  magnitude: number // how many times this expenses has been logged in the last <period>
  note: string
}

export interface FastExpenseCandidatePagedData {
  count: number
  // next: string|null,
  // previous: string|null,
  results: FastExpenseCandidate[]
}

export interface FastExpenseCandidateResponse {
  response: HTTPResponse
  data: FastExpenseCandidatePagedData
}

export interface AccountResponse {
  response: HTTPResponse
  data: Account
}

export interface AccountPagedData {
  count: number
  // next: string|null,
  // previous: string|null,
  results: Account[]
}

export interface AccountsResponse {
  response: HTTPResponse
  data: AccountPagedData
}

export interface AssetResponse {
  response: HTTPResponse
  data: Asset
}

export interface DividendResponse {
  response: HTTPResponse
  data: Dividend
}

export interface AssetPagedData {
  count: number
  // next: string|null,
  // previous: string|null,
  results: Asset[]
}

export interface AssetsResponse {
  response: HTTPResponse
  data: AssetPagedData
}

export interface DividendPagedData {
  count: number
  // next: string|null,
  // previous: string|null,
  results: Dividend[]
}

export interface DividendsResponse {
  response: HTTPResponse
  data: DividendPagedData
}

// This differs from the other interfaces, as this is the end-user endpoint
// returning plain json, instead of a budgeteer API standard return signature,
// hence the "count" propery is missing

export interface EndUserDividendsPagedData {
  account: Account
  dividends: SimplifiedDividend[]
}

export interface EndUserDividendsResponse {
  response: HTTPResponse
  data: EndUserDividendsPagedData
}

export interface DeletedDividendsPagedData {
  records_deleted: number
}

export interface DeletedDividendsResponse {
  response: HTTPResponse
  data: DeletedDividendsPagedData
}

export interface DeletedObgliationPagedData {
  records_deleted: number
}

export interface DeletedAssetsResponse {
  response: HTTPResponse
  data: DeletedDividendsPagedData
}

export interface DeletedObligationResponse {
  response: HTTPResponse
  data: DeletedObgliationPagedData
}

// This differs from the other interfaces, as this is the end-user endpoint
// returning plain json, instead of a budgeteer API standard return signature,
// hence the "count" propery is missing
export interface EndUserAssetPagedData {
  account: Account
  assets: EndUserAsset[]
}

export interface EndUserAssetResponse {
  response: HTTPResponse
  data: EndUserAssetPagedData
}

export interface YearlyAggregatePagedData {
  count: number
  // next: string|null,
  // previous: string|null,
  results: YearlyAggregateDividend[]
}

export interface SymbolAggregatePagedData {
  count: number
  // next: string|null,
  // previous: string|null,
  results: SymbolAggregateDividend[]
}

export interface YearlyAggregateDividendResponse {
  response: HTTPResponse
  data: YearlyAggregatePagedData
}

export interface SymbolAggregateDividendResponse {
  response: HTTPResponse
  data: SymbolAggregatePagedData
}

export interface SpendingLimitPagedData {
  count: number
  // next: string|null,
  // previous: string|null,
  results: SpendingLimit[]
}

export interface SpendingLimitsResponse {
  response: HTTPResponse
  data: SpendingLimitPagedData
}

// UI Interfaces
export type ReactFragment = React.ReactFragment

export interface ExpenseAggregate {
  label: string
  // The backend is returning a string, as the amount is stored as Decmial
  amount: string
}

export interface CategoriesTableProps {
  categories: Array<SavingCategory | IncomeCategory | MonthlyBalanceCategory | Category>
  url: string
}

export interface ExpensesTableProps {
  data: Expense[]
  deleteFunc: DeleteFunction
  updateParentData: cleanCacheFunction
}

export interface IncomesTableProps {
  data: Income[]
  deleteFunc: DeleteFunction
  updateParentData: cleanCacheFunction
}

export interface LimitExpensesTableProps {
  data: SpendingLimit[]
  deleteFunc: DeleteFunction
  updateParentData: cleanCacheFunction
  url: string
}

export interface AccountTableProps {
  data: Account[]
}

export interface BalanceTableProps {
  data: Expense[]
}

export interface MonthlyBalanceTableProps {
  data: MonthlyBalanceWithNestedCategory[]
  expensesData: Record<number, number>
}

export interface GoalTableProps {
  data: Goal[]
  deleteFunc: DeleteFunction
  updateParentData: cleanCacheFunction
}

export interface AssetsTableProps {
  data: Asset[]
  isAggregate: boolean
  forceYenDecimals?: boolean
}

export interface DividendTableProps {
  data: Dividend[]
  aggregate: YearlyAggregateDividend[]
  symbolAggregate: SymbolAggregateDividend[]
}

export interface PulldownProps {
  data: Category[]
  // TODO: shouldn't onChange be React.ChangeEvent<HTMLSelectElement> instead?
  onChange: (event: React.ChangeEvent<HTMLSelectElement>) => void
}

export interface ReducedTableProps {
  data: ExpenseAggregate[]
}

export type SetCriticalErrorFunction = (value: boolean) => void

export interface YearMonth { year: number | null, month: number | null }

export interface ExternalRedirectProps {
  url: string
  label: string
}

export interface ExternalRedirectPageProps {
  url: string
  label: string
  title: string
  desc: string
}

export interface internalChartDataSet {
  labels: string[]
  datasets: Array<{
    label: string
    data: string[]
  }>
}

export interface FastCreateExpenseTableProps {
  data: FastExpenseCandidate[]
  categories: Category[]
  setValue: (name: string, value: any) => void
  getValues: () => ExpenseFormInputs
  onSubmit: (formValues: ExpenseFormInputs) => void
}

export type SetStateFunction<T> = React.Dispatch<React.SetStateAction<T>>

export interface SubmissionDependencies {
  setShowDismissable: SetStateFunction<boolean>
  setIsSending: SetStateFunction<boolean>
  getIsSending: () => boolean
  createExpense: (formValues: ExpenseFormInputs, signal?: AbortSignal) => Promise<ExpenseResponse>
  setGenericFormError: SetStateFunction<string>
  setIsUpdateSuccess: SetStateFunction<boolean>
  setError: (name: keyof ExpenseFormInputs, error: { message: string }) => void
  reset: (values: Partial<ExpenseFormInputs>) => void
  defaultValues: Partial<ExpenseFormInputs>
}

export interface AlertDependencies {
  setShowDismissable: SetStateFunction<boolean>
  getIsShowDismissable: () => boolean
  successMessage: string
  failureMessage: string
  getIsUpdateSuccess: () => boolean
}

export interface ExchangeRateResponse {
  success: boolean
  data: {
    exchange_rate: number
    last_updated: string
  }
}

export interface ThemeSettings {
  dark_theme: boolean
  last_updated: string
}

export interface ThemeResponse {
  success: boolean
  data: ThemeSettings
}

/**
 * Form inputs for creating or updating a goal.
 * Used for the goal creation and editing forms.
 */
export interface GoalFormInputs {
  text: string
  amount: number
  note?: string
  is_archived: boolean
}

export interface UpdateSpendingLimitPayload {
  amount?: number
  currency?: string
  days?: number
  category?: number
  category_text?: string
  text?: string
}

/**
 * Represents a budget with resolved Category/SavingCategory objects instead of just IDs.
 * Used for displaying budget information with category details.
 */
export interface BudgetWithId {
  id: number
  category?: Category | null
  savingcategory?: SavingCategory | null
  year: number
  month?: number | null
  amount: number
  allocated_amount?: number | null
}

export interface BudgetPatchFormResponse {
  amount: number
  allocated_amount: number | null
}

/**
 * Extends BudgetWithId by adding an optional ID field.
 * Used when working with both new and existing budget records.
 */
export interface BudgetWithCategory {
  id?: number
  category?: Category | null
  savingcategory?: SavingCategory | null
  year: number
  month?: number | null
  amount: number
  allocated_amount?: number | null
}

export interface IncomePatchResponse {
  response: HTTPResponse
  data: IncomeWihtoutID
}

export interface IncomeResponse {
  response: HTTPResponse
  data: Income
}

export interface IncomePagedData {
  count: number
  // next: ??,
  // previous: ??,
  results: Income[]
}
