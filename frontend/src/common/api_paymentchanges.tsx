import type * as Interfaces from "./interfaces"
import { getAuthHeaders, credentialsOption } from "./api_helpers"

export async function createPaymentChange (data: any, signal?: AbortSignal): Promise<Interfaces.ObligationPaymentChangeResponse> {
  return await fetch("/api/v1/obligations-changes/", {
    method: "POST",
    headers: getAuthHeaders(),
    ...credentialsOption,
    body: JSON.stringify(data),
    signal
  }).then(async (response): Promise<Interfaces.ObligationPaymentChangeResponse> => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err): Promise<Interfaces.ObligationPaymentChangeResponse> => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const empty: Interfaces.ObligationPaymentChange = {
        id: 0,
        obligation: 0,
        new_monthly_payment: 0,
        change_date: ""
      }
      return await Promise.resolve({ response, data: empty })
    })
  })
}

export async function deletePaymentChange (id: number, signal?: AbortSignal): Promise<Interfaces.HTTPResponseWithMessage> {
  return await fetch(`/api/v1/obligations-changes/${id}/`, {
    method: "DELETE",
    headers: getAuthHeaders(),
    ...credentialsOption,
    signal
  }).then(async (response): Promise<Interfaces.HTTPResponseWithMessage> => {
    // TODO: is this callback really needed? Let's see if we can remove it
    return { response }
  })
}

export async function getPaymentChange (id: number, signal?: AbortSignal): Promise<Interfaces.ObligationPaymentChangeResponse> {
  return await fetch(`/api/v1/obligations-changes/${id}/`, { signal }) // <=== make this singular
    .then(async (response): Promise<Interfaces.ObligationPaymentChangeResponse> => {
      return await response.json().then(parsedJson => {
        return { response, data: parsedJson }
      }).catch(async (err: Error): Promise<Interfaces.ObligationPaymentChangeResponse> => {
        // This catch handles the case where there is no json body in the response
        console.log(err)
        const empty: Interfaces.ObligationPaymentChange = {
          id: 0,
          obligation: 0,
          new_monthly_payment: 0,
          change_date: ""
        }
        return await Promise.resolve({ response, data: empty })
      })
    })
}

export async function putPaymentChange (id: number, formInfo: Interfaces.PaymentChangeFormInput, signal?: AbortSignal): Promise<Interfaces.ObligationPaymentChangeResponse> {
  const url = `/api/v1/obligations-changes/${id}/`
  // TODO: validate parameters!
  return await fetch(url, {
    method: "PUT",
    headers: getAuthHeaders(),
    ...credentialsOption,
    body: JSON.stringify(formInfo),
    signal
  }).then(async (response): Promise<Interfaces.ObligationPaymentChangeResponse> => {
    return await response.json().then(async (parsedJson) => {
      return { response, data: parsedJson }
    }).catch(async (err) => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const empty: Interfaces.ObligationPaymentChange = {
        id: 0,
        obligation: 0,
        new_monthly_payment: 0,
        change_date: ""
      }
      return await Promise.resolve({ response, data: empty })
    })
  })
}
