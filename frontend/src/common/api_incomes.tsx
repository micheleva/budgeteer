import type * as Interfaces from "./interfaces"
import { getAuthHeaders, credentialsOption } from "./api_helpers"

export async function getIncomesByDates (start: string, end: string, signal?: AbortSignal): Promise<Interfaces.IncomesResponse> {
  // TODO: validate parameters!
  const url = `/api/v1/incomes?format=json&huge_page=yes&start=${start}&end=${end}`
  return await fetch(encodeURI(url), { signal })
    .then(async (response): Promise<Interfaces.IncomesResponse> => {
      return await response.json().then(parsedJson => {
        return { response, data: parsedJson }
      }).catch(async (err): Promise<Interfaces.IncomesResponse> => {
        // This catch handles the case where there is no json body in the response
        console.log(err)
        const emptyRes = [] as Interfaces.Income[]
        const empty = { count: 0, results: emptyRes }
        return await Promise.resolve({ response, data: empty })
      })
    })
}

export async function createIncome (data: any, signal?: AbortSignal): Promise<Interfaces.IncomeResponse> {
  return await fetch("/api/v1/incomes", {
    method: "POST",
    headers: getAuthHeaders(),
    ...credentialsOption,
    body: JSON.stringify(data),
    signal
  }).then(async (response) => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err): Promise<Interfaces.IncomeResponse> => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const empty: Interfaces.Income = {
        id: 0,
        amount: 0,
        category_id: 0,
        category_text: "",
        note: "",
        date: ""
      }
      return await Promise.resolve({ response, data: empty })
    })
  })
}

export async function getIncomesAggregate (signal?: AbortSignal): Promise<Interfaces.AggregateIncomesResponse> {
  const url = "/api/v1/incomes/aggregate"
  return await fetch(encodeURI(url), { signal })
    .then(async (response) => {
      return await response.json().then(parsedJson => {
        return { response, data: parsedJson }
      }).catch(async (err): Promise<Interfaces.AggregateIncomesResponse> => {
        // This catch handles the case where there is no json body in the response
        console.log(err)
        const emptyRes = [] as Interfaces.AggregateIncome[]
        const empty = { count: 0, results: emptyRes }
        return await Promise.resolve({ response, data: empty })
      })
    })
}

export async function getIncome (id: number, signal?: AbortSignal): Promise<Interfaces.IncomeResponse> {
  return await fetch(`/api/v1/incomes/${id}/`, { signal })
    .then(async (response) => {
      return await response.json().then(parsedJson => {
        return { response, data: parsedJson }
      }).catch(async (err): Promise<Interfaces.IncomeResponse> => {
        // This catch handles the case where there is no json body in the response
        console.log(err)
        const empty: Interfaces.Income = {
          id: 0,
          amount: 0,
          category_id: 0,
          category_text: "",
          note: "",
          date: ""
        }
        return await Promise.resolve({ response, data: empty })
      })
    })
}

// This API does not return JSON, hence it's a bit different from the other funcs
export async function deleteIncome (id: number, signal?: AbortSignal): Promise<Interfaces.HTTPResponse> {
  return await fetch(`/api/v1/incomes/${id}/`, {
    method: "DELETE",
    headers: getAuthHeaders(),
    ...credentialsOption,
    signal
  })
}

export async function patchIncome (id: number, data: Interfaces.PatchExpenseFormInputs, signal?: AbortSignal): Promise<Interfaces.IncomePatchResponse> {
  return await fetch(`/api/v1/incomes/${id}/`, {
    method: "PATCH",
    headers: getAuthHeaders(),
    ...credentialsOption,
    body: JSON.stringify(data),
    signal
  }).then(async (response): Promise<Interfaces.IncomePatchResponse> => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err): Promise<Interfaces.IncomePatchResponse> => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const empty: Interfaces.IncomeWihtoutID = {
        amount: 0,
        category: 0,
        note: "",
        date: ""
      }
      return await Promise.resolve({ response, data: empty })
    })
  })
}

export async function getIncomesByCategoryId (categoryId: number, start: string, end: string, note?: string, signal?: AbortSignal): Promise<Interfaces.IncomesResponse> {
  // TODO: validate parameters!
  let url = `/api/v1/incomes?category_id=${categoryId}&format=json&huge_page=yes&start=${start}&end=${end}`
  if (note !== null && note !== undefined) {
    url += `&note=${note}`
  }
  return await fetch(encodeURI(url), { signal })
    .then(async (response) => {
      return await response.json().then(parsedJson => {
        return { response, data: parsedJson }
      }).catch(async (err): Promise<Interfaces.IncomesResponse> => {
        // This catch handles the case where there is no json body in the response
        console.log(err)
        const emptyRes = [] as Interfaces.Income[]
        const empty = { count: 0, results: emptyRes }
        return await Promise.resolve({ response, data: empty })
      })
    })
}
