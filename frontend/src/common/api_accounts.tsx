import type * as Interfaces from "./interfaces"
import { getAuthHeaders, credentialsOption } from "./api_helpers"

export async function getAccounts (signal?: AbortSignal): Promise<Interfaces.AccountsResponse> {
  return await fetch("/api/v1/accounts/create_list", { signal }).then(async (response) => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err): Promise<Interfaces.AccountsResponse> => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const emptyRes = [] as Interfaces.Account[]
      const empty = { count: 0, results: emptyRes }
      return await Promise.resolve({ response, data: empty })
    })
  })
}

export async function createAccount (data: any, signal?: AbortSignal): Promise<Interfaces.AccountResponse> {
  return await fetch("/api/v1/accounts/create_list", {
    method: "POST",
    headers: getAuthHeaders(),
    ...credentialsOption,
    body: JSON.stringify(data),
    signal
  }).then(async (response) => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err) => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const emptyRes: Interfaces.Account = {
        id: 0,
        text: "",
        credit_entity: "",
        is_foreign_currency: false
      }
      return await Promise.resolve({ response, data: emptyRes })
    })
  })
}
