import type * as Interfaces from "./interfaces"

export async function getExchangeRate (signal?: AbortSignal): Promise<Interfaces.ExchangeRateResponse> {
  return await fetch("/api/alpha/exchange-rate", { signal })
    .then(async (response) => {
      return await response.json().then(parsedJson => {
        return parsedJson
      }).catch(async (err) => {
        console.log(err)
        return await Promise.resolve({
          success: false,
          data: {
            exchange_rate: 1,
            last_updated: new Date().toISOString()
          }
        })
      })
    })
}

export async function getThemeSettings (signal?: AbortSignal): Promise<Interfaces.ThemeResponse> {
  return await fetch("/api/alpha/theme", { signal })
    .then(async (response) => {
      return await response.json().then(parsedJson => {
        return parsedJson
      }).catch(async (err) => {
        console.log(err)
        return await Promise.resolve({
          success: false,
          data: {
            dark_theme: false,
            last_updated: new Date().toISOString()
          }
        })
      })
    })
}
