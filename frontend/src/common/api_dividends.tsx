import type * as Interfaces from "./interfaces"
import { getAuthHeaders, credentialsOption } from "./api_helpers"

export async function createDividend (data: any, signal?: AbortSignal): Promise<Interfaces.DividendResponse> {
  return await fetch("/api/v1/dividends/create", {
    method: "POST",
    headers: getAuthHeaders(),
    ...credentialsOption,
    body: JSON.stringify(data),
    signal
  }).then(async (response): Promise<Interfaces.DividendResponse> => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err) => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const emptyRes: Interfaces.Dividend = {
        id: 0,
        account: 0,
        asset_text: "",
        local_amount: 0,
        foreign_amount: 0,
        currency: "",
        rate: 0,
        record_date: ""
      }
      return await Promise.resolve({ response, data: emptyRes })
    })
  })
}

export async function getDividends (start?: string | null, end?: string | null, signal?: AbortSignal): Promise<Interfaces.DividendsResponse> {
  // TODO: validate parameters
  // FIXME: atm if only end is passed, the URL is invalid and it will throw an error(?)
  let url = "/api/v1/dividends"
  if (start !== null && start !== undefined) {
    url += `&start=${start}`
  }
  if (end !== null && end !== undefined) {
    url += `&end=${end}`
  }
  return await fetch(url, { signal }).then(async (response) => {
    return await response.json().then(async (parsedJson): Promise<Interfaces.DividendsResponse> => {
      return { response, data: parsedJson }
    }).catch(async (err) => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const emptyRes = [] as Interfaces.Dividend[]
      const empty = { count: 0, results: emptyRes }
      return await Promise.resolve({ response, data: empty })
    })
  })
}

export async function deleteAllDividends (signal?: AbortSignal): Promise<Interfaces.DeletedDividendsResponse> {
  return await fetch("/api/v1/dividends/delete", {
    method: "DELETE",
    headers: getAuthHeaders(),
    ...credentialsOption,
    signal
  }).then(async (response): Promise<Interfaces.DeletedDividendsResponse> => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err) => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const emptyRes: Interfaces.DeletedDividendsPagedData = { records_deleted: 0 }
      return await Promise.resolve({ response, data: emptyRes })
    })
  })
}

export async function getDividendsYearlyAggregate (signal?: AbortSignal): Promise<Interfaces.YearlyAggregateDividendResponse> {
  return await fetch("/api/v1/dividends/aggregate", { signal })
    .then(async (response): Promise<Interfaces.YearlyAggregateDividendResponse> => {
      return await response.json().then(parsedJson => {
        return { response, data: parsedJson }
      }).catch(async (err) => {
        // This catch handles the case where there is no json body in the response
        console.log(err)
        const emptyRes = [] as Interfaces.YearlyAggregateDividend[]
        const empty = { count: 0, results: emptyRes }
        return await Promise.resolve({ response, data: empty })
      })
    })
}

export async function getDividendsAggregateBySymbol (signal?: AbortSignal): Promise<Interfaces.SymbolAggregateDividendResponse> {
  return await fetch("/api/v1/dividends/aggregate-by-symbol", { signal })
    .then(async (response): Promise<Interfaces.SymbolAggregateDividendResponse> => {
      return await response.json().then(parsedJson => {
        return { response, data: parsedJson }
      }).catch(async (err) => {
        // This catch handles the case where there is no json body in the response
        console.log(err)
        const emptyRes = [] as Interfaces.SymbolAggregateDividend[]
        const empty = { count: 0, results: emptyRes }
        return await Promise.resolve({ response, data: empty })
      })
    })
}

export async function getDividendRawDataByAccName (account: Interfaces.Account, date: string, url: string, signal?: AbortSignal): Promise<Interfaces.EndUserDividendsResponse> {
  // TODO if url is empty return error
  // console.log(account)
  const finalurl = `${url}/${account.text}/${date}.json`
  return await fetch(finalurl, { signal }).then(async (response) => {
    return await response.json().then(async (parsedJson): Promise<Interfaces.EndUserDividendsResponse> => {
      const formattedData: Interfaces.EndUserDividendsPagedData = { account, dividends: parsedJson }
      return { response, data: formattedData }
    }).catch(async (err): Promise<Interfaces.EndUserDividendsResponse> => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const emptyDividendss = [] as Interfaces.SimplifiedDividend[]
      const emptyRes: Interfaces.EndUserDividendsPagedData = { account, dividends: emptyDividendss }
      return await Promise.resolve({ response, data: emptyRes })
    })
  })
}
