import type * as Interfaces from "./interfaces"
import { getAuthHeaders, credentialsOption } from "./api_helpers"

export async function createObligation(data: any, signal?: AbortSignal): Promise<Interfaces.ObligationResponse> {
  try {
    const response = await fetch("/api/v1/obligations/", {
      method: "POST",
      headers: getAuthHeaders(),
      ...credentialsOption,
      body: JSON.stringify(data),
      signal
    })

    if (!response.ok) {
      const empty: Interfaces.Obligation = {
        id: 0,
        name: "",
        obligation_type: "",
        monthly_payment: 0,
        payment_day: 0,
        start_date: "",
        end_date: ""
      }
      return await Promise.resolve({ response, data: empty })
    }

    try {
      const parsedJson = await response.json()
      return { response, data: parsedJson }
    } catch (error) {
      // This catch handles the case where there is no json body in the response
      const empty: Interfaces.Obligation = {
        id: 0,
        name: "",
        obligation_type: "",
        monthly_payment: 0,
        payment_day: 0,
        start_date: "",
        end_date: ""
      }
      return { response, data: empty }
    }
  } catch (error) {
    // This catch handles network errors and other exceptions
    console.error("Fetch error:", error)
    const empty: Interfaces.Obligation = {
      id: 0,
      name: "",
      obligation_type: "",
      monthly_payment: 0,
      payment_day: 0,
      start_date: "",
      end_date: ""
    }
    const errorMessage = error instanceof Error ? error.message : "Unknown error"
    const response = { status: 500, statusText: errorMessage, ok: false }
    return await Promise.resolve({ response, data: empty })
  }
}

export async function deleteObligation(id: number, signal?: AbortSignal): Promise<Interfaces.DeletedObligationResponse> {
  return await fetch(`/api/v1/obligations/${id}/`, {
    method: "DELETE",
    headers: getAuthHeaders(),
    ...credentialsOption,
    signal
  }).then(async (response): Promise<Interfaces.DeletedObligationResponse> => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async err => {
      // This catch handles the case where there is no json body in the response
      console.error(err)
      const emptyRes: Interfaces.DeletedObgliationPagedData = { records_deleted: 0 }
      return await Promise.resolve({ response, data: emptyRes })
    })
  })
}

export async function getObligations(signal?: AbortSignal): Promise<Interfaces.ObligationsResponse> {
  return await fetch("/api/v1/obligations/", { signal })
    .then(async (response): Promise<Interfaces.ObligationsResponse> => {
      return await response.json().then(parsedJson => {
        return { response, data: parsedJson }
      }).catch(async (err): Promise<Interfaces.ObligationsResponse> => {
        // This catch handles the case where there is no json body in the response
        console.error(err)
        const emptyRes = [] as Interfaces.Obligation[]
        const empty = { count: 0, results: emptyRes }
        return await Promise.resolve({ response, data: empty })
      })
    })
}

export async function getVariableObligations(signal?: AbortSignal): Promise<Interfaces.ObligationsResponse> {
  try {
    const response = await fetch("/api/v1/obligations/", { signal })

    if (!response.ok) {
      const emptyRes = [] as Interfaces.Obligation[]
      const empty = { count: 0, results: emptyRes }
      return await Promise.resolve({ response, data: empty })
    }

    const result = await response.json()
    // Since we do not have an api to actually filter our varaiable obligations, we do it here
    if (result.count > 0 && Array.isArray(result.results)) {
      result.results = result.results.filter((obligation: Interfaces.Obligation) => obligation.obligation_type === "variable")
    }
    return { response, data: result }
  } catch (err) {
    console.error(err)
    const emptyRes = [] as Interfaces.Obligation[]
    const empty = { count: 0, results: emptyRes }
    const errorMessage = err instanceof Error ? err.message : "Unknown error"
    return await Promise.resolve({ response: { status: 500, statusText: errorMessage, ok: false }, data: empty })
  }
}

export async function patchObligation(id: number, data: Interfaces.ObligationFormInputs, signal?: AbortSignal): Promise<Interfaces.ObligationResponse> {
  return await fetch(`/api/v1/obligations/${id}/`, {
    method: "PATCH",
    headers: getAuthHeaders(),
    ...credentialsOption,
    body: JSON.stringify(data),
    signal
  }).then(async (response): Promise<Interfaces.ObligationResponse> => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err): Promise<Interfaces.ObligationResponse> => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const goal: Interfaces.Obligation = {
        id: 0,
        name: "",
        obligation_type: "",
        monthly_payment: 0,
        payment_day: 0,
        start_date: "",
        end_date: ""
      }
      return await Promise.resolve({ response, data: goal })
    })
  })
}

export async function getObligation(id: number, signal?: AbortSignal): Promise<Interfaces.ObligationResponse> {
  return await fetch(`/api/v1/obligations/${id}`, { signal })
    .then(async (response): Promise<Interfaces.ObligationResponse> => {
      return await response.json().then(parsedJson => {
        return { response, data: parsedJson }
      }).catch(async (err): Promise<Interfaces.ObligationResponse> => {
        // This catch handles the case where there is no json body in the response
        console.error(err)
        const emptyRes: Interfaces.Obligation = {
          id: 0,
          name: "",
          obligation_type: "",
          monthly_payment: 0,
          payment_day: 0,
          start_date: "",
          end_date: ""
        }
        return await Promise.resolve({ response, data: emptyRes })
      })
    })
}
