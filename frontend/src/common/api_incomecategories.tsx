import type * as Interfaces from "./interfaces"
import { getAuthHeaders, credentialsOption } from "./api_helpers"

export async function getIncomeCategories (signal?: AbortSignal): Promise<Interfaces.IncomeCategoriesResponse> {
  return await fetch("/api/v1/income-categories", { signal })
    .then(async (response): Promise<Interfaces.IncomeCategoriesResponse> => {
      return await response.json().then(parsedJson => {
        return { response, data: parsedJson }
      }).catch(async (err): Promise<Interfaces.IncomeCategoriesResponse> => {
        // This catch handles the case where there is no json body in the response
        console.log(err)
        const emptyRes = [] as Interfaces.IncomeCategory[]
        const empty = { count: 0, results: emptyRes }
        return await Promise.resolve({ response, data: empty })
      })
    })
}

export async function createIncomeCategory (data: any, signal?: AbortSignal): Promise<Interfaces.IncomeCategoryResponse> {
  return await fetch("/api/v1/income-categories", {
    method: "POST",
    headers: getAuthHeaders(),
    ...credentialsOption,
    body: JSON.stringify(data),
    signal
  }).then(async (response): Promise<Interfaces.IncomeCategoryResponse> => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err): Promise<Interfaces.IncomeCategoryResponse> => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const emptyRes: Interfaces.IncomeCategory = {
        id: 0,
        text: ""
      }
      return await Promise.resolve({ response, data: emptyRes })
    })
  })
}

export async function getIncomeCategory (id: number, signal?: AbortSignal): Promise<Interfaces.IncomeCategoryResponse> {
  return await fetch(`/api/v1/income-categories/${id}`, { signal })
    .then(async (response) => {
      return await response.json().then(parsedJson => {
        return { response, data: parsedJson }
      }).catch(async (err): Promise<Interfaces.IncomeCategoryResponse> => {
        // This catch handles the case where there is no json body in the response
        console.log(err)
        const empty: Interfaces.IncomeCategory = {
          id: 0,
          text: ""
        }
        return await Promise.resolve({ response, data: empty })
      })
    })
}

export async function patchIncomeCategory (id: number, data: Interfaces.IncomeCategoryFormInputs, signal?: AbortSignal): Promise<Interfaces.IncomeCategoryResponse> {
  return await fetch(`/api/v1/income-categories/${id}/`, {
    method: "PATCH",
    headers: getAuthHeaders(),
    ...credentialsOption,
    body: JSON.stringify(data),
    signal
  }).then(async (response) => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err): Promise<Interfaces.IncomeCategoryResponse> => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const category: Interfaces.IncomeCategory = {
        id: 0,
        text: ""
      }
      return await Promise.resolve({ response, data: category })
    })
  })
}
