import type * as Interfaces from "./interfaces"
import { getAuthHeaders, credentialsOption, handleEmptyResponse } from "./api_helpers"

export async function createSavingCategory (data: any, signal?: AbortSignal): Promise<Interfaces.SavingCategoryResponse> {
  return await fetch("/api/v1/savingcategories", {
    method: "POST",
    headers: getAuthHeaders(),
    ...credentialsOption,
    body: JSON.stringify(data),
    signal
  }).then(async (response): Promise<Interfaces.SavingCategoryResponse> => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err): Promise<Interfaces.SavingCategoryResponse> => {
      // This catch handles the case where there is no json body in the response
      const empty: Interfaces.SavingCategory = {
        id: 0,
        text: ""
      }
      return handleEmptyResponse(err, empty)
    })
  })
}

export async function getSavingCategories (signal?: AbortSignal): Promise<Interfaces.SavingCategoriesResponse> {
  return await fetch("/api/v1/savingcategories", {
    signal
  }).then(async (response): Promise<Interfaces.SavingCategoriesResponse> => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err): Promise<Interfaces.SavingCategoriesResponse> => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const emptyRes = [] as Interfaces.SavingCategory[]
      const empty = { count: 0, results: emptyRes }
      return await Promise.resolve({ response, data: empty })
    })
  })
}

export async function getSavingCategory (id: number, signal?: AbortSignal): Promise<Interfaces.SavingCategoryResponse> {
  return await fetch(`/api/v1/savingcategory/${id}`, {
    signal
  }).then(async (response): Promise<Interfaces.SavingCategoryResponse> => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err): Promise<Interfaces.SavingCategoryResponse> => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const empty: Interfaces.SavingCategory = {
        id: 0,
        text: ""
      }
      return await Promise.resolve({ response, data: empty })
    })
  })
}

export async function patchSavingCategory (id: number, data: Interfaces.SavingCategoryFormInputs, signal?: AbortSignal): Promise<Interfaces.SavingCategoryResponse> {
  return await fetch(`/api/v1/savingcategory/${id}/`, {
    method: "PATCH",
    headers: getAuthHeaders(),
    ...credentialsOption,
    body: JSON.stringify(data),
    signal
  }).then(async (response): Promise<Interfaces.SavingCategoryResponse> => {
    return await response.json().then(parsedJson => {
      return { response, data: parsedJson }
    }).catch(async (err): Promise<Interfaces.SavingCategoryResponse> => {
      // This catch handles the case where there is no json body in the response
      console.log(err)
      const category: Interfaces.SavingCategory = {
        id: 0,
        text: ""
      }
      return await Promise.resolve({ response, data: category })
    })
  })
}
