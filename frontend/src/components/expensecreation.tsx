import { useForm } from "react-hook-form"
import React, { useEffect, useRef, useState, useCallback } from "react"

import { createExpense, getExpenseCandiates } from "../common/api_expenses"
import { getCategories } from "../common/api_categories"
import type { Category, ExpenseFormInputs, FastExpenseCandidate, FastCreateExpenseTableProps, ReactFragment } from "../common/interfaces"
import { createCategoryGuidance, CustomSpinner, FailedBanner, styleErrorMessage } from "./shared"
import { createExpenseSubmissionHandler, locallyLookUpCategory, renderDismissableAlert } from "../common/utilities"

const defaultValues: ExpenseFormInputs = {
  amount: null,
  category_id: 0,
  note: "",
  date: new Date().toLocaleDateString("en-CA")
}

const updateForm = (setValue: { (name: string, value: any): void, (name: string, value: any): void, (arg0: string, arg1: any): void }, category: number, amount: number, note: string): void => {
  setValue("category", category)
  setValue("amount", amount)
  setValue("note", note)
}

/**
 * Renders a row for quick expense creation with frequently logged expense details.
 *
 * @param {Object} props - The properties object.
 * @param {string} props.categoryName - The name of the expense category.
 * @param {Function} props.setValue - Function to set form values.
 * @param {FastExpenseCandidate} props.expense - The expense data.
 * @param {Function} props.getValues - Function to get form values.
 * @param {Function} props.onSubmit - Function to handle form submission.
 * @returns {JSX.Element} - JSX element representing a row in the fast expense creation table.
 */
const FastExpenseTableRow = ({ categoryName, setValue, expense, getValues, onSubmit }: {
  categoryName: string
  setValue: (name: string, value: any) => void
  expense: FastExpenseCandidate
  getValues: () => ExpenseFormInputs
  onSubmit: (formValues: ExpenseFormInputs) => void
}): JSX.Element => {
  return (
    <tr>
      {/* Hardcoded to JPY for now */}
      <td>￥{Math.floor(Number(expense.amount))}</td>
      <td>{categoryName} (ID: {expense.categoryId})</td>
      <td>{expense.note}</td>
      <td>
        <button className="btn btn-secondary my-2" type="button" onClick={() => {
          updateForm(setValue, expense.categoryId, Math.floor(Number(expense.amount)), expense.note)
        }}>Fill
        </button>
      </td>
      <td>
        <button className="btn btn-primary my-2" type="button" onClick={() => {
          updateForm(setValue, expense.categoryId, Math.floor(Number(expense.amount)), expense.note)
          onSubmit(getValues())
        }}>Fill and Submit
        </button>
      </td>
    </tr>
  )
}

/**
 * Component for quickly creating expense entries in a table format.
 *
 * @component
 * @param {FastCreateExpenseTableProps} props - The properties for the component.
 * @param {Expense[]} props.data - The array of expense data to be displayed.
 * @param {Category[]} props.categories - The array of categories for expense categorization.
 * @param {Function} props.setValue - Function to set the value of an expense field.
 * @param {Function} props.getValues - Function to get the current values of the expense fields.
 * @param {Function} props.onSubmit - Function to handle the submission of the expense form.
 * @returns {JSX.Element} The rendered component.
 */
const FastCreateExpenseTable = ({ data, categories, setValue, getValues, onSubmit }: FastCreateExpenseTableProps): JSX.Element => {
  const fastExpenses = data.map((expense, index) => {
    const categoryName = locallyLookUpCategory(expense.categoryId, categories)
    return (
      <FastExpenseTableRow
        key={index}
        categoryName={categoryName}
        setValue={setValue}
        expense={expense}
        getValues={getValues}
        onSubmit={onSubmit}
      />
    )
  })

  return (
    <div className="table-responsive">
      <table className="table table-striped">
        <thead>
          <tr>
            <th>Price</th>
            <th>Category</th>
            <th>Note</th>
            <th>Auto fill</th>
            <th>Auto create</th>
          </tr>
        </thead>
        <tbody>
          {fastExpenses}
        </tbody>
      </table>
    </div>
  )
}

export const ExpenseCreation = (): JSX.Element => {
  // eslint-disable-next-line @typescript-eslint/unbound-method
  const { register, handleSubmit, setError, formState: { errors }, reset, setValue, getValues } = useForm<ExpenseFormInputs>()
  const [isSending, setIsSending] = useState(false)
  const [categories, setCategories] = useState<Category[]>([{ id: 0, text: "Loading..." }])
  const initialRender = useRef(true)
  const [showDismissable, setShowDismissable] = useState(false)
  const [isUpdateSuccess, setIsUpdateSuccess] = useState(false)
  const [fastExpenseCandidates, setFastExpenseCandidates] = useState<FastExpenseCandidate[]>([])

  const [apiResponseComplete, setApiResponseComplete] = useState(false)
  const [criticalError, setCriticalError] = useState(false)

  const [genericFormError, setGenericFormError] = useState("")

  const onSubmit = useCallback(
    (formValues: ExpenseFormInputs) => {
      const abortController = new AbortController()
      const signal = abortController.signal

      const handler = createExpenseSubmissionHandler({
        setShowDismissable,
        setIsSending,
        getIsSending: () => isSending,
        createExpense,
        setGenericFormError,
        setIsUpdateSuccess,
        setError,
        reset,
        defaultValues
      }, signal)

      return handler(formValues)
    },
    [isSending, reset, setError]
  )

  useEffect(() => {
    const abortController = new AbortController()
    const signal = abortController.signal

    if (initialRender.current) {
      initialRender.current = false
      setIsSending(true)

      Promise.all([getCategories(signal), getExpenseCandiates(signal)]).then((values) => {
        const cat = values[0]
        const exp = values[1]
        let isCriticalError = false

        if (cat?.response?.ok) {
          setCategories(cat.data.results)
        } else {
          isCriticalError = true
        }

        if (exp?.response?.ok) {
          setFastExpenseCandidates(exp.data.results)
        } else {
          isCriticalError = true
        }

        setCriticalError(isCriticalError)
      }).catch((err) => {
        setCriticalError(true)
        console.log("Failed to fetch page: ", err)
      }).finally(() => {
        setIsSending(false)
        setApiResponseComplete(true)
      })
    }

    return () => { abortController.abort() }
  }, [])

  const displayTitle = (): JSX.Element => {
    return <h3>Create an expense </h3>
  }

  const displayCreationForm = (): ReactFragment => {
    return (
      <form onSubmit={handleSubmit(onSubmit)} className="form">
        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="category_field">Category</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <select id="category_field" name="category" ref={register({ required: "This field is required." })}>
              {categories.map(value => (<option key={value.id} value={value.id}>
                {value.text}
              </option>))}
            </select>
            {errors?.category?.message !== undefined ? styleErrorMessage(errors.category.message) : null}
          </div>
        </div>

        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="amount_field">Amount</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input className="form-control" name="amount" type="number" step="0.01" id="amount_field"
              ref={register({
                required: "This field is required.",
                min: {
                  value: 0.01,
                  message: "Amount should be more than 0"
                },
                validate: value => /^\d+(\.\d{1,2})?$/.test(value) || "Amount should be a valid number with up to 2 decimal places"
              })}
            />
            {errors?.amount?.message !== undefined ? styleErrorMessage(errors.amount.message) : null}
          </div>
        </div>

        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="date_entity">Date&nbsp;</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input name="date" type="date" id="date_entity" defaultValue={new Date().toLocaleDateString("en-CA")} ref={register({ required: true })} />
            {errors?.date?.message !== undefined ? styleErrorMessage(errors.date.message) : null}
          </div>
        </div>

        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="note_field">Note</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input className="form-control" name="note" id="note_field"
              ref={register({
                maxLength: {
                  value: 50,
                  message: "Max 50 chars"
                }
              })}
            />
            {errors?.note?.message !== undefined ? styleErrorMessage(errors.note.message) : null}
          </div>
        </div>

        <div className="form-group">
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            {genericFormError !== "" ? styleErrorMessage(genericFormError) : null}
            <button className="btn btn-secondary my-2 my-sm-0" disabled={isSending}>{isSending ? "Submitting..." : "Submit"}</button>
          </div>
        </div>

      </form>
    )
  }

  const displayFastExpenseCandidates = (): JSX.Element | null => {
    return (
      fastExpenseCandidates.length > 0
        ? <div>
          <h5>Most frequent expenses in the last 6 months</h5>
          <FastCreateExpenseTable data={fastExpenseCandidates} categories={categories} setValue={setValue} getValues={getValues} onSubmit={onSubmit} />
        </div>
        : null
    )
  }

  const renderDismissable = (): JSX.Element | null =>
    renderDismissableAlert({
      setShowDismissable,
      getIsShowDismissable: () => showDismissable,
      successMessage: "Expense created!",
      failureMessage: "Expense creation failed :(",
      getIsUpdateSuccess: () => isUpdateSuccess
    })

  const displayView = (): JSX.Element => {
    if (!apiResponseComplete) {
      return CustomSpinner()
    }

    if (criticalError) {
      return FailedBanner()
    }

    return <React.Fragment>
      {displayTitle()}
      {renderDismissable()}
      {categories !== null && Array.isArray(categories) && categories.length > 0 ? displayCreationForm() : createCategoryGuidance()}
      {displayFastExpenseCandidates()}
    </React.Fragment>
  }

  return displayView()
}
