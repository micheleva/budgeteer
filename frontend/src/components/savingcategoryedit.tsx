import { useForm } from "react-hook-form"
import Alert from "react-bootstrap/Alert"
import { useParams, Link } from "react-router-dom"
import React, { useEffect, useRef, useState, useCallback } from "react"
import { getSavingCategory, patchSavingCategory } from "../common/api_savingcategories"
import type { SavingCategory, SavingCategoryFormInputs, SavingCategoryResponse } from "../common/interfaces"
import { CustomSpinner, styleErrorMessage } from "./shared"

export const SavingCategoryEdit = (): JSX.Element => {
  // eslint-disable-next-line @typescript-eslint/unbound-method
  const { register, handleSubmit, setError, errors, setValue } = useForm<SavingCategoryFormInputs>()
  const [isSending, setIsSending] = useState(false)
  const { id } = useParams()
  const [category, setCategory] = useState<SavingCategory>()
  const initialRender = useRef(true)
  const [showDismissable, setShowDismissable] = useState(false)
  const [isUpdateSuccess, setIsUpdateSuccess] = useState(false)
  const autoCloseDismissibleAfterMs = 1500
  const [invalidCategory, setInvalidCategory] = useState(false)

  const [genericFormError, setGenericFormError] = useState("")

  const fetchData = useCallback((displayModal = true, signal?: AbortSignal) => {
    if (isSending) return
    setIsSending(true)
    setIsUpdateSuccess(false)

    getSavingCategory(Number(id), signal).then((g: SavingCategoryResponse) => {
      console.log(g, g?.response === undefined)
      if (g?.response === undefined) {
        setInvalidCategory(true)
        setIsUpdateSuccess(false)
      } else if (g?.response?.ok) {
        setInvalidCategory(false)
        setIsUpdateSuccess(false)
        const savingCat = g.data
        setCategory(savingCat)
        // IMPORTANT: We need to call setValue() AFTER render (with the form) has run
        // of the form will render undefined (i.e. "") in each field
        // as per https://stackoverflow.com/a/59547360
        // This means we can not use a flag like apiResponseComplete
        // as we do in goalstab.tsx
        setValue("text", savingCat.text)
        setValue("is_archived", savingCat.is_archived)
      } else {
        setInvalidCategory(true)
        setIsUpdateSuccess(false)
      }
    }).catch((err) => {
      setInvalidCategory(true)
      setIsUpdateSuccess(false)
      console.log("Failed to fetch page: ", err)
    }).finally(() => {
      setIsSending(false)
      if (displayModal === true) {
        setShowDismissable(true)
      }
    })
  }, [id])

  useEffect(() => {
    if (initialRender.current) {
      initialRender.current = false
      fetchData(false)
    }
  }, [fetchData])

  const onSubmit = useCallback(async (formValues: SavingCategoryFormInputs) => {
    setIsSending(true)
    const abortController = new AbortController()
    const signal = abortController.signal

    await patchSavingCategory(category?.id ?? 0, formValues, signal)
      .then((res) => {
        if (!res?.response?.ok) {
          if (res?.data) {
            for (const [key, value] of Object.entries(res.data)) {
              console.log("Saving Category edit form error(s):", key, value)
              // Set error for existing fields
              if (key in formValues) {
                setError(key as keyof SavingCategoryFormInputs, { message: value[0] })
              } else {
                // Other validation errors
                setGenericFormError(value[0])
              }
            }
          } else {
            // Handle 40X,50X errors in here
            setGenericFormError("Something is wrong with the server, try again later!")
          }
          setIsUpdateSuccess(false)
        } else {
          setGenericFormError("")
          setIsUpdateSuccess(true)
        }
      })
      .catch(error => {
        if (error instanceof Error && error.name !== "AbortError") {
          console.log("Saving Category edit error ", error)
          setIsUpdateSuccess(false)
        }
      })
      .finally(() => {
        if (!signal.aborted) {
          setIsSending(false)
          setShowDismissable(true)
          const timer = setTimeout(() => {
            setShowDismissable(false)
          }, autoCloseDismissibleAfterMs)
          return () => { clearTimeout(timer) }
        }
      })

    return () => { abortController.abort() }
  }, [category])

  const displayTitle = (category: SavingCategory): JSX.Element => <h3>Editing Saving Category ID: {category.id}</h3>

  const missingOrUnavailableAlert = (): React.ReactNode | JSX.Element => {
    // NOTE: This "invalid-category-marker" id is used by the functional tests, do not remove it!
    return <Alert id="invalid-category-marker" variant="danger">
      <Alert.Heading>Sorry, it seems the Saving Category you're trying to edit does not exist...or there was an error. Please try again later, or reload the page</Alert.Heading>
    </Alert>
  }

  const displayForm = (): JSX.Element => {
    return (
      <form onSubmit={handleSubmit(onSubmit)} className="form">
        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="text_field">Saving Category text</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input className="form-control" name="text" type="text" id="text_field"
              ref={register({
                required: "This field is required.",
                maxLength: {
                  value: 20,
                  message: "Max 20 chars"
                }
              })}
            />
          </div>
          {errors?.text !== undefined && errors?.text !== null ? styleErrorMessage(errors.text.message as string) : null}
        </div>

        <div className="form-group">
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <label><input type="checkbox" id="is_archived_field" name="is_archived" ref={register()} />&nbsp;Is archived</label>
            {errors?.is_archived !== undefined && errors?.is_archived !== null ? styleErrorMessage(errors.is_archived.message as string) : null}
          </div>
        </div>

        <div className="form-group">
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            {genericFormError !== "" ? styleErrorMessage(genericFormError) : null}
            <button className="btn btn-secondary my-2 my-sm-0" disabled={isSending}>{isSending ? "Submitting..." : "Submit"}</button>
          </div>
        </div>

      </form>
    )
  }
  const displayView = (): JSX.Element => {
    // IMPORTANT: We need to call setValue() AFTER render (with the form) has run
    // of the form will render undefined (i.e. "") in each field
    // as per https://stackoverflow.com/a/59547360

    // This means we can not use a flag like apiResponseComplete
    // as we do in goalstab.tsx

    // FIXME: BUGFIX-245
    // TRY ME OUT <========
    // try to make the form 'hidden' using css, and see if the setValue() binding works

    if (invalidCategory) {
      return missingOrUnavailableAlert()
    } else if (category !== undefined && category !== null) {
      return <React.Fragment>
        <div>
          {showDismissable
            ? <Alert variant={isUpdateSuccess ? "success" : "danger"} onClose={() => { setShowDismissable(false) }} dismissible>
              <Alert.Heading>{isUpdateSuccess ? "Category updated!" : "Category failed :("}</Alert.Heading>
            </Alert>
            : null}
          <Link to={"/app/saving-categories"}>Back to Saving Categories</Link>
          <div>{displayTitle(category)}</div>
        </div>

        {displayForm()}
      </React.Fragment>
    } else {
      return CustomSpinner()
    }
  }

  return displayView()
}
