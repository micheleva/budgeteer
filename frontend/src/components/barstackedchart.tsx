import React, { useEffect, useState } from "react"
import { Chart } from "chart.js"
import { Bar } from "react-chartjs-2"
import type { Goal, internalChartDataSet, MonthlyBalanceWithFlatCategory, MonthlyBalanceWithFlatCategoryPagedData, Obligation } from "../common/interfaces"
import ChartjsPluginStacked100 from "chartjs-plugin-stacked100" // @ts-ignore
import "chartjs-plugin-colorschemes"

// Credits https://stackoverflow.com/a/64821465
//  "Since you install chart.js as a peer dependency with react-chartjs-2 you can simply use the object given by chart.js.
//  Javascript objects work with references, so changing settings there will modify react-chartjs-2."
Chart.pluginService.register(ChartjsPluginStacked100)

interface BarStackedChartProps {
  chartData: {
    monthlyBalances: MonthlyBalanceWithFlatCategoryPagedData[]
    goals: Goal[]
    specialGoals: Obligation[]
  }
}

export const BarStackedChart = (props: BarStackedChartProps): JSX.Element => {
  const [chartData, setChartData] = useState({})

  useEffect(() => {
    getChartData()
  }, [])

  const getChartData = (): void => {
    const { monthlyBalances, goals, specialGoals } = props.chartData
    const isInvesting = (balance: any): boolean => {
      // When assets are returned as Monthly Balance, their category is set to -1
      // hence if the category.id is negative or if the category is explicitly marked as investing
      // mark this balance category as "Investing (e.g. not cash-like)"
      return (balance?.is_investing === true)
    }

    const groupDataByType = (data: MonthlyBalanceWithFlatCategory[], goals: Goal[], specialGoals: Goal[]): internalChartDataSet => {
      const nonInvested = {}
      const invested = {}
      const labels: string[] = []
      data.map(a => !labels.includes(a.date) ? labels.push(a.date) : null)

      labels.sort(function (a, b) {
        return a > b ? 1 : a < b ? -1 : 0
      })

      labels.map(date => { // Initialize the balances for each date
        nonInvested[date] = 0
        invested[date] = 0
      })

      data.map(balance => { // Add the balances to the correct date
        const isInvestingAmount = isInvesting(balance)
        if (isInvestingAmount) {
          invested[balance.date] += balance.amount
        } else {
          nonInvested[balance.date] += balance.amount
        }
      })

      const nonInvestedBalancesFlat = Object.keys(nonInvested).map(date => {
        return nonInvested[date]
      })
      const investedBalancesFlat = Object.keys(nonInvested).map(date => {
        return invested[date]
      })

      const finalData = {
        labels,
        datasets: [{
          label: "Non invested (Cash)",
          data: nonInvestedBalancesFlat
          // stack: 1
        },
        {
          label: "Invested Assets",
          data: investedBalancesFlat
          // stack: 1
        }
        ]
      }

      return finalData
    }

    const finalData = groupDataByType(monthlyBalances, goals, specialGoals)
    setChartData(finalData)
  }

  const chartOptions = {
    plugins: {
      title: {
        display: true,
        text: "Monthly Budgets stacked"
      },
      stacked100: {
        enable: true
      }
    },
    responsive: true,
    tooltips: {
      mode: "label",
      callbacks: {
        footer: (tooltipItems, data) => {
          const value = tooltipItems.reduce((a, e) => {
            return a + parseInt(e.yLabel)
          }, 0)

          const total = new Intl.NumberFormat("ja-JP", {
            style: "currency",
            currency: "JPY"
          }).format(value)

          return `Total: ${total}`
        },
        label: (tooltipItem, data) => {
          const datasetIndex = tooltipItem.datasetIndex
          const index = tooltipItem.index
          const datasetLabel = data.datasets[datasetIndex].label || ""
          const originalValue = data.originalData[datasetIndex][index]
          const styledRawValue = new Intl.NumberFormat("ja-JP", {
            style: "currency",
            currency: "JPY"
          }).format(originalValue)
          const rateValue = data.calculatedData[datasetIndex][index]
          return `${datasetLabel}: ${rateValue}% (${styledRawValue})`
        }
      }
    },
    maintainAspectRatio: false,
    scales: {
      y: {
        beginAtZero: true
      }
    }
  }

  return (
    <Bar data={chartData} height={800} options={chartOptions}/>
  )
}
