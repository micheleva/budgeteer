import React, { useState } from "react"
import Alert from "react-bootstrap/Alert"
import Button from "react-bootstrap/Button"

import type { Account } from "../common/interfaces"

export const DismissableAlert = ({ url, date, accounts, text, commandTooltip }: { url: string, date: string, text: string, accounts: Account[], commandTooltip: string }): JSX.Element => {
  const [show, setShow] = useState(false)
  const accountsDiv = Array.isArray(accounts) && accounts?.length > 0
    ? accounts.map((a: Account) => {
      const fullUrl = encodeURI(`${url}/${a.text}/${date}.json`)
      return (
        <div key={a.id}>Account named: {a.text}, URL: <a href={fullUrl}>{fullUrl}</a></div>
      )
    })
    : <div> {url}/account-name/YYYY-mm-dd.json (note: do NOT use the account ID, use the account name where this asset is stored. Beware that the account name should be an encoded uri)</div>
  if (show) {
    return (
      <Alert variant="primary" onClose={() => { setShow(false) }} dismissible>
        <Alert.Heading>What you should provide:</Alert.Heading>
        <div>
          <div className="text-danger"> {commandTooltip} </div>
           <div>Each of the following URL:</div>
          {accountsDiv}
          <br></br>
          <div>Should return an array like the following:</div>
          <div style={{ whiteSpace: "pre" }}>{text}</div>
        </div>
      </Alert>
    )
  }
  return <Button onClick={() => { setShow(true) }}>Show expected input format </Button>
}
