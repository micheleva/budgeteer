import React, { useEffect, useCallback, useRef, useState } from "react"
import Alert from "react-bootstrap/Alert"
import { useForm } from "react-hook-form"
import { useParams } from "react-router-dom"
import { displayCategoriesPullDown, FailedBanner, CustomSpinner, styleErrorMessage } from "./shared"
import { getExpense, patchExpense } from "../common/api_expenses"
import { getCategories } from "../common/api_categories"
import type { Category, ExpenseWithCategoryId, PatchExpenseFormInputs, ExpenseResponse } from "../common/interfaces"

export const ExpenseEdit = (): JSX.Element => {
  // eslint-disable-next-line @typescript-eslint/unbound-method
  const { register, handleSubmit, setError, formState: { errors }, setValue } = useForm<PatchExpenseFormInputs>()
  const [isSending, setIsSending] = useState(false)
  const { id } = useParams()
  const [expenseData, setExpenseData] = useState<ExpenseWithCategoryId>()
  const [categories, setCategories] = useState<Category[]>([])
  const initialRender = useRef(true)
  const [showDismissable, setShowDismissable] = useState(false)
  const [isUpdateSuccess, setIsUpdateSuccess] = useState(false)
  const autoCloseDismissibleAfterMs = 1500

  const [criticalError, setCriticalError] = useState(false)
  const [invalidExpense, setInvalidExpense] = useState(false)
  const [genericFormError, setGenericFormError] = useState("")

  const onSubmit = useCallback(async (formValues: PatchExpenseFormInputs) => {
    if (isSending) return
    setShowDismissable(false)
    setIsSending(true)
    const abortController = new AbortController()
    const signal = abortController.signal

    await patchExpense(Number(id), formValues, signal)
      .then((res: ExpenseResponse) => {
        if (!res?.response?.ok) {
          if (res?.data != null && res?.data !== undefined) {
            for (const [key, value] of Object.entries(res.data)) {
              console.log("Expense edit form error(s):", key, value)
              // Set error for existing fields
              if (key in formValues) {
                setError(key as keyof PatchExpenseFormInputs, { message: value[0] })
              } else {
                // Other validation errors
                setGenericFormError(value[0])
              }
            }
          } else {
            // Handle 40X,50X errors in here
            setGenericFormError("Something is wrong with the server, try again later!")
          }
          setIsUpdateSuccess(false)
        } else {
          setGenericFormError("")
          setIsUpdateSuccess(true)
        }
      })
      .finally(() => {
        setIsSending(false)
        setShowDismissable(true)
        const timer = setTimeout(() => {
          setShowDismissable(false)
        }, autoCloseDismissibleAfterMs)
        return () => { clearTimeout(timer) }
      })
    return () => { abortController.abort() }
  }, [id])

  const fetchData = useCallback((signal?: AbortSignal) => {
    if (isSending) return
    setIsSending(true)

    if (id !== undefined && id !== null) {
      Promise.all([getCategories(signal), getExpense(Number(id), signal)]).then((values) => {
        const categoriesRes = values[0]
        const expRes = values[1]
        let isCriticalError = false

        if (categoriesRes?.response?.ok && Array.isArray(categoriesRes.data.results)) {
          setCategories(categoriesRes.data.results)
        } else {
          isCriticalError = true
        }

        if (expRes?.response?.ok) {
          setInvalidExpense(false)
          setExpenseData(expRes.data)
          // Fixed to JPY for now: i.e. no decimal places
          setValue("amount", Math.floor(Number(expRes.data.amount)))
          setValue("date", expRes.data.date)
          // TODO: create an expense interface with a category field
          setValue("category", expRes.data?.category)
          setValue("note", expRes.data.note)
        } else {
          setCriticalError(true)
          // FIXME: we end up here for errors like 403, and 404, so we should
          // set to screen a more descriptive error, and not a generic one
          setInvalidExpense(true)
        }
        setCriticalError(isCriticalError)
      }).catch((e: Error) => {
        if (e instanceof Error && e.name !== "AbortError") {
          console.log(e)
        }
        setCriticalError(true)
      }).finally(() => {
        if (signal !== undefined && !signal?.aborted) {
          setIsSending(false)
        }
      })
    } else {
      setInvalidExpense(true)
    }
  }, [id])

  useEffect(() => {
    const abortController = new AbortController()
    const signal = abortController.signal

    if (initialRender.current) {
      initialRender.current = false
      fetchData(signal)
    }

    return () => { abortController.abort() }
  }, [fetchData])

  const missingOrUnavailableHeader = (): JSX.Element => {
    const msg = "Sorry, it seems the expense you're trying to editing does not exist...or there was an error. Please try again later, or reload the page"
    // NOTE: This "invalid-expense-marker" id is used by the functional tests, do not remove it!
    return <Alert id="invalid-expense-marker" variant="danger">
      <Alert.Heading>{msg}</Alert.Heading>
    </Alert>
  }

  const displayTitle = (): JSX.Element => <div><h3>Update an expense </h3></div>

  const displayView = (): JSX.Element => {
    // IMPORTANT: We need to call setValue() AFTER render (with the form) has run
    // of the form will render undefined (i.e. "") in each field
    // as per https://stackoverflow.com/a/59547360

    // This means we can not use a flag like apiResponseComplete
    // as we do in goaledittab.tsx

    // FIXME: BUGFIX-245
    // TRY ME OUT <========
    // try to make the form 'hidden' using css, and see if the setValue() binding works

    if (invalidExpense) {
      return missingOrUnavailableHeader()
    } else if (criticalError) {
      return FailedBanner()
    } else if (expenseData != null) {
      return (
        <React.Fragment>
          {displayTitle()}
          {showDismissable
            ? <Alert variant={isUpdateSuccess ? "success" : "danger"} onClose={() => { setShowDismissable(false) }} dismissible>
              <Alert.Heading>{isUpdateSuccess ? "Expense updated!" : "An error has accourred. Please try again."}</Alert.Heading>
            </Alert>
            : null}
          <form onSubmit={handleSubmit(onSubmit)} className="form">

            <div className="form-group">
              <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="text_field">Expense Category:&nbsp;</label>
              <div className="col-sm-6 col-md-4 3 col-lg-3">
                {displayCategoriesPullDown(categories, register)}
                {(errors?.category?.message !== null && errors?.category?.message !== undefined) ? styleErrorMessage(errors.category.message) : null}
              </div>
            </div>

            <div className="form-group">
              <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="amount_field">Amount</label>
              <div className="col-sm-6 col-md-4 3 col-lg-3">
                <input className="form-control" name="amount" type="number" step="0.01" id="amount_field"
                  ref={register({
                    required: "This field is required.",
                    min: {
                      value: 0.01,
                      message: "Amount should be more than 0"
                    },
                    validate: value => /^\d+(\.\d{1,2})?$/.test(value) || "Amount should be a valid number with up to 2 decimal places"
                  })}
                />
                {errors?.amount?.message !== undefined ? styleErrorMessage(errors.amount.message) : null}
              </div>
            </div>

            <div className="form-group">
              <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="date_entity">Date&nbsp;</label>
              <div className="col-sm-6 col-md-4 3 col-lg-3">
                <input name="date" type="date" id="date_entity" defaultValue={new Date().toLocaleDateString("en-CA")} ref={register({ required: true })} />
                {(errors.date != null) ? styleErrorMessage(errors.date.message as string) : null}
              </div>
            </div>

            <div className="form-group">
              <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="note_field">Note</label>
              <div className="col-sm-6 col-md-4 3 col-lg-3">
                <input className="form-control" name="note" id="note_field"
                  ref={register({
                    maxLength: {
                      value: 50,
                      message: "Max 50 chars"
                    }
                  })}
                />
                {(errors.note != null) ? styleErrorMessage(errors.note.message as string) : null}
              </div>
            </div>

            <div className="form-group">
              <div className="col-sm-6 col-md-4 3 col-lg-3">
                {(genericFormError.length > 0) ? styleErrorMessage(genericFormError) : null}
              </div>
            </div>

            <div className="form-group">
              <div className="col-sm-6 col-md-4 3 col-lg-3">
                <button className="btn btn-secondary my-2 my-sm-0" disabled={isSending}>{isSending ? "Submitting..." : "Submit"}</button>
              </div>
            </div>

          </form>
        </React.Fragment>)
    } else {
      return CustomSpinner()
    }
  }

  return displayView()
}
