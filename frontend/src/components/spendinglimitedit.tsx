import { useForm } from "react-hook-form"
import Alert from "react-bootstrap/Alert"
import { useParams, Link } from "react-router-dom"
import React, { useEffect, useRef, useState, useCallback } from "react"
import { getSpendingLimit, updateSpendingLimit } from "../common/api_spendinglimits"
import { getCategories } from "../common/api_categories"
import type { Category, SpendingLimit, SpendingLimitFormInputs } from "../common/interfaces"
import { FailedBanner, CustomSpinner, displayCategoriesPullDown, styleErrorMessage } from "./shared"

export const SpendingLimitEdit = (): JSX.Element => {
  // eslint-disable-next-line @typescript-eslint/unbound-method
  const { register, handleSubmit, setError, errors, setValue } = useForm<SpendingLimitFormInputs>()
  const [isSending, setIsSending] = useState(false)
  const { id } = useParams()
  const [limit, setLimit] = useState<SpendingLimit>()
  const [categories, setCategories] = useState<Category[]>([])
  const initialRender = useRef(true)
  const [showDismissable, setShowDismissable] = useState(false)
  const [isUpdateSuccess, setIsUpdateSuccess] = useState(false)
  const autoCloseDismissibleAfterMs = 1500
  const [InvalidLimit, setInvalidLimit] = useState(false)
  const [criticalError, setCriticalError] = useState(false)
  const [genericFormError, setGenericFormError] = useState("")

  const fetchData = useCallback((signal?: AbortSignal) => {
    if (isSending) return
    setIsSending(true)

    if (id !== undefined && id !== null) {
      Promise.all([getCategories(signal), getSpendingLimit(Number(id), signal)]).then((values) => {
        const categoriesRes = values[0]
        const limitRes = values[1]
        let isCriticalError = false

        if (categoriesRes?.response?.ok && Array.isArray(categoriesRes.data.results)) {
          setCategories(categoriesRes.data.results)
        } else {
          isCriticalError = true
        }

        if (limitRes?.response?.ok) {
          const resLimit = limitRes.data
          setLimit(resLimit)
          // IMPORTANT: We need to call setValue() AFTER render (with the form) has run
          // of the form will render undefined (i.e. "") in each field
          // as per https://stackoverflow.com/a/59547360
          // This means we can not use a flag like apiResponseComplete
          // as we do in goalstab.tsx
          setValue("amount", resLimit.amount)
          setValue("currency", resLimit.currency)
          setValue("days", resLimit.days)
          setValue("category", resLimit.category)
          setValue("text", resLimit.text)
          setGenericFormError("")
          setInvalidLimit(false)
          setIsUpdateSuccess(true)
        } else {
          isCriticalError = true
          // FIXME: we end up here for errors like 403, and 404, so we should
          // set to screen a more descriptive error, and not a generic one
          setInvalidLimit(true)
        }
        setCriticalError(isCriticalError)
      }).catch((e) => {
        if (e instanceof Error && e.name !== "AbortError") {
          console.log(e)
          setCriticalError(true)
        }
      }).finally(() => {
        if (signal !== undefined && !signal?.aborted) {
          setIsSending(false)
          // setApiResponseComplete(true)
        }
      })
    } else {
      setInvalidLimit(true)
    }
  }, [id])

  useEffect(() => {
    const abortController = new AbortController()
    const signal = abortController.signal

    if (initialRender.current) {
      initialRender.current = false
      fetchData(signal)
    }

    return () => { abortController.abort() }
  }, [fetchData])

  const onSubmit = useCallback((formValues: SpendingLimitFormInputs) => {
    setIsSending(true)
    const abortController = new AbortController() 
    const signal = abortController.signal

    if (limit?.id !== undefined && limit?.id !== null) {
      updateSpendingLimit(limit.id, formValues, signal)
        .then((res) => {
          if (!res?.response?.ok) {
            if (res?.data != null) {
              for (const [key, value] of Object.entries(res.data)) {
                console.log("Get goal error(s):", key, value)
                // Set error for existing fields
                if (key in formValues) {
                  setError(key as keyof SpendingLimitFormInputs, { message: value[0] })
                } else {
                  // Other validation errors
                  setGenericFormError(value[0])
                }
              }
            } else {
              // Handle 40X,50X errors in here
              setGenericFormError("Something is wrong with the server, try again later!")
            }
            setIsUpdateSuccess(false)
          } else {
            setIsUpdateSuccess(true)
            setGenericFormError("")
          }
        })
        .catch((err) => {
          if (err instanceof Error && err.name !== "AbortError") {
            console.log(err)
            setIsUpdateSuccess(false)
          }
        })
        .finally(() => {
          if (signal !== undefined && !signal?.aborted) {
            setIsSending(false)
            setShowDismissable(true)
            const timer = setTimeout(() => {
              setShowDismissable(false)
            }, autoCloseDismissibleAfterMs)
            return () => { clearTimeout(timer) }
          }
        })
    } else {
      setInvalidLimit(true)
      setIsSending(false)
    }
  }, [limit])

  const displayTitle = (limit: SpendingLimit): JSX.Element => {
    return <h3>Editing SpendingLimit ID: {limit.id}</h3>
  }

  const missingOrUnavailableHeader = (): JSX.Element => {
    const msg = "Sorry, it seems the SpendingLimit you're trying to editing does not exist...or there was an error. Please try again later, or reload the page"
    // NOTE: This "invalid-spending-limit-marker" id is used by the functional tests, do not remove it!
    return <Alert id="invalid-spending-limit-marker" variant="danger">
      <Alert.Heading>{msg}</Alert.Heading>
    </Alert>
  }

  const displayForm = (): JSX.Element => {
    return (
      <form onSubmit={handleSubmit(onSubmit)} className="form">
        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="amount_field">Amount</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input className="form-control" name="amount" type="number" id="amount_field"
              ref={register({
                required: "This field is required.",
                min: {
                  value: 1,
                  message:
                    "Amount should be more than 0"
                }
              })}
            />
            {(errors.amount != null) ? styleErrorMessage(errors.amount.message as string) : null}
          </div>
        </div>

        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="category_field">Category</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            {displayCategoriesPullDown(categories, register)}
            {(errors?.category?.message !== null && errors?.category?.message !== undefined) ? styleErrorMessage(errors.category.message) : null}
          </div>
        </div>

        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="text_field">Text</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input className="form-control" name="text" id="text_field"
              ref={register({
                required: "This field is required.",
                maxLength: {
                  value: 20,
                  message: "Max 20 chars"
                }
              })}
            />
            {(errors.text != null) ? styleErrorMessage(errors.text.message as string) : null}
          </div>
        </div>

        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="currency_field">Currency</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input className="form-control" name="currency" id="currency_field"
              ref={register({
                maxLength: {
                  value: 3,
                  message: "Max 3 chars"
                }
              })}
            />
            {(errors.currency != null) ? styleErrorMessage(errors.currency.message as string) : null}
          </div>
        </div>

        <div className="form-group">
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <label><input type="checkbox" id="is_archived_field" name="is_archived" ref={register()} />&nbsp;Is archived</label>
            {errors.is_archived !== null && errors.is_archived !== undefined ? styleErrorMessage(errors.is_archived?.message as string) : null}
          </div>
        </div>

        <div className="form-group">
          <div className="col-sm-6 col-md-4 col-lg-3">
            <div>{genericFormError !== null && genericFormError !== undefined && Array.isArray(genericFormError) && genericFormError.length > 0 ? styleErrorMessage(genericFormError[0]) : null}</div>
          </div>
        </div>

        <div className="form-group">
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <button className="btn btn-secondary my-2 my-sm-0" disabled={isSending}>{isSending ? "Submitting..." : "Submit"}</button>
          </div>
        </div>

      </form>
    )
  }
  const displayView = (): JSX.Element => {
    // IMPORTANT: We need to call setValue() AFTER render (with the form) has run
    // of the form will render undefined (i.e. "") in each field
    // as per https://stackoverflow.com/a/59547360

    // This means we can not use a flag like apiResponseComplete
    // as we do in goalstab.tsx

    // FIXME: BUGFIX-245
    // TRY ME OUT <========
    // try to make the form 'hidden' using css, and see if the setValue() binding works

    if (InvalidLimit) {
      // if the limit does not exist or belongs to another user, we simply show an error message
      return missingOrUnavailableHeader()
      // If the limit exists, and it is returned by the API we have at least one category
      // i.e. we assume that categories can not be deleted. // atm delete category api does even exist
    } else if (criticalError) {
      return FailedBanner()
    } else if (limit !== null && limit !== undefined) {
      return <React.Fragment>
        <div>
          {showDismissable
            ? <Alert variant={isUpdateSuccess ? "success" : "danger"} onClose={() => { setShowDismissable(false) }} dismissible>
              <Alert.Heading>{isUpdateSuccess ? "SpendingLimit updated!" : "SpendingLimit update failed :("}</Alert.Heading>
            </Alert>
            : null}
          <Link to={"/app/spending-limits"}>Back to SpendingLimits</Link>
          <div>{displayTitle(limit)}</div>
        </div>

        {displayForm()}
      </React.Fragment>
    } else {
      return CustomSpinner()
    }
  }

  return displayView()
}
