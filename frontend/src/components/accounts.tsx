import React, {
  useEffect, useRef, useState, Fragment
} from "react"
import { getAccounts } from "../common/api_accounts"
import type { AccountsResponse, Account } from "../common/interfaces"
import { AccountTable, createAccountGuidance, FailedBanner, CustomSpinner } from "./shared"

/**
 * This component displays a list of accounts.
  *
  * @returns A JSX element displaying the list of accounts.
 */
const Accounts = (): JSX.Element => {
  const [data, setData] = useState<Account[]>([])
  const initialRender = useRef(true)

  const [apiResponseComplete, setApiResponseComplete] = useState(false)
  const [criticalError, setCriticalError] = useState(false)

  useEffect(() => {
    const abortController = new AbortController()

    if (initialRender.current) {
      initialRender.current = false
      getAccounts(abortController.signal).then((dat: AccountsResponse) => {
        if (dat?.response?.ok) {
          setCriticalError(false)
          const accountRes = dat.data.results
          setData(accountRes)
        } else {
          setCriticalError(true)
        }
      })
        .catch((e: Error) => {
          if (e instanceof Error && e.name !== "AbortError") {
            setCriticalError(true)
          }
        })
        .finally(() => {
          if (abortController?.signal !== undefined && !abortController?.signal?.aborted) {
            setApiResponseComplete(true)
          }
        })
    }

    return () => {
      abortController.abort()
    }
  }, [])

  const displayTitle = (): JSX.Element => <h3>List Accounts</h3>

  const displayView = (): JSX.Element => {
    if (!apiResponseComplete) {
      return CustomSpinner()
    }

    if (criticalError) {
      return FailedBanner()
    }

    // If we have no categories, we definitely have no expenses
    if (apiResponseComplete && (data.length === 0)) {
      return <Fragment>{displayTitle()}{createAccountGuidance()}</Fragment>
    }

    return (
      <Fragment>
        { displayTitle() }
        { data.length > 0 ? <AccountTable data={data} /> : null}
      </Fragment>
    )
  }

  return displayView()
}

export default Accounts
