import React, { useEffect, useRef, useState } from "react"
import type { FC, ReactElement } from "react"
import { Link } from "react-router-dom"
import type { RegisterOptions } from "react-hook-form"
import { accumulator, reduceAssets, SmartFormatter } from "../common/utilities"
import type {
  Account, AccountTableProps, Asset, AssetsTableProps,
  BalanceTableProps, BudgetWithCategory,
  Dividend, DeletedObligationResponse,
  Expense, SpendingLimit, ExpensesTableProps,
  GoalTableProps, HTTPResponse, IncomesTableProps, MonthlyBalanceTableProps,
  Obligation,
  ObligationsTableProps,
  PulldownProps, ReducedTableProps, SetCriticalErrorFunction, SymbolAggregateDividend, YearlyAggregateDividend,
  Category,
  CategoriesTableProps,
  SavingCategory,
  LimitExpensesTableProps,
  IncomeCategory,
  MonthlyBalanceCategory,
  ObligationPaymentChange,
  Goal,
  Income,
  HTTPResponseWithMessage
} from "../common/interfaces"
import Alert from "react-bootstrap/Alert"
import Button from "react-bootstrap/Button"
import Modal from "react-bootstrap/Modal"
import ProgressBar from "react-bootstrap/ProgressBar"
import Spinner from "react-bootstrap/Spinner"

/**
 * Formatter for formatting numbers as Japanese Yen (JPY).
 */
export const Formatter = new Intl.NumberFormat("ja-JP", { style: "currency", currency: "JPY" })

/**
 * Dropdown component for selecting options.
 *
 * @param {PulldownProps} props - The component properties.
 * @param {Array<PulldownItem>} props.data - Array of dropdown items.
 * @param {(event: React.ChangeEvent<HTMLSelectElement>) => void} props.onChange - Function to handle change events in the parent component.
 * @returns {React.ReactElement} - JSX element representing the dropdown.
 */
export const Pulldown: FC<PulldownProps> = ({ data, onChange }): ReactElement => {
  const optionItems = data.map((cat) =>
      <option key={cat.id} value={cat.id}>{cat.text}</option>
  )
  return (
     <select onChange={onChange} >
        {optionItems}
     </select>
  )
}

/**
 * Displays a list of categories items, with the option to edit them,
 *
 * @param {CategoriesTableProps} props - The component properties.
 * @param {Array<SavingCategory | IncomeCategory | MonthlyBalanceCategory | Category>} props.categories - Array of categories items.
 * @param {string} props.url - The url corresponding to the category route. e.g. Categories, IncomeCategories, etc.
 * @returns {React.ReactElement} - JSX element representing the expense table.
 */
export const CategoryTable: FC<CategoriesTableProps> = ({ categories, url }): ReactElement => {
  /**
   * Generates the table header for the Categories component
   *
   * @returns The React element representing the table header.
   */
  const generateTableHeader = (): React.ReactElement => {
    return <thead>
      <tr>
         {/* Hide ID row in small screens */}
         <th className="col-1 d-none d-md-table-cell">ID</th>
         <th className="col-1">Category Text</th>
         <th className="col-1">Edit</th>
      </tr>
    </thead>
  }

  /**
   * Generates the table body for displaying categories.
   *
   * @param categories - An array of Category objects.
   * @returns A React element representing the table body.
   */
  const generateTableBody = (categories: Array<SavingCategory | IncomeCategory | MonthlyBalanceCategory | Category>): React.ReactElement => {
    // IMPORTANT: id="categories-table-body" is used in the functional tests, do not remove it!
    return <tbody className="col-12" id="categories-table-body">
      {categories.map((category: SavingCategory | IncomeCategory | MonthlyBalanceCategory | Category) => {
        const completeUrl = `/app/${url}/${category.id}`
        // IMPORTANT: <td id={`cat-${category.id}`}> is used in the functional tests, do not remove it!
        return <tr className="col-12" key={category.id} id={`cat-${category.id}`}>
          <td className="col-1 d-none d-md-table-cell">{category.id}</td>
          <td className="col-1 text-wrap">{category.text !== "" ? category.text : category.text }</td>
          {/* IMPORTANT: <td id={`edit-${category.id}`}> is used in the functional tests, do not remove it! */}
          <td className="col-1 text-wrap"><Link to={completeUrl}><Button id={`edit-${category.id}`} className="btn-success">Edit</Button></Link></td>
        </tr>
      })}
    </tbody>
  }

  const tableHeader = generateTableHeader()
  const tableBody = generateTableBody(categories)
  return <div>
    <div className="table-responsive-sm">
      <table className="table table-sm" >
        { tableHeader }
        { categories.length > 0 ? tableBody : null}
      </table>
    </div>
  </div>
}

/**
 * Displays a list of SpendingLimit items, with the option to edir or delete them.
 *
 * @param {LimitExpensesTableProps} props - The component properties.
 * @param {Array<LimitExpense>} props.data - Array of LimitExpense items.
 * @param {(id: number) => Promise<HTTPResponse>} props.deleteFunc - API function to actually issue a delete request
 * @param {(id: number) => void} props.updateParentData - Function to update parent data after deletion.
 * @returns {React.ReactElement} - JSX element representing the LimitExpense table.
 */
export const LimitExpenseTable: FC<LimitExpensesTableProps> = ({ data, deleteFunc, updateParentData }): ReactElement => {
  // FIXME: remove displayAlertIntoParent, and test everything keeps working
  // FIXME (BUGFIX-242) : DUPLICATED CODE (LimitExpenseTable): great candidate for refactoring. Have a table that accepts 'url for deletion', 'deletion modal text'

  // NOTE: this modal is for displaying the deletion confirmation popup
  const [showModal, setShowModal] = useState(false)
  const [idToDelete, setidToDelete] = useState(0)

  // NOTE: This dismissable is to display the deletion success/failure to the user
  const [showDismissable, setShowDismissable] = useState(false)
  const [isUpdateSuccess, setIsUpdateSuccess] = useState(false)
  const autoCloseDismissibleAfterMs = 1500
  const timerRef = useRef<NodeJS.Timeout>()
  const isMounted = useRef(true)

  const handleShow = (id: number): void => {
    if (!isMounted.current) return
    setShowModal(true)
    setidToDelete(id)
  }

  const handleClose = (): void => {
    if (!isMounted.current) return
    setShowModal(false)
    setidToDelete(0)
  }

  // See ISSUE 38: Cleanup timer when component unmounts
  useEffect(() => {
    isMounted.current = true
    return () => {
      isMounted.current = false
      if (timerRef.current !== null && timerRef.current !== undefined) {
        clearTimeout(timerRef.current)
      }
    }
  }, [])

  const handleDelete = async (): Promise<void> => {
    try {
      if (!isMounted.current || timerRef.current === null || timerRef.current !== undefined) return
      setShowModal(false)
      const res: HTTPResponse = await deleteFunc(idToDelete)
      if (res.ok) {
        setIsUpdateSuccess(true)
        setShowDismissable(true)

        // Close the dismissable after autoCloseDismissibleAfterMs milliseconds
        timerRef.current = setTimeout(() => {
          if (isMounted.current) {
            setShowDismissable(false)
          }
        }, autoCloseDismissibleAfterMs)

        setidToDelete(0)

        // FIX-39: Update parent (which might unmount this component)
        // FIX-39: Apply this to other components sharing the same architecture
        updateParentData(idToDelete)
      } else {
        setIsUpdateSuccess(false)
        setShowDismissable(true)

        timerRef.current = setTimeout(() => {
          if (isMounted.current) {
            setShowDismissable(false)
          }
        }, autoCloseDismissibleAfterMs)
      }
    } catch (err) {
      if (!isMounted.current) return
      console.log(err)
      setIsUpdateSuccess(false)
      setShowDismissable(true)

      timerRef.current = setTimeout(() => {
        if (isMounted.current) {
          setShowDismissable(false)
        }
      }, autoCloseDismissibleAfterMs)
    }
  }

  /**
   * Generates the table header for the LimitExpense table.
   *
   * @returns The React element representing the table header.
   */
  const generateTableHeader = (): React.ReactElement => {
    return <thead>
      <tr>
         {/* Hide ID row in small screens */}
         <th className="col-1 d-none d-md-table-cell">ID</th>
         <th className="col-1">Category</th>
         <th className="col-1">Amount</th>
         <th className="col-1">Currency</th>
         <th className="col-1">Days</th>
         <th className="col-1">Text</th>
         <th className="col-1">Edit</th>
         <th className="col-1">Delete</th>
      </tr>
    </thead>
  }

  /**
   * Generates the table body for displaying LimitExpense.
   *
   * @param expenses - An array of LimitExpense objects.
   * @returns A React element representing the table body.
   */
  const generateTableBody = (data: SpendingLimit[]): React.ReactElement => {
    const currencyFormatter = SmartFormatter("JPY")
    // IMPORTANT: id="spendinglimits-table-body" is used in the functional tests, do not remove it!
    return <tbody className="col-12" id="spendinglimits-table-body">
      {data.map((el: SpendingLimit) => {
        const url = `/app/spending-limits/${el.id}`
        // IMPORTANT: <td id={`exp-${expense.id}`}> is used in the functional tests, do not remove it!
        return <tr className="col-12" key={el.id} id={`exp-${el.id}`}>
          <td className="col-1 d-none d-md-table-cell">{el.id}</td>
          <td className="col-1 text-wrap">{el.category_text}</td>
          <td className="col-1 text-wrap">{currencyFormatter.format(el.amount)}</td>
          <td className="col-1 text-truncate">{el.currency}</td>
          <td className="col-1 text-wrap">{el.days}</td>
          <td className="col-1 text-wrap">{el.text}</td>
          <td className="col-1 text-wrap"><Link to={url}><Button className="btn-success">Edit</Button></Link></td>
          {/* IMPORTANT: <td id={`deleteLimitExpenseButton-${el.id}`}> is used in the functional tests, do not remove it! */}
          <td className="col-1 text-wrap"><Button id={`deleteLimitExpenseButton-${el.id}`} onClick={() => { handleShow(el.id) }} className="btn btn-danger my-2 my-sm-0 triggerModal">Delete</Button></td>
        </tr>
      })}
    </tbody>
  }

  const tableHeader = generateTableHeader()
  return <div>
      <Modal show={showModal} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Modal heading</Modal.Title>
        </Modal.Header>
        <Modal.Body>Do you really want to delete LimitExpense with id: {idToDelete}? # need to add a way to handle error on deletion here</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Undo
          </Button>
          {/* id="confirmDeletion" is used in the functional tests: leave it there! */}
          <Button variant="primary" id="confirmDeletion" onClick={handleDelete}>
            Confirm deletion
          </Button>
        </Modal.Footer>
      </Modal>
      {showDismissable
        ? <Alert variant={isUpdateSuccess ? "success" : "danger"} onClose={() => { setShowDismissable(false) }} dismissible>
          <Alert.Heading>{isUpdateSuccess ? "LimitExpense deleted successfully!" : "LimitExpense deletion failed :("}</Alert.Heading>
        </Alert>
        : null}
      <div className="table-responsive-sm">
        <table className="table table-sm" >
          { tableHeader }
          { data.length > 0 ? generateTableBody(data) : null}
        </table>
      </div>
  </div>
}

/**
 * Displays a list of expense items, and their total, with the option to edir or delete them.
 *
 * @param {ExpensesTableProps} props - The component properties.
 * @param {Array<Expense>} props.data - Array of expense items.
 * @param {(id: number) => Promise<HTTPResponse>} props.deleteFunc - API function to actually issue a delete request
 * @param {(id: number) => void} props.updateParentData - Function to update parent data after deletion.
 * @returns {React.ReactElement} - JSX element representing the expense table.
 */
export const ExpenseTable: FC<ExpensesTableProps> = ({ data, deleteFunc, updateParentData }): ReactElement => {
  // FIXME: remove displayAlertIntoParent, and test everything keeps working
  // FIXME (BUGFIX-242) : DUPLICATED CODE (IncomesTable): great candidate for refactoring. Have a table that accepts 'url for deletion', 'deletion modal text'

  // NOTE: this modal is for displaying the deletion confirmation popup
  const [showModal, setShowModal] = useState(false)
  const [idToDelete, setidToDelete] = useState(0)

  // NOTE: This dismissable is to display the deletion success/failure to the user
  const [showDismissable, setShowDismissable] = useState(false)
  const [isUpdateSuccess, setIsUpdateSuccess] = useState(false)
  const autoCloseDismissibleAfterMs = 1500

  const handleClose = (): void => {
    setShowModal(false)
    setidToDelete(0)
  }

  const handleDelete = (): void => {
    deleteFunc(idToDelete).then((res: HTTPResponse) => {
      if (res.ok) {
        updateParentData(idToDelete)
        handleClose()
        setIsUpdateSuccess(true)
      } else {
        setIsUpdateSuccess(false)
      }
    }).catch((err) => {
      console.log(err)
      setIsUpdateSuccess(false)
    }).finally(() => {
      setShowDismissable(true)
      const timer = setTimeout(() => {
        setShowDismissable(false)
      }, autoCloseDismissibleAfterMs)
      return () => { clearTimeout(timer) }
    })
  }

  const handleShow = (id: number): void => {
    setShowModal(true)
    setidToDelete(id)
  }

  /**
   * Generates the table header for the Expenses table.
   *
   * @returns The React element representing the table header.
   */
  const generateTableHeader = (): React.ReactElement => {
    return <thead>
      <tr>
         {/* Hide ID row in small screens */}
         <th className="col-1">Category</th>
         <th className="col-1">Amount</th>
         <th className="col-1">Note</th>
         <th className="col-1">Date</th>
         <th className="col-1">Edit</th>
         <th className="col-1">Delete</th>
      </tr>
    </thead>
  }

  /**
   * Generates a summary row with the sum of all expenses.
   *
   * @param expenses - An array of expenses.
   * @returns A React element representing the summary row.
   */
  const generateSummaryRow = (expenses: Expense[]): React.ReactElement => {
    const currencyFormatter = SmartFormatter("JPY")
    return (
    <tr className="col-12">
      <td className="col-1 d-md-table-cell"><b>TOTAL</b></td>
      {/* Handle decimals */}
      <td>{currencyFormatter.format(expenses.reduce((acc, curr) => acc + Number(curr.amount), 0))}</td>
      <td className="col-1 d-md-table-cell"><b></b></td>
      <td className="col-1 d-md-table-cell"><b></b></td>
      <td className="col-1 d-md-table-cell"><b></b></td>
      <td className="col-1 d-md-table-cell"><b></b></td>
    </tr>)
  }

  /**
   * Generates the table body for displaying expenses.
   *
   * @param expenses - An array of Expense objects.
   * @returns A React element representing the table body.
   */
  const generateTableBody = (expenses: Expense[]): React.ReactElement => {
    const currencyFormatter = SmartFormatter("JPY")
    // IMPORTANT: id="expenses-table-body" is used in the functional tests, do not remove it!
    return <tbody className="col-12" id="expenses-table-body">
      {generateSummaryRow(expenses)}
      {expenses.map((expense: Expense) => {
        const url = `/app/expenses/${expense.id}`
        // IMPORTANT: <td id={`exp-${expense.id}`}> is used in the functional tests, do not remove it!
        return <tr className="col-12" key={expense.id} id={`exp-${expense.id}`}>
          <td className="col-1 text-wrap">{expense.category_text}</td>
          <td className="col-1 text-wrap">{ currencyFormatter.format(Number(expense.amount))}</td>
          <td className="col-1 text-truncate">{expense.note}</td>
          <td className="col-1 text-wrap">{expense.date}</td>
          <td className="col-1 text-wrap"><Link to={url}><Button className="btn-success">Edit</Button></Link></td>
          {/* IMPORTANT: <td id={`deleteExpenseButton-${expense.id}`}> is used in the functional tests, do not remove it! */}
          <td className="col-1 text-wrap"><Button id={`deleteExpenseButton-${expense.id}`} onClick={() => { handleShow(expense.id) }} className="btn btn-danger my-2 my-sm-0 triggerModal">Delete</Button></td>
        </tr>
      })}
    </tbody>
  }

  const tableHeader = generateTableHeader()
  const tableBody = generateTableBody(data)
  return <div>
      <Modal show={showModal} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Modal heading</Modal.Title>
        </Modal.Header>
        <Modal.Body>Do you really want to delete expense with id: {idToDelete}? # need to add a way to handle error on deletion here</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Undo
          </Button>
          {/* id="confirmDeletion" is used in the functional tests: leave it there! */}
          <Button variant="primary" id="confirmDeletion" onClick={handleDelete}>
            Confirm deletion
          </Button>
        </Modal.Footer>
      </Modal>
      {showDismissable
        ? <Alert variant={isUpdateSuccess ? "success" : "danger"} onClose={() => { setShowDismissable(false) }} dismissible>
          <Alert.Heading>{isUpdateSuccess ? "Expense deleted successfully!" : "Expense deletion failed :("}</Alert.Heading>
        </Alert>
        : null}
      <div className="table-responsive-sm">
        <table className="table table-sm" >
          { tableHeader }
          { data.length > 0 ? tableBody : null}
        </table>
      </div>
  </div>
}

/**
 * Displays a unstyled ordered list of income items, and their total, with the option to edit or delete them
 *
 * @param {IncomesTableProps} props - The component properties.
 * @param {Array<Income>} props.data - Array of income items.
 * @param {(id: number) => Promise<HTTPResponse>} props.deleteFunc - API function to actually issue a delete request
 * @param {(id: number) => void} props.updateParentData - Function to update parent data after deletion.
 * @param {(message: string) => void} props.displayAlertIntoParent - Function to display an alert message in the parent component.
 * @returns {React.ReactElement} - JSX element representing the income table.
 */
export const IncomesTable: FC<IncomesTableProps> = ({ data, deleteFunc, updateParentData }): ReactElement => {
  // FIXME (BUGFIX-242): DUPLICATED CODE (ExpenseTable) (BUGFIX-242): great candidate for refactoring. Have a table that accepts 'url for deletion', 'deletion modal text'
  // NOTE: this modal is for displaying the deletion confirmation popup
  const [showModal, setShowModal] = useState(false)
  const [idToDelete, setidToDelete] = useState(0)

  // NOTE: This dismissable is to display the deletion success/failure to the user
  const [showDismissable, setShowDismissable] = useState(false)
  const [isUpdateSuccess, setIsUpdateSuccess] = useState(false)
  const autoCloseDismissibleAfterMs = 1500

  const handleClose = (): void => {
    setShowModal(false)
    setidToDelete(0)
  }

  const handleDelete = (): void => {
    deleteFunc(idToDelete).then((res: HTTPResponse) => {
      if (res.ok) {
        updateParentData(idToDelete)
        handleClose()
        setIsUpdateSuccess(true)
      } else {
        setIsUpdateSuccess(false)
      }
    }).catch((err) => {
      console.log(err)
      setIsUpdateSuccess(false)
    }).finally(() => {
      setShowDismissable(true)
      const timer = setTimeout(() => {
        setShowDismissable(false)
      }, autoCloseDismissibleAfterMs)
      return () => { clearTimeout(timer) }
    })
  }

  const handleShow = (id: number): void => {
    setShowModal(true)
    setidToDelete(id)
  }

  /**
   * Generates the table header for the Incomes table.
   *
   * @returns The React element representing the table header.
   */
  const generateTableHeader = (): React.ReactElement => {
    return <thead>
      <tr>
         {/* Hide ID row in small screens */}
         <th className="col-1 d-none d-md-table-cell">ID</th>
         <th className="col-1">Category</th>
         <th className="col-1">Amount</th>
         <th className="col-1">Note</th>
         <th className="col-1">Date</th>
         <th className="col-1">Edit</th>
         <th className="col-1">Delete</th>
      </tr>
    </thead>
  }

  /**
   * Generates a summary row with the sum of all incomes.
   *
   * @param income - An array of incomes.
   * @returns A React element representing the summary row.
   */
  const generateSummaryRow = (incomes: Income[]): React.ReactElement => {
    const currencyFormatter = SmartFormatter("JPY")
    return (
    <tr className="col-12">
      <td className="col-1 d-none d-md-table-cell"><b>-</b></td>
      <td className="col-1 d-md-table-cell"><b>TOTAL</b></td>
      <td>{currencyFormatter.format(incomes.reduce((acc, curr) => acc + curr.amount, 0))}</td>
      <td className="col-1 d-md-table-cell"><b></b></td>
      <td className="col-1 d-md-table-cell"><b></b></td>
      <td className="col-1 d-md-table-cell"><b></b></td>
      <td className="col-1 d-md-table-cell"><b></b></td>
    </tr>)
  }

  /**
   * Generates the table body for displaying Incomes.
   *
   * @param incomes - An array of Expense objects.
   * @returns A React element representing the table body.
   */
  const generateTableBody = (incomes: Income[]): React.ReactElement => {
    const currencyFormatter = SmartFormatter("JPY")
    // IMPORTANT: id="incomes-table-body" is used in the functional tests, do not remove it!
    return <tbody className="col-12" id="incomes-table-body">
      {generateSummaryRow(incomes)}
      {incomes.map((expense: Expense) => {
        const url = `/app/incomes/${expense.id}`
        // IMPORTANT: <td id={`exp-${expense.id}`}> is used in the functional tests, do not remove it!
        return <tr className="col-12" key={expense.id} id={`exp-${expense.id}`}>
          <td className="col-1 d-none d-md-table-cell">{expense.id}</td>
          <td className="col-1 text-wrap">{expense.category_text}</td>
          <td className="col-1 text-wrap">{ currencyFormatter.format(expense.amount)}</td>
          <td className="col-1 text-truncate">{expense.note}</td>
          <td className="col-1 text-wrap">{expense.date}</td>
          <td className="col-1 text-wrap"><Link to={url}><Button className="btn-success">Edit</Button></Link></td>
          {/* IMPORTANT: <td id={`deleteIncomeButton-${expense.id}`}> is used in the functional tests, do not remove it! */}
          <td className="col-1 text-wrap"><Button id={`deleteIncomeButton-${expense.id}`} onClick={() => { handleShow(expense.id) }} className="btn btn-danger my-2 my-sm-0 triggerModal">Delete</Button></td>
        </tr>
      })}
    </tbody>
  }

  const tableHeader = generateTableHeader()
  const tableBody = generateTableBody(data)
  return <div>
      <Modal show={showModal} onHide={handleClose}>
        <Modal.Header closeButton>
          <Modal.Title>Modal heading</Modal.Title>
        </Modal.Header>
        <Modal.Body>Do you really want to delete income with id: {idToDelete}? # need to add a way to handle error on deletion here</Modal.Body>
        <Modal.Footer>
          <Button variant="secondary" onClick={handleClose}>
            Undo
          </Button>
          {/* id="confirmDeletion" is used in the functional tests: leave it there! */}
          <Button variant="primary" id="confirmDeletion" onClick={handleDelete}>
            Confirm deletion
          </Button>
        </Modal.Footer>
      </Modal>
      {showDismissable
        ? <Alert variant={isUpdateSuccess ? "success" : "danger"} onClose={() => { setShowDismissable(false) }} dismissible>
          <Alert.Heading>{isUpdateSuccess ? "Income deleted successfully!" : "Income deletion failed :("}</Alert.Heading>
        </Alert>
        : null}
      <div className="table-responsive-sm">
        <table className="table table-sm" >
          { tableHeader }
          { data.length > 0 ? tableBody : null}
        </table>
      </div>
  </div>
}

/**
 * Displays the balances, and their total, in an unstyled unordered list
 *
 * @param {BalanceTableProps} props - The component properties.
 * @param {Array<{ id: number, category_text: string, amount: number }>} props.data - Array of items with category, ID, and amounts.
 * @returns {React.ReactElement} - JSX element representing the balance table.
 */
export const BalanceTable: FC<BalanceTableProps> = ({ data }): ReactElement => {
  const list = data.map(item => {
    return (
      <li key={item.id}>
        {item.category_text}: {Formatter.format(item.amount)}
      </li>
    )
  })
  const total = data.reduce((acc, curr) => acc + curr.amount, 0)
  return <React.Fragment>
    <div>Total: {Formatter.format(total)}</div>
    <ul>{list}</ul>
  </React.Fragment>
}

/**
 * Displays a table of obligations with corresponding information.
 *
 * @param {ObligationsTableProps} props - The component properties.
 * @param {Array<Obligation>} props.data - Array of obligation items.
 * @param {string} props.id - The ID to assign to the table. Used to speed up functional tests.
 * @param {(id: number) => Promise<DeletedObligationResponse>} props.deleteOblFunc - API function to actually issue a delete request
 * @param {(id: number) => void} props.deletePaymentFunc - API function to actually issue a delete request for a payment change
 * @param {(id: number) => void} props.updateParentData - Function to update parent data after deletion.
 * @param {{id: number) => void} props.updateParentPaymentChangeData - Function to update parent data's nested payments after deletion.}
 * @returns {React.ReactElement} - JSX element representing the obligations table.
 */
export const ObligationsTable: FC<ObligationsTableProps> = ({ id, obligations, deleteOblFunc, deletePaymentFunc, updateParentData, updateParentPaymentChangeData }: ObligationsTableProps): ReactElement => {
  // NOTE: this modal is used to display the Obligation deletion confirmation
  const [showModal, setShowModal] = useState(false)
  // Since we have two types of entities, we need to know which one we are deleting
  const [entityName, setEntityName] = useState("Obligation")
  const [idToDelete, setidToDelete] = useState(0)

  // NOTE: This dismissable is to display the deletion success/failure to the user
  const [showDismissable, setShowDismissable] = useState(false)
  const [isUpdateSuccess, setIsUpdateSuccess] = useState(false)
  const autoCloseDismissibleAfterMs = 1500

  const handleClose = (): void => {
    setShowModal(false)
    setidToDelete(0)
  }

  const handleDelete = (): void => {
    const execDeleteFunc = entityName === "Obligation" ? deleteOblFunc : deletePaymentFunc
    const handleParent = entityName === "Obligation" ? updateParentData : updateParentPaymentChangeData
    execDeleteFunc(idToDelete).then((res: DeletedObligationResponse | HTTPResponseWithMessage) => {
      if (res.response.ok) {
        handleParent(idToDelete)
        handleClose()
        setIsUpdateSuccess(true)
      } else {
        setIsUpdateSuccess(false)
      }
    }).catch((err) => {
      console.log(err)
      setIsUpdateSuccess(false)
    }).finally(() => {
      setShowDismissable(true)
      const timer = setTimeout(() => {
        setShowDismissable(false)
      }, autoCloseDismissibleAfterMs)
      return () => { clearTimeout(timer) }
    })
  }

  const handleShow = (id: number, text: string): void => {
    setShowModal(true)
    setEntityName(text)
    setidToDelete(id)
  }

  const currencyFormatter = SmartFormatter("JPY")

  const obligationTable = <table className="table table-hover table-sm" id={id}>
    <thead>
      <tr>
        <th scope="col">ID</th>
        <th scope="col">Name</th>
        <th scope="col">Amount</th>
        <th scope="col">Type</th>
        <th scope="col">Monthly Payment</th>
        <th scope="col">Start Date</th>
        <th scope="col">End Date</th>
        <th scope="col">Payment Day</th>
        <th scope="col">Edit</th>
        <th scope="col">Delete</th>
      </tr>
    </thead>
    <tbody>
      {obligations.map((obligation: Obligation) => {
        const url = `/app/obligations/${obligation.id}`
        // IMPORTANT: <td id={`ob-${obligation.id}`}> is used in the functional tests, do not remove it!
        return <tr key={obligation.id} id={`ob-${obligation.id}`}>
          <td>{obligation.id}</td>
          <td>{obligation.name}</td>
          {/* NOTE: Since total amount is a Decimal on the backend, it is being returned as a string here <=== */}
          {obligation.obligation_type !== "variable" ? <td>{currencyFormatter.format(obligation.total_amount)}</td> : null}
          {obligation.obligation_type === "variable" && (
            <td>
              <table className="table table-hover table-lg">
                <tbody>
                  <tr style={{ backgroundColor: "transparent", border: "none" }}>
                    <td colSpan={10} style={{ borderTop: "none", backgroundColor: "transparent", border: "none" }}>
                      <table className="table table-sm">
                        <thead style={{borderTop: "none" }}>
                          <tr>
                            <th>Change ID</th>
                            <th>New Monthly Payment</th>
                            <th>Change Date</th>
                            <th>Edit</th>
                            <th>Delete</th>
                          </tr>
                        </thead>
                        <tbody>
                          {obligation?.changes?.map((payment: ObligationPaymentChange) => {
                            const changeUrl = `/app/obligations/payment-change/${payment.id}`
                            return (<tr key={payment.id}>
                              <td>{payment.id}</td>
                              <td>{payment.new_monthly_payment}</td>
                              <td>{payment.change_date}</td>
                              <td><Link to={changeUrl}><Button className="btn-success btn-sm">Edit</Button></Link></td>
                              <td>
                                <Button className="btn-danger  btn-sm" onClick={() => { handleShow(payment.id, "PaymentChange") }}>Delete</Button>
                              </td>
                            </tr>)
                          })}
                        </tbody>
                      </table>
                    </td>
                  </tr>
                </tbody>
              </table>
            </td>
          )}
          <td>{obligation.obligation_type}</td>
          <td>{obligation.start_date}</td>
          <td>{obligation.end_date}</td>
          <td>{currencyFormatter.format(obligation.monthly_payment)}</td>
          <td>{obligation.payment_day}</td>
          <td><Link to={url}><Button className="btn-success">Edit</Button></Link></td>
          <td>
            <Button className="btn-danger" onClick={() => { handleShow(obligation.id, "Obligation") }}>Delete</Button>
          </td>
        </tr>
      })}
    </tbody>
  </table>

  // FIXME: export this React to a subcomponent and use it for "Table"
  return (
    <React.Fragment>
        {/* Obligation modal */}
        <Modal show={showModal} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Modal heading</Modal.Title>
          </Modal.Header>
          {/* TODO: need to add a way to handle error on deletion here */}
          <Modal.Body>Do you really want to delete {entityName} with id: {idToDelete}?</Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Undo
            </Button>
            <Button variant="primary" onClick={handleDelete}>
              Confirm deletion
            </Button>
          </Modal.Footer>
        </Modal>
        {/* FIXME: when deleting the last item in the list, the Dismissable is not displayed at all */}
        {showDismissable
          ? <Alert variant={isUpdateSuccess ? "success" : "danger"} onClose={() => { setShowDismissable(false) }} dismissible>
            <Alert.Heading>{isUpdateSuccess ? `${entityName} deleted successfully!` : `${entityName} deletion failed :(` }</Alert.Heading>
          </Alert>
          : null}
        { obligations.length > 0 ? <ul>{obligationTable}</ul> : <React.Fragment>Your fixed Obligations will appear here after you create any!</React.Fragment>}
    </React.Fragment>
  )
}

/**
 * Displays aggregate (i.e. reduced) expenses, and their total, in an unstyled unordered list
 *
 * @param {ReducedTableProps} props - The component properties.
 * @param {Array<{ label: string, amount: number }>} props.data - Array of items with labels and amounts.
 * @returns {React.ReactElement} - JSX element representing the reduced table.
 */
export const ReducedTable: FC<ReducedTableProps> = ({ data }): ReactElement => {
  const list = data.map((item) => {
    return (
      <li key={item.label}>{item.label} : {Formatter.format(item.amount)}</li>
    )
  })
  const total = data.reduce((acc, curr) => acc + curr.amount, 0)
  return <React.Fragment>
    <div>Total: {Formatter.format(total)}</div>
    <ul>{list}</ul>
  </React.Fragment>
}

/**
 * Displays the accounts in an unstyled unordered list
 *
 * @param {AccountTableProps} props - The component properties.
 * @param {Account[]} props.accounts - Array of account data.
 * @returns {React.ReactElement} - JSX element representing the account table.
 */
export const AccountTable: FC<AccountTableProps> = ({ data }): ReactElement => {
  /**
   * Generates the table header for the Account component
   *
   * @returns The React element representing the table header.
   */
  const generateTableHeader = (): React.ReactElement => {
    return <thead>
      <tr>
        {/* Hide ID row in small screens */}
        <th className="col-1 d-none d-md-table-cell">ID</th>
        <th className="col-1">Account Text</th>
        <th className="col-1">Credit Entity</th>
        <th className="col-1">Foreign Currency?</th>
      </tr>
    </thead>
  }

  /**
   * Generates the table body for displaying accounts.
   *
   * @param accounts - An array of Account objects.
   * @returns A React element representing the table body.
   */
  const generateTableBody = (accounts: Account[]): React.ReactElement => {
    // IMPORTANT: id="accounts-table-body" is used in the functional tests, do not remove it!
    return <tbody className="col-12" id="accounts-table-body">
      {accounts.map((account: Account) => {
        // IMPORTANT: <td id={`acc-${account.id}`}> is used in the functional tests, do not remove it!
        return <tr className="col-12" key={account.id} id={`acc-${account.id}`}>
          <td className="col-1 d-none d-md-table-cell">{account.id}</td>
          <td className="col-1 text-wrap">{account.text}</td>
          <td className="col-1 text-wrap">{account.credit_entity}</td>
          <td className="col-1 text-wrap">{account.is_foreign_currency ? "Yes" : "No"}</td>
        </tr>
      })}
    </tbody>
  }

  const tableHeader = generateTableHeader()
  const tableBody = generateTableBody(data)
  return <div>
    <div className="table-responsive-sm">
      <table className="table table-sm" >
        { tableHeader }
        { data.length > 0 ? tableBody : null}
      </table>
    </div>
  </div>
}

/**
 * Displays the goals in an unstyled unordered list
 *
 * @param {GoalTableProps} props - The component properties.
 * @param {Goal[]} props.data - Array of goal data.
 * @param {function} props.deleteFunc - Function to delete a goal.
 * @param {function} props.updateParentData - Function to update parent data after deletion.
 * @param {function} props.navigate - Navigation function.
 * @returns {React.ReactElement} - JSX element representing the goal table.
 */
export const GoalTable: FC<GoalTableProps> = ({ data, deleteFunc, updateParentData }, navigate): ReactElement => {
  // NOTE: this modal is used to display the goal deletion confirmation
  const [showModal, setShowModal] = useState(false)
  const [idToDelete, setidToDelete] = useState(0)

  // NOTE: This dismissable is to display the deletion success/failure to the user
  const [showDismissable, setShowDismissable] = useState(false)
  const [isUpdateSuccess, setIsUpdateSuccess] = useState(false)
  const autoCloseDismissibleAfterMs = 1500

  const handleClose = (): void => {
    setShowModal(false)
    setidToDelete(0)
  }

  const handleDelete = (): void => {
    deleteFunc(idToDelete).then(res => {
      if (res.ok) {
        updateParentData(idToDelete)
        handleClose()
        setIsUpdateSuccess(true)
      } else {
        setIsUpdateSuccess(false)
      }
    }).catch((err) => {
      console.log(err)
      setIsUpdateSuccess(false)
    }).finally(() => {
      setShowDismissable(true)
      const timer = setTimeout(() => {
        setShowDismissable(false)
      }, autoCloseDismissibleAfterMs)
      return () => { clearTimeout(timer) }
    })
  }

  const handleShow = (id: number): void => {
    setShowModal(true)
    setidToDelete(id)
  }

  /**
   * Generates the table header for the goal list.
   *
   * @returns The React element representing the goal list table header.
   */
  const generateTableHeader = (): React.ReactElement => {
    return <thead>
      <tr>
         {/* Hide ID row in small screens */}
         <th className="col-1 d-none d-md-table-cell">ID</th>
         <th className="col-1">Text</th>
         <th className="col-1">Amount</th>
         <th className="col-1">Note</th>
         <th className="col-1">Is Archived?</th>
         <th className="col-1">Edit</th>
         <th className="col-1">Delete</th>
      </tr>
    </thead>
  }

  /**
   * Generates the table body for displaying goals.
   *
   * @param goals - An array of Goal objects.
   * @returns A React element representing the table body.
   */
  const generateTableBody = (goals: Goal[]): React.ReactElement => {
    const currencyFormatter = SmartFormatter("JPY")
    // IMPORTANT: id="goals-table-body" is used in the functional tests, do not remove it!
    return <tbody className="col-12" id="goals-table-body">
      {goals.map((goal: Goal) => {
        const isArchived = goal.is_archived ? "Yes" : "No"
        const url = `/app/goals/${goal.id}`
        // Mark archived goals to make them stand out
        let classForRow = "col-12"
        if (isArchived === "Yes") {
          classForRow = "col-12 bg-secondary"
        }
        // IMPORTANT: <td id={`exp-${goal.id}`}> is used in the functional tests, do not remove it!
        return <tr className={classForRow} key={goal.id} id={`exp-${goal.id}`}>
          <td className="col-1 d-none d-md-table-cell">{goal.id}</td>
          <td className="col-1 text-wrap">{goal.text}</td>
          <td className="col-1 text-wrap">{ currencyFormatter.format(goal.amount)}</td>
          <td className="col-1 text-truncate">{goal.note}</td>
          <td className="col-1 text-wrap">{isArchived}</td>
          <td className="col-1 text-wrap">
            <Link to={url}><Button className="btn-success">Edit</Button></Link>
            </td>
          {/* IMPORTANT: <td id={`deleteExpenseButton-${expense.id}`}> is used in the functional tests, do not remove it! */}
          <td className="col-1 text-wrap">
            <Button id={`deleteExpenseButton-${goal.id}`}
              onClick={() => { handleShow(goal.id) }}
              className="btn btn-danger my-2 my-sm-0 triggerModal">Delete</Button>
          </td>
        </tr>
      })}
    </tbody>
  }

  const tableHeader = generateTableHeader()
  const tableBody = generateTableBody(data)
  return (
    <div>
        <Modal show={showModal} onHide={handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Modal heading</Modal.Title>
          </Modal.Header>
          <Modal.Body>Do you really want to delete goal with id: {idToDelete}?<br></br><small> # need to add a way to handle error on deletion here</small></Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={handleClose}>
              Undo
            </Button>
            <Button variant="primary" onClick={handleDelete}>
              Confirm deletion
            </Button>
          </Modal.Footer>
        </Modal>
        {showDismissable
          ? <Alert variant={isUpdateSuccess ? "success" : "danger"} onClose={() => { setShowDismissable(false) }} dismissible>
            <Alert.Heading>{isUpdateSuccess ? "Goal deleted successfully!" : "Goal deletion failed :("}</Alert.Heading>
          </Alert>
          : null}
        <div className="table-responsive-sm">
          <table className="table table-sm" >
            { tableHeader }
            { data.length > 0 ? tableBody : null}
          </table>
        </div>
    </div>
  )
}

/**
 * Displays a bootrstap card with the budgets information
 *
 * @param {MonthlyBalanceTableProps} props - The component properties.
 * @param {BudgetWithNestedCategory[]} props.data - Array of budget data.
 * @param {Record<number, number>} props.expensesData - Expenses data.
 * @param {function} props.navigate - Navigation function.
 * @returns {React.ReactElement} - JSX element representing the budgets table.
 */
export const BudgetsTable: FC<MonthlyBalanceTableProps> = ({ data, expensesData }, navigate): React.ReactElement => { // <=== is MonthlyBalanceTablePropscorrect?
  const currencyFormatter = SmartFormatter("JPY")

  const formatCurrency = (amount: number): string => currencyFormatter.format(amount)

  const calculateUsage = (spent: number, budgeted: number): number => {
    if (spent === undefined || isNaN(spent) || budgeted === 0) {
      return 0
    }
    // return (spent / budgeted) * 100
    return parseFloat((spent / budgeted * 100).toFixed(2))
  }

  /**
   * Determines the Bootstrap ProgressBar color based on the budget usage percentage.
   *
   * @param {number} usage - The percentage of budget usage.
   * @returns {string} - The Bootstrap ProgressBar variant: "danger" for overspending,
   *   "warning" for approaching budget cap, "info" for correctly budgeted (100%), and
   *   "success" for a safe zone (indicative, unaware of the budgeted period's position).
   */
  const getProgressBarState = (usage: number): string => {
    if (usage > 100) {
      // Overspending is bad
      return "danger"
    } else if (usage === 100) {
      // 100% means the user has correctly budgeted, no need to trigger any alarm
      return "success"
    } else if (usage > 90) {
      // Approaching the cap of the user's budget, slow down the spending!
      return "warning"
    }
    // Safe zone, the user is likely on track with their budget
    // (NOTE: This bar is unaware of the budgeted period's position, consider it indicative)
    return "info"
  }

  const renderProgressBar = (spent: number, budgeted: number): React.ReactNode | null => {
    if (spent === undefined || isNaN(spent)) {
      return null
    }

    const usage = calculateUsage(spent, budgeted)

    return <ProgressBar now={usage} label={`${usage}%`} variant={getProgressBarState(usage)} />
  }

  const renderBudgetItem = (bdgt: BudgetWithCategory): React.ReactNode => {
    const catName = bdgt?.category !== null && bdgt?.category !== undefined ? bdgt.category.text : bdgt?.savingcategory?.text
    const url = getUrl(bdgt)
    const budgeted = formatCurrency(parseFloat(bdgt.amount))
    const spent = getSpent(bdgt)

    // TODO: change background color for saving budgets

    return (
      <div key={bdgt.id} className="col-md-3 mb-3 col-lg-3">
        <div className="card">
          <div className="card-body text-center">
            <h5 className="card-title"><Link to={url}>{catName}</Link></h5>
            <div className="card-text">
              {(spent === undefined || isNaN(spent)) ? <Spinner animation="border" size="sm" variant="success" /> : `${formatCurrency(spent)}/${budgeted}`}
            </div>
            {renderProgressBar(spent, parseFloat(bdgt?.amount))}
          </div>
        </div>
      </div>
    )
  }

  const getUrl = (bdgt: BudgetWithCategory): string => {
    if (bdgt?.category?.id !== undefined && !isNaN(bdgt?.category?.id)) {
      return `/app/budgets/edit/${bdgt.id}`
    }
    // TODO: we are not handling the case where the budget is malformed, see getSpent()
    return `/app/budgets/saving/edit/${bdgt.id}`
  }

  const getSpent = (bdgt: BudgetWithCategory): number | undefined => {
    if (bdgt?.category?.id !== undefined && !isNaN(bdgt?.category?.id)) {
      return expensesData[bdgt?.category?.id] !== undefined ? expensesData[bdgt?.category?.id] : undefined
    }
    if (bdgt?.savingcategory?.id !== undefined && !isNaN(bdgt?.savingcategory?.id) && bdgt?.allocated_amount !== undefined && bdgt?.allocated_amount !== null) {
      return bdgt?.allocated_amount
    }
    // TODO: after returningn 0 we should let the UI know to show some kind of werror, as this budgets is malformed
    return 0
  }

  const isDatLoaded = (): boolean => {
    const spendingBudgets = data.filter(bdgt => bdgt?.category?.id !== undefined && !isNaN(bdgt?.category?.id))
    // If we only have spending budgets, we only need to check for expense data length to be the same as data
    //  i.e. we have 10 budgets and 10 expenses data point.
    // Note this does not account for possilbe failed expenses retrieve calls
    // TODO (implement a re-trey , or return a null value on failure, and handle it in the UI)
    if (spendingBudgets.length > 0) {
      return Object.keys(expensesData).length === spendingBudgets.length
    }
    // If we only have saving budgets, then we already have the allocated amount, as it is included in the budget object itself
    // and we do not need extra API calls
    return true
  }

  const renderTotal = (): React.ReactNode => {
    const totalBudgeted = data.reduce((acc, bdgt) => acc + parseFloat(bdgt.amount), 0)

    const totalSpent = data.reduce((acc, bdgt) => {
      const spent = getSpent(bdgt)
      return acc + spent
    }, 0)

    const totalUsage = calculateUsage(totalSpent, totalBudgeted)
    return (
      <div key="budgets_total" className="col-md-3 mb-3 col-lg-12">
        <div className="card total">
          <div className="card-body text-center">
            <h5 className="card-title">Total</h5>
            <div className="card-text">
              {/* FIXME if we only have 1 savingmcategory budget this spinner will be displayed forever */}
              { isDatLoaded() ? `${formatCurrency(totalSpent)}/${formatCurrency(totalBudgeted)}` :  <Spinner animation="border" size="sm" variant="success" className="mb-1" />  }
            </div>
            <ProgressBar now={totalUsage} label={`${totalUsage}%`} variant={getProgressBarState(totalUsage)} />
          </div>
        </div>
      </div>
    )
  }

  return (
    // NOTE: This "budgetsContainer" id is used by the functional tests, do not remove it!
    <div className="container col-sm-12 col-md-12 col-lg-8 my-2" id="budgetsContainer">
      <div className="row">
        {data.map(renderBudgetItem)}
        {renderTotal()}
      </div>
    </div>
  )
}

/**
 * Displays a bootstrap card with yearly aggregated dividends statistics
 *
 * @param {YearlyAggregateDividend[]} aggregate - Array of yearly-aggregated dividend data.
 * @returns {React.ReactNode} - JSX element representing the formatted yearly aggregated dividends statistics table.
 */
export const formatDividendsYearlyStatsTable = (aggregate: YearlyAggregateDividend[]): React.ReactNode => {
  const aggregatesList = aggregate.map((item: YearlyAggregateDividend) => {
    // NOTE: atm (2023-10-02) we only serve local_currency (i.e. JPY) aggregate data
    const currencyFormatter = SmartFormatter("JPY")
    let year: string | number = item.year
    let keyName = "aggregate" + year
    if (item.year === -1) {
      year = "Total" // The horror, we assign a string to a vaiable that can hold numbers as well
      keyName = "totalAllCurrencies"
    }
    // FIXME: the key does not seems to be set correctly (?)
    return (
      <tr key={keyName}>
        {/* NOTE: ATM We store assets bought in local currency with a foreign_amount of 0 */}
        <td>{year}</td>
        <td>{currencyFormatter.format(item.local_total)}</td>
        <td>{currencyFormatter.format(item.local_per_day)}</td>
      </tr>
    )
  })

  return (
    <div className="container">
      <table className="table">
        <thead>
          <tr>
            <th>Year</th>
            <th>Total</th>
            <th>Per Day</th>
          </tr>
        </thead>
        <tbody>
          {aggregatesList}
        </tbody>
      </table>
    </div>
  )
}

/**
 * Displays a bootstrap card with aggregated dividends
 *
 * @param {SymbolAggregateDividend[]} aggregate - Array of symbol-aggregated dividend data.
 * @returns {React.ReactNode} - JSX element representing the formatted aggregated dividend table.
 */
export const formatDividendAggregateTable = (aggregate: SymbolAggregateDividend[]): React.ReactNode => {
  const jpyFormatter = SmartFormatter("JPY")
  const usdFormatter = SmartFormatter("USD")

  const aggregateList = aggregate.map((item: SymbolAggregateDividend) => {
    const liName = item.asset_text + "_aggregate_symbol"
    return (
      <tr key={liName}>
        <td className="w-50">{item.asset_text}</td>
        <td>{item.total_foreign_amount > 0 ? usdFormatter.format(item.total_foreign_amount) : jpyFormatter.format(item.total_local_amount)}</td>
      </tr>
    )
  })

  return (
    <div className="container">
      <table className="table">
        <thead>
          <tr>
            <th className="w-50">Asset</th>
            <th>Total Amount</th>
          </tr>
        </thead>
        <tbody>
          {aggregateList}
        </tbody>
      </table>
    </div>
  )
}

/**
 * Displays a bootstrap card with a list of dividends with corresponding information.
 *
 * @param {Dividend[]} data - Array of dividend data.
 * @returns {React.ReactNode} - JSX element representing the formatted list of dividend details.
 */
export const formatDividendsDetails = (data: Dividend[]): React.ReactNode => {
  const rows = data.map((item: Dividend): React.ReactNode => {
    const currencyFormatter = SmartFormatter(item.currency)
    return (
      <tr key={item.id}>
        <td className="w-25">{item.asset_text}</td>
        <td>{item.record_date}</td>
        <td>{item.foreign_amount === 0 ? currencyFormatter.format(item.local_amount) : currencyFormatter.format(item.foreign_amount)}</td>
      </tr>
    )
  })

  return (
    <div className="table-responsive">
      <table className="table table-striped">
        <thead>
          <tr>
            <th className="w-25">Asset</th>
            <th>Record Date</th>
            <th>Amount</th>
          </tr>
        </thead>
        <tbody>
          {rows}
        </tbody>
      </table>
    </div>
  )
}
/**
 * Displays a table of assets with corresponding information.
 *
 * @param {AssetsTableProps} props - Props containing 'data' (array of assets), 'isAggregate', 'forceYenDecimals'.
 * @returns {React.ReactElement} - JSX element for displaying the asset table.
 */
export const assetToTableRow = (item: any, totalCurrentForeign: number, isAggregate: boolean, forceYenDecimals: boolean = false, decimalFigures: number = 3, hideTotalCurrency: boolean = false, getSelectedItems = null, toggleSelectItem = null): ReactElement => {
  const portfolioPerc = (item.current_local_value * 100 / totalCurrentForeign).toFixed(decimalFigures)
  const percString = <td>{portfolioPerc}%</td>
  const foreignFormatter = SmartFormatter(item.foreign_currency, forceYenDecimals)
  const numberFormat = new Intl.NumberFormat()
  let checkbox = null
  if (!isAggregate && getSelectedItems !== null && toggleSelectItem !== null) {
    checkbox = <input type="checkbox" name={`delete_${item.id}`} onChange={e => {
      toggleSelectItem(item.id)
    }} checked={getSelectedItems().includes(item.id)}/>
  }
  return (
    // IMPORTANT: "asset" class name is used by functional tests, do not change it
    <tr key={item.id} className="asset">

        {/* Delete checkbox (or null if this is the 'Total' row) */}
        <td>
          { checkbox }
        </td>
        <td>{item.text}</td>
        {isAggregate ? percString : <td></td>}
        <td>{item.amount !== "-" ? numberFormat.format(item.amount) : item.amount}</td>

        { item.foreign_currency === "JPY"
          ? <td>{foreignFormatter.format(item.bought_price_local)}</td>
          : <td>{foreignFormatter.format(item.bought_price_foreign)} ({Formatter.format(item.bought_price_local.toFixed(decimalFigures))})</td>
        }

        { item.foreign_currency === "JPY"
          ? <td>{foreignFormatter.format(item.current_local_value)}</td>
          : <td>{ foreignFormatter.format(item.current_foreign_value)} ({Formatter.format(item.current_local_value.toFixed(decimalFigures))})</td>
        }

        { item.foreign_currency === "JPY"
          ? <td className={`${item.net_loss_foreign >= 0 && item.net_loss_local >= 0 ? "table-success" : "table-danger"}`}>{foreignFormatter.format(item.net_loss_local)}</td>
          : <td className={`${item.net_loss_foreign >= 0 && item.net_loss_local >= 0 ? "table-success" : "table-danger"}`}>{foreignFormatter.format(item.net_loss_foreign)} ({Formatter.format(item.net_loss_local.toFixed(decimalFigures))})</td>
        }

        <td>{item.account.text}</td>
        <td>{hideTotalCurrency ? "-" : item.foreign_currency}</td>
        <td>{item.record_date}</td>
     </tr>
  )
}

/**
 * Displays a table of assets with corresponding information.
 *
 * @param {AssetsTableProps} props - Props containing 'data' (array of assets), 'isAggregate', 'forceYenDecimals'.
 * @returns {React.ReactElement} - JSX element for displaying the asset table.
 */
export const AssetTable: FC<AssetsTableProps> = ({ data, isAggregate, forceYenDecimals, getSelectedItems, toggleSelectAll, toggleSelectItem }): ReactElement => {
  const totalCurrentForeign = data.map((v: any) => v.current_local_value).reduce(accumulator, 0)
  const list = data.map((item: Asset) => assetToTableRow(item, totalCurrentForeign, isAggregate, forceYenDecimals, 3, false, getSelectedItems, toggleSelectItem))
  const totalRow = assetToTableRow(reduceAssets(data), totalCurrentForeign, isAggregate, false, 0, false, null, null)
  const percentageHeader = <th scope="col">Portfolio percentage</th>
  const deleteHeader = <input type="checkbox" name={`delete_all`} onChange={e => {
    toggleSelectAll()
  }} checked={getSelectedItems().length === data.length && data.length > 0}/>
  // IMPORTANT: id="assets-table" is used by functional tests. DO NOT CHANGE IT
  return <div className="table-responsive" id="assets-table">
    <table className="table table-hover table-sm table-striped">
      <thead>
        <tr>
          {!isAggregate ? <th>{deleteHeader}</th> : <th></th> }
          <th scope="col">Asset</th>
          {isAggregate ? percentageHeader : <th></th> }
          <th scope="col">Amount</th>
          <th scope="col">Bought Foreign (Local)</th>
          <th scope="col">Curr. Foreign value (Local)</th>
          <th scope="col">Net/Loss Foreign (Local)</th>
          <th scope="col">Account</th>
          <th scope="col">Currency</th>
          <th scope="col">Record date</th>
        </tr>
      </thead>
      <tbody>
        { totalRow }
        { list }
      </tbody>
    </table>
   </div>
}

/**
 * Displays a bootstrap card with a list of spending limits with corresponding information.
 *
 * @param {object} props - Props containing 'data' (array of limits) and 'missingDatGuidance' (function to render guidance when data is missing).
 * @returns {React.ReactElement} - JSX element for displaying the list of spending limits.
 */
export const LimitTable = ({ data, missingDatGuidance}): ReactElement => {
  // Display guidance only after the loading has completed. <===
  // TODO, take another parameter: has api call completed
  // if not, display a spinner instead (or do it inside the parent component?)
  if (data?.length === 0) {
    return missingDatGuidance()
  }
  const list = data.map((limit: any) => {
    const foreignFormatter = SmartFormatter(limit.currency)
    const isOverSpent = limit.spent > limit.amount
    return (
      <li key={limit.id} className={ isOverSpent ? "text-danger list-group-item" : "text-success list-group-item"}>
        Amount spent in {limit.text} in {limit.category_text} the last {limit.days} day(s) : {foreignFormatter.format(limit.spent)}/{foreignFormatter.format(limit.amount)}
      </li>
    )
  })
  return list
}

/**
 * Displays a custom spinner for indicating loading.
 *
 * @returns {React.ReactElement} - JSX element for displaying the custom spinner.
 */
export const CustomSpinner = (): ReactElement => {
  return (
      <div className="text-primary text-center" role="status">
        <Spinner animation="border" variant="success"/>
        <span className="sr-only">Loading...</span>
      </div>
  )
}

/**
 * Displays an unrecoverable error banner.
 *
 * @returns {React.ReactElement} - JSX element for displaying the unrecoverable error banner.
 */
export const FailedBanner = (): ReactElement => {
  return <Alert variant="danger">
    <Alert.Heading>An unrecoverable error has occourred. Please reload the page, or try again later!</Alert.Heading>
  </Alert>
}

/**
 * Displays a non dismissible error banner.
 *
 * @returns {React.ReactElement} - JSX element for displaying the dismissible error banner.
 */
export const RecoverableErrorBanner = (): ReactElement => {
  return <Alert variant="danger">
    <Alert.Heading>An error has accourred. Please try again.</Alert.Heading>
  </Alert>
}

/**
 * Displays a dismissible error banner.
 *
 * @param {SetCriticalErrorFunction} setCriticalError - Function to dismiss the banner.
 * @returns {React.ReactElement} - JSX element for displaying the dismissible error banner.
 */
export const DismissableRecoverableErrorBanner = (setCriticalError: SetCriticalErrorFunction): ReactElement => {
  return <Alert variant="danger" dismissible onClose={() => {setCriticalError(false)}}>
    <Alert.Heading>An error has accourred. Please try again.</Alert.Heading>
  </Alert>
}

/**
 * Styles error messages
 *
 * @param {string | string[]} error - The error message or an array of error messages.
 * @returns {React.ReactElement} - JSX element for displaying styled error messages.
 */
export const styleErrorMessage = (error: string | string[]): ReactElement => {
  // TODO: <====== this should be a p ? // also check functional tests checking for errors
  if (Array.isArray(error)) {
    return (
      <span className="text-danger">
        {error.map((errorMsg, index) => (
          <span key={index}>{errorMsg}</span>
        ))}
      </span>
    )
  } else {
    return <span className="text-danger help-inline">{error}</span>
  }
}

/**
 * Styles error messages with an inline format
 *
 * @param {string | string[]} error - The error message or an array of error messages.
 * @returns {React.ReactElement} - JSX element for displaying error messages.
 */
export const InlinestyleErrorMessage = (error: string | string[]): ReactElement => {
  if (Array.isArray(error)) {
    return (
      <span className="controls text-danger px-1">
        {error.map((errorMsg, index) => (
          <span key={index}>{errorMsg}</span>
        ))}
      </span>
    )
  } else {
    return <div className="form-group"><span className="text-danger px-1">{error}</span></div>
  }
}

export const displayCategoriesPullDown = (categories: Category[], register: (rules?: RegisterOptions) => (instance: HTMLInputElement | null) => void): JSX.Element => {
  if (categories.length > 0) {
    return <select id="category_field" name="category" ref={register({ required: "This field is required." })}>
            {categories.map(value => (
              <option key={value.id} value={value.id}>
                {value.text}
              </option>)
            )}
           </select>
  } else {
    return CustomSpinner()
  }
}

export const createCategoryGuidance = (): JSX.Element => {
  return <div>
    <h5 className="text-info"> You need to create a Category first! </h5>
    <Link className="btn btn-info mt-2 " to={"/app/categories/creation"}>Create a Category</Link>
  </div>
}

export const createIncomeCategoryGuidance = (): JSX.Element => {
  return <div>
    <h5 className="text-info"> You need to create an Income Category first! </h5>
    <Link className="btn btn-info mt-2 " to={"/app/income-categories/creation"}>Create an Income Category</Link>
  </div>
}

export const createAccountGuidance = (): JSX.Element => {
  return <div>
         <h5 className="text-info">You need to create an Account first! </h5>
         <Link className="btn btn-info mt-2 " to={"/app/accounts/creation"}>Create an Account</Link>
      </div>
}

export const createSavingCategoryGuidance = (): JSX.Element => {
  return <div>
    <h5 className="text-info">Your Saving Categories will appear here after you create any!</h5>
    <Link className="btn btn-info mt-2 " to={"/app/saving-categories/creation"}>Create a Saving Category</Link>
  </div>
}

export const createMonthlyBalanceCategoryGuidance = (): JSX.Element => {
  return <div>
    <h5 className="text-info"> You need to create a Monthly Balance Category first! </h5>
    <Link className="btn btn-info mt-2 " to={"/app/monthly-balances/categories/creation"}>Create a Monthly Balance Category</Link>
  </div>
}

export const createSpendingLimitGuidance = (): JSX.Element => {
  return <div>
    <h5 className="text-info">Your Spending Limits will appear here after you create any!</h5>
    <Link className="btn btn-info mt-2 " to={"/app/spending-limits/creation"}>Create a Spending Limit</Link>
  </div>
}

export const createAssetsGuidance = (): JSX.Element => {
  const dateYYmm = `${new Date().getFullYear()}-${String(new Date().getMonth() + 1).padStart(2, "0")}`
  return <div>
          <h5 className="text-info">You need to create some Monthly Balance or Assets first! </h5>
          <div><Link className="btn btn-info mt-2 " to={"/app/assets/import"}>Import Assets data</Link></div>
          <div><Link className="btn btn-info mt-2 " to={`/app/monthly-balances/multiple-creation/date/${dateYYmm}`}>Create Monthly Balances</Link></div>
      </div>
}

export const displayCreateDataGuidance = (): React.ReactNode => {
  return <div>
          <h5 className="text-info">You need to create some Dividends first! </h5>
          <Link className="btn btn-info mt-2 " to={"/app/dividends/import"}>Import Dividends</Link>
      </div>
}
