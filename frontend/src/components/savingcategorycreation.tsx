import { useForm } from "react-hook-form"
import Alert from "react-bootstrap/Alert"
import React, { useState, useCallback } from "react"
import { createSavingCategory } from "../common/api_savingcategories"
import type { SavingCategoryFormInputs, ReactFragment } from "../common/interfaces"
import { styleErrorMessage } from "./shared"

const defaultValues: SavingCategoryFormInputs = {
  text: ""
}

export const CreateSavingCategory = (): JSX.Element => {
  // eslint-disable-next-line @typescript-eslint/unbound-method
  const { register, handleSubmit, setError, formState: { errors }, reset } = useForm<SavingCategoryFormInputs>()
  const [isSending, setIsSending] = useState(false)
  const [showDismissable, setShowDismissable] = useState(false)
  const [isUpdateSuccess, setIsUpdateSuccess] = useState(false)
  const autoCloseDismissibleAfterMs = 1500

  const [genericFormError, setGenericFormError] = useState("")

  const onSubmit = useCallback((formValues: SavingCategoryFormInputs) => {
    if (isSending) return
    setShowDismissable(false)
    setIsSending(true)
    const abortController = new AbortController()
    const signal = abortController.signal

    createSavingCategory(formValues, signal)
      .then((res) => {
        if (!res?.response?.ok) {
          if (res?.data !== undefined) {
            for (const [key, value] of Object.entries(res.data)) {
              console.log("Account creation form error(s):", key, value)
              // Set error for existing fields
              if (key in formValues) {
                setError(key as keyof SavingCategoryFormInputs, { message: value[0] })
              } else {
                // Other validation errors
                setGenericFormError(value[0])
              }
            }
          } else {
            // Handle 40X,50X errors in here
            setGenericFormError("Something is wrong with the server, try again later!")
          }
          setIsUpdateSuccess(false)
        } else {
          reset(defaultValues)
          setGenericFormError("")
          setIsUpdateSuccess(true)
        }
      })
      .catch(error => {
        console.log("Category creation frontend error:", error)
        setIsUpdateSuccess(false)
      })
      .finally(() => {
        setIsSending(false)
        setShowDismissable(true)
        const timer = setTimeout(() => {
          setShowDismissable(false)
        }, autoCloseDismissibleAfterMs)
        return () => { clearTimeout(timer) }
      })

    return () => { abortController.abort() }
  }, [reset])

  const displayTitle = (): JSX.Element => <div className="col-sm-12"><h3>Create a Saving Category</h3></div>

  const renderDismissable = (): React.ReactNode => {
    if (showDismissable) {
      return <Alert variant={isUpdateSuccess ? "success" : "danger"} onClose={() => { setShowDismissable(false) }} dismissible>
        <Alert.Heading>{isUpdateSuccess ? "Category created!" : "Category creation failed :("}</Alert.Heading>
      </Alert>
    } else {
      return null
    }
  }

  const displayCreationForm = (): ReactFragment => {
    return (
      <form onSubmit={handleSubmit(onSubmit)} className="form">
        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 col-lg-3" htmlFor="text_field">Saving Category Name</label>
          <div className="col-sm-3">
            <input name="text" type="text" id="text_field" placeholder="Enter the saving category name" className="form-control"
              ref={register({
                required: "This field is required.",
                maxLength: {
                  value: 40,
                  message: "Max 40 chars"
                }
              })}
            />
            {errors?.text !== null && errors?.text !== undefined ? styleErrorMessage(errors?.text?.message as string) : null}
          </div>
        </div>

        <div className="form-group">
          <div className="col-sm-12 col-md-12 col-lg-12">
            {genericFormError !== "" ? styleErrorMessage(genericFormError) : null}
          </div>
        </div>

        <div className="form-group">
          <div className="col-sm-6 col-md-4 col-lg-3">
            <button className="btn btn-secondary my-2 my-sm-0" disabled={isSending}>{isSending ? "Submitting..." : "Submit"}</button>
          </div>
        </div>
      </form>
    )
  }

  const displayView = (): JSX.Element => {
    // Category has no foreign keys, hence we do not need a spinner, as we do not fetch other entities
    return <React.Fragment>
      {displayTitle()}
      {renderDismissable()}
      {displayCreationForm()}
    </React.Fragment>
  }

  return displayView()
}
