import React from "react"
import Container from "react-bootstrap/Container"
import Col from "react-bootstrap/Col"
import Row from "react-bootstrap/Row"
import type { ExternalRedirectPageProps, ExternalRedirectProps } from "../../common/interfaces"

const ExternalRedirect = ({ url, label }: ExternalRedirectProps): JSX.Element => {
  window.open(url, "_blank", "noopener,noreferrer")
  const handleClick = (event: React.MouseEvent<HTMLAnchorElement>): void => {
    event.preventDefault()
    window.open(url, "_blank", "noopener,noreferrer")
  }

  return (
      <Row>
         <Col xs md lg="12">
         <a
      href={url}
      onClick={handleClick}
      className="btn btn-secondary lead mb-2"
    >
      {label}
    </a>
         </Col>
      </Row>
  )
}

const ExternalRedirectPage = ({url, label, title, desc }: ExternalRedirectPageProps): JSX.Element => {
  return (
    <Container className="d-flex flex-column justify-content-center align-items-center">
    <Row className="text-center">
        <Col>
        <h2 className="mb-2">{title}</h2>
        <p className="lead mb-2">{desc}</p>
        <ExternalRedirect url={url} label={label} />
        </Col>
    </Row>
    </Container>
  )
}

export default ExternalRedirectPage
