import React from "react"

export const Logout = (): JSX.Element => {
  window.location.replace("/accounts/logout")
  return (
    <div />
  )
}
