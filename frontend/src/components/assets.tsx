import React, { Fragment, useEffect, useRef, useState, useCallback } from "react"

import { deleteAssets, getAssets, getAssetsDates } from "../common/api_assets"
import { getAccounts } from "../common/api_accounts"
import type { AssetsResponse, Asset, Account } from "../common/interfaces"
import { AssetTable, createAssetsGuidance, FailedBanner, CustomSpinner } from "./shared"
import { aggregateAssets, preventFormSubmit, sortByCurrentLocalValue } from "../common/utilities"
import { DoughnutChart } from "./doughnutchart"
import { Button, Col, Form, Row } from "react-bootstrap"
import Container from "react-bootstrap/Container"

/**
 * This component displays a list of assets.
 *
 * @returns A JSX element displaying the list of assets.
 */
export const Assets = (): JSX.Element => {
  const [isSending, setIsSending] = useState(false)
  const [rawAssets, setRawAssets] = useState<Asset[]>([])
  const [accounts, setAccounts] = useState<Account[]>([])
  const initialRender = useRef(true)
  const [assets, setAssets] = useState<Asset[]>([])
  const [dates, setDates] = useState<string[]>([])
  const [filters, setFilters] = useState({ name: "", date: "", account: "" })
  const [showAggregate, setshowAggregate] = useState(false)
  const [showTable, setshowTable] = useState(true)
  const forceYenDecimals = true
  const [hasAssets, setHasAssets] = useState(true)
  const [selectedItems, setSelectedItems] = useState<number[]>([])

  const [apiResponseComplete, setApiResponseComplete] = useState(false)
  const [criticalError, setCriticalError] = useState(false)
  const abortControllerRef = React.useRef<AbortController | null>(null)

  const toggleSelectAll = (): void => {
    setSelectedItems(
      // Should be this assets or raw assets?
      selectedItems.length === assets.length ? [] : assets.map((item) => item.id)
    )
  }

  const handleDelete = async (): Promise<void> => {
    // Create a new AbortController if one doesn't exist
    if (abortControllerRef.current === null) {
      abortControllerRef.current = new AbortController()
    }

    try {
      const response = await deleteAssets({ ids: selectedItems }, abortControllerRef.current.signal)
      if (response?.response?.ok) {
        setAssets((prevItems) =>
          prevItems.filter((item) => !selectedItems.includes(item.id))
        )
        setRawAssets((prevItems) => 
          prevItems.filter((item) => !selectedItems.includes(item.id))
        )
        setSelectedItems([])
      } else {
        console.error("Failed to delete items")
      }
    } catch (error) {
      if (error instanceof Error && error.name !== "AbortError") {
        console.error("Error deleting items:", error)
      }
    }
  }

  const toggleSelectItem = (id: number): void => {
    setSelectedItems((prevSelected) =>
      prevSelected.includes(id)
        ? prevSelected.filter((itemId) => itemId !== id)
        : [...prevSelected, id]
    )
  }

  // FIXME: if we select "Aggregate accounts by assets", and press refresh, if refresh returns new data
  // the table will still use the old data until we uncheck and re-check "Aggregate accounts by assets"
  const fetchData = useCallback(() => {
    if (isSending) return
    setIsSending(true)

    // Create a new AbortController for this request
    abortControllerRef.current = new AbortController()
    const signal = abortControllerRef.current.signal

    getAssets(filters, signal)
      .then((data: AssetsResponse) => {
        if (data?.response?.ok) {
          setCriticalError(false)
          setRawAssets(data.data.results)
        } else {
          setCriticalError(true)
        }
      })
      .catch((err) => {
        if (err.name !== "AbortError") {
          setCriticalError(true)
          console.log("Failed to fetch page: ", err)
        }
      }).finally(() => {
        setIsSending(false)
        setApiResponseComplete(true)
      })

    return () => abortControllerRef.current?.abort()
  }, [showAggregate, filters])

  // This is called everytime the checkboxed changes value, after fetchData() has completed
  useEffect(() => {
    if (showAggregate) {
      setAssets(aggregateAssets(assets).sort(sortByCurrentLocalValue))
    } else {
      setAssets(rawAssets.sort(sortByCurrentLocalValue))
    }
  }, [rawAssets, showAggregate])

  // This is called once, when the view is mounted
  useEffect(() => {
    // Use the component-wide AbortController instead of creating a new one
    abortControllerRef.current = new AbortController()
    const signal = abortControllerRef.current.signal

    const fetchInitialData = async (): Promise<void> => {
      try {
        const assetDates = await getAssetsDates(signal)
        const accountsData = await getAccounts(signal)

        if (accountsData?.response?.ok) {
          setAccounts(accountsData.data.results)
        }

        if (assetDates?.response?.ok) {
          if (Array.isArray(assetDates?.data) && assetDates?.data.length > 0) {
            setDates(assetDates?.data)
            const latestAssetsDate = assetDates?.data[0]
            // Calling handleFilterChange() here, will re-trigger an API call, and the UI will flicker.
            // This is intentional for now, as we want the picker to show the latest date.
            const e = { target: { name: "date", value: latestAssetsDate } }
            handleFilterChange(e as React.ChangeEvent<HTMLSelectElement>)
            const filters = { date: latestAssetsDate }
            const assetdata = await getAssets(filters, signal)
            if (!assetdata?.response?.ok || assetdata?.data?.results === null || assetdata?.data?.results === undefined) {
              setCriticalError(true)
            } else {
              setRawAssets(assetdata.data.results)
            }
          } else {
            setHasAssets(false)
          }
        } else {
          setCriticalError(true)
        }
      } catch (error) {
        if (error instanceof Error && error.name !== "AbortError") {
          setCriticalError(true)
          console.error("Failed to fetch page:", error)
        }
      } finally {
        setIsSending(false)
        setApiResponseComplete(true)
      }
    }

    if (initialRender.current) {
      initialRender.current = false
      setApiResponseComplete(false)
      void fetchInitialData()
    } else {
      void fetchData()
    }

    return () => {
      abortControllerRef.current?.abort()
      abortControllerRef.current = null
    }
  }, [fetchData])

  const handleFilterChange = (e: React.ChangeEvent<HTMLSelectElement>): void => {
    const { name, value } = e.target
    setFilters((prevFilters) => ({
      ...prevFilters,
      [name]: value
    }))
  }

  const tableBody = (): React.ReactFragment => {
    return (
      <React.Fragment>
        <div className="row" >
          <div className="col-sm-12">
            <form className="form" onSubmit={preventFormSubmit}>
              <div className="form-group row">
                 <div className="col-sm-1 col-md-3 col-lg-2">
                     <label><input type="checkbox" name="is_aggregate" onChange={e => { setshowAggregate(!showAggregate) }} checked={showAggregate}/>&nbsp;Aggregate by Asset</label>
                 </div>
                <div className="col-sm-1 col-md-3 col-lg-2">
                  {/* FIXME: THIS TEXT SHOULD REFLECT THE  */}
                <h6>{Array.isArray(assets) && assets?.length > 0 && assets[0]?.record_date !== null && assets[0]?.record_date !== undefined ? `Displaying only assets with date: ${assets[0]?.record_date}` : ""} </h6>
                    <button onClick={fetchData} className="btn btn-primary my-2">{ isSending ? "Refreshing..." : "Refresh" }</button>
                    {/* We do not handle aggregate deletion: show the delete checkbox only when showing individual assets */}
                    { showAggregate
                      ? null
                      : <Button className="ml-2" variant="danger" onClick={handleDelete} disabled={selectedItems.length === 0} >
                      Delete Selected
                    </Button> }
                </div>
              </div>
              <h4>Filters</h4>
              <Row>
                <Col>
                  <Form.Group controlId="filterName">
                    <Form.Label>Asset Name</Form.Label>
                    <Form.Control
                      type="text"
                      name="name"
                      value={filters.name}
                      onChange={handleFilterChange}
                      placeholder="Enter asset name"
                    />
                  </Form.Group>
                </Col>
                <Col>
                  <Form.Group controlId="filterAccount">
                    <Form.Label>Account</Form.Label>
                    <Form.Control
                      as="select"
                      name="account"
                      value={filters.account}
                      onChange={handleFilterChange}
                    >
                      <option value="">Select an Account</option>
                      {accounts.map((account) => (
                        <option key={account.id} value={account.id}>
                          {account.text}
                        </option>
                      ))}
                    </Form.Control>
                  </Form.Group>
                </Col>
                <Col>
                <Form.Group controlId="filterDate">
                  <Form.Label>Date</Form.Label>
                  <Form.Control
                    as="select"
                    name="date"
                    value={filters.date}
                    onChange={handleFilterChange}
                  >
                    <option value="">Select a date</option>
                    {dates.map((date) => (
                      <option key={date} value={date}>
                        {date}
                      </option>
                    ))}
                  </Form.Control>
                </Form.Group>
                </Col>
              </Row>
            </form>
          </div>
        </div>
        {/* id "assetTableNoData" is used in Functional tests, do not change it */}
         { assets.length > 0 ? <AssetTable data={assets} isAggregate={showAggregate} toggleSelectAll={toggleSelectAll} forceYenDecimals={forceYenDecimals} getSelectedItems={() => selectedItems} toggleSelectItem={toggleSelectItem} /> : <div id="assetTableNoData">No data for these filtering conditions</div> }
      </React.Fragment>
    )
  }

  const graphBody = (): React.ReactNode => (
      <Container>
        <div className="row">
          <div className="col-sm-12">
            <DoughnutChart />
          </div>
        </div>
    </Container>
  )

  const displayTitle = (): JSX.Element => <h3>List Assets</h3>

  const displayView = (): JSX.Element => {
    if (!apiResponseComplete) {
      return CustomSpinner()
    }

    if (criticalError) {
      return FailedBanner()
    }

    if (!hasAssets) {
      return <Fragment>{displayTitle()}{createAssetsGuidance()}</Fragment>
    }

    return (
      <div>
        {displayTitle()}
        <div className="row" >
          <div className="col-sm-12">
            <form className="form" onSubmit={preventFormSubmit}>
              <div className="form-group">
                <div className="col-sm-6 col-md-4 3 col-lg-3">
                  <label htmlFor="is_table_shown">
                    <input type="checkbox" name="is_aggregate" onChange={e => {
                      setshowAggregate(true)
                      setshowTable(!showTable)
                    }} checked={showTable}/>&nbsp;Show data in a table
                  </label>
                </div>
              </div>
             </form>
           </div>
         </div>
        { showTable ? tableBody() : graphBody()}
      </div>
    )
  }
  return displayView()
}
