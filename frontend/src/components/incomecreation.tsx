import { useForm } from "react-hook-form"
import { Link } from "react-router-dom"
import Alert from "react-bootstrap/Alert"
import React, { useEffect, useRef, useState, useCallback } from "react"
import { createIncome } from "../common/api_incomes"
import { getIncomeCategories } from "../common/api_incomecategories"
import type { IncomeCategory, IncomeFormInputs, IncomeResponse } from "../common/interfaces"
import { FailedBanner, CustomSpinner, styleErrorMessage } from "./shared"

const defaultValues: IncomeFormInputs = {
  amount: 0,
  category_id: 0,
  note: "",
  date: new Date().toLocaleDateString("en-CA")
}

export const IncomeCreation = (): JSX.Element => {
  // eslint-disable-next-line @typescript-eslint/unbound-method
  const { register, handleSubmit, setError, formState: { errors }, reset } = useForm<IncomeFormInputs>()
  const [isSending, setIsSending] = useState(false)
  const [categories, setCategories] = useState<IncomeCategory[]>([{ id: 0, text: "Loading..." }])
  const initialRender = useRef(true)
  const [showDismissable, setShowDismissable] = useState(false)
  const [isUpdateSuccess, setIsUpdateSuccess] = useState(false)
  const autoCloseDismissibleAfterMs = 1500

  const [apiResponseComplete, setApiResponseComplete] = useState(false)
  const [criticalError, setCriticalError] = useState(false)

  const [genericFormError, setGenericFormError] = useState("")

  const onSubmit = useCallback((formValues: IncomeFormInputs) => {
    if (isSending) return
    setShowDismissable(false)
    setIsSending(true)
    const abortController = new AbortController()
    const signal = abortController.signal

    createIncome(formValues, signal)
      .then((res: IncomeResponse) => {
        if (!res?.response?.ok) {
          if (res?.data !== null && res?.data !== undefined) {
            for (const [key, value] of Object.entries(res.data)) {
              console.log("Account creation form error(s):", key, value)
              // Set error for existing fields
              if (key in formValues) {
                setError(key as keyof IncomeFormInputs, { message: value[0] })
              } else {
                // Account has a "text AND credit_entity" unique constraints
                if (value[0] === "An Account with the same text AND credit_entity already exists for this user.") {
                  setError("text" as keyof IncomeFormInputs, { message: value[0] })
                } else {
                // Other validation errors
                  setGenericFormError(value[0])
                }
              }
            }
          } else {
            // Handle 40X,50X errors in here
            setGenericFormError("Something is wrong with the server, try again later!")
          }
          setIsUpdateSuccess(false)
        } else {
          reset(defaultValues)
          setGenericFormError("")
          setIsUpdateSuccess(true)
        }
      })
      .catch(error => {
        console.log("Income creation error ", error)
        setIsUpdateSuccess(false)
      })
      .finally(() => {
        setIsSending(false)
        setShowDismissable(true)
        const timer = setTimeout(() => {
          setShowDismissable(false)
        }, autoCloseDismissibleAfterMs)
        return () => { clearTimeout(timer) }
      })

    return () => { abortController.abort() }
  }, [reset])

  useEffect(() => {
    const abortController = new AbortController()
    const signal = abortController.signal
    if (initialRender.current) {
      initialRender.current = false
      setIsSending(true)

      getIncomeCategories(signal).then((res) => {
        if (!res?.response?.ok) {
          setCriticalError(true)
        } else if (res?.response?.ok && Array.isArray(res?.data?.results)) {
          setCriticalError(false)
          setCategories(res?.data.results)
        } else {
          setCriticalError(true)
        }
      }).catch((err) => {
        setCriticalError(true)
        console.log("Failed to fetch page: ", err)
      }).finally(() => {
        setIsSending(false)
        setApiResponseComplete(true)
      })
    }

    return () => { abortController.abort() }
  }, [])

  const displayTitle = (): JSX.Element => {
    return <h3>Create an income </h3>
  }

  const createIncomeCategoryGuidance = (): JSX.Element => {
    return <div>
      <h5 className="text-info"> You need to create a Income Category first! </h5>
      <Link className="btn btn-info mt-2 " to={"/app/income-categories/creation"}>Create an Income Category</Link>
    </div>
  }

  const displayCreationForm = (): JSX.Element => {
    if (categories?.length === 0) {
      return createIncomeCategoryGuidance()
    }
    return (
      <form onSubmit={handleSubmit(onSubmit)} className="form">
        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="text_field">Income Category:&nbsp;</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <select id="category_field" name="category" ref={register({ required: "This field is required." })}>
              {categories.map(value => (<option key={value.id} value={value.id}>
                  {value.text}
                </option>))}
            </select>
            {(errors?.category?.message !== undefined && errors?.category?.message !== null) ? styleErrorMessage(errors.category.message) : null}
          </div>
        </div>

        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="amount_field">Amount</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input className="form-control" name="amount" type="number" id="amount_field"
               ref={register({
                 required: "This field is required.",
                 min: {
                   value: 1,
                   message:
                        "Amount should be more than 0"
                 }
               })}
            />
            {(errors?.amount?.message !== null && errors?.amount?.message !== undefined) ? styleErrorMessage(errors.amount.message) : null}
          </div>
        </div>

        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="date_entity">Date&nbsp;</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input name="date" type="date" id="date_entity" defaultValue={new Date().toLocaleDateString("en-CA")} ref={register({ required: true })} />
            {(errors?.date?.message !== null && errors?.date?.message !== undefined) ? styleErrorMessage(errors.date.message) : null}
          </div>
        </div>

        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="note_field">Note&nbsp;</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input className="form-control" type="text" name="note" id="note_field"
              ref={register({
                maxLength: {
                  value: 150,
                  message: "Max 150 chars"
                }
              })}
            />
            {(errors?.note?.message !== null && errors?.note?.message !== undefined) ? styleErrorMessage(errors.note.message) : null}
          </div>
        </div>

        <div className="form-group">
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            {genericFormError !== "" ? styleErrorMessage(genericFormError) : null}
            <button className="btn btn-secondary my-2 my-sm-0" disabled={isSending}>{ isSending ? "Submitting..." : "Submit" }</button>
          </div>
        </div>

      </form>
    )
  }

  const renderDismissable = (): JSX.Element | null => {
    if (showDismissable) {
      return <Alert variant={isUpdateSuccess ? "success" : "danger"} onClose={() => { setShowDismissable(false) }} dismissible>
          <Alert.Heading>{isUpdateSuccess ? "Income created!" : "Income creation failed :("}</Alert.Heading>
        </Alert>
    } else {
      return null
    }
  }

  const displayView = (): JSX.Element => {
    if (!apiResponseComplete) {
      return CustomSpinner()
    }

    if (criticalError) {
      return FailedBanner()
    }

    return <React.Fragment>
      { displayTitle() }
      { renderDismissable() }
      { displayCreationForm() }
    </React.Fragment>
  }

  return displayView()
}
