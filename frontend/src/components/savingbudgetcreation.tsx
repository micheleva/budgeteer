import { useForm } from "react-hook-form"
import { Link } from "react-router-dom"

import Alert from "react-bootstrap/Alert"
import React, { useEffect, useRef, useState, useCallback } from "react"
import { createBudget } from "../common/api_budgets"
import { getSavingCategories } from "../common/api_savingcategories"
import type { BudgetFormInput, SavingCategory, BudgetResponse } from "../common/interfaces"
import { FailedBanner, CustomSpinner, styleErrorMessage } from "./shared"

const defaultValues: BudgetFormInput = {
  // NOTE: do not reset the Saving Category
  year: new Date().getFullYear(),
  month: new Date().getMonth() + 1,
  amount: 0,
  allocated_amount: 0
}

export const SavingBudgetCreation = (): JSX.Element => {
  // eslint-disable-next-line @typescript-eslint/unbound-method
  const { register, handleSubmit, setError, formState: { errors }, setValue, watch, reset } = useForm<BudgetFormInput>({ defaultValues })
  const [isSending, setIsSending] = useState(false)
  const [savingcategories, setSavingCategories] = useState<SavingCategory[]>([{ id: 0, text: "Loading..." }])
  const initialRender = useRef(true)
  const [showDismissable, setShowDismissable] = useState(false)
  const [isUpdateSuccess, setIsUpdateSuccess] = useState(false)
  const autoCloseDismissibleAfterMs = 1500

  const [apiResponseComplete, setApiResponseComplete] = useState(false)
  const [criticalError, setCriticalError] = useState(false)
  // const [recoverableError, setRecoverableError] = useState(false)
  const [genericFormError, setGenericFormError] = useState("")

  const targetDateWatchObj = watch(["date", "isYearlyBudget"], [new Date().toLocaleDateString("en-CA"), false])

  interface BudgetTarget {
    month: number | null
    year: number | null
  }

  const getCorrectTargetDate = (): BudgetTarget => {
    if (!targetDateWatchObj?.date === null) {
      return { month: null, year: null }
    }

    const date01 = `${targetDateWatchObj?.date}T00:00:00`
    const d = new Date(date01)
    if (targetDateWatchObj?.isYearlyBudget === true) {
      return { month: null, year: d.getFullYear() }
    } else {
      return { month: d.getMonth() + 1, year: d.getFullYear() }
    }
  }

  const getCorrectTargetDateAsString = (): JSX.Element => {
    const res = getCorrectTargetDate()
    if (res?.month !== null) {
      return `${res.year}-${String(res.month).padStart(2, "0")}`
    }
    return res.year
  }

  const formatPostBody = (formValues: BudgetFormInput): BudgetFormInput => {
    const sanitizedValues = { ...formValues }
    const date01 = `${sanitizedValues.date}T00:00:00`
    const d = new Date(date01)
    const year = d.getFullYear()
    const month = d.getMonth() + 1
    if (sanitizedValues?.isYearlyBudget === true) {
      sanitizedValues.month = null
      sanitizedValues.year = year
    } else {
      sanitizedValues.month = month
      sanitizedValues.year = year
    }
    // NOTE: date and isYearlyBudget fields are used in the frontend to properly extract year and month as numbers: do not post them
    delete sanitizedValues.date
    delete sanitizedValues.isYearlyBudget
    return sanitizedValues
  }

  const onSubmit = useCallback((formValues: BudgetFormInput) => {
    if (isSending) return
    setShowDismissable(false)
    setIsSending(true)
    const abortController = new AbortController()
    const signal = abortController.signal

    const postBodyData = formatPostBody(formValues)
    createBudget(postBodyData, signal)
      .then((res: BudgetResponse) => {
        if (!res?.response?.ok) {
          if (res?.data !== undefined) {
            for (const [key, value] of Object.entries(res.data)) {
              console.log("Budgets creation form error(s):", key, value)
              // Set error for existing fields
              if (key in formValues) {
                setError(key, { message: value[0] })
              } else {
                // FIXME THIS IS COPIED FROM ACCOUNT. FIX ME!!! <== Add unit and functional tests, and see what text do we returns for this Model
                if (value[0] === "An Account with the same text AND credit_entity already exists for this user.") {
                  setError("text", { message: value[0] })
                } else {
                  // Other validation errors
                  setGenericFormError(value[0])
                }
              }
            }
          } else {
            // Handle 40X,50X errors in here
            setGenericFormError("Something is wrong with the server, try again later!")
          }
          setIsUpdateSuccess(false)
        } else {
          reset(defaultValues)
          setGenericFormError("")
          setIsUpdateSuccess(true)
        }
      })
      .catch(error => {
        console.log("Budget creation error ", error)
        setIsUpdateSuccess(false)
      })
      .finally(() => {
        setIsSending(false)
        setShowDismissable(true)
        const timer = setTimeout(() => {
          setShowDismissable(false)
        }, autoCloseDismissibleAfterMs)
        return () => { clearTimeout(timer) }
      })

    return () => { abortController.abort() }
  }, [reset])

  useEffect(() => {
    const abortController = new AbortController()
    const signal = abortController.signal

    if (initialRender.current) {
      initialRender.current = false
      setIsSending(true)
      getSavingCategories(signal).then((res) => {
        if (!res?.response?.ok || !Array.isArray(res?.data?.results)) {
          setCriticalError(true)
        } else {
          setCriticalError(false)
          setSavingCategories(res?.data.results)
        }
        // TODO: differentiate between recoverable and critical errors
        // currently if data.results is not an array we do not tell the user
        // this is something back on the server side but we should
        // tell the user that there was an error
      }).catch((err) => {
        setCriticalError(true)
        console.log("Failed to fetch page: ", err)
      }).finally(() => {
        setIsSending(false)
        setApiResponseComplete(true)
        setValue("date", new Date().toLocaleDateString("en-CA"))
      })
    }

    return () => { abortController.abort() }
  }, [])

  const displayTitle = (): JSX.Element => <h3>Create a Saving Budget </h3>

  const createSavingCategoryGuidance = (): JSX.Element => {
    return <div>
      <h5 className="text-info"> You need to create a Saving Category first! </h5>
      <Link className="btn btn-info mt-2 " to={"/app/saving-categories/creation"}>Create a Saving Category</Link>
    </div>
  }

  const displayCreationForm = (): JSX.Element => {
    if (savingcategories?.length === 0) {
      return createSavingCategoryGuidance()
    }
    return (
      <form onSubmit={handleSubmit(onSubmit)} className="form">
        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="savingcategory_field">Category:&nbsp;</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <select id="savingcategory_field" name="savingcategory" ref={register({ required: "This field is required." })}>
              {savingcategories.map(value => (<option key={value.id} value={value.id}>
                {value.text}
              </option>))}
            </select>
            {errors.savingcategory !== null && errors.savingcategory !== undefined ? styleErrorMessage(errors.savingcategory.message as string) : null}
          </div>
        </div>

        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="amount_field">Amount</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input className="form-control" name="amount" type="number" id="amount_field"
              ref={register({
                required: "This field is required.",
                min: {
                  value: 0,
                  message:
                    "Amount should be more or equal to 0"
                }
              })}
            />
            {errors.amount !== null && errors.amount !== undefined ? styleErrorMessage(errors.amount.message as string) : null}
          </div>
        </div>

        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="amount_field">Allocated Amount</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input className="form-control" name="allocated_amount" type="number" id="allocated_amount_field"
              ref={register({
                required: "This field is required.",
                min: {
                  value: 0,
                  message:
                    "Amount should be more or equal to 0"
                }
              })}
            />
            {errors.allocated_amount !== null && errors.allocated_amount !== undefined ? styleErrorMessage(errors.allocated_amount.message as string) : null}
          </div>
        </div>

        {/* Firefox does not support yet type="date" https://bugzilla.mozilla.org/show_bug.cgi?id=1283382 (8 year old bug)
            https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/month#browser_compatibility */}
        <div className="form-group">
          <label className="control-label col-sm-7 col-md-5 col-lg-4" htmlFor="date_entity">Date <small>(Firefox does not support month pickers, so pick <i>any day</i> of the month to set the budget for)</small>&nbsp;</label>
          <div className="col-sm-6 col-md-4 col-lg-3">
            <input name="date" type="date" id="date_entity" defaultValue={new Date().toLocaleDateString("en-CA")} ref={register({ required: true })} />
            {errors.date !== null && errors.date !== undefined ? styleErrorMessage(errors.date.message as string) : null}
          </div>
        </div>

        <div className="form-group">
          <div className="col-sm-6 col-md-4 col-lg-3">
            <label><input type="checkbox" name="isYearlyBudget" ref={register()} />&nbsp;Yearly Saving budget?</label>
            {errors.isYearlyBudget !== null && errors.isYearlyBudget !== undefined ? styleErrorMessage(errors.isYearlyBudget.message as string) : null}
          </div>
        </div>

        <div className="form-group">
          <div className="col-sm-6 col-md-4 col-lg-3">
            <b><div> Setting Saving Budget for: <span className="text-success"> {getCorrectTargetDateAsString()}</span></div></b>
          </div>
        </div>

        <div className="form-group">
          <div className="col-sm-12 col-md-12 col-lg-12">
            <b><div>{genericFormError !== null && genericFormError !== undefined && genericFormError.length > 0 ? styleErrorMessage(genericFormError) : null}</div></b>
          </div>
        </div>

        <div className="form-group">
          <div className="col-sm-6 col-md-4 col-lg-3">
            <button className="btn btn-secondary my-2 my-sm-0" disabled={isSending}>{isSending ? "Submitting..." : "Submit"}</button>
          </div>
        </div>

      </form>
    )
  }

  const renderDismissable = (): JSX.Element | null => {
    if (showDismissable) {
      return <Alert variant={isUpdateSuccess ? "success" : "danger"} onClose={() => { setShowDismissable(false) }} dismissible>
        <Alert.Heading>{isUpdateSuccess ? "Saving Budget created!" : "Saving Budget creation failed :("}</Alert.Heading>
      </Alert>
    } else {
      return null
    }
  }

  const renderWIPAlert = (): JSX.Element => {
    return <Alert variant={"warning"}>
      <Alert.Heading>Work in progress</Alert.Heading>
      <div className="text-dark">{"After v1.3 is released, users will be able to be mass update/create Budgets in the same fashion as MonthlyBalances. Bare with me while I'm at it :D "}</div>
    </Alert>
  }

  const displayView = (): JSX.Element => {
    if (!apiResponseComplete) {
      return CustomSpinner()
    }

    if (criticalError) {
      return FailedBanner()
    }

    return <React.Fragment>
      {renderWIPAlert()}
      {displayTitle()}
      {renderDismissable()}
      {displayCreationForm()}
    </React.Fragment>
  }

  return displayView()
}
