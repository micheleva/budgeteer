import React, { Fragment, useEffect, useRef, useState } from "react"
import { Link } from "react-router-dom"

import { getObligations, deleteObligation } from "../common/api_obligations"
import { deletePaymentChange } from "../common/api_paymentchanges"
import type { Obligation, ObligationsResponse } from "../common/interfaces"
import { FailedBanner, ObligationsTable, CustomSpinner } from "./shared"

const Obligations = (): JSX.Element => {
  const [obligations, setObligations] = useState<Obligation[]>([])
  const initialRender = useRef(true)
  const [invalidResponse, setInvalidResponse] = useState(false)
  const [apiResponseComplete, setApiResponseComplete] = useState(false)

  const removeFromLocalCache = (id: number): void => {
    setObligations((original) => original.filter((obligation) => obligation.id !== id))
  }

  const removePaymentChangeFromLocalCache = (id: number): void => {
    setObligations((original) => original.map((obligation) => {
      if (obligation.obligation_type === "variable" && obligation?.changes?.length !== undefined && obligation?.changes?.length > 0) {
        obligation.changes = obligation.changes.filter((change) => change.id !== id)
      }
      return obligation
    }))
  }

  useEffect(() => {
    const abortController = new AbortController()
    const signal = abortController.signal

    if (initialRender.current) {
      initialRender.current = false
      setApiResponseComplete(false)
      getObligations(signal).then((data: ObligationsResponse) => {
        if (typeof data.response === "undefined") {
          setInvalidResponse(true)
        } else if (data?.response?.ok && Array.isArray(data?.data?.results)) {
          setInvalidResponse(false)
          const responseObligations = data.data.results
          setObligations(responseObligations)
        } else {
          setInvalidResponse(true)
        }
      })
        .catch((e) => {
          console.log(e)
          setInvalidResponse(true)
        })
        .finally(() => {
          setApiResponseComplete(true)
        })
    }

    return () => { abortController.abort() }
  }, [])

  const createObligationsGuidance = (): JSX.Element => {
    return <div>
      <h3>List Obligations</h3>
      <h5 className="text-info">Your obligations will appear here after you create any!</h5>
      <Link className="btn btn-info mt-2 "
            to={"/app/obligations/creation"}>Create an Obligation</Link>
    </div>
  }

  const displayObligationsBody = (): JSX.Element => {
    if (!apiResponseComplete) {
      return CustomSpinner()
    }

    if (invalidResponse) {
      return FailedBanner()
    }

    if (apiResponseComplete && obligations.length === 0) {
      return createObligationsGuidance()
    }

    // IMPORTANT: id="obligations-table" is used in the integration tests, do not alter it
    return <Fragment>
          <div className="col-sm-12"><h3>List Obligations</h3></div>
          <ObligationsTable
            id="obligations-table"
            obligations={obligations}
            deleteOblFunc={async (id: number) => { return await deleteObligation(id) }}
            deletePaymentFunc={async (id: number) => { return await deletePaymentChange(id) }}
            updateParentData={(id: number) => { removeFromLocalCache(id) }}
            updateParentPaymentChangeData={(id: number) => { removePaymentChangeFromLocalCache(id) }}
          />
        </Fragment>
  }

  return displayObligationsBody()
}

export default Obligations
