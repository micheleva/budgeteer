import React, { Fragment, useCallback, useEffect, useState } from "react"
import { Link } from "react-router-dom"
import { getExpensesAggregateByMonth } from "../common/api_expenses"
import { getIncomesAggregate } from "../common/api_incomes"
import { FailedBanner, CustomSpinner } from "./shared"
import { Bar } from "react-chartjs-2"
import "chartjs-plugin-colorschemes"

const displayTitle = (): JSX.Element => <h3> Expenses vs Income </h3>

const ExpenseIncomeGraphComponent = (): JSX.Element => {
  const [isSending, setIsSending] = useState(false)
  const [chartData, setChartData] = useState({})

  const [apiResponseComplete, setApiResponseComplete] = useState(false)
  const [criticalError, setCriticalError] = useState(false)

  const createExpensesOrIncomes = (): JSX.Element => {
    return <div>
      <h5 className="text-info">Your Expenses and Incomes will appear here after you create any!</h5>
      <div>
        <Link className="btn btn-info mt-2 " to={"/app/expenses/creation"}>Create an Expense</Link>
       </div>
      <Link className="btn btn-info mt-2 " to={"/app/incomes/creation"}>Create an Income</Link>
    </div>
  }

  const fetchData = useCallback((signal?: AbortSignal) => {
    if (isSending) return
    setIsSending(true)
    setApiResponseComplete(false)

    Promise.all([getIncomesAggregate(signal), getExpensesAggregateByMonth(signal)]).then((values) => {
      const incomes = values[0]
      const expenses = values[1]
      let isCriticalError = false

      if (!incomes?.response?.ok) {
        isCriticalError = true
      }

      if (!expenses?.response?.ok) {
        isCriticalError = true
      }

      setCriticalError(isCriticalError)
      if (!isCriticalError) {
        setChartData({
          incomes: incomes.data,
          expenses: expenses.data
        })
      }
    }).catch((err: Error) => {
      setCriticalError(true)
      console.log("Failed to fetch page: ", err)
    }).finally(() => {
      setIsSending(false)
      setApiResponseComplete(true)
    })
  }, [])

  useEffect(() => {
    const abortController = new AbortController()
    const signal = abortController.signal

    fetchData(signal)

    return () => { abortController.abort() }
  }, [fetchData])

  const displayView = (): JSX.Element => {
    if (!apiResponseComplete) {
      return CustomSpinner()
    }
    if (criticalError) {
      return FailedBanner()
    }

    if (chartData?.expenses?.length === 0 && chartData?.incomes?.length === 0) {
      return <Fragment>{displayTitle()}{createExpensesOrIncomes()}</Fragment>
    }

    return <ExpenseIncomeGraph chartData={chartData} />
  }

  return displayView()
}

const ExpenseIncomeGraph = (props: { chartData: { incomes: any[]; expenses: any[] } }): JSX.Element => {
  const [chartData, setChartData] = useState({})

  useEffect(() => {
    const abortController = new AbortController()
    const signal = abortController.signal

    getChartData(signal)

    return () => { abortController.abort() }
  }, [])

  const getChartData = (signal?: AbortSignal) => {
    const { incomes, expenses } = props.chartData

    // Create a set of unique months from both incomes and expenses
    const allMonthsSet = new Set([
      ...incomes.map(row => `${row.year}-${row.month}`),
      ...expenses.map(row => `${row.year}-${row.month}`)
    ])
    const allMonths = [...allMonthsSet].sort()

    // Adjust incomes and expenses data to have the same order of months
    const adjustedIncomes = allMonths.map(month => {
      const income = incomes.find(row => `${row.year}-${row.month}` === month)
      return income ? income.total_amount : undefined
    })

    const adjustedExpenses = allMonths.map(month => {
      const expense = expenses.find(row => `${row.year}-${row.month}` === month)
      return expense ? expense.total_amount : undefined
    })

    const finalData = {
      labels: allMonths,
      datasets: [
        {
          label: "Expenses",
          data: adjustedExpenses
        },
        {
          label: "Incomes",
          data: adjustedIncomes
        }
      ]
    }
    setChartData(finalData)
  }

  const displayView = (): JSX.Element => {
    const scalesSettings = {
      x: {
        stacked: true
      },
      y: {
        stacked: true
      }
    }
    // Privacy mode is not implemented yet
    const isPrivacyMode = false
    const chartOptions = {
      plugins: {
        title: {
          display: true,
          text: "Expenses vs Income"
        },
        legend: {
          position: "top"
        }
      },
      responsive: true,
      maintainAspectRatio: false,
      scales: scalesSettings,
      tooltips: {
        mode: "label", // NOTE: remove this to avoid the mouse-hover if you want to avoid displaying too many items at the same time
        callbacks: {
          // FIXME: footer should show difference between income and expenses <<====
          // remember to handle months where we only have expenses or only incomes!
          // if possible color in red negative months
          footer: (tooltipItems, data) => {
            const total = new Intl.NumberFormat("ja-JP", {
              style: "currency",
              currency: "JPY"
            }).format(tooltipItems[0].value)
            return `Net: ${total}`
            // return isPrivacyMode ? "Net: xxx" : `Net: ${total}`
          },
          label: function (tooltipItem, data) {
            if (tooltipItem.value === "0") return
            if (isPrivacyMode) {
              return data.datasets[tooltipItem.datasetIndex].label
            } else {
              if (isNaN(tooltipItem.value)) {
                return (data.datasets[tooltipItem.datasetIndex].label + " " + new Intl.NumberFormat("ja-JP", {
                  style: "currency",
                  currency: "JPY"
                }).format(0))
              }
              return (data.datasets[tooltipItem.datasetIndex].label + " " + new Intl.NumberFormat("ja-JP", {
                style: "currency",
                currency: "JPY"
              }).format(tooltipItem.value)) // return data.datasets[tooltipItem.datasetIndex].label + " " + "-" in privacy mode
            }
          }
        }
      }
    }

    return (
      <div className="row">
        <div className="col-sm-12">
          {displayTitle()}
        </div>
        <div className="col-sm-12">
          <Bar data={chartData} height={800} options={chartOptions}/>
        </div>
      </div>
    )
  }

  return displayView()
}

export default ExpenseIncomeGraphComponent
