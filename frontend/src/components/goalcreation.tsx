import { useForm } from "react-hook-form"
import Alert from "react-bootstrap/Alert"
import React, { useState, useCallback } from "react"
import { createGoal } from "../common/api_goals"
import type { GoalFormInputs, GoalResponse, ReactFragment } from "../common/interfaces"
import { styleErrorMessage } from "./shared"

const defaultValues: GoalFormInputs = {
  text: "",
  amount: 0,
  note: "",
  is_archived: false
}

export const GoalCreation = (): JSX.Element => {
  // eslint-disable-next-line @typescript-eslint/unbound-method
  const { register, handleSubmit, setError, formState: { errors }, reset } = useForm<GoalFormInputs>()
  const [isSending, setIsSending] = useState(false)
  const [showDismissable, setShowDismissable] = useState(false)
  const [isUpdateSuccess, setIsUpdateSuccess] = useState(false)
  const autoCloseDismissibleAfterMs = 1500

  const [serverError, setServerError] = useState("")

  const onSubmit = useCallback((formValues: GoalFormInputs) => {
    if (isSending) return
    setShowDismissable(false)
    setIsSending(true)
    // Goal models allow None for note field, but not blank (i.e. empty string)
    if (formValues.note === "") {
      delete formValues.note
    }
    const abortController = new AbortController()
    const signal = abortController.signal

    createGoal(formValues, signal)
      .then((res: GoalResponse) => {
        if (!res?.response?.ok) {
          if (typeof res.data === "object" && Object.keys(res.data).length > 0) {
            for (const [key, value] of Object.entries(res.data)) {
              console.log("Account creation form error(s):", key, value)
              // Set error for existing fields
              if (key in formValues) {
                setError(key as keyof GoalFormInputs, { message: value[0] })
              } else {
                // Other validation errors
                setServerError(value[0])
              }
            }
          } else {
            // Handle 40X,50X errors in here
            setServerError("Something is wrong with the server, try again later!")
          }
          setIsUpdateSuccess(false)
        } else {
          reset(defaultValues)
          setServerError("")
          setIsUpdateSuccess(true)
        }
      })
      .catch((error: Error) => {
        console.log("Goal creation frontend error:", error)
        setIsUpdateSuccess(false)
      })
      .finally(() => {
        setIsSending(false)
        setShowDismissable(true)
        const timer = setTimeout(() => {
          setShowDismissable(false)
        }, autoCloseDismissibleAfterMs)
        return () => { clearTimeout(timer) }
      })
    return () => { abortController.abort() }
  }, [reset])

  const displayTitle = (): JSX.Element => {
    return <h3>Create a goal</h3>
  }

  const renderDismissable = (): JSX.Element | null => {
    if (showDismissable) {
      return <Alert variant={isUpdateSuccess ? "success" : "danger"} onClose={() => { setShowDismissable(false) }} dismissible>
              <Alert.Heading>{isUpdateSuccess ? "Goal created!" : "Goal creation failed :("}</Alert.Heading>
            </Alert>
    } else {
      return null
    }
  }

  const displayCreationForm = (): ReactFragment => {
    return (
      <form onSubmit={handleSubmit(onSubmit)} className="form">
      <div className="form-group">
        <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="text_field">Text</label>
        <div className="col-sm-6 col-md-4 3 col-lg-3">
          <input id="text_field" className="form-control" name="text" type="text" placeholder="Insert the goal title"
           ref={register({
             required: "This field is required.",
             maxLength: {
               value: 20,
               message: "Max 20 chars"
             }
           })}
          />
          {errors.text !== null && errors.text !== undefined ? styleErrorMessage(errors.text.message as string) : null}
        </div>
      </div>

      <div className="form-group">
        <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="amount_field">Amount</label>
        <div className="col-sm-6 col-md-4 3 col-lg-3">
          <input className="form-control" name="amount" type="number" id="amount_field"
            ref={register({
              required: "This field is required.",
              min: {
                value: 1,
                message:
                "Amount should be more than 0"
              }
            })}
          />
          {errors.amount !== null && errors.amount !== undefined ? styleErrorMessage(errors.amount.message as string) : null}
        </div>
      </div>

      <div className="form-group">
        <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="note_field">Note</label>
        <div className="col-sm-6 col-md-4 3 col-lg-3">
          <input className="form-control" name="note" type="text" placeholder="Short goal description" id="note_field"
           ref={register({
             required: "This field is required.",
             maxLength: {
               value: 50,
               message: "Max 50 chars"
             }
           })}
        />
        {errors.note !== null && errors.note !== undefined ? styleErrorMessage(errors.note.message as string) : null}
        </div>
      </div>

      <div className="form-group">
        <div className="col-sm-6 col-md-4 3 col-lg-3">
        <label><input type="checkbox" id="is_archived_field" name="is_archived" ref={register()} />&nbsp;Is archived</label>
        {errors.is_archived !== null && errors.is_archived !== undefined ? styleErrorMessage(errors.is_archived.message as string) : null}
        </div>
      </div>

      <div className="form-group">
        <div className="col-sm-6 col-md-4 3 col-lg-3">
          {serverError !== null && serverError !== undefined && serverError !== "" ? styleErrorMessage(serverError) : null}
          {/* NOTE: the id="CreateButton" is used in the functional tests, do not remove it */}
          <button id="CreateButton" className="btn btn-secondary my-2 my-sm-0" disabled={isSending}>{ isSending ? "Submitting..." : "Submit" }</button>
        </div>
      </div>

      </form>
    )
  }

  const displayView = (): JSX.Element => {
    // Goal has no foreign keys, hence we do not need a spinner, as we do not fetch other entities
    return <React.Fragment>
      { displayTitle() }
      { renderDismissable() }
      { displayCreationForm() }
    </React.Fragment>
  }

  return displayView()
}
