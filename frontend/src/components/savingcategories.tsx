import React, { Fragment, useEffect, useRef, useState } from "react"

import { getSavingCategories } from "../common/api_savingcategories"
import type { SavingCategory, SavingCategoriesResponse } from "../common/interfaces"
import { FailedBanner, CustomSpinner, CategoryTable, createSavingCategoryGuidance } from "./shared"

export const SavingCategories = (): JSX.Element => {
  const [categories, setCategories] = useState<SavingCategory[]>([])
  const initialRender = useRef(true)
  const [invalidResponse, setInvalidResponse] = useState(false)
  const [apiResponseComplete, setApiResponseComplete] = useState(false)

  useEffect(() => {
    const abortController = new AbortController()
    const signal = abortController.signal

    if (initialRender.current) {
      initialRender.current = false
      setApiResponseComplete(false)
      getSavingCategories(signal).then((data: SavingCategoriesResponse) => {
        if (!data?.response?.ok) {
          setInvalidResponse(true)
        } else {
          setInvalidResponse(false)
          const responseCategories = data.data.results
          setCategories(responseCategories)
        }
      })
        .catch((e: Error) => {
          console.log(e)
          setInvalidResponse(true)
        })
        .finally(() => {
          setApiResponseComplete(true)
        })
    }

    return () => { abortController.abort() }
  }, [])

  const displayTitle = (): JSX.Element => <div className="col-sm-12"><h3>List Saving Categories</h3></div>

  const displayGoalsBody = (): JSX.Element => {
    if (!apiResponseComplete) {
      return CustomSpinner()
    }

    if (invalidResponse) {
      return FailedBanner()
    }

    if (apiResponseComplete && categories.length === 0) {
      return <Fragment>{displayTitle()}{createSavingCategoryGuidance()}</Fragment>
    }

    const list = categories.length > 0
      ? <CategoryTable categories={categories} url="saving-category" />
      : null

    return <React.Fragment>
      {displayTitle()}
      <ul>{list}</ul>
    </React.Fragment>
  }

  return displayGoalsBody()
}
