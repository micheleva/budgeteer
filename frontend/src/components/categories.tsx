import React, { Fragment, useEffect, useRef, useState } from "react"

import { getCategories } from "../common/api_categories"
import type { Category, CategoriesResponse } from "../common/interfaces"
import { createCategoryGuidance, CustomSpinner, FailedBanner, CategoryTable } from "./shared"

export const Categories = (): JSX.Element => {
  const [categories, setCategories] = useState<Category[]>([])
  const initialRender = useRef(true)
  const [invalidResponse, setInvalidResponse] = useState(false)
  const [apiResponseComplete, setApiResponseComplete] = useState(false)

  useEffect(() => {
    const abortController = new AbortController()
    const signal = abortController.signal

    if (initialRender.current) {
      initialRender.current = false
      getCategories(signal).then((data: CategoriesResponse) => {
        if (!data?.response?.ok) {
          setInvalidResponse(true)
        } else {
          setInvalidResponse(false)
          const responseCategories = data.data.results
          setCategories(responseCategories)
        }
      })
        .catch((e) => {
          console.log(e)
          setInvalidResponse(true)
        })
        .finally(() => {
          setApiResponseComplete(true)
        })
    }

    return () => { abortController.abort() }
  }, [])

  const displayTitle = (): JSX.Element => <div className="col-sm-12 mx-4"><h3>List Categories</h3></div>

  const displayGoalsBody = (): JSX.Element => {
    if (!apiResponseComplete) {
      return CustomSpinner()
    }

    if (invalidResponse) {
      return FailedBanner()
    }

    if (apiResponseComplete && categories.length === 0) {
      return <Fragment>{displayTitle()}{createCategoryGuidance()}</Fragment>
    }

    const list = categories.length > 0
      ? <CategoryTable categories={categories} url="categories" />
      : null

    return <React.Fragment>
      {displayTitle()}
      <ul>{list}</ul>
    </React.Fragment>
  }

  return displayGoalsBody()
}
