import React, { Fragment, useEffect, useRef, useState } from "react"

import { getMonthlyBalanceCategories } from "../common/api_monthlybalances"
import type { IncomeCategory, MonthlyBalanceCategoriesResponse } from "../common/interfaces"
import { FailedBanner, createMonthlyBalanceCategoryGuidance, CustomSpinner, CategoryTable } from "./shared"

export const MonthlyBalanceCategories = (): JSX.Element => {
  const [categories, setCategories] = useState<IncomeCategory[]>([])
  const initialRender = useRef(true)
  const [invalidResponse, setInvalidResponse] = useState(false)
  const [apiResponseComplete, setApiResponseComplete] = useState(false)

  useEffect(() => {
    const abortController = new AbortController()
    const signal = abortController.signal

    if (initialRender.current) {
      initialRender.current = false
      getMonthlyBalanceCategories(signal).then((data: MonthlyBalanceCategoriesResponse) => {
        if (data?.response?.ok && Array.isArray(data?.data?.results)) {
          setInvalidResponse(false)
          const responseCategories = data.data?.results
          setCategories(responseCategories)
        } else {
          setInvalidResponse(true)
        }
      })
        .catch((e: Error) => {
          console.log(e)
          if (e.name !== "AbortError") {
            setInvalidResponse(true)
          }
        })
        .finally(() => {
          if (!signal.aborted) {
            setApiResponseComplete(true)
          }
        })
    }

    return () => { abortController.abort() }
  }, [])

  const displayTitle = (): JSX.Element => <div className="col-sm-12"><h3>List Monthly Balance Categories</h3></div>

  const displayBody = (): JSX.Element => {
    if (!apiResponseComplete) {
      return CustomSpinner()
    }

    if (invalidResponse) {
      return FailedBanner()
    }

    if (apiResponseComplete && categories.length === 0) {
      return <Fragment>{displayTitle()}{createMonthlyBalanceCategoryGuidance()}</Fragment>
    }

    const list = categories.length > 0
      ? <CategoryTable categories={categories} url="monthly-balances/categories" />
      : null

    return <React.Fragment>
      {displayTitle()}
      <ul>{list}</ul>
    </React.Fragment>
  }

  return displayBody()
}
