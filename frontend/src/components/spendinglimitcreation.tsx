import { useForm } from "react-hook-form"
import React, { useState, useRef, useEffect, useCallback } from "react"
import { createSpendingLimit } from "../common/api_spendinglimits"
import { getCategories } from "../common/api_categories"
import type { Category, SpendingLimitFormInputs, SpendingLimitResponse, ReactFragment } from "../common/interfaces"
import { createCategoryGuidance, displayCategoriesPullDown, styleErrorMessage, FailedBanner } from "./shared"
import { renderDismissableAlert } from "../common/utilities"

const defaultValues: SpendingLimitFormInputs = {
  amount: 0,
  category: 0,
  currency: "",
  days: 0,
  text: ""
}

export const SpendingLimitCreation = (): JSX.Element => {
  // eslint-disable-next-line @typescript-eslint/unbound-method
  const { register, handleSubmit, setError, formState: { errors }, reset } = useForm<SpendingLimitFormInputs>()
  const [isSending, setIsSending] = useState(false)
  const [categories, setCategories] = useState<Category[]>([])
  const initialRender = useRef(true)
  const [showDismissable, setShowDismissable] = useState(false)
  const [isUpdateSuccess, setIsUpdateSuccess] = useState(false)
  const autoCloseDismissibleAfterMs = 1500
  const [criticalError, setCriticalError] = useState(false)

  const [serverError, setServerError] = useState("")

  useEffect(() => {
    const abortController = new AbortController()
    const signal = abortController.signal

    if (initialRender.current) {
      initialRender.current = false
      setIsSending(true)

      const fetchCategories = async (): Promise<void> => {
        const res = await getCategories(signal)
        if (!res?.response?.ok) {
          setCriticalError(true)
        } else if (res?.response?.ok && Array.isArray(res?.data?.results)) {
          setCriticalError(false)
          setCategories(res?.data.results)
        } else {
          setCriticalError(true)
        }
      }

      fetchCategories().catch((err) => {
        if (err instanceof Error && err.name !== "AbortError") {
          setCriticalError(true)
          console.log("Failed to fetch page: ", err)
        }
      }).finally(() => {
        if (!signal.aborted) {
          setIsSending(false)
        }
      })
    }

    return () => { abortController.abort() }
  }, [])

  const onSubmit = useCallback((formValues: SpendingLimitFormInputs) => {
    if (isSending) return
    setShowDismissable(false)
    setIsSending(true)
    const abortController = new AbortController()
    const signal = abortController.signal

    createSpendingLimit(formValues, signal)
      .then((res: SpendingLimitResponse) => {
        if (!res?.response?.ok) {
          if (typeof res.data === "object" && Object.keys(res.data).length > 0) {
            for (const [key, value] of Object.entries(res.data)) {
              console.log("Spending Limit creation form error(s):", key, value)
              // Set error for existing fields
              if (key in formValues) {
                setError(key as keyof SpendingLimitFormInputs, { message: value[0] })
              } else {
                // Other validation errors
                setServerError(value[0])
              }
            }
          } else {
            // Handle 40X,50X errors in here
            setServerError("Something is wrong with the server, try again later!")
          }
          setIsUpdateSuccess(false)
        } else {
          reset(defaultValues)
          setServerError("")
          setIsUpdateSuccess(true)
        }
      })
      .catch(error => {
        if (error instanceof Error && error.name !== "AbortError") {
          console.log("SpendingLimit creation frontend error:", error)
        }
        setIsUpdateSuccess(false)
      })
      .finally(() => {
        if (!signal.aborted) {
          setIsSending(false)
          setShowDismissable(true)
          const timer = setTimeout(() => {
            setShowDismissable(false)
          }, autoCloseDismissibleAfterMs)
          return () => { clearTimeout(timer) }
        }
      })

    return () => { abortController.abort() }
  }, [reset])

  const displayTitle = (): JSX.Element => {
    return <h3>Create a Spending Limit</h3>
  }

  const renderDismissable = (): JSX.Element | null =>
    renderDismissableAlert({
      setShowDismissable,
      getIsShowDismissable: () => showDismissable,
      successMessage: "Spending Limit created!",
      failureMessage: "Spending Limit creation failed :(",
      getIsUpdateSuccess: () => isUpdateSuccess
    })

  const displayCreationForm = (): ReactFragment => {
    return (
      <form onSubmit={handleSubmit(onSubmit)} className="form">
        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="amount_field">Amount</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input className="form-control" name="amount" type="number" id="amount_field"
              ref={register({
                required: "This field is required.",
                min: {
                  value: 1,
                  message:
                    "Amount should be more than 0"
                },
                max: {
                  value: 2147483647,
                  message:
                    "Amount should be less than 2147483647"
                }
              })}
            />
            {errors?.amount !== undefined && errors?.amount !== null ? styleErrorMessage(errors.amount?.message as string) : null}
          </div>
        </div>
        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="category_field">Category</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            {displayCategoriesPullDown(categories, register)}
            {(errors?.category?.message !== null && errors?.category?.message !== undefined) ? styleErrorMessage(errors.category.message) : null}
          </div>
        </div>

        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="currency_field">Currency</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input id="currency_field" className="form-control" name="currency" type="text" placeholder="Insert the currency"
              ref={register({
                required: "This field is required.",
                maxLength: {
                  value: 3,
                  message: "Max 3 chars"
                }
              })}
            />
            {errors.currency !== undefined && errors.currency !== null ? styleErrorMessage(errors.currency.message as string) : null}
          </div>
        </div>

        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="days_field">Days</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input className="form-control" name="days" type="number" id="days_field"
              ref={register({
                required: "This field is required.",
                min: {
                  value: 1,
                  message:
                    "Days should be more than 0"
                }
              })}
            />
            {errors.days !== undefined && errors.days !== null ? styleErrorMessage(errors.days.message as string) : null}
          </div>
        </div>

        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="text_field">Text</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input id="text_field" className="form-control" name="text" type="text" placeholder="Insert the Spending Limit text"
              ref={register({
                required: "This field is required.",
                maxLength: {
                  value: 150,
                  message: "Max 150 chars"
                }
              })}
            />
            {errors.text !== undefined && errors.text !== null ? styleErrorMessage(errors.text.message as string) : null}
          </div>
        </div>

        <div className="form-group">
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            {serverError !== "" ? styleErrorMessage(serverError) : null}
            {/* NOTE: the id="CreateButton" is used in the functional tests, do not remove it */}
            <button id="CreateButton" className="btn btn-secondary my-2 my-sm-0" disabled={isSending}>{isSending ? "Submitting..." : "Submit"}</button>
          </div>
        </div>

      </form>
    )
  }

  const displayView = (): JSX.Element => {
    return <React.Fragment>
      {criticalError ? FailedBanner() : null}
      {displayTitle()}
      {renderDismissable()}
      {categories !== null && Array.isArray(categories) && categories.length > 0 ? displayCreationForm() : createCategoryGuidance()}
    </React.Fragment>
  }

  return displayView()
}
