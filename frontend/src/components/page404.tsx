import React from "react"

export const Page404 = (): JSX.Element => {
  const displayTitle = (): JSX.Element => {
    return <React.Fragment><h4>Error 404</h4></React.Fragment>
  }

  const displayBody = (): JSX.Element => {
    return <div>{"The page you're looking for does not seem to exist..."}</div>
  }

  const displayView = (): JSX.Element => {
    return <React.Fragment>
      { displayTitle() }
      { displayBody() }
    </React.Fragment>
  }

  return displayView()
}
