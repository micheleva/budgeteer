import { useForm } from "react-hook-form"
import Alert from "react-bootstrap/Alert"
import { useParams, Link } from "react-router-dom"
import React, { useEffect, useRef, useState, useCallback } from "react"
import { getBudget, patchBudget } from "../common/api_budgets"
import type { BudgetWithId, SavingBudgetEditFormInputs } from "../common/interfaces"
import { CustomSpinner, styleErrorMessage } from "./shared"

const SavingBudgetEdit = (): JSX.Element => {
  // eslint-disable-next-line @typescript-eslint/unbound-method
  const { errors, handleSubmit, register, setError, setValue } = useForm<SavingBudgetEditFormInputs>()
  const [isSending, setIsSending] = useState(false)
  const { id } = useParams()
  const [budget, setBudget] = useState<BudgetWithId>()
  const initialRender = useRef(true)
  const [showDismissable, setShowDismissable] = useState(false)
  const [isUpdateSuccess, setIsUpdateSuccess] = useState(false)
  const autoCloseDismissibleAfterMs = 1500
  const [invalidBudget, setInvalidbudget] = useState(false)

  const [genericFormError, setGenericFormError] = useState("")

  const fetchData = useCallback((displayModal = true, signal?: AbortSignal) => {
    if (isSending) return
    setIsSending(true)
    setIsUpdateSuccess(false)

    if (id !== null && id !== undefined) {
      getBudget(Number(id), signal).then((bdg) => {
        if (!bdg?.response?.ok === null && !bdg?.response?.ok === undefined) {
          setInvalidbudget(true)
          setIsUpdateSuccess(false)
        } else if (bdg?.response.ok) {
          setInvalidbudget(false)
          setIsUpdateSuccess(false)
          const budgetResponse = bdg.data
          setBudget(budgetResponse)
          // IMPORTANT: We need to call setValue() AFTER render (with the form) has run
          // of the form will render undefined (i.e. "") in each field
          // as per https://stackoverflow.com/a/59547360
          // This means we can not use a flag like apiResponseComplete
          // as we do in goalstab.tsx
          setValue("amount", parseFloat(budgetResponse.amount))
          setValue("allocated_amount", parseFloat(budgetResponse.allocated_amount))
        } else {
          setInvalidbudget(true)
          setIsUpdateSuccess(false)
        }
      }).catch((err) => {
        setInvalidbudget(true)
        setIsUpdateSuccess(false)
        console.log("Failed to fetch page: ", err)
      }).finally(() => {
        setIsSending(false)
        if (displayModal === true) {
          setShowDismissable(true)
        }
      })
    } else {
      setInvalidbudget(true)
      setIsSending(false)
    }
  }, [id])

  useEffect(() => {
    const abortController = new AbortController()
    const signal = abortController.signal

    if (initialRender.current) {
      initialRender.current = false
      fetchData(signal)
    }

    return () => { abortController.abort() }
  }, [fetchData])

  const onSubmit = useCallback((formValues: SavingBudgetEditFormInputs) => {
    setIsSending(true)
    const abortController = new AbortController()
    const signal = abortController.signal
    let timeoutId: ReturnType<typeof setTimeout>

    patchBudget(Number(id), formValues, signal).then((res) => {
      if (!(res?.response.ok)) {
        if (res?.data != null && res?.data !== undefined) {
          for (const [key, value] of Object.entries(res.data)) {
            // Set error for existing fields
            if (key in formValues) {
              setError(key as keyof SavingBudgetEditFormInputs, { message: value[0] })
            } else {
              // Other validation errors
              setGenericFormError(value[0])
            }
          }
        } else {
          // Handle 40X,50X errors in here
          setGenericFormError("Something is wrong with the server, try again later!")
        }
        setIsUpdateSuccess(false)
      } else {
        setGenericFormError("")
        setIsUpdateSuccess(true)
      }
    }).catch((err) => {
      // Only log and handle errors that aren't from aborting
      if (err.name !== 'AbortError') {
        console.log(err)
        setIsUpdateSuccess(false)
      }
    }).finally(() => {
      if (!signal.aborted) {
        setIsSending(false)
        setShowDismissable(true)
        timeoutId = setTimeout(() => {
          setShowDismissable(false)
        }, autoCloseDismissibleAfterMs)
      }
    })

    // This is the actual cleanup function that should be returned from useCallback
    return () => { 
      abortController.abort()
      if (timeoutId) clearTimeout(timeoutId)
    }
  }, [id, autoCloseDismissibleAfterMs])

  const displayTitle = (budget: BudgetWithId): JSX.Element => {
    // FIXME be a more defensive: we might get malformed data from the backend, and a budget might be missing the category and the setasidecategory as well
    const categoryText = budget?.savingcategory?.text
    const period = budget.month === null ? `the whole ${budget.year} year` : `the month of ${budget.year}-${budget.month}`
    const msg = `Editing Budget for Saving category "${categoryText}" for ${period}`
    return <h3>{msg}</h3>
  }

  const missingOrUnavailableHeader = (): JSX.Element => {
    // NOTE: This "invalid-budget-marker" id is used by the functional tests, do not remove it!
    return <Alert id="invalid-budget-marker" variant="danger">
              <Alert.Heading>Sorry, it seems the Budget you&apros;re trying to editing does not exist...or there was an error. Please try again later, or reload the page</Alert.Heading>
      </Alert>
  }

  const renderWIPAlert = (): JSX.Element => {
    return <Alert variant={"warning"}>
          <Alert.Heading>Work in progress</Alert.Heading>
          <div className="text-dark">After v1.4 is released, users will be able to be mass update/create Budgets in the same fashion as MonthlyBalances. Bare with me while I&apros;m at it :D </div>
        </Alert>
  }

  const displayForm = (): JSX.Element => {
    return (
      <form onSubmit={handleSubmit(onSubmit)} className="form">

        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="amount_field">Amount</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input className="form-control" name="amount" type="number" id="amount_field"
               ref={register({
                 required: "This field is required.",
                 min: {
                   value: 0,
                   message: "Amount should be more or equal to 0"
                 }
               })}
            />
            {(errors.amount != null) ? styleErrorMessage(errors.amount.message as string) : null}
          </div>
        </div>

        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="allocated_amount_field">Allocated Amount</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input className="form-control" name="allocated_amount" type="number" id="allocated_amount_field"
               ref={register({
                 min: {
                   value: 0,
                   message: "Allocated Amount should be more or equal to 0"
                 }
               })}
            />
            {(errors.allocated_amount != null) ? styleErrorMessage(errors.allocated_amount.message as string) : null}
          </div>
        </div>

        <div className="form-group">
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            {(genericFormError.length > 0) ? styleErrorMessage(genericFormError) : null}
            <button className="btn btn-secondary my-2 my-sm-0" disabled={isSending}>{ isSending ? "Submitting..." : "Submit" }</button>
          </div>
        </div>

        </form>
    )
  }

  const generateBackLink = (budget: BudgetWithId): JSX.Element => {
    const currYear = new Date().getFullYear()
    const currMonth = new Date().getMonth() + 1
    let backLink = ""
    let text = ""
    if (budget === null || budget === undefined) {
      // NOTE Case where the budget is not yeat loaded
      console.log("Budget seems not to be loaded yet")
      backLink = `/app/budgets/date/${currYear}/${currMonth}`
      text = `the month of ${currYear}-${currMonth}`
      return <Link to={backLink}>Back to {text} Budgets</Link>
    }

    if (budget?.year === null) {
      // NOTE Case where the budget is horribly malformed as it has year set to null
      console.error("Budget seems to be malformed, having year set to null")
      backLink = `/app/budgets/date/${currYear}/${currMonth}`
      text = `the month of ${currYear}-${currMonth}`
      return <Link to={backLink}>Back to {text} Budgets</Link>
    }

    backLink = `/app/budgets/date/${budget.year}`
    text = `the whole ${budget.year} year`
    if (budget?.month !== null) {
      text = `the month of ${budget.year}-${budget.month}`
      backLink += `/${budget.month}`
    }
    return <Link to={backLink}>Back to the Budgets for {text}</Link>
  }

  const displayView = (budget: BudgetWithId): JSX.Element => {
    // IMPORTANT: We need to call setValue() AFTER render (with the form) has run
    // of the form will render undefined (i.e. "") in each field
    // as per https://stackoverflow.com/a/59547360

    // This means we can not use a flag like apiResponseComplete as we do in goaledittab.tsx
    // FIXME: BUGFIX-245
    // Possible workardound: make the form 'hidden' using css, and show it after the loading is complete
    // TODO test that out and see if the setValue() binding works when the form is hidden
    if (invalidBudget) {
      return missingOrUnavailableHeader()
    } else if (budget !== null && budget !== undefined) {
      return <React.Fragment>
        <div>
          {showDismissable
            ? <Alert variant={isUpdateSuccess ? "success" : "danger"} onClose={() => { setShowDismissable(false) }} dismissible>
              <Alert.Heading>{isUpdateSuccess ? "Saving Budget updated!" : "Saving Budget update failed :("}</Alert.Heading>
            </Alert>
            : null}
          {generateBackLink(budget)}
          { renderWIPAlert() }
          <div>{displayTitle(budget)}</div>
        </div>

        { displayForm() }
      </React.Fragment>
    } else {
      return CustomSpinner()
    }
  }

  return displayView(budget)
}

export default SavingBudgetEdit
