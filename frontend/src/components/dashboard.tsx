import React from "react"
import Col from "react-bootstrap/Col"
import Row from "react-bootstrap/Row"

import { monthlyBalanceLastSixMonthsWidget } from "./widgets/last_6_months_monthly_balance"
import { missingMonthlyBudgetsWidget, missingYearlyBudgetsWidget, missingMonthlySavingBudgetsWidget, missingYearlySavingBudgetsWidget } from "./widgets/missing_budgets"
import { expensePieWidget } from "./widgets/expense_pie"
import { yearlyOverbudgetedWidget } from "./widgets/overbudget"
import { assetsStatsWidget } from "./widgets/assets"
import { spendingLimitsWidget } from "./widgets/spending_limit"
import { dividendsYearlyStatsWidget } from "./widgets/dividends_yearly_stat"

const Dashboard = (): JSX.Element => {
  const displayTitle = (): React.ReactElement => <h3 className="text-center">Dashboard</h3>

  const displayView = (): React.ReactElement => {
    return (
      <React.Fragment>
       <Row>
          <Col xs md lg="12">
            { displayTitle() }
          </Col>
       </Row>
       <Row>
           { expensePieWidget() }
           { spendingLimitsWidget() }
           { monthlyBalanceLastSixMonthsWidget() }
           { assetsStatsWidget() }
           { dividendsYearlyStatsWidget() }
           { missingMonthlyBudgetsWidget() }
           { missingYearlyBudgetsWidget() }
           { missingMonthlySavingBudgetsWidget() }
           { missingYearlySavingBudgetsWidget() }
           { yearlyOverbudgetedWidget() }

        </Row>
      </React.Fragment>
    )
  }

  return displayView()
}

export default Dashboard
