import { useForm } from "react-hook-form"
import Alert from "react-bootstrap/Alert"
import React, { useState, useCallback } from "react"
import { createMonthlyBalanceCategory } from "../common/api_monthlybalances"
import type { MonthlyBalanceCategoryFormInputs, ReactFragment } from "../common/interfaces"
import { styleErrorMessage } from "./shared"

const defaultValues: MonthlyBalanceCategoryFormInputs = {
  text: "",
  is_foreign_currency: false
}

const MonthlyBalanceCategoryCreation = (): JSX.Element => {
  // eslint-disable-next-line @typescript-eslint/unbound-method
  const { register, handleSubmit, setError, formState: { errors }, reset } = useForm<MonthlyBalanceCategoryFormInputs>()
  const [isSending, setIsSending] = useState(false)
  const [showDismissable, setShowDismissable] = useState(false)
  const [isUpdateSuccess, setIsUpdateSuccess] = useState(false)
  const autoCloseDismissibleAfterMs = 1500

  const [genericFormError, setGenericFormError] = useState("")

  const onSubmit = useCallback((formValues: MonthlyBalanceCategoryFormInputs) => {
    if (isSending) return
    setShowDismissable(false)
    setIsSending(true)
    const abortController = new AbortController()
    const signal = abortController.signal

    createMonthlyBalanceCategory(formValues, signal)
      .then((res) => {
        if (!res?.response?.ok) {
          if (res?.data !== undefined && res?.data !== null && typeof res?.data === "object") {
            for (const [key, value] of Object.entries(res.data)) {
              console.log("Monthly Balance creation form error(s):", key, value)
              // Set error for existing fields
              if (key in formValues) {
                setError(key as keyof MonthlyBalanceCategoryFormInputs, { message: value[0] })
              } else {
              // Other validation errors
                setGenericFormError(value[0])
              }
            }
          } else {
          // Handle 40X,50X errors in here
            setGenericFormError("Something is wrong with the server, try again later!")
          }
          setIsUpdateSuccess(false)
        } else {
          reset(defaultValues)
          setGenericFormError("")
          setIsUpdateSuccess(true)
        }
      })
      .catch(error => {
        if (error instanceof Error && error.name !== "AbortError") {
          console.log("Monthly Balance creation frontend error:", error)
          setIsUpdateSuccess(false)
        }
      })
      .finally(() => {
        if (!signal.aborted) {
          setIsSending(false)
          setShowDismissable(true)
          const timer = setTimeout(() => {
            setShowDismissable(false)
          }, autoCloseDismissibleAfterMs)
          return () => { clearTimeout(timer) }
        }
      })

    return () => { abortController.abort() }
  }, [reset])

  const displayTitle = (): JSX.Element => <div className="col-sm-12"><h3>Create a Monthly Balance</h3></div>

  const renderDismissable = (): JSX.Element | null => {
    if (showDismissable) {
      return <Alert variant={isUpdateSuccess ? "success" : "danger"} onClose={() => { setShowDismissable(false) }} dismissible>
              <Alert.Heading>{isUpdateSuccess ? "Monthly Balance created!" : "Monthly Balance creation failed :("}</Alert.Heading>
            </Alert>
    } else {
      return null
    }
  }

  const displayCreationForm = (): ReactFragment => {
    return (
       <form onSubmit={handleSubmit(onSubmit)} className="form">
          <div className="form-group">
            <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="text_field">Monthly Balance Category Name</label>
            <div className="col-sm-3">
              <input name="text" type="text" id="text_field" placeholder="Enter the monthly balance category name" className="form-control"
                 ref={register({
                   required: "This field is required.",
                   maxLength: {
                     value: 40,
                     message: "Max 40 chars"
                   }
                 })}
              />
              {errors?.text !== null && errors?.text !== undefined ? styleErrorMessage(errors.text.message as string) : null}
            </div>
          </div>

          <div className="form-group">
            <div className="col-sm-6 col-md-4 3 col-lg-3">
              <label><input type="checkbox" id="is_foreign_currency_field" name="is_foreign_currency" ref={register()} />&nbsp;Is archived</label>
              {errors.is_foreign_currency !== null && errors.is_foreign_currency !== undefined ? styleErrorMessage(errors.is_foreign_currency.message as string) : null}
            </div>
          </div>

          <div className="form-group">
            <div className="col-sm-6 col-md-4 3 col-lg-3">
              { genericFormError !== null && genericFormError !== undefined ? styleErrorMessage(genericFormError) : null}
              <button className="btn btn-secondary my-2 my-sm-0" disabled={isSending}>{ isSending ? "Submitting..." : "Submit" }</button>
            </div>
          </div>
      </form>
    )
  }

  const displayView = (): JSX.Element => {
    // Monthly Balance Category has no foreign keys, hence we do not need a spinner, as we do not fetch other entities
    return <React.Fragment>
      { displayTitle() }
      { renderDismissable() }
      { displayCreationForm() }
    </React.Fragment>
  }

  return displayView()
}

export default MonthlyBalanceCategoryCreation
