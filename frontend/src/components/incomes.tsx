import React, { useEffect, useState } from "react"
import { IncomesTable, DismissableRecoverableErrorBanner, CustomSpinner } from "./shared"
import { getIncomesByDates, deleteIncome } from "../common/api_incomes"
import type { Income, IncomesResponse } from "../common/interfaces"
import { getAMonthAgoDate, preventFormSubmit, sortByDateValue, yearMonthIsValidDate } from "../common/utilities"

export const ListIncomes = (): JSX.Element => {
  const [isSending, setIsSending] = useState(false)
  const [incomes, setIncomes] = useState<Income[]>([])
  const [start, setstart] = useState<string>(getAMonthAgoDate())
  const [end, setend] = useState<string>(new Date().toLocaleDateString("en-CA"))

  const [apiResponseComplete, setApiResponseComplete] = useState(false)
  const [recoverableError, setRecoverableError] = useState(false)

  const fetchData = React.useCallback((signal?: AbortSignal) => {
    if (!yearMonthIsValidDate(start) || !yearMonthIsValidDate(end)) {
      return
    }
    if (isSending) return
    setIsSending(true)

    getIncomesByDates(start, end, signal)
      .then((data: IncomesResponse) => {
        if (data?.response?.ok && Array.isArray(data?.data?.results)) {
          setRecoverableError(false)
          setIncomes(data.data.results.sort(sortByDateValue))
        } else {
          setRecoverableError(true)
          setIncomes([])
        }
      })
      .catch((err: Error) => {
        console.log("Failed to fetch page: ", err)
        setRecoverableError(true)
      }).finally(() => {
        setIsSending(false)
        setApiResponseComplete(true)
      })
  }, [start, end])

  const removeFromLocalCache = (id: number): void => {
    setIncomes((original) => original.filter((exp) => exp.id !== id))
  }

  useEffect(() => {
    const abortController = new AbortController()
    const signal = abortController.signal

    fetchData(signal)

    return () => { abortController.abort() }
  }, [fetchData])

  const displayTitle = (): JSX.Element => <h3>Incomes by date range</h3>

  const displayView = (): JSX.Element => {
    if (!apiResponseComplete) {
      return CustomSpinner()
    }

    return <React.Fragment>
      <form className="form" onSubmit={preventFormSubmit}>
      { displayTitle() }
      { recoverableError ? DismissableRecoverableErrorBanner(setRecoverableError) : null }
      <div className="form-group">
        <label className="control-label col-sm-2" htmlFor="start">From&nbsp;</label>
        <div className="col-sm-2">
          <input type="date" id="start" name="start" onChange={e => { setstart(e.target.value) }} value={start}/>
          {/* {{errors.start ? styleErrorMessage(errors.start.message as string) : null}} */}
        </div>
      </div>

      <div className="form-group">
        <label className="control-label col-sm-2" htmlFor="end">To&nbsp;</label>
        <div className="col-sm-2">
          <input type="date" id="end" name="end" onChange={e => { setend(e.target.value) }} value={end}/>
          {/* {{errors.end ? styleErrorMessage(errors.end.message as string) : null}} */}
        </div>
      </div>

      {/* <input type="date" onChange={e => { setstart(e.target.value) }} value={start}/> */}
      {/* <input type="date" onChange={e => { setend(e.target.value) }} value={end}/>* /}
      {/* NICE TO HAVE: show an error if start date is after end date */}

      <div className="form-group">
        <div className="col-sm-12">
          <button id="dateFilterSubmit" className="btn btn-secondary my-2 mr-2" onClick={fetchData} disabled={isSending}>{ isSending ? "Fetching..." : "Fetch" }</button>
          <IncomesTable data={incomes}
            deleteFunc={ async (id: number) => await deleteIncome(id)}
            updateParentData={(id: number) => { removeFromLocalCache(id) }}
          />
          </div>
        </div>

      </form>
    </React.Fragment>
  }

  return displayView()
}
