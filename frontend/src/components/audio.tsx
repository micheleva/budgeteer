import React, { useState, useCallback, useRef, useEffect } from "react"
import Alert from "react-bootstrap/Alert"
import { Button } from "react-bootstrap"
import { useForm } from "react-hook-form"
import { createExpense } from "../common/api_expenses"
import { isAsrEnabled, postAudio } from "../common/api_audio"
import { getCategories } from "../common/api_categories"
import type { Category, ExpenseFormInputs } from "../common/interfaces"
import { createCategoryGuidance, CustomSpinner, styleErrorMessage } from "./shared"
import { FormatGuidance } from "./audio-guidance"
import { createExpenseSubmissionHandler, checkEnvironment } from "../common/utilities"

const defaultValues: ExpenseFormInputs = {
  amount: 0,
  category_id: 0,
  note: "",
  date: ""
}

export const AudioRecorder = (): JSX.Element => {
  const [isRecording, setIsRecording] = useState(false)
  const [audioURL, setAudioURL] = useState("")
  const [audioBlob, setAudioBlob] = useState<Blob | null>(null)
  const [result, setResult] = useState<any>(null)
  const [error, setError] = useState<string | null>(null)
  const mediaRecorder = useRef<MediaRecorder | null>(null)
  const audioChunks = useRef<Blob[]>([])
  const initialRender = useRef(true)

  // eslint-disable-next-line @typescript-eslint/unbound-method
  const { register, handleSubmit, formState: { errors }, reset, setValue } = useForm()
  const [genericFormError, setGenericFormError] = useState("")
  const [isRecorderReady, setIsRecorderReady] = useState(false)

  const [isSending, setIsSending] = useState(false)
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [showDismissable, setShowDismissable] = useState(true)
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [isUpdateSuccess, setIsUpdateSuccess] = useState(false)

  // NOTE: there were some issues with hooks being executed inside conditionals, so I moved the state to the top
  // Let's clean up the components after the 1.4 release. This is a temporary fix, since this feature is still in alpha <===
  const [state, setState] = useState({
    // isRecording: false,
    // audioURL: "",
    // audioBlob: null as Blob | null,
    // result: null as any,
    categories: [] as Category[],
    isAsrAvailable: false,
    isBackendAvailable: false,
    // isRecorderReady: false,
    // isSending: false,
    // showDismissable: false,
    // isUpdateSuccess: false,
    errorMessages: [] as string[],
    isApiCompleted: false
  })

  useEffect(() => {
    // Actual setup the audio recording bits
    const setupAudio = async (): Promise<void> => {
      try {
        if (!navigator.mediaDevices?.getUserMedia) {
          throw new Error("MediaDevices API not available.")
        }

        const stream = await navigator.mediaDevices.getUserMedia({ audio: true })
        const AudioContext = window.AudioContext || window.webkitAudioContext
        const audioContext = new AudioContext()
        const source = audioContext.createMediaStreamSource(stream)
        const analyser = audioContext.createAnalyser()
        source.connect(analyser)
        mediaRecorder.current = new MediaRecorder(stream)
        mediaRecorder.current.ondataavailable = (event) => {
          audioChunks.current.push(event.data)
        }
        mediaRecorder.current.onstop = () => {
          convertToMono()
        }
        setIsRecorderReady(true)
      } catch (err: unknown) {
        setState((prev) => ({
          ...prev,
          errorMessages: [...prev.errorMessages, "Error setting up audio:" + (err instanceof Error ? err.message : String(err))]
        }))
        console.error(err)
      }
    }
    setupAudio()
  }, [])

  const fetchData = useCallback(async () => {
    const abortController = new AbortController()
    const signal = abortController.signal

    const isAsrAvailableAndfetchCategories = async (): Promise<void> => {
      try {
        const categoriesRes = await getCategories(signal)
        if (categoriesRes?.response?.ok && Array.isArray(categoriesRes.data.results)) {
          // setCategories(categoriesRes.data.results)
          console.log("completed fetch categories")
          console.log("IMPLEMENT ME: we are not showing an error message if the categories are not fetched")
        }
        const envInfo = checkEnvironment()

        // Check for browser limitations
        let isBrowserOk = false
        const newErrors = [] as string[]
        if (!envInfo.isHttps && !envInfo.isLocalhost) {
          newErrors.push("Audio recording is only available on HTTPS or localhost")
          isBrowserOk = false
        } else {
          isBrowserOk = true
        }

        // Check for backend limitations
        const isBackendAvailable = await isAsrEnabled()
        if (!isBackendAvailable) {
          newErrors.push("ASR is not enabled on this budgeteer instance")
        }

        // NOTE: We set the state only once, to avoid multiple re-renders
        setState((prev) => ({
          ...prev,
          categories: categoriesRes.data.results,
          isBackendAvailable,
          isAsrAvailable: isBrowserOk,
          errorMessages: [...prev.errorMessages, ...newErrors],
          isApiCompleted: true
        }))
      } catch (err: unknown) {
        setState((prev) => ({
          ...prev,
          errorMessages: [...prev.errorMessages, "Failed to fetch categories: " + (err instanceof Error ? err.message : String(err))]
        }))
      }
    }
    await isAsrAvailableAndfetchCategories()

    return () => { abortController.abort() }
  }, [])

  useEffect(() => {
    if (initialRender.current) {
      initialRender.current = false
      fetchData().catch((err) => {
        setState((prev) => ({
          ...prev,
          errorMessages: [...prev.errorMessages, "Failed to fetch data: " + err.message]
        }))
      })
    }
  }, [fetchData])

  const startRecording = (): void => {
    try {
      if (mediaRecorder.current === null) {
        setState((prev) => ({
          ...prev,
          errorMessages: [...prev.errorMessages, "MediaRecorder is not initialized"]
        }))
        console.error("MediaRecorder is not initialized")
        return
      }
      audioChunks.current = []
      mediaRecorder.current.start()
      setIsRecording(true)
    } catch (err: unknown) {
      setState((prev) => ({
        ...prev,
        errorMessages: [...prev.errorMessages, "Error starting recording: " + (err instanceof Error ? err.message : String(err))]
      }))
      console.error("Error starting recording:", error)
    }
  }

  const stopRecording = (): void => {
    mediaRecorder?.current?.stop()
    setIsRecording(false)
  }

  const convertToMono = async (): Promise<void> => {
    try {
      const audioContext = new (window.AudioContext || window.webkitAudioContext)()
      const audioBuffer = await audioContext.decodeAudioData(await new Blob(audioChunks.current).arrayBuffer())

      // On mobile devices, the audioBuffer might have only one channel
      const numberOfChannels = audioBuffer.numberOfChannels

      if (numberOfChannels === 0) {
        throw new Error("Audio buffer has no channels.")
      }

      const monoBuffer = audioContext.createBuffer(1, audioBuffer.length, audioBuffer.sampleRate)
      const monoChannelData = monoBuffer.getChannelData(0)

      if (numberOfChannels === 1) {
        // If the audio is already mono, just copy the data
        monoChannelData.set(audioBuffer.getChannelData(0))
      } else if (numberOfChannels >= 2) {
        // If stereo or more, average the channels
        for (let i = 0; i < audioBuffer.length; i++) {
          monoChannelData[i] = (audioBuffer.getChannelData(0)[i] + audioBuffer.getChannelData(1)[i]) / 2
        }
      }

      const monoWav = encodeWAV(monoBuffer)
      const monoBlob = new Blob([monoWav], { type: "audio/wav" })
      const audioUrl = URL.createObjectURL(monoBlob)
      setAudioURL(audioUrl)
      setAudioBlob(monoBlob)
    } catch (err: unknown) {
      setState((prev) => ({
        ...prev,
        errorMessages: [...prev.errorMessages, "Error converting audio to mono: " + (err instanceof Error ? err.message : String(err))]
      }))

      console.error("Error converting to mono:", error)
    }
  }

  const encodeWAV = (audioBuffer): ArrayBuffer => {
    const numChannels = 1
    const sampleRate = audioBuffer.sampleRate
    const format = 1 // PCM
    const bitDepth = 16

    const buffer = new ArrayBuffer(44 + audioBuffer.length * 2)
    const view = new DataView(buffer)

    /* RIFF identifier */
    writeString(view, 0, "RIFF")
    /* RIFF chunk length */
    view.setUint32(4, 36 + audioBuffer.length * 2, true)
    /* RIFF type */
    writeString(view, 8, "WAVE")
    /* format chunk identifier */
    writeString(view, 12, "fmt ")
    /* format chunk length */
    view.setUint32(16, 16, true)
    /* sample format (raw) */
    view.setUint16(20, format, true)
    /* channel count */
    view.setUint16(22, numChannels, true)
    /* sample rate */
    view.setUint32(24, sampleRate, true)
    /* byte rate (sample rate * block align) */
    view.setUint32(28, sampleRate * 2, true)
    /* block align (channel count * bytes per sample) */
    view.setUint16(32, numChannels * 2, true)
    /* bits per sample */
    view.setUint16(34, bitDepth, true)
    /* data chunk identifier */
    writeString(view, 36, "data")
    /* data chunk length */
    view.setUint32(40, audioBuffer.length * 2, true)

    const samples = new Int16Array(audioBuffer.length)
    const channelData = audioBuffer.getChannelData(0)
    for (let i = 0; i < audioBuffer.length; i++) {
      samples[i] = channelData[i] < 0 ? channelData[i] * 0x8000 : channelData[i] * 0x7FFF
    }
    const sampleBytes = new Uint8Array(samples.buffer)
    for (let i = 0; i < sampleBytes.length; i++) {
      view.setUint8(44 + i, sampleBytes[i])
    }

    return buffer
  }

  const writeString = (view, offset, string): void => {
    for (let i = 0; i < string.length; i++) {
      view.setUint8(offset + i, string.charCodeAt(i))
    }
  }

  const sendAudioToBackend = async (): Promise<void> => {
    if (audioBlob === null) {
      console.error("No audio recorded")
      setState((prev) => ({
        ...prev,
        errorMessages: [...prev.errorMessages, "No audio recorded"]
      }))
      return
    }
    try {
      const response = await postAudio(audioBlob)
      setResult(response)
      setError(null)
    } catch (err: unknown) {
      setState((prev) => ({
        ...prev,
        errorMessages: [...prev.errorMessages, "Error sending audio to backend: " + (err instanceof Error ? err.message : String(err))]
      }))
      console.error("Error sending audio to backend:", error)
    }
  }

  // Render error messages in the UI
  const renderErrorMessages = (): JSX.Element | null => {
    if (state.errorMessages.length > 0) {
      return (
        <div className="error-messages">
          {state.errorMessages.map((msg, index) => (
            <div key={index} className="text-red-500 font-bold">
              Error: {msg}
            </div>
          ))}
        </div>
      )
    }
    return null
  }

  useEffect(() => {
    // Let's try to autofill the form fields for a while
    if (result !== null) {
      setAllFieds()
    }
  }, [result])

  const setAllFieds = (): void => {
    setValue("category", result.category?.inferred_from_existing_categoies?.id)
    setValue("amount", result?.amount)
    setValue("note", result?.note)
    setValue("date", result?.date)
  }

  const onSubmit = useCallback(
    createExpenseSubmissionHandler({
      setShowDismissable,
      setIsSending,
      getIsSending: () => isSending,
      createExpense,
      setGenericFormError,
      setIsUpdateSuccess,
      setError,
      reset,
      defaultValues
    }),
    [isSending, reset, setValue]
  )

  const displayARSResults = (): JSX.Element => {
    return (
      <div className="bg-gray-100 p-4 rounded-md w-full max-w-2xl">
        <h3 className="font-bold mb-2">Result:</h3>
        <p><strong>Text:</strong> {result.text}</p>
        <p><strong>Amount:</strong> {result.amount}</p>
        <div className="mt-2">
          <strong>Categories:</strong>
          <ul className="list-disc pl-5">
            <li>
              <strong>Inferred from first word:</strong> {result.category.inferred_from_first_word}
            </li>
            <li>
              <strong>Inferred from existing categories:</strong>
              {result.category.inferred_from_existing_categoies !== null && (
                <ul className="list-circle pl-5">
                  <li><strong>ID:</strong> {result.category.inferred_from_existing_categoies.id}</li>
                  <li><strong>Text:</strong> {result.category.inferred_from_existing_categoies.text}</li>
                  <li><strong>Is Archived:</strong> {result.category.inferred_from_existing_categoies.is_archived === true ? "Yes" : "No"}</li>
                </ul>)}
            </li>
          </ul>
        </div>
        <p><strong>Note:</strong> {result.note}</p>
        <p><strong>Date:</strong> {result.date}</p>
        <p><Button onClick={() => { setAllFieds() }} className="btn btn-success btn-sm my-2 my-sm-12">Set all fields</Button></p>
        <p><Button onClick={() => { setValue("amount", result.amount) }} className="btn btn-warning my-2">Set amount to  {result.amount}</Button></p>
        {result.category.inferred_from_existing_categoies !== null && (
          <p><Button onClick={() => { setValue("category", result.category.inferred_from_existing_categoies.id) }} className="btn btn-warning btn-sm my-12">Set category to {result.category.inferred_from_existing_categoies.text}</Button></p>
        )}
        <p><Button onClick={() => { setValue("note", result.note) }} className="btn btn-warning btn-sm my-12">Set note to  {result.note}</Button></p>
        <p><Button onClick={() => { setValue("date", result.date) }} className="btn btn-warning btn-sm my-12">Set date to  {result.date}</Button></p>
      </div>
    )
  }

  const displayCategoriesPullDown = (): JSX.Element => {
    if (state.categories.length > 0) {
      return <select id="category_field" name="category" ref={register({ required: "This field is required." })}>
        {state.categories.map(value => (
          <option key={value.id} value={value.id}>
            {value.text}
          </option>)
        )}
      </select>
    } else {
      return CustomSpinner()
    }
  }

  const displayAsrNotAvailableAlert = (): JSX.Element => {
    return (
      <div className="row h-100">
        <div className="alert col-12 alert-danger" role="warning">
          <h3>Automatic Speech Recognition is not available on HTTP on non-localhost domains. This is a browser restriction, and not a limitation of the application. </h3>
        </div>
        <div className="alert col-12 alert-warning" role="warning">
          <h3>Serve the application from HTTPS, or use localhost if you are in your development environment</h3>
        </div>
      </div>
    )
  }

  const displayBackendDisabledAsr = (): JSX.Element => {
    return (
      <div className="row h-100">
        <div className="alert col-12 alert-danger" role="warning">
          <h3>This budgeteer instance does not provide any Asr functionalities.</h3>
        </div>
      </div>
    )
  }

  const displayCreationForm = (): JSX.Element | null => {
    if (state.isAsrAvailable && state.isBackendAvailable) {
      return <form onSubmit={handleSubmit(onSubmit)} className="w-full max-w-md">
        <div className="mb-4">
          <div className="form-group">
            <label className="control-label col-sm-12 col-md-4 3 col-lg-3" htmlFor="text_field">Category:&nbsp;</label>
            <div className="col-sm-6 col-md-4 3 col-lg-3">
              {displayCategoriesPullDown()}
              {(errors?.category?.message !== null && errors?.category?.message !== undefined) ? styleErrorMessage(errors.category.message) : null}
            </div>
          </div>
        </div>
        <div className="form-group">
          <label className="control-label col-sm-12 col-md-4 3 col-lg-3" htmlFor="amount_field">Amount</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input className="form-control" name="amount" type="number" id="amount_field"
              ref={register({
                required: "This field is required.",
                min: {
                  value: 1,
                  message:
                    "Amount should be more than 0"
                }
              })}
            />
            {(errors.amount != null) ? styleErrorMessage(errors.amount.message as string) : null}
          </div>
        </div>
        <div className="form-group">
          <label className="control-label col-sm-12 col-md-4 3 col-lg-4" htmlFor="date_entity">Date&nbsp;</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input name="date" type="date" id="date_entity" defaultValue={new Date().toLocaleDateString("en-CA")} ref={register({ required: true })} />
            {(errors.date != null) ? styleErrorMessage(errors.date.message as string) : null}
          </div>
        </div>
        <div className="form-group">
          <label className="control-label col-sm-12 col-md-4 3 col-lg-12" htmlFor="note_field">Note</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input className="form-control" name="note" id="note_field"
              ref={register({
                maxLength: {
                  value: 50,
                  message: "Max 50 chars"
                }
              })}
            />
            {(errors.note != null) ? styleErrorMessage(errors.note.message as string) : null}
          </div>
        </div>
        <div className="form-group">
          <div className="col-sm-12 col-md-4 3 col-lg-3">
            {(genericFormError.length > 0) ? styleErrorMessage(genericFormError) : null}
          </div>
        </div>
        <div className="form-group">
          <div className="col-sm-12 col-md-4 3 col-lg-3">
            <button className="btn btn-secondary my-2 my-sm-0" disabled={isSending}> {isSending ? "Submitting..." : "Create expense"} </button>
          </div>
        </div>
      </form>
    } else {
      return null
    }
  }

  const displayASRCommands = (): JSX.Element => {
    return (
      <div>
        <div className="flex flex-col items-center space-y-2">
          <Button
            onClick={isRecorderReady ? (isRecording ? stopRecording : startRecording) : undefined}
            disabled={!isRecorderReady}
            className={`${isRecording ? "btn btn-danger  mr-1" : "btn btn-primary  mr-1"}`}
          >
            {isRecorderReady ? (isRecording ? "Stop Recording" : "Click to record") : "Preparing the mic..."}
          </Button>
          {audioURL !== "" && (
            <audio className="btn ml-1" controls src={audioURL}>Your browser does not support the audio element.</audio>
          )}
          {audioURL !== "" && (<Button className="btn btn-success ml-1 " onClick={sendAudioToBackend}>Send to Backend</Button>)}
        </div>

        {error !== null && (
          <div className="text-red-500 font-bold">
            Error: {error}
          </div>
        )}
      </div>
    )
  }

  const renderWIPAlert = (): JSX.Element | null => {
    if (!showDismissable) return null

    const toDo = `
    List of things to be done:
    - add a banner explaining this is an alpha feature
    - Document the feature. English only, for now.
        + Where <Date> should be "Month Day"
        + Document that categories including spaces are not supported
        + Document that categories named "Date" or "note" are not supported, as it messes up the parsing
    - Find a way to test the feature (having vox to pull the model in the CI/CD pipeline seems like a bad idea: the CI/CD will be slow)
        + Add a secondary pipeline that runs the tests that require vox, and run said pipeline on some condition only
    - Find a way to do integration tests on this feature
    - Add a button to delete the audio file and the text file
        + e.g. in case the user wants to record again. ATM they can just record again, and it will overwrite the previous recording.
              However the UX is not great, as the user might think that the previous recording is still there
    `
    return (
      <Alert variant={"warning"} dismissible onClose={() => { setShowDismissable(false) }}>
        <Alert.Heading>Work in progress</Alert.Heading>
        <div style={{ whiteSpace: "pre-wrap", fontFamily: "monospace" }} className="text-dark">{toDo}</div>
      </Alert>
    )
  }

  const displayView = (): JSX.Element => {
    return (
      <React.Fragment>
        <div className="col-12 col-sm-12 col-lg-12">
          {renderWIPAlert()}
        </div>

        {state.isApiCompleted && state.categories.length === 0
          ? <div className="col-12 col-sm-12 col-lg-12">
            {createCategoryGuidance()}
          </div>
          : null}

        <div className="col-12 col-sm-12 col-lg-6">
          <FormatGuidance isVisible={state.isAsrAvailable && state.isBackendAvailable} />

          {state.isAsrAvailable && state.isBackendAvailable && state.isApiCompleted
            ? displayASRCommands()
            : null
          }

          {state.isApiCompleted && !state.isAsrAvailable && displayAsrNotAvailableAlert()}
          {state.isApiCompleted && !state.isBackendAvailable && displayBackendDisabledAsr()}

          {renderErrorMessages()}

          {state.isAsrAvailable && state.isBackendAvailable && state.isApiCompleted
            ? displayCreationForm()
            : null
          }
        </div>

        <div className="col-12 col-sm-12 col-lg-6">
          {result !== null && displayARSResults()}
        </div>

      </React.Fragment>

    )
  }
  return displayView()
}
