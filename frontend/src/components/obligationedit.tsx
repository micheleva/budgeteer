import { useForm } from "react-hook-form"
import Alert from "react-bootstrap/Alert"
import { useParams, Link } from "react-router-dom"
import React, { useEffect, useRef, useState, useCallback } from "react"
import { getObligation, patchObligation } from "../common/api_obligations"
import type { ObligationFormInputs, Obligation } from "../common/interfaces"
import { CustomSpinner, styleErrorMessage } from "./shared"

export const ObligationEdit = (): JSX.Element => {
  // eslint-disable-next-line @typescript-eslint/unbound-method
  const { register, handleSubmit, setError, errors, setValue, watch } = useForm<ObligationFormInputs>()
  const [isSending, setIsSending] = useState(false)
  const { id } = useParams()
  const [obligation, setObligation] = useState<Obligation>()
  const initialRender = useRef(true)
  const [showDismissable, setShowDismissable] = useState(false)
  const [isUpdateSuccess, setIsUpdateSuccess] = useState(false)
  const autoCloseDismissibleAfterMs = 1500
  const [invalidObligation, setInvalidObligation] = useState(false)

  const [genericFormError, setGenericFormError] = useState("")

  const startDate = watch("start_date")
  const endDate = watch("end_date")

  const fetchData = useCallback((displayModal = true, signal?: AbortSignal) => {
    if (isSending) return
    setIsSending(true)
    setIsUpdateSuccess(false)

    getObligation(Number(id), signal).then((res) => {
      if (!res?.response?.ok) {
        setInvalidObligation(true)
      } else {
        const responseObligation = res.data
        setObligation(responseObligation)
        // IMPORTANT: We need to call setValue() AFTER render (with the form) has run
        // of the form will render undefined (i.e. "") in each field
        // as per https://stackoverflow.com/a/59547360
        // This means we can not use a flag like apiResponseComplete
        // as we do in goalstab.tsx
        setValue("name", responseObligation.name)
        setValue("total_amount", responseObligation.total_amount)
        setValue("obligation_type", responseObligation.obligation_type)
        setValue("monthly_payment", responseObligation.monthly_payment)
        setValue("start_date", responseObligation.start_date)
        setValue("end_date", responseObligation.end_date)
        setValue("payment_day", responseObligation.payment_day)
        setGenericFormError("")
        setInvalidObligation(false)
        setIsUpdateSuccess(true)
      }
    }).catch((err) => {
      if (err instanceof Error && err.name !== "AbortError") {
        setInvalidObligation(true)
        setIsUpdateSuccess(false)
        console.log("Failed to fetch page: ", err)
      }
    }).finally(() => {
      if (!signal?.aborted) {
        setIsSending(false)
        if (displayModal === true) {
          setShowDismissable(true)
        }
      }
    })
  }, [id])

  useEffect(() => {
    const abortController = new AbortController()
    const signal = abortController.signal

    if (initialRender.current) {
      initialRender.current = false
      fetchData(signal)
    }

    return () => { abortController.abort() }
  }, [fetchData])

  const onSubmit = useCallback((formValues: ObligationFormInputs) => {
    setIsSending(true)
    patchObligation(obligation?.id ?? 0, formValues) // Make the linter happy
      .then((res) => {
        if (!(res?.response?.ok)) {
          if (res?.data !== undefined && res?.data !== null && typeof res.data === "object") {
            for (const [key, value] of Object.entries(res.data)) {
              console.log("Get obligation error(s):", key, value)
              // Set error for existing fields
              if (key in formValues) {
                setError(key as keyof ObligationFormInputs, { message: value[0] })
              } else {
                // Other validation errors
                setGenericFormError(value[0])
              }
            }
          } else {
            // Handle 40X,50X errors in here
            setGenericFormError("Something is wrong with the server, try again later!")
          }
          setIsUpdateSuccess(false)
        } else {
          setIsUpdateSuccess(true)
          setGenericFormError("")
        }
      })
      .catch((err) => {
        console.log(err)
        setIsUpdateSuccess(false)
      })
      .finally(() => {
        setIsSending(false)
        setShowDismissable(true)
        const timer = setTimeout(() => {
          setShowDismissable(false)
        }, autoCloseDismissibleAfterMs)
        return () => { clearTimeout(timer) }
      })
  }, [obligation])

  const displayTitle = (o: Obligation): JSX.Element => <h3>Editing Obligation ID: {o.id}</h3>

  const missingOrUnavailableHeader = (): JSX.Element => {
    const message = "Sorry, it seems the Obligation you're trying to editing does not exist...or there was an error. Please try again later, or reload the page"
    // NOTE: This "invalid-obligation-marker" id is used by the functional tests, do not remove it!
    return <Alert id="invalid-obligation-marker" variant="danger">
              <Alert.Heading>{message}</Alert.Heading>
      </Alert>
  }

  const displayForm = (): JSX.Element => {
    return (
      <form onSubmit={handleSubmit(onSubmit)} className="form">

        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="name_field">Name</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input id="name_field" className="form-control" name="name" type="name" placeholder="Insert the obligation title"
             ref={register({
               required: "This field is required.",
               maxLength: {
                 value: 100,
                 message: "Max 100 chars"
               }
             })}
            />
            {(errors.name != null) ? styleErrorMessage(errors.name.message as string) : null}
          </div>
        </div>

        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="total_amount_field">Total Amount</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input className="form-control" name="total_amount" type="number" id="total_amount_field"
              ref={register({
                required: "This field is required.",
                min: {
                  value: 1, // TODO: change me when we use DecimalField
                  message: "Total Amount should be more than 0"
                }
              })}
            />
            {(errors.total_amount != null) ? styleErrorMessage(errors.total_amount.message as string) : null}
          </div>
        </div>

        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="obligation_type_field">Type</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <select id="obligation_type_field" name="obligation_type" ref={register({ required: "This field is required." })}>
              <option key="regular" value="regular">Regular</option>
              <option key="variable" value="variable">Variable</option>
              {/* TODO: improve the naming of the labels of these two pulldown, or add tooltips */}
            </select>
          {(errors.obligation_type != null) ? styleErrorMessage(errors.obligation_type.message as string) : null}
          </div>
        </div>

        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="monthly_payment_field">Total Amount</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input className="form-control" name="monthly_payment" type="number" id="monthly_payment_field"
              ref={register({
                required: "This field is required.",
                min: {
                  value: 1, // TODO: change me when we use DecimalField
                  message:
                  "Monthly Payment Amount should be more than 0"
                }
              })}
            />
            {(errors.monthly_payment != null) ? styleErrorMessage(errors.monthly_payment.message as string) : null}
          </div>
        </div>

        <div className="form-group">
            <label className="control-label col-sm-7 col-md-5 col-lg-4" htmlFor="start_date_field">Start date</label>
            <div className="col-sm-6 col-md-4 col-lg-3">
              <input name="start_date" type="date" id="start_date_field"
              ref={register({
                required: "This field is required.",
                validate: value => {
                  if (endDate !== undefined && value !== undefined) {
                    return new Date(value) < new Date(endDate) || "Start date must be before end date"
                  }
                  return true
                }
              })} />
              {((errors?.start_date) != null) ? styleErrorMessage(errors.start_date.message as string) : null}
            </div>
        </div>

        <div className="form-group">
            <label className="control-label col-sm-7 col-md-5 col-lg-4" htmlFor="end_date_field">End date</label>
            <div className="col-sm-6 col-md-4 col-lg-3">
              <input name="end_date" type="date" id="end_date_field"
                ref={register({
                  required: "This field is required.",
                  validate: value => {
                    if (startDate !== undefined && value !== undefined) {
                      return new Date(value) > new Date(startDate) || "End date must be after start date"
                    }
                    return true
                  }
                })} />
              {((errors?.end_date) != null) ? styleErrorMessage(errors.end_date.message as string) : null}
            </div>
        </div>

          <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="payment_day_field">Payment day</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input className="form-control" name="payment_day" type="number" id="payment_day_field"
              ref={register({
                required: "This field is required.",
                min: {
                  value: 1,
                  message:
                  "Payment Day should be more than 1"
                },
                max: {
                  value: 31,
                  message:
                  "Payment Day should be less than 31"
                }
              })}
            />
            {(errors.payment_day != null) ? styleErrorMessage(errors.payment_day.message as string) : null}
          </div>
        </div>

        <div className="form-group">
            <div className="col-sm-6 col-md-4 3 col-lg-3">
              <div>{(genericFormError.length > 0) ? styleErrorMessage(genericFormError) : null}</div>
            </div>
          </div>

        <div className="form-group">
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            {/* IMPORTANT: do not edit this id="submit_button". It is used in the functional tests */}
            <button className="btn btn-secondary my-2 my-sm-0" id="submit_button" disabled={isSending}>{ isSending ? "Submitting..." : "Submit" }</button>
          </div>
        </div>

      </form>
    )
  }
  const displayView = (): JSX.Element => {
    // IMPORTANT: We need to call setValue() AFTER render (with the form) has run
    // of the form will render undefined (i.e. "") in each field
    // as per https://stackoverflow.com/a/59547360

    // This means we can not use a flag like apiResponseComplete
    // as we do in goalstab.tsx

    // FIXME: BUGFIX-245
    // TRY ME OUT <========
    // try to make the form 'hidden' using css, and see if the setValue() binding works

    if (invalidObligation) {
      return missingOrUnavailableHeader()
    } else if (obligation !== undefined) {
      return <React.Fragment>
        <div>
          {showDismissable
            ? <Alert variant={isUpdateSuccess ? "success" : "danger"} onClose={() => { setShowDismissable(false) }} dismissible>
              <Alert.Heading>{isUpdateSuccess ? "Obligation updated!" : "Obligation update failed :("}</Alert.Heading>
            </Alert>
            : null}
          <Link to={"/app/obligations"}>Back to Obligations</Link>
          <div>{displayTitle(obligation)}</div>
        </div>

        { displayForm() }
      </React.Fragment>
    } else {
      return CustomSpinner()
    }
  }

  return displayView()
}
