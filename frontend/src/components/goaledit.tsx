import { useForm } from "react-hook-form"
import Alert from "react-bootstrap/Alert"
import { useParams, Link } from "react-router-dom"
import React, { useEffect, useRef, useState, useCallback } from "react"
import { getGoal, patchGoal } from "../common/api_goals"
import type { Goal, GoalFormInputs } from "../common/interfaces"
import { CustomSpinner, styleErrorMessage } from "./shared"

export const GoalEdit = (): JSX.Element => {
  // eslint-disable-next-line @typescript-eslint/unbound-method
  const { register, handleSubmit, setError, errors, setValue } = useForm<GoalFormInputs>()
  const [isSending, setIsSending] = useState(false)
  const { id } = useParams()
  const [goal, setGoal] = useState<Goal>()
  const initialRender = useRef(true)
  const [showDismissable, setShowDismissable] = useState(false)
  const [isUpdateSuccess, setIsUpdateSuccess] = useState(false)
  const autoCloseDismissibleAfterMs = 1500
  const [invalidGoal, setInvalidGoal] = useState(false)

  const [genericFormError, setGenericFormError] = useState("")

  const fetchData = useCallback((displayModal = true) => {
    if (isSending) return
    setIsSending(true)
    setIsUpdateSuccess(false)

    const abortController = new AbortController()
    const signal = abortController.signal

    getGoal(Number(id), signal).then((res) => {
      if (!res?.response?.ok) {
        setInvalidGoal(true)
      } else {
        const responseGoal = res.data
        setGoal(responseGoal)
        // IMPORTANT: We need to call setValue() AFTER render (with the form) has run
        // of the form will render undefined (i.e. "") in each field
        // as per https://stackoverflow.com/a/59547360
        // This means we can not use a flag like apiResponseComplete
        // as we do in goalstab.tsx
        setValue("text", responseGoal.text)
        setValue("amount", responseGoal.amount)
        setValue("note", responseGoal.note)
        setValue("is_archived", responseGoal.is_archived)
        setGenericFormError("")
        setInvalidGoal(false)
        setIsUpdateSuccess(true)
      }
    }).catch((err: Error) => {
      setInvalidGoal(true)
      setIsUpdateSuccess(false)
      console.log("Failed to fetch page: ", err)
    }).finally(() => {
      // Only update state if the request wasn't aborted
      if (!signal.aborted) {
        setIsSending(false)
        if (displayModal === true) {
          setShowDismissable(true)
        }
      }
    })

    return () => { abortController.abort() }
  }, [id])

  useEffect(() => {
    if (initialRender.current) {
      initialRender.current = false
      fetchData(false)
    }
  }, [fetchData])

  const onSubmit = useCallback((formValues: GoalFormInputs) => {
    setIsSending(true)
    // Goal models allow None for note field, but not blank (i.e. empty string)
    if (formValues.note === "") {
      delete formValues.note
    }
    const abortController = new AbortController()
    const signal = abortController.signal

    if (goal?.id !== undefined && goal?.id !== null) {
      patchGoal(goal.id, formValues, signal)
        .then((res) => {
          if (!(res?.response.ok)) {
            if (res?.data != null) {
              for (const [key, value] of Object.entries(res.data)) {
                console.log("Get goal error(s):", key, value)
                // Set error for existing fields
                if (key in formValues) {
                  setError(key as keyof GoalFormInputs, { message: value[0] })
                } else {
                  // Other validation errors
                  setGenericFormError(value[0])
                }
              }
            } else {
              // Handle 40X,50X errors in here
              setGenericFormError("Something is wrong with the server, try again later!")
            }
            setIsUpdateSuccess(false)
          } else {
            setIsUpdateSuccess(true)
            setGenericFormError("")
          }
        })
        .catch((err) => {
          console.log(err)
          setIsUpdateSuccess(false)
        })
        .finally(() => {
          setIsSending(false)
          setShowDismissable(true)
          const timer = setTimeout(() => {
            setShowDismissable(false)
          }, autoCloseDismissibleAfterMs)
          return () => { clearTimeout(timer) }
        })
    } else {
      setInvalidGoal(true)
      setIsSending(false)
    }

    return () => { abortController.abort() }
  }, [goal])

  const displayTitle = (goal: Goal): JSX.Element => {
    return <h3>Editing Goal ID: {goal.id}</h3>
  }

  const missingOrUnavailableHeader = (): JSX.Element => {
    const msg = "Sorry, it seems the goal you're trying to editing does not exist...or there was an error. Please try again later, or reload the page"
    // NOTE: This "invalid-goal-marker" id is used by the functional tests, do not remove it!
    return <Alert id="invalid-goal-marker" variant="danger">
              <Alert.Heading>{msg}</Alert.Heading>
      </Alert>
  }

  const displayForm = (): JSX.Element => {
    return (
      <form onSubmit={handleSubmit(onSubmit)} className="form">
          <div className="form-group">
            <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="amount_field">Amount</label>
            <div className="col-sm-6 col-md-4 3 col-lg-3">
              <input className="form-control" name="amount" type="number" id="amount_field"
                 ref={register({
                   required: "This field is required.",
                   min: {
                     value: 1,
                     message:
                          "Amount should be more than 0"
                   }
                 })}
              />
              {(errors.amount != null) ? styleErrorMessage(errors.amount.message as string) : null}
            </div>
          </div>

          <div className="form-group">
            <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="text_field">Text</label>
            <div className="col-sm-6 col-md-4 3 col-lg-3">
              <input className="form-control" name="text" id="text_field"
                 ref={register({
                   required: "This field is required.",
                   maxLength: {
                     value: 20,
                     message: "Max 20 chars"
                   }
                 })}
              />
            {(errors.text != null) ? styleErrorMessage(errors.text.message as string) : null}
            </div>
          </div>

          <div className="form-group">
            <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="note_field">Note</label>
            <div className="col-sm-6 col-md-4 3 col-lg-3">
              <input className="form-control" name="note" id="note_field"
                ref={register({
                  maxLength: {
                    value: 50,
                    message: "Max 50 chars"
                  }
                })}
              />
              {(errors.note != null) ? styleErrorMessage(errors.note.message as string) : null}
            </div>
          </div>

          <div className="form-group">
            <div className="col-sm-6 col-md-4 3 col-lg-3">
            <label><input type="checkbox" id="is_archived_field" name="is_archived" ref={register()} />&nbsp;Is archived</label>
            {(errors.is_archived != null) ? styleErrorMessage(errors.is_archived.message as string) : null}
            </div>
          </div>

          <div className="form-group">
            <div className="col-sm-6 col-md-4 3 col-lg-3">
              <div>{(genericFormError.length > 0) ? styleErrorMessage(genericFormError) : null}</div>
            </div>
          </div>

          <div className="form-group">
            <div className="col-sm-6 col-md-4 3 col-lg-3">
              <button className="btn btn-secondary my-2 my-sm-0" disabled={isSending}>{ isSending ? "Submitting..." : "Submit" }</button>
            </div>
          </div>

      </form>
    )
  }
  const displayView = (): JSX.Element => {
    // IMPORTANT: We need to call setValue() AFTER render (with the form) has run
    // of the form will render undefined (i.e. "") in each field
    // as per https://stackoverflow.com/a/59547360

    // This means we can not use a flag like apiResponseComplete
    // as we do in goalstab.tsx

    // FIXME: BUGFIX-245
    // TRY ME OUT <========
    // try to make the form 'hidden' using css, and see if the setValue() binding works

    if (invalidGoal) {
      return missingOrUnavailableHeader()
    } else if (goal !== null && goal !== undefined) {
      return <React.Fragment>
        <div>
          {showDismissable
            ? <Alert variant={isUpdateSuccess ? "success" : "danger"} onClose={() => { setShowDismissable(false) }} dismissible>
              <Alert.Heading>{isUpdateSuccess ? "Goal updated!" : "Goal update failed :("}</Alert.Heading>
            </Alert>
            : null}
          <Link to={"/app/goals"}>Back to Goals</Link>
          <div>{displayTitle(goal)}</div>
        </div>

        { displayForm() }
      </React.Fragment>
    } else {
      return CustomSpinner()
    }
  }

  return displayView()
}
