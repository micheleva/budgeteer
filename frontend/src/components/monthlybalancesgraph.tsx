import React, { useCallback, useEffect, useState } from "react"
import { Link } from "react-router-dom"

import Container from "react-bootstrap/Container"
import Col from "react-bootstrap/Col"
import Row from "react-bootstrap/Row"

import { getAllMonthlyBalancesAndAssets } from "../common/api_assets"
import { getGoals } from "../common/api_goals"
import { getObligations } from "../common/api_obligations"
import type { ChartData } from "../common/interfaces"
import { FailedBanner, CustomSpinner } from "./shared"

import { BarChart } from "./barchart"
import { BarStackedChart } from "./barstackedchart"

export const MonthlyBalancesGraph = (): JSX.Element => {
  const [isSending, setIsSending] = useState(false)

  const [chartData, setChartData] = useState<ChartData>({} as ChartData)
  const [isByCategory, setIsByCategory] = useState(true)
  const [apiResponseComplete, setApiResponseComplete] = useState(false)
  const [criticalError, setCriticalError] = useState(false)

  const fetchData = useCallback((signal?: AbortSignal) => {
    if (isSending) return
    setIsSending(true)
    setApiResponseComplete(false)

    Promise.all([getAllMonthlyBalancesAndAssets(signal), getGoals(signal), getObligations(signal)]).then((values) => {
      // MonthlyBalancesWithFlatCategoryPagedResponse
      const monthlyBalances = values[0]
      const goals = values[1]
      const obligations = values[2]
      let isCriticalError = false

      if (!monthlyBalances?.response?.ok) {
        isCriticalError = true
      }

      if (!goals?.response?.ok) {
        isCriticalError = true
      }

      if (!obligations?.response?.ok) {
        isCriticalError = true
      }

      setCriticalError(isCriticalError)
      if (!isCriticalError) {
        setChartData({
          monthlyBalances: monthlyBalances.data,
          goals: goals.data.results,
          obligations: obligations.data.results
        })
      }
    }).catch((err) => {
      setCriticalError(true)
      console.log("Failed to fetch page: ", err)
    }).finally(() => {
      setIsSending(false)
      setApiResponseComplete(true)
    })

    return () => { abortController.abort() }
  }, [isByCategory])

  useEffect(() => {
    const abortController = new AbortController()
    const signal = abortController.signal

    fetchData(signal)

    return () => { abortController.abort() }
  }, [fetchData, isByCategory])

  const BarGraph = (): React.ReactNode => {
    return <Container fluid>
              <Row className="p-0">
                <Col sm="12"className="p-0">
                  <BarChart chartData={chartData}/>
                </Col>
              </Row>
            </Container>
  }

  const StackedBarGraph = (): JSX.Element => (
    <Container fluid>
      <Row className="p-0">
        <Col sm="12"className="p-0">
          <BarStackedChart chartData={chartData}/>
        </Col>
      </Row>
    </Container>
  )

  // TODO: rename is ready to displayGraph()
  const isReady = (): React.ReactNode => {
    return isByCategory ? BarGraph() : StackedBarGraph()
  }

  const displayTitle = (): React.ReactNode => <h3>Monthly Blances and Goals Graph</h3>

  const createMonthlyBalanceGuidance = (): React.ReactNode => {
    const currYearMonthWihtOne = `${new Date().getFullYear()}-${String(new Date().getMonth() + 1).padStart(2, "0")}-01`
    return <div>
      <h5 className="text-info"> You need to create some Monthly Balances first! </h5>
      <Link className="btn btn-info mt-2 " to={`/app/monthly-balances/multiple-creation/date/${currYearMonthWihtOne}`}>Create Monthly Balances</Link>
    </div>
  }

  const displayGraphs = (): React.ReactFragment => {
    return <React.Fragment>
        <label htmlFor="is_table_shown">Break down by category:&nbsp;</label>
        <input type="checkbox" name="is_table_shown" onChange={e => {
          setIsByCategory(!isByCategory)
        }} checked={isByCategory}/>
        { Object.keys(chartData).length > 0 ? isReady() : null}
        </React.Fragment>
  }

  const displayView = (): JSX.Element => {
    if (!apiResponseComplete) {
      return CustomSpinner()
    }

    if (criticalError) {
      return FailedBanner()
    }

    return (
      <React.Fragment>
        { displayTitle() }
        { Object.keys(chartData).length === 0 || chartData?.monthlyBalances?.length === 0 ? createMonthlyBalanceGuidance() : displayGraphs() }
      </React.Fragment>
    )
  }

  return displayView()
}
