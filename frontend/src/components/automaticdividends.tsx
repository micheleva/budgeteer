import React, { useEffect, useRef, useState, useCallback } from "react"
import { createDividend, getDividendRawDataByAccName } from "../common/api_dividends"
import { getAccounts } from "../common/api_accounts"
import type { Account, AccountsResponse, getDividendDataFunc, EndUserDividends, DividendResponse } from "../common/interfaces"
import { createAccountGuidance, CustomSpinner, DismissableRecoverableErrorBanner, FailedBanner } from "./shared"
import { DismissableAlert } from "./dismissablealert"
import { isDataValidForDividend } from "../common/utilities"
import Alert from "react-bootstrap/Alert"

const text = String.raw`
[
  {
    "date": "2023-07-11",
    "currency": "USD",
    "ticker": "STOCK-SYMBOL", // the asset name, or symbol in case of stocks
    "amount": 240.47, // the grand amount of dividends received from this asset
    "rate": 110.19 // rate of local currency to "currency" on the day the dividend was received
  },
  {
    "date": "2023-02-11",
    "currency": "USD",
    "ticker": "STOCK-SYMBOL",
    "amount": 125.35,
    "rate": 120.19
  },
 [...]
  {
    "date": "2021-02-12",
    "currency": "USD",
    "ticker": "STOCK-SYMBOL",
    "amount": 51.66,
    "rate": 110.19
  }
]

NOTE: For local currency dividends return a json file where each item has its rate set to 1, and its currency to "<local-currency>" e.g.JPY
[
  {
    "date": "2020-07-11",
    "currency": "JPY",
    "ticker": "OTHER-STOCK-SYMBOL",
    "amount": 578,
    "rate": 1
  },
  {
    "date": "2020-02-11",
    "currency": "JPY",
    "ticker": "OTHER-STOCK-SYMBOL",
    "amount": 579,
    "rate": 1
  },
[...]
]
`

const cmdTooltip = "To spin up a mock data server, please run `python manage.py dividends-api-mock-server` in a different terminal"

const getDividendDataWrapper = async (callback: getDividendDataFunc, account: Account, targetDate: string, importUrl: string, setShowDismissable: (value: boolean) => void): Promise<any> => {
  setShowDismissable(true)
  return await callback(account, targetDate, importUrl)
}

export const AutomaticDividends = (): JSX.Element => {
  const [isSending, setIsSending] = useState(false)
  const initialRender = useRef(true)
  const [targetDate, setTargetDate] = useState<string>(new Date().toLocaleDateString("en-CA"))
  const [importUrl, setImportUrl] = useState<string>("http://localhost:8080/profits_files")
  const [accounts, setAccounts] = useState<Account[]>([])

  const [showDismissable, setShowDismissable] = useState(true)

  const [accountsWithData, setAccountsWithData] = useState(-1)
  const [createdDividends, setcreatedDividends] = useState(0)
  const [totalDividends, setTotalDividends] = useState(0)
  const [malformedDividends, setMalformedDividends] = useState(0)
  const [erroredDividends, setErroredDividends] = useState(0)
  const [userHasNoCategories, setUserHasNoCategories] = useState(false)

  const [apiResponseComplete, setApiResponseComplete] = useState(false)
  const [criticalError, setCriticalError] = useState(false)
  const [recoverableError, setRecoverableError] = useState(false)

  const resetComponentState = (): void => {
    // Reset the component state in case we get back to this view through a link, and the view gets reused
    setAccountsWithData(-1)
    setcreatedDividends(0)
    setTotalDividends(0)
    setMalformedDividends(0)
    setErroredDividends(0)
  }

  const fetchData = useCallback(() => {
    if (isSending) return
    setIsSending(true)
    resetComponentState()

    // Counters to display how many divdends have been created (i.e. text updated as soon as API returns. A primitive process bar using text)
    const createdDividends: number[] = []
    const totalDividendsToBeCreated: number[] = []

    // We do not call setTotalAssets(<dividend>) as it might incur in race conditions.
    // We instead push dummy numbers in the array, as it is more likely atomic,
    // and set component's totalDidivends to the length of this dummy array instead

    const toBeCreatedPromises: Array<Promise<DividendResponse>> = []
    const malformed: number[] = []
    const errored: number[] = []

    // NOTE: this intentionally duplicates the component's criticalError
    // As we need to set the component's one only after ALL promises have been fulfilled
    let isRecoverableFetchError = false

    // Since getDividendRawDataByAccName() does not have the account object in its response, we wrap said call to inject the account in the return value
    const accPromises = accounts.map(async account => await getDividendDataWrapper(getDividendRawDataByAccName, account, targetDate, importUrl, setShowDismissable))

    const createDividendPromise = (dividend: EndUserDividends, account: Account): void => {
      const postData = {
        account: account.id,
        asset_text: dividend.ticker,
        // FIXME: (BUGFIX-244) the "foreign amount" field could use a better name
        local_amount: account.is_foreign_currency ? dividend.amount * dividend.rate : dividend.amount,
        foreign_amount: account.is_foreign_currency ? dividend.amount : 0, // fix me once we have fixed the broker parser, to return the correct rate!
        currency: dividend.currency,
        rate: account.is_foreign_currency ? dividend.rate : 1,
        record_date: dividend.date
      }

      toBeCreatedPromises.push(
        createDividend(postData).then((result: DividendResponse) => {
          setShowDismissable(true)
          // This internal promise will update the banner with "Created <n>/<total> assets" (our primitive progress bar)
          // as soon as they're created!
          if (result?.response?.ok && result?.data?.id !== 0) {
            createdDividends.push(result?.data.id)
            setcreatedDividends(createdDividends.length)
          } else {
            errored.push(1)
            setErroredDividends(errored.length)
            console.log(`Account ${account.id} ${dividend.ticker} has failed to be created for ${targetDate}`)
          }
          return result
        })
      )
    }

    Promise.allSettled(accPromises).then(results => {
      // notEmptyAccNum is displayed to screen to show how many accounts have succeeded
      const notEmptyAccNum = results.reduce((count, result) => {
        if (result.status === "fulfilled" && result.value?.data?.dividends?.length > 0) {
          return count + 1
        }
        return count
      }, 0)
      setAccountsWithData(notEmptyAccNum)

      // FIXME: FireFox has a weird issue in which map has to be stored in a variable. Find a wokraround!
      results.map(async (accPromRes) => {
        if (accPromRes.status === "fulfilled" && accPromRes.value?.data?.dividends?.length > 0) {
          // Filter out accounts that have no assets
          const dividends = accPromRes?.value?.data.dividends
          if (accPromRes?.value?.response?.ok === true && Array.isArray(accPromRes?.value?.data.dividends) && accPromRes?.value?.data?.dividends?.length > 0 && accPromRes?.value?.data?.account !== null) {
            const account = accPromRes?.value?.data?.account
            // eslint-disable-next-line array-callback-return
            dividends.map((dividend: any) => {
              totalDividendsToBeCreated.push(1)
              setTotalDividends(totalDividendsToBeCreated.length)
              if (isDataValidForDividend(dividend)) {
                createDividendPromise(dividend, account)
              } else {
                // The third party API has returned a unepected format object: let the user know (but do not abort the import)
                malformed.push(1)
                setMalformedDividends(malformed.length)
              }
            })

            if (toBeCreatedPromises.length === 0) {
            // NICE TO HAVE: show a meaningful error that suggests to the user to confirm whether the date is correct, as all accounts have no data.
            // Also, it might suggest as well to double check whether their endpoint is returning data correctly or not
              // NICE TO HAVE: account entities should have a has_dividends boolean, and we only select accounts with dividneds in this function (e.g. getAccounts gets a value to filter out accounts without dividends)
              console.error(`No account has any assets. Are you sure the ${targetDate} is in the correct format, and that's the date you intend to fetch data for? Is the endpoint proving this data up and running correcly?`)
            }
          }
        } else {
          if (accPromRes.status === "fulfilled") {
            console.log(`Account ID: ${accPromRes.value.data.account.id}, ${accPromRes.value.data.account.text} seems to have no data`)
          } else {
            console.log(`Promise rejected with reason: ${accPromRes.reason}`)
          }
        }
      })

      const assetsPromsiesWrapper = new Promise((resolve) => {
        Promise.allSettled(toBeCreatedPromises)
          .then((results) => {
            results.forEach((result) => {
              if (result.status !== "fulfilled") {
                console.log(result)
                isRecoverableFetchError = true
              }
            })
            setRecoverableError(isRecoverableFetchError)
          }).catch((err) => {
            console.log("Failed to fetch page: ", err)
            setRecoverableError(true)
          }).finally(() => {
            resolve(1)
          })
      })

      assetsPromsiesWrapper.finally(() => {
        setIsSending(false)
        console.log("Finally statement executing firing after all toBeCreatePromises have settled.")
      }).catch((e) => {
        console.log(e)
      })
    }).catch((e) => {
      console.log(e)
    }) // Closing Promise.allSettled(accPromises). [...]
  }, [targetDate, accounts, importUrl])

  // In order to have the finally() block called only once, we wrap allSettled() in another Promise.
  // The reason is that we're altering the content of toBeCreatePromises dynamically and in an async way
  // FIXME: This is a HORRIBLE HACK. If you are reading this, clean me up.
  useEffect(() => {
    if (initialRender.current) {
      initialRender.current = false
      getAccounts().then((data: AccountsResponse) => {
        if (data?.response?.ok) {
          setCriticalError(false)
          setAccounts(data?.data?.results)
          if (data?.data?.results?.length === 0) {
            setUserHasNoCategories(true)
          }
        } else {
          setCriticalError(true)
        }
      }).catch((e) => {
        console.log(e)
        setCriticalError(true)
      })
        .finally(() => {
          setIsSending(false)
          setApiResponseComplete(true)
        })
    }
  }, [fetchData])

  const displayTitle = (): React.ReactNode => {
    return <h3>Automatically import dividends data</h3>
  }

  const displayView = (): JSX.Element => {
    if (!apiResponseComplete) {
      return CustomSpinner()
    }

    if (criticalError) {
      return <React.Fragment>
        { displayTitle() }
        { FailedBanner() }
        </React.Fragment>
    }

    if (userHasNoCategories) {
      return <React.Fragment>
          { displayTitle() }
          { createAccountGuidance() }
        </React.Fragment>
    }

    // FIXME: BUGFIX-245

    return (
      <div className="form-group">
        { displayTitle() }
        { recoverableError ? DismissableRecoverableErrorBanner(setRecoverableError) : null }
        <label htmlFor="targetDate">Import all dividends earner as per date:&nbsp;</label>
        <input type="date" id="targetDate" className="form-control col-sm-6 col-md-4 3 col-lg-3" name="targetDate" onChange={e => { setTargetDate(e.target.value) }} value={targetDate}/>

        <label htmlFor="importUrl">Import url:&nbsp;</label>
        {/* NICE TO HAVE: Validate the field text so that if it's not a well-formatted URL, it will show an error to screen */}
        <input type="text" id="importUrl" className="form-control col-sm-6 col-md-4 3 col-lg-3" name="importUrl" onChange={e => { setImportUrl(e.target.value) }} value={importUrl}/>
        <button onClick={fetchData} className="btn btn-secondary my-2 mr-2" disabled={isSending}>{ isSending ? "Fetching..." : "Fetch and create" }</button>
        <DismissableAlert url={importUrl} date={targetDate} accounts={accounts} text={text} commandTooltip={cmdTooltip}/>

       {accountsWithData > -1 && showDismissable
         ? <Alert variant="primary" onClose={() => { setShowDismissable(false) }} dismissible>
            <Alert.Heading>Results:</Alert.Heading>
               <div>{accountsWithData}/{accounts.length} accounts have data for {targetDate}</div>
               { (createdDividends > 0 || totalDividends > 0) && showDismissable ? <div>Created {createdDividends}/{totalDividends} dividends entries</div> : null}
               { (createdDividends > 0 && totalDividends > 0 && createdDividends === totalDividends) && showDismissable ? <div><b>All dividends have been created successfully!</b></div> : null}
               { erroredDividends > 0 && showDismissable ? <b><div className="text-danger">{erroredDividends} dividend(s) creation failed.</div></b> : null}
               { malformedDividends > 0 && showDismissable ? <b><div className="text-danger">{importUrl} returned {malformedDividends} malformed data point(s). Could not create dividend(s) for it(those).</div></b> : null}
          </Alert>
         : null}
        { isSending ? CustomSpinner() : null}
      </div>
    )
  }
  return displayView()
}
