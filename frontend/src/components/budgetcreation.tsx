import { useForm } from "react-hook-form"

import Alert from "react-bootstrap/Alert"
import React, { useEffect, useRef, useState, useCallback } from "react"
import { createBudget } from "../common/api_budgets"
import { getCategories } from "../common/api_categories"
import type { BudgetFormInput, Category, BudgetResponse } from "../common/interfaces"
import { createCategoryGuidance, CustomSpinner, FailedBanner, styleErrorMessage } from "./shared"

const defaultValues: BudgetFormInput = {
  // NOTE: do not reset the category
  year: new Date().getFullYear(),
  month: new Date().getMonth() + 1,
  amount: 0
}

const BudgetCreation = (): JSX.Element => {
  // eslint-disable-next-line @typescript-eslint/unbound-method
  const { register, handleSubmit, setError, formState: { errors }, setValue, watch, reset } = useForm<BudgetFormInput>()
  const [isSending, setIsSending] = useState(false)
  const [categories, setCategories] = useState<Category[]>([{ id: 0, text: "Loading..." }])
  const initialRender = useRef(true)
  const [showDismissable, setShowDismissable] = useState(false)
  const [isUpdateSuccess, setIsUpdateSuccess] = useState(false)
  const autoCloseDismissibleAfterMs = 1500

  const [apiResponseComplete, setApiResponseComplete] = useState(false)
  const [criticalError, setCriticalError] = useState(false)
  const [genericFormError, setGenericFormError] = useState("")

  const chooseWatch = watch(["date", "isYearlyBudget"], [new Date().toLocaleDateString("en-CA"), false])

  interface BudgetTarget {
    month: number | null
    year: number | null
  }

  const getCorrectTargetDate = (): BudgetTarget => {
    if (!chooseWatch?.date === null) {
      return { month: null, year: null }
    }

    const date01 = `${chooseWatch?.date}T00:00:00`
    const d = new Date(date01)
    if (chooseWatch?.isYearlyBudget === true) {
      return { month: null, year: d.getFullYear() }
    } else {
      return { month: d.getMonth() + 1, year: d.getFullYear() }
    }
  }

  const getCorrectTargetDateAsString = (): React.ReactNode => {
    const res = getCorrectTargetDate()
    if (res?.month !== null) {
      return `${res.year}-${String(res.month).padStart(2, "0")}`
    }
    return res.year
  }

  const formatPostBody = (formValues: BudgetFormInput) => {
    const sanitizedValues = { ...formValues }
    const date01 = `${sanitizedValues.date}T00:00:00`
    const d = new Date(date01)
    const year = d.getFullYear()
    const month = d.getMonth() + 1
    if (sanitizedValues?.isYearlyBudget === true) {
      sanitizedValues.month = null
      sanitizedValues.year = year
    } else {
      sanitizedValues.month = month
      sanitizedValues.year = year
    }
    // NOTE: date and isYearlyBudget fields are used in the frontend to properly extract year and month as numbers: do not post them
    delete sanitizedValues.date
    delete sanitizedValues.isYearlyBudget
    return sanitizedValues
  }

  const onSubmit = useCallback((formValues: BudgetFormInput) => {
    if (isSending) return
    setShowDismissable(false)
    setIsSending(true)

    const postBodyData = formatPostBody(formValues)
    createBudget(postBodyData)
      .then((res: BudgetResponse) => {
        if (!res?.response?.ok) {
          if (res?.data !== undefined) {
            for (const [key, value] of Object.entries(res.data)) {
              console.log("Budgets creation form error(s):", key, value)
              // Set error for existing fields
              if (key in formValues) {
                setError(key, { message: value[0] })
              } else {
                // FIXME Update to reflect budget constraints <============
                if (value[0] === "An Account with the same text AND credit_entity already exists for this user.") {
                  setError("text", { message: value[0] })
                } else {
                  // Other validation errors
                  setGenericFormError(value[0])
                }
              }
            }
          } else {
            // Handle 40X,50X errors in here
            setGenericFormError("Something is wrong with the server, try again later!")
          }
          setIsUpdateSuccess(false)
        } else {
          reset(defaultValues)
          setGenericFormError("")
          setIsUpdateSuccess(true)
        }
      })
      .catch(error => {
        console.log("Budget creation error ", error)
        setIsUpdateSuccess(false)
      })
      .finally(() => {
        setIsSending(false)
        setShowDismissable(true)
        const timer = setTimeout(() => {
          setShowDismissable(false)
        }, autoCloseDismissibleAfterMs)
        return () => { clearTimeout(timer) }
      })
  }, [reset])

  useEffect(() => {
    const abortController = new AbortController()
    const signal = abortController.signal

    if (initialRender.current) {
      initialRender.current = false
      setIsSending(true)
      getCategories(signal).then((res) => {
        if (!res?.response) {
          setCriticalError(true)
        } else if (res?.response?.ok && res?.data?.results) {
          setCriticalError(false)
          setCategories(res?.data.results)
        } else {
          setCriticalError(true)
        }
      }).catch((err) => {
        setCriticalError(true)
        console.log("Failed to fetch page: ", err)
      }).finally(() => {
        setIsSending(false)
        setApiResponseComplete(true)
        setValue("date", new Date().toLocaleDateString("en-CA"))
      })
    }

    return () => { abortController.abort() }
  }, [])

  const displayTitle = (): JSX.Element => <div className="col-sm-12"><h3>Create a Spending Budget </h3></div>

  const displayCreationForm = (): JSX.Element => {
    if (categories?.length === 0) {
      return createCategoryGuidance()
    }
    return (
      <form onSubmit={handleSubmit(onSubmit)} className="form">
        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="category_field">Category:&nbsp;</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <select id="category_field" name="category" ref={register({ required: "This field is required." })}>
              {categories.map(value => (<option key={value.id} value={value.id}>
                {value.text}
              </option>))}
            </select>
            {(errors.category != null) ? styleErrorMessage(errors.category.message) : null}
          </div>
        </div>

        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="amount_field">Amount</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input className="form-control" name="amount" type="number" id="amount_field"
              ref={register({
                required: "This field is required.",
                min: {
                  value: 0,
                  message:
                    "Amount should be more or equal to 0"
                }
              })}
            />
            {((errors?.amount) != null) ? styleErrorMessage(errors.amount.message as string) : null}
          </div>
        </div>

        {/* Firefox does not support yet type="month" https://bugzilla.mozilla.org/show_bug.cgi?id=1283382 (8 year old bug)
            https://developer.mozilla.org/en-US/docs/Web/HTML/Element/input/month#browser_compatibility */}
        <div className="form-group">
          <label className="control-label col-sm-7 col-md-5 col-lg-4" htmlFor="date_entity">Date <small>(Firefox does not support month pickers, so pick <i>any day</i> of the month to set the budget for)</small>&nbsp;</label>
          <div className="col-sm-6 col-md-4 col-lg-3">
            <input name="date" type="date" id="date_entity" defaultValue={new Date().toLocaleDateString("en-CA")} ref={register({ required: true })} />
            {((errors?.date) != null) ? styleErrorMessage(errors.date.message as string) : null}
          </div>
        </div>

        <div className="form-group">
          <div className="col-sm-6 col-md-4 col-lg-3">
            <label><input type="checkbox" name="isYearlyBudget" ref={register()} />&nbsp;Yearly budget?</label>
            {((errors?.isYearlyBudget) != null) ? styleErrorMessage(errors?.isYearlyBudget?.message as string) : null}
          </div>
        </div>

        <div className="form-group">
          <div className="col-sm-6 col-md-4 col-lg-3">
            <b><div> Setting Budget for: <span className="text-success"> {getCorrectTargetDateAsString()}</span></div></b>
          </div>
        </div>

        <div className="form-group">
          <div className="col-sm-12 col-md-12 col-lg-12">
            <b><div>{(genericFormError.length > 0) ? styleErrorMessage(genericFormError) : null}</div></b>
          </div>
        </div>

        <div className="form-group">
          <div className="col-sm-6 col-md-4 col-lg-3">
            <button className="btn btn-secondary my-2 my-sm-0" disabled={isSending}>{isSending ? "Submitting..." : "Submit"}</button>
          </div>
        </div>

      </form>
    )
  }

  const renderDismissable = (): JSX.Element | null => {
    if (showDismissable) {
      return <Alert variant={isUpdateSuccess ? "success" : "danger"} onClose={() => { setShowDismissable(false) }} dismissible>
        <Alert.Heading>{isUpdateSuccess ? "Budget created!" : "Budget creation failed :("}</Alert.Heading>
      </Alert>
    } else {
      return null
    }
  }

  const renderWIPAlert = (): JSX.Element => {
    return <Alert variant={"warning"}>
      <Alert.Heading>Work in progress</Alert.Heading>
      <div className="text-dark">{"After v1.4 is released, users will be able to be mass update/create Budgets in the same fashion as MonthlyBalances. Bare with me while I'm at it :D "}</div>
    </Alert>
  }

  const displayView = (): JSX.Element => {
    if (!apiResponseComplete) {
      return CustomSpinner()
    }

    if (criticalError) {
      return FailedBanner()
    }

    return <React.Fragment>
      {renderWIPAlert()}
      {displayTitle()}
      {renderDismissable()}
      {displayCreationForm()}
    </React.Fragment>
  }

  return displayView()
}

export default BudgetCreation
