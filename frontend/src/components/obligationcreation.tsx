import { useForm } from "react-hook-form"
import Alert from "react-bootstrap/Alert"
import React, { useState, useCallback } from "react"
// import the createObligation function from the api module
import { createObligation } from "../common/api_obligations"
import type { ObligationFormInputs, ReactFragment } from "../common/interfaces"
import { styleErrorMessage } from "./shared"

// Define the default values for the obligation form
const defaultValues = {
  name: "",
  total_amount: 0,
  obligation_type: "regular",
  monthly_payment: 0,
  payment_day: 0,
  start_date: null,
  end_date: null,
  changes: []
}

// Create the react view for the obligation creation form
export const ObligationCreation = (): JSX.Element => {
  // eslint-disable-next-line @typescript-eslint/unbound-method
  const { register, handleSubmit, setError, formState: { errors }, reset, watch } = useForm<ObligationFormInputs>()
  const [isSending, setIsSending] = useState(false)
  const [showDismissable, setShowDismissable] = useState(false)
  const [isCreateSuccess, setIsCreateSuccess] = useState(false)
  const autoCloseDismissibleAfterMs = 1500

  // State for server errors
  const [serverError, setServerError] = useState("")

  const startDate = watch("start_date")
  const endDate = watch("end_date")

  // onSubmit is a function that is called when the form is submitted
  const onSubmit = useCallback((formValues: ObligationFormInputs) => {
    // If the form is already being sent, return early
    if (isSending) return
    setShowDismissable(false)
    setIsSending(true)
    const abortController = new AbortController()
    const signal = abortController.signal

    // Create the obligation
    createObligation(formValues, signal)
      .then((res) => {
        // If the response is not ok, handle the error
        if (!res?.response?.ok) {
          if (Array.isArray(res?.data) && res.data.length > 0) {
            for (const [key, value] of Object.entries(res.data)) {
              console.log("Obligation creation form error(s):", key, value)
              // Set error for existing fields
              if (key in formValues) {
                setError(key, { message: value[0] })
              } else {
                // Other validation errors
                setServerError(value[0])
              }
            }
          } else {
            // Handle 40X,50X errors in here
            setServerError("Something is wrong with the server, try again later!")
          }
          setIsCreateSuccess(false)
        } else {
          reset(defaultValues)
          setServerError("")
          setIsCreateSuccess(true)
        }
      })
      .catch(error => {
        if (error instanceof Error && error.name !== "AbortError") {
          console.log("Goal creation frontend error:", error)
          setIsCreateSuccess(false)
        }
      })
      .finally(() => {
        if (!signal.aborted) {
          setIsSending(false)
          setShowDismissable(true)
          const timer = setTimeout(() => {
            setShowDismissable(false)
          }, autoCloseDismissibleAfterMs)
          return () => { clearTimeout(timer) }
        }
      })

    return () => { abortController.abort() }
  }, [isSending, setError, reset]) // These are the dependencies for useCallback

  const displayTitle = (): JSX.Element => <h3>Create a Obligation</h3>

  const renderDismissable = (): JSX.Element | null => {
    if (showDismissable) {
      return <Alert variant={isCreateSuccess ? "success" : "danger"} onClose={() => { setShowDismissable(false) }} dismissible>
              <Alert.Heading>{isCreateSuccess ? "Obgliation created!" : "Obligation creation failed :("}</Alert.Heading>
            </Alert>
    } else {
      return null
    }
  }

  const displayCreationForm = (): ReactFragment => {
    return (
      <div>
        <form onSubmit={handleSubmit(onSubmit)} className="form">
        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="name_field">Name</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input id="name_field" className="form-control" name="name" type="name" placeholder="Insert the obligation title"
             ref={register({
               required: "This field is required.",
               maxLength: {
                 value: 100,
                 message: "Max 100 chars"
               }
             })}
            />
            {(errors.name != null) ? styleErrorMessage(errors.name.message as string) : null}
          </div>
        </div>

        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="total_amount_field">Total Amount</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input className="form-control" name="total_amount" type="number" id="total_amount_field"
              ref={register({
                required: "This field is required.",
                min: {
                  value: 1, // TODO: change me when we use DecimalField
                  message: "Total Amount should be more than 0"
                }
              })}
            />
            {(errors.total_amount != null) ? styleErrorMessage(errors.total_amount.message as string) : null}
          </div>
        </div>

        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="obligation_type_field">Type</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <select id="obligation_type_field" name="obligation_type" ref={register({ required: "This field is required." })}>
              <option key="regular" value="regular">Regular</option>
              <option key="variable" value="variable">Variable</option>
              {/* TODO: improve the naming of the labels of these two pulldown, or add tooltips */}
            </select>
          {(errors.obligation_type != null) ? styleErrorMessage(errors.obligation_type.message as string) : null}
          </div>
        </div>

        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="monthly_payment_field">Monthly Amount</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input className="form-control" name="monthly_payment" type="number" id="monthly_payment_field"
              ref={register({
                required: "This field is required.",
                min: {
                  value: 1, // TODO: change me when we use DecimalField
                  message:
                  "Monthly Payment Amount should be more than 0"
                }
              })}
            />
            {(errors.monthly_payment != null) ? styleErrorMessage(errors.monthly_payment.message as string) : null}
          </div>
        </div>

        <div className="form-group">
            <label className="control-label col-sm-7 col-md-5 col-lg-4" htmlFor="start_date_field">Start date</label>
            <div className="col-sm-6 col-md-4 col-lg-3">
              <input name="start_date" type="date" id="start_date_field" defaultValue={new Date().toLocaleDateString("en-CA")}
              ref={register({
                required: "This field is required.",
                validate: value => {
                  if (endDate !== undefined && value !== undefined) {
                    return new Date(value) < new Date(endDate) || "Start date must be before end date"
                  }
                  return true
                }
              })} />
              {((errors?.start_date) != null) ? styleErrorMessage(errors.start_date.message as string) : null}
            </div>
        </div>

        <div className="form-group">
            <label className="control-label col-sm-7 col-md-5 col-lg-4" htmlFor="end_date_field">End date</label>
            <div className="col-sm-6 col-md-4 col-lg-3">
              <input name="end_date" type="date" id="end_date_field" defaultValue={new Date(new Date().setDate(new Date().getDate() + 1)).toLocaleDateString("en-CA")}
                ref={register({
                  required: "This field is required.",
                  validate: value => {
                    if (startDate !== undefined && value !== undefined) {
                      return new Date(value) > new Date(startDate) || "End date must be after start date"
                    }
                    return true
                  }
                })} />
              {((errors?.end_date) != null) ? styleErrorMessage(errors.end_date.message as string) : null}
            </div>
        </div>

        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="payment_day_field">Payment day</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input className="form-control" name="payment_day" type="number" id="payment_day_field"
              ref={register({
                required: "This field is required.",
                min: {
                  value: 1,
                  message:
                  "Payment Day should be more than 1"
                },
                max: {
                  value: 31,
                  message:
                  "Payment Day should be less than 31"
                }
              })}
            />
            {(errors.payment_day != null) ? styleErrorMessage(errors.payment_day.message as string) : null}
          </div>
        </div>

        <div className="form-group">
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            {(serverError.length > 0) ? styleErrorMessage(serverError) : null}
            {/* IMPORTANT: do not edit this id="submit_button". It is used in the functional tests */}
            <button className="btn btn-secondary my-2 my-sm-0" id="submit_button" disabled={isSending}>{ isSending ? "Submitting..." : "Submit" }</button>
          </div>
        </div>

        </form>
        {(serverError.length > 0) && <Alert variant="danger">{serverError}</Alert>}
      </div>
    )
  }

  const displayView = (): JSX.Element => {
    // Obgliation has no foreign keys, hence we do not need a spinner, as we do not fetch other entities
    return <React.Fragment>
      { displayTitle() }
      { renderDismissable() }
      { displayCreationForm() }
    </React.Fragment>
  }

  return displayView()
}
