import React, { useEffect, useRef, useState } from "react"
import { deleteExpense, getExpensesByCategoryId } from "../common/api_expenses"
import { getCategories } from "../common/api_categories"
import type { CategoriesResponse, Category, Expense, ExpensesResponse } from "../common/interfaces"
import { createCategoryGuidance, CustomSpinner, DismissableRecoverableErrorBanner, ExpenseTable, FailedBanner, Pulldown, styleErrorMessage } from "./shared"
import { preventFormSubmit, sortByDateValue, yearMonthIsValidDate } from "../common/utilities"

export const ExpensebyCategory = (): JSX.Element => {
  const [isSending, setIsSending] = useState(false)
  const [data, setData] = useState<Expense[]>([])
  const initialRender = useRef(true)
  const [CategoryId, setCategoryId] = useState<number>(1)
  const [categories, setCategories] = useState<Category[]>([])
  const [start, setstart] = useState<string>(new Date().getFullYear() + "-01-01")
  const [end, setend] = useState<string>(new Date().getFullYear() + "-12-31")
  const [apiResponseComplete, setApiResponseComplete] = useState(false)
  const [criticalError, setCriticalError] = useState(false)
  const [recoverableError, setRecoverableError] = useState(false)

  const [genericFormError, setGenericFormError] = useState("")

  const fetchData = React.useCallback((signal?: AbortSignal) => {
    if (!yearMonthIsValidDate(start) || !yearMonthIsValidDate(end)) {
      return
    }
    // FIXME: is the "=== 0" check really necessary?
    if (isSending || CategoryId === 0) return
    setIsSending(true)

    // FIXME: on the first load, it seems this function is called twice
    getExpensesByCategoryId(CategoryId, start, end, null, signal)
      .then((data: ExpensesResponse) => {
        if (!data?.response?.ok) {
          if (data?.data !== undefined) {
            for (const [, value] of Object.entries(data.data)) {
              setGenericFormError(value[0])
            }
            setRecoverableError(true)
            setData([])
          } else {
            // Handle 40X,50X errors in here
            setGenericFormError("Something is wrong with the server, try again later!")
            setRecoverableError(true)
            setData([])
          }
        } else {
          setGenericFormError("")
          setRecoverableError(false)
          setData(data.data.results.sort(sortByDateValue))
        }
      }).catch((err: Error) => {
        // Only log and update state if it's not an AbortError
        if (err.name !== "AbortError") {
          setData([])
          setRecoverableError(true)
          console.log("Failed to fetch page: ", err)
        }
      }).finally(() => {
        // Only update state if the request wasn't aborted
        if (signal?.aborted !== true) {
          setIsSending(false)
          setApiResponseComplete(true)
        }
      })
  }, [start, end, CategoryId])

  useEffect(() => {
    const abortController = new AbortController()
    const signal = abortController.signal

    if (initialRender.current) {
      initialRender.current = false
      getCategories(signal).then((data: CategoriesResponse) => {
        if (!data?.response?.ok) {
          setCriticalError(true)
        } else if (data?.response?.ok) {
          setCriticalError(false)
          setCategories(data.data.results)
        } else {
          setCriticalError(true)
        }
      }).catch((e: Error) => {
        if (e.name !== "AbortError") {
          console.log(e)
          setCriticalError(true)
        }
      })
        .finally(() => {
          // Only update state if the request wasn't aborted
          if (!signal.aborted) {
            setIsSending(false)
            setApiResponseComplete(true)
          }
        })
    } else {
      fetchData(signal)
    }

    return () => { abortController.abort() }
  }, [fetchData])

  const handleChange = (e: React.ChangeEvent<HTMLSelectElement>): void => { setCategoryId(parseInt(e.target.value, 10)) }

  const removeFromLocalCache = (id: number): void => {
    setData((original) => original.filter((exp) => exp.id !== id))
  }

  const displayTitle = (): JSX.Element =>
    <div className="form-group">
      <div className="col-sm-12">
        <h3>Expenses by Category ID</h3>
      </div>
    </div>

  const displayView = (): JSX.Element => {
    if (!apiResponseComplete) {
      return CustomSpinner()
    }

    if (criticalError) {
      return FailedBanner()
    }

    // If we have no categories, we definitely have no expenses
    if (apiResponseComplete && (categories.length === 0)) {
      return <React.Fragment>{displayTitle()}
        {createCategoryGuidance()}
      </React.Fragment>
    }

    return <React.Fragment>
      <form className="form" onSubmit={preventFormSubmit}>
        {displayTitle()}
        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="start">From&nbsp;</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input type="date" id="start" name="start" onChange={e => { setstart(e.target.value) }} value={start} />
          </div>
        </div>

        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="end">To&nbsp;</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input type="date" id="end" name="end" onChange={e => { setend(e.target.value) }} value={end} />
          </div>
        </div>

        <div className="form-group">
          <div className="col-sm-12">
            <b>{genericFormError !== undefined && genericFormError !== null && Array.isArray(genericFormError) && genericFormError.length > 0 ? styleErrorMessage(genericFormError) : null}</b>
          </div>
        </div>

        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="category_field">Category&nbsp;</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            {/* NICE-TO-HAVE-234 FIX ME: allow the Pulldown component to get a name and an id peroperties, so that we can assign them to a label for the pulldown input field */}
            <Pulldown data={categories} onChange={handleChange} />
          </div>
        </div>

        <div className="form-group">
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <button className="btn btn-secondary my-2 my-sm-0" onClick={fetchData} disabled={isSending}>{isSending ? "Fetching..." : "Fetch"}</button>
          </div>
        </div>

        <div className="form-group">
          <div className="col-sm-12">
            {data.length > 0
              ? <ExpenseTable
                data={data}
                deleteFunc={async (id: number) => await deleteExpense(id)}
                updateParentData={(id: number) => { removeFromLocalCache(id) }} />
              : null}
            {recoverableError ? DismissableRecoverableErrorBanner(setRecoverableError) : null}
          </div>
        </div>
      </form>
    </React.Fragment>
  }

  return displayView()
}
