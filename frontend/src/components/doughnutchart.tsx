import React, { useEffect, useState } from "react"
import { getAssets } from "../common/api_assets"
import { aggregateAssets, sortByCurrentLocalValue } from "../common/utilities"

import { Doughnut } from "react-chartjs-2"
import type chartjs from "chart.js"
import "chartjs-plugin-colorschemes"

// NOTE: This component _intentionally_ re-fetches all assets, as the user could have added some
// assets while this chart was being displayed
export const DoughnutChart = (): JSX.Element => {
  const [chartData, setChartData] = useState({})

  useEffect(() => {
    const abortController = new AbortController()
    const signal = abortController.signal

    void getChartData(signal)

    return () => { abortController.abort() }
  }, [])

  const getChartData = async (signal?: AbortSignal): Promise<void> => {
    await getAssets(null, signal)
      .then(assets => {
        return aggregateAssets(assets.data.results).sort(sortByCurrentLocalValue)
      })
      .then(res => {
        const assets = res
        let labels = []
        let data = []
        labels = assets.map((a) => a.text)
        data = assets.map((a) => a.current_local_value)

        setChartData({
          labels,
          datasets: [
            {
              label: labels,
              data,
              fill: false
            }
          ]
        }
        )
      })
  }

  const options: chartjs.ChartOptions = {
    maintainAspectRatio: false,
    responsive: true,

    scales: {
      xAxes: [{
        stacked: true,
        display: false
      }],
      yAxes: [{
        display: false,
        stacked: true
      }]
    },
    legend: {
      labels: {
        boxWidth: 12
      }
    },
    plugins: {
      colorschemes: {
        scheme: "tableau.Classic20"
      }
    },
    tooltips: {
      mode: "label", // NOTE: remove this to avoid the mouse-hover if you want to avoid displaying too many items at the same time
      callbacks: {
        label: function (tooltipItem, data) {
          const labelName = data?.labels?.[tooltipItem?.index]
          if (tooltipItem.value === "0") return
          // Privacy mode is not implemented yet
          const isPrivacyMode = false
          if (isPrivacyMode) {
            return labelName
          } else {
            return (labelName + " " + new Intl.NumberFormat("ja-JP", {
              style: "currency",
              currency: "JPY"
            }).format(data?.datasets?.[tooltipItem?.datasetIndex]?.data?.[tooltipItem.index]))
            // return data.datasets[tooltipItem.datasetIndex].label + " " + "-" in privacy mode
          }
        }
      }
    }
  }

  return (
        <Doughnut width={800} height={800} data={chartData} options={options}/>
  )
}
