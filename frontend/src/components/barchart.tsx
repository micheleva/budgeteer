import React, { useEffect, useState } from "react"

import type { Goal, MonthlyBalanceWithFlatCategory, Obligation } from "../common/interfaces"

import { Bar } from "react-chartjs-2"
import "chartjs-plugin-colorschemes"

// Component displaying Assets, Monthly Balances, Goals and Obligations
export const BarChart = (props): JSX.Element => {
  const [chartData, setChartData] = useState({})

  useEffect(() => {
    getChartData()
  }, [])

  const getChartData = (): void => {
    const { monthlyBalances, goals, specialGoals, obligations } = props.chartData as { monthlyBalances: MonthlyBalanceWithFlatCategory[], goals: Goal[] | null, specialGoals: Goal[] | null, obligations: Obligation[] | null }
    const finalData = groupDataByCategory(monthlyBalances, goals, specialGoals, obligations)
    setChartData(finalData)
  }

  const chartOptions = generateChartOptions()
  return (
        <Bar data={chartData} height={900} options={chartOptions}/>
  )
}

// Widget used in the dashboard, displayin only monthly Assets and Monthly Balances
export const SimpleBarChart = (props: { chartData: { monthlyBalances: MonthlyBalanceWithFlatCategory[] } }): JSX.Element => {
  const [chartData, setChartData] = useState({})

  useEffect(() => {
    getChartData()
  }, [])

  const getChartData = (): void => {
    const { monthlyBalances } = props.chartData as { monthlyBalances: MonthlyBalanceWithFlatCategory[] }
    const monthsToKeep = 12
    const finalData = groupDataByCategory(monthlyBalances, null, null, null, monthsToKeep)
    setChartData(finalData)
  }

  const chartOptions = generateChartOptions()
  return (
        <Bar data={chartData} width={100} height={400} options={chartOptions}/>
  )
}

export const groupDataByCategory = (data: MonthlyBalanceWithFlatCategory[], goals: Goal[] | null = null, specialGoals = null, obligations: Obligation[] | null, monthsToKeep: number | null = null) => {
  if (data === null || data === undefined) {
    return {
      labels: [],
      datasets: []
    }
  }
  const allBalances = {}
  const idToLabel = {}
  let labels: string[] = []
  data.map((a: MonthlyBalanceWithFlatCategory) => !labels.includes(a.date) ? labels.push(a.date) : null)

  labels.sort(function (a, b) {
    return a > b ? 1 : a < b ? -1 : 0
  })

  // When monthsToKeep is passed, only return the last <monthsToKeep> months of data
  // or all the data we have, if there is less months of data than monthsToKeep
  let dataPointsToInclude = labels.length
  if (monthsToKeep !== null) {
    dataPointsToInclude = Math.min(monthsToKeep, labels.length - 1)
  }
  labels = labels.slice(-dataPointsToInclude)

  data.map((a: MonthlyBalanceWithFlatCategory) => {
    // NOTE: some balances might not be present on the first date, so we need to set this at every loop
    //       to avoid it to be undefined
    //       e.g. category_id X has only a balance for 2021-XX-01, but it is not present in any other month
    idToLabel[a.category_id] = a.category_text
    if (allBalances.hasOwnProperty(a.category_id)) {
      allBalances[a.category_id].push(a)
    } else {
      allBalances[a.category_id] = [a]
    }
    return a // To comply with the linter
  })

  const sortedDatasets = {}

  labels.map((label: string): string => {
    Object.keys(allBalances).map(categoryId => {
      if (!sortedDatasets.hasOwnProperty(categoryId)) {
        sortedDatasets[categoryId] = []
      }
      const res = allBalances[categoryId].find(x => x.date === label)
      if (res === undefined) {
        sortedDatasets[categoryId].push(0)
      } else {
        sortedDatasets[categoryId].push(res.amount)
      }
      return label // To comply with the linter
    })
    return label // To comply with the linter
  })

  const finalData = {
    labels,
    datasets: []
  }

  if (goals !== null && Array.isArray(goals) && goals.length > 0) {
    // NICE TO HAVE  sort goals by asc amount, and THEN add them to the tooltip!
    goals.map((goal: Goal) => {
      if (goal.is_archived) return
      const dataPoints = new Array(labels.length).fill(goal.amount)

      finalData.datasets.push({
        label: `Goal:${goal.text}`,
        data: dataPoints,
        type: "line",
        fill: false
      })
    })
  }

  if (obligations !== null && Array.isArray(obligations) && obligations.length > 0) {
    obligations.map((ob: Obligation) => {
      const obligationData = processObligations(ob, 24, labels)
      finalData.datasets.push(obligationData)
    })
  }

  // Add monthly balances and assets at the end, so that lines will have a higher Z value
  // and so that lines will be displayed on top of stacked bars
  Object.keys(allBalances).map(a => {
    finalData.datasets.push({
      label: idToLabel[a],
      data: sortedDatasets[a].slice(-dataPointsToInclude),
      stack: 1
    })
    return a // To comply with the linter
  })

  return finalData
}

const processObligations = (obligation: Obligation, today: number, labels: string[]): any => {
  const graphData = []
  let monthlyPayment = parseFloat(obligation.monthly_payment)
  const totalAmount = obligation.total_amount ? parseFloat(obligation.total_amount) : null
  const startDate = new Date(obligation.start_date)
  const endDate = new Date(obligation.end_date)

  let remainingAmount = totalAmount

  // Sort changes by date to ensure correct order
  const sortedChanges = obligation.changes.sort((a, b) => new Date(a.change_date).getTime() - new Date(b.change_date).getTime())

  // Calculate total amount for variable obligations if total_amount is null
  if (obligation.obligation_type === "variable" && totalAmount === null) {
    remainingAmount = 0
    let lastChangeDate = new Date(startDate)

    sortedChanges.forEach(change => {
      const changeDate = new Date(change.change_date)
      const monthsBetween = (changeDate.getFullYear() - lastChangeDate.getFullYear()) * 12 + (changeDate.getMonth() - lastChangeDate.getMonth())
      remainingAmount += monthsBetween * monthlyPayment
      monthlyPayment = parseFloat(change.new_monthly_payment)
      lastChangeDate = changeDate
    })

    // Add remaining amount from the last change to the end date
    const monthsBetweenEnd = (endDate.getFullYear() - lastChangeDate.getFullYear()) * 12 + (endDate.getMonth() - lastChangeDate.getMonth())
    remainingAmount += monthsBetweenEnd * monthlyPayment
  }

  // Create an array of values related to this obligation
  // The array index value is the x axis value (dates), and the array value is the y axis value (Obligation amount to be paid on that day)
  labels.forEach(label => {
    const labelDate = new Date(label)

    // If the Obligation has not started, do not draw anything at all
    if (labelDate < startDate) {
      graphData.push(undefined)
      return
    }

    // If the Obligation is over, draw 0 in the graph
    if (labelDate > endDate) {
      graphData.push(0)
      return
    }

    // NOTE: RemainingAmount can not be null here, as we've set it above for variable obligations
    // and fixed obligations have a totalAmount by default
    // Just in case, draw zero if it is should be negative
    if (remainingAmount === null || remainingAmount <= 0) {
      graphData.push(0)
      return
    }

    let paymentAmount = monthlyPayment

    // If this Obligation is variable, check if this date has a new payment value, or if we should use the old one
    if (obligation.obligation_type === "variable") {
      // Find the most recent change before or on the current label date
      for (const change of sortedChanges) {
        if (new Date(change.change_date) <= labelDate) {
          paymentAmount = parseFloat(change?.new_monthly_payment)
        } else {
          break
        }
      }
    }

    remainingAmount -= paymentAmount

    // TODO: if this is the last item of the array (current month), and today param is after or equal to obligation.payment_day
    // substract one more month!
    graphData.push(remainingAmount > 0 ? remainingAmount : 0)
  })

  return {
    label: `Obligation: ${obligation.name}`,
    data: graphData,
    type: "line",
    fill: false
  }
}

const generateChartOptions = () => {
  let scalesSettings = {
    x: {
      stacked: true
    },
    y: {
      stacked: true
    }
  }

  const isPrivacyMode = false
  if (isPrivacyMode) {
    scalesSettings = {
      xAxes: [{
        ticks: {
          display: true
        }
      }],
      yAxes: [{
        ticks: {
          display: false // this will remove ONLY the label
        }
      }]
    }
  }

  const chartOptions = {
    plugins: {
      title: {
        display: true,
        text: "Monthly Budgets barchart"
      }
    },
    legend: {
      display: false
    },
    responsive: true,
    maintainAspectRatio: false,
    tooltips: {
      mode: "label", // NOTE: remove this to avoid the mouse-hover if you want to avoid displaying too many items at the same time
      callbacks: {
        footer: (tooltipItems, data) => {
          // Do NOT include the goals in the sum of monthly balances
          const value = tooltipItems.reduce((a, e) => {
            if (data.datasets[e.datasetIndex].type === "line") {
              return a
            }
            return a + parseInt(e.yLabel)
          }, 0)

          const total = new Intl.NumberFormat("ja-JP", {
            style: "currency",
            currency: "JPY"
          }).format(value)

          return isPrivacyMode ? "Total: xxx" : `Total: ${total}`
        },
        label: function (tooltipItem, data) {
          if (tooltipItem.value === "0") return
          if (isPrivacyMode) {
            return data.datasets[tooltipItem.datasetIndex].label
          } else {
            return (data.datasets[tooltipItem.datasetIndex].label + " " + new Intl.NumberFormat("ja-JP", {
              style: "currency",
              currency: "JPY"
            }).format(tooltipItem.value)) // return data.datasets[tooltipItem.datasetIndex].label + " " + "-" in privacy mode
          }
        }
      }
    },
    scales: scalesSettings
  }

  return chartOptions
}
