import { useForm } from "react-hook-form"
import { Link } from "react-router-dom"

import React, { useEffect, useRef, useState, useCallback } from "react"
import { getAccounts } from "../common/api_accounts"
import { createAsset, getAssetsData } from "../common/api_assets"
import type { Account, AccountsResponse, AssetFormInput, AssetResponse, EndUserAsset, EndUserAssetResponse, getAssetsFunction } from "../common/interfaces"
import { FailedBanner, DismissableRecoverableErrorBanner, CustomSpinner } from "./shared"
import { DismissableAlert } from "./dismissablealert"
import { isValidAssetFormat } from "../common/utilities"
import Alert from "react-bootstrap/Alert"

const defaultValues: AssetFormInput = {
  account: 0,
  amount: 0,
  text: "",
  // Length 3 as per ISO 4217
  foreign_currency: "",
  current_foreign_value: 0,
  current_local_value: 0,
  bought_price_foreign: 0,
  bought_price_local: 0,
  net_loss_foreign: 0,
  net_loss_local: 0,
  record_date: ""
}

const text = String.raw`
[
  {"ticker": "XXXX",
   "amount": 1337, // number of assets owned
   "currency": "USD",
   "sum_current": {"foreign": 420.69, "local": 45013.83}, // This is the current value is USD and JPY multiplied by the number of assets owned (amount)
   "sum_bought": {"foreign": 4206.9, "local": 450138}, // This is the amount of USD and the correspective in JPY used to aquire the asset
   "sum_net": {"foreign": -3786.21, "local": -405124.17}, // This is the difference among the two abpve fields
   "rate": 110.42, # currency to local currency change rate
  },
  {"ticker": "YYY",
   "amount": 42,
   "currency": "JPY",
   "sum_current": {"foreign": 0.0, "local": 420000}, // e.g 420,000 ÷ 42 = The frontend will infer that each of those 42 assets is currently worth 10,000 JPY each
   "sum_bought": {"foreign": 0.0, "local": 413100},
   "sum_net": {"foreign": 0.0, "local": 6900},
   "rate": 110.42, # currency to local currency change rate
  },
]`

const cmdTooltip = "TO spin up a mock data server, please run `python manage.py assets-api-mock-server` in a different terminal"

const getAssetsDataWrapper = async (callback: getAssetsFunction, account: Account, targetDate: string, importUrl: string, setShowDismissable: React.Dispatch<React.SetStateAction<boolean>>): Promise<EndUserAssetResponse> => {
  setShowDismissable(true)
  return await callback(account, targetDate, importUrl)
}

export const AutomaticAssets = (): JSX.Element => {
  // eslint-disable-next-line @typescript-eslint/unbound-method
  const { reset } = useForm<AssetFormInput>()
  const [isSending, setIsSending] = useState(false)
  const initialRender = useRef(true)
  const [targetDate, setTargetDate] = useState<string>(new Date().toLocaleDateString("en-CA"))
  const [importUrl, setImportUrl] = useState<string>("http://localhost:8080/json_files_with_rate")
  const [accounts, setAccounts] = useState<Account[]>([])

  const [showDismissable, setShowDismissable] = useState(true)

  const [accountsWithData, setAccountsWithData] = useState(-1)
  const [assetCreated, setassetCreated] = useState(0)
  const [totalAssets, setTotalAssets] = useState(0)
  const [malformedAssets, setMalformedAssets] = useState(0)
  const [erroredAssets, setErroredAssets] = useState(0)

  const [apiResponseComplete, setApiResponseComplete] = useState(false)
  const [criticalError, setCriticalError] = useState(false)
  const [recoverableError, setRecoverableError] = useState(false)

  const resetComponentState = (): void => {
    // Reset the component state in case we get back to this view through a link, and the view gets reused
    setAccountsWithData(-1)
    setassetCreated(0)
    setTotalAssets(0)
    setMalformedAssets(0)
    setErroredAssets(0)
  }

  const fetchData = useCallback(async () => {
    if (isSending) return
    setIsSending(true)
    resetComponentState()

    // Counters to display how many assets have been created (i.e. text updated as soon as API returns. A primitive process bar using text)
    const created: number[] = []
    const totalAssetsToBeCreated: number[] = []
    // We do not call setTotalAssets(<asset>) as it might incur in race conditions.
    // We instead push dummy numbers in the array, as it is more likely atomic,
    // and set component's totalAssets to the length of this dummy array instead

    const toBeCreatePromises: Array<Promise<AssetResponse>> = []
    const malformed: number[] = []
    const errored: number[] = []

    // NOTE: this intentionally duplicates the component's criticalError
    // As we need to set the component's one only after ALL promises have been fulfilled
    let isRecoverableError = false

    const accPromises = accounts.map(async account => await getAssetsDataWrapper(getAssetsData, account, targetDate, importUrl, setShowDismissable))

    const createAndQueueStockCreatePromise = (stock: EndUserAsset, account: Account, created: number[]): void => {
      const postData = {
        account: account.id,
        amount: stock.amount,
        text: stock.ticker,
        foreign_currency: stock.currency,
        current_foreign_value: stock.sum_current.foreign,
        current_local_value: stock.sum_current.local,
        bought_price_foreign: stock.sum_bought.foreign,
        bought_price_local: stock.sum_bought.local,
        net_loss_foreign: stock.sum_net.foreign,
        net_loss_local: stock.sum_net.local,
        rate: stock.rate,
        record_date: targetDate
      }

      toBeCreatePromises.push(
        createAsset(postData).then((result: AssetResponse) => {
          setShowDismissable(true)
          // This internal promise will update the banner with "Created <n>/<total> assets" (our primitive progress bar)
          // as soon as they're created!
          if (result?.response?.ok && result?.data?.id !== 0) {
            created.push(result?.data?.id)
            setassetCreated(created.length)
          } else {
            errored.push(1)
            setErroredAssets(errored.length)
            console.log(`Account ${account.id} ${stock.ticker} has failed to be created for $[targetDate}`) // tin here record_date var is not defined. confirm if targetDate is defined
          }
          return result
        })
      )
    }

    await Promise.allSettled(accPromises).then(results => {
      // notEmptyAccNum is displayed to screen to show how many accounts have succeeded
      const notEmptyAccNum = results.reduce((count, result) => {
        if (result.status === "fulfilled") {
          const assets = result.value.data.assets
          return count + ((result.value.response.ok && Array.isArray(assets) && assets.length > 0) ? 1 : 0)
        }
        return count
      }, 0)
      setAccountsWithData(notEmptyAccNum)

      // FIXME: FireFox has a weird issue in which map has to be stored in a variable. Find a wokraround!
      results.map(async (accPromRes) => {
        // Only create assets from accounts that had at least one asset
        if (accPromRes.status === "fulfilled" && accPromRes.value.data.assets.length > 0) {
          const assets = accPromRes?.value?.data?.assets
          if (accPromRes?.value?.response?.ok && Array.isArray(assets) && assets.length > 0 && accPromRes?.value?.data?.account !== undefined) {
            const account = accPromRes.value.data.account
            // eslint-disable-next-line array-callback-return
            assets.map((asset: any): void => {
              totalAssetsToBeCreated.push(1)
              setTotalAssets(totalAssetsToBeCreated.length)

              if (isValidAssetFormat(asset)) {
                createAndQueueStockCreatePromise(asset, account, created)
              } else {
                // The third party API has returned a unepected format object: let the user know (but do not abort the import)
                malformed.push(1)
                setMalformedAssets(malformed.length)
              }
            })

            if (toBeCreatePromises.length === 0) {
              // NICE TO HAVE: how a meaningful error that suggests to the user to confirm whether the date is correct, as all accounts have no data.
              // Also, it might suggest as well to double check whether their endpoint is returning data correctly or not
              console.error(`No account have any assets. Are you sure the ${targetDate} is in the correct format, and is the date you meant to fetch data for? Is your endpoint up and running? Does it return data in the correct format?`)
            }
          }
        } else {
          // NICE TO HAVE: account entities should have a has_assets boolean, and we only select accounts with assets in this function (e.g. getAccounts gets a value to filter out accounts without assets)
          if (accPromRes.status === "fulfilled") {
            console.log(`Account ID: ${accPromRes.value.data.account.id}, ${accPromRes.value.data.account.text} seems to have no data`)
          } else {
            console.log(`Account promise was rejected: ${accPromRes.reason}`)
          }
        }
      })

      // In order to have the finally() block called only once, we wrap allSettled() in another Promise
      // the reason is that we're altering the content of toBeCreatePromises dynamically and in an async way
      // This is a dirty woraround
      const assetsPromsiesWrapper = new Promise((resolve) => {
        Promise.allSettled(toBeCreatePromises)
          .then((results) => {
            results.forEach((result) => {
              if (result.status !== "fulfilled") {
                console.log(result)
                isRecoverableError = true
              }
            })
            setRecoverableError(isRecoverableError)
          }).catch((err) => {
            console.log("Failed to fetch page: ", err)
            setRecoverableError(true)
          }).finally(() => {
            resolve(1) // Resolve the wrapper promise after the inner Promise.allSettled is done
          })
      })
      assetsPromsiesWrapper
        .catch((error) => {
          console.error("Error in assetsPromsiesWrapper:", error)
        })
        .finally(() => {
          setIsSending(false)
          console.log("Finally statement executing firing after all toBeCreatePromises have settled.")
        })
    })
  }, [targetDate, accounts, importUrl])

  useEffect(() => {
    if (initialRender.current) {
      initialRender.current = false
      getAccounts().then((accs: AccountsResponse) => {
        if (!accs.response?.ok) {
          setCriticalError(true)
        } else if (accs?.response?.ok) {
          setCriticalError(false)
          const accountRes = accs.data.results
          setAccounts(accountRes)
        } else {
          setCriticalError(true)
        }
      }).catch((e) => {
        console.log(e)
        setCriticalError(true)
      })
        .finally(() => {
          reset(defaultValues)
          setIsSending(false)
          setApiResponseComplete(true)
        })
    }
  }, [fetchData])

  const displayTitle = (): JSX.Element => {
    return <React.Fragment><h3>Automatically import assets data</h3></React.Fragment>
  }

  const displayCreateDataGuidance = (): JSX.Element => {
    return <div>
           <h5 className="text-info">You need to create an Account first! </h5>
           <Link className="btn btn-info mt-2 " to={"/app/accounts/creation"}>Create an Account</Link>
        </div>
  }

  const displayView = (): JSX.Element => {
    // IMPORTANT: We need to call setValue() AFTER render (with the form) has run
    // of the form will render undefined (i.e. "") in each field
    // as per https://stackoverflow.com/a/59547360

    // This means we can not use a flag like apiResponseComplete
    // as we do in goalstab.tsx

    // FIXME: BUGFIX-245
    // TRY ME OUT <===========
    // TODO: make the form 'hidden' using css, and see if the setValue() binding works when the form is not visible

    if (!apiResponseComplete) {
      return CustomSpinner()
    }

    if (criticalError) {
      return FailedBanner()
    } else if (accounts.length > 0) {
      return (
          <div>
            <div className="form-group">
              { displayTitle() }
              { recoverableError ? DismissableRecoverableErrorBanner(setRecoverableError) : null }
              <label htmlFor="targetDate">Date:&nbsp;</label>
              <input type="date" id="targetDate" className="form-control col-sm-6 col-md-4 3 col-lg-3" name="targetDate" onChange={e => { setTargetDate(e.target.value) }} value={targetDate}/>

              <label htmlFor="importUrl">Import url:&nbsp;</label>
              {/* NICE TO HAVE: Validate the text field to be a well-formatted URL */}
              <input type="text" id="importUrl" className="form-control col-sm-6 col-md-4 3 col-lg-3" name="importUrl" onChange={e => { setImportUrl(e.target.value) }} value={importUrl}/>
              <button onClick={fetchData} className="btn btn-secondary my-2 mr-2" disabled={isSending}>{ isSending ? "Fetching..." : "Fetch" }</button>
              <DismissableAlert url={importUrl} date={targetDate} accounts={accounts} text={text} commandTooltip={cmdTooltip}/>
               {accountsWithData > -1 && showDismissable
                 ? <Alert variant="primary" onClose={() => { setShowDismissable(false) }} dismissible>
                    <Alert.Heading>Results:</Alert.Heading>
                       <div>{accountsWithData}/{accounts.length} accounts have data for {targetDate}</div>
                       { (assetCreated > 0 || totalAssets > 0) && showDismissable ? <div>Created {assetCreated}/{totalAssets} assets entries</div> : null}
                       { (assetCreated > 0 && totalAssets > 0 && assetCreated === totalAssets) && showDismissable ? <div><b>All assets have been created successfully!</b></div> : null}
                       { erroredAssets > 0 && showDismissable ? <b><div className="text-danger">{erroredAssets} assets(s) creation failed.</div></b> : null}
                       { malformedAssets > 0 && showDismissable ? <b><div className="text-danger">{importUrl} returned {malformedAssets} malformed data point(s). Could not create assets for it(those). </div></b> : null}
                  </Alert>
                 : null}
              { isSending ? CustomSpinner() : null}
            </div>
          </div>
      )
    } else {
      return <div>{displayTitle()} {displayCreateDataGuidance()}</div>
    }
  }
  return displayView()
}
