import { useForm } from "react-hook-form"
import Alert from "react-bootstrap/Alert"
import React, { useState, useCallback } from "react"
import { createAccount } from "../common/api_accounts"
import type { AccountFormInputs, ReactFragment } from "../common/interfaces"
import { styleErrorMessage } from "./shared"

/**
 * This component displays a form to create an account.
 *
 * @returns A JSX element displaying the form to create an account.
 */
const defaultValues: AccountFormInputs = {
  text: "",
  credit_entity: "",
  is_foreign_currency: false
}

export const CreateAccount = (): JSX.Element => {
  // eslint-disable-next-line @typescript-eslint/unbound-method
  const { register, handleSubmit, setError, formState: { errors }, reset } = useForm<AccountFormInputs>()
  const [isSending, setIsSending] = useState(false)
  const [showDismissable, setShowDismissable] = useState(false)
  const [isUpdateSuccess, setIsUpdateSuccess] = useState(false)
  const autoCloseDismissibleAfterMs = 1500

  const [genericFormError, setGenericFormError] = useState("")
  const abortControllerRef = React.useRef<AbortController | null>(null)

  // Clean up function to abort any in-flight requests when component unmounts
  React.useEffect(() => {
    return () => {
      if (abortControllerRef.current !== null) {
        abortControllerRef.current.abort()
      }
    }
  }, [])

  const onSubmit = useCallback((formValues: AccountFormInputs) => {
    if (isSending) return

    // Abort any existing request
    if (abortControllerRef.current !== null) {
      abortControllerRef.current.abort()
    }

    // Create a new AbortController for this request
    abortControllerRef.current = new AbortController()
    const signal = abortControllerRef.current.signal

    setShowDismissable(false)
    setIsSending(true)
    createAccount(formValues, signal)
      .then((res) => {
        if (!res?.response?.ok) {
          if (res?.data !== undefined) {
            for (const [key, value] of Object.entries(res.data)) {
              console.log("Account creation form error(s):", key, value)
              // Set error for existing fields
              if (key in formValues) {
                setError(key as keyof AccountFormInputs, { message: value[0] })
              } else {
                // Account has a "text AND credit_entity" unique constraints
                if (value[0] === "An Account with the same text AND credit_entity already exists for this user.") {
                  setError("text", { message: value[0] })
                } else {
                // Other validation errors
                  setGenericFormError(value[0])
                }
              }
            }
          } else {
            // Handle 40X,50X errors in here
            setGenericFormError("Something is wrong with the server, try again later!")
          }
          setIsUpdateSuccess(false)
        } else {
          reset(defaultValues)
          setGenericFormError("")
          setIsUpdateSuccess(true)
        }
      })
      .catch(error => {
        // Don't show error for aborted requests
        if (error.name !== "AbortError") {
          console.log("Account creation error:", error)
          setIsUpdateSuccess(false)
        }
      })
      .finally(() => {
        // Only update state if the request wasn't aborted
        if (!signal.aborted) {
          setIsSending(false)
          setShowDismissable(true)
          const timer = setTimeout(() => {
            setShowDismissable(false)
          }, autoCloseDismissibleAfterMs)
          return () => { clearTimeout(timer) }
        }
      })
  }, [reset, isSending])

  const displayTitle = (): JSX.Element => {
    return <div className="col-sm-12"><h3>Create an Account</h3></div>
  }

  const renderDismissable = (): JSX.Element | null => {
    if (showDismissable) {
      return <Alert variant={isUpdateSuccess ? "success" : "danger"} onClose={() => { setShowDismissable(false) }} dismissible>
              <Alert.Heading>{isUpdateSuccess ? "Account created!" : "Account creation failed :("}</Alert.Heading>
            </Alert>
    } else {
      return null
    }
  }

  const displayCreationForm = (): ReactFragment => {
    return (
        <form onSubmit={handleSubmit(onSubmit)} className="form">
        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="text_field">Account name</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input id="text_field" className="form-control" name="text" type="text" placeholder="Insert the Account name"
               ref={register({
                 required: "This field is required.",
                 maxLength: {
                   value: 20,
                   message: "Max 20 chars"
                 }
               })}
            />
            {((errors?.text) != null) ? styleErrorMessage(errors.text.message as string) : null}
          </div>
        </div>

      <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="credit_entity">Credit entity name</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input name="credit_entity" id="credit_entity" className="form-control"
               ref={register({
                 required: "This field is required.",
                 maxLength: {
                   value: 20,
                   message: "Max 20 chars"
                 }
               })}
            />
            {((errors?.credit_entity) != null) ? styleErrorMessage(errors.credit_entity.message as string) : null}
          </div>
      </div>

      <div className="form-group">
        <div className="col-sm-6 col-md-4 3 col-lg-3">
        <label><input type="checkbox" id="is_foreign_currency" name="is_foreign_currency" ref={register()} />&nbsp;Is a foreign currency account</label>
        {((errors?.is_foreign_currency) != null) ? styleErrorMessage(errors.is_foreign_currency.message as string) : null}
        </div>
      </div>

      <div className="form-group">
        <div className="col-sm-6 col-md-4 3 col-lg-3">
          {(genericFormError.length > 0) ? styleErrorMessage(genericFormError) : null}
          <button className="btn btn-secondary my-2 my-sm-0" disabled={isSending}>{ isSending ? "Submitting..." : "Submit" }</button>
        </div>
      </div>

      </form>
    )
  }

  const displayView = (): JSX.Element => {
    // Acccount has no foreign keys, hence we do not need a spinner, as we do not fetch other entities
    return <React.Fragment>
      { displayTitle() }
      { renderDismissable() }
      { displayCreationForm() }
    </React.Fragment>
  }

  return displayView()
}
