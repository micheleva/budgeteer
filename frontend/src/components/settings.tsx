import React, { useEffect, useState } from "react"
import { getExchangeRate, getThemeSettings } from "../common/api_settings"

const Settings = (): JSX.Element => {
  const [exchangeRate, setExchangeRate] = useState<number | null>(null)
  const [lastUpdated, setLastUpdated] = useState<string>("")
  const [darkTheme, setDarkTheme] = useState<boolean>(false)
  const [themeLastUpdated, setThemeLastUpdated] = useState<string>("")

  useEffect(() => {
    const abortController = new AbortController()
    const signal = abortController.signal

    const fetchExchangeRate = async (): Promise<void> => {
      try {
        const response = await getExchangeRate(signal)
        if (response.success) {
          setExchangeRate(response.data.exchange_rate)
          setLastUpdated(response.data.last_updated)
        }
      } catch (error) {
        if (error instanceof Error && error.name !== "AbortError") {
          console.error("Failed to fetch exchange rate:", error)
        }
      }
    }

    const fetchThemeSettings = async (): Promise<void> => {
      try {
        const response = await getThemeSettings(signal)
        if (response.success) {
          setDarkTheme(response.data.dark_theme)
          setThemeLastUpdated(response.data.last_updated)
        }
      } catch (error) {
        if (error instanceof Error && error.name !== "AbortError") {
          console.error("Failed to fetch theme settings:", error)
        }
      }
    }

    void fetchExchangeRate()
    void fetchThemeSettings()

    return () => {
      abortController.abort()
    }
  }, [])

  return (
    <div className="container mt-4">
      <h1>Settings</h1>
      <div className="card">
        <div className="card-body">
          <h2 className="card-title">Application Settings</h2>
          {/* Theme Settings Section */}
          <div className="mb-4">
            <h3>Theme</h3>
            <div className="form-check">
              <input
                className="form-check-input"
                type="radio"
                name="theme"
                id="lightTheme"
                checked={!darkTheme}
                disabled
              />
              <label className="form-check-label" htmlFor="lightTheme">
                Light Theme
              </label>
            </div>
            <div className="form-check">
              <input
                className="form-check-input"
                type="radio"
                name="theme"
                id="darkTheme"
                checked={darkTheme}
                disabled
              />
              <label className="form-check-label" htmlFor="darkTheme">
                Dark Theme
              </label>
            </div>
            <div className="alert alert-warning mt-2">
              Theme settings are managed by your instance administrator
            </div>
            <small className="text-muted d-block mt-1">
              Last updated: {themeLastUpdated !== "" ? new Date(themeLastUpdated).toLocaleString() : "Loading..."}
            </small>
          </div>

          {/* Currency Settings Section */}
          <div className="mb-4">
            <h3>Primary Currency Selection</h3>
            <div className="mb-3">
              <label className="form-label">Local Currency</label>
              <select className="form-select ml-2" aria-label="Local currency selection" disabled defaultValue="JPY (¥)">
                <option>JPY (¥)</option>
              </select>
            </div>
            <div className="alert alert-warning">
              The local currency is set by your instance administrator
            </div>
          </div>

          {/* Foreign Currency Settings Section */}
          <div className="mb-4">
            <h3>Foreign Currency Settings <span className="badge bg-secondary">Alpha</span></h3>
            <div className="mb-3">
              <label className="form-label">Primary Foreign Currency</label>
              <select className="form-select ml-2" aria-label="Primary foreign currency selection" disabled defaultValue="USD ($)">
                <option>USD ($)</option>
              </select>
              <small className="text-muted d-block mt-1">
                Exchange rate: $1 = {(exchangeRate !== null ? `${exchangeRate.toFixed(4)} ¥` : "Loading...")}
              </small>
              <small className="text-muted d-block mt-1">
                Last updated: {lastUpdated !== "" ? new Date(lastUpdated).toLocaleString() : "Loading..."}
              </small>
            </div>
            <div className="mb-2">
              <label className="form-label">Secondary Foreign Currency</label>
              <select className="form-select ml-2" aria-label="Secondary foreign currency selection" disabled defaultValue="EUR (€)">
                <option>EUR (€)</option>
              </select>
              <small className="text-muted d-block mt-1">
                Exchange rate: 1€ = {(exchangeRate !== null ? `${exchangeRate.toFixed(4)} ¥` : "Loading...")}
              </small>
              <small className="text-muted d-block mt-1">
                Last updated: {lastUpdated !== "" ? new Date(lastUpdated).toLocaleString() : "Loading..."}
              </small>
            </div>
            <div className="alert alert-warning mt-2">
              Foreign currency settings are in alpha and cannot be modified
            </div>
            <div className="alert alert-info mt-2">
              Note: Exchange rates are set by your instance administrator<br/>
            </div>
          </div>

          {/* Privacy Mode Section */}
          <div className="mb-4">
            <h3>Privacy Mode <span className="badge bg-secondary">Being developed</span></h3>
            <div className="form-check form-switch">
              <input
                className="form-check-input"
                type="checkbox"
                role="switch"
                id="privacyMode"
                disabled
              />
              <label className="form-check-label" htmlFor="privacyMode">
                Enable Privacy Mode
              </label>
            </div>
            <div className="alert alert-info mt-2">
              Privacy Mode masks sensitive financial information when enabled, replacing exact amounts with asterisks (e.g., &quot;¥***,***&quot;). This feature helps prevent shoulder surfing and protects your financial privacy when using the application in public spaces.
            </div>
            <div className="alert alert-warning mt-2">
              Privacy Mode is currently under development and will be available in a future update.
            </div>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Settings
