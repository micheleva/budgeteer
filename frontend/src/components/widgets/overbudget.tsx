import React, { useEffect, useState } from "react"

import type { overBudgetedCategory } from "../../common/interfaces"
import { SmartFormatter } from "../../common/utilities"
import { makeWidget, displayCreateDataGuidance } from "./widget_helpers"
import { getBudgets, getYearlyBudgetAggregate } from "../../common/api_budgets"

/**
 * Formats over budget details.
 *
 * This function formats over budget details for a given year. It checks if there are any monthly or yearly budgets for the year.
 * If there are no budgets, it displays guidance to create budgets. If there are over budgeted categories, it displays the details.
 *
 * @param {overBudgetedCategory[]} over - An array of over budgeted categories
 * @param {Object} budgets - An object containing budget information
 * @returns {React.ReactNode} A React node displaying over budget details or guidance to create budgets
 */
const formatOverBudgetDetails = (over: overBudgetedCategory[], budgets): React.ReactNode => {
  const currencyFormatter = SmartFormatter("JPY")

  // setBudgets({ numberOfCategoriesWithAYearlyBudget: bdgts?.data?.results?.length, aggregateMonthlyBudgetOverthisYear: bdgtAggregates?.data?.results?.length })
  const generateBody = (): React.ReactNode => {
    if (budgets.aggregateMonthlyBudgetOverthisYear === 0) {
      return <div>
        <h5 className="text-info">You have no Monthly Spending Budgets for this year yet: go and create some!</h5>
        {displayCreateDataGuidance()}
      </div>
    }

    if (budgets.numberOfCategoriesWithAYearlyBudget === 0) {
      return <div>
        <h5 className="text-info">You have no Yearly Spending Budgets for this year yet: go and create some!</h5>
        {displayCreateDataGuidance()}
      </div>
    }

    if (over.length === 0) {
      return <h5 className="text-success">Monthly Spending budgets are within your Yearly Spending Budgets: good job!</h5>
    }

    return over.map((item: overBudgetedCategory) => {
      return (
        <li key={item.categoryId} className="list-group-item">
          {item.name} : <span className="text-danger">{currencyFormatter.format(item.overbudgetedAmount)}</span>
        </li>
      )
    })
  }

  return (
    <div className="container">
      <ul className="list-group">
        {generateBody()}
      </ul>
    </div>
  )
}


/**
 * Widget that displays over budgeted amounts for a given year.
 *
 * This widget fetches budget and yearly budget aggregate data, then identifies over budgeted categories.
 * It handles loading states, error states, and provides guidance when no categories exist.
 * 
 * The widget uses AbortController to properly clean up API requests when unmounted to prevent memory leaks.
 *
 * @returns {React.ReactNode} A widget displaying over budgeted amounts or guidance to create budgets
 */
export const yearlyOverbudgetedWidget = (): React.ReactNode => {
  const [target, setTarget] = useState({ year: new Date().getFullYear(), month: null })
  const [isFailed, setIsFailed] = useState(false)
  const [hasCompleted, setIsCompleted] = useState(false)
  const [overdBudgetedCat, setOverdBudgetedCat] = useState<overBudgetedCategory[]>([])
  const [budgets, setBudgets] = useState({ numberOfCategoriesWithAYearlyBudget: 0, aggregateMonthlyBudgetOverthisYear: 0 })

  const forceReload = (): void => {
    setTarget({ year: new Date().getFullYear(), month: null })
  }

  useEffect(() => {
    const controller = new AbortController()
    const signal = controller.signal

    const fetchData = async (): Promise<void> => {
      try {
        setIsCompleted(false)
        const bdgts = await getBudgets(target, signal)
        const bdgtAggregates = await getYearlyBudgetAggregate(target, signal)

        if (!signal.aborted) {
          setIsCompleted(true)
          if (!bdgts?.response?.ok || !bdgtAggregates?.response.ok) {
            setIsFailed(true)
            return
          }

          const budgets = bdgts?.data?.results?.map(
            bdgt => {
              return {
                id: bdgt?.category?.id ? bdgt?.category?.id : bdgt?.savingcategory?.id,
                text: bdgt?.category?.id ? bdgt?.category?.text : bdgt?.savingcategory?.text,
                amount: parseFloat(bdgt?.amount)
              }
            })
            .sort((a, b) => a.id > b.id)

          const aggregates = bdgtAggregates?.data?.results?.map(
            agg => {
              return {
                id: agg?.category,
                amount: agg?.sum_amount
              }
            })
            .sort((a, b) => a.id > b.id)
          // FIXME: find out how is it possible that these two endpoints return different amount of items
          // Maybe we have monthly budgets for categories that do not have a yearly budget yet.. or viceversa?

          const overbudgetedDetails = aggregates
            .filter((agg: BudgetYearlyAggregatePagedData) => budgets.some(budget => budget.id === agg.id && agg.amount > budget.amount))
            .map((over: BudgetYearlyAggregatePagedData): any => {
              const matchingBudget = budgets.find(budget => budget.id === over.id)
              if (matchingBudget !== undefined) {
                return {
                  categoryId: over.id,
                  name: matchingBudget.text,
                  overbudgetedAmount: over.amount - matchingBudget.amount
                }
              }
              return null // Make the linter happy
            })

          setOverdBudgetedCat(overbudgetedDetails)
          setBudgets({
            numberOfCategoriesWithAYearlyBudget: bdgts?.data?.results?.length,
            aggregateMonthlyBudgetOverthisYear: bdgtAggregates?.data?.results?.length
          })
        }
      } catch (error) {
        if (error instanceof Error && error.name !== "AbortError" && !signal.aborted) {
          console.error("Failed to fetch data:", error)
          setIsFailed(true)
        }
      }
    }

    void fetchData()

    return () => {
      controller.abort()
    }
  }, [target])

  // Handle case where we have no monthly nor yearly budgets
  const content = formatOverBudgetDetails(overdBudgetedCat, budgets)
  const title = "Monthly Overbudgeted Amounts"
  return makeWidget(title, hasCompleted, isFailed, forceReload, content)
}
