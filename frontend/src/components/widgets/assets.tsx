import React, { useEffect, useState } from "react"
import { Link } from "react-router-dom"

import { getAssets, getAllMonthlyBalancesAndAssets } from "../../common/api_assets"
import { reduceAssets } from "../../common/utilities"

import { groupDataByCategory } from "../../components/barchart"
import { makeWidget } from "./widget_helpers"

/**
 * Widget that displays asset statistics.
 *
 * This widget fetches asset and monthly balance data, then calculates the net loss percentage and the increase/decrease percentage.
 * It handles loading states, error states, and provides guidance when no data exists.
 *
 * @returns {React.ReactNode} A widget displaying asset statistics or guidance to create data
 */
export const assetsStatsWidget = (): React.ReactNode => {
  const [isSending, setIsSending] = useState(false)
  const [netLoss, setNetLoss] = useState<number>(0)
  const [increase, setIncrease] = useState<number>(0)
  const [months, setMonths] = useState<number>(0)
  const [noMbNorAssets, setNoMbNorAssets] = useState(false)
  const [apiResponseComplete, setApiResponseComplete] = useState(false)
  const [isFailed, setIsFailed] = useState(false)

  const [reloadTrigger, setReloadTrigger] = useState({})

  const forceReload = (): void => {
    setReloadTrigger({})
  }

  useEffect(() => {
    const controller = new AbortController()
    const signal = controller.signal

    const fetchData = async (): Promise<void> => {
      if (isSending || signal.aborted) return
      try {
        setIsSending(true)
        setApiResponseComplete(false)

        const assets = await getAssets(null, signal)
        const mbAss = await getAllMonthlyBalancesAndAssets(signal)

        if (signal.aborted) return

        setApiResponseComplete(true)
        setIsSending(false)

        if (!assets?.response?.ok || !mbAss?.response?.ok) {
          setIsFailed(true)
          return
        }

        const assetsAggregate = reduceAssets(assets.data.results)

        // Case where user has no data yet
        let perc = 0
        setNoMbNorAssets(true)

        // If the user has any data, calculate the current net/loss percentage
        if (assetsAggregate?.bought_price_foreign !== 0) {
          perc = (assetsAggregate.net_loss_foreign * 100 / assetsAggregate.bought_price_foreign)
          setNoMbNorAssets(false)
        }
        setNetLoss(perc)

        // Try to get 12 months worth of past data. We'll get less months if there is not enough past data
        const monthsToKeep = 12
        const finalData = groupDataByCategory(mbAss.data, null, null, monthsToKeep)

        // Calculate the sum of this month, and the oldest month (12 months or less) data
        const today = finalData.datasets.reduce((acc, curr) => {
          if (curr === undefined) {
            return acc
          }
          return acc + curr?.data[curr?.data?.length - 1]
        }, 0)
        const past = finalData.datasets.reduce((acc, curr) => {
          if (curr === undefined) {
            return acc
          }
          return acc + curr?.data[0]
        }, 0)
        const diff = today - past
        setMonths(finalData?.labels?.length - 1)
        setIncrease(diff * 100 / past)
      } catch (error) {
        if (error instanceof Error && error.name !== "AbortError" && !signal.aborted) {
          console.error("Failed to fetch data:", error)
          setIsFailed(true)
          setApiResponseComplete(true)
          setIsSending(false)
        }
      }
    }

    void fetchData()

    return () => {
      controller.abort()
    }
  }, [reloadTrigger])

  const displayCreateDataGuidance = (): JSX.Element => {
    const currYearMonthWihtOne = `${new Date().getFullYear()}-${String(new Date().getMonth() + 1).padStart(2, "0")}-01`
    return <div>
      <h5 className="text-info">You need to create some Monthly Balance or Assets first! </h5>
      <div><Link className="btn btn-info mt-2 " to={"/app/assets/import"}>Import Assets data</Link></div>
      <div><Link className="btn btn-info mt-2 " to={`/app/monthly-balances/multiple-creation/date/${currYearMonthWihtOne}`}>Create Monthly Balances</Link></div>
    </div>
  }

  const displayView = (): React.ReactNode => {
    if (noMbNorAssets) {
      return displayCreateDataGuidance()
    }
    return <div>
      <h5 className={netLoss >= 0 ? "text-success" : "text-danger"}>
        Current portfolio {netLoss >= 0 ? "gain" : "loss"}: {netLoss.toLocaleString("en-CA", { minimumFractionDigits: 2, maximumFractionDigits: 2 })}%
      </h5>
      <h5 className={increase >= 0 ? "text-success" : "text-danger"}>
        Portfolio {increase >= 0 ? "increase" : "decrease"} vs {months} months ago: {increase.toLocaleString("en-CA", { minimumFractionDigits: 2, maximumFractionDigits: 2 })}%
      </h5>
      <Link className="btn btn-info mt-2 " to={"/app/assets"}>See Details</Link>
    </div>
  }

  const title = "Assets stats"
  return makeWidget(title, apiResponseComplete, isFailed, forceReload, displayView())
}
