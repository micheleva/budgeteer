import { SpendingLimitDisplay } from "../../components/spendinglimit"

/**
 * Widget that displays spending limits.
 *
 * This widget fetches spending limit data and displays it in a formatted manner.
 * It handles loading states, error states, and provides guidance when no spending limits exist.
 *
 * @returns {React.ReactNode} A widget displaying spending limits or guidance to create spending limits
 */
export const spendingLimitsWidget = (): React.ReactNode => {
  return SpendingLimitDisplay()
}
