import React, { useEffect, useState } from "react"
import { Link } from "react-router-dom"

import "chartjs-plugin-colorschemes"
import { Pie } from "react-chartjs-2"
import { getExpensesAggregateByCategory } from "../../common/api_expenses"
import { getAMonthAgoDate } from "../../common/utilities"
import { makeWidget } from "./widget_helpers"
import type { PieGraph } from "../../common/interfaces"

/**
 * Widget that displays an expense pie chart.
 *
 * This widget fetches expense data and displays it in a pie chart.
 * It handles loading states, error states, and provides guidance when no data exists.
 *
 * @returns {React.ReactNode} A widget displaying an expense pie chart or guidance to create data
 */
export const expensePieWidget = (): React.ReactNode => {
  const [chartData, setChartData] = useState<PieGraph>({} as PieGraph)
  const [start, setStart] = useState<string>(getAMonthAgoDate())
  const [end, setEnd] = useState<string>(new Date().toLocaleDateString("en-CA"))
  const [hasCompleted, setIsCompleted] = useState(false)
  const [isFailed, setIsFailed] = useState(false)

  useEffect(() => {
    const controller = new AbortController()
    const signal = controller.signal

    const fetchData = async (): Promise<void> => {
      try {
        setIsCompleted(false)
        const data = await getExpensesAggregateByCategory(start, end, signal)

        if (signal.aborted) return
        setIsCompleted(true)
        setChartData({
          labels: data.data.map((row: ExpensesAggregateByCategory) => row.category_text),
          datasets: [
            {
              label: "Expenses by Category",
              data: data.data.map((row: ExpensesAggregateByCategory) => row.total_amount),
              hoverOffset: 4
            }
          ]
        })
      } catch (error) {
        if (error instanceof Error && error.name !== "AbortError" && !signal.aborted) {
          console.error("Error fetching data:", error)
          setIsFailed(true)
          setIsCompleted(true)
        }
      }
    }

    void fetchData()

    return () => {
      controller.abort()
    }
  }, [start, end])

  const forceReload = (): void => {
    setStart(getAMonthAgoDate())
    setEnd(new Date().toLocaleDateString("en-CA"))
  }

  const formattedTotal = (): string => {
    let total = 0
    if (chartData?.datasets?.length > 0 && chartData?.datasets[0]?.data.length > 0) {
      total = chartData?.datasets[0]?.data.reduce((a: number, b: number) => a + b, 0)
    }
    return new Intl.NumberFormat("ja-JP", {
      style: "currency",
      currency: "JPY"
    }).format(total)
  }

  const displayView = (): React.ReactNode => {
    const isPrivacyMode = false
    const chartOptions = {
      plugins: {
        title: {
          display: true,
          text: "Expenses by Category"
        }
      },
      legend: {
        display: false
      },
      responsive: true,
      maintainAspectRatio: false,
      tooltips: {
        // NOTE: remove the callback and labels to avoid the mouse-hover if you want to avoid displaying too many items at the same time
        mode: "label",
        callbacks: {
          label: function (tooltipItem: any, data: any) {
            const labelName = data.labels[tooltipItem.index]
            if (tooltipItem.value === "0") return
            if (isPrivacyMode) {
              return labelName
            } else {
              return (labelName + " " + new Intl.NumberFormat("ja-JP", {
                style: "currency",
                currency: "JPY"
              }).format(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index])) // return data.datasets[tooltipItem.datasetIndex].label + " " + "-" in privacy mode
            }
          }
        }
      }
    }

    return (
      (chartData?.datasets?.length > 0 && chartData?.datasets[0]?.data.length > 0)
        ? <div>
          <div>
            <Pie data={chartData} width={900} height={450} options={chartOptions} />
          </div>
          <h6 className="card-title mt-2">Total {formattedTotal()}</h6>
          <Link className="btn btn-info mt-2 " to={"/app/expenses"}>See Details</Link>
        </div>
        : <div>
          <h5 className="text-info">No expenses logged for the current month yet!</h5>
          <Link className="btn btn-info mt-2 " to={"/app/expenses/creation"}>Create one</Link>
        </div>
    )
  }

  const title = "Expenses"
  return makeWidget(title, hasCompleted, isFailed, forceReload, displayView())
}
