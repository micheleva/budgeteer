import React, { useEffect, useState } from "react"
import { Link } from "react-router-dom"

import "chartjs-plugin-colorschemes"

import type { Category, SavingCategory } from "../../common/interfaces"
import { getBudgets } from "../../common/api_budgets"
import { getCategories } from "../../common/api_categories"
import { getSavingCategories } from "../../common/api_savingcategories"
import { makeWidget } from "./widget_helpers"

/**
 * Format the widget body displaying categories that do not have a Budget for a given period
 *
 * @returns {React.ReactNode} - The formatted body content.
 */
const formatMissingBudgetsBody = (missingCats: Category[], period: string, userHasNoCategories: boolean, detailsButton): React.ReactNode => {
  if (userHasNoCategories) {
    return <div>
      <h5 className="text-info"> You need to create a category first! </h5>
      <Link className="btn btn-info mt-2 " to={"/app/categories/creation"}>Create a Category</Link>
    </div>
  }
  if (missingCats.length === 0) {
    return <h5 className="text-success">All Spending Categories have a budget. You&apos;re all set!<div>{detailsButton}</div></h5>
  }
  return (
    <div className="container">
      <h5 className="text-danger">These <i>Spending</i> Categories do not have a Budget for {period}</h5>
      {missingCats.length > 3 ? <small>Mark the Spending Categories you&apos;re not using anymore as inactive to avoid unnecessay notifications here</small> : null}
      <ul className="list-group">
        {missingCats.map(category => (
          <li key={category.id} className="list-group-item">
            {category.text}
          </li>
        ))}
      </ul>
      <div>{detailsButton}</div>
    </div>
  )
}

/**
 * Widget that displays spending categories that don't have a monthly budget for the current month.
 *
 * This widget fetches budget and category data, then identifies spending categories that are active
 * but don't have a corresponding monthly budget. It handles loading states, error states, and
 * provides guidance when no categories exist.
 *
 * @returns {React.ReactNode} A widget displaying categories missing monthly budgets
 */
export const missingMonthlyBudgetsWidget = (): React.ReactNode => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [missingBudgets, setMissingBudgets] = useState<Category[]>([])
  const [target, setTarget] = useState({ year: new Date().getFullYear(), month: new Date().getMonth() + 1 })
  const [isFailed, setIsFailed] = useState(false)
  const [hasCompleted, setIsCompleted] = useState(false)
  const [userHasNoCategories, setUserHasNoCategories] = useState(false)

  const forceReload = (): void => {
    setTarget({ year: new Date().getFullYear(), month: new Date().getMonth() + 1 })
  }

  useEffect(() => {
    const controller = new AbortController()
    const signal = controller.signal

    const fetchData = async (): Promise<void> => {
      try {
        setIsCompleted(false)
        const bdgts = await getBudgets(target, signal)
        const ctg = await getCategories(signal)

        if (!signal.aborted) {
          setIsCompleted(true)
          if (bdgts?.response?.ok && ctg?.response.ok) {
            const bdgtCategoryIds = bdgts?.data?.results?.map(bdgt => bdgt?.category?.id)
            const missing = ctg?.data?.results?.filter(item => (item.is_archived !== null && ((item?.is_archived) === false)) && !bdgtCategoryIds.includes(item.id))
            setMissingBudgets(missing)
            setUserHasNoCategories(ctg?.data?.results?.length === 0)
          } else {
            setIsFailed(true)
          }
        }
      } catch (error) {
        if (error instanceof Error && error.name !== "AbortError" && !signal.aborted) {
          console.error("Failed to fetch data:", error)
          setIsFailed(true)
        }
      }
    }

    void fetchData()

    return () => {
      controller.abort()
    }
  }, [target])

  const title = "Monthly Spending Budgets"
  const label = (target.month !== null) ? `${target.year}-${String(target.month).padStart(2, "0")}` : `${target.year}`
  const detailsButton = <Link className="btn btn-info mt-2 " to={`/app/budgets/date/${target.year}/${target.month}`}>See Details</Link>
  const content = formatMissingBudgetsBody(missingBudgets, label, userHasNoCategories, detailsButton)
  return makeWidget(title, hasCompleted, isFailed, forceReload, content)
}

/**
 * Format the widget body displaying categories that do not have a Budget for a given period
 *
 * @returns {React.ReactNode} - The formatted body content.
 */
const formatMissingSavingBudgetsBody = (missingCats: SavingCategory[], period: string, userHasNoCategories: boolean, detailsButton): React.ReactNode => {
  if (userHasNoCategories) {
    return <div>
      <h5 className="text-info"> You need to create a Saving Category first! </h5>
      <Link className="btn btn-info mt-2 " to={"/app/saving-categories/creation"}>Create a Saving Category</Link>
    </div>
  }
  if (missingCats.length === 0) {
    return <div>
      <h5 className="text-success">All Saving Categories have a budget. You&apos;re all set! </h5>
      <div>{detailsButton}</div>
    </div>
  }
  return (
    <div className="container">
      {/* NICE TO HAVE: insteaf od 'for {period} we should say for the month of / for the year of */}
      <h5 className="text-danger">These <i>Saving</i> Categories do not have a Budget for {period}</h5>
      {missingCats.length > 3 ? <small>Mark the Saving categories you&apos;re not using anymore as inactive to avoid unnecessay notifications here</small> : null}
      <ul className="list-group">
        {missingCats.map(category => (
          <li key={category.id} className="list-group-item">
            {category.text}
          </li>
        ))}
      </ul>
      <div>{detailsButton}</div>
    </div>
  )
}

/**
 * Widget that displays saving categories that don't have a monthly budget for the current month.
 *
 * This widget fetches budget and saving category data, then identifies saving categories that are active
 * but don't have a corresponding monthly budget. It handles loading states, error states, and
 * provides guidance when no saving categories exist.
 *
 * @returns {React.ReactNode} A widget displaying saving categories missing monthly budgets
 */
export const missingMonthlySavingBudgetsWidget = (): React.ReactNode => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [missingBudgets, setMissingBudgets] = useState<SavingCategory[]>([])
  const [target, setTarget] = useState({ year: new Date().getFullYear(), month: new Date().getMonth() + 1 })
  const [isFailed, setIsFailed] = useState(false)
  const [hasCompleted, setIsCompleted] = useState(false)
  const [userHasNoCategories, setUserHasNoCategories] = useState(false)

  const forceReload = (): void => {
    setTarget({ year: new Date().getFullYear(), month: new Date().getMonth() + 1 })
  }

  useEffect(() => {
    const controller = new AbortController()
    const signal = controller.signal

    const fetchData = async (): Promise<void> => {
      try {
        setIsCompleted(false)
        const bdgts = await getBudgets(target, signal)
        const ctg = await getSavingCategories(signal)

        if (!signal.aborted) {
          setIsCompleted(true)
          if (bdgts?.response?.ok && ctg?.response.ok) {
            const bdgtCategoryIds = bdgts?.data?.results?.map(bdgt => bdgt?.savingcategory?.id)
            const missing = ctg?.data?.results?.filter(item => item.is_archived !== null && (item?.is_archived) === false && !bdgtCategoryIds.includes(item.id))
            setMissingBudgets(missing)
            setUserHasNoCategories(ctg?.data?.results?.length === 0)
          } else {
            setIsFailed(true)
          }
        }
      } catch (error) {
        if (error instanceof Error && error.name !== "AbortError" && !signal.aborted) {
          console.error("Failed to fetch data:", error)
          setIsFailed(true)
        }
      }
    }

    void fetchData()

    return () => {
      controller.abort()
    }
  }, [target])

  const title = "Monthly Saving Budgets"
  const label = (target.month !== null) ? `${target.year}-${String(target.month).padStart(2, "0")}` : `${target.year}`
  const detailsButton = <Link className="btn btn-info mt-2 " to={`/app/budgets/date/${target.year}/${target.month}`}>See Details</Link>
  const content = formatMissingSavingBudgetsBody(missingBudgets, label, userHasNoCategories, detailsButton)
  return makeWidget(title, hasCompleted, isFailed, forceReload, content)
}

/**
 * Widget that displays saving categories that don't have a yearly budget for the current year.
 *
 * This widget fetches budget and saving category data, then identifies saving categories that are active
 * but don't have a corresponding yearly budget. It handles loading states, error states, and
 * provides guidance when no categories exist.
 *
 * @returns {React.ReactNode} A widget displaying categories missing yearly budgets
 */
export const missingYearlySavingBudgetsWidget = (): React.ReactNode => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [missingBudgets, setMissingBudgets] = useState<SavingCategory[]>([])
  const [target, setTarget] = useState({ year: new Date().getFullYear(), month: null })
  const [isFailed, setIsFailed] = useState(false)
  const [hasCompleted, setIsCompleted] = useState(false)
  const [userHasNoCategories, setUserHasNoCategories] = useState(false)

  const forceReload = (): void => {
    setTarget({ year: new Date().getFullYear(), month: null })
  }

  useEffect(() => {
    const controller = new AbortController()
    const signal = controller.signal

    const fetchData = async (): Promise<void> => {
      try {
        setIsCompleted(false)
        const bdgts = await getBudgets(target, signal)
        const ctg = await getSavingCategories(signal)

        if (!signal.aborted) {
          setIsCompleted(true)
          if (bdgts?.response?.ok && ctg?.response.ok) {
            const bdgtCategoryIds = bdgts?.data?.results?.map(bdgt => bdgt?.savingcategory?.id)
            const missing = ctg?.data?.results?.filter(item => item?.is_archived !== null && (item?.is_archived === false) && !bdgtCategoryIds.includes(item.id))
            setMissingBudgets(missing)
            setUserHasNoCategories(ctg?.data?.results?.length === 0)
          } else {
            setIsFailed(true)
          }
        }
      } catch (error) {
        if (error instanceof Error && error.name !== "AbortError" && !signal.aborted) {
          console.error("Failed to fetch data:", error)
          setIsFailed(true)
        }
      }
    }

    void fetchData()

    return () => {
      controller.abort()
    }
  }, [target])

  const title = "Yearly Saving Budgets"
  const detailsButton = <Link className="btn btn-info mt-2 " to={`/app/budgets/date/${target.year}`}>See Details</Link>
  const content = formatMissingSavingBudgetsBody(missingBudgets, String(target?.year), userHasNoCategories, detailsButton)
  return makeWidget(title, hasCompleted, isFailed, forceReload, content)
}

/**
 * Widget that displays spending categories that don't have a yearly budget for the current year.
 *
 * This widget fetches budget and spending category data, then identifies spending categories that are active
 * but don't have a corresponding yearly budget. It handles loading states, error states, and
 * provides guidance when no categories exist.
 *
 * @returns {React.ReactNode} A widget displaying categories missing yearly budgets
 */
export const missingYearlyBudgetsWidget = (): React.ReactNode => {
  // eslint-disable-next-line @typescript-eslint/no-unused-vars
  const [missingBudgets, setMissingBudgets] = useState<Category[]>([])
  const [target, setTarget] = useState({ year: new Date().getFullYear(), month: null })
  const [isFailed, setIsFailed] = useState(false)
  const [hasCompleted, setIsCompleted] = useState(false)
  const [userHasNoCategories, setUserHasNoCategories] = useState(false)

  const forceReload = (): void => {
    setTarget({ year: new Date().getFullYear(), month: null })
  }

  useEffect(() => {
    const controller = new AbortController()
    const signal = controller.signal

    const fetchData = async (): Promise<void> => {
      try {
        setIsCompleted(false)
        const bdgts = await getBudgets(target, signal)
        const ctg = await getCategories(signal)

        if (!signal.aborted) {
          setIsCompleted(true)
          if (bdgts?.response?.ok && ctg?.response.ok) {
            const bdgtCategoryIds = bdgts?.data?.results?.map(bdgt => bdgt?.category?.id)
            const missing = ctg?.data?.results?.filter(item => item?.is_archived !== null && (item?.is_archived === false) && !bdgtCategoryIds.includes(item.id))
            setMissingBudgets(missing)
            setUserHasNoCategories(ctg?.data?.results?.length === 0)
          } else {
            setIsFailed(true)
          }
        }
      } catch (error) {
        if (error instanceof Error && error.name !== "AbortError" && !signal.aborted) {
          console.error("Failed to fetch data:", error)
          setIsFailed(true)
        }
      }
    }

    void fetchData()

    return () => {
      controller.abort()
    }
  }, [target])

  const title = "Yearly Spending Budgets"
  const detailsButton = <Link className="btn btn-info mt-2 " to={`/app/budgets/date/${target.year}`}>See Details</Link>
  const content = formatMissingBudgetsBody(missingBudgets, String(target?.year), userHasNoCategories, detailsButton)
  return makeWidget(title, hasCompleted, isFailed, forceReload, content)
}