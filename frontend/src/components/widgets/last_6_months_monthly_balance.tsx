import React, { useEffect, useState } from "react"
import { Link } from "react-router-dom"

import { getAllMonthlyBalancesAndAssets } from "../../common/api_assets"

import { SimpleBarChart } from "../barchart"
import { makeWidget } from "./widget_helpers"
import type { simpleBarChart } from "../../common/interfaces"
/**
 * Widget that displays the trend of monthly balances over the last six months.
 *
 * This widget fetches monthly balance data and displays it in a chart.
 * It handles loading states, error states, and provides guidance when no data exists.
 *
 * @returns {React.ReactNode} A widget displaying the trend of monthly balances or guidance to create data
 */
export const monthlyBalanceLastSixMonthsWidget = (): React.ReactNode => {
  const [isSending, setIsSending] = useState(false)
  // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
  const [chartData, setChartData] = useState<simpleBarChart>(null)
  const [apiResponseComplete, setApiResponseComplete] = useState(false)
  const [isFailed, setIsFailed] = useState(false)

  const [reloadTrigger, setReloadTrigger] = useState({})

  const forceReload = (): void => {
    setReloadTrigger({})
  }

  useEffect(() => {
    const controller = new AbortController()
    const signal = controller.signal

    const fetchData = async (): Promise<void> => {
      if (isSending || signal.aborted) return

      try {
        setIsSending(true)
        setApiResponseComplete(false)

        const res = await getAllMonthlyBalancesAndAssets(signal)
        if (signal.aborted) return
        setIsSending(false)
        setApiResponseComplete(true)
        if (!res?.response?.ok) {
          // NOTE: If the response state is not ok, but we have data, data contains error messages
          setIsFailed(true)
          setChartData({} as simpleBarChart)
          return
        }
        setChartData({
          monthlyBalances: res.data
        })
      } catch (error) {
        if (error instanceof Error && error.name !== "AbortError" && !signal.aborted) {
          console.error("Failed to fetch data:", error)
          setIsFailed(true)
          setChartData({} as simpleBarChart)
          setApiResponseComplete(true)
          setIsSending(false)
        }
      }
    }

    void fetchData()

    return () => {
      controller.abort()
    }
  }, [reloadTrigger])

  const displayView = (): React.ReactNode => {
    if (chartData === null) {
      return null
    }
    if ((chartData?.monthlyBalances !== undefined && chartData?.monthlyBalances !== null) && chartData?.monthlyBalances?.length === 0) {
      const currYearMonthWihtOne = `${new Date().getFullYear()}-${String(new Date().getMonth() + 1).padStart(2, "0")}-01`
      return <div>
        <h5 className="text-info">You have no monthly balances yet!</h5>
        <Link className="btn btn-info mt-2 " to={`/app/monthly-balances/multiple-creation/date/${currYearMonthWihtOne}`}>Create a Monthly Balance</Link>
      </div>
    } else {
      return <div>
        <div>
          <SimpleBarChart chartData={chartData} />
        </div>
        <Link className="btn btn-info mt-2 " to={"/app/monthly-balances/graph"}>See Details</Link>
      </div>
    }
  }

  const title = "Monthly balances trend"
  return makeWidget(title, apiResponseComplete, isFailed, forceReload, displayView())
}
