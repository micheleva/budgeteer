import React from "react"
import { Link } from "react-router-dom"

import "chartjs-plugin-colorschemes"

import Button from "react-bootstrap/Button"
import Col from "react-bootstrap/Col"
import Spinner from "react-bootstrap/Spinner"

const ReloadButton = ({ onClick }: { onClick: () => void }): JSX.Element => (
  // refresh icon: we use the svg to avoid importing the whole icon libraries only to get once icon!
  // Source https://icons.getbootstrap.com/icons/arrow-clockwise/
  <Button variant="primary" onClick={onClick}>
    <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" className="bi bi-arrow-clockwise" viewBox="0 0 16 16">
      <path fillRule="evenodd" d="M8 3a5 5 0 1 0 4.546 2.914.5.5 0 0 1 .908-.417A6 6 0 1 1 8 2z" />
      <path d="M8 4.466V.534a.25.25 0 0 1 .41-.192l2.36 1.966c.12.1.12.284 0 .384L8.41 4.658A.25.25 0 0 1 8 4.466" />
    </svg>
  </Button>
)

/**
 * Generates a bootstrap card layout widget
 *
 * @param {string} title - The title of the widget.
 * @param {boolean} hasCompleted - Indicates whether the data fetching has completed. While true, a spinner is displayed.
 * @param {boolean} isFailed - Indicates whether the data fetching has failed. If true, a retry button will be displayed.
 * @param {Function} handler - The callback function to handle widget reload on retry button click. Required when isFailed is true.
 * @param {React.ReactNode} content - The content to be displayed in body of the widget.
 * @returns {React.ReactElement} - The formatted widget component.
 */
export const makeWidget = (title: string, hasCompleted: boolean, isFailed: boolean, handler: any, content: any): React.ReactNode => {
  let body = <Spinner animation="border" variant="success" />
  if (hasCompleted) {
    body = content
  }
  if (isFailed) {
    body = <div> The widget has failed loading. Click to reload: <ReloadButton onClick={handler} /></div>
  }
  return (
    <Col key={title} xs="12" md="4" lg="3" className="my-2">
      <div className="card h-100">
        <div className="card-body text-center">
          <h5 className="card-title">{title}</h5>
          <div className="card-text">
            {body}
          </div>
        </div>
      </div>
    </Col>
  )
}

/**
 * Displays guidance for creating data.
 *
 * This function returns a React node that displays a button to create a spending budget.
 *
 * @returns {React.ReactNode} A React node displaying a button to create a spending budget
 */
export const displayCreateDataGuidance = (): React.ReactNode => {
  return <div>
    <div><Link className="btn btn-info" to={"/app/budgets/creation"}>Create a Spending Budget</Link></div>
  </div>
}
