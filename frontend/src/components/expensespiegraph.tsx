import React, { useEffect, useState } from "react"
import { Pie } from "react-chartjs-2"
import "chartjs-plugin-colorschemes"

const ExpensesPieGraph = (props): React.ReactElement => {
  const [chartData, setChartData] = useState({})

  useEffect(() => {
    getChartData()
  }, [props])

  const getChartData = (): void => {
    const { expenses } = props.chartData
    if (expenses === null || expenses === undefined || expenses.length === 0) {
      return
    }
    const finalData = {
      labels: expenses.map(row => row.category_text),
      datasets: [
        {
          label: "Expenses by Category",
          data: expenses.map(row => row.total_amount),
          hoverOffset: 4
        }
      ]
    }
    setChartData(finalData)
  }

  const displayView = (): JSX.Element => {
    const isPrivacyMode = false
    const chartOptions = {
      plugins: {
        title: {
          display: true,
          text: "Expenses by Category"
        }
      },
      legend: {
        position: "top"
      },
      responsive: true,
      maintainAspectRatio: false,
      tooltips: {
        mode: "label", // NOTE: remove this to avoid the mouse-hover if you want to avoid displaying too many items at the same time
        callbacks: {
          label: function (tooltipItem, data) {
            const labelName = data.labels[tooltipItem.index]
            if (tooltipItem.value === "0") return
            if (isPrivacyMode) {
              return labelName
            } else {
              return (labelName + " " + new Intl.NumberFormat("ja-JP", {
                style: "currency",
                currency: "JPY"
              }).format(data.datasets[tooltipItem.datasetIndex].data[tooltipItem.index])) // return data.datasets[tooltipItem.datasetIndex].label + " " + "-" in privacy mode
            }
          }
        }
      }
    }
    return (
      <div>
          { chartData?.datasets?.length > 0 ? <Pie data={chartData} width={1800} height={600} options={chartOptions}/> : null }
      </div>
    )
  }

  return displayView()
}

export default ExpensesPieGraph
