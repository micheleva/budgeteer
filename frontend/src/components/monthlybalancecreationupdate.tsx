import { useForm } from "react-hook-form"
import React, { useEffect, useRef, useState, useCallback } from "react"
import { useParams, useNavigate } from "react-router-dom"
import { createOrUpdateMonthlyBalance, getMonthlyBalances, getMonthlyBalanceCategories } from "../common/api_monthlybalances"
import type { MonthlyBalanceCategory, MonthlyBalanceCreateUpdateForm, MonthlyBalanceWithNestedCategory } from "../common/interfaces"
import { createMonthlyBalanceCategoryGuidance, CustomSpinner, FailedBanner, InlinestyleErrorMessage } from "./shared"
import { getNextYYYYMMdd, getPreviousYYYYMMdd } from "../common/utilities"
import Button from "react-bootstrap/Button"

export const MultipleMonthlyBalanceCreationUpdate = (): JSX.Element => {
  const { period } = useParams()

  const [isSending, setIsSending] = useState(false)
  const [monthlyBalances, setMonthlyBalances] = useState<MonthlyBalanceWithNestedCategory[]>([])
  const [monthlyBalanceCategories, setmonthlyBalanceCategories] = useState<MonthlyBalanceCategory[]>([])

  const formRefs = useRef([]) // Store references to child forms
  const navigate = useNavigate()

  // Target date should be YYYY-m, with month always using 2 digits
  const currMonth = `${new Date().getFullYear()}-${String(new Date().getMonth() + 1).padStart(2, "0")}`
  // NICE TO HAVE: confirm period ends in -01
  const [target, settarget] = useState<string>(`${period ?? currMonth}-01`)

  const [apiResponseComplete, setApiResponseComplete] = useState(false)
  const [criticalError, setCriticalError] = useState(false)
  const [createdBudgets, setCreatedBudgets] = useState<Record<number, number>>({})

  const generatePrevButton = (): JSX.Element | null => {
    // FIXME: do we need this ternary operator? target is already checking for possibly missing period
    const month = getPreviousYYYYMMdd(target)
    if (month === null) {
      return null
    } else {
      // previousMonth is used by functional tests, do not edit it
      return <div className="col-sm-6 col-md-4 col-lg-2 my-1"><Button id="previousMonth" className="col-sm-12" onClick={() => { navigateToMonth(month) }}>{month}</Button></div>
    }
  }

  const generateNextButton = (): JSX.Element | null => {
    // FIXME: do we need this ternary operator? target is already checking for possibly missing period
    const month = getNextYYYYMMdd(target)
    if (month === null) {
      return null
    } else {
      // nextMonth is used by functional tests, do not edit it
      return <div className="col-sm-6 col-md-4 col-lg-2 my-1"><Button id="nextMonth" className="col-sm-12" onClick={() => { navigateToMonth(month) }}>{month}</Button></div>
    }
  }

  const navigateToMonth = (month: string): void => {
    navigate(`/app/monthly-balances/multiple-creation/date/${month}`)
    settarget(month)
    resetComponent()
  }

  const resetComponent = (): void => {
    setApiResponseComplete(false)
    // Remove stale references to old children forms
    while (formRefs.length > 0) {
      formRefs.pop()
    }
    setCreatedBudgets({})
  }

  const handleSubmitAll = (): void => {
    formRefs?.current?.forEach((formRef) => {
      formRef?.current?.click()
    })
  }

  const fetchData = useCallback((signal?: AbortSignal) => {
    if (isSending) return
    setIsSending(true)
    // We reset the component here, as navigating to other months will retain some state (IIUC)
    resetComponent()

    let isCriticalError = false
    Promise.all([getMonthlyBalances(target, signal), getMonthlyBalanceCategories(signal)]).then((values) => {
      const monthlyBalances = values[0]
      const categories = values[1]

      if (!monthlyBalances?.response?.ok) {
        isCriticalError = true
      } else if (monthlyBalances?.response?.ok) {
        setMonthlyBalances(monthlyBalances.data.results)
      } else {
        isCriticalError = true
      }

      if (!categories?.response?.ok) {
        isCriticalError = true
      } else if (categories?.response?.ok) {
        setmonthlyBalanceCategories(categories.data.results)
      } else {
        isCriticalError = true
      }

      setCriticalError(isCriticalError)
    }).catch((err) => {
      setCriticalError(true)
      console.log("Failed to fetch page: ", err)
    }).finally(() => {
      setIsSending(false)
      setApiResponseComplete(true)
    })

    return () => { abortController.abort() }
  }, [target])

  useEffect(() => {
    const abortController = new AbortController()
    const signal = abortController.signal

    fetchData(signal)

    return () => { abortController.abort() }
  }, [fetchData])

  const displayTitle = (): JSX.Element => <h3>Create/Update multiple Monthly Balances for month {target}</h3>

  const displayChildrenForms = (): JSX.Element => {
    let body = CustomSpinner()

    // Inject existing balances data in the category object, so that we avoid nesting loops later on
    const categoryAmountsMap: Record<number, number> = {}
    monthlyBalances.forEach(item => {
      const categoryId = item.category.id
      const amount = item.amount
      categoryAmountsMap[categoryId] = amount
    })

    const getCorrectAmount = (categoryAmountsMap: Record<number, number>, category: number) => {
      // Get CorrectAmount returns its amount if exists or null if it dosen't
      if (Object.keys(categoryAmountsMap).length > 0 && categoryAmountsMap.hasOwnProperty(category)) {
        return categoryAmountsMap[category]
      }

      // This function has access to createdBudgets, no need to pass it as an argument
      if (Object.keys(createdBudgets).length > 0 && createdBudgets.hasOwnProperty(category)) {
        //
        return createdBudgets[category]
      }
      return null
    }

    if (apiResponseComplete && monthlyBalanceCategories.length > 0) {
      body = <div>
      {monthlyBalanceCategories.map((category) => {
        return <MonthlyBudgetInlineForm
          key={category.id}
          category={category}
          month={target}
          amount={getCorrectAmount(categoryAmountsMap, category.id)}
          registerToParent={(childref) => {
            // FIXME (FRONTEND-268): if the registerToParent callback is called after the parent has been umounted
            // we'll get the following error. To fix that, we should check if the parent is still mounted from the child...
            //
            // Warning: Can't perform a React state update on an unmounted component. This is a no-op, but it indicates a memory leak in your application.
            // To fix, cancel all subscriptions and asynchronous tasks in a useEffect cleanup function.
            // MonthlyBudgetInlineForm@webpack://budgeteer-frontend/./src/components/monthlybalancecreation.tsx?:278:18
            if (formRefs.current) {
              // First, clean the stale items with current set to null
              formRefs.current = formRefs.current?.filter(item => item.current !== null)
              // Then, push the new childref into the array
              formRefs.current?.push(childref)
            }
          }}
          // notifyOnDone lets the parent know which MonthlyBudgets have been created, so that it can update children
          // props, to properly display Create or Update in thei buttons
          notifyOnDone={(catData) => {
            setCreatedBudgets((prevState) => { return { ...prevState, [catData.id]: catData.amount } })
          }
          }
        />
      })}
      <button type="submit" className="btn btn-success mb-2" onClick={handleSubmitAll}>Create/Update All</button>
    </div>
    } else if (apiResponseComplete && monthlyBalanceCategories.length === 0) {
      body = createMonthlyBalanceCategoryGuidance()
      // body = <React.Fragment>You do not have any monthlybalance category yet. Create some before you can add any data in here!</React.Fragment>
    }
    return (
      <React.Fragment>
        <div className="row">
          { generatePrevButton() }
          { generateNextButton() }
        </div>
        { <br></br>}
        { body }
      </React.Fragment>
    )
  }

  const displayView = (): JSX.Element => {
    if (criticalError) {
      return FailedBanner()
    }

    return <React.Fragment>
        { displayTitle() }
        { displayChildrenForms() }
    </React.Fragment>
  }

  return displayView()
}

const MonthlyBudgetInlineForm = ({ category, month, amount, registerToParent, notifyOnDone }): JSX.Element => {
  const { handleSubmit, register, control, errors, setValue } = useForm()
  const initialRender = useRef(true)
  const isUpdateForm = amount !== null
  const formRef = React.createRef()
  const [apiCallCompleted, setApiCallCompleted] = useState(false)
  const [completionMessage, setCompletionMessage] = useState("")
  const [apiSuccess, setApiSuccess] = useState(false)

  const onSubmitHandler = async (data) => {
    const formInfo: MonthlyBalanceCreateUpdateForm = { amount: data.amount, date: month, category: category.id }
    await createOrUpdateMonthlyBalance(formInfo).then((res) => {
      setApiCallCompleted(true)
      if (!res?.response?.ok) {
        setApiSuccess(false)

        if (res?.data) {
          for (const [key, value] of Object.entries(res.data)) {
            console.log("Monthly Balance creation/update form error(s):", key, value)
            setCompletionMessage(`${key} ${value}`)
            // Set error for existing fields
            if (key in formInfo) {
              console.log(key, { message: value[0] })
              setCompletionMessage(`${key} ${value}`)
            } else {
              // Other validation errors
              console.log(value[0])
              setCompletionMessage(`${key} ${value}`)
            }
          }
        } else {
          // Handle 40X,50X errors in here
          console.log("Something is wrong with the server, try again later!")
          setCompletionMessage("Something is wrong with the server, try again later!")
        }
      } else {
        setApiSuccess(true)
        notifyOnDone({ id: category?.id, amount: data.amount })
        setCompletionMessage("Success")
        // Toggle the apiCallCompleted message after 2.5 sec
        const timer = setTimeout(() => {
          setApiCallCompleted(false)
        }, 2500)
      }
    })
  }

  useEffect(
    () => {
      let mounted = true
      if (initialRender.current) {
        initialRender.current = false
        if (mounted) {
          setValue("amount", amount || 0)
          setApiCallCompleted(false)
        }
      }
      // We need to update the reference on each render, as the button changes text and class names
      // so IIUC it is destroyed and recreated, making the previous ref.current null
      if (mounted) {
        registerToParent(formRef)
      }
      return () => {
        mounted = false
      }
    }
  )

  // Change the color of the update/create button based on whether this is an update or a creation
  const btnClasses = (isUpdateForm ? "btn-primary" : "btn-info") + " btn mb-2"
  // Adjust the success class based on whether the API did succeed or not
  const resultColor = apiSuccess ? "col-sm-1 col-md-1 col-lg-1 text-success text-left" : "col-sm-1 col-md-1 col-lg-1 text-danger text-left"
  return (
    <form className="form-inline row" onSubmit={handleSubmit(onSubmitHandler)}>
      <div className="col-sm-12 col-md-4 col-lg-3">{category.text}</div>
      <div className="input-group col-sm-5 col-md-4 col-lg-3">
          {/* IMPORTANT: text_field_<category_id> is used by functional tests, do not edit it */}
          <input id={"text_field_" + category.id} className={errors.amount ? "form-control is-invalid" : "form-control"} name="amount" type="number"
           ref={register({
             required: "This field is required.",
             min: {
               value: 0,
               message: "Amount should be equal or more than 0"
             }
           })}
          />
          {errors.amount !== null && errors.amount !== undefined ? InlinestyleErrorMessage(errors.amount.message as string) : null}
      </div>
      {/* HORRIBLE HACK: we need this invisible button so that the parent can post this form: find out if we can improve this */}
      <div className="col-md-1" style={{ display: "none" }}>
        <button ref={formRef} type="submit" style={{ display: "none" }} />
      </div>
      <div className="input-group col-sm-1 col-md-1 col-lg-1">
        <button type="submit" className={btnClasses} >{isUpdateForm ? "Update" : "Create"}</button>
      </div>
      { apiCallCompleted ? <div id={"text_field_" + category.id + "_success"} className={resultColor}>&nbsp;{completionMessage}</div> : null}
    </form>
  )
}

export default MonthlyBudgetInlineForm
