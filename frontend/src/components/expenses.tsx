import React, { useEffect, useState } from "react"
import { ExpenseTable, DismissableRecoverableErrorBanner, CustomSpinner, styleErrorMessage } from "./shared"
import { getExpensesAggregateByCategory, getExpensesByDates, deleteExpense } from "../common/api_expenses"
import type { AggregateExpensesByCategoryResponse, Expense, expensesChart, ExpensesResponse } from "../common/interfaces"
import { getAMonthAgoDate, preventFormSubmit, sortByDateValue, yearMonthIsValidDate } from "../common/utilities"
import ExpensesPieGraph from "./expensespiegraph"

export const ListExpenses = (): JSX.Element => {
  const [isSending, setIsSending] = useState(false)
  const [expenses, setExpenses] = useState<Expense[]>([])
  // eslint-disable-next-line @typescript-eslint/consistent-type-assertions
  const [chartData, setChartData] = useState({} as expensesChart)
  const [start, setstart] = useState<string>(getAMonthAgoDate())
  const [end, setend] = useState<string>(new Date().toLocaleDateString("en-CA"))

  const [apiResponseComplete, setApiResponseComplete] = useState(false)
  const [recoverableError, setRecoverableError] = useState<boolean>(false)

  const [genericFormError, setGenericFormError] = useState("")

  const fetchData = React.useCallback(async () => {
    const abortController = new AbortController()
    if (!yearMonthIsValidDate(start) || !yearMonthIsValidDate(end)) {
      return
    }
    if (isSending) return
    setIsSending(true)

    try {
      const expensesData = await getExpensesByDates(start, end, abortController.signal)
      if (!expensesData?.response?.ok) {
        if (expensesData?.data?.results?.length > 0) {
          // eslint-disable-next-line @typescript-eslint/no-unused-vars
          for (const [_, value] of Object.entries(expensesData.data)) {
            // TODO: test me out (BUGFIX-220) : specify a start date after end date!!!
            // NOTE: end is before start (or start is after end) error case
            setGenericFormError(value[0])
          }
          setRecoverableError(true)
          setExpenses([])
        } else {
          // Handle 40X,50X errors in here
          setRecoverableError(true)
          setGenericFormError("Something is wrong with the server, try again later!")
          setExpenses([])
        }
      } else {
        setGenericFormError("")
        setRecoverableError(false)
        setExpenses(expensesData.data.results.sort(sortByDateValue))
      }

      // Second API call for chart data
      if (!abortController.signal.aborted) {
        const chartResponse = await getExpensesAggregateByCategory(start, end, abortController.signal)
        if (chartResponse?.response?.ok) {
          setChartData({
            expenses: chartResponse.data
          })
        }
      }
    } catch (err) {
      if (err instanceof Error && err.name !== "AbortError") {
        console.log("Failed to fetch page: ", err)
        setRecoverableError(true)
      }
    } finally {
      if (!abortController.signal.aborted) {
        setIsSending(false)
        setApiResponseComplete(true)
      }
    }

    return () => {
      abortController.abort()
    }
  }, [start, end])

  const removeFromLocalCache = (id: number): void => {
    setExpenses((original) => original.filter((exp) => exp.id !== id))
  }

  useEffect(() => {
    const fetchDataPromise = fetchData()

    return () => {
      // Clean up by calling the abort function returned by fetchData
      void fetchDataPromise.then(cleanup => cleanup && cleanup())
    }
  }, [fetchData])

  const displayTitle = (): React.ReactNode =>
        <div className="form-group">
          <div className="col-sm-6">
            <h3>Expenses by date range</h3>
          </div>
        </div>

  const displayView = (): JSX.Element => {
    if (!apiResponseComplete) {
      return CustomSpinner()
    }

    return <React.Fragment>
      {/* FIXME: if you navigate to "edit an expense" page before the SpendingLimitDisplay API calls have completed
          HOW TO FIXME: we should cancel all subscriptions and async task from spending displaylimit
          by returning a function to cleanup after we've unmounted
            useEffect(() => {
              // actions performed when component mounts

              return () => {
                // actions to be performed when component unmounts
              }
            }, []);
      */}
      <div className="row" >
        <div className="col-sm-12 col-sm-12 col-lg-12">
          <form className="form" onSubmit={preventFormSubmit}>
            { displayTitle() }
            <div className="form-group">
              <label className="control-label col-sm-2" htmlFor="start">From&nbsp;</label>
              <div className="col-sm-2">
                <input type="date" id="start" name="start" onChange={e => { setstart(e.target.value) }} value={start}/>
                {/* {{errors.start ? styleErrorMessage(errors.start.message as string) : null}} */}
              </div>
            </div>

            <div className="form-group">
              <label className="control-label col-sm-2" htmlFor="end">To&nbsp;</label>
              <div className="col-sm-2">
                <input type="date" id="end" name="end" onChange={e => { setend(e.target.value) }} value={end}/>
                {/* {{errors.end ? styleErrorMessage(errors.end.message as string) : null}} */}
              </div>
            </div>

            <div className="form-group">
              <div className="col-sm-12">
                <b>{typeof genericFormError === "string" && genericFormError.length === 0 ? styleErrorMessage(genericFormError) : null}</b>
              </div>
            </div>

            <div className="form-group">
              <div className="col-sm-2">
                {/* IMPORTANT: id="triggerDeleteButtonsDisplay" is used in the functional tests: leave it there! */}
                <button className="btn btn-secondary my-2 my-sm-0" id="dateFilterSubmit" onClick={fetchData} disabled={isSending}>{ isSending ? "Fetching..." : "Fetch" }</button>
              </div>
            </div>

          </form>
        </div>
      </div>
      <div className="row">
        <div className="col-sm-12 col-sm-6 col-lg-6 order-1-md order-1-lg order-2">
            <div className="form-group">
              { recoverableError ? DismissableRecoverableErrorBanner(setRecoverableError) : null }
              <ExpenseTable data={expenses}
                deleteFunc={ async (id: number) => await deleteExpense(id)}
                updateParentData={(id: number) => { removeFromLocalCache(id) }}
              />
        </div>
          </div>
        <div className="col-sm-12 col-sm-6 col-lg-6 order-1">
          { Array.isArray(chartData.expenses) && chartData.expenses.length > 0 ? <ExpensesPieGraph chartData={chartData} /> : null}
        </div>
    </div>

    </React.Fragment>
  }

  return displayView()
}
