import React, { useEffect, useRef, useState } from "react"

import Row from "react-bootstrap/Row"

import { getDividends, getDividendsAggregateBySymbol, getDividendsYearlyAggregate } from "../common/api_dividends"
import { getAssets } from "../common/api_assets"
import type { Dividend, SymbolAggregateDividend, YearlyAggregateDividend } from "../common/interfaces"
import { CustomSpinner, displayCreateDataGuidance, FailedBanner, formatDividendAggregateTable, formatDividendsDetails, formatDividendsYearlyStatsTable } from "./shared"
import { makeWidget } from "./widgets/widget_helpers"
import "./../index.css"

export const Dividends = (): JSX.Element => {
  const [dividendsDetails, setDividendsDetails] = useState<Dividend[]>([])
  const [aggregateData, setYearlyaggregateData] = useState<YearlyAggregateDividend[]>([])
  const [dividendsAggregateData, setDividendsAggregateData] = useState<SymbolAggregateDividend[]>([])
  const initialRender = useRef(true)

  const [apiResponseComplete, setApiResponseComplete] = useState(false)
  const [criticalError, setCriticalError] = useState(false)

  useEffect(() => {
    const abortController = new AbortController()
    const signal = abortController.signal

    if (initialRender.current) {
      initialRender.current = false
      setApiResponseComplete(false)

      Promise.all([
        getDividends(null, null, signal),
        getAssets(null, signal),
        getDividendsYearlyAggregate(signal),
        getDividendsAggregateBySymbol(signal)
      ]).then((values) => {
        let isCriticalError = false

        const dividends = values[0]
        const responseAssets = values[1]
        const yearlyAggregate = values[2]
        const dividendsAggregate = values[3]

        if (!dividends?.response?.ok) {
          isCriticalError = true
        }

        if (!responseAssets?.response?.ok) {
          isCriticalError = true
        }

        if (!yearlyAggregate?.response?.ok) {
          isCriticalError = true
        }

        if (!dividendsAggregate?.response?.ok) {
          isCriticalError = true
        }

        setCriticalError(isCriticalError)
        if (!isCriticalError) {
          setDividendsDetails(dividends.data.results)
          setYearlyaggregateData(yearlyAggregate.data.results)
          setDividendsAggregateData(dividendsAggregate.data.results)
        }
      }).catch((err) => {
        if (err.name !== "AbortError") {
          setCriticalError(true)
          console.log("Failed to fetch page: ", err)
        }
      }).finally(() => {
        if (!abortController.signal.aborted) {
          setApiResponseComplete(true)
        }
      })
    }
    return () => {
      abortController.abort()
    }
  }, [])

  const displayTitle = (): React.ReactNode => <h3>List Dividends</h3>

  const displayDividendWidgets = (): React.ReactNode => {
    if (dividendsDetails.length === 0) {
      return displayCreateDataGuidance()
    }

    return <Row>
            { dividendsYearlyStatsWidget() }
            { dividendsAggregateWidget() }
            { dividendsDetailsWidget() }
          </Row>
  }

  const dividendsYearlyStatsWidget = (): React.ReactNode => {
    const title = "Dividends yearly stat"
    const content = formatDividendsYearlyStatsTable(aggregateData)
    // TODO: make this widget totally indipendend to the parent view!
    // that way it will be easier to fetch single api failed calls
    return makeWidget(title, true, false, () => {}, content)
  }

  const dividendsAggregateWidget = (): React.ReactNode => {
    const title = "Aggregate by symbol"
    const content = formatDividendAggregateTable(dividendsAggregateData)
    // TODO: make this widget totally indipendend to the parent view!
    // that way it will be easier to fetch single api failed calls
    return makeWidget(title, true, false, () => {}, content)
  }

  const dividendsDetailsWidget = (): React.ReactNode => {
    const title = "Dividends details"
    const content = formatDividendsDetails(dividendsDetails)
    // TODO: make this widget totally indipendend to the parent view!
    // that way it will be easier to fetch single api failed calls
    return makeWidget(title, true, false, () => {}, content)
  }

  const displayView = (): JSX.Element => {
    if (!apiResponseComplete) {
      return CustomSpinner()
    }

    if (criticalError) {
      return FailedBanner()
    }

    return (
      <div>
        <div>
          {displayTitle()}
          { displayDividendWidgets() }
        </div>
      </div>
    )
  }

  return displayView()
}
