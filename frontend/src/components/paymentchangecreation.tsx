import { useForm } from "react-hook-form"
import { Link } from "react-router-dom"
import Alert from "react-bootstrap/Alert"
import React, { useEffect, useRef, useState, useCallback } from "react"
import { createPaymentChange } from "../common/api_paymentchanges"
import { getVariableObligations } from "../common/api_obligations"
import type { ObligationPaymentChangeResponse, Obligation, PaymentChangeFormInput } from "../common/interfaces"
import { FailedBanner, CustomSpinner, styleErrorMessage } from "./shared"

const defaultValues: PaymentChangeFormInput = {
  obligation: 0,
  new_monthly_payment: 0,
  change_date: new Date().toLocaleDateString("en-CA")
}

export const PaymentChangeCreation = (): JSX.Element => {
  // eslint-disable-next-line @typescript-eslint/unbound-method
  const { register, handleSubmit, setError, formState: { errors }, reset } = useForm<PaymentChangeFormInput>()
  const [isSending, setIsSending] = useState(false)
  const [categories, setCategories] = useState<Obligation[]>([])
  const initialRender = useRef(true)
  const [showDismissable, setShowDismissable] = useState(false)
  const [isUpdateSuccess, setIsUpdateSuccess] = useState(false)
  const autoCloseDismissibleAfterMs = 1500

  const [apiResponseComplete, setApiResponseComplete] = useState(false)
  const [criticalError, setCriticalError] = useState(false)

  const [genericFormError, setGenericFormError] = useState("")

  const onSubmit = useCallback((formValues: PaymentChangeFormInput) => {
    if (isSending) return
    setShowDismissable(false)
    setIsSending(true)
    const abortController = new AbortController()
    const signal = abortController.signal

    createPaymentChange(formValues, signal)
      .then((res: ObligationPaymentChangeResponse) => {
        if (!res?.response?.ok) {
          if (res?.data !== null && res?.data !== undefined) {
            for (const [key, value] of Object.entries(res.data)) {
              console.log("PaymentChange creation form error(s):", key, value)
              // Set error for existing fields
              if (key in formValues) {
                setError(key as keyof PaymentChangeFormInput, { message: value[0] })
              } else {
                // PaymentChangeFormInput has a obligation and new_monthly_payment unique constraints
                if (value[0] === "A PaymentChangeFormInput with the same obligation and new_monthly_payment exists for this user.") {
                  setError("text" as keyof PaymentChangeFormInput, { message: value[0] })
                } else {
                // Other validation errors
                  setGenericFormError(value[0])
                }
              }
            }
          } else {
            // Handle 40X,50X errors in here
            setGenericFormError("Something is wrong with the server, try again later!")
          }
          setIsUpdateSuccess(false)
        } else {
          reset(defaultValues)
          setGenericFormError("")
          setIsUpdateSuccess(true)
        }
      })
      .catch(error => {
        if (error instanceof Error && error.name !== "AbortError") {
          console.log("Income creation error ", error)
          setIsUpdateSuccess(false)
        }
      })
      .finally(() => {
        if (!signal.aborted) {
          setIsSending(false)
          setShowDismissable(true)
          const timer = setTimeout(() => {
            setShowDismissable(false)
          }, autoCloseDismissibleAfterMs)
          return () => { clearTimeout(timer) }
        }
      })

    return () => { abortController.abort() }
  }, [reset])

  useEffect(() => {
    const abortController = new AbortController()
    const signal = abortController.signal

    if (initialRender.current) {
      initialRender.current = false
      setIsSending(true)
      getVariableObligations(signal).then((res) => {
        if (!res?.response?.ok) {
          setCriticalError(true)
        } else if (res?.response?.ok && Array.isArray(res?.data?.results) && res?.data?.results?.length > 0) {
          setCriticalError(false)
          setCategories(res?.data.results)
        } else {
          setCriticalError(true)
        }
      }).catch((err) => {
        setCriticalError(true)
        console.log("Failed to fetch page: ", err)
      }).finally(() => {
        setIsSending(false)
        setApiResponseComplete(true)
      })
    }

    return () => { abortController.abort() }
  })

  const displayTitle = (): JSX.Element => {
    return <h3>Create a Payment Change </h3>
  }

  const createObligationCategoryGuidance = (): JSX.Element => {
    return <div>
      <h5 className="text-info"> You need to create an Obligation first! </h5>
      <Link className="btn btn-info mt-2 " to={"/app/obligations/creation"}>Create an Obligation</Link>
    </div>
  }

  const displayCreationForm = (): JSX.Element => {
    if (categories?.length === 0) {
      return createObligationCategoryGuidance()
    }
    return (
      <form onSubmit={handleSubmit(onSubmit)} className="form">
        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="text_field">Obligation:&nbsp;</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <select id="obligation_field" name="obligation" ref={register({ required: "This field is required." })}>
              {categories.map(value => (<option key={value.id} value={value.id}>
                  {value.name}
                </option>))}
            </select>
            {(errors?.obligation?.message !== undefined && errors?.obligation?.message !== null) ? styleErrorMessage(errors.obligation.message) : null}
          </div>
        </div>

        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="new_monthly_payment_field">New monthly payment</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input className="form-control" name="new_monthly_payment" type="number" id="new_monthly_payment_field"
               ref={register({
                 required: "This field is required.",
                 min: {
                   value: 1,
                   message:
                        "new_monthly_payment should be more than 0"
                 }
               })}
            />
            {(errors?.new_monthly_payment?.message !== null && errors?.new_monthly_payment?.message !== undefined) ? styleErrorMessage(errors.new_monthly_payment.message) : null}
          </div>
        </div>

        <div className="form-group">
          <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="change_date_entity">Change Date&nbsp;</label>
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            <input name="change_date" type="date" id="change_date_entity" defaultValue={new Date().toLocaleDateString("en-CA")} ref={register({ required: true })} />
            {(errors?.change_date?.message !== null && errors?.change_date?.message !== undefined) ? styleErrorMessage(errors.change_date.message) : null}
          </div>
        </div>

        <div className="form-group">
          <div className="col-sm-6 col-md-4 3 col-lg-3">
            {genericFormError !== "" ? styleErrorMessage(genericFormError) : null}
            <button className="btn btn-secondary my-2 my-sm-0" disabled={isSending}>{ isSending ? "Submitting..." : "Submit" }</button>
          </div>
        </div>

      </form>
    )
  }

  const renderDismissable = (): JSX.Element | null => {
    if (showDismissable) {
      return <Alert variant={isUpdateSuccess ? "success" : "danger"} onClose={() => { setShowDismissable(false) }} dismissible>
          <Alert.Heading>{isUpdateSuccess ? "PaymentChange created!" : "PaymentChange creation failed :("}</Alert.Heading>
        </Alert>
    } else {
      return null
    }
  }

  const displayView = (): JSX.Element => {
    if (!apiResponseComplete) {
      return CustomSpinner()
    }

    if (criticalError) {
      return FailedBanner()
    }

    return <React.Fragment>
      { displayTitle() }
      { renderDismissable() }
      { displayCreationForm() }
    </React.Fragment>
  }

  return displayView()
}
