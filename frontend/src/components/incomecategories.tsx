import React, { Fragment, useEffect, useRef, useState } from "react"
import { getIncomeCategories } from "../common/api_incomecategories"
import type { IncomeCategory, IncomeCategoriesResponse } from "../common/interfaces"
import { CategoryTable, createIncomeCategoryGuidance, CustomSpinner, FailedBanner } from "./shared"

export const IncomeCategories = (): JSX.Element => {
  const [categories, setCategories] = useState<IncomeCategory[]>([])
  const initialRender = useRef(true)
  const [invalidResponse, setInvalidResponse] = useState(false)
  const [apiResponseComplete, setApiResponseComplete] = useState(false)

  useEffect(() => {
    const abortController = new AbortController()
    const signal = abortController.signal

    if (initialRender.current) {
      initialRender.current = false
      getIncomeCategories(signal).then((data: IncomeCategoriesResponse) => {
        if (data?.response?.ok && data?.data?.results !== undefined && Array.isArray(data?.data?.results)) {
          setInvalidResponse(false)
          const responseCategories = data.data.results
          setCategories(responseCategories)
        } else {
          setInvalidResponse(true)
        }
      })
        .catch((e: Error) => {
          console.log(e)
          setInvalidResponse(true)
        })
        .finally(() => {
          setApiResponseComplete(true)
        })
    }

    return () => { abortController.abort() }
  }, [])

  const displayTitle = (): JSX.Element => <h3>List Income Categories</h3>

  const displayGoalsBody = (): JSX.Element => {
    if (!apiResponseComplete) {
      return CustomSpinner()
    }

    if (invalidResponse) {
      return FailedBanner()
    }

    if (apiResponseComplete && categories.length === 0) {
      return <Fragment>{displayTitle()}{createIncomeCategoryGuidance()}</Fragment>
    }

    const list = categories.length > 0
      ? <CategoryTable categories={categories} url="income-categories" />
      : null

    return <div>{displayTitle()}{list}</div>
  }

  return displayGoalsBody()
}
