import React from "react"
import { Alert } from "react-bootstrap"

export const FormatGuidance = ({ isVisible }: { isVisible: boolean }): JSX.Element | null => {
  if (!isVisible) return null
  return (
    <div className="row">
        <Alert className="col-12" variant="info">
          <strong>Record your voice, in English, in the following format:</strong>
          <br />
          <b>&lt;Category&gt; &lt;Amount (without currency)&gt; date &lt;date&gt; note &lt;note&gt;</b>.
          <br />
          <i>e.g. pronounce {"\"Groceries fifty date Septmber twentyfifth note weekly groceries\""}</i>
        </Alert>
    </div>
  )
}
