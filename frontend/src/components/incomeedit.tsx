import React, { useEffect, useCallback, useRef, useState } from "react"
import Alert from "react-bootstrap/Alert"
import { useForm } from "react-hook-form"
import { useParams } from "react-router-dom"
import { FailedBanner, CustomSpinner, styleErrorMessage } from "./shared"
import { getIncome, patchIncome } from "../common/api_incomes"
import { getIncomeCategories } from "../common/api_incomecategories"
import type { IncomeCategory, Income, PatchIncomeFormInputs } from "../common/interfaces"

export const IncomeEdit = (): JSX.Element => {
  // eslint-disable-next-line @typescript-eslint/unbound-method
  const { register, handleSubmit, setError, formState: { errors }, reset, setValue } = useForm<PatchIncomeFormInputs>()
  const [isSending, setIsSending] = useState(false)
  const { id } = useParams()
  const [incomeData, setIncomeData] = useState<Income>()
  const [categories, setCategories] = useState<IncomeCategory[]>([])
  const initialRender = useRef(true)
  const [showDismissable, setShowDismissable] = useState(false)
  const [isUpdateSuccess, setIsUpdateSuccess] = useState(false)
  const autoCloseDismissibleAfterMs = 1500
  // The three statuses below are used to manage the error handling
  const [criticalError, setCriticalError] = useState(false)
  const [invalidExpense, setInvalidExpense] = useState(false)
  const [genericFormError, setGenericFormError] = useState("")

  const onSubmit = useCallback(async (formValues: PatchIncomeFormInputs) => {
    if (isSending) return
    setShowDismissable(false)
    setIsSending(true)
    const abortController = new AbortController()
    const signal = abortController.signal

    await patchIncome(Number(id), formValues, signal)
      .then((res) => {
        if (!res.response.ok) {
          if (res?.data !== null && res?.data !== undefined) {
            for (const [key, value] of Object.entries(res.data)) {
              console.log("Income edit form error(s):", key, value)
              // Set error for existing fields
              if (key in formValues) {
                setError(key as keyof PatchIncomeFormInputs, { message: value[0] })
              } else {
                // Other validation errors
                setGenericFormError(value[0])
              }
            }
          } else {
            // Handle 40X,50X errors in here
            setGenericFormError("Something is wrong with the server, try again later!")
          }
          setIsUpdateSuccess(false)
        } else {
          setGenericFormError("")
          setIsUpdateSuccess(true)
        }
      })
      .finally(() => {
        setIsSending(false)
        setShowDismissable(true)
        const timer = setTimeout(() => {
          setShowDismissable(false)
        }, autoCloseDismissibleAfterMs)
        return () => { clearTimeout(timer) }
      })

    return () => { abortController.abort() }
  }, [reset])

  const fetchData = useCallback(() => {
    if (isSending) return
    setIsSending(true)
    const abortController = new AbortController()
    const signal = abortController.signal

    Promise.all([getIncomeCategories(signal), getIncome(Number(id), signal)]).then((values) => {
      const categoriesRes = values[0]
      const incomeRes = values[1]
      let isCriticalError = false

      if (!categoriesRes?.response?.ok) {
        isCriticalError = true
      } else if (categoriesRes?.response?.ok) {
        setCategories(categoriesRes.data.results)
      } else {
        isCriticalError = true
      }

      if (!incomeRes?.response?.ok) {
        isCriticalError = true
        setInvalidExpense(false)
      } else {
        setInvalidExpense(false)
        setIncomeData(incomeRes.data)
        setValue("amount", incomeRes.data.amount)
        setValue("date", incomeRes.data.date)
        setValue("category", incomeRes.data.category)
        setValue("note", incomeRes.data.note)
      }

      setCriticalError(isCriticalError)
    }).catch((e) => {
      setCriticalError(true)
    })
      .finally(() => {
        setIsSending(false)
      })

    return () => { abortController.abort() }
  }, [id])

  useEffect(() => {
    if (initialRender.current) {
      initialRender.current = false
      fetchData()
    }
  })

  const missingOrUnavailableHeader = (): JSX.Element => {
    const msg = "Sorry, it seems the income you're trying to editing does not exist...or there was an error. Please try again later, or reload the page"
    // NOTE: This "invalid-income-marker" id is used by the functional tests, do not remove it!
    return <Alert id="invalid-income-marker" variant="danger">
              <Alert.Heading>{msg}</Alert.Heading>
      </Alert>
  }

  const displayTitle = (): React.ReactNode => <h3>Update an income </h3>

  const displayBackButton = (): React.ReactNode => <a href="/app/incomes" className="btn btn-secondary my-2 my-sm-0">Back</a>

  const displayCategoriesPullDown = (): React.ReactNode => {
    if (categories.length > 0) {
      return <select id="category_field" name="category" ref={register({ required: "This field is required." })}>
              {categories.map(value => (
                <option key={value.id} value={value.id}>
                  {value.text}
                </option>)
              )}
             </select>
    } else {
      return CustomSpinner()
    }
  }

  const displayView = (): JSX.Element => {
    // IMPORTANT: We need to call setValue() AFTER render (with the form) has run
    // of the form will render undefined (i.e. "") in each field
    // as per https://stackoverflow.com/a/59547360

    // This means we can not use a flag like apiResponseComplete as we do in goaledittab.tsx
    // FIXME: BUGFIX-245
    // Possible workardound: make the form 'hidden' using css, and show it after the loading is complete
    // TODO test that out and see if the setValue() binding works when the form is hidden

    if (invalidExpense) {
      return missingOrUnavailableHeader()
    } else if (criticalError) {
      return FailedBanner()
    } else if (incomeData != null) {
      return (
        <React.Fragment>
              {displayTitle()}
              {displayBackButton()}
              {showDismissable
                ? <Alert variant={isUpdateSuccess ? "success" : "danger"} onClose={() => { setShowDismissable(false) }} dismissible>
                  <Alert.Heading>{isUpdateSuccess ? "Income updated!" : "An error has accourred. Please try again."}</Alert.Heading>
                </Alert>
                : null}
                <form onSubmit={handleSubmit(onSubmit)} className="form">
                  <div className="form-group">
                    <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="text_field">Income Category:&nbsp;</label>
                    <div className="col-sm-3">
                      { displayCategoriesPullDown() }
                      {(errors.category !== null && errors.category !== undefined) ? styleErrorMessage(errors.category.message as string) : null}
                    </div>
                  </div>

                  <div className="form-group">
                    <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="amount_field">Amount</label>
                    <div className="col-sm-6 col-md-4 3 col-lg-3">
                      <input className="form-control" name="amount" type="number" id="amount_field"
                         ref={register({
                           required: "This field is required.",
                           min: {
                             value: 1,
                             message:
                                  "Amount should be more than 0"
                           }
                         })}
                      />
                      {(errors.amount != null) ? styleErrorMessage(errors.amount.message as string) : null}
                    </div>
                  </div>

                <div className="form-group">
                  <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="date_entity">Date&nbsp;</label>
                  <div className="col-sm-6 col-md-4 3 col-lg-3">
                    <input name="date" type="date" id="date_entity" defaultValue={new Date().toLocaleDateString("en-CA")} ref={register({ required: true })} />
                    {(errors.date != null) ? styleErrorMessage(errors.date.message as string) : null}
                  </div>
                </div>

                <div className="form-group">
                  <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="note_field">Note&nbsp;</label>
                  <div className="col-sm-6 col-md-4 3 col-lg-3">
                    <input name="note" id="note_field"
                      ref={register({
                        maxLength: {
                          value: 150,
                          message: "Max 150 chars"
                        }
                      })}
                    />
                    {(errors.note != null) ? styleErrorMessage(errors.note.message as string) : null}
                  </div>
                </div>

                <div className="form-group">
                  <div className="col-sm-6 col-md-4 3 col-lg-3">
                    <div>{(genericFormError.length > 0) ? styleErrorMessage(genericFormError) : null}</div>
                  </div>
                </div>

                <div className="form-group">
                  <div className="col-sm-6 col-md-4 3 col-lg-3">
                    <button className="btn btn-secondary my-2 my-sm-0" disabled={isSending}>{ isSending ? "Submitting..." : "Submit" }</button>
                  </div>
                </div>

              </form>
        </React.Fragment>)
    } else {
      return CustomSpinner()
    }
  }

  return displayView()
}
