import React, { useEffect, useState, useCallback } from "react"
import { useParams, useNavigate, Link } from "react-router-dom"

import Alert from "react-bootstrap/Alert"
import { getBudgets } from "../common/api_budgets"
import { getExpensesByCategoryId } from "../common/api_expenses"
import { getCategories } from "../common/api_categories"
import { getSavingCategories } from "../common/api_savingcategories"
import type { BudgetWithCategory, Category, SavingCategory, YearMonth } from "../common/interfaces"
import { BudgetsTable, FailedBanner, CustomSpinner } from "./shared"
import { getFollowingYearMonth, getPreviousYearMonth } from "../common/utilities"
import Button from "react-bootstrap/Button"

const Budgets = (): JSX.Element => {
  const [isSending, setIsSending] = useState(false)
  const [budgets, setBudgets] = useState<BudgetWithCategory[]>([])
  const [categories, setCategories] = useState<Category[]>([])
  const [savingCategories, setSavingCategories] = useState<SavingCategory[]>([])
  const [expensesData, setExpensesData] = useState<{ [key: number]: number }>({})
  const { year, month } = useParams()

  const computeTarget = (year: number, month: number | undefined): YearMonth => {
    const currentMonth = new Date().getMonth() + 1
    if (year === undefined) {
      return {
        year: new Date().getFullYear(),
        month: currentMonth
      }
    }

    const yearNum: number = parseInt(year, 10)
    if (month === undefined || month === null) {
      return {
        year,
        month: null
      }
    }

    // TODO validate year and month: the user might have put garbage in the URL
    const monthNum: number = parseInt(month, 10)
    return {
      year: yearNum,
      month: monthNum
    }
  }

  const [target, setTarget] = useState<YearMonth>(computeTarget(year, month))
  const [apiResponseComplete, setApiResponseComplete] = useState(false)
  const [criticalError, setCriticalError] = useState(false)

  const navigate = useNavigate()

  const generatePrevButton = (): JSX.Element | null => {
    const { year, month } = getPreviousYearMonth(target)
    if (year === undefined && month === undefined) {
      return null
    } else {
      const label = (month !== null) ? `${year}-${String(month).padStart(2, "0")}` : `${year}`
      return <Button className="mr-2" onClick={() => { navigateToYearMonth(year, month) }}> {label} </Button>
    }
  }

  const generateNextButton = (): JSX.Element | null => {
    const { year, month } = getFollowingYearMonth(target)
    if (year === undefined && month === undefined) {
      return null
    } else {
      const label = (month !== null) ? `${year}-${String(month).padStart(2, "0")}` : `${year}`
      return <Button className="mr-2" onClick={() => { navigateToYearMonth(year, month) }}> {label} </Button>
    }
  }

  const navigateToYearMonth = (year: number, month: number | null): null => {
    if (month === null) {
      navigate(`/app/budgets/date/${year}`)
      setTarget({ year, month })
    } else {
      navigate(`/app/budgets/date/${year}/${month}`)
      setTarget({ year, month })
    }
  }

  const resetComponentState = (): void => {
    // Reset the component state in case we get back to this view through a link, and the view gets reused
    setCategories([])
    setSavingCategories([])
    setExpensesData({})
    setBudgets([])
  }

  const fetchData = useCallback(async (signal?: AbortSignal) => {
    if (isSending) return
    setIsSending(true)
    resetComponentState()
    setApiResponseComplete(false)

    let isCriticalError = false
    await Promise.all([getBudgets(target, signal), getCategories(signal), getSavingCategories(signal)]).then((values) => {
      const budgets = values[0]
      const categories = values[1]
      const savingCategories = values[2]

      if (budgets?.response?.ok) {
        setBudgets(budgets.data.results)
      } else {
        isCriticalError = true
      }

      if (categories?.response?.ok) {
        setCategories(categories.data.results)
      } else {
        isCriticalError = true
      }

      if (savingCategories?.response?.ok) {
        setSavingCategories(savingCategories.data.results)
      } else {
        isCriticalError = true
      }

      setCriticalError(isCriticalError)
    }).catch((err) => {
      setCriticalError(true)
      console.log("Failed to fetch page: ", err)
    }).finally(() => {
      setIsSending(false)
      setApiResponseComplete(true)
    })
  }, [target])

  useEffect(() => {
    const abortController = new AbortController()
    const signal = abortController.signal

    void fetchData(signal)

    return () => { abortController.abort() }
  }, [fetchData])

  useEffect(() => {
    const fetchExpenses = async (): Promise<undefined> => {
      if (budgets.length === 0) return

      let expensesDict = {}

      try {
        for (const budget of budgets) {
          const categoryId = budget?.category?.id

          // Return early if this is a Saving Budget
          if (categoryId === undefined) {
            continue
          }

          let startDate
          let endDate

          if (target.month === null) {
            startDate = `${target.year}-01-01`
            endDate = `${target.year}-12-31`
          } else {
            // Get the last day of the month
            const lastDay = new Date(target.year, target.month, 0).getDate()
            startDate = `${target.year}-${String(target.month).padStart(2, "0")}-1`
            endDate = `${target.year}-${String(target.month).padStart(2, "0")}-${lastDay}`
          }

          const expensesResponse = await getExpensesByCategoryId(
            categoryId,
            // Add your start and end date logic here based on your requirements
            startDate,
            endDate
          )

          if (expensesResponse?.response?.ok) {
            // Summing up the expenses for the category
            const totalExpenses = expensesResponse.data.results.reduce(
              (sum, expense) => sum + Number(expense.amount),
              0
            )

            expensesDict = { ...expensesDict, [categoryId]: totalExpenses }
          }
        }
      } catch (err) {
        setCriticalError(true)
        console.log("Failed to fetch expenses: ", err)
      } finally {
        setIsSending(false)
        setApiResponseComplete(true)
        setExpensesData(expensesDict)
      }
    }

    void fetchExpenses()
  }, [budgets, target])

  const displayTitle = (): JSX.Element => {
    return <h3>Budgets for {target?.year}{target?.month !== null ? `-${target.month}` : null}</h3>
  }

  const renderWIPAlert = (): JSX.Element => {
    return <Alert variant={"warning"}>
      <Alert.Heading>Work in progress</Alert.Heading>
      <div className="text-dark">{"After the release of v1.6, users will have the ability to mass update/create budgets in the same manner as Monthly Balances. Bear with me while I work on it :D"}</div>
    </Alert>
  }

  const displayBudgetsGuidance = (): JSX.Element => {
    return <div>
      <h5 className="text-info mt-2">You do not have any Spending Budget for this month</h5>
      <div><Link className="btn btn-info" to={"/app/budgets/creation"}>Create a Spending Budget</Link></div>
    </div>
  }

  const displayTableandButtons = (): JSX.Element => {
    let body = CustomSpinner()

    if (apiResponseComplete && (categories?.length > 0 || savingCategories.length > 0)) {
      body = <BudgetsTable data={budgets} expensesData={expensesData} />
    } else if (apiResponseComplete && budgets.length > 0) {
      body = displayBudgetsGuidance()
    }
    return (
      <React.Fragment>
        {renderWIPAlert()}
        {generatePrevButton()}
        {generateNextButton()}
        {<br></br>}
        {body}
      </React.Fragment>
    )
  }

  const displayView = (): JSX.Element => {
    if (criticalError) {
      return FailedBanner()
    }

    return <React.Fragment>
      {/* NICE TO HAVE: Add a date type input field, and allow navigation from there, this field should have autofocus
            Now, since we only need YYYY-MM, we cant' just use type='date' as the browser will show the days of the month as well */}
      {displayTitle()}
      {displayTableandButtons()}
    </React.Fragment>
  }

  return displayView()
}

export default Budgets
