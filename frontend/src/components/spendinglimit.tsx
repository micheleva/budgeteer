import React, { useEffect, useRef, useState } from "react"
import { Link } from "react-router-dom"

import { LimitTable } from "./shared"
import { makeWidget } from "./widgets/widget_helpers"
import { getExpensesByCategoryId } from "../common/api_expenses"
import { getSpendingLimits } from "../common/api_spendinglimits"
import type { Expense, ExpensesResponse, SpendingLimit } from "../common/interfaces"

export const SpendingLimitDisplay = (): React.ReactNode => {
  const endDate = (new Date()).toISOString().split("T")[0]

  const [spended, setSpended] = useState<SpendingLimit[]>([])
  const [dummy, setDummy] = useState({})

  const [apiResponseComplete, setApiResponseComplete] = useState(false)
  const [criticalError, setCriticalError] = useState(false)
  const initialRender = useRef(true)

  const fetchLimits = React.useCallback(async (signal?: AbortSignal) => {
    if (initialRender.current) {
      initialRender.current = false
    }

    const limitRes = await getSpendingLimits(signal)
    if (!limitRes?.response?.ok) {
      setCriticalError(true)
      return
    }

    const promises: Array<Promise<ExpensesResponse>> = []
    // eslint-disable-next-line array-callback-return
    limitRes.data.results.map((limit: SpendingLimit): void => {
      const startDate = new Date()
      startDate.setDate(startDate.getDate() - limit.days)

      // Format the date in yyyy-mm-dd
      const start = startDate.toISOString().split("T")[0]
      promises.push(getExpensesByCategoryId(limit?.category, start, endDate, limit?.text, signal))
    })

    // const spendedPromiseWrapper = new Promise((resolve) => {
    Promise.allSettled(promises).then((expensesPromise) => {
      const tot = expensesPromise.map((expRes: PromiseSettledResult<ExpensesResponse>): number => {
        if (expRes.status === "fulfilled") {
          return expRes.value.data.results.reduce((acc: number, val: Expense) => {
            return acc + Number(val.amount)
          }, 0)
        } else {
          if (signal !== undefined && !signal?.aborted) {
            setCriticalError(true)
          }
          return 0
        }
      })

      const spendedReduced = []
      for (let i = tot.length - 1; i >= 0; i--) {
        const spent = { spent: tot[i] }
        const objToApply = Object.assign({}, limitRes.data.results[i])
        spendedReduced.push(Object.assign(objToApply, spent))
      }
      setSpended(spendedReduced)
    }).catch((err) => {
      if (err instanceof Error && err.name !== "AbortError") {
        console.log("Failed to fetch page: ", err)
      }
    }).finally(() => {
      if (signal !== undefined && !signal?.aborted) {
        setApiResponseComplete(true)
      }
    })
  }, [dummy])

  const displayCreateDataGuidance = (): React.ReactNode => {
    return <div>
      <h5 className="text-info">No Spending Limits created yet!</h5>
      <Link className="btn btn-info mt-2 " to={"/app/spending-limits/creation"}>Create a Spending Limit</Link>
    </div>
  }

  useEffect(() => {
    const abortController = new AbortController()
    const signal = abortController.signal

    const fetchData = async (): Promise<void> => {
      await fetchLimits(signal)
    }
    fetchData().catch((err) => {
      if (err instanceof Error && err.name !== "AbortError") {
        console.error("Failed to fetch data: ", err)
      }
    })

    return () => { abortController.abort() }
  }, [fetchLimits])

  const forceReload = (): void => {
    setDummy({})
  }

  const displayView = (): React.ReactNode => {
    const title = "Spending Limits"
    const content = <LimitTable data={spended} missingDatGuidance={displayCreateDataGuidance} />
    return makeWidget(title, apiResponseComplete, criticalError, forceReload, content)
  }

  return displayView()
}
