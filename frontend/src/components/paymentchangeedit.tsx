import React, { useEffect, useCallback, useRef, useState } from "react"
import Alert from "react-bootstrap/Alert"
import { useForm } from "react-hook-form"
import { useParams } from "react-router-dom"
import { FailedBanner, CustomSpinner, styleErrorMessage } from "./shared"

import { getPaymentChange, putPaymentChange } from "../common/api_paymentchanges"
import { getVariableObligations } from "../common/api_obligations"
import type { Obligation, ObligationPaymentChange, PaymentChangeFormInput } from "../common/interfaces"

export const PaymentChangeEdit = (): JSX.Element => {
  // eslint-disable-next-line @typescript-eslint/unbound-method
  const { register, handleSubmit, setError, formState: { errors }, setValue } = useForm<PaymentChangeFormInput>()
  const [isSending, setIsSending] = useState(false)
  const { id } = useParams()
  const [expenseData, setExpenseData] = useState<ObligationPaymentChange>()
  const [categories, setCategories] = useState<Obligation[]>([])
  const initialRender = useRef(true)
  const [showDismissable, setShowDismissable] = useState(false)
  const [isUpdateSuccess, setIsUpdateSuccess] = useState(false)
  const autoCloseDismissibleAfterMs = 1500
  // The three statuses below are used to manage the error handling
  const [criticalError, setCriticalError] = useState(false)
  const [invalidExpense, setInvalidExpense] = useState(false)

  const [genericFormError, setGenericFormError] = useState("")

  const onSubmit = useCallback(async (formValues: PaymentChangeFormInput) => {
    if (isSending) return
    setShowDismissable(false)
    setIsSending(true)
    const abortController = new AbortController()
    const signal = abortController.signal

    await putPaymentChange(Number(id), formValues, signal)
      .then((res) => {
        if (!res?.response?.ok) {
          if (res?.data != null && res?.data !== undefined) {
            for (const [key, value] of Object.entries(res.data)) {
              console.log("PaymentChange edit form error(s):", key, value)
              // Set error for existing fields
              if (key in formValues) {
                setError(key as keyof PaymentChangeFormInput, { message: value[0] })
              } else {
                // Other validation errors
                //  <========== todo handle 404 and 50X errors
                setGenericFormError(value[0])
              }
            }
          } else {
            // Handle 40X,50X errors in here
            setGenericFormError("Something is wrong with the server, try again later!")
          }
          setIsUpdateSuccess(false)
        } else {
          setGenericFormError("")
          setIsUpdateSuccess(true)
        }
      })
      .catch(error => {
        if (error instanceof Error && error.name !== "AbortError") {
          console.log("PaymentChange edit error ", error)
          setIsUpdateSuccess(false)
        }
      })
      .finally(() => {
        if (!signal.aborted) {
          setIsSending(false)
          setShowDismissable(true)
          const timer = setTimeout(() => {
            setShowDismissable(false)
          }, autoCloseDismissibleAfterMs)
          return () => { clearTimeout(timer) }
        }
      })
  }, [id])

  const fetchData = useCallback((signal?: AbortSignal) => {
    if (isSending) return
    setIsSending(true)

    if (id !== undefined && id !== null) {
      Promise.all([getVariableObligations(signal), getPaymentChange(Number(id), signal)]).then((values) => {
        const categoriesRes = values[0]
        const expRes = values[1]
        let isCriticalError = false

        if (categoriesRes?.response?.ok && Array.isArray(categoriesRes.data.results)) {
          setCategories(categoriesRes.data.results)
        } else {
          isCriticalError = true
        }

        if (expRes?.response?.ok) {
          setInvalidExpense(false)
          setExpenseData(expRes.data)
          setValue("obligation", expRes.data.obligation)
          setValue("change_date", expRes.data.change_date)
          setValue("new_monthly_payment", expRes.data.new_monthly_payment)
        } else {
          setCriticalError(true)
          // FIXME: we end up here for errors like 403, and 404, so we should
          // set to screen a more descriptive error, and not a generic one
          setInvalidExpense(true)
        }
        setCriticalError(isCriticalError)
      }).catch((e) => {
        console.log(e)
        setCriticalError(true)
      }).finally(() => {
        setIsSending(false)
      })
    } else {
      setInvalidExpense(true)
    }
  }, [id])

  useEffect(() => {
    const abortController = new AbortController()
    const signal = abortController.signal

    if (initialRender.current) {
      initialRender.current = false
      fetchData(signal)
    }

    return () => { abortController.abort() }
  }, [fetchData])

  const missingOrUnavailableHeader = (): JSX.Element => {
    const msg = "Sorry, it seems the PaymentChange you're trying to editing does not exist...or there was an error. Please try again later, or reload the page"
    // NOTE: This "invalid-payment-change-marker" id is used by the functional tests, do not remove it!
    return <Alert id="invalid-payment-change-marker" variant="danger">
              <Alert.Heading>{msg}</Alert.Heading>
      </Alert>
  }

  const displayTitle = (): JSX.Element => <div><h3>Update a PaymentChange </h3></div>

  const displayView = (): JSX.Element => {
    // IMPORTANT: We need to call setValue() AFTER render (with the form) has run
    // of the form will render undefined (i.e. "") in each field
    // as per https://stackoverflow.com/a/59547360

    // This means we can not use a flag like apiResponseComplete
    // as we do in goaledittab.tsx

    // FIXME: BUGFIX-245
    // TRY ME OUT <========
    // try to make the form 'hidden' using css, and see if the setValue() binding works

    if (invalidExpense) {
      return missingOrUnavailableHeader()
    } else if (criticalError) {
      return FailedBanner()
    } else if (expenseData != null) {
      return (
        <React.Fragment>
              {displayTitle()}
              {showDismissable
                ? <Alert variant={isUpdateSuccess ? "success" : "danger"} onClose={() => { setShowDismissable(false) }} dismissible>
                  <Alert.Heading>{isUpdateSuccess ? "PaymentChange updated!" : "An error has accourred. Please try again."}</Alert.Heading>
                </Alert>
                : null}
                <form onSubmit={handleSubmit(onSubmit)} className="form">
                  <div className="form-group">
                    <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="text_field">Obligation:&nbsp;</label>
                    <div className="col-sm-6 col-md-4 3 col-lg-3">
                      <select id="obligation_field" name="obligation" ref={register({ required: "This field is required." })}>
                        {categories.map(value => (<option key={value.id} value={value.id}>
                            {value.name}
                          </option>))}
                      </select>
                      {(errors?.obligation?.message !== undefined && errors?.obligation?.message !== null) ? styleErrorMessage(errors.obligation.message) : null}
                    </div>
                  </div>

                  <div className="form-group">
                    <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="new_monthly_payment_field">New monthly payment</label>
                    <div className="col-sm-6 col-md-4 3 col-lg-3">
                      <input className="form-control" name="new_monthly_payment" type="number" id="new_monthly_payment_field"
                        ref={register({
                          required: "This field is required.",
                          min: {
                            value: 1,
                            message:
                                  "new_monthly_payment should be more than 0"
                          }
                        })}
                      />
                      {(errors?.new_monthly_payment?.message !== null && errors?.new_monthly_payment?.message !== undefined) ? styleErrorMessage(errors.new_monthly_payment.message) : null}
                    </div>
                  </div>

                  <div className="form-group">
                    <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="change_date_entity">Change Date&nbsp;</label>
                    <div className="col-sm-6 col-md-4 3 col-lg-3">
                      <input name="change_date" type="date" id="change_date_entity" defaultValue={new Date().toLocaleDateString("en-CA")} ref={register({ required: true })} />
                      {(errors?.change_date?.message !== null && errors?.change_date?.message !== undefined) ? styleErrorMessage(errors.change_date.message) : null}
                    </div>
                  </div>

                  <div className="form-group">
                    <div className="col-sm-6 col-md-4 3 col-lg-3">
                      {genericFormError !== "" ? styleErrorMessage(genericFormError) : null}
                    </div>
                  </div>

                  <div className="form-group">
                    <div className="col-sm-6 col-md-4 3 col-lg-3">
                      <button className="btn btn-secondary my-2 my-sm-0" disabled={isSending}>{ isSending ? "Submitting..." : "Submit" }</button>
                    </div>
                  </div>

                </form>
        </React.Fragment>)
    } else {
      return CustomSpinner()
    }
  }

  return displayView()
}
