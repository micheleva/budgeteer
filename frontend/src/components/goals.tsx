import React, { Fragment, useEffect, useRef, useState } from "react"
import { Link } from "react-router-dom"
import { getGoals, deleteGoal } from "../common/api_goals"
import type { Goal, GoalsResponse } from "../common/interfaces"
import { FailedBanner, GoalTable, CustomSpinner } from "./shared"

export const Goals = (): JSX.Element => {
  const [goals, setGoals] = useState<Goal[]>([])
  const initialRender = useRef(true)
  const [invalidResponse, setInvalidResponse] = useState(false)
  const [apiResponseComplete, setApiResponseComplete] = useState(false)

  const removeFromLocalCache = (id: number): void => {
    setGoals((original) => original.filter((goal) => goal.id !== id))
  }

  useEffect(() => {
    const abortController = new AbortController()
    const signal = abortController.signal

    if (initialRender.current) {
      initialRender.current = false
      setApiResponseComplete(false)
      getGoals(signal).then((data: GoalsResponse) => {
        if (data?.response?.ok && Array.isArray(data?.data?.results)) {
          setInvalidResponse(false)
          const responseGoals = data.data.results
          setGoals(responseGoals)
        } else {
          setInvalidResponse(true)
        }
      })
        .catch((e: Error) => {
          console.log(e)
          setInvalidResponse(true)
        })
        .finally(() => {
          setApiResponseComplete(true)
        })
    }

    return () => { abortController.abort() }
  }, [])

  const createGoalsGuidance = (): JSX.Element => {
    return <div>
      <h3>List Goals</h3>
      <h5 className="text-info">Your goals will appear here after you create any!</h5>
      <Link className="btn btn-info mt-2 " to={"/app/goals/creation"}>Create a Goal</Link>
    </div>
  }

  const displayGoalsBody = (): JSX.Element => {
    if (!apiResponseComplete) {
      return CustomSpinner()
    }

    if (invalidResponse) {
      return FailedBanner()
    }

    if (apiResponseComplete && goals.length === 0) {
      return createGoalsGuidance()
    }

    return <Fragment>
        <h3>List Goals</h3>
        <GoalTable
          data={goals}
          deleteFunc={async (id: number) => await deleteGoal(id)}
          updateParentData={(id: number) => { removeFromLocalCache(id) }}
          />
      </Fragment>
  }

  return displayGoalsBody()
}
