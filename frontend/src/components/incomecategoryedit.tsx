import { useForm } from "react-hook-form"
import Alert from "react-bootstrap/Alert"
import { useParams, Link } from "react-router-dom"
import React, { useEffect, useRef, useState, useCallback } from "react"
import { getIncomeCategory, patchIncomeCategory } from "../common/api_incomecategories"
import type { IncomeCategory, IncomeCategoryFormInputs } from "../common/interfaces"
import { CustomSpinner, styleErrorMessage } from "./shared"

export const IncomeCategoryEdit = (): JSX.Element => {
  // eslint-disable-next-line @typescript-eslint/unbound-method
  const { register, handleSubmit, setError, errors, setValue } = useForm<IncomeCategoryFormInputs>()
  const [isSending, setIsSending] = useState(false)
  const { id } = useParams()
  const [category, setCategory] = useState<IncomeCategory>()
  const initialRender = useRef(true)
  const [showDismissable, setShowDismissable] = useState(false)
  const [isUpdateSuccess, setIsUpdateSuccess] = useState(false)
  const autoCloseDismissibleAfterMs = 1500
  const [invalidCategory, setInvalidCategory] = useState(false)

  const [genericFormError, setGenericFormError] = useState("")

  const fetchData = useCallback((displayModal = true) => {
    if (isSending) return
    setIsSending(true)
    setIsUpdateSuccess(false)

    const abortController = new AbortController()
    const signal = abortController.signal

    getIncomeCategory(Number(id), signal).then((g) => {
      if (g?.response.ok && g?.data !== undefined && g?.data !== null && typeof g?.data === "object") {
        setInvalidCategory(false)
        setIsUpdateSuccess(false)
        const CategoryResponse = g.data
        setCategory(CategoryResponse)
        // IMPORTANT: We need to call setValue() AFTER render (with the form) has run
        // of the form will render undefined (i.e. "") in each field
        // as per https://stackoverflow.com/a/59547360
        // This means we can not use a flag like apiResponseComplete
        // as we do in goalstab.tsx
        setValue("text", CategoryResponse.text)
      } else {
        setInvalidCategory(true)
        setIsUpdateSuccess(false)
      }
    }).catch((err) => {
      setInvalidCategory(true)
      setIsUpdateSuccess(false)
      console.log("Failed to fetch page: ", err)
    }).finally(() => {
      setIsSending(false)
      if (displayModal === true) {
        setShowDismissable(true)
      }
    })

    return () => { abortController.abort() }
  }, [id])

  useEffect(() => {
    if (initialRender.current) {
      initialRender.current = false
      fetchData(false)
    }
  }, [fetchData])

  const onSubmit = useCallback((formValues: IncomeCategoryFormInputs) => {
    setIsSending(true)
    const abortController = new AbortController()
    const signal = abortController.signal
    if (category?.id !== undefined) {
      patchIncomeCategory(category.id, formValues, signal).then((res) => {
        if (!(res?.response.ok)) {
          if (res?.data != null) {
            for (const [key, value] of Object.entries(res.data)) {
              // Set error for existing fields
              if (key in formValues) {
                setError(key as keyof IncomeCategoryFormInputs, { message: value[0] })
              } else {
                // Other validation errors
                setGenericFormError(value[0])
              }
            }
          } else {
            // Handle 40X,50X errors in here
            setGenericFormError("Something is wrong with the server, try again later!")
          }
          setIsUpdateSuccess(false)
        } else {
          setGenericFormError("")
          setIsUpdateSuccess(true)
        }
      }).catch((err) => {
        console.log(err)
        setIsUpdateSuccess(false)
      }).finally(() => {
        setIsSending(false)
        setShowDismissable(true)
        const timer = setTimeout(() => {
          setShowDismissable(false)
        }, autoCloseDismissibleAfterMs)
        return () => { clearTimeout(timer) }
      })
    } else {
      setInvalidCategory(true)
      setIsSending(false)
    }

    return () => { abortController.abort() }
  }, [category])

  const displayTitle = (category: IncomeCategory): JSX.Element => {
    return <h3>Editing Income Category ID: {category.id}</h3>
  }

  const missingOrUnavailableAlert = (): JSX.Element => {
    // NOTE: This "invalid-category-marker" id is used by the functional tests, do not remove it!
    return <Alert id="invalid-category-marker" variant="danger">
              <Alert.Heading>{"Sorry, it seems the income category you're trying to editing does not exist...or there was an error. Please try again later, or reload the page"}</Alert.Heading>
      </Alert>
  }

  const displayForm = (): JSX.Element => {
    return (
      <form onSubmit={handleSubmit(onSubmit)} className="form">
          <div className="form-group">
            <label className="control-label col-sm-6 col-md-4 3 col-lg-3" htmlFor="text_field">Category Name</label>
            <div className="col-sm-6 col-md-4 3 col-lg-3">
              <input className="form-control" name="text" type="text" id="text_field"
                 ref={register({
                   required: "This field is required.",
                   maxLength: {
                     value: 20,
                     message: "Max 20 chars"
                   }
                 })}
              />
              {(errors.text !== null && errors.text !== undefined) ? styleErrorMessage(errors.text.message as string) : null}
            </div>
          </div>

        <div className="form-group">
            <div className="col-sm-6 col-md-4 3 col-lg-3">
              {genericFormError !== "" ? styleErrorMessage(genericFormError) : null}
              <button className="btn btn-secondary my-2 my-sm-0" disabled={isSending}>{ isSending ? "Submitting..." : "Submit" }</button>
            </div>
        </div>

      </form>
    )
  }
  const displayView = (): JSX.Element => {
    // IMPORTANT: We need to call setValue() AFTER render (with the form) has run
    // of the form will render undefined (i.e. "") in each field
    // as per https://stackoverflow.com/a/59547360

    // This means we can not use a flag like apiResponseComplete as we do in goaledittab.tsx
    // FIXME: BUGFIX-245

    // Possible workardound: make the form 'hidden' using css, and show it after the loading is complete
    // TODO test that out and see if the setValue() binding works when the form is hidden
    if (invalidCategory) {
      return missingOrUnavailableAlert()
    } else if (category !== null && category !== undefined) {
      return <React.Fragment>
        <div>
          {showDismissable
            ? <Alert variant={isUpdateSuccess ? "success" : "danger"} onClose={() => { setShowDismissable(false) }} dismissible>
              <Alert.Heading>{isUpdateSuccess ? "Income Category updated!" : "Income Category failed :("}</Alert.Heading>
            </Alert>
            : null}
          <Link to={"/app/income-categories"}>Back to Income Categories</Link>
          <div>{displayTitle(category)}</div>
        </div>

        { displayForm() }
      </React.Fragment>
    } else {
      return CustomSpinner()
    }
  }

  return displayView()
}
