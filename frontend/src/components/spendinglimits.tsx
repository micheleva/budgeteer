import React, { Fragment, useEffect, useRef, useState } from "react"

import { deleteSpendingLimit, getSpendingLimits } from "../common/api_spendinglimits"
import type { SpendingLimit, SpendingLimitsResponse } from "../common/interfaces"
import { createSpendingLimitGuidance, CustomSpinner, FailedBanner, LimitExpenseTable } from "./shared"

export const SpendingLimits = (): JSX.Element => {
  const [spendingLimits, setSpendingLimits] = useState<SpendingLimit[]>([])
  const initialRender = useRef(true)
  const [invalidResponse, setInvalidResponse] = useState(false)
  const [apiResponseComplete, setApiResponseComplete] = useState(false)

  useEffect(() => {
    const abortController = new AbortController()
    const signal = abortController.signal

    if (initialRender.current) {
      initialRender.current = false
      getSpendingLimits(signal).then((data: SpendingLimitsResponse) => {
        if (!data?.response?.ok) {
          setInvalidResponse(true)
        } else {
          setInvalidResponse(false)
          const limitRes = data.data.results
          setSpendingLimits(limitRes)
        }
      })
        .catch((e: Error) => {
          console.log(e)
          setInvalidResponse(true)
        })
        .finally(() => {
          if (signal !== undefined && !signal?.aborted) {
            setApiResponseComplete(true)
          }
        })
    }

    return () => { abortController.abort() }
  })

  const displayTitle = (): JSX.Element => <div className="col-sm-12 mx-4"><h3>List SpendingLimits</h3></div>

  const removeFromLocalCache = (id: number): void => {
    // TODO: this might throw an error if the delete call is slow and the
    // abort signal is triggered. We should use a component wide signal
    // and check if it is aborted before updating the local cache
    setSpendingLimits((original) => original.filter((exp) => exp.id !== id))
  }

  const displayBody = (): JSX.Element => {
    if (!apiResponseComplete) {
      return CustomSpinner()
    }

    if (invalidResponse) {
      return FailedBanner()
    }

    if (apiResponseComplete && spendingLimits.length === 0) {
      return <Fragment>{displayTitle()}{createSpendingLimitGuidance()}</Fragment>
    }

    const list = spendingLimits.length > 0
      ? <LimitExpenseTable data={spendingLimits}
        deleteFunc={ async (id: number) => { return await deleteSpendingLimit(id) }}
        updateParentData={(id: number) => { removeFromLocalCache(id) }}
        url="category" />
      : null

    return <React.Fragment>
    {displayTitle()}
    <ul>{list}</ul>
    </React.Fragment>
  }

  return displayBody()
}
