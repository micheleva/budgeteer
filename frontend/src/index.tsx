import React, { useState } from "react"
import PropTypes from "prop-types"
import { render } from "react-dom"
import {
  BrowserRouter as Router,
  Route,
  Routes,
  NavLink
} from "react-router-dom"
import {
  Navbar,
  Nav,
  NavDropdown
} from "react-bootstrap"
import useOnclickOutside from "react-cool-onclickoutside"

import Accounts from "./components/accounts"
import { Assets } from "./components/assets"
import { AudioRecorder } from "./components/audio"
import { AutomaticAssets } from "./components/automaticassets"
import { AutomaticDividends } from "./components/automaticdividends"
import Budgets from "./components/budgets"
import BudgetCreation from "./components/budgetcreation"
import BudgetEdit from "./components/budgetedit"
import SavingBudgetEdit from "./components/savingbudgetedit"
import { CreateAccount } from "./components/accountcreation"
import { CreateCategory } from "./components/categorycreation"
import MonthlyBalanceCategoryCreation from "./components/monthlybalancecategorycreation"
import { Categories } from "./components/categories"
import { CategoryEdit } from "./components/categoryedit"
import { CreateSavingCategory } from "./components/savingcategorycreation"
import Dashboard from "./components/dashboard"
import { Dividends } from "./components/dividends"
import { ExpensebyCategory } from "./components/expensesbycategory"
import { ExpenseCreation } from "./components/expensecreation"
import { ExpenseEdit } from "./components/expenseedit"
import ExpenseIncomeGraph from "./components/expenseincomebargraph"
import { Goals } from "./components/goals"
import { GoalCreation } from "./components/goalcreation"
import { GoalEdit } from "./components/goaledit"
import { IncomeCategories } from "./components/incomecategories"
import { IncomeCategoryCreation } from "./components/incomecategorycreation"
import { IncomeCategoryEdit } from "./components/incomecategoryedit"
import { IncomeCreation } from "./components/incomecreation"
import { IncomeEdit } from "./components/incomeedit"
import { ListIncomes } from "./components/incomes"
import { ListExpenses } from "./components/expenses"
import { Logout } from "./components/redirects/logout"
import { MonthlyBalancesGraph } from "./components/monthlybalancesgraph"
import { MonthlyBalanceCategories } from "./components/monthlybalancecategories"
import { MultipleMonthlyBalanceCreationUpdate } from "./components/monthlybalancecreationupdate"
import { MonthlyBalanceCategoryEdit } from "./components/monthlybalancecategoryedit"
import { ObligationEdit } from "./components/obligationedit"
import Obligations from "./components/obligations"
import { ObligationCreation } from "./components/obligationcreation"
import { Page404 } from "./components/page404"
import { PaymentChangeCreation } from "./components/paymentchangecreation"
import { PaymentChangeEdit } from "./components/paymentchangeedit"
import { SavingBudgetCreation } from "./components/savingbudgetcreation"
import { SavingCategories } from "./components/savingcategories"
import { SavingCategoryEdit } from "./components/savingcategoryedit"
import { SpendingLimits } from "./components/spendinglimits"
import { SpendingLimitCreation } from "./components/spendinglimitcreation"
import { SpendingLimitEdit } from "./components/spendinglimitedit"
import ExternalRedirectPage from "./components/redirects/external_redirect"
import { sha } from "./common/commit-hash"
import Settings from "./components/settings"

// NOTE:
// Since mobile needs to handle the click/tap event (desktop relies on hover), the logic got quite complicated.
// We need to resort to an external library  (react-cool-onclickoutside" in order to catch click outside the
// menu, as navdropdown, contrary to normal dropdown[1], does not implement the 'autoClose' API. So we have to handle it ourselves.
// Unfortunately, the much more (mantained and) popular "react-onclickoutside" [2] library, does not work well with hooks yet[3].
// Hence we rely on less maintained library[2].

// [1] https://react-bootstrap.github.io/components/dropdowns/
// [2] https://www.npmjs.com/package/react-onclickoutside
// [3] https://www.npmjs.com/package/react-onclickoutside#functional-component-with-usestate-hook
const HoverNavDropDown = ({ children, updateParentOpen, ...props }: { children: React.ReactNode, updateParentOpen: (id: string) => void, [key: string]: any }): JSX.Element => {
  // TODO: re-dive again in the 'multiple ref' section of react-cool-onclickoutside library[1],
  // and see if we can remove this complicated workaround for mobile users
  // [1] https://github.com/wellyshen/react-cool-onclickoutside#usage
  const ref = useOnclickOutside(() => {
    updateParentOpen("")
  }, {
    // NOTE: we need to explictly ignore clicks on the other siblings,
    // othwewise the click (event) on the siblings would close trigger this
    // function, and prevent the siblings from receinv the click (event)
  // i.e. clicking on <HoverNavDropDown-1> opens the <HoverNavDropDown-1> Menu, and the clicking on
  // <HoverNavDropDown-2> simply closes <HoverNavDropDown-1 Menu. Requiring the user to click again
  // on <HoverNavDropDown-2> to have the <HoverNavDropDown-2> Menu opening.log
  // The above described behaviour feels odd and buddy, hence we resort to a very convoluted
  // implementation to work around the limitation of the 'react-cool-onclickoutside' library
    ignoreClass: "ignore-onclickoutside"
  })

  return (
      <NavDropdown {...props}
        // eslint-disable-next-line react/prop-types
        show={props.open === props.id}
        id={props.id}
        title={props.title}
        ref={ref}
        // eslint-disable-next-line react/prop-types
        onClick={() => {updateParentOpen(props.id)}}
        // eslint-disable-next-line react/prop-types
        onMouseEnter={() => {updateParentOpen(props.id)}}
        onMouseLeave={() => {updateParentOpen("")}}
        className="ignore-onclickoutside">
         {children}
      </NavDropdown>
  )
}
HoverNavDropDown.propTypes = {
  children: PropTypes.node.isRequired,
  updateParentOpen: PropTypes.func.isRequired,
  id: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
  open: PropTypes.string.isRequired
}

const Links = (): JSX.Element => {
  const currMonth = new Date().getMonth() + 1
  const currYear = new Date().getFullYear()
  const dateYYMM = `${new Date().getFullYear()}-${String(new Date().getMonth() + 1).padStart(2, "0")}`
  // NOTE: this handles the id of the currently open children
  const [whoIsOpen, setWhoisOpen] = useState("")

  // NOTE: This function is used to communicate between siblings HoverNavDropDown
  const parentClickOutside = (id: string): void => {
    // NOTE: if we have the same id as before, it means the user has clicked
    // on the same menu to close it. Here is where we handle it
    setWhoisOpen((prevState: string): string => {
      if (prevState === id) {
        return ""
      } else {
        return id
      }
    })
  }

  return (
    <div className="container">
    <Navbar variant="dark" expand="sm" className="justify-content-center">
      <Nav >
        <Navbar.Brand href="/">Home</Navbar.Brand>
        <HoverNavDropDown title="Expenses" id="basic-nav-dropdown-exp" updateParentOpen={parentClickOutside} open={whoIsOpen}>
          <NavDropdown.Item href="/app/categories/creation">
            Create Category
          </NavDropdown.Item>
          <NavDropdown.Item href="/app/categories">
            List Categories
          </NavDropdown.Item>
          <NavDropdown.Divider />
          <NavDropdown.Item href="/app/expenses/creation">
            Create Expense
          </NavDropdown.Item>
          <NavDropdown.Item as={NavLink} to="/app/expenses/audio">
            Create expense (Audio)
          </NavDropdown.Item>
          <NavDropdown.Item href="/app/expenses">
            List Expenses
          </NavDropdown.Item>
          <NavDropdown.Item href="/app/expenses/by-category">
            List Expenses by Category
          </NavDropdown.Item>
          <NavDropdown.Item href="/app/expenses/graphs/expvsinc">
            Expenses vs Income Graph
          </NavDropdown.Item>
        </HoverNavDropDown>

        <HoverNavDropDown title="Assets" id="basic-nav-dropdown-assets" updateParentOpen={parentClickOutside} open={whoIsOpen}>
          <NavDropdown.Item href="/app/accounts/creation">
            Create Account
          </NavDropdown.Item>
          <NavDropdown.Item href="/app/accounts">
            List Acccounts
          </NavDropdown.Item>
          <NavDropdown.Divider />
          <NavDropdown.Item href="/app/assets">
            List Invested Assets
          </NavDropdown.Item>
          <NavDropdown.Item href="/app/assets/import">
            Import Invested Assets
          </NavDropdown.Item>
          <NavDropdown.Divider />
          <NavDropdown.Item href="/app/monthly-balances/categories/creation">
            Create Monthly Balances Category
          </NavDropdown.Item>
          <NavDropdown.Item href="/app/monthly-balances/categories">
            List Monthly Balances Categories
          </NavDropdown.Item>
          <NavDropdown.Divider />
          <NavDropdown.Item href={`/app/monthly-balances/multiple-creation/date/${dateYYMM}`}>
            Create/Update Monthly Balances
          </NavDropdown.Item>
          <NavDropdown.Item href="/app/monthly-balances/graph">
            Monthly Balances Graph
          </NavDropdown.Item>
        </HoverNavDropDown>

        <HoverNavDropDown title="SpendingLimits" id="basic-nav-dropdown-spending-limits" updateParentOpen={parentClickOutside} open={whoIsOpen}>
          <NavDropdown.Item href="/app/spending-limits/creation">
            Create Spending Limit
          </NavDropdown.Item>
          <NavDropdown.Item href="/app/spending-limits">
            List Spending Limits
          </NavDropdown.Item>
        </HoverNavDropDown>

        <HoverNavDropDown title="Income" id="basic-nav-dropdown-income" updateParentOpen={parentClickOutside} open={whoIsOpen}>
          <NavDropdown.Item href="/app/income-categories/creation">
            Create Income Categories
          </NavDropdown.Item>
          <NavDropdown.Item href="/app/income-categories">
            List Income Categories
          </NavDropdown.Item>
          <NavDropdown.Divider />
          <NavDropdown.Item href="/app/incomes/creation">
            Create Income
          </NavDropdown.Item>
          <NavDropdown.Item href="/app/incomes">
            List Incomes
          </NavDropdown.Item>
          <NavDropdown.Divider />
          <NavDropdown.Item href="/app/dividends">
            List Dividends
          </NavDropdown.Item>
          <NavDropdown.Item href="/app/dividends/import">
            Import Dividends
          </NavDropdown.Item>
        </HoverNavDropDown>

        <HoverNavDropDown title="Budgets" id="basic-nav-dropdown-budgets" updateParentOpen={parentClickOutside} open={whoIsOpen}>
          <NavDropdown.Item href="/app/goals/creation">
            Create Goal
          </NavDropdown.Item>
          <NavDropdown.Item href="/app/goals">
            List Goals
          </NavDropdown.Item>
          <NavDropdown.Divider />
          <NavDropdown.Item href="/app/saving-categories/creation">
            Create Saving Category
          </NavDropdown.Item>
          <NavDropdown.Item href="/app/saving-categories">
            List Saving Categories
          </NavDropdown.Item>
          <NavDropdown.Divider />
          <NavDropdown.Item href="/app/budgets/creation">
            Create Spending Budget
          </NavDropdown.Item>
          <NavDropdown.Item href="/app/budgets/saving/creation">
            Create Saving Budget
          </NavDropdown.Item>
          
        <NavDropdown.Item href={`/app/budgets/date/${currYear}/${currMonth}`}>
            List All Budgets (Monthly)
          </NavDropdown.Item>
        <NavDropdown.Item href={`/app/budgets/date/${currYear}`}>
            List All Budgets (Yearly)
          </NavDropdown.Item>
    {/*  <NavDropdown.Item href="/app/budgets/mutliple-creation">
                Create/Update Multiple Budgets
              </NavDropdown.Item> */}
        </HoverNavDropDown>

        <HoverNavDropDown title="Debts" id="basic-nav-dropdown-debts" updateParentOpen={parentClickOutside} open={whoIsOpen}>
          <NavDropdown.Item href="/app/obligations/creation">
            Create Debt
          </NavDropdown.Item>
          <NavDropdown.Item href="/app/obligations">
            List Debts
          </NavDropdown.Item>
          <NavDropdown.Item href="/app/obligations/payment-change/creation">
            Update Debt Monthly Payment
          </NavDropdown.Item>
        </HoverNavDropDown>

        <Nav.Link as={NavLink} to="/app/settings">
          Settings
        </Nav.Link>
        <Nav.Link as={NavLink} to="/app/docs">
          Docs
        </Nav.Link>
        <Nav.Link as={NavLink} to="/app/logout">
          Logout
        </Nav.Link>
        {/* Use style to prevent hover effects on the hash */}
        <Nav.Link as="span" style={{ cursor: "default", pointerEvents: "none" }}>
          {/* NOTE: "git_commit_hash" is used in the functional tests. DO NOT REMOVE */}
          <small id="git_commit_hash">Version: {sha}</small>
        </Nav.Link>
      </Nav>
    </Navbar>
    </div>
  )
}

const MyRoutes = (): JSX.Element => {
  return (
          <Routes>
            <Route path="/" element={<Dashboard />} />
            <Route path="app" element={<Dashboard />} />
            <Route path="app/categories" element={<Categories />} />
            <Route path="app/categories/creation" element={<CreateCategory />} />
            <Route path="app/categories/:id" element={<CategoryEdit />} />

            <Route path="app/expenses" element={<ListExpenses />} />
            <Route path="app/expenses/creation" element={<ExpenseCreation />} />
            <Route path="app/expenses/audio" element={<AudioRecorder />} />
            <Route path="app/expenses/:id" element={<ExpenseEdit />} />
            <Route path="app/expenses/by-category" element={<ExpensebyCategory />} />
            <Route path="app/expenses/graphs/expvsinc" element={<ExpenseIncomeGraph />} />

            <Route path="app/monthly-balances/categories" element={<MonthlyBalanceCategories />} />
            <Route path="app/monthly-balances/categories/creation" element={<MonthlyBalanceCategoryCreation />} />
            <Route path="app/monthly-balances/categories/:id" element={<MonthlyBalanceCategoryEdit />} />
            <Route path="app/monthly-balances/graph" element={<MonthlyBalancesGraph />} />
            <Route path="app/monthly-balances/multiple-creation/date/:period" element={<MultipleMonthlyBalanceCreationUpdate />} />

            <Route path="app/income-categories" element={<IncomeCategories />} />
            <Route path="app/income-categories/creation" element={<IncomeCategoryCreation />} />
            <Route path="app/income-categories/:id" element={<IncomeCategoryEdit />} />

            <Route path="app/incomes" element={<ListIncomes />} />
            <Route path="app/incomes/creation" element={<IncomeCreation />} />
            <Route path="app/incomes/:id" element={<IncomeEdit />} />

            <Route path="app/obligations" element={<Obligations />} />
            <Route path="app/obligations/creation" element={<ObligationCreation />} />
            <Route path="app/obligations/:id" element={<ObligationEdit />} />
            <Route path="app/obligations/payment-change/creation" element={<PaymentChangeCreation />} />
            <Route path="app/obligations/payment-change/:id" element={<PaymentChangeEdit />} />

            <Route path="app/spending-limits" element={<SpendingLimits />} />
            <Route path="app/spending-limits/creation" element={<SpendingLimitCreation />} />
            <Route path="app/spending-limits/:id" element={<SpendingLimitEdit />} />

            <Route path="app/goals" element={<Goals />} />
            <Route path="app/goals/creation" element={<GoalCreation />} />
            <Route path="app/goals/:id" element={<GoalEdit />} />

            <Route path="app/accounts" element={<Accounts />} />
            <Route path="app/accounts/creation" element={<CreateAccount />} />

            <Route path="app/assets" element={<Assets />} />
            <Route path="app/assets/import" element={<AutomaticAssets />} />

            <Route path="app/dividends" element={<Dividends />} />
            <Route path="app/dividends/import" element={<AutomaticDividends />} />

            <Route path="app/saving-categories" element={<SavingCategories />} />
            <Route path="app/saving-categories/creation" element={<CreateSavingCategory />} />
            <Route path="app/saving-category/:id" element={<SavingCategoryEdit />} />

            <Route path="app/budgets/saving/creation" element={<SavingBudgetCreation />} />
            <Route path="app/budgets/creation" element={<BudgetCreation />} />
            <Route path="app/budgets/date/:year/:month" element={<Budgets />} />
            <Route path="app/budgets/date/:year" element={<Budgets />} />
            <Route path="app/budgets/edit/:id" element={<BudgetEdit />} />
            <Route path="app/budgets/saving/edit/:id" element={<SavingBudgetEdit />} />

            <Route path="app/settings" element={<Settings />} />
            <Route path="app/docs" element={<ExternalRedirectPage
                                               url="https://micheleva.gitlab.io/budgeteer/"
                                               label="Access Documentation"
                                               title="Comprehensive Project Documentation"
                                               desc="We're redirecting you to our detailed online documentation."/>} />
            <Route path="app/logout" element={<Logout />} />
            <Route path="*" element={<Page404 />} />
          </Routes>
  )
}

export default function App (): JSX.Element {
  return (
    <Router>
        <Links />
          <MyRoutes />
    </Router>
  )
}
render(
  <App />,
  document.getElementById("root")
)
