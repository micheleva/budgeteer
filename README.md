# Budgeteer - Household Budget Management made simple 📈💰

![GPL v.3 license](https://img.shields.io/badge/license-GPL%20v3.0-brightgreen.svg "GPL v.3 License")
![Test coverage 92%](https://img.shields.io/badge/coverage-92%25-hsl(72%2C%20100%25%2C%2040%25) "Test coverage 92%")
![Python](https://img.shields.io/badge/python-ffde57?logo=python&logoColor=%233776AB "Python logo")
![React](https://img.shields.io/badge/react-black?logo=react&logoColor=%2361DAFB "React logo")
![PostgreSQL](https://img.shields.io/badge/postgresql-%234169E1?logo=postgresql&logoColor=white "PostgreSQL logo")
![nginx](https://img.shields.io/badge/nginx-white?logo=nginx&logoColor=%23009639 "Nginx logo")

- [About the project](README.md#about-the-project)
- [Roadmap](README.md#roadmap)
- [Features](README.md#features)
- [Built with](README.md#built-with)
- [Getting started](README.md#getting-started)
  - [Prerequisites](README.md#prerequisites)
  - [Installation](README.md#installation)
  - [Prepare the env files](README.md#prepare-the-env-files)
- [Quick start](README.md#quick-start)
  - [Backend](README.md#backend)
  - [Frontend Single page app](README.md#frontend-single-page-app)
  [Documentation](README.md#documentation)
- [Housekeeping tasks, useful commands, tips](README.md#housekeeping-tasks-useful-commands-tips)
- [Author](README.md#author)
- [License](README.md#license)
- [Credits](README.md#credits)
- [References and useful links](README.md#references-and-useful-links)


About the project
=================

Budgeteer is a barebone web application for managing household budgets.

**The official documentation can be found [here](https://micheleva.gitlab.io/budgeteer).**

The alpha command line for this project can be found at [bcli](https://gitlab.com/micheleva/bcli).

Features
=================
- Multi tenant
- Track expenses, budgets ,assets, dividends, income, expenses, incomes, capital goals
- Visualize expenses against incomes, and expenses against monthly and yearly budgets trends
- Import third party investing data (assets and dividends)
- Include command to simulate above mentioned third parties APIs, to enable developers to test budgeeter locally
- Allow granular monthly, and yearly, budgeting per expense category
- Interactive graphs
- Night mode
- Permissive Open source license
- Fully Containerised
- Dump/Restore DB data with one command
- Include command to seed the DB, to enable developer to test the app locally, out-of-the-box
- Includes Ansible playbooks to provision an host, and to deploy the app remotely
- Housekeeping and GUI tools already set-up in an extra override docker-compose file
- 92% unit & functional test coverage
- Detailed documentation
- Centralised environments management
- (Optional) Automatic DB backups stored into git, and/or into s3 buckets
- (Optional) *Rudimental* bots blocking logic implemented
- **(beta)** Partial support for foreign currencies 
- **(alpha)** Automatic Speech Recognition: Speak and create expenses

Built with
---------------------
- Python 3.12
- Django 4.2
- Django restframework 3.14
- React 17
- Postgre 14
- nginx 12.7

Roadmap
=================
Refer to the [roadmap section of the documentation](https://micheleva.gitlab.io/budgeteer/roadmap/).

Getting started
===============

Prerequisites
--------------------------
- Python 3.11 or later installed
- Optional: Postgre 14 instance if working with the non containarized application (i.e. non-containarized devevelopment environement)

Installation
--------------------------

Clone the repo

    git clone https://gitlab.com/micheleva/budgeteer

Install the requirements

    cd app
    python -m venv virtualenv
    source virtualenv/bin/activate
    python -m pip install --upgrade pip
    (virtualenv) $ pip install -r requirements.txt

Prepare the env files
--------------------------
Copy the sample env files, and edit them accordingly

    cp .env.sample .env
    cp app/.env.sample app/.env

The `.env` file in the root folder will decide whether to use cache for docker builder, or not.

The `.env` file inside the app folder will decide everything else.
For a comprehensive explaination of all env parameters, refer to the [env-explaination section of the documentation](https://micheleva.gitlab.io/budgeteer/env-explaination/).

For more details on the parameters used by the frontend, plase see the `Frontend (Single page app)` section of the [of the documentation](https://micheleva.gitlab.io/budgeteer/env-explaination/).


Quick start
=======
For more details, refer to the [quickstart section of the documentation](https://micheleva.gitlab.io/budgeteer/quick-start/)

Backend
--------------------------

Run the django development server

    cd app
    # this assume you've alread executed `python -m venv virtualenv` as shown above
    source virtualenv/bin/activate
    python -m pip install --upgrade pip
    pip install -r test-requirements.txt
    (virtualenv) $ python manage.py runserver [0.0.0.0:8000]

Run the dockerized version of the app (note we use `docker-compose` and not `docker compose`)

    DOCKER_BUILDKIT=1 BUILDKIT_INLINE_CACHE=1 docker-composer -f docker-compose.yml up -d --build


Frontend (Single page app)
--------------------------

Make sure to be using `npm 6.14` or higher versions

    npm --version
    6.14.6

Run the webpack dev server to have live reloading

    cd frontend
    npm ci # or `npm install` (this might update the lock file)
    # In case of error `Error: error:0308010C:digital envelope routines::unsupported`
    # See https://stackoverflow.com/a/73027407: it has the hacky and fast solution, and as the correct one (choose wisely Neo!)
    npm run start # or `npm start`

Make sure to change the `.env` file inside the `app` folder in order to actually use live reload in the app.

    sed -iE "s/USE_WEBPACK_DEV_SERVER=(.*)/USE_WEBPACK_DEV_SERVER=y/" ../app/.env

If you should ever get `Error: error:0308010C:digital envelope routines::unsupported` error when running the webpack dev server, please refer to the [self-memo section documentation](https://micheleva.gitlab.io/budgeteer/self-memo).

In production use a static bundle instead

    sed -iE "s/USE_WEBPACK_DEV_SERVER=(.*)/USE_WEBPACK_DEV_SERVER=n/" ../app/.env

Build the static bundle to be served by Nginx

    npm run build
    
    # In the rare event where the bundle should not have been refreshed properly, manually delete the bundle.js files and rebuild it
    rm ../app/budgets/static/js/bundle.js
    rm ../app/static/js/bundle.js
    npm run build

Housekeeping tasks, useful commands, tips
===========================
To resolve errors that might be encountered during the first test suite execution (e.g. fresh new posgre setup missing privileges), one liners to restore/backup the data, increase/decrease dev env log verbosity, and other common issues that might arise during development, please refer to the [self-memo section documentation](https://micheleva.gitlab.io/budgeteer/self-memo).


Documentation
===========================
To browse the documentation offline, please do

```
cd ./docs/mkdocs-src
python -m venv virtualenv
source virtualenv/bin/activate
python -m pip install --upgrade pip
pip install -r docs-requirements.txt
mkdocs serve --clean [-v]
```


Author
=======

Budgeteer was created by [Michele Valsecchi](https://gitlab.com/micheleva).


License
=======

GNU General Public License v3.0

See [COPYING](./COPYING) to see the full text.


Credits
=======

Refer to the [credits section of the documentation](https://micheleva.gitlab.io/budgeteer/credits).


References and useful links
===========================

Refer to the [reference section of the documentation](https://micheleva.gitlab.io/budgeteer/references).
