# Coding Conventions (linting)

## Backend

Use `black` (version `v23.12.1`) with the `--line-length=79` option. Ensure that black is executed from the `app` folder to allow it to automatically fetch configuration from `app/pyproject.toml`.

    cd app
    python -m venv virtualenv
    source virtualenv/bin/activate
    (virtualenv) pip install -U pip setuptools
    (virtualenv) pip install poetry
    (virtualenv) poetry install --with dev [--no-root] # See https://python-poetry.org/docs/basic-usage/#installing-dependencies-only
    (virtualenv) $ black --check --verbose app/api/v1/tests # When running black from inside the app folder
    (virtualenv) $ cd ..
    (virtualenv) $ black --check --verbose app/api/v1/tests --config app/pyproject.toml # When running black from any other folder

Use the snippet below to the limitation mentioned in [issue 2843](https://github.com/psf/black/issues/2843), where black struggles to maintain pragma comments on the correct line. For special pragma comments, such as those related to `E1101` for Django Models, encapsulate the relevant blocks with `fmt:off` and `fmt:on` comments.

```python
# fmt: off
self.assertEqual(
   m.Account.objects.count(), 1  # pylint: disable=E1101; # noqa
)
# fmt: on
```

## Frontend

`TODO`

See `frontend/.eslintrc.js` in the meanwhile.


## Install pre-commit linting hook(s)

To install the back-end code linting pre-commit hook, use the following steps:

    cd app
    python -m venv virtualenv
    source virtualenv/bin/activate
    (virtualenv) pip install -U pip setuptools
    (virtualenv) pip install poetry
    (virtualenv) poetry install --with dev [--no-root] # See https://python-poetry.org/docs/basic-usage/#installing-dependencies-only
    (virtualenv) $ pre-commit install

### Manually run the hook

    cd app
    python -m venv virtualenv
    source virtualenv/bin/activate
    (virtualenv) pip install -U pip setuptools
    (virtualenv) pip install poetry
    (virtualenv) poetry install --with dev [--no-root] # See https://python-poetry.org/docs/basic-usage/#installing-dependencies-only
    (virtualenv) $ pre-commit run


