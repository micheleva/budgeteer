## Dropped tasks:

- 4 FRONTEND: add "1 month" "1 year" buttons to "List Expenses" frontend view, so that we canchange the start/end fields on the fly without having to enter them manually
- 12 BACKEND-FIX: change redis cache format, so that we can add new dividends
    - at the moment, as soon as a symbol has one dividend, nothing will ever be checked again
    - "old" budgeteer:1:YYYY-MM-DD entries do not contain user ID, but it contains how much stock are owned. This should be fixed! // or 
    - **BORKED** The calendar's current month (e.g. "November 2023") seems to be floating, on mobiles. Because of that it messes up with the calendar controls.
    - **BORKED** Also, on mobile clicking on the events does not show any detail
    - in `seed-db` command, add future dividend data for the generated assets
    - in `wipe-user-id` command, remove the future dividend data for the generated assets if no other users have said asset
- 19 FRONTEND: implement privacy mode: display numbers only if the privacy mode is off, otherwise show "XXX", and hide mouseover info on graphs
    - store settings on DB or in a cookie, not in redis or in an .env file!
- 25 FRONTEND-NEW: make the app multilingual
    - still force comma and currencies formatting for now?
    - old pages do not get i18n l10n: they sta in hardcoded English
- 26 BACKEND-NEW: add currency (and rate?) on each table? Or add rates table with each (pair and date (and time???))?
    - what about assets? each transaction should have its own rate!
        - partially done in `BACKEND-FIX 202`
- 27 BACKEND: change all places to use UUID instead of IDs
- 28 BACKEND: freshly created models should return their ids/UUIDs on the /create api return object, so that tests can be claner, and frontend has an easier life
    - note that spa integration tests are doing complex things to get the ID after creation: 
      simplify them after returning the created obj id in the repsonse
- 29 FRONTEND: enable sort by date (asc/desc) in display expenses (all views)
- 32 introduce terraform usage
    - add terraform script to generate instance ec2
    - create ec2 user, add it to sudoers
    - change ssh port
    - disable root remote connection ONLY AFTER CONFIRMING YOU can connect with that user
    - generate ansible hosts file
- 33 point domain to our applications with a script (i.e. use aws cli/commands)
    - add terraform script? aws-cli to point s3 record(s) to new spinned up instance
    - confirm how long the tts is for that domain
    - add load balancer in front of ec2 instances ?
- 36 BACKEND: fix "lax" cookies in the Django app: figure out what these modes are, and default to best practices (likely some work need in the django bits)
- 38 (django view pages, "/old" pages) assets pie graph should format amounts to show currency and use the correct comma separator
- 40 FRONTEND: re-implement perform_update() method in Expenses (in api.v1.views), in order to prevent the server from revealing which expenses do exists and which do not
    - add test to patch expense endpoint
    - do it for all the other models as well
- 44 value if we should allow to host static files (js, icons, css, etc.) in s3, through a flag in the docker-compose.yaml file
    - [Docs](https://docs.djangoproject.com/en/4.2/howto/static-files/deployment/#serving-static-files-from-a-cloud-service-or-cdn)
    - [Docs](https://django-storages.readthedocs.io/en/latest/)
    - [References](https://testdriven.io/blog/storing-django-static-and-media-files-on-amazon-s3/)
    - [and its repo](https://github.com/testdrivenio/django-docker-s3)
- 47 generate ical for each user with future dividends events
- 73 INFRA: update budgeteer backup script to use a simpler diff logic, since pgdump is a text file and not a binary one
- 75 add encrypt cert-bot container
    - add the "create the certificate from scratch logic" (and its container)
    - add the renewal container
    - add dummy cert container: atm docker-composer will fail hard without a pre-made HTTPS cert
- 81 FRONTEND-NEW: nothing is paginated in the spa: fix it!
    - how to paginate items that will be used to populate dropdown: i.e. what about users with 32,000 categories?
    - should we have an upper limit for some entities? if so, can we enforce it at db level (likely not!), or at a model level on django side (e.g. as a hook on create, etc.)
- 82 FRONTEND: each entity creation view should enable to create its parent entity as well
    - e.g. creating an expense should enable to create a category in the same view
    - creating an income should enable to create an income category in the same view
    - this is going to be massive, delayed to post v1.0
- 87 BACKEND: registration should use the usual email verification flow
- 91 rename apiResponseComplete() to onViewInitApiResponsesComplete()
- 107 INFRA-FIX: make the containers use ports higher than 1024 when exposing them # dockerfile best-practices
    - once they're unified update the frontend as well
- 121 rename "category" to "expense category" model... also update all the views, the unit tests and the integration tests as well
    - yes, even for the /old page django views!
- 170 INFRA-FIX: add static volume to nginx container, so we can detect bot from the logs
    - we already log the files to the host, we can have fail2ban on the host. This will make the containers leaner
- 171 INFRA-NEW: add fail-to-ban to nginx container? feed nginx logs to the host fail-to-ban, and create an ansible script to set it up?
- 181 REFACTOR: remove all the logic in the backend (and the frontend as well) to consider JPY as the local currency (especially the orm select statement, and in particular those using `Case`)
    - we are not going to have other currencies as the local one in `v1.0`
- 186 NEW: allow account edit
    - create frontend view for editing
    - edit account list view to link to edit view
    - backend: change function based route to ClassBased one
    - make sure that the serializer allows to update is_foreign_currency when also PUT/PATCH ing with the original text in the body // See category serializer
- 194 FRONTEND-BUG: edit monthly balance in spa app does not have focus on the text input on view init
    - need to update react-hook-form to [7.27. or higher](https://github.com/react-hook-form/react-hook-form/releases/tag/v7.27.0)
    - use this repo to speed up fixing the update breaking code (errors and register) [ref](https://github.com/react-hook-form/codemod)
- 196 BACKEND & FRONTEND-NEW: implement signup and login logic in api.v1 (i.e. do not use the old app login page), and provide routes for the frontend
    - implement frontend views
    - change landing page, and add "login from single page app" (same for signup)
    - add noscript links for old login and register routes
- 197 FRONTEND & BACKEND REFACTOR: delete income categories???
    - should we really allow delete for categories? Atm it is not doing cascade delete, but setting the foreign key to null.
    - Also, most parts of the app, in particular the odl, are not handling the null case
    - Lookup if we can do a transaction where we delete all the income with that category + the category, and revert on failure of either
    - Also, show how many items would be deleted (e.g. "Deleting this category will destroy 123 income entries as well. Do you want to proceed?")
        - And maybe add an input field in which the user should type "Yes, I'm OK destroyin 123 entries".
- 198 INFRA: use redis instead of db for auth (i.e. store sessions in readis to avoid hitting the db on each request)
    - session != auth. If we store auth info in the sesssion, on redis, the redis container will become a greater attack vector. Also, it will make harder to invalidate or update auth info, since we do not hit the db on each request anymore
- 199 INFRA: tune databases indexes (e.g. see if the current index are being referenced/used, add extra indexes: especially on the expense, asset and dividend tables, etc.)
- 214 TESTING: add regression (functional and unit) tests for `BUGFIX-213`
- 217 TESTING: add regression functional tests for `BUGFIX-216`
- 226 INFRA: add different folders under var_configs and the change the playbook to use hosts: all, and reference the vars related to the server name in the playbook:
    - production should use vars_config/production_root_env vars_config/productio_app_env
- 234 FRONTEND-FIX: allow the Pulldown component to receive a name and an id props, so that we can set them to the select and have a label to point to it
- 236 TESTING: add regression functional and unit tests for `BUGFIX-230`
- 237 FRONTEND-FIX: confirm errors are displayed correctly in all forms // likely need to alter formatError() func to return somehing else than an unstyled "p"
- 239 FRONTEND-FIX: show more details in the deletion confirmation popup (e.g. asset amount & name & account name, expense category & amount & date, etc.)
- 244 BACKEND-FIX: see if we can improve the naming of Dividend model's fields. i.e. the "foreign amount" field
- 246 BACKEND-FIX: monthly_balances_and_assets() is considering all assets as foreign currency, but this is not always the case, as some Accounts might have is_foreign_currency flag set to False
    - this is not an issue as said param is not used by the frontend
- 248 FRONTEND-FIX: make recoverableError and CriricalError banners responsive (and use dimissisableRecoverableError banner in all views)
- 249 BACKEND-FIX: double check that dividends creation is really validating all fields, and enforcing all constraints
- 255 INFRA: review nginx.conf, and remove as many "if" as possible.
    - [References](https://github.com/benoitc/gunicorn/blob/master/examples/nginx.conf)
- 260 TESTING: implement integration tests for `BUGFIX-250` (dividend aggregate by year and by symbol apis)
- 264 DOCS: Add s3 IAM role creation instructions to create a profile for the backup to s3
        {
            "Version": "2012-10-17",
            "Statement": [
                {
                    "Effect": "Allow",
                    "Action": "s3:*",
                    "Resource": [
                        "arn:aws:s3:::your-bucket-name",
                        "arn:aws:s3:::your-bucket-name/*"
                    ]
                }
            ]
        }
- 267 BACKEND-NEW: add a django task to download the most recent backup from the s3 bucket
    - inject it into the db
    - make sure this is not run on container start (i.e. by passing flags, otherwise when the container is restarted, as long as the flag is there... i.e. the container is not recreated, the same import will be execute, and fail hard as said data deos already exists)
- 272 TOOLING: add "future dividends" in redis in the seed-db command, and clean said "future dividends data" from redis in the wipe-user-id command
- 285 BACKEND-FIX: `MonthlyBudget` model could have `year` and `month` fields instead of a date ending in a dummy `-01`. See `Budget` model
- 307 COMMANDS: seed-db should generate expenses and budgets for rent, so that rent budgets usage is always 100%
    - This way we are sure that the UI has at lest one green bar
- 311 PIPELINES: publish a base budgeteer:test image, and use it in the pipelines, instead of relying on cache
    - also add a pipeline task to generate said image locally if not present and can't pull it (i.e. air-gapped env use case)
- 315 PIPELINES: lookup how to prevent merge if test coverage has gone down, of if unit tests have failed
    - Can't do on gitlab.com without Premium or Ultimate tier
    - References https://docs.gitlab.com/ee/ci/testing/code_coverage.html#coverage-check-approval-rule
    - References https://gitlab.com/gitlab-org/gitlab/-/merge_requests/64816
    - References https://gitlab.com/gitlab-org/gitlab/-/merge_requests/63079
    - references https://gitlab.com/gitlab-org/gitlab/-/issues/15765
    - Workaround https://gitlab.com/jheimbuck_gl/java-code-coverage/-/blob/master/code-coverage.sh?ref_type=heads
    - Cleaner wokraround https://rpadovani.com/gitlab-code-coverage
- 319 BACKEND: add a route to return only the sum of the expenses for a given period: this should drastically reduce the amount of data requested by the widgets => use ExpenseListAggregateByCategory? Create a view that just selects data using year and month, using postgres function?