title: Testing

## Testing

This application was developed and tested on Fedora :simple-fedora: versions 30 to 36, as well as Amazon Linux 2 (Karoo) and AWS2023 Linux :simple-amazonaws:.

The frontend is tested only on Firefox :simple-firefox:. (1)
{ .annotate }

1.  The functional test suite can easily be run using Chromium :material-google-chrome:. Set `Chromium` in `IntegrationTest.BROWSER` inside `app/functional_tests/test_parent.py`. Make sure Chromium is installed, and it is in your `$PATH`.

Bugs against other platforms will be closed as `Not reproducible` (1)
{ .annotate}

1.  The application _might_ be working on Windows, but I'm unable to test it. Because of that any bug report for Windows will be closed as `Not reproducible`: do not take it personally, it is not. :cat:

## Prepare the evironment

=== "Use host selenium and firefox binary files"
    Confirm geckodriver is your `$PATH`

        which geckodriver

    Install the requirements

        cd app
        source virtualenv/bin/activate
        (virtualenv) pip install -U pip setuptools
        (virtualenv) pip install poetry
        (virtualenv) poetry install [--no-root] # See https://python-poetry.org/docs/basic-usage/#installing-dependencies-only
        pip install -r test-requirements.txt



=== "Use containarized Selenium and Firefox/Chromium"
    Install the requirements

        cd app
        source virtualenv/bin/activate
        (virtualenv) pip install -U pip setuptools
        (virtualenv) pip install poetry
        (virtualenv) poetry install [--no-root] # See https://python-poetry.org/docs/basic-usage/#installing-dependencies-only
        pip install -r test-requirements.txt

    Prepare the Firefox/Chromium Selenium Hub container
        
        # Get the IP address of docker0 interface
        DOCKER0_IP=$(docker network inspect bridge --format '{{(index .IPAM.Config 0).Gateway}}')

        # Inject said IP, otherwise the remote selenium container will try to access its own localhost
        docker run -d --name firefox-latest --shm-size="2g" --network=host --add-host host.docker.internal:"$DOCKER0_IP" selenium/standalone-firefox:latest

        # e.g.
            # docker run -d  --name firefox-latest --shm-size="2g" --network=host --add-host host.docker.internal:172.17.0.1 selenium/standalone-firefox:latest

    To enable concurrency use the following paramters: replace `<integer-number>` with a number (e.g. `4`). Please beware that using too many concurrent sessions migh lead to test failure if your machine does not have enough CPU/RAM. See selenium/selenoid upstream documentation for more details on how to tune those values.

        docker run -d -e VNC_NO_PASSWORD=1 -e SE_NODE_OVERRIDE_MAX_SESSIONS=true -e SE_NODE_MAX_SESSIONS=<integer-number>  --name <firefox-budgteer-testing> --shm-size="2g" --network=host --add-host host.docker.internal:172.17.0.1 selenium/standalone-firefox:latest



# Run the test suites

### Prerequisites:


=== "Use host Selenium and Firefox/Chromium binary files"
    - Firefox :simple-firefox: is installed
    - Geckodriver is installed. [Get geckodriver](https://github.com/mozilla/geckodriver/releases). 
    - **_Optional_**: if you want to run the functional test suite against `Chromium` :material-google-chrome:, make sure that both Chromium and [chromiumdriver](https://chromedriver.chromium.org/downloads) are installed and in your `$PATH`. :warning: Make sure to install the correct driver for your Chromium version. For more details [refer to this matrix](https://chromedriver.chromium.org/downloads/version-selection).
    - `IntegrationTest` class's CONTAINARIZED parameter should be set to `FALSE`
    - :warning: Make sure to install the correct driver for your Firefox/Chromium version. For more details [refer to this matrix](https://firefox-source-docs.mozilla.org/testing/geckodriver/Support.html).


=== "Use containarized Selenium and Firefox/Chromium"
    - A container with Firefox or Chromium should be up and running. See `Prepare the environment` section in this Testing document
    - `IntegrationTest` class's `CONTAINARIZED` parameter should be set to `TRUE` (This is the default)



### Actually run the tests

These steps are the same when using the host Selenium and Firefox/Chromium binary files, and when using containarized Selenium and Firefox/Chromium as well.

Run both functional and unit test suites

    [TEST_TARGET=localhost ] python manage.py test --keep [--parallel]

Note that `parallel` option requires the third-party tblib package to display tracebacks correctly ([django docs)](https://docs.djangoproject.com/en/4.2/ref/django-admin/#cmdoption-test-parallel):

    source virtualenv/bin/activate
    (virtualenv) $ poetry add tblib --dev

How to run only the functional test suite:

    [TEST_TARGET=localhost ] python manage.py test functional_tests [--no-input --keep]

How to run a only single functional test:

    [TEST_TARGET=localhost ] python manage.py test functional_tests.<file_name_without_py>.<class_name>.<method_name> [--no-input --keep]
    # e.g. python app/manage.py test functional_tests.test_base.FunctionalTest.test_expenses [--no-input --keep]
    # e.g. python app/manage.py test budgets.tests.test_models.ModelsTest.test_malformed_expenses_triggers_errors [--no-input --keep]

How to run only the unit test suite:

    [TEST_TARGET=localhost ] python manage.py test budgets [--no-input --keep]

How to run a only single unit test:

    [TEST_TARGET=localhost ] python manage.py test api.v1.tests.SetAsideCategoryTest.test_can_not_create_duplicated_purpose_savingcategories [--no-input --keep]


### Tips

Use the [--keep flag](https://docs.djangoproject.com/en/.2/topics/testing/overview/#the-test-database) to make tests faster.

In order to see what the browser is actually doing in the fuctional test, change `HEADLESS` to be `False` in `app/functional_tests/test_parent.py` when running the functional test suite using Selenium and Firefox/Chromium binaries. When using the containarized setup, just navigate to the Selenium hub (default `127.0.0.1:4444`). The Selenium hub address can be found in the execution logs of the functional tests suite.

The functional tests are setup to run by default with `Firefox`. When `BROWSER` in `app/functional_tests/test_base.py` is set to a value different from `Firefox`, `Chromium` browser will be used instead.

If your local environment has `USE_WEBPACK_DEV_SERVER` set to `"y"` in `app/.env`, make sure to have `webpack` running (e.g. `npm run start-dev`) while execuing the tests suite, otherwise most of the functional tests will fail.


## Coverage

Generate coverage

    cd app
    ./tools/generate_coverage.sh

CLI report is already printed to screen and `app/coverage_html` contains the html representation of the results.
