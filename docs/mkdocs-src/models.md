# Models
## ERDs

=== "Styled ERD"
    Click [here to enlarge the image](./images/budgeteer-erd-d2.png).
    ![ERD](./images/budgeteer-erd-d2.png){ align=left }

=== "Conventional ERD"
    Click [here to enlarge the image](./images/budgeteer_erd.png).

    ![ERD](./images/budgeteer_erd.png){ align=left }


## Models details

<span style="color: var(--md-code-hl-function-color); font-weight: bold;">User</span> represents individuals registered within Budgeteer, capturing their account information.

!!! abstract "Implementation details"

    The logic lives in `app/accounts`, and the `User` entity is taken from `django.contrib.auth`


<span style="color: var(--md-code-hl-function-color); font-weight: bold;">Category</span> represents a type of Expense. e.g. Food, Rent, Medicals

!!! example "Example"

    e.g. Food, Rent, Bills, etc. - <small>This entity should have been named `Expense Category` :sweat_smile:</small> 

<span style="color: var(--md-code-hl-function-color); font-weight: bold;">Expense</span> represents the details of a negative transaction. 

!!! example annotate "Example"

    e.g. Spent $12 at the grocery store to buy Food on 1970-01-01 (1)

  1.  :bulb: "Food" is the `Category`

<span style="color: var(--md-code-hl-function-color); font-weight: bold;">Budget</span> represents how much money has been allocated to

- be **_spent_** monthly, or yearly, for a given `Category`
- to be **_saved_** (or to be **_invested_**) monthly, or yearly, for a given `Saving Category`

<span style="color: var(--md-code-hl-function-color); font-weight: bold;">Saving Category</span> represents the amount of money to set aside monthly (or yearly) for saving or investing.


<span style="color: var(--md-code-hl-function-color); font-weight: bold;">Income category</span> represents a type of income transaction.

!!! example "Example"

    Wage, Insurance payback, Pension

<span style="color: var(--md-code-hl-function-color); font-weight: bold;">Income</span> respresents the details of a positive transaction 

!!! example annotate "Example"

    Received my $420 wage on 1970-01-01(1) 

1.  "Wage" is the `Income Category`

<span style="color: var(--md-code-hl-function-color); font-weight: bold;">Monthly balance category</span> represents the category of a balance.

!!! example "Example"

    Bank account 01, Cash hidden under the mattress - <small>this entity should have been named account, as in a 'Bank account'</small>

<span style="color: var(--md-code-hl-function-color); font-weight: bold;">Monthly balance</span> represents the details of the balance at the end of each month for a given category.

!!! example annotate "Example"

    At the end of January 1970, I had $408 in my Bank account, at Bank 01(1)

    At the end of January 1970, I had $2 hidden under my mattress(2)

    At the end of February 1970, I had $450 in my emergency expenses envelope(3)

1.  :bulb: "Bank account" is the `Category`
2.  :bulb: "Hidden under my mattres" is the `Monthly Balance Category`. The `Monthly Balance Category` can literally be anything, it does not have to represent a bank/brokerage account
2.  :bulb: "Emergency expenses envelop" is the `Monthly Balance Category`. The `Monthly Balance Category` can literally be anything, it does not have to represent a bank/brokerage account

<span style="color: var(--md-code-hl-function-color); font-weight: bold;">Goal</span> represents the amount of money needed to reach a certain goal.

!!! example "Example"

    I want to my whole fortune to be worth $842 by the end of 1971

    I need $5,000 to FIRE

    I want to save $315 so that I travel to the South Pole on a brand new camel

<span style="color: var(--md-code-hl-function-color); font-weight: bold;">Obligation</span> represents regular and variable payment obligations.

!!! example "Example"

    Fixed Obligation: A fixed-rate loan with a total amount of $10,000, a monthly payment of $200, starting on 2023-01-01, with payments due on the 15th of each month, ending on 2028-01-01.

    Variable Obligation: A variable-rate loan with a monthly payment of $150, starting on 2023-01-01, with payments due on the 10th of each month, ending on 2025-01-01.

    Fixed Obligation: A mortgage with a total amount of $200,000, a monthly payment of $1,500, starting on 2022-05-01, with payments due on the 1st of each month, ending on 2042-05-01.

<span style="color: var(--md-code-hl-function-color); font-weight: bold;">ObligationPaymentChange</span> tracks adjustments to the `Obligation` payment amounts and the dates these changes take effect.

!!! example "Example"

    Change for Obligation 2: New monthly payment of $250, effective from 2024-01-01.

    Change for Obligation 2: New monthly payment of $170, effective from 2024-06-01.

    Change for Obligation 2: New monthly payment of $1,600, effective from 2023-12-01.

<span style="color: var(--md-code-hl-function-color); font-weight: bold;">Account</span> represents an account holding assets.

!!! example "Example"

    Brokerage account at firm A

    RSU stocks from my company


!!! note "Account vs Monthly Balances"

    Why do we need both `Accounts` and `Monthly Balance` as well?

    First, `Monthly Balance` can only store a number, while models linked to an `Account` can store much more data. Second, the following models have different granularity when it comes to data

    `Monthly balances` allow <strong>monthly</strong> granular records. i.e. The balance at the end of a given month for a given Category

    `Accounts` related models (e.g. `Asset` and `Dividend`) allow **daily** granular records. i.e. The assets owned on 1970-01-01

<span style="color: var(--md-code-hl-function-color); font-weight: bold;">Asset</span> represents any kind of financial assets.

!!! example "Example"

    Stocks of company A, Sovereign bonds of Country 1, Corporate bonds, Preferred equities, Foreign currency cash

!!! question "Importing Third parties API data"

    To avoid users micromanaging these entity, these models can only be created by importing data from third party APIs. For details about the quirks of how Asset data should be provided, refer [the third party API format specs](./assets-and-dividends-creation-json-spec.md).

    For details about how budgeteer displays assets (and why) refer [the assets and profits quirks doc](./assets-and-dividends-quirks.md).

<span style="color: var(--md-code-hl-function-color); font-weight: bold;">Dividend</span> represents any passive income (unearned income).

!!! example "Example"

    Interest from savings accounts, Bond interest, Dividends from Stock A


<span style="color: var(--md-code-hl-function-color); font-weight: bold;">Spending limits</span> represents the sum of expenses recorded in the last X days, for a given category, including a given text in the expense's note field.

!!! example "Example"

    Spending Limits for Expenses of Category "Food", with text "grocery store" in the last 31 days: their sum should be less than 3,200