### Log errors

Unless you change `app/budgeteer/settings.py`, by default, to see errors in the console, when running `python manage.py runserver`use `logger.info()` or higher severity as `.warning()` or `.error()` if `'DEBUG'` is set to `'y'` in `app/.env`.

If you want to see logger.debug() messages as well, change the console log facility level
and set it to DEBUG (beware you'll see lots of logs from django as well. Do not do it in production):

    LOGGING = {
        [...
        'handlers': {
            [...]
            },
            'console': {
                'class': 'logging.StreamHandler',
                'level': 'INFO' if DEBUG else 'ERROR', <=== change this line to 'DEBUG'

## Backup data

Dump the postgres content to a file:

     docker exec budgeteer-web-1 sh ./backup.sh
     docker cp budgeteer_web_1:/home/app/web/data_only.sql .

## Restore data

Move the backup file to web container, and restore it

    docker cp data_only.sql budgeteer-web-1:/home/app/web/data_only.sql
    docker-compose exec web /home/app/web/restore.sh /home/app/web/data_only.sql
    # or 
    # docker-compose exec  web  bash -c "BACKUP_PATH=/home/app/web/data_only.sql /home/app/web/restore.sh"

## Manually restore data

Assuming the data has already been moved to the web container, do:

    docker-compose exec -it web sh
    export PGPASSWORD='<your-pg-password>' # optional, if you do not set it, a prompt will ask for your password
    psql -h db -U budgeteer_user -d budgeteer_db < <file-name-to-inject>

## Postgresql Table's Indexes reset

Reset an index in postgresql:

    ALTER SEQUENCE <tablename>_<id>_seq RESTART WITH <integer>
    e.g. ALTER budgets_profit_id_seq RESTART with 1;


## Force python to print inside the container

In the code:

    print("<result-to-print>", flush=True)


## How to fix errors running the npm dev server

If you see this error:

        Error: error:0308010C:digital envelope routines::unsupported
        at new Hash (node:internal/crypto/hash:67:19)
        at Object.createHash (node:crypto:130:10)
        at module.exports (/Users/user/Programming Documents/WebServer/untitled/node_modules/webpack/lib/util/createHash.js:135:53)
        at NormalModule._initBuildHash (/Users/user/Programming Documents/WebServer/untitled/node_modules/webpack/lib/NormalModule.js:417:16)
        at handleParseError (/Users/user/Programming Documents/WebServer/untitled/node_modules/webpack/lib/NormalModule.js:471:10)
        at /Users/user/Programming Documents/WebServer/untitled/node_modules/webpack/lib/NormalModule.js:503:5
        at /Users/user/Programming Documents/WebServer/untitled/node_modules/webpack/lib/NormalModule.js:358:12
        at /Users/user/Programming Documents/WebServer/untitled/node_modules/loader-runner/lib/LoaderRunner.js:373:3
        at iterateNormalLoaders (/Users/user/Programming Documents/WebServer/untitled/node_modules/loader-runner/lib/LoaderRunner.js:214:10)
        at iterateNormalLoaders (/Users/user/Programming Documents/WebServer/untitled/node_modules/loader-runner/lib/LoaderRunner.js:221:10)
    /Users/user/Programming Documents/WebServer/untitled/node_modules/react-scripts/scripts/start.js:19
      throw err;
      ^

Refer to this [stackoverflow question](https://stackoverflow.com/questions/69692842/): this link has the [fast and dirty solution](https://stackoverflow.com/a/77714180), as well as the [correct one](https://stackoverflow.com/a/73027407)

## Enable Django to create test db

    (virtualenv) [user@localhost app]$ python manage.py test [api]
    Creating test database for alias 'default'...
    Got an error creating the test database: permission denied to create database

    (virtualenv) [user@localhost app]$ sudo su - postgres
    [postgres@localhost ~]$ psql
    psql (12.8)
    Type "help" for help.

    postgres=# ALTER USER budgeteer_user WITH CREATEDB;
    ALTER ROLE

    (virtualenv) [user@localhost app]$ python manage.py test [api]
    Creating test database for alias 'default'...
    System check identified no issues (0 silenced).
    .......
    ----------------------------------------------------------------------
    Ran 7 tests in 1.113s

    OK


## Most Functionals tests are randomly failing!
It might be that your machine is not having enough CPU/RAM availabl to use --parallel in the test suite: said mode will run one parallel test process per number of cpus available on your machine. i.e. the browser instances managed by selenium might not have enough time to properly render the js before the test's TIMEOUT marks the test as failed.


Either test this on a machine with enough CPU and RAM, or temporarily edit the tools/coverage.sh to remove the --parallel flag, or to allocate a lower number of tests to be run parallelly by specifing a number (e.g. --parallel=3 will only run 3 tests at the same time. You can check how many cpus you do have with `lscpu | grep '^CPU(s):'`). Another workaround would be to set `USE_WEBPACK_DEV_SERVER=n` in `app/.env` and stop the `webpack dev server` to spare extra resources for selenium.


## Access local postgre instance (non containarized) on dev machine

For Fedeora refer to [fedora docs](https://docs.fedoraproject.org/en-US/quick-docs/postgresql/#installation)


CLI:

    sudo dnf install postgresql-server postgresql-contrib
    sudo systemctl enable postgresql
    sudo postgresql-setup --initdb --unit postgresql
    sudo systemctl start postgresql
    sudo -u postgres psql


GUI:

    podman run -p 5051:80 --network slirp4netns:allow_host_loopback=true --add-host postgresql:127.0.0.1 -e PGADMIN_DEFAULT_EMAIL=admin-email@example.com -e PGADMIN_DEFAULT_PASSWORD=secret-password -d dpage/pgadmin4
    # then access example.com:5051 // make sure to have "example.com 127.0.0.1" in your /etc/hosts
    # in connection hostname use "10.0.2.2", port 5432, maintenance database: "postgres", username: <your-pgsql-username>, password: <your-pgsql-password>


See the following links for details about the misterious 10.0.2.2, and the "--network slirp4netns:allow_host_loopback=true" options

- [stackoverflow](https://stackoverflow.com/a/74580507)

- [podman docs](https://docs.podman.io/en/latest/markdown/podman-run.1.html#network-mode-net)


## Export json from postgres

CLI:

    psql -h db -d budgeteer_db -U budgeteer_user // insert pwd when prompted
    \t
    \a
    \o dates.json
    SELECT array_to_json(array_agg(row_to_json (p))) FROM ( SELECT DISTINCT record_date FROM budgets_dividend ORDER BY record_date)p;


## Unable to drop database in Postgres

    ERROR:  database "database_name" is being accessed by other users
    DETAIL:  There are 1 other session(s) using the database.

Credits[SO](https://stackoverflow.com/a/75806551):

    mydb=> select pid, application_name, state, query from pg_stat_activity where application_name = 'myapp';
     pid  | application_name | state | query 
    ------+------------------+-------+-------
     1234 | myapp            | idle  | 
     5678 | myapp            | idle  | 
    (2 rows)

    Then, closing the connection with the PID you picked like this

    mydb=> SELECT pg_terminate_backend(1234);


## Generate favico
Input the settings below at [https://favicon.io/favicon-generator](https://favicon.io/favicon-generator)

```
Text: BDG
Background: Rounded
Font Family: ABeeZee
Font Variant: Regular 400 Normal
Font Size: 60
```

## Generate d2 erd png

CLI::

    1. Generate pgdump
    docker exec budgeteer-db-1 pg_dumpall -U <budgeteer_user> > file.pgdump
    2. Feed the pgdump to pg2mysql (https://github.com/ChrisLundquist/pg2mysql)
    ./pg2mysql.pl < file.pgdump > mysql.sql
    3. Remove lines containing "CONSTRAINT", "ADD GENERATED BY DEFAULT AS IDENTITY" (also removing trailing commas when removing "ADD GENERATED BY DEFAULT AS IDENTITY")
    4. Feed the mysql dialect translation to sqldiagram (https://github.com/RadhiFadlillah/sqldiagram/tree/master)
    # Note, it seems --raw can not be redirected with -o, use shell redirection
    # Install it if needed
    # go install github.com/hayago/sqldiagram@latest
    sqldiagram mysql --raw-d2 true mysql.sql > erd.d2
    5. Manually edit the output, and add the relationships between tables, as follows at the end of the file
        budgets_asset.id -> budgets_account.id
        budgets_dividend.id -> budgets_account.id
        budgets_expense.category_id -> budgets_category.id
        budgets_income.category_id -> budgets_incomecategory.id
        budgets_monthlybalance.category_id -> budgets_monthlybalancecategory.id
        category -> budget
        setasidecategory -> budget
        budgets_spendinglimit.category_id -> budgets_incomecategory.id

    6. Shorten table names by removing the 'budgets_' prefix
    7. Add "direction:down" directive right before the tables declaration
    vim erd.2
    // d2 --dark-theme 200 -w erd.d2 --layout elk erd.png
    d2 --dark-theme 200 -w erd.d2 --layout elk
    # As it seems that atm playwright dependency is broken, manually convert svg to png
    8. Convert the svg result to png
    10. Move the png images to docs/mkdocs-src/images (these are targets for symbolic links to docs/erd)

GUI:

    1. Generate pgdump
    2. Feed the pgdump to pg2mysql (https://github.com/ChrisLundquist/pg2mysql)
    ./pg2mysql.pl < file.pgdump > mysql.sql
    3. Remove lines containing "CONSTRAINT", "ADD GENERATED BY DEFAULT AS IDENTITY"
    4. Feed the mysql dialect translation to sqldiagram (https://github.com/RadhiFadlillah/sqldiagram/tree/master)
    # Note, it seems --raw can not be redirected with -o, use shell redirection
    # Install it if needed
    # go install github.com/hayago/sqldiagram@latest
    sqldiagram mysql --raw-d2 true mysql.sql > erd.d2
    5. Manually edit the output, and add the relationships between tables, as follows at the end of the file
        budgets_asset.id -> budgets_account.id
        budgets_dividend.id -> budgets_account.id
        budgets_expense.category_id -> budgets_category.id
        budgets_income.category_id -> budgets_incomecategory.id
        budgets_monthlybalance.category_id -> budgets_monthlybalancecategory.id
        category -> budget
        setasidecategory -> budget
        monthlybalancecategory -> monthlybalance
    vim erd.2
    6. Shorten table names by removing the 'budgets_' prefix
    vim erd.2
    7. Add "direction:down" directive right before the tables declaration
    vim erd.2
    8. Copy paste in the sandbox (https://play.d2lang.com/) and choose "ELK" from Layout Engine (nice straight arrows) https://play.d2lang.com
    9. Save as png
    10. The png images are stored in both docs/erd and in docs/mkdocs-src/images as well