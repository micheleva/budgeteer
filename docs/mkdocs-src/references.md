## References and useful links

1. [TDD with Python and Django](https://www.obeythetestinggoat.com/)
1. [Django documentation](https://docs.djangoproject.com/en/4.2/)
1. [Selenium documentation](https://seleniumhq.github.io/selenium/docs/api/py/api.html)
1. [NGINX](https://nginx.org/en/docs/)
1. [Ansible documentation](https://docs.ansible.com/)
1. [Get geckodriver](https://github.com/mozilla/geckodriver)
1. [Imports order convention](https://docs.openstack.org/hacking/latest/user/hacking.html#imports)
1. [Migrate django from sqlite3 to postgreSQL](https://web.archive.org/web/20200802014537/https://www.vphventures.com/how-to-migrate-your-django-project-from-sqlite-to-postgresql/)
1. [Tmp folder permissions in RHEL like distros](https://stackoverflow.com/a/33223403)
1. [Fedora wiki on this feature](https://fedoraproject.org/wiki/Features/ServicesPrivateTmp)
1. [More about it](https://serverfault.com/a/464025)
1. [About custom selinux policies](https://serverfault.com/a/763507/332670)
1. [Executing queries on init (e.g. Models.py populating dropdown)](https://stackoverflow.com/a/39084645)


## Django notes 

1. [full_clean()](https://stackoverflow.com/questions/22587019/how-to-use-full-clean-for-data-validation-before-saving-in-django-1-5-graceful)
1. [full_clean() implementation history](https://stackoverflow.com/questions/4441539/why-doesnt-djangos-model-save-call-full-clean/4441740#4441740)
1. [django validation](https://stackoverflow.com/questions/13036315/correct-way-to-validate-django-model-objects/13039057#13039057)
1. [clean() vs save()](https://github.com/jamescooke/django-clean-vs-save/blob/master/clean_vs_save/clean_vs_save/tests.py)


## Speeding up Seleniunm notes 
1. [Make selenium faster](https://www.lucidchart.com/techblog/2015/07/21/selenium-7-things-you-need-to-know-2/)
1. [How to make Selenium tests reliable](https://news.ycombinator.com/item?id=9925951)
1. [Speeding up Selenium](https://helpfulsheep.com/2017-05-24-speeding-up-selenium/)
1. [Tips on to improve speed of Selenium tests execution](https://letztest.blogspot.com/2016/03/10-tips-for-improving-speed-of.html)
1. [More about it](https://seleniumjava.com/2015/12/12/how-to-make-selenium-webdriver-scripts-faster/)


## Docker arg and env 
1. [Docker arg and env pasisng by](https://vsupalov.com/docker-build-pass-environment-variables/)
1. [Condition using build arguments this will invalid the cache](https://medium.com/@tonistiigi/advanced-multi-stage-build-patterns-6f741b852fae)
1. [More about it](https://stackoverflow.com/questions/43654656/dockerfile-if-else-condition-with-external-arguments/60820156#60820156)
1. [Visual explaination](https://vsupalov.com/docker-arg-vs-env/)
1. [Even more](https://vsupalov.com/docker-arg-env-variable-guide/)
1. [You can not get enough, can't you?](https://www.dev-diaries.com/social-posts/conditional-logic-in-dockerfile/)
1. [Oh come one, you still want more?](https://phoenixnap.com/kb/docker-environment-variables)
1. [Env files deep dive](https://dimmaski.com/env-files-docker/)
1. [Official docs](https://docs.docker.com/engine/reference/builder/#env)
1. [Stackoverflow answer](https://stackoverflow.com/questions/35093256/how-do-i-pass-an-argument-along-with-docker-compose-up)