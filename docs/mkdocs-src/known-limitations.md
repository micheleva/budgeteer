Known limitations
=================
The project underwent a significant rework, transitioning from the original version that relied on Django templating to a React-based frontend. 

Because of that, and because of the feature creep that has significantly impacted this project, some dangling/dead/duplicated code still lives in the repo. 

I have other priorities at the moment, so please be patient if you find any issue.

- There is no email confirmation step in the registration, this is intentional and it is not in the roadmap, or in the backlog at the moment.
- There is no reset update or password flow implemented, this is intentional and it is not in the roadmap, or in the backlog at the moment. (Use the [python manage.py changepassword USERNAME command ](https://docs.djangoproject.com/en/3.2/ref/django-admin/#changepassword)(or `django-admin changepassword USERNAME`) or directly [edit the db through the orm](https://docs.djangoproject.com/en/3.2/topics/auth/default/#changing-passwords))
- The app is monolingual (English), localization and internationalization are not in the roadmap, or in the backlog at the moment.
- The local currency is `JPY`, and some models only accept integer values for (some) money related fields, any improvement is not in the scope of `v1.X` releases. This is tracked in `BACKEND-FIX-18`
- Currenly only `USD` and `EUR` are partially supported as foreign currencies. No more currencies are in the roadmap, or in the backlog at the moment.
- The foreign currencies are actually sharing the same rate (mulitple foreign currencies feature is not fully implemented yet)
- `Accounts` can not be deleted or edited, this is intentional and it is not in the roadmap, or in the backlog at the moment.
- Any type of Category (i.e. `Category`, `Income Category`, `MonthlyBalanceCategory`, `SetAside Category`) can not be deleted, this is intentional and it is not in the roadmap, or in the backlog at the moment.
- Any uncaught exception (e.g. "Division by zero", etc. ) will cause react to fail horribly. This is tracked in `FRONTEND-FIX-140`
- The frontend dependencies file is likely containing libraries not used anymore. Cleaning it up is not in the roadmap, or in the backlog at the moment.
- Automatically importing dividends from third party APIs generates a lot of unnecessary calls (i.e. one per dividend). This might be addressed in the future.
- the project is using `docker-composer` command (known as V2), so using the `docker compose` command (V3) _might_ not work out of the box. Having said that, on `AWS2023 Linux` installing `docker-compose` through `pip` (as done in the `provision.yaml` ansible playbook) seems to work out of the box! Tracked in [#42](https://gitlab.com/micheleva/budgeteer/-/issues/42)
- For more details about know bugs, details about which features are half implemented and which are not fully working yet, details about why the code ended up being like it is, please refer to [stretch-features.md](./stretch-features.md) and [corners-cut-and-dangling-code.md](./corners-cut.md).
- expenses-by-category and expense aggregate are not showing data on first load: it seems it's not getting the cateory when mounted (do we set category id 0 on init? ...why?). This is tracked in `FRONTEND-BUG: 182`
- assets.tsx if we select "Aggregate accounts by assets", and press refresh, if refresh returns new data the table will still use the old data until we uncheck and re-check "Aggregate accounts by assets". This is tracked in `FRONTEND-BUG 119`
- when an IntegrityError in catched, we accidentlly return 201 on create. We should return 400 or 500? Same for delete and update: genrly handle the db failure, but do tell the user the operation did fail. This is tracked in `BACKEND: 321`
- multiple views, in the frontend, have their own Pulldown (<Select>)for that model's category implemented separately: unify it. This is tracked in `FRONTEND-FIX: 235`
- In the frontend code, most tables are sharing duplicated code: unify them. This is tracked in `FRONTEND-FIX: 242`
- Atm we _intentionally_ serve a single fat js file: look into webpack options on how to split the js bundle. This is tracked in `FRONTEND-FIX: 31`
    - look into using dynamic imports ?
    - Docs: https://webpack.js.org/plugins/split-chunks-plugin/
    - references: https://medium.com/naukri-engineering/mastering-webpack-splitchunks-plugin-tips-and-tricks