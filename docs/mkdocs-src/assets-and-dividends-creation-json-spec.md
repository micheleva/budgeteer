To avoid users micromanaging `Assets` and `Dividends`, these models can only be created by importing data from third party APIs.


### Demo server returning dummy data

To see how the third party API expected format for `Assets`, sping up a mock data server with `python manage.py assets-api-mock-server`.

To see how the third party API expected format `Dividends`, sping up a mock data server with `python manage.py dividends-api-mock-server`.

### Feature spec
For the data to be imported, each account should have an endpoint with the *account's name* (*not the account's id* as it would be expected: this is going to be fixed around `v1.6`).

URL for account 1: `<http|https>://<user-proided-url>/<account1.text>/<date-to-be-imported>.json`

URL for account 2: `<http|https>://<user-proided-url>/<account2.text>/<date-to-be-imported>.json`


### The formats are the following:

#### Assets:
```
[
  {"ticker": "XXXX",
   "amount": 1337, // number of assets owned
   "currency": "USD",
   "sum_current": {"USD": 420.69, "JPY": 45013.83}, // This is the current value is USD and JPY multiplied by the number of assets owned (amount)
   "sum_bought": {"USD": 4206.9, "JPY": 450138}, // This is the amount of USD and the correspective in JPY used to aquire the asset
   "sum_net": {"USD": -3786.21, "JPY": -405124.17}, // This is the difference among the two above fields
   "rate": 110.42, # currency to local currency change rate
  },
  {"ticker": "YYY",
   "amount": 42,
   "currency": "USD",
   "sum_current": {"USD": 0.0, "JPY": 420000}, // e.g 420,000 ÷42 = The frontend will infer that each of those 42 assets is currently worth 10,000 JPY each
   "sum_bought": {"USD": 0.0 "JPY": 413100},
   "sum_net": {"USD": 0.0, "JPY": 6900},
   "rate": 110.42, # currency to local currency change rate
  },
]
```

**NOTE:** the assets returned by the above endpoint will be saved to have `record_date` set to the date in the endpoint (e.g. `http://my-assets.example.com/Broker_account_1/2023-07-01.json` will have its data ingested, and each asset will have the `record_date` field set to `2023-07-01`)


#### Dividends:
```
[
  {
    "date": "2023-07-11",
    "currency": "USD",
    "ticker": "STOCK-SYMBOL", // the asset name, or symbol in case of stocks
    "amount": 240.47, // the grand amount of dividends received from this asset
    "rate": 110.19 // rate of local currency to "currency" on the day the dividend was received
  },
  {
    "date": "2023-02-11",
    "currency": "USD",
    "ticker": "STOCK-SYMBOL",
    "amount": 125.35,
    "rate": 120.19
  },
 [...]
  {
    "date": "2021-02-12",
    "currency": "USD",
    "ticker": "STOCK-SYMBOL",
    "amount": 51.66,
    "rate": 110.19
  }
]

NOTE: For local currency dividends return a json file where each item has its rate set to 1 (until v1.0), and currency set to "<local-currency>" e.g.JPY
[
  {
    "date": "2020-07-11",
    "currency": "JPY",
    "ticker": "OTHER-STOCK-SYMBOL",
    "amount": 578,
    "rate": 1
  },
  {
    "date": "2020-02-11",
    "currency": "JPY",
    "ticker": "OTHER-STOCK-SYMBOL",
    "amount": 579,
    "rate": 1 # This is going to be finalized around v1.0. i.e. even local currencies should have the correct rate for the foreign_currency
  },
[...]
]
```