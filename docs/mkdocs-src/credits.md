## Credits

[Dark Theme's license: MIT Copyright 2014-2021 Thomas Park](https://github.com/thomaspark/bootswatch/blob/v4/dist/darkly/bootstrap.css)

[Favicon generator](https://favicon.io/favicon-generator/)

[Ascii flow](https://asciiflow.com)

[Shields.io](https://shields.io/badges/static-badge)

[Nedbat gist for shields colors](https://github.com/nedbat/badge-samples)

[Nedbat gist for shields colors (source)](https://gist.github.com/nedbat/a27aaed4944c1f760a969a543fb52767)

[SQLDiagram](https://github.com/RadhiFadlillah/sqldiagram/tree/master)

[PG2MySQL Converter](https://github.com/ChrisLundquist/pg2mysql)

[d2](https://d2lang.com/tour/sql-tables/)

[mkdocs-materials](https://squidfunk.github.io/mkdocs-material/)