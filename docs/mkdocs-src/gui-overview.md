
## Dashboard

[Enlarge image](./images/dashboard.png)
![Dashboard screenshoot](./images/dashboard.png){ align=left }

## Monthly graph

=== "Monthly graphs (balances, assets)"
    [Enlarge image](./images/graph_balances.png)
    ![Assets Pie Graph screenshoot](./images/graph_balances.png){ align=left }

=== "Monthly graphs (balances, assets, goals)"
    [Enlarge image](./images/graph_balances_and_goals.png)
    ![Assets Table screenshoot](./images/graph_balances_and_goals.png){ align=left }

## Assets

=== "Assets Pie Graph"
    [Enlarge image](./images/assets_pie.png)
    ![Assets Pie Graph screenshoot](./images/assets_pie.png){ align=left }

=== "Assets Table"
    [Enlarge image](./images/assets_table.png)
    ![Assets Table screenshoot](./images/assets_table.png){ align=left }

## Dividends

[Enlarge image](./images/dividends_details.png)
![Dashboard screenshoot](./images/dividends_details.png){ align=left }

## Budgets

=== "Monthly Budgets"
    [Enlarge image](./images/monthly_budgets.png)
    ![Assets Pie Graph screenshoot](./images/monthly_budgets.png){ align=left }

=== "Yearly Budgets"
    [Enlarge image](./images/yearly_budgets.png)
    ![Assets Table screenshoot](./images/yearly_budgets.png){ align=left }
