# Provision and deploy

The repository contains some basic fat ansible playbook (1) to deploy the application to a (remote) host. 
{ .annotate }

  1.  We use `Tags` instead of `Roles`

For details about the `tag` usage, refer to tags session below.

## Prepare the environments and the inventory
Copy the sample inventory, and replace the data with your host information.

    cp deploy/inventory/sample deploy/inventory/inventory.yaml
    cp deploy/vars/sample_external_vars.yaml deploy/vars/external_vars.yaml
    cp deploy/vars/config_files/root_env.sample deploy/vars/config_files/root_env
    cp deploy/vars/config_files/app_env.sample deploy/vars/config_files/app_env
    
    vim deploy/vars/config_files/app_env # edit the variables to match your host(s), configure the app as you like
    vim deploy/vars/config_files/root_env # edit the variables to match your host(s), configure the app as you like
    vim deploy/inventory/inventory.yaml # edit the variables to match your host(s)
    vim deploy/vars/external_vars.yaml # edit the variable to match your host(s), configure the provision/deployment scripts as you like


=== ":simple-ansible: Just deploy it:exclamation:"
    ```
    cd deploy
    ansible-playbook deploy.yaml [ --tags frontend-rebuild,bundle,docker ] # Deploy the app to the remote host
    ```

=== ":simple-ansible: :material-server: Provision and deploy it:exclamation:"
    ```
    cd deploy
    ansible-playbook provision.yaml [ --tags frontend-rebuild,bundle,docker ] # Provision the remote host
    ansible-playbook deploy.yaml [ --tags bundle,s3,bakcup,cron,git,pip,docker ] # Deploy the app to the remote host
    ```

## Deploy playbook summary:

`deploy.yaml` deployes the application. Here are the tasks it does:

- Generates a new bundle for the frontend **locally**.
- Uploads the entire application (backend and frontend code, plus all the Dockerfiles, etc.) to the **remote host**.
- Performs a backup of the currently running application folder in the **remote host**.
- Deploys the containerized application using docker-compose down and docker-compose up in the **remote host**

## Provision playbook summary:

`provision.yaml` sets up a remote host to be ready for the app to be deployed. Here is what it does:

- Confirms both `root env` file and the `app env` files are present in `deploy/vars/config_files`
- Creates the directory where the app will reside in the **remote host** (and the subdirectory app as well)
- Copies the env files to the remote host
- Install docker-compose pip module so that ansible can rebuild the app in the deployment.yaml playbooks

Depending on what variables are set in `deploy/vars/external_vars.yaml`, it can also set up cron jobs for automatic database backups. 

Two backup options are available, and none, either, or both can be active simultaneously:

=== "Option 1: :simple-amazonaws: :bucket:"
    Creates a cron job to dump the database content into a .sql file and uploads it to an S3 bucket. 

    !!! warning "Do not use public buckets :rotating_light:"
        <span style="color: var(--md-code-hl-number-color); font-weight: bold;">Ensure the S3 bucket is not publicly accessible</span>, and _optionally_ enable versioning if you are interested in tracking your backup history.


=== "Option 2: :material-git:"
    Creates a cron job to dump the database content into a .sql file, version controls its content using git, and creates a commit for each change before pushing them remotely.

    !!! danger "Read carefully :rotating_light:"
        <span style="color: var(--md-code-hl-number-color); font-weight: bold;">:warning: This option requires an SSH key with write privileges on the remote repository to be on the remote host :warning:</span>

        It is essential to consider the security implications of this approach! When the private key is copied to the remote host, **there is a huge risk if the machine gets compromised.**

        To mitigate this risk, ensure that the SSH key has write privileges **only for the backup repository** and nothing else.

        It is highly recommended to restrict force push capabilities and limit the branches that the key can push to.


**Important Note:** The current implementation uses a hardcoded path for the SSH key used to push to the Git repository i.e. `/deploy/home_budget_dbdump_key`.

Please be aware that the cron job with S3 and Git options are intended as a temporary solutions and may not provide robustness and optimum security. 
**In production environment, consider implementing more reliable and secure backup mechanisms :sweat_smile:**


## Tags:


`frontend-rebuild` (`deploy.yaml` playbook only):

- Regenerate the frontend js bundle file

`bundle`:

- Remove previously created bundle locally
- Create new bundle locally, tar and gzip it
- Create the destination folder where to extract the bundle exists if if does not exist yet
- Backup the bundle extract destination folder if it exists
  ( Renames the original folder to have the current time in the name e.g. 'budgteeer' => 'budgeteer.bk.2023-06-25-23-29-01')
- Extract the bundle remotely
- Copy the app folder env file from the backup folder, to the newly extracted bundle one
- Copy the root folder env file from the backup folder, to the newly extracted bundle one
- Install pip modules to enable docker_compose module to work properly
- trigger docker-compose down and up


`backup` (`provision.yaml` playbook only):

- Copy script to dumpdb to git
- Ensure 'backup to git' crontab entry exists
- Copy script to dumpdb to s3
- Ensure 'backup to s3' crontab entry exists
- Install git to save backup to remote git repo
- Configure ssh to use ansible key for remote repo
- Copy private key to clone repo
- Confirm the git repo exists, or creates it

`git` tag will (`provision.yaml` playbook only):

- Copy script to dumpdb to git
- Ensure 'backup to git' crontab entry exists
- Install git to save backup to remote git repo
- Configure ssh to use ansible key for remote repo
- Copy private key to clone repo
- Confirm the git repo exists (in the remote host), or creates it

 `cron` tag will (`provision.yaml` playbook only):

- Copy script to dumpdb to git
- Ensure 'backup to s3' crontab entry exists
- Ensure 'backup to git' crontab entry exists

`docker` tag will:

- Install pip modules to enable docker_compose module to work properly (`provision.yaml` playbook only)
- docker-compose down
- docker-compose up (detatched)


`pip` (`provision.yaml` playbook only):

- Install pip modules to enable docker_compose module to work properly


`s3` (`provision.yaml` playbook only):

- Copy script to dumpdb to s3
- Ensure 'backup to s3' crontab entry exists

## Ansible Variables explained:

`remote_username`

**Description:** This field specifies the username used to log in to the remote host.

**Accepted values:** A string representing the username.

**Mandatory:** <span style="color: var(--md-code-hl-number-color); font-weight: bold;">Yes</span>

**Example:** `ec-user`

**Extra Information:** Please note that `ansible_user` is a reserved variable name. In the future, it may be possible to refactor this field out, as Ansible already sets the same value in the `remote_user` variable.

----

`remote_home_dir`

**Description:** This field specifies the absolute path of the remote user's home directory.

**Accepted values:** A string representing the absolute path.

**Mandatory:** <span style="color: var(--md-code-hl-number-color); font-weight: bold;">Yes</span>

**Example:** `/home/ec2-user`

**Extra Information:** Please note that this field may be refactored out in the future, as Ansible can already detect the remote user's home directory automatically.

----

`remote_app_dir`

**Description:** This field specifies the absolute path where the application will be deployed.

**Accepted values:** A string representing the absolute path.

**Mandatory:** <span style="color: var(--md-code-hl-number-color); font-weight: bold;">Yes</span>

**Example:** `/home/ec2-user/budgeteer`

----

`remote_script_dir`

**Description:** This field specifies the path where the scripts used by cron jobs should be saved.

**Accepted values:** A string representing the absolute path.

**Mandatory:** <span style="color: var(--md-code-hl-string-color); font-weight: bold;">No</span> (However, <span style="color: var(--md-code-hl-number-color); font-weight: bold;">it becomes mandatory if</span> either `backup_git_repo_url` or `s3_bucket_name` are set)

**Example:** `/home/ec2-user/scripts`

----

`backup_git_repo_url`

**Description:** This field specifies the URL of a remote Git repository where the content of the database dump will be pushed.

**Accepted values:** A string representing the Git URL.

**Mandatory:** <span style="color: var(--md-code-hl-string-color); font-weight: bold;">No</span> 

**Example:** `git@example.com:username/budgeteer_db_dump.git`

**Extra Information:** The provided URL must be a valid Git URL that supports SSH keypair authentication. The private SSH key used for pushing to this repository should be placed inside the deploy folder and named home_budget_dbdump_key (this path is currently hardcoded).

!!! danger "Read carefully :rotating_light:"

    It is essential to consider the security implications of this approach! When the private key is copied to the remote host, **there is a huge risk if the machine gets compromised.**

    To mitigate this risk, ensure that the SSH key has write privileges **only for the backup repository** and nothing else.

    It is highly recommended to restrict force push capabilities and limit the branches that the key can push to.

----

`remote_git_dump_dir`

**Description:** This field specifies the absolute path of the Git repository.

**Accepted values:** A string representing the absolute path.

**Mandatory:** <span style="color: var(--md-code-hl-string-color); font-weight: bold;">No</span> (However, <span style="color: var(--md-code-hl-number-color); font-weight: bold;">it becomes mandatory if</span> `backup_git_repo_url` is set)

**Example:** `/home/ec2-user/budgeteer_git_dbdump`

**Extra Information:** Please note that this field is required if backup_git_repo_url is set. It represents the path where the Git repository will be located on the remote host.

----

`s3_bucket_name`

**Description:** This field specifies the name of the S3 bucket where the database dump file will be uploaded.

**Accepted values:** A string representing the bucket name.

**Mandatory:** <span style="color: var(--md-code-hl-string-color); font-weight: bold;">No</span> 

**Example:** `my-s3-budgeteer-backup-name`

**Extra Information:** Ensure that the specified S3 bucket is not public to maintain data security. Apply appropriate security measures, such as restricting access permissions, enabling encryption, and implementing IAM policies, to safeguard the contents of the bucket.

----

`remote_s3_dump_dir`

**Description:** This field specifies the path where the database content is dumped before being uploaded to the S3 bucket.

**Accepted values:** A string representing the path.

**Mandatory:** <span style="color: var(--md-code-hl-string-color); font-weight: bold;">No</span>  (However, <span style="color: var(--md-code-hl-number-color); font-weight: bold;">it becomes mandatory if</span> `s3_bucket_name` is set)

**Example:** `/home/ec2-user/budgeteer_dbdump`

**Extra Information:** Conceptually, consider this folder as a staging zone (e.g. git's staging area). It serves as an intermediate location where the database content is dumped before being uploaded to the designated S3 bucket.