Title: Quickstart
# Quickstart

For more information, read the docs first :smile:

Buf you know already what to do, go ahead :fast_forward:

=== "Backend development server"

    This setup assumes you have Postgres 14 and Redis up and running locally.

    Copy the sample files, and edit them accordingly to your env. 

    For details refer to [env-explaination.md](./env-explaination.md) :blue_book:.

        cp .env.sample .env # Enable docker cache usage
        cp app/.env.sample app/.env
        vi .env # To disable docker cache usage
        vi app/.env # Edit the default values to match your env


        cd app
        python -m venv virtualenv
        source virtualenv/bin/activate
        (virtualenv) pip install -U pip setuptools
        (virtualenv) pip install poetry
        (virtualenv) poetry install --with dev [--no-root] # See https://python-poetry.org/docs/basic-usage/#installing-dependencies-only
        (virtualenv) $ python manage.py runserver [0.0.0.0:8000]


    _Optional_: populate the database with semi-random data, to test how the app feels.

        python manage.py seed-db --yes-pollute-the-db-with-demo-data-i-ve-checked-the-env-and-its-not-pointing-to-prod-db


=== "Frontend development server"

    Make sure to be using `npm 6.14` or higher versions

        npm --version
        6.14.6

    Install the dependencies

        cd frontend
        npm ci # or `npm install` (this might update the lock file)

    Make sure to change the `.env` file inside the `app` folder in order to actually use live reload in the app.

        cd frontend
        sed -iE "s/USE_WEBPACK_DEV_SERVER=(.*)/USE_WEBPACK_DEV_SERVER=y/" ../app/.env

    Run the webpack dev server to have live reloading

        npm run start # or `npm start`

    :warning: In case of error `Error: error:0308010C:digital envelope routines::unsupported` :warning:

    Refer to this [stackoverflow question](https://stackoverflow.com/questions/69692842/): this link has the [fast and dirty solution](https://stackoverflow.com/a/77714180), as well as the [correct one](https://stackoverflow.com/a/73027407)


    **In production** use a static bundle instead

        sed -iE "s/USE_WEBPACK_DEV_SERVER=(.*)/USE_WEBPACK_DEV_SERVER=n/" ../app/.env

    How to build the static bundle to be served by Nginx (or by the local django development server if you have `USE_WEBPACK_DEV_SERVER` set to `"n"`):

        npm run build

     :exclamation: In the rare event where the bundle should not have been refreshed properly, manually delete the bundle.js files and rebuild it. :exclamation: 

        rm ../app/budgets/static/js/bundle.js
        rm ../app/static/js/bundle.js
        npm run build


=== "Development docker-container"

    This setup does not require Postgres 14 nor Redis running locally.

        DOCKER_BUILDKIT=1 BUILDKIT_INLINE_CACHE=1 docker-composer -f docker-compose.yml up -d --build

    _Optional_: populate the database with semi-random data, to test how the app feels.

        docker exec -it $(docker-compose ps | grep web | cut -f1 -d" ") python manage.py seed-db --yes-pollute-the-db-with-demo-data-i-ve-checked-the-env-and-its-not-pointing-to-prod-db


=== "Monitoring services"

    This setup does not require Postgres 14 nor Redis running locally.

    To also start `redis-commander` (a web GUI service to view and edit redis data) and `postgres-admin` (a web GUI service to view and edit postgres data) in the containarized version, use the following command instead:

    ```
    DOCKER_BUILDKIT=1 BUILDKIT_INLINE_CACHE=1 docker-compose -f docker-compose.yml -f docker-compose.utilities.yml up -d --build && docker-compose logs -f
    ```
