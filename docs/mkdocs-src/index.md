Title: Home

# Budgeteer - Household Budget Management made simple 📈💰
![GPL v.3 license](https://img.shields.io/badge/license-GPL%20v3.0-brightgreen.svg "GPL v.3 License")
![Test coverage 92%](https://img.shields.io/badge/coverage-92%25-hsl(72%2C%20100%25%2C%2040%25) "Test coverage 92%")
![Python](https://img.shields.io/badge/python-ffde57?logo=python&logoColor=%233776AB "Python logo")
![React](https://img.shields.io/badge/react-black?logo=react&logoColor=%2361DAFB "React logo")
![PostgreSQL](https://img.shields.io/badge/postgresql-%234169E1?logo=postgresql&logoColor=white "PostgreSQL logo")
![redis](https://img.shields.io/badge/redis-white?logo=redis&logoColor=%23DC382D "Redis logo")
![nginx](https://img.shields.io/badge/nginx-white?logo=nginx&logoColor=%23009639 "Nginx logo")

## About the project

Budgeteer is a barebone web application for managing household budgets.

**The code of the project is available [on gitlab.com](https://gitlab.com/micheleva/budgeteer)**

The _alpha_ command line interface for this project can be found at [bcli](https://gitlab.com/micheleva/bcli).

<small>The old app, using Django templates, has been refactored into a single page app. To use the old version, checkout either [0.8](https://gitlab.com/micheleva/budgeteer/-/tags/0.8) or [1.1](https://gitlab.com/micheleva/budgeteer/-/tags/1.1) tag.</small>


## Built with
---------------------
- Python 3.12
- Django 4.2
- Django restframework 3.14
- React 17
- Postgre 14
- nginx 12.7.4

## Author
---------------------
Budgeteer was created by [Michele Valsecchi](https://gitlab.com/micheleva).


## License
---------------------
GNU General Public License v3.0

See [COPYING](./COPYING) to see the full text.


## Credits
---------------------
See [Credits](./credits.md).


## References and useful links
---------------------
See [References](./references.md).