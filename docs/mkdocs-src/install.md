title: Installation

## Prerequisites
--------------------------
- Python 3.12 or later installed
- _Optional_: Postgre 14 instance if working with the non containarized application <small>(i.e. non-containarized devevelopment environement)</small>

## Installation
--------------------------

Clone the repo

    git clone https://gitlab.com/micheleva/budgeteer

Install the requirements

    cd app
    python -m venv virtualenv
    source virtualenv/bin/activate
    (virtualenv) pip install -U pip setuptools
    (virtualenv) pip install poetry
    (virtualenv) poetry install --with dev [--no-root] # See https://python-poetry.org/docs/basic-usage/#installing-dependencies-only

## Prepare the env files
--------------------------
Copy the sample files, and edit them accordingly

    cp .env.sample .env
    cp app/.env.sample app/.env

The `.env` file in the root folder will decide whether to use docker cache feature or not.

The `.env` file inside the app folder will decide everything else.

For a comprehensive explaination of all env parameters, refer to [env-explaination.md](./env-explaination.md).

For more details on the parameters used by the frontend, plase refer to [env-explaination.md](./env-explaination.md).

**NOTE:** as the `docker-compose.yaml` is now declaring separate networks for redis and postgresql. Because of that unless your dev machine can directly reach postgre and redis, you'll get an error.

Compare the current `docker-compose.yaml` and the one in tag `0.8` for the differences (i.e. refer to the `network` declaration part)
