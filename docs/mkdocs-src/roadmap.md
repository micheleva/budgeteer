# Roadmap

Budgeteer has finally reached `1.0`, so most APIs can be considered stable.

Some corners were cut, and some features are half implemented, for more details refer to [corners-cut-and-dangling-code.md](./corners-cut.md).

Merge requests for tasks listed in the `NICE TO HAVE tasks` and in the `NICE TO HAVE tasks (very low priority)` sections of [nice-to-have-tasks.md](./stretch-features.md) might be accepted.

However please contact the mantainer before starting to work on them if you want to contribute.

PR related to tasks listed [dropped-tasks.md](./dropped-tasks.md) are very unlikely to be accepted, as the scope of the project has already exceeded the mantainer's initial expectations.

However, if contributors still really feel like implementing any of those tasks, please contact the mainteiner before starting to work on them, something might be figured out.

The changelog for each version can be found inside `CHANGELOG`, at the [root of the repo](https://gitlab.com/micheleva/budgeteer/-/blob/master/CHANGELOG).

### Criteria to consider "Budgeeter" as "done":
  - Can other developers easily understand the app?
  - Is the code clean, and DRY?
  - Is it secure and following good practices?
    (Remember, this is a one-person pet project, so let's not aim too high and never deliver!)
  - Can the app be _easily_ used by users on mobiles?
