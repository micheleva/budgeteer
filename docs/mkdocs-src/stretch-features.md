### NICE TO HAVE tasks: **8** left

- [#59](https://gitlab.com/micheleva/budgeteer/-/issues/59) (23): try out poetry to manage python dependencies
    - figure out which dependecies were a first level one, and which were not
    - see if we can bump anything while we're at it
- [#61](https://gitlab.com/micheleva/budgeteer/-/issues/61) (310): selinux should be `enforced` when provisioning remote hosts :smile:
    1. Update the deploy scripts to use AWS Linux 2023
       - This task includes `207 INFRA`: update repo to use `docker compose` v3 (i.e. `docker compose` cmd, instead of `docker-compose` cmd)
    2. Add a script to simply pull new images, instead of locally creating them. i.e. just copy the new `docker-compose.yaml` and the single `.env` file to the remove, and do `docker-compose build && docker-compose up`
    3. In order to do that, create containers and publish them from the pipelines!
- Y: Add Decimals for all other models, and make the API returns consistent values, and make the frontend handling the amounts correcly (i.e. cast string to Number, as for now the frontend does not any calculation, but only displaying them. Hence no need to worry aboutt the frontend introducing floating point errors ).
- Z: Implement MonthlyBalance create/update UI for Budgets as well (298)
    - Allow to create and update Budgets for a given month for all categories: _TODO_
    - Allow to create and update Budgets for a given year for all categories: _TODO_
    - Allow to create and update Budgets for a given category for all months in a year: _TODO_
    - Superseed `286 FRONTEND-NEW`: `MonthlyBalances` and `Budgets` multiple create/update should have a way to reference and set the latest used amount or the most used amount for each category.
    - It would be nice to have also a way to reference and be able to set budgets based on past expenses aggregate results
- W: FRONTEND: in the dashboard budgets widget, show the total usage of monthly and yearly budgets if all categories have a budget (304)
    - Note that yearly usage is not in sync with monthly usage. Fix it


**When the above tasks are done, bump the app version to 1.6**

- [#46](https://gitlab.com/micheleva/budgeteer/-/issues/46) (287): Some integration testing are calling the api to confirm that models have been created: refactor to actually use the UI to get said info. This behavior is making the gitlab CI/CD pipeline flaky.
- [#54](https://gitlab.com/micheleva/budgeteer/-/issues/54) add _integration_ tests for the spa for the following pages . (29)
    - expense : **DONE**
    - monthly balances: _TODO_
    - dividends: **DONE**
    - accounts: **DONE**
    - assets: _TODO_
    - monthly balances categories: _TODO_ (152)
    - monthly balances: _TODO_
    - setasidecategories: _TODO_
    - budgets: _TODO_
    - obligations: **DONE**
    - obligation_payment_change: _TODO_
    - spending_limits: **DONE**
    - delete assets: _TODO_
- [#58](https://gitlab.com/micheleva/budgeteer/-/issues/58) (219) **Dividends** not exising anymore in the third party api responses are not deleted from the database when we automatic import new dividends. Or, implement Dividends deletion.

**When the above tasks are done, bump the app version to 1.7**

NICE TO HAVE tasks (_very low priority_): **8** left
---

- XX: check again web/nginx Dockerfiles: atm is using 755 for static folder permission (e.g. use the same user-id group-id in both containers instead!) (99)
- 127 BACKEND-FIX: have python exception logged using logger (check if we can wrap all django routes/functions with a decorator that use logging.critical())
    - [Code](https://github.com/django/django/blob/4.2.6/django/core/handlers/exception.py#L172-L185)
    - [References](https://www.agiliq.com/blog/2017/12/configure-django-log-exceptions-production/)
- 140 FRONTEND-FIX: Prevent React from not displaying at all when unrecoverable errors are thrown. See "BUGFIX: reduce() had no initial value" commit for an example of this behaviour.
- 241 FRONTEND-FIX: handle error on deletion failure when API calls are issued from modals
- 245 FRONTEND-FIX: as the form need to be displayed for react-form-hook to bind correctly, we can not display a spinner while loading the necessary information to render the UI
    - As per this issue, most components with forms that have dependencies on other models (i.e. foreign keys to be loaded from APIs) are using a hackish way to make the react-form-hook library's bind methods to work correcly
- 256 BACKEND-FIX: Asset creation should do get_or_create in the same fashion as Asset creation does
    - atm if the user imports the same asset twice, they'll have a duplicated assets (and atm it can not be deleted)
    - update unit and functional test accordingly when changing this!
- 275 TOOLING: `backup_to_s3.sh` occasionally uploads an empty `.tar.gz` file.
    - There's no discernible pattern in the timing of the empty file uploads. We only have one cronjob running at 00:00 and 12:00, yet the empty .tar.gz files are randomly uploaded at different times throughout the day.

**When the above tasks are done, bump the app version to 1.8**
- ZZ: budgets yearly used is not linked to the "used" sum of budget monthly
- ZZZ: Account and Monthly Budget Balance should be merged
- ZZZZ: Rename Obligation to debt, or add a note in the docs
- ZZZZZ: Make all models use Decimals
- ZZZZZZ: seed-db is not working anymore
