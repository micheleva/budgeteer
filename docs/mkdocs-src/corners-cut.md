# Corners I've cut writing this app, and code that is likely to be dead or need some fixes


Some money amounts are stored as integers, while dividends and assets use floats instead of Decimals. Initially, this was a weekend project, and I didn't anticipate needing to handle foreign currencies (with decimals).


This app was created as an exercise to practice TDD. However as feature creep kicked in, TDD was promply abandoned, hence the low test coverage. I'm trying to add test every time I touch old code, but it will take some time to go back to 95%+ coverage.


In the Ansible playbook we're pushing private keys to a remote/production machine. Make sure that said key has only write permission to the git repository used to store the db dump git repo, and to nothing else!!!


The object level permission were implemented from scratch, as it did not occourred to me that overwriting `django.contrib.auth.mixins.PermissionRequiredMixin` could have been enough. Having said that, this is not irriversible, and it could be fixed later. Using said mixin, and to extend BasePermission, and to have it listed in `permission_classes` for `django_restframework` class based views... would indeed allow a lot of code to be deleted!


The pagination class used for the expenses views is going to hit performance issues after the db has a few millions rows.
See:
 - https://pganalyze.com/blog/pagination-django-postgres
 - https://www.slideshare.net/MarkusWinand/p2d2-pagination-done-the-postgresql-way
 - https://use-the-index-luke.com/no-offset
 - https://tinystruggles.com/tech/django_slowness_traps/


