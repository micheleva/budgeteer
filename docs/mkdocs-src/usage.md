## Seed db with demo data

Populate the local database

    python manage.py seed-db --yes-pollute-the-db-with-demo-data-i-ve-checked-the-env-and-its-not-pointing-to-prod-db

Populate the database for dockerized apps

    docker exec -it $(docker-compose ps | grep web | cut -f1 -d" ") python manage.py seed-db --yes-pollute-the-db-with-demo-data-i-ve-checked-the-env-and-its-not-pointing-to-prod-db

Completely delete locally seeded data

    python manage.py python manage.py wipe-user-id --yes-permanently-remove-data-from-the-db-i-ve-checked-the-env-and-its-not-pointing-to-prod-db --user-id <user-id-to-be-deleted>

Completely delete dockerized app seeded data

    docker exec -it $(docker-compose ps | grep web | cut -f1 -d" ") python manage.py --yes-permanently-remove-data-from-the-db-i-ve-checked-the-env-and-its-not-pointing-to-prod-db --user-id <user-id-to-be-deleted>

## Backup data
---------------------
Dump the postgres content to a file

     docker-compose exec web sh ./backup.sh
     docker cp $(docker-compose ps | grep web | cut -f1 -d" "):/home/app/web/data_only.sql .

## Restore data
---------------------
Move the backup file to web container, and restore it

    docker cp data_only.sql budgeteer-db-1:/home/app/web/data_only.sql
    docker-compose exec web /home/app/web/restore.sh /home/app/web/data_only.sql
    # or 
    # docker-compose exec web bash -c "BACKUP_PATH=/home/app/web/data_only.sql /home/app/web/restore.sh"

For details about how to backup/restore the db refer to [self-memo.md](./self-memo.md).

## Simulate Third Party API Assets endpoint
---------------------

To see how the third party API expected format for `Assets`, sping up a mock data server with

```sh
python manage.py assets-api-mock-server

# From a different terminal

# Note: the name of the json file can be anything

# Return mixed data: malformed and well formed
curl http://localhost:13505/assets/mixed-data/x.json

# Return only malformed data
curl http://localhost:13505/assets/malformed_data_only/x.json

# Return only well formed data
# Any path that does not contain "mixed-data" or "malformed_data_only" will do
curl http://localhost:13505/assets/well-formed-data/x.json
```

## Simulate Third Party API Dividends endpoint
---------------------

To see how the third party API expected format `Dividends`, sping up a mock data server with

```sh
python manage.py dividends-api-mock-server

# From a different terminal

# Note: The JSON file can have any name, as long as the URL ends with ".json".

# Return mixed data: malformed and well formed
curl http://localhost:13504/dividends/mixed-data/x.json

# Return only malformed data
curl http://localhost:13504/dividends/malformed_data_only/x.json

# Return only well formed data
# Any path that does not contain "mixed-data" or "malformed_data_only" will do
curl http://localhost:13504/dividends/well-formed-data/x.json
```