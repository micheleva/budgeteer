Title: Environments

# Environment summary

`UA_BLOCK_FEATURE`
---
`Description`: Blocks all User Agents other than the allowed ones

`Accepted values`: `true`, `false`

`Default`: `false`

`Mandatory`: <span style="color: var(--md-code-hl-number-color); font-weight: bold;">Yes</span>

`Extra info`: If true, only the UA regex specified in `nginx/useragent.rules` will be allowed to access the app


`DJANGO_DEBUG_MODE`
---
`Description`: Controls whether or not Django is in debug mode

`Accepted values`: `"y"`

`Extra info`: anything other than `"y"` will set it to false (including Y,YES,True, or any other Python truthy value).

When `DEBUG` is set to `True`, Django will display detailed error pages when an exception is raised. This can be helpful for debugging, but it can also expose sensitive information about your environment, so it should only be used in development and not in production.

One of the main features of debug mode is the display of detailed error pages. If your app raises an exception when `DEBUG` is True, Django will display a detailed traceback, including a lot of metadata about your environment, such as all the currently defined Django settings (from settings.py).

When `DEBUG` is True and `SITENAME` is empty, the host is validated against [`.localhost`, `127.0.0.1`, `[::1]`].
`DJANGO_DEBUG_MODE` will also display the Django Debug toolbar, and enable the BrowsableAPIRenderer, which will prettify the API response if accessed from browser

`Mandatory`: <span style="color: var(--md-code-hl-string-color); font-weight: bold;">No</span>, <span style="color: var(--md-code-hl-number-color); font-weight: bold;">(However, if you do **NOT** set `SITENAME`, this becomes required).</span>

`Extra info`: see [Django settings docs](https://docs.djangoproject.com/en/4.2/ref/settings/#debug)


`SIGNUP_DISABLED`
---
`Description`: Toggles the ability to disable new user signups in the application.

`Type`: String

`Accepted values`: `True`, `true`, `False`, `false`

`Mandatory`:  <span style="color: var(--md-code-hl-string-color); font-weight: bold;">No</span>

`Extra info`: If not set or set to any value other than `True` or `true`, the application will allow user signups by default. This variable is particularly useful for scenarios like maintenance periods, invite-only systems, or restricting access to specific users. It can also be used to harden the application when exposing it to the internet, ensuring tighter control over who can register.


`DJANGO_SETTINGS_MODULE`
---
`Description`: this is needed to run scripts outside Django. Not needed for this app, as we're using Django Command to populate the DB

`Type`: String

`Accepted values`: `budgeteer.settings`

`Mandatory`:  <span style="color: var(--md-code-hl-string-color); font-weight: bold;">No</span>

`Extra info`: reference in case we want to execute bash script invoking python, outside django initialization later on


`USE_WEBPACK_DEV_SERVER`
---
`Description`: Enables the use of the webpack-dev-server during development

`Type`: String

`Accepted values`: `"y"`, `"n"`  `TODO check other js falsy values`

Default: `"n"`

`Mandatory`:  <span style="color: var(--md-code-hl-string-color); font-weight: bold;">No</span>

`Extra info`: when set to `"y"`, the bundle for the frontend has to be served at http://localhost:4000/bundle.js

`SITENAME`
---
`Description`: Domain name accepted by django

`Type`: String

`Mandatory`: <span style="color: var(--md-code-hl-number-color); font-weight: bold;">Yes</span>, (However, <span style="color: var(--md-code-hl-string-color); font-weight: bold;">it is not needed if `DJANGO_DEBUG_MODE` is set to `"y"` </span> and you're accessing Budgeteer from the same system - e.g. localhost).


`Extra info`: The value of this env is set to django's ALLOWED_HOSTS. When DEBUG is True and SITENAME is empty, the host is validated against [`.localhost`, `127.0.0.1`, `[::1]`].

`CURRENCY`
---
`Description`: Symbol of the currency used as local currency for all the users

`Type`: String

`Accepted values`: JPY, EUR // `TODO check if other currencies will work as well`

`Mandatory`: <span style="color: var(--md-code-hl-number-color); font-weight: bold;">Yes</span>

`Extra info`: if this should ever eventually moved to a per user basis settings, it will happen in v2

`DJANGO_SECRET_KEY`
--
`Description`: This value is the key to securing signed data in the django application

`Type`: string

`Mandatory`: <span style="color: var(--md-code-hl-number-color); font-weight: bold;">Yes</span>

`Extra info`: see [Django docs](https://docs.djangoproject.com/en/4.2/ref/settings/#std-setting-SECRET_KEY)

`EXCHANGE_RATE`
---
`Description`: the <foreign-currency> to CURRENCY rate

`Type`: Integer

`Mandatory`: <span style="color: var(--md-code-hl-number-color); font-weight: bold;">Yes</span>

`Extra info`: if this should ever eventually changed to be Decimal, it will happen in 0.8

`SPENDING_LIMITS`
---
`Description`: This feature only exists in the React Single page application, it monitors the current expenses. These settings are shared for all users.

`Type`: 5 fields comma delimited string, furtherly separated by semicolumns

`Example`: `1200,JPY,30,Food,coffee;7000,JPY,30,Sport,equipment`

`Example explaination`: Set two spending limits: 

- one of 1200 JPY for the last 30 days in the category Food for expenses with a node matching "coffee"
- and one of 7000 JPY for the last 30 days in category named Sport for expenses with a note matching "equipment"

`Extra info`: the category `note` field is matched with a _case insestive regex_ as in `"where note ILIKE '%xxx%'"`. 

`Known issue`: **Spending limits are global in the budgeteer instance: i.e. they are shared for ALL users.** It means that user `A` wants a spending limits on their category `Food`, any user using the same budgeteer instance, having a category named Food as well, will see the same spending limits. This issue is being tracked in `211 BACKEND-FIX`

`Mandatory`:  <span style="color: var(--md-code-hl-string-color); font-weight: bold;">No</span>

`DARK_THEME`
---
`Description`: This work both in the django app, and in the React frontend app as well

`Type`: string

Allowed values: "y", "n"

`Mandatory`:  <span style="color: var(--md-code-hl-string-color); font-weight: bold;">No</span>

`Extra info`: the value casted as bool() in python before being fed to backend and frontend

`DATABASE`
---
`Description`: Dialect Django will use to create queries

`Type`: string

Example: postgres

`Mandatory`: <span style="color: var(--md-code-hl-number-color); font-weight: bold;">Yes</span>

`SQL_HOST`
---
`Description`: Hostname of the db container

`Type`: string

`Example`: db

`Mandatory`: <span style="color: var(--md-code-hl-number-color); font-weight: bold;">Yes</span>

`Extra info`: Used in the db container, must match the name (not the hostname, the *name*) of the db service declared in docker-compose.yaml

`SQL_PORT`
---
`Description`: Port number used to use when connecting to the database

`Type`: Integer

`Example`: 5432

`Allowed values`: 5432

`Mandatory`: <span style="color: var(--md-code-hl-number-color); font-weight: bold;">Yes</span>

`Extra info`: Used in the db container

`SQL_DATABASE`
---
`Description`: Database name to use when connecting to the database

`Type`: String

`Example`: budgeteer_db

`Mandatory`: <span style="color: var(--md-code-hl-number-color); font-weight: bold;">Yes</span>

`Extra info`: Used in the db container. This value has to match the one of `POSTGRES_DB`

`SQL_USER`
---
`Description`: Set the user to connect to the db. This value has to match the one of `POSTGRES_USER`

`Type`: String

`Mandatory`: <span style="color: var(--md-code-hl-number-color); font-weight: bold;">Yes</span>

`Extra info`: Used in the db container. This value has to match the one of `POSTGRES_USER`

`SQL_PASSWORD`
---
`Description`: This environment variable is required for you to use the PostgreSQL image. It must not be empty or undefined. This environment variable sets the superuser password for PostgreSQL. The default superuser is defined by the `POSTGRES_USER` environment variable

`Type`: String

`Mandatory`: <span style="color: var(--md-code-hl-number-color); font-weight: bold;">Yes</span>

`Extra info`: Used in the db container. This value has to match the one of `POSTGRES_PASSWORD`

`POSTGRES_DB`
---
`Description`: This optional environment variable can be used to define a different name for the default database that is created when the image is first started. If it is not specified, then the value of POSTGRES_USER will be used. This value has to match the one of `SQL_DATABASE`

`Mandatory`: <span style="color: var(--md-code-hl-number-color); font-weight: bold;">Yes</span> // `TODO confirm whether this is correct or not`

`Example`: budgeteer_db

`Extra info`: Used in the db container, must match the corrispective value for the web container. See https://hub.docker.com/_/postgres/

`POSTGRES_USER`
---
`Description`: This optional environment variable is used in conjunction with `POSTGRES_PASSWORD` to set a user and its password. This variable will create the specified user with superuser power and a database with the same name. If it is not specified, then the default user of postgres will be used. This value has to match the one of `SQL_USER`.

`Type`: String

`Mandatory`: <span style="color: var(--md-code-hl-number-color); font-weight: bold;">Yes</span> // `TODO confirm whether this is correct or not`

`Example`: budgeteer_user

`Extra info`: Used in the db container, must match the corrispective value for the web container. See https://hub.docker.com/_/postgres/

`POSTGRES_PASSWORD`
---
`Description`: This environment variable is required for you to use the PostgreSQL image. It must not be empty or undefined. This environment variable sets the superuser password for PostgreSQL. The default superuser is defined by the POSTGRES_USER environment variable. This value has to match the one of `SQL_PASSWORD`

`Type`: String

`Mandatory`: <span style="color: var(--md-code-hl-number-color); font-weight: bold;">Yes</span>

`Extra info`: Used in the db container, must match the corrispective value for the web container. See [postgres docker image docs](https://hub.docker.com/_/postgres/)

`PGADMIN_DEFAULT_EMAIL`
---
`Description`: email to be set as login user for the admin user of the pgadmin service

`Type`: String

`Mandatory`: <span style="color: var(--md-code-hl-number-color); font-weight: bold;">Yes</span>

`Example`: admin@example.com

`PGADMIN_DEFAULT_PASSWORD`
---
`Description`: Password for the admin user of the pgadmin service

`Type`: String

`Mandatory`: <span style="color: var(--md-code-hl-number-color); font-weight: bold;">Yes</span>

`ASR_ENABLED`
---
`Description`: Whether creating expense using Automatic Speech Recognition is allowed or not

`Type`: Boolean

`Accepted values`: `True`, `False`, `true`, `false`, `y`, `n`

`Default`: `True`

`Mandatory`: <span style="color: var(--md-code-hl-string-color); font-weight: bold;">No</span>

`Extra info`: Any value that is not insensitive `True` or empty, will be considered as false. This is not set by default, and we only set inside `docker-compose-no-asr.yaml` for those who want to turn off the feature.

