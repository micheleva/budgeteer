title: Documentation

## Install documentation dependencies

    cd app
    python -m venv virtualenv
    source virtualenv/bin/activate
    (virtualenv) pip install -U pip setuptools
    (virtualenv) pip install poetry
    (virtualenv) poetry install [--with|--only] doc [--no-root] # See https://python-poetry.org/docs/basic-usage/#installing-dependencies-only
    cd ../docs/mkdocs-src

## Serve the documentation locally

    mkdocs serve --clean [-v]

## Build the documentation

    mkdocs build [--site-dir ../../public] [-v]