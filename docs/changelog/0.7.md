Version 0.7
---

**BUGFIX:**

 - 164: fix StackedBarGraph logic
 - Flips the logic, and now cash-like assets and invested assets are correctly showed in the graph.
 - Assets are correctly displayed in graphs
 - investing categories were ignored if less than 2.

**Command changes:**

 - Furtherly improve the seed command
 - Generate .env file when seeding data
 - Output .env file for the seeded user.
 - Create special goals.
 - Create special goals with seed-db, and dump the to-be-used .env file content
 - seed-db 401k is now stored as an Asset.
 - Remove hardcoded category ID 83.
 - Remove hard-coded ids from seed-db cmd
 - Complete seed-db command

**Update models and migrations:**

 - Fix Assets not being able to have zero value in current foreign field.
 - Fix Monthly Balance not being able to have zero in amount field.
 - Fix MonthlyBudget not being able to have zero in amount field.
 - Allow (local currency) assets to have 0 as foreign value.
 - Update migration to allow 0 in current_foreign_value Asset model.
 - Allow monthly budget amount to be zero
 - Improve models with more sane validators

**UI/UX fixes:**

 - Improve Monthly Balance list UI: Spinner is not moving up and down when changing month swiftly.
 - Remove extra parenthesis from frontend view.
 - Draw goals line above monthly balance in the frontend bar graph.

**Code refactoring:**

- Remove dead code.
- Remove hardcoded monthlybudget api logic
- Add defaults when envs are not set
- Print exception stracktrace inside cmd

**DOCS and README:**
 - Update README's steps to dump the data.
 - Update task_left with details on some tasks.
 - Improve Assets and Profit quirks wording.
 - Fix documentation layout
 - Update self-memo with data restore notes

See merge request !27