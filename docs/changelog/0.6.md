Version 0.6
---

**BUGFIX:**
- Few bugfixes in api.v1 backend code
- Fix minor bugs in the api/v1 backend module
- Fix minor bugs in the React frontend

**Infra changes:**
- Improve ngxing container security

**Command changes:**
- Add db-seed command
- seed-db command now create Assets and Profit objects as well
- Add wipe-user-id command to cleab the seeded data

**Update models and migrations:**
- Update Goal unique constraints

**UI/UX fixes:**
- Add one entry to the navigation menu

**Code refactoring:**
- Update .gitignore
- Use logging library instead of print
- Move User Agent block behind feature flag

**DOCS and README:**
- Document how to renewal SSL cert with the cert-bot
- Improve docs on how to access local postgre instance from container (GUI) or CLI
- Add references to accessing postgres instance from container notes in the docs
- Fix some typos