# Steps to create the lets-encrypt cert
This document is based on [let's encrypt docker compose](https://github.com/evgeniy-khist/letsencrypt-docker-compose/tree/v1)


**Note these steps are using an old version, the most recent commit has a nice script to do everything automatically**


Login into the host
---
    $ cd ~/letsencrypt-docker-compose
    $ docker volume create --name=nginx_conf
    $ docker volume create --name=letsencrypt_certs
    $ docker compose down
    $ docker compose up -d


Tail logs for certificate renewal completion
---
 source [renew_cers.dh](https://github.com/evgeniy-khist/letsencrypt-docker-compose/blob/v1/cron/renew_certs.sh)

     $ docker compose logs -f 
    evgeniy-khyst-certbot-1  | Cleaning up challenges
    evgeniy-khyst-certbot-1  | 
    evgeniy-khyst-certbot-1  | Successfully received certificate. <======================================================
    evgeniy-khyst-certbot-1  | Certificate is saved at: /etc/letsencrypt/live/<domain-name>/fullchain.pem
    evgeniy-khyst-certbot-1  | Key is saved at:         /etc/letsencrypt/live/<domain-name>/privkey.pem
    evgeniy-khyst-certbot-1  | This certificate expires on 2023-06-24. <======================================================
    evgeniy-khyst-certbot-1  | These files will be updated when the certificate renews.
    evgeniy-khyst-certbot-1  | NEXT STEPS:
    evgeniy-khyst-certbot-1  | - The certificate will need to be renewed before it expires. Certbot can automatically renew the certificate in the background, but you may need to take steps to enable that functionality. See https://certbot.org/renewal-setup for instructions.


Create directory to store the new keys
---
To be done **on the host**


    $ mdir <todays-date>-keys

Note down the certificate name suffix (if there is any)
---
    $ docker exec -it evgeniy-khyst-nginx-1 ls -lah /etc/letsencrypt/archive/<domain-name>/
    cert1.pem       chain1.pem      fullchain1.pem  privkey1.pem <========


Copy the keys out of the container to the host
---
    $ docker exec evgeniy-khyst-nginx-1 ls /etc/letsencrypt/archive/<domain-name>/cert1.pem
    $ docker cp evgeniy-khyst-nginx-1:/etc/letsencrypt/archive/<domain-name>/fullchain1.pem ./<todays-date>-keys/
    $ docker cp evgeniy-khyst-nginx-1:/etc/letsencrypt/archive/<domain-name>/cert1.pem ./<todays-date>-keys/
    $ docker cp evgeniy-khyst-nginx-1:/etc/letsencrypt/archive/<domain-name>/chain1.pem ./<todays-date>-keys/
    $ docker cp evgeniy-khyst-nginx-1:/etc/letsencrypt/archive/<domain-name>/privkey1.pem ./<todays-date>-keys/


Rename the keys if necessary
---
    $ mv cert1.pem cert.pem
    $ mv chain1.pem chain.pem
    $ mv fullchain1.pem fullchain.pem
    $ mv privkey1.pem privkey.pem


Make a tar.gz to save the keys somewhere
---
    $ tar -cvzf ./keys.tar.gz ./*pem
    ./cert.pem
    ./chain.pem
    ./fullchain.pem
    ./privkey.pem

Save the keys offline
---
To be done **from another machine**


    $ scp -o IdentitiesOnly=yes -i <key-path> <remote-user-name>@<domain-name>:/home/<remote-user-name>/letsencrypt-docker-compose/<todays-date>-keys/./keys.tar.gz .


Move the keys to budgeteer directory, and remove the tar archive from lets-encrypt-folder
----
To be done **on the host**


    $ cp keys.tar.gz ~/budgeteer/nginx/
    $ rm -rf keys.tar.gz


Backup the old keys
--
    $ cd ~/budgeteer/nginx/
    $ mkdir -p old-keys/<date-of-the-previous-key-renewal>
    $ mv ./*pem old-keys/<date-of-the-previous-key-renewal>
    $ tar -xvf keys.tar.gz 
    ./cert.pem
    ./chain.pem
    ./fullchain.pem
    ./privkey.pem


Stop lets-encrypt nginx container, and the related containers
---
    $ cd ~/letsencrypt-docker-compose/
    $ docker-compose down

Remove the tar archive from nginx folder
---
    $ cd ~/budgeteer/nginx/
    $ rm keys.tar.gz
    $ cd ..
    $ docker-compose down && docker-compose -f docker-compose.yml up -d  --build

Confirm the replacement has worked
---
    $ curl https://<domain-name> -vI
    [...]
    * ALPN: server accepted http/1.1
    * Server certificate:
    *  subject: CN=<domain-name>
    *  start date: Mar 26 06:08:07 2023 GMT
    *  expire date: Jun 24 06:08:06 2023 GMT <=======
    *  subjectAltName: host "<domain-name>" matched cert's "<domain-name>"
    *  issuer: C=US; O=Let's Encrypt; CN=R3
    *  SSL certificate verify ok.
    * using HTTP/1.1