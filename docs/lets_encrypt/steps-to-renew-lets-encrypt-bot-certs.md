# Steps to renew the certs


Stop Budgeteer
---
    $ cd <budgeteer-folder>
    $ docker-compose down


Actually run the command
---
    $ cd <letsencrypt-docker-compose-folder>
    $ docker-compose up -d
    $ docker-compose run --rm --no-TTY --entrypoint certbot certbot renew --no-random-sleep-on-renew


Create directory to store the new keys
---
To be done **on the host**

    $ mdir <todays-date>-keys


Note down the certificate name suffix (if there is any)
---
    $ docker exec -it evgeniy-khyst-nginx-1 ls -lah /etc/letsencrypt/archive/<domain-name>/
    cert1.pem       chain1.pem      fullchain1.pem  privkey1.pem <========


Copy the keys out of the container to the host
---
    $ docker exec evgeniy-khyst-nginx-1 ls /etc/letsencrypt/archive/<domain-name>
    $ docker cp evgeniy-khyst-nginx-1:/etc/letsencrypt/archive/<domain-name>/fullchain<n>.pem ./<todays-date>-keys/
    $ docker cp evgeniy-khyst-nginx-1:/etc/letsencrypt/archive/<domain-name>/cert<n>.pem ./<todays-date>-keys/
    $ docker cp evgeniy-khyst-nginx-1:/etc/letsencrypt/archive/<domain-name>/chain<n>.pem ./<todays-date>-keys/
    $ docker cp evgeniy-khyst-nginx-1:/etc/letsencrypt/archive/<domain-name>/privkey<n>.pem ./<todays-date>-keys/


Rename the keys if necessary
---
    $ mv cert1.pem cert.pem
    $ mv chain1.pem chain.pem
    $ mv fullchain1.pem fullchain.pem
    $ mv privkey1.pem privkey.pem


Make a tar.gz to save the keys somewhere
---
    $ tar -cvzf ./keys.tar.gz ./*pem
    ./cert.pem
    ./chain.pem
    ./fullchain.pem
    ./privkey.pem


Save the keys offline
---
Steps to be done **from another machine**

    $ scp -o IdentitiesOnly=yes -i <key-path> <remote-user-name>@<domain-name>:/home/<remote-user-name>/letsencrypt-docker-compose/<todays-date>-keys/./keys.tar.gz .


Move the keys to budgeteer directory, and remove the tar archive from lets-encrypt-folder
---
Steps to be done **on the host**

    $ cp keys.tar.gz <path-to-budgeteer>/nginx/
    $ rm -rf keys.tar.gz

Backup the old keys
---
    $ cd <path-to-budgeteer>/nginx/
    $ mkdir -p old-keys/<date-of-the-previous-key-renewal>
    $ mv ./*pem old-keys/<date-of-the-previous-key-renewal>
    $ tar -xvf keys.tar.gz 
    ./cert.pem
    ./chain.pem
    ./fullchain.pem
    ./privkey.pem


Stop lets-encrypt nginx container, and the related containers
---
    $ cd ~/letsencrypt-docker-compose/
    $ docker-compose down


Remove the tar archive from nginx folder
---
    $ cd ~/budgeteer/nginx/
    $ rm keys.tar.gz
    $ cd ..
    $ docker-compose down && docker-compose -f docker-compose.yml up -d  --build


Confirm the replacement has worked
---
    $ curl https://<domain-name> -vI
    [...]
    * ALPN: server accepted http/1.1
    * Server certificate:
    *  subject: CN=<domain-name>
    *  start date: Mar 26 06:08:07 2023 GMT
    *  expire date: Jun 24 06:08:06 2023 GMT <=======
    *  subjectAltName: host "<domain-name>" matched cert's "<domain-name>"
    *  issuer: C=US; O=Let's Encrypt; CN=R3
    *  SSL certificate verify ok.
    * using HTTP/1.1