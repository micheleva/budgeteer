sudo yum update -y
# Yes... we're piping a remote script directly into bash
# TODO: fix this horror
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
nvm install 16
nvm use 16
sudo curl -L https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m) -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose
cd budgeteer/frontend/
npm install
npm run build

sudo usermod -aG docker $USER
sudo systemctl enable docker.service --now
docker-compose down && docker-compose -f docker-compose.yml up -d  --build

aws configure
# TODO: Create docker-compose systemd unit, enable it and start it <====