## Related Issue
Link to the related issue(s) if any.

## Details:
---
- Close XX

## Type of changes
- [ ] Bug fix (non-breaking change fixing an issue)
- [ ] New feature (non-breaking change adding functionality)
- [ ] Refactoring (improving code structure without modifying functionality)
- [ ] **Breaking change (a fix, addition, or removal that would change an existing functionality)**
- [ ] New version Release

## Implementation Checklist
- [ ] My code follows the code style of this project
- [ ] My change requires a change to the documentation
- [ ] I have updated the documentation accordingly
- [ ] I have added tests to cover my changes
- [ ] (New features and breaking changes) I have added new functional tests, or updated the existing one accordingly
- [ ] All new and existing tests are passing
- [ ] I have updated the CHANGELOG accordingly

## Additional Notes
Add any other relevant information about the merge request here.