# Summary
(Summarize the bug encountered concisely)

## Affected Version

- Version: X.X
- Commit: XXXX

## Environment

- **OS:** 
- **Docker:** 
- **Docker Compose:** 

## Must Do Before Opening a Bug

- [ ] I have checked `docs/mkdocs-src/nice-to-have.md`, and the issue is not listed there
- [ ] I have checked the `docs/mkdocs-src/known-limitations.md`, and the issue is not listed there
- [ ] (if this happens only in containerized environments) I confirm I have not edited any Dockerfile
- [ ] (if this is a front-end bug) I confirm I am using the most up-to-date version of the bundle
- [ ] (if this is a front-end bug) I have rebuilt the bundle after checking the above check-box. Yes, I really did it!
- [ ] (if this is a front-end bug) I have also confirmed that the back-end is using the bundle (`USE_WEBPACK_DEV_SERVER=y`), and I have stopped and restarted the development server (or re-built the web container) after rebuilding the bundle
- [ ] Is your bug title as clear as possible?
- [ ] Is the description as concise as possible?

## How Reproducible
- 

## Steps to Reproduce

1. 
2. 
3. 

## Actual Behavior
- 

## Expected Behavior
- 

## Additional Info
- 

## Is This a Regression?
- Yes/No

## Possible Fix
- 
