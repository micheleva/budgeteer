# Summary
(Summarize the bug encountered concisely)

## Affected Version

- Version: X.X
- Commit: XXXX

## Environment

- **OS:** 
- **Docker:** 
- **Docker Compose:** 

## Must Do Before Opening a Bug

- [ ] I have checked `docs/mkdocs-src/nice-to-have.md`, and the issue is not listed there
- [ ] I have checked the `docs/mkdocs-src/known-limitations.md`, and the issue is not listed there
- [ ] (if this happens only in containerized environments) I confirm I have not edited any Dockerfile
- [ ] (if this is a front-end bug) I confirm I am using the most up-to-date version of the bundle
- [ ] (if this is a front-end bug) I have rebuilt the bundle after checking the above check-box. Yes, I really did it!
- [ ] (if this is a front-end bug) I have also confirmed that the back-end is using the bundle (`USE_WEBPACK_DEV_SERVER=y`), and I have stopped and restarted the development server (or re-built the web container) after rebuilding the bundle
- [ ] Is your bug title as clear as possible?
- [ ] Is the description as concise as possible?

## Feature Description
(Provide a clear and detailed description of the proposed feature)

## Use Case
(Describe the specific use case or problem this feature would address)

## Expected Behavior
(Explain how you expect the feature to work)

## Proposed Implementation
(If you have ideas on how to implement this feature, describe them here)

## Alternatives Considered
(Describe any alternative solutions or features you've considered)

## Benefits
(Explain the benefits this feature would bring to users and/or the project)

## Potential Drawbacks
(Discuss any potential drawbacks or challenges in implementing this feature)

## Additional Context
(Add any other context, screenshots, or examples about the feature request here)

## Is This Related to an Existing Issue?
 - Yes/No (If yes, please link the related issue)

## Are You Willing to Contribute to This Feature?
Yes/No (If yes, please indicate your level of commitment)