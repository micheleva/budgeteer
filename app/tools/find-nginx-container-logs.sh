#!/bin/bash

# Default docker working directory
docker_containers_dir="/var/lib/docker/containers"

# Find all config.v2.json files
config_files=$(find "$docker_containers_dir" -name 'config.v2.json')

for config_file in $config_files; do
    # Get the folder containing the config file
    container_folder=$(dirname "$config_file")
    # Extract the container ID
    container_id=$(basename "$container_folder")

    # Check if this folder contains an nginx container data or not
    if [ "$(jq -r '.Config.Hostname' "$config_file")" = "nginx" ]; then
        # If the container failed to build, it might not have logs: proceed only if the log file exists
        log_file="$container_folder/$container_id-json.log"
        if [ -e "$log_file" ]; then
            echo -e "\033[1;32mNginx container found\033[0m with ID: \033[1;34m$container_id\033[0m"
            echo -e "Path \033[1;32m $log_file \033[0m"
            echo "IP with most access (NOTE: this does include real users as well)"
            echo -e "\033[4;31mInstead of passing this data to fail-to-ban, feed it with raw nginx-log, and ban IPs with too many 400\033[0m"
            echo -e "\033[4;34mSee docs/investigate-bot-access.txt for bots access patterns\033[0m"
            echo "$(jq --raw-output '.log' $log_file | sed '/^$/d' | awk '{print $1}' | sort | uniq -c | grep -vF -e docker-entrypoint.sh -e 127.0.0.1 | awk '$1 >= 10' | sort -nr)"
            jq --raw-output '.log' "$log_file" | sed '/^$/d' | awk '{print $7}' | grep -vE '/api|/old|/accounts|/spa|/static' | while IFS= read -r line; do
                echo "$line" >> filtered_access.log
            done

        fi
    fi
done


## raw NGINX log without empty lines
# jq --raw-output '.log' $target_file  | sed '/^\s*$/d'
## most common IP
#jq --raw-output '.log' $target_file  | sed '/^\s*$/d' | awk '{print $1}' | sort | uniq -c | grep -v docker-entrypoint.sh | sort -nr
## most common IP, filter out those with less than 10 hits
#jq --raw-output '.log' $target_file | sed '/^$/d' | awk '{print $1}' | sort | uniq -c | grep -v docker-entrypoint.sh | awk '$1 >= 10' | sort -nr
