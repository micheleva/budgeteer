#!/bin/sh
export COVERAGE_RCFILE=./tools/.coveragerc
coverage erase
coverage run --concurrency=multiprocessing manage.py test --no-input --parallel=3
coverage combine
coverage html -d coverage_html
coverage report
