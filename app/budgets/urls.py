# Copyright: (c) 2019, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from django.urls import include
from django.urls import path
from django.urls import re_path
from django.templatetags.static import static
from django.views.generic.base import RedirectView

from budgets import views

app_name = "budgets"  # pylint: disable=C0103; # noqa

urlpatterns = [
    # Until we remove the old non-react pages let's keep both / and /spa
    # as entry point for the react app
    path("", views.spa, name="home"),
    re_path("app.*", views.spa, name="spa"),
    # TODO: fix me, this is an horrible hack for favico
    re_path(".*favicon.ico", RedirectView.as_view(url=static("favicon.ico"))),
    path("landing_page", views.landing_page, name="landing_page"),
]
