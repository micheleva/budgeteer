# Copyright: (c) 2019, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
# pylint: disable=C0115,W0223,E0602,C0301,R0903,E1101
from decimal import Decimal
import os
import datetime

from django.conf import settings

from rest_framework import serializers
from rest_framework.exceptions import MethodNotAllowed

from budgets.models import Account
from budgets.models import Asset
from budgets.models import Budget
from budgets.models import Category
from budgets.models import Expense
from budgets.models import Goal
from budgets.models import Income
from budgets.models import IncomeCategory
from budgets.models import MonthlyBalance
from budgets.models import MonthlyBalanceCategory
from budgets.models import SpendingLimit
from budgets.models import Dividend
from budgets.models import Obligation
from budgets.models import ObligationPaymentChange
from budgets.models import SavingCategory


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ["id", "text", "is_archived"]

    def validate_text(self, value):
        created_by = self.context["request"].user
        existing_category = Category.objects.filter(
            text=value, created_by=created_by
        )

        # Used on patch/put
        if self.instance:
            existing_category = existing_category.exclude(id=self.instance.id)

        if existing_category.exists():
            msg = "A Category with this text already exists for this user."
            raise serializers.ValidationError(msg)
        return value


class CategorySerializerWithOwnership(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ["id", "text", "created_by"]


class BasicExpenseSerializer(serializers.ModelSerializer):
    class Meta:
        model = Expense
        fields = ["category", "amount", "note", "date"]


class ExpenseSerializer(serializers.ModelSerializer):
    category_text = serializers.SerializerMethodField()

    class Meta:
        model = Expense
        fields = [
            "id",
            "amount",
            "category_id",
            "category_text",
            "note",
            "date",
        ]

    # Handle case where category is NULL/None
    def get_category_text(self, instance):
        category = instance.category
        if category:
            return category.text
        return None


class AggregateExpensesByMonthSerializer(serializers.ModelSerializer):
    year = serializers.SerializerMethodField()
    month = serializers.SerializerMethodField()
    total_amount = serializers.DecimalField(
        max_digits=10, decimal_places=2, coerce_to_string=False
    )

    class Meta:
        model = Expense
        fields = ("year", "month", "total_amount")

    def get_year(self, obj):
        return obj["year"]

    def get_month(self, obj):
        return obj["month"]


class AggregateExpensesByCategorySerializer(serializers.ModelSerializer):
    category_text = serializers.SerializerMethodField()
    total_amount = serializers.DecimalField(
        max_digits=10, decimal_places=2, coerce_to_string=False
    )

    class Meta:
        model = Expense
        fields = ("category_text", "total_amount")

    def get_category_text(self, obj):
        return obj["category_text"]


class MonthlyBalanceCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = MonthlyBalanceCategory
        fields = ["id", "text", "is_foreign_currency"]
        # NOTE: This is required as api module is not using ModelForms
        # hence  we need to enforce validation
        # FIXME: see how we do it inside PlainMonthlyBalanceSerializer
        # (N.B. this class was removed in a few commits before 0.9)
        # As per https://stackoverflow.com/a/56151659
        # now clean() is not called, we need to handle it manually
        extra_kwargs = {
            "text": {"required": True},
            "is_foreign_currency": {"required": True},
        }


class MonthlyBalanceCategoryBasicSerializer(serializers.ModelSerializer):
    class Meta:
        model = MonthlyBalanceCategory
        fields = ["id", "text", "is_foreign_currency"]


class IncomeCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = IncomeCategory
        fields = ["id", "text"]

    def validate_text(self, value):
        created_by = self.context["request"].user
        existing_category = IncomeCategory.objects.filter(
            text=value, created_by=created_by
        )

        # Used on patch/put
        if self.instance:
            existing_category = existing_category.exclude(id=self.instance.id)

        if existing_category.exists():
            msg = "An Income category with this text already exists for this user."
            raise serializers.ValidationError(msg)

        return value


class BasicIncomeSerializer(serializers.ModelSerializer):
    class Meta:
        model = Income
        fields = ["category", "amount", "note", "date"]


class IncomeSerializer(serializers.ModelSerializer):
    category_text = serializers.SerializerMethodField()

    class Meta:
        model = Income
        fields = [
            "id",
            "amount",
            "category_id",
            "category_text",
            "note",
            "date",
        ]

    # Handle case where category is NULL/None
    def get_category_text(self, instance):
        category = instance.category
        if category:
            return category.text
        return None


class AggregateIncomeSerializer(serializers.ModelSerializer):
    year = serializers.SerializerMethodField()
    month = serializers.SerializerMethodField()
    total_amount = serializers.IntegerField()

    class Meta:
        model = Income
        fields = ("year", "month", "total_amount")

    def get_year(self, obj):
        return obj["year"]

    def get_month(self, obj):
        return obj["month"]


# NOTE: this serializer is to be used on put,post, as category is read_only
# to prevent users to update them (we force users to delete and recreate new
# monthly balance ON PURPOSE)
class MonthlyBalanceSerializerWithNestedCategory(serializers.ModelSerializer):
    # Since category is read only we do not need to validate it
    category = serializers.ReadOnlyField()

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data["category"] = MonthlyBalanceCategoryBasicSerializer(
            instance.category
        ).data
        return data

    class Meta:
        model = MonthlyBalance
        fields = ["id", "amount", "category", "date"]
        extra_kwargs = {
            "category": {"read_only": True},
            "date": {"read_only": True},
        }

    def validate_date(self, value):
        if value.day != 1:
            year = value.year
            month = value.month
            modified_date = datetime.datetime(year, month, 1).date()
            value = modified_date
        return value

    def create(self, validated_data):
        # Prevent create operation
        raise MethodNotAllowed(
            "POST",
            detail=(
                "Creation via this serializer is not allowed."
                "Use MonthlyBalanceSerializerForCreation instead!"
            ),
        )

    # Need to make sure an account with the same date AND category does NOT already exist
    # (same category and different date is allowed)
    def validate(self, data):
        obj_date = data.get("date")
        category = data.get("category")
        created_by = self.context["request"].user
        existing_mb = MonthlyBalance.objects.filter(
            date=obj_date, category=category, created_by=created_by
        )

        # Used on patch/put
        if self.instance:
            existing_mb = existing_mb.exclude(id=self.instance.id)
        # Check if a MonthlyBalance with the same date, categody
        # and created_by already exists
        if existing_mb.exists():
            msg = (
                "A MonthlyBalance with the same date and category already"
                " exists for this user."
            )
            raise serializers.ValidationError(msg)
        return data


# IMPORTANT: This serializer is for read-only purposes and should not be used
# for creating or updating the model. Please use
# MonthlyBalanceSerializerWithNestedCategory or MonthlyBalanceSerializerForCreation instead.
class MonthlyBalanceSerializer(serializers.ModelSerializer):
    category_text = serializers.CharField(
        source="category.text"
    )  # handle when category is none
    category_is_foreign_currency = serializers.BooleanField(
        source="category.is_foreign_currency"
    )
    amount = serializers.SerializerMethodField()

    # TODO (NICE TO HAVE): this should be able to handle multiple foreign currencies (i.e. different rates)
    def get_amount(self, obj):
        # FIXME (BUGFIX-26, BUGFIX-247) TODO handle what happens if no env!
        rate = settings.EXCHANGE_RATE
        if not rate:
            rate = 1
        if obj.category.is_foreign_currency:
            return obj.amount * rate
        return obj.amount

    class Meta:
        model = MonthlyBalance
        fields = [
            "id",
            "amount",
            "category_id",
            "category_text",
            "category_is_foreign_currency",
            "date",
        ]

    def create(self, validated_data):
        # Prevent create operation
        raise MethodNotAllowed(
            "POST",
            detail=(
                "Creation via this serializer is not allowed."
                "Use MonthlyBalanceSerializerForCreation instead!"
            ),
        )

    def update(self, instance, validated_data):
        # Prevent update operation
        raise MethodNotAllowed(
            "PUT/PATCH",
            detail=(
                "Update via this serializer is not allowed."
                "Use MonthlyBalanceSerializerWithNestedCategory instead!"
            ),
        )


# NOTE: this serializer is to be used for MonthlyBalance creation requests
class MonthlyBalanceSerializerForCreation(serializers.ModelSerializer):
    class Meta:
        model = MonthlyBalance
        fields = [
            "id",
            "amount",
            "date",
            "category",
        ]  # this will ignore PK error, and call validate

    def validate_date(self, value):
        if value.day != 1:
            year = value.year
            month = value.month
            modified_date = datetime.datetime(year, month, 1).date()
            value = modified_date
        return value

    def validate_category(self, value):
        if value is None or value == "":
            raise serializers.ValidationError(
                serializers.Field.default_error_messages["required"]
            )
        query = MonthlyBalanceCategory.objects.filter(
            **{"created_by": self.context["user_id"], "id": value.id}
        )
        if not query.exists():
            raise serializers.ValidationError(
                serializers.Field.default_error_messages["required"]
            )
        return value

    # Need to make sure an account with the same date AND category does NOT already exist
    # (same category and different date is allowed)
    def validate(self, data):
        if "category" not in data or data["category"] is None:
            raise serializers.ValidationError(
                {
                    "category": serializers.Field.default_error_messages[
                        "required"
                    ]
                }
            )

        obj_date = data.get("date")
        category = data.get("category")
        created_by = self.context["user_id"]
        existing_mb = MonthlyBalance.objects.filter(
            date=obj_date, category=category, created_by=created_by
        )

        # Used on patch/put
        if self.instance:
            existing_mb = existing_mb.exclude(id=self.instance.id)

        # Check if a MonthlyBalance with the same date, categody, and created_by already exists
        if existing_mb.exists():
            msg = "A MonthlyBalance with the same date and category already exists for this user."
            raise serializers.ValidationError(msg)
        return data


class MonthlyBalanceSerializerForCreateUpdate(serializers.ModelSerializer):
    class Meta:
        model = MonthlyBalance
        # This serializer does not use the primary key to update the model
        fields = ["amount", "date", "category"]

    def validate_date(self, value):
        if value.day != 1:
            year = value.year
            month = value.month
            modified_date = datetime.datetime(year, month, 1).date()
            value = modified_date
        return value

    def validate_category(self, value):
        if value is None or value == "":
            raise serializers.ValidationError(
                serializers.Field.default_error_messages["required"]
            )
        query = MonthlyBalanceCategory.objects.filter(
            **{"created_by": self.context["user_id"], "id": value.id}
        )
        if not query.exists():
            raise serializers.ValidationError(
                serializers.Field.default_error_messages["required"]
            )
        return value

    def create(self, validated_data):
        obj, created = MonthlyBalance.objects.update_or_create(
            date=validated_data["date"],
            category=validated_data["category"],
            created_by=validated_data["created_by"],
            # We must NOT filter by amount, because if we're updating, the query will not match
            # the existing value, and on creation, it will fail with a
            # django.db.utils.IntegrityError: "duplicate key value violates
            # unique constraint 'monthly-balance-date-unique'".
            # However, we NEED to include the amount value in defaults, otherwise, we will get
            # a django.db.utils.IntegrityError: "null value in column 'amount' of relation
            # 'budgets_monthlybalance' violates not-null constraint"
            defaults={"amount": validated_data["amount"]},
        )
        # Let the view know whether this was an update or a creation, by returning
        # the 'created' boolean. Django will set the response code accordingly (201 or 200)
        obj.created = created
        return obj


class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ("id", "text", "credit_entity", "is_foreign_currency")

    # Need to make sure an account with the same text AND credity_entity do NOT already exist for this user
    # (same text and different credit_entity is allowed)
    def validate(self, data):
        text = data.get("text")
        credit_entity = data.get("credit_entity")
        created_by = self.context["request"].user
        existing_account = Account.objects.filter(
            text=text, credit_entity=credit_entity, created_by=created_by
        )

        # Used on patch/put
        if self.instance:
            existing_account = existing_account.exclude(id=self.instance.id)

        # Check if an Account with the same text, credit_entity, and created_by already exists
        if existing_account.exists():
            msg = "An Account with the same text AND credit_entity already exists for this user."
            raise serializers.ValidationError(msg)
        return data


class RawAccountSerializer(serializers.Serializer):
    id = serializers.IntegerField(source="account_id")
    text = serializers.CharField(source="account_text")


class AccountSeriazlierMinimalFields(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ("id", "text")
        # Do not include "credit_entity", "is_foreign_currency"


class AssetSerializerWithAccount(serializers.ModelSerializer):
    account = AccountSeriazlierMinimalFields()

    class Meta:
        model = Asset
        fields = (
            "id",
            "account",
            "amount",
            "text",
            "foreign_currency",
            "current_foreign_value",
            "current_local_value",
            "bought_price_foreign",
            "bought_price_local",
            "net_loss_foreign",
            "net_loss_local",
            "record_date",
            "rate",
        )


class AssetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Asset
        fields = (
            "id",
            "account",
            "amount",
            "text",
            "foreign_currency",
            "current_foreign_value",
            "current_local_value",
            "bought_price_foreign",
            "bought_price_local",
            "net_loss_foreign",
            "net_loss_local",
            "record_date",
            "rate",
        )


class AssetSerializerWithAccountText(serializers.ModelSerializer):
    account = serializers.CharField(source="account.text")

    class Meta:
        model = Asset
        fields = (
            "id",
            "account",
            "amount",
            "text",
            "foreign_currency",
            "current_foreign_value",
            "current_local_value",
            "bought_price_foreign",
            "bought_price_local",
            "net_loss_foreign",
            "net_loss_local",
            "record_date",
            "rate",
        )


class AggregateDividendSerializer(serializers.ModelSerializer):
    year = serializers.IntegerField()
    local_total = serializers.DecimalField(max_digits=10, decimal_places=2)
    local_per_day = serializers.FloatField()
    days = serializers.IntegerField()

    class Meta:
        model = Dividend
        fields = ("year", "local_total", "local_per_day", "days")

    def to_representation(self, instance):
        data = super().to_representation(instance)
        data["local_total"] = Decimal(data["local_total"])
        return data


class AggregateDividendSerializerBySymbol(serializers.ModelSerializer):
    asset_text = serializers.CharField()
    total_local_amount = serializers.DecimalField(
        max_digits=10, decimal_places=2
    )
    total_foreign_amount = serializers.DecimalField(
        max_digits=10, decimal_places=2
    )

    class Meta:
        model = Dividend
        fields = ("asset_text", "total_local_amount", "total_foreign_amount")

    def to_representation(self, instance):
        # Convert local_total and foreign_total to Decimal
        data = super().to_representation(instance)
        data["total_local_amount"] = Decimal(data["total_local_amount"])
        data["total_foreign_amount"] = Decimal(data["total_foreign_amount"])
        return data


class DividendSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dividend
        fields = (
            "id",
            "account",
            "asset_text",
            "local_amount",
            "foreign_amount",
            "currency",
            "rate",
            "record_date",
        )

    def create(self, validated_data):
        #     # Set default values for fields not provided in validated_data <=====
        #     validated_data.setdefault('account', None)  # Set default value if needed
        #     validated_data.setdefault('currency', None)  # Set default value if needed
        #     validated_data.setdefault('rate', None)  # Set default value if needed
        dividend, _ = Dividend.objects.get_or_create(**validated_data)
        return dividend


class DividendSerializerWithAccountText(serializers.ModelSerializer):
    account_text = serializers.CharField(source="account.text")

    class Meta:
        model = Dividend
        fields = (
            "id",
            "account",
            "account_text",
            "asset_text",
            "local_amount",
            "foreign_amount",
            "currency",
            "rate",
            "record_date",
        )

    def create(self, validated_data):
        dividend, _ = Dividend.objects.get_or_create(**validated_data)
        return dividend


class GoalSerializer(serializers.ModelSerializer):
    class Meta:
        model = Goal
        fields = ["id", "amount", "text", "note", "is_archived"]
        extra_kwargs = {
            "text": {"required": True},
            "amount": {"required": True},
        }

    def validate_text(self, value):
        created_by = self.context["request"].user
        existing_goal = Goal.objects.filter(text=value, created_by=created_by)

        # Used on patch/put
        if self.instance:
            existing_goal = existing_goal.exclude(id=self.instance.id)

        # Check if a Goal with the same text and created_by already exists
        if existing_goal.exists():
            raise serializers.ValidationError(
                "A Goal with the same text already exists for this user."
            )

        return value


class ObligationPaymentChangeSerializer(serializers.ModelSerializer):
    class Meta:
        model = ObligationPaymentChange
        fields = ["id", "obligation", "new_monthly_payment", "change_date"]


class ObligationPaymentChangeSerializerForUpdate(serializers.ModelSerializer):
    obligation = serializers.PrimaryKeyRelatedField(read_only=True)

    class Meta:
        model = ObligationPaymentChange
        fields = ["id", "obligation", "new_monthly_payment", "change_date"]

    def validate_new_monthly_payment(self, value):
        if value is None:
            msg = "This field may not be null."
            raise serializers.ValidationError(msg)
        if value <= 0:
            msg = "New Monthly payment must be greater than 0."
            raise serializers.ValidationError(msg)
        return value

    def validate(self, data):
        # Check if the change date is after the end date of the obligation
        change_date = data.get("change_date", None)
        msg = "The change date cannot be after the end date of the obligation."
        if change_date:
            if change_date > self.instance.obligation.end_date:
                raise serializers.ValidationError(msg)

        # Check if the change date is after the start date of the obligation
        msg = "The change date must be after the start date of the obligation."
        if change_date:
            if change_date <= self.instance.obligation.start_date:
                raise serializers.ValidationError(msg)

        # Confirm we do not already have changes with this date
        msg = "This Obligation already has a change for this date"
        changes = self.instance.obligation.changes.all()

        # Used on patch/put
        if self.instance:
            changes = changes.exclude(id=self.instance.id)

        if len(changes.all()) > 1:
            for x in changes:
                if x.change_date == data["change_date"]:
                    raise serializers.ValidationError(msg)
        return data


class ObligationPaymentChangeSerializerForCreation(
    serializers.ModelSerializer
):
    class Meta:
        model = ObligationPaymentChange
        fields = ["id", "obligation", "new_monthly_payment", "change_date"]

    def validate_new_monthly_payment(self, value):
        if value is None:
            msg = "This field may not be null."
            raise serializers.ValidationError(msg)
        if value <= 0:
            msg = "New Monthly payment must be greater than 0."
            raise serializers.ValidationError(msg)
        return value

    def validate(self, data):
        # Check if the change date is after the end date of the obligation
        change_date = data.get("change_date", None)
        msg = "The change date cannot be after the end date of the obligation."
        if change_date:
            if change_date > data["obligation"].end_date:
                raise serializers.ValidationError(msg)

        # Check if the change date is after the start date of the obligation
        msg = "The change date must be after the start date of the obligation."
        if change_date:
            if change_date <= data["obligation"].start_date:
                raise serializers.ValidationError(msg)

        # Confirm we do not already have changes with this date
        msg = "This Obligation already has a change for this date"
        changes = data["obligation"].changes.all()
        if len(changes) > 0:
            for x in changes:
                if x.change_date == data["change_date"]:
                    raise serializers.ValidationError(msg)

        return data


class ObligationSerializer(serializers.ModelSerializer):
    changes = ObligationPaymentChangeSerializer(many=True, read_only=True)

    class Meta:
        model = Obligation
        fields = [
            "id",
            "name",
            "total_amount",
            "obligation_type",
            "monthly_payment",
            "start_date",
            "end_date",
            "payment_day",
            "changes",
            "created_by",
        ]

    def validate(self, data):
        # print('ObligationSerializer.validate() called')
        start_date = data.get("start_date")
        end_date = data.get("end_date")

        if not start_date:
            raise serializers.ValidationError("Start date is required.")
        if not end_date:
            raise serializers.ValidationError("End date is required.")
        if start_date >= end_date:
            raise serializers.ValidationError(
                "Start date must be before end" " date."
            )

        # TODO: add a check to make sure the monthly payment is not more than
        # the total. (Decide if we really do need this constraint)

        return data


class SavingCategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = SavingCategory
        fields = ["id", "text", "is_archived"]

    # Need to make sure an SavingCategory Serializer with the text does NOT
    # already exist for this user
    def validate(self, data):
        obj_text = data.get("text")
        created_by = self.context["request"].user
        existing_save_cat = SavingCategory.objects.filter(
            text=obj_text, created_by=created_by
        )

        # Used on patch/put
        if self.instance:
            existing_save_cat = existing_save_cat.exclude(id=self.instance.id)

        # Check if a MonthlyBalance with the same date, categody, and
        # created_by already exists
        if existing_save_cat.exists():
            msg = (
                "A SavingCategory with the text already exists"
                " for this user."
            )
            raise serializers.ValidationError(msg)
        return data


class BudgetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Budget
        fields = [
            "id",
            "category",
            "savingcategory",
            "year",
            "month",
            "amount",
            "allocated_amount",
        ]

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if "allocated_amount" in data and data["allocated_amount"] is not None:
            data["allocated_amount"] = Decimal(data["allocated_amount"])
        return data

    def validate_year(self, value):
        if value is None:
            msg = "This field may not be null."
            raise serializers.ValidationError(msg)
        if value < 1970:
            msg = "Year must be an integer equal or more than 1970."
            raise serializers.ValidationError(msg)
        return value

    def validate_month(self, value):
        if value is None:
            return value
        if not 1 <= value <= 12:
            msg = "Month must be between 1 and 12 (inclusive)."
            raise serializers.ValidationError(msg)
        return value

    def validate_amount(self, value):
        if value is None:
            msg = "This field may not be null."
            raise serializers.ValidationError(msg)
        if value < 0:
            msg = f"{value} should be more than 0."
            raise serializers.ValidationError(msg)
        return value

    def validate_allocated_amount(self, value):
        if value is None:
            return value
        if value < 0:
            msg = f"{value} should be more than 0."
            raise serializers.ValidationError(msg)
        return value

    def validate(self, data):
        user = self.context["request"].user
        category = data.get("category", None)
        savingcategory = data.get("savingcategory", None)
        allocated_amount = data.get("allocated_amount", None)
        year = data.get("year", None)
        month = data.get("month", None)

        if not category and not savingcategory:
            raise serializers.ValidationError(
                "Either a Category or a Saving Category must be provided."
            )

        if category and savingcategory:
            raise serializers.ValidationError(
                "Cannot specify both category and savingcategory in the same budget."
            )

        if category and allocated_amount is not None:
            raise serializers.ValidationError(
                "Cannot specify both category and allocated_amount in the same budget."
            )

        if savingcategory and allocated_amount is None:
            raise serializers.ValidationError(
                "Saving Budgets requires a 0 or greater allocated"
                " amount value."
            )

        if category:
            if not Category.objects.filter(
                id=category.id, created_by=user
            ).exists():
                msg = f'Invalid pk "{category.id}" - object does not exist.'
                raise serializers.ValidationError(msg)
        elif savingcategory:
            if not SavingCategory.objects.filter(
                id=savingcategory.id, created_by=user
            ).exists():
                msg = f'Invalid pk "{savingcategory.id}" - object does not exist.'
                raise serializers.ValidationError(msg)
            if allocated_amount is not None and allocated_amount < 0:
                msg = "allocated_amount can not be less than 0."
                raise serializers.ValidationError(msg)

        # Check for duplicates on creation
        if category:
            existing_budgets = Budget.objects.filter(
                category=category, year=year, month=month, created_by=user
            )
            msg = (
                "A Budget for this Category for this year/month (or year)"
                " already exists for this user."
            )
        else:
            existing_budgets = Budget.objects.filter(
                savingcategory=savingcategory,
                year=year,
                month=month,
                created_by=user,
            )
            msg = (
                "A Budget for this Saving Category for this year/month"
                " (or year) already exists for this user."
            )

        # Used on patch/put
        if self.instance:
            existing_budgets = existing_budgets.exclude(id=self.instance.id)

        # Check if a Budget with the same year, month, and created_by already
        # exists
        if existing_budgets.exists():
            raise serializers.ValidationError(msg)

        return data


class BudgetSerializerAllowOnlyAmountsUpdate(serializers.ModelSerializer):
    class Meta:
        model = Budget
        fields = ["amount", "allocated_amount"]
        extra_kwargs = {
            "id": {"read_only": True},
        }

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if "allocated_amount" in data and data["allocated_amount"] is not None:
            data["allocated_amount"] = Decimal(data["allocated_amount"])
        return data

    def validate_amount(self, value):
        if value is None:
            msg = "This field may not be null."
            raise serializers.ValidationError(msg)
        if value < 0:
            msg = f"{value} should be more than 0."
            raise serializers.ValidationError(msg)
        return value

    def validate_allocated_amount(self, value):
        if value is None:
            return value
        if value < 0:
            msg = f"{value} should be more than 0."
            raise serializers.ValidationError(msg)
        return value

    def validate(self, data):
        # Used on patch/put
        if self.instance:
            # Prevent setting allocated_amount fot Budgets with a Category
            if (
                self.instance.category is not None
                and data.get("allocated_amount", None) is not None
            ):
                raise serializers.ValidationError(
                    "Cannot update allocated_amount for Budget having a category."
                )

        # Since this serializer is not allowing altering year and month, the following cases are already covered
        # - Prevent update to accidentally create duplicated year only budgets with a Category
        # - Prevent update to accidentally create duplicated year/month budgets with a Category
        # - Prevent update to accidentally create duplicated year only budgets with a SavingCategory
        # - Prevent update to accidentally create duplicated year/month budgets with a SavingCategory
        return data


class SavingCategorySerializerWithID(SavingCategorySerializer):
    class Meta:
        model = SavingCategory
        fields = ["id", "text"]
        extra_kwargs = {
            "id": {"read_only": True},
            "text": {"read_only": True},
        }


class BudgetSerializerWithCategory(serializers.ModelSerializer):
    category = CategorySerializer()
    savingcategory = SavingCategorySerializerWithID()

    class Meta:
        model = Budget
        fields = [
            "id",
            "category",
            "savingcategory",
            "year",
            "month",
            "amount",
            "allocated_amount",
        ]
        extra_kwargs = {
            "id": {"read_only": True},
            "category": {"read_only": True},
            "savingcategory": {"read_only": True},
            "year": {"read_only": True},
            "month": {"read_only": True},
            "amount": {"read_only": True},
            "allocated_amount": {"read_only": True},
        }

    def to_representation(self, instance):
        data = super().to_representation(instance)
        if "allocated_amount" in data and data["allocated_amount"] is not None:
            data["allocated_amount"] = Decimal(data["allocated_amount"])
        return data


class YearlyAggregateBudgetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Budget
        # Note: this will need some changes when we support SetAsideCategories <=====================================
        fields = ["category", "savingcategory", "amount", "allocated_amount"]


class BasicSpendingLimitSerializer(serializers.ModelSerializer):
    def validate_amount(self, value):
        if value is None:
            msg = "This field may not be null."
            raise serializers.ValidationError(msg)
        if value < 0:
            msg = f"{value} should be more than 0."
            raise serializers.ValidationError(msg)
        return value

    def validate_days(self, value):
        if value is None:
            msg = "This field may not be null."
            raise serializers.ValidationError(msg)
        if value < 0:
            msg = f"{value} should be more than 0."
            raise serializers.ValidationError(msg)
        return value

    def validate_text(self, value):
        if value is None:
            msg = "This field may not be null."
            raise serializers.ValidationError(msg)
        if len(value) > 150:
            msg = f"{value} should be shorter than 150 chars."
            raise serializers.ValidationError(msg)
        # Check if the string is empty
        if not value.strip():
            msg = "This field may not be blank."
            raise serializers.ValidationError(msg)
        return value

    def validate_currency(self, value):
        if value is None:
            msg = "This field may not be null."
            raise serializers.ValidationError(msg)
        if len(value) > 3:
            msg = f"{value} should be shorter than 3 chars."
            raise serializers.ValidationError(msg)
        # Check if the string is empty
        if not value.strip():
            msg = "This field may not be blank."
            raise serializers.ValidationError(msg)
        return value


class SpendingLimitSerializer(BasicSpendingLimitSerializer):
    category_text = serializers.SerializerMethodField()

    class Meta:
        model = SpendingLimit
        fields = [
            "id",
            "category",
            "category_text",
            "amount",
            "currency",
            "days",
            "text",
        ]
        extra_kwargs = {
            "category_text": {"required": False, "read_only": True},
        }

    def get_category_text(self, instance):
        if not instance.category:
            return None
        return instance.category.text

    def validate(self, data):
        request_method = self.context["request"].method
        user = self.context["request"].user
        category = data.get("category", None)

        # If we are doing a PATCH, allow category to remain unchanged
        if self.instance and request_method == "PATCH":
            category = category or self.instance.category

        # Ensure category is provided on POST and PUT
        if request_method in ["POST", "PUT"] and not category:
            raise serializers.ValidationError("A Category must be provided.")

        amount = data.get("amount", None)
        currency = data.get("currency", None)
        days = data.get("days", None)
        text = data.get("text", None)

        # Ensure the category belongs to the current user
        if (
            category
            and not Category.objects.filter(
                id=category.id, created_by=user
            ).exists()
        ):
            msg = f'Invalid pk "{category.id}" - object does not exist.'
            raise serializers.ValidationError({"category": [msg]})

        # Check for duplicates on creation
        existing_limit = SpendingLimit.objects.filter(
            category=category,
            currency=currency,
            days=days,
            amount=amount,
            text=text,
            created_by=user,
        )
        msg = (
            "A SpendingLimit for this Category with the same currency, amount, days, and text "
            "already exists for this user."
        )

        # Exclude the current instance when updating
        if self.instance:
            existing_limit = existing_limit.exclude(id=self.instance.id)

        if existing_limit.exists():
            raise serializers.ValidationError(msg)

        return data


class SpendingLimitPartialUpdateSerializer(BasicSpendingLimitSerializer):
    class Meta:
        model = SpendingLimit
        fields = [
            "id",
            "amount",
            "currency",
            "days",
            "text",
            "category",
        ]
        extra_kwargs = {
            "category": {"required": False},
            "amount": {"required": False},
            "currency": {"required": False},
            "days": {"required": False},
            "text": {"required": False},
        }

    def validate(self, data):
        user = self.context["request"].user
        category = data.get("category", None)

        # Use the existing category if the category filed is not provided in PATCH
        if self.instance and not category:
            category = self.instance.category

        amount = data.get("amount", None)
        currency = data.get("currency", None)
        days = data.get("days", None)
        text = data.get("text", None)

        # Ensure the category belongs to the user if it's being updated
        if (
            category
            and not Category.objects.filter(
                id=category.id, created_by=user
            ).exists()
        ):
            msg = f'Invalid pk "{category.id}" - object does not exist.'
            raise serializers.ValidationError({"category": [msg]})

        # Check for duplicates
        existing_limit = SpendingLimit.objects.filter(
            category=category,
            currency=currency,
            days=days,
            amount=amount,
            text=text,
            created_by=user,
        )

        # Exclude the current instance when updating
        if self.instance:
            existing_limit = existing_limit.exclude(id=self.instance.id)

        if existing_limit.exists():
            msg = (
                "A SpendingLimit for this Category with the same currency, amount, days, and text "
                "already exists for this user."
            )
            raise serializers.ValidationError(msg)

        return data
