# Generated by Django 4.2.6 on 2025-02-15 12:54

import budgets.models
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("budgets", "0007_spendinglimit"),
    ]

    operations = [
        migrations.AlterField(
            model_name="expense",
            name="amount",
            field=models.DecimalField(
                decimal_places=2,
                max_digits=10,
                validators=[
                    budgets.models.validate_positive_number_allow_zero
                ],
            ),
        ),
    ]
