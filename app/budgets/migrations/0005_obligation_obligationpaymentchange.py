# Generated by Django 4.2.6 on 2024-05-31 04:39

import budgets.models
from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):
    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ("budgets", "0004_savingcategory_is_archived_and_more"),
    ]

    operations = [
        migrations.CreateModel(
            name="Obligation",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("name", models.CharField(max_length=100)),
                (
                    "total_amount",
                    models.DecimalField(
                        blank=True, decimal_places=2, max_digits=10, null=True
                    ),
                ),
                (
                    "obligation_type",
                    models.CharField(
                        choices=[
                            ("regular", "Fixed Obligation"),
                            ("variable", "Variable Obligation"),
                        ],
                        default="regular",
                        max_length=10,
                    ),
                ),
                (
                    "monthly_payment",
                    models.DecimalField(decimal_places=2, max_digits=10),
                ),
                ("start_date", models.DateField()),
                (
                    "payment_day",
                    models.IntegerField(
                        validators=[budgets.models.validate_day_of_month]
                    ),
                ),
                ("end_date", models.DateField()),
                (
                    "created_by",
                    models.ForeignKey(
                        default=None,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
            ],
        ),
        migrations.CreateModel(
            name="ObligationPaymentChange",
            fields=[
                (
                    "id",
                    models.AutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                (
                    "new_monthly_payment",
                    models.DecimalField(decimal_places=2, max_digits=10),
                ),
                ("change_date", models.DateField()),
                (
                    "created_by",
                    models.ForeignKey(
                        default=None,
                        null=True,
                        on_delete=django.db.models.deletion.SET_NULL,
                        to=settings.AUTH_USER_MODEL,
                    ),
                ),
                (
                    "obligation",
                    models.ForeignKey(
                        on_delete=django.db.models.deletion.CASCADE,
                        related_name="changes",
                        to="budgets.obligation",
                    ),
                ),
            ],
        ),
    ]
