# Copyright: (c) 2019, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
import calendar
import datetime

from rest_framework import serializers


def current_month_boundaries():
    """
    Return a tuple composed of the first and the last day
    of the current month, as datetime objects

    Note: be sure the server has the right timezone!
    Otherwise efault timezone will be UTC
    FIXME: If the server is actually in a different time zone,
    beware off-by-one month errors when using this function
    """
    start = datetime.date.today().replace(day=1)
    month_range = calendar.monthrange(start.year, start.month)
    last_day_of_month = month_range[1]
    end = datetime.date(start.year, start.month, last_day_of_month)
    return (start, end)


def check_for_none_values(serializer):
    text = serializer.validated_data.get("text", None)
    if text is None:
        raise serializers.ValidationError(
            {"text": ["This field is required."]}
        )

    current_foreign_value = serializer.validated_data.get(
        "current_foreign_value", None
    )
    if current_foreign_value is None:
        raise serializers.ValidationError(
            {"current_foreign_value": ["This field is required."]}
        )

    current_local_value = serializer.validated_data.get(
        "current_local_value", None
    )
    if current_local_value is None:
        raise serializers.ValidationError(
            {"current_local_value": ["This field is required."]}
        )

    bought_price_local = serializer.validated_data.get(
        "bought_price_local", None
    )
    if bought_price_local is None:
        raise serializers.ValidationError(
            {"bought_price_local": ["This field is required."]}
        )

    bought_price_foreign = serializer.validated_data.get(
        "bought_price_foreign", None
    )
    if bought_price_foreign is None:
        raise serializers.ValidationError(
            {"bought_price_foreign": ["This field is required."]}
        )

    net_loss_foreign = serializer.validated_data.get("net_loss_foreign", None)
    if net_loss_foreign is None:
        raise serializers.ValidationError(
            {"net_loss_foreign": ["This field is required."]}
        )

    net_loss_local = serializer.validated_data.get("net_loss_local", None)
    if net_loss_local is None:
        raise serializers.ValidationError(
            {"net_loss_local": ["This field is required."]}
        )

    foreign_currency = serializer.validated_data.get("foreign_currency", None)
    if foreign_currency is None:
        raise serializers.ValidationError(
            {"foreign_currency": ["This field is required."]}
        )

    net_loss_foreign = serializer.validated_data.get("net_loss_foreign", None)
    if net_loss_foreign is None:
        raise serializers.ValidationError(
            {"net_loss_foreign": ["This field is required."]}
        )

    net_loss_local = serializer.validated_data.get("net_loss_local", None)
    if net_loss_local is None:
        raise serializers.ValidationError(
            {"net_loss_local": ["This field is required."]}
        )

    record_date = serializer.validated_data.get("record_date", None)
    if record_date is None:
        raise serializers.ValidationError(
            {"record_date": ["This field is required."]}
        )
