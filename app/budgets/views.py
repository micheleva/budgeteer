"""Django views for budgets module."""

# Copyright: (c) 2019, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
# pylint: disable=R0901,E1101
from django.contrib.auth.decorators import login_required
from django.shortcuts import render
from django.views.decorators.http import require_http_methods


###############################################################################
# Function based classes
###############################################################################


@require_http_methods(["GET"])
def landing_page(request):
    """Render the langing page"""
    return render(request, "landing_page.html")


@login_required
@require_http_methods(["GET"])
def spa(request):
    """Display the single page application."""
    return render(request, "spa.html")


###############################################################################
# Permission errors views
###############################################################################
def permission_denied_view(
    request, exception=None
):  # pylint: disable=W0613; # noqa
    """Show a custom permission denied page"""
    return render(request, "errors/403.html", {})


def page_not_found_view(
    request, exception=None
):  # pylint: disable=W0613; # noqa
    """Show a custom page not found page"""
    return render(request, "errors/404.html", {})
