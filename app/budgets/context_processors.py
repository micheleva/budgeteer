from django.conf import settings


def inject_vars(_):
    return {
        # Note: this flag does not control React dark theme
        #       it only controls the main css theme returned by django
        "dark_theme": settings.DARK_THEME,
        "rate": settings.EXCHANGE_RATE,
        "currency": settings.CURRENCY,
        "webpack": settings.USE_WEBPACK_DEV_SERVER,
        "signup_enabled": settings.SIGNUP_ENABLED,
    }
