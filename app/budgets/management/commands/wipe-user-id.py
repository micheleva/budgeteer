# Copyright: (c) 2023, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
# pylint: disable=E1101,W0611
from collections import defaultdict
from datetime import timedelta
import logging

from django.db.models import Sum
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand, CommandError

import budgets.models as m


class Command(BaseCommand):
    """
    This command removes all the entities created by a given user from the db.
    """

    help = "Removes the data of a given user from the db. DANGER: this operat"\
           "ion can not be undone!!!"

    def add_arguments(self, parser):
        parser.add_argument(
            "--yes-permanently-remove-data-from-the-db-i-ve-checked-the-"
            "env-and-its-not-pointing-to-prod-db",
            action="store_true",
            help="Confirm the permanent removal of the user's data from the"
                 " database.\n"
                 "Please ensure that the env file is NOT pointing to the "
                 "production database!!!",
            required=True,
        )
        msg = ("The ID of the user whose data needs to be wiped from"
               " the database.")
        parser.add_argument(
            "--user-id",
            type=int,
            help=msg,
            required=True,
        )

    def main(self, user_id):  # pylint: disable=E1101,W0611,R0912,R0914,R0915
        try:
            user = User.objects.get(id=user_id)

            goals_count = m.Goal.objects.filter(created_by=user_id).count()
            if goals_count > 0:
                m.Goal.objects.filter(created_by=user_id).delete()
                self.stdout.write(
                    self.style.SUCCESS(f"{goals_count} Goal(s) deleted.")
                )
            else:
                self.stdout.write(
                    self.style.WARNING("No goals found for this user")
                )

            mb_count = m.MonthlyBalance.objects.filter(created_by=user).count()
            if mb_count > 0:
                msg = f"{mb_count} MonthlyBalance(s) deleted."
                m.MonthlyBalance.objects.filter(created_by=user).delete()
                self.stdout.write(
                    self.style.SUCCESS(msg)
                )
            else:
                self.stdout.write(
                    self.style.WARNING(
                        "No MonthlyBalances found for this user"
                    )
                )

            mb_cat_count = m.MonthlyBalanceCategory.objects.filter(
                created_by=user
            ).count()
            if mb_cat_count > 0:
                m.MonthlyBalanceCategory.objects.filter(
                    created_by=user
                ).delete()
                self.stdout.write(
                    self.style.SUCCESS(
                        f"{mb_cat_count} MonthlyBalanceCategory(s) deleted."
                    )
                )
            else:
                self.stdout.write(
                    self.style.WARNING(
                        "No MonthlyBalanceCategories found for this user"
                    )
                )

            exp_count = m.Expense.objects.filter(created_by=user).count()
            if exp_count > 0:
                m.Expense.objects.filter(created_by=user).delete()
                self.stdout.write(
                    self.style.SUCCESS(f"{exp_count} Expense(s) deleted.")
                )
            else:
                self.stdout.write(
                    self.style.WARNING("No Expenses found for this user")
                )

            cat_count = m.Category.objects.filter(created_by=user).count()
            if cat_count > 0:
                m.Category.objects.filter(created_by=user).delete()
                self.stdout.write(
                    self.style.SUCCESS(f"{cat_count} Categories(s) deleted.")
                )
            else:
                self.stdout.write(
                    self.style.WARNING("No Categories found for this user")
                )

            inc_cat_count = m.IncomeCategory.objects.filter(
                created_by=user
            ).count()
            if inc_cat_count > 0:
                m.IncomeCategory.objects.filter(created_by=user).delete()
                self.stdout.write(
                    self.style.SUCCESS(
                        f"{inc_cat_count} IncomeCategory(s) deleted."
                    )
                )
            else:
                self.stdout.write(
                    self.style.WARNING(
                        "No IncomeCategories found for this user"
                    )
                )

            inc_count = m.Income.objects.filter(created_by=user).count()
            if inc_count > 0:
                m.Income.objects.filter(created_by=user).delete()
                self.stdout.write(
                    self.style.SUCCESS(f"{inc_count} Income(s) deleted.")
                )
            else:
                self.stdout.write(
                    self.style.WARNING("No Incomes found for this user")
                )

            acc_count = m.Account.objects.filter(created_by=user).count()
            if acc_count > 0:
                m.Account.objects.filter(created_by=user).delete()
                self.stdout.write(
                    self.style.SUCCESS(f"{acc_count} Account(s) deleted.")
                )
            else:
                self.stdout.write(
                    self.style.WARNING("No Accounts found for this user")
                )

            ass_count = m.Asset.objects.filter(created_by=user).count()
            if ass_count > 0:
                m.Asset.objects.filter(created_by=user).delete()
                self.stdout.write(
                    self.style.SUCCESS(f"{ass_count} Asset(s) deleted.")
                )
            else:
                self.stdout.write(
                    self.style.WARNING("No Assets found for this user")
                )

            prof_count = m.Dividend.objects.filter(created_by=user).count()
            if prof_count > 0:
                m.Dividend.objects.filter(created_by=user).delete()
                self.stdout.write(
                    self.style.SUCCESS(f"{prof_count} Dividend(s) deleted.")
                )
            else:
                self.stdout.write(
                    self.style.WARNING("No Dividends found for this user")
                )

            bud_count = m.Budget.objects.filter(created_by=user).count()
            if bud_count > 0:
                m.Budget.objects.filter(created_by=user).delete()
                self.stdout.write(
                    self.style.SUCCESS(f"{bud_count} Budget(s) deleted.")
                )
            else:
                self.stdout.write(
                    self.style.WARNING("No Budget found for this user")
                )

            saving_cat_sum = m.SavingCategory.objects.filter(
                created_by=user
            ).count()
            if saving_cat_sum > 0:
                m.Budget.objects.filter(created_by=user).delete()
                self.stdout.write(
                    self.style.SUCCESS(
                        f"{saving_cat_sum} SetAside Categories(s) deleted."
                    )
                )
            else:
                self.stdout.write(
                    self.style.WARNING(
                        "No SetAside Categories found for this user"
                    )
                )

            try:
                user.delete()
                self.stdout.write(
                    self.style.SUCCESS(f"User (ID: {user_id}) deleted.")
                )
            except Exception:  # pylint: disable=W0718; # noqa
                self.stdout.write(self.style.ERROR("User deletion failed"))

        except User.DoesNotExist as e:
            self.stdout.write(
                self.style.ERROR(
                    f"No such user (ID: {user_id}). Data deletion failure!"
                )
            )
            raise SystemExit(
                "Terminating execution. DB was not clened correctly."
            ) from e

    def handle(self, *args, **options):
        try:
            user_id = options["user_id"]
            self.main(user_id)
            self.stdout.write(
                self.style.SUCCESS(
                    "Data removal completed without exceptions being thrown!!!"
                )
            )
        except Exception as e:  # pylint: disable=W0718; # noqa
            self.stdout.write(
                self.style.ERROR(f"wipe-user-id command has failed hard: {e}")
            )
            logging.basicConfig(level=logging.ERROR)
            logger = logging.getLogger(__name__)
            logger.exception(e)
