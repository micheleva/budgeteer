# Copyright: (c) 2023, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
import logging

from django.core.management.base import BaseCommand, CommandError

from functional_tests.test_dividends import MockDividendsAPI

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    """
      This command seeds the database with semi-realistic data
      so that it's easy to test on the flight
    """
    help = 'Sping up an HTTP server that returns dummy dividends data, to test your application'

    def main(self):
        mock_server = MockDividendsAPI()
        mock_server.daemon = True
        mock_server.start()

        try:
            msg = ("\nUse 'http://localhost:13504/dividends' as 'Import url' in the Automatic "
                   "Dividend view, found at '/spa/dividends/import'."
                   )
            self.stdout.write(self.style.SUCCESS(msg))  # pylint: disable=E1101; # noqa
            input("Press Enter to stop the server.")
        except KeyboardInterrupt:
            self.stdout.write(self.style.SUCCESS("Server stopped by user"))  # pylint: disable=E1101; # noqa
        finally:
            mock_server.stop()

        return 0

    def handle(self, *args, **options):
        try:
            return_code = self.main()
            if return_code == 0:
                msg = "The command returned successfully"
                self.stdout.write(self.style.SUCCESS(msg))  # pylint: disable=E1101; # noqa
                return 0

            msg = f"The command seems to have failed with code {return_code}"
            self.stdout.write(self.style.ERROR(msg))  # pylint: disable=E1101; # noqa
            return return_code

        except Exception as e:
            error_message = f"dividends-api-mock-server command has failed hard: {e}"
            self.stdout.write(self.style.ERROR(error_message))  # pylint: disable=E1101; # noqa
            logger.exception(e)
            raise CommandError(error_message) from e
