# Copyright: (c) 2023, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
# pylint: disable=R0901,E1101
from decimal import Decimal
from decimal import ROUND_DOWN
from collections import defaultdict
import datetime
from datetime import timedelta
import random
import logging
import string

from django.db.models import F
from django.db.models import Sum
from django.contrib.auth.models import User
from django.core.management.base import BaseCommand, CommandError

import budgets.models as m
from api.v1.views import assets_monthly_combined

logger = logging.getLogger(__name__)


class Command(BaseCommand):
    """
    This command seeds the database with semi-realistic data
    so that it's easy to test on the flight
    """

    help = (
        "Populate the db with data suited for demoing the app. DANGER: "
        "this operation actually writes to the database!!!"
    )

    def add_arguments(self, parser):
        parser.add_argument(
            "--yes-pollute-the-db-with-demo-data-i-ve-checked"
            "-the-env-and-its-not-pointing-to-prod-db",
            action="store_true",
            help="Confirm to pollute the"
            "database with demo data. "
            "Please ensure that the env file is not pointing"
            " to the production database!!!",
            required=True,
        )

    def main(self):
        intial_monthly_income = 2500
        # Every time we try to create an expense that would exceed the income
        # for that month we have 10% (i.e. (1- 0.9) * 100 %) chance of success
        # of creating it
        overspend_coefficent = 0.9
        income_increase_rate_min = 1.02
        income_increase_rate_max = 1.27

        demo_user = self.create_user()

        # NOTE: Since the MonthlyBudget model will be deprecated (in favour of
        # Budget) around version 1.2, we do not generate data for said entity

        incomes = self.create_income(
            demo_user,
            intial_monthly_income,
            income_increase_rate_min,
            income_increase_rate_max,
        )
        demo_cats = self.create_categories(demo_user.id)

        expenses_sum = self.create_expenses(
            demo_user, demo_cats, overspend_coefficent
        )
        spending_limits_env = self._get_demo_user_spending_limits()

        monthly_balances = self.create_monthly_balances(demo_user)
        _ = self.create_goals(demo_user, incomes, expenses_sum)

        res = self.create_assets(demo_user)
        rate_history = res["currency_exchange_rate_historyical"]

        # Turn 401k monthly balance into Assets
        self.pensionmb_to_assets(demo_user, rate_history)

        # Create Budgets for this user
        self.create_recent_budgets(demo_user)

        special_goals_env = self.create_special_goals(
            demo_user, monthly_balances
        )
        self.create_seeded_data_env_file(
            demo_user, special_goals_env, spending_limits_env
        )
        return 0

    def handle(self, *args, **options):
        try:
            return_code = self.main()
            self.stdout.write(
                self.style.SUCCESS("Successfully seeded the database!!!")
            )
            return return_code
        except Exception as e:
            error_message = f"seed-db command has failed hard: {e}"
            self.stdout.write(self.style.ERROR(error_message))
            logger.exception(e)
            raise CommandError(error_message) from e
        # FIX ME except is not returning any error_code, is this correct?
        return return_code

    def create_user(self):
        readable_chars = string.ascii_letters + string.digits
        for char in ["0", "O", "o", "1", "l", "I", "B", "8", "5", "s", "S"]:
            readable_chars = readable_chars.replace(char, "")

        username_lenght = 8
        password_length = 24

        password = "".join(random.choices(readable_chars, k=username_lenght))
        username = "".join(random.choices(readable_chars, k=password_length))
        created_user = User.objects.create_user(username, password=password)
        user_id = created_user.id
        self.stdout.write(
            self.style.SUCCESS(
                f"Username: {username}\nPassword"
                f": {password}\nUser id: {user_id}"
            )
        )
        return created_user

    def create_income(
        self,
        user,
        initial_income,
        income_increase_rate_min,
        income_increase_rate_max,
    ):
        inc_cat = m.IncomeCategory.objects.create(text="Wage", created_by=user)

        # Print all the income categories created for this user to screen
        self._write_inc_cat_res_to_screen(user)

        income_analysis = {inc_cat.text: {"value": initial_income}}

        # To make things easy, since we start creating expense from Jan 2015
        # we create a monthly budget for the previous month, so that we do not
        # need to account for the first month (Jan 2015) not being able to
        # refer the previous month's monthly balance
        start_date = datetime.date(2014, 12, 1)

        incomes = []
        raises = []
        for inc_category in [inc_cat]:
            for year in range(start_date.year, datetime.date.today().year + 1):
                for month in range(1, 13):
                    amount = income_analysis[inc_category.text]["value"]
                    # Every three year we get a random salary increase between,
                    # income_increase_rate_min% and income_increase_rate_max%
                    if (year - start_date.year) % 3 == 0:
                        # We only get a raise per year: this loop is run for
                        # each category! i.e. we only grant a raise per year
                        # no matter which category did get said raise
                        if year not in raises:
                            income_analysis[inc_category.text][
                                "value"
                            ] *= random.uniform(
                                income_increase_rate_min,
                                income_increase_rate_max,
                            )
                            raises.append(year)
                    inc = m.Income.objects.create(
                        category=inc_category,
                        date=datetime.date(year, month, 1),
                        amount=amount,
                        created_by=user,
                    )
                    incomes.append(inc)
        self._write_inc_res_to_screen(user)

        return incomes

    def create_categories(self, user):
        categories_text = [
            "Water",
            "Electricity",
            "Gas",
            "Phone",
            "Internet",
            "Rent",
            "Food",
            "Medicals",
            "Leisure",
            "Sport",
            "Charity",
            "Clothes",
            "Books",
            "Hosting",
            "Other",
        ]
        categories_objects = []

        for ct in categories_text:
            c = m.Category.objects.create(text=ct, created_by_id=user)
            categories_objects.append(c)

        cat_count = m.Category.objects.filter(created_by=user).count()
        if cat_count > 0:
            msg = f"{cat_count} Categories(s) created."
            self.stdout.write(self.style.SUCCESS(msg))
        else:
            msg = "No Categories created for this user"
            self.stdout.write(self.style.WARNING(msg))

        return categories_objects

    def create_expenses(self, user, categories, overspend_coefficent):
        start_date = datetime.date(2015, 1, 1)
        end_date = datetime.date.today()

        exp_to_be_created = []
        expenses_sum = defaultdict(int)
        for category in categories:
            for year in range(start_date.year, end_date.year + 1):
                start_month = start_date.month if year == start_date.year else 1
                end_month = end_date.month if year == end_date.year else 12
                # For each year in the range from the start year to the end
                # year (inclusive):
                # a. Determines the start and end months based on the year
                #    and the start/end dates
                # b. Iterates over each month in the range from the start
                #    month to the end month (inclusive)
                # c. Creates a date object for the current year and month
                # d. If the category is "rent", sets the amount to <rent>,
                #    creates an Expense object for the current month
                # e. If the category is not "rent", generates a random number
                #    of expenses (between 0 and 6) for the current month
                #    If the expenses sum for the current month is greater
                #    than said month's income and a random number is less
                #    than <overspend_coefficent>, the loop continues without
                #    creating an Expense object
                # For each create expenses add the amount to the expenses sum
                # for the corresponding month
                for month in range(start_month, end_month + 1):
                    # Yes, 27 is not included, but let's err on the safe side
                    # and pretend months have at most 26 days
                    # See https://infiniteundo.com/post/25326999628/falsehoods-
                    # programmers-believe-about-time
                    date = datetime.date(
                        year, month, int(random.uniform(1, 27))
                    )
                    # Do not generate expenses in the future
                    if date > datetime.datetime.now().date():
                        date = datetime.datetime.now().date()

                    total_income = (
                        m.Income.objects.filter(
                            date__year=year, date__month=month, created_by=user
                        ).aggregate(Sum("amount"))["amount__sum"]
                        or 0
                    )
                    expense_data = {
                      'category': category,
                      'date': date,
                      'user': user,
                      'total_income': total_income,
                      'overspend_coefficent': overspend_coefficent
                    }
                    self._generate_expenses(expense_data, exp_to_be_created,
                                            expenses_sum)

        self._write_exp_to_db_and_log_res_to_screen(exp_to_be_created, user)
        return dict(expenses_sum)

    def create_monthly_balances(self, user):
        # Again, to make things easy we start from Dec 2014, as expenses start
        # from Jan 2015
        cats = self._create_and_return_mb_cats_and_print_to_screen(user)
        self._crate_initial_empty_mbs(user, cats)

        current_date = datetime.datetime.now().date()
        start_date = datetime.datetime(2015, 1, 1).date()
        end_date = current_date.replace(day=1)
        monthly_balances = []

        while start_date <= end_date:
            total_expenses = (
                m.Expense.objects.filter(
                    date__year=start_date.year,
                    date__month=start_date.month,
                    created_by=user,
                ).aggregate(Sum("amount"))["amount__sum"]
                or 0
            )
            total_income = (
                m.Income.objects.filter(
                    date__year=start_date.year,
                    date__month=start_date.month,
                    created_by=user,
                ).aggregate(Sum("amount"))["amount__sum"]
                or 0
            )
            prev_month_year = (
                start_date.year if start_date.month > 1 else start_date.year - 1
            )
            prev_month_month = (
                start_date.month - 1 if start_date.month > 1 else 12
            )

            try:
                prev_month_amount = (
                    m.MonthlyBalance.objects.filter(
                        date__year=prev_month_year,
                        date__month=prev_month_month,
                        date__day=1,
                        created_by=user,
                    ).aggregate(Sum("amount"))["amount__sum"]
                    or 0
                )
            except m.MonthlyBalance.DoesNotExist:
                prev_month_amount = 0

            month_remainder = (
                prev_month_amount + total_income - total_expenses
            )

            # To make the Monthly Budget graph a bit more interesting, allocate
            # what we have left for this month in a semi-random fashion between
            # the various categories we have for this user
            amounts = self._allocate_money_between_categories(month_remainder)

            start_date = self._create_monthly_balances(user, cats, amounts,
                                                       start_date,
                                                       monthly_balances)

        self._print_mb_to_screen(user)

        return monthly_balances

    def create_goals(self, user, incomes, expenses_sum):
        """
        Create goals for this user, goals' amounts will be based on the
        already generated monthly balances
        """
        goal_samples = [
            ("Passion project", "Invest in personal venture."),
            ("World travel", "Embark on a year-long journey."),
            ("Emergency fund", "Set funds aside for unexpected expenses."),
            ("Down payment", "Save for a house."),
            ("FIRE", "Achieve financial independence."),
        ]

        inc_years, yrly_income_sum = self._calculate_yearly_income_sum(incomes)
        exp_years, yrly_expense_sum = self._calculate_yearly_expense_sum(
            expenses_sum
        )

        goal_objects = []
        for goal_data in goal_samples:
            goal_text, goal_note = goal_data
            goal_amount = None
            if goal_text == "Passion project":
                # 5 years worth of the yearly income of year 3
                goal_amount = int(5 * yrly_income_sum[inc_years[3]])
            elif goal_text == "World travel":
                # 10 years worth of the yearly income of year 4
                goal_amount = int(10 * yrly_income_sum[inc_years[4]])
            elif goal_text == "Emergency fund":
                # 3 years worth of the yearly income of year 4
                goal_amount = int(3 * yrly_income_sum[inc_years[4]])
            elif goal_text == "Down payment":
                # 20,000 is a magic number. I just tried some values manually
                # until the final data looked realistic enough
                goal_amount = int(20000)
            elif goal_text == "FIRE":
                # 20 times the yearly amount spent in year 1
                goal_amount = int(20 * yrly_expense_sum[exp_years[1]])
            if goal_amount is not None:
                goal = m.Goal.objects.create(
                    amount=goal_amount,
                    text=goal_text,
                    note=goal_note,
                    created_by=user,
                )
                goal_objects.append(goal)

        self._print_goals_to_screen(user)

        return goal_objects

    def create_special_goals(self, user, monthly_balances):
        before = m.Goal.objects.filter(created_by=user).count()
        special_goal_to_be_replaced = m.Goal.objects.create(
            amount=1, text="House Loan", note="Home sweet home",
            created_by=user
        )
        second_special_goal_to_be_replaced = m.Goal.objects.create(
            amount=1,
            text="Rocket Loan",
            note="Why did I buy a rocket to the Moon?",
            created_by=user,
        )

        # Monthly Balance object have always their date ending in "-01"
        current_date = datetime.datetime.now().date().replace(day=1)
        current_date_yyyymmdd = current_date.strftime("%Y-%m-%d")

        # See how much money is left for the current month (i.e. sum of all
        # monthly balances)
        curr_month_mb_amount = sum(
            [
                m.amount
                for m in monthly_balances
                if m.date.strftime("%Y-%m-%d") == current_date_yyyymmdd
            ]
        )

        res = assets_monthly_combined(user)
        curr_month_assets = [
            item["real_amount"]
            for item in res
            if item["record_date"].strftime("%Y-%m-%d") == current_date_yyyymmdd
        ][0]
        curr_month_available = curr_month_mb_amount + curr_month_assets

        end_date_of_special_goals = datetime.datetime.strptime(
            "2050-01-01", "%Y-%m-%d"
        )
        months_to_goal_end = (
            end_date_of_special_goals.year - current_date.year
        ) * 12 + (end_date_of_special_goals.month - current_date.month)

        to_half_the_goal_at_current_month = (
            curr_month_available * 1.4 / months_to_goal_end
        )
        one_third_the_goal_at_current_month = (
            curr_month_available * 0.9 / months_to_goal_end
        )

        special_goals_env = (
            f"SPECIAL_GOALS={user.id},"
            f"{special_goal_to_be_replaced.id},"
            "2050-05-01,2021-11-01,"
            f"{int(one_third_the_goal_at_current_month/3)},"
            f"{int(one_third_the_goal_at_current_month)},0,25"
            f";{user.id},"
            f"{second_special_goal_to_be_replaced.id},"
            "2050-05-01,2023-01-01,-1,"
            f"{int(to_half_the_goal_at_current_month)},0,25"
        )
        self._print_special_goals_to_screen(user, before)

        return special_goals_env

    def create_assets(self, user):
        """
        Create assets, and their dividends
        """
        acc = m.Account.objects.create(
            text="Stocks",
            credit_entity="Foreign Broker",
            is_foreign_currency=True,
            created_by=user,
        )
        self._print_accounts_to_screen(user)

        assets = ["RSU_SYMBOL", "STK1", "STK2", "BND"]

        today = datetime.date.today()
        start_date = datetime.date(2016, 1, 1)

        # assets_analysis holds data about each asset, and their dividend
        # return data

        # 122 is a magic number from
        # https://www.exchangerates.org.uk/USD-JPY-spot-exchange-rates-history-2018.html  # pylint: disable=C0301; # noqa
        rate = 112
        assets_analysis = self._get_harcoded_assets(
            acc, user, rate, today
        )

        (
          assets_to_be_created,
          dividends_to_be_created,
          assets_hist,
          rate_history
        ) = self._generate_dividend_and_rate_history(assets, assets_analysis,
                                                     acc, user, today, rate,
                                                     start_date)

        # How much monthly variance do we want for the USD/JPY exchange rate
        increase_tick = 3
        for asset in assets:
            assets_hist[asset] = {}
            for year in range(start_date.year, today.year + 1):
                for month in range(1, 13, 1):
                    first_day_of_the_month = datetime.date(year, month, 1)
                    yymmdd_date = first_day_of_the_month.strftime("%Y-%m-%d")

                    # Do not generate assets or dividends with future dates!
                    if first_day_of_the_month > datetime.date.today():
                        break

                    # Make USD/JPY exchange rate fluctuate monthly
                    # If this is the first asset we loop through for this month
                    # alter the rate, and save the generated fluctation/date.
                    # In this way all the assets will use the same exchange
                    # rate for every given month
                    if yymmdd_date not in rate_history:
                        # Simulate a small crash in the exchange rate of USD
                        # against JPY if USD gets too strong against JPY
                        # (i.e. more than 150)
                        if currency_exchange_rate > 150:
                            increase_tick = -30
                        else:
                            # Othweise simulate USD steadily getting stronger
                            # over JPY
                            increase_tick = 3
                        currency_exchange_rate += increase_tick
                        rate_history[yymmdd_date] = currency_exchange_rate
                    else:
                        currency_exchange_rate = rate_history[yymmdd_date]

                    # Create the asset for this month
                    asset_obj = self._create_asset(acc, assets_analysis, asset,
                                                   user,
                                                   rate_history,
                                                   first_day_of_the_month)
                    assets_to_be_created.append(asset_obj)
                    assets_hist[asset][yymmdd_date] = asset_obj

                    # Genereate dividends every three months. Note this will
                    # generate dividends on the first month of the asset
                    # ownership as well... but let let it be so. We do not
                    # have to be 100% realistic in this data generation
                    if month % 3 == 0 and assets_analysis[asset]["return"] > 0:
                        details = [asset, user, acc, first_day_of_the_month]
                        self._generate_divided(assets_analysis, details,
                                               rate_history,
                                               dividends_to_be_created)

                    self._alter_asset(assets_analysis, asset,
                                      rate_history, yymmdd_date)

        extra = [acc, user]
        self._generate_assets_for_not_reinvested_cash(dividends_to_be_created,
                                                      assets_to_be_created,
                                                      rate_history, extra)

        # Since we create a few hundred Asset and Dividend objects
        # it's better to create them in one chunk
        m.Asset.objects.bulk_create(assets_to_be_created)
        m.Dividend.objects.bulk_create(dividends_to_be_created)

        self._write_assets_to_screen(user)
        self._write_dividends_to_screen(user)

        return {
            "assets_historical": assets_hist,
            "currency_exchange_rate_historyical": rate_history,
        }

    def create_seeded_data_env_file(
        self, user, special_goals_env, spending_limits_env
    ):
        self.stdout.write(
            self.style.SUCCESS(
                "Add the following variables to the app/.env file, to have"
                f" User ID: {user.id} graph to be displayed properly!"
            )
        )
        self.stdout.write(special_goals_env)
        self.stdout.write(spending_limits_env)

    def pensionmb_to_assets(self, user, rate_history):
        self.stdout.write(
            self.style.SUCCESS("Turning 401k Monthly Balances into Assets...")
        )
        acc = m.Account.objects.create(
            text="401K",
            credit_entity="Pension system",
            is_foreign_currency=False,
            created_by=user,
        )
        assets_to_be_created = []
        try:
            mbcat_401k = m.MonthlyBalanceCategory.objects.filter(
                text="401k account", created_by=user
            ).get()
            for mb in m.MonthlyBalance.objects.filter(
                created_by=user, category=mbcat_401k
            ).all():
                # We pretend that the 401k Assets do not fluctuate, but they're
                # instead stable for the whole time. This is just to keep the
                # seed-db command's logic symple
                if mb.amount > 0:
                    if mb.date.strftime("%Y-%m-%d") not in rate_history.keys():
                        # Hardcode JPY/USD rate to 112 for months before
                        # 2016-01, as we're not generating rate history before
                        # that date
                        rate = 112
                    else:
                        rate = rate_history[mb.date.strftime("%Y-%m-%d")]

                    asset_obj = m.Asset(
                        account=acc,
                        amount=1,
                        text="401K",
                        created_by=user,
                        foreign_currency="JPY",
                        current_foreign_value=0,
                        current_local_value=mb.amount,
                        bought_price_foreign=0,
                        bought_price_local=mb.amount,
                        net_loss_foreign=0,
                        net_loss_local=0,
                        rate=rate,
                        record_date=mb.date,
                    )
                    assets_to_be_created.append(asset_obj)
            # Since we create a few hundred Asset objects, it's better to
            # create them in one chunk
            self._write_assets_to_db_and_print_res(assets_to_be_created)

            # Once the Assets have replaced the MonthlyBalance objects
            # delete them, and the linked Monthly Budget Category
            self._delete_mb_and_print_res(user, mbcat_401k)
            self._delete_mb_cat_and_print_res(mbcat_401k)

        except m.MonthlyBalanceCategory.DoesNotExist as e:
            self.stdout.write(self.style.ERROR("""MonthlyBalanceCategory with
 text "401k account" does not exists. Make sure you have not altered the
seed-db command by accident. Double check if create_monthly_balances()
is properly called, and if it is creating a "401k account"
MonthlyBalanceCategory!""")
                              )
            msg = "Terminating execution. DB was not seeded."
            raise SystemExit(msg) from e

    def _get_harcoded_assets(
        self, acc, user, currency_exchange_rate, start_date
    ):
        # Number of assets held at the beginning
        # There sare magic numbers that look like generating realistic data
        rsu_initial_amount = 3
        stk1_inital_amount = 2
        stk2_inital_amount = 7
        bnd_inital_amount = 12
        yymmdd_start_date = start_date.strftime("%Y-%m-%d")
        return {
            # Low return stock
            "RSU_SYMBOL": {
                "text": "RSU_SYMBOL",
                "return": 0.021,
                "amount": rsu_initial_amount,
                "account": acc,
                "created_by": user,
                "foreign_currency": "USD",
                # This asset has an initial value of 40
                "current_foreign_value": 40 * rsu_initial_amount,
                "current_local_value": 40
                * rsu_initial_amount
                * currency_exchange_rate,
                "bought_price_foreign": 40 * rsu_initial_amount,
                "bought_price_local": 40
                * rsu_initial_amount
                * currency_exchange_rate,
                "net_loss_foreign": 0,
                "net_loss_local": 0,
                "record_date": yymmdd_start_date,
            },
            # High return stock
            "STK1": {
                "text": "STK1",
                "return": 0.055,
                "amount": stk1_inital_amount,
                "account": acc,
                "created_by": user,
                "foreign_currency": "USD",
                "current_foreign_value": 35 * stk1_inital_amount,
                "current_local_value": 35
                * stk1_inital_amount
                * currency_exchange_rate,
                "bought_price_foreign": 35 * stk1_inital_amount,
                "bought_price_local": 35
                * stk1_inital_amount
                * currency_exchange_rate,
                "net_loss_foreign": 0,
                "net_loss_local": 0,
                "record_date": yymmdd_start_date,
            },
            # Growth stock, no dividends
            "STK2": {
                "text": "STK2",
                "return": 0,
                "amount": stk2_inital_amount,
                "account": acc,
                "created_by": user,
                "foreign_currency": "USD",
                # This asset has an initial value of 19
                "current_foreign_value": 19 * stk2_inital_amount,
                "current_local_value": 19
                * stk2_inital_amount
                * currency_exchange_rate,
                "bought_price_foreign": 19 * stk2_inital_amount,
                "bought_price_local": 19
                * stk2_inital_amount
                * currency_exchange_rate,
                "net_loss_foreign": 0,
                "net_loss_local": 0,
                "record_date": yymmdd_start_date,
            },
            # Bond, low yeld
            "BND": {
                "text": "BND",
                "return": 0.002,
                "amount": bnd_inital_amount,
                "account": acc,
                "created_by": user,
                "foreign_currency": "USD",
                # This asset has an initial value of 2
                "current_foreign_value": 2 * bnd_inital_amount,
                "current_local_value": 2
                * bnd_inital_amount
                * currency_exchange_rate,
                "bought_price_foreign": 2 * bnd_inital_amount,
                "bought_price_local": 2
                * bnd_inital_amount
                * currency_exchange_rate,
                "net_loss_foreign": 0,
                "net_loss_local": 0,
                "record_date": yymmdd_start_date,
            },
        }

    def _delete_mb_and_print_res(self, user, mbcat_401k):
        deleted_mb = m.MonthlyBalance.objects.filter(
            created_by=user, category=mbcat_401k
        ).delete()
        if deleted_mb[0] > 0:
            self.stdout.write(
                self.style.SUCCESS(
                    f"{deleted_mb[0]} Monthly Balance(s) (401k related "
                    "one(s)) have been deleted."
                )
            )
        else:
            self.stdout.write(
                self.style.NOTICE(
                    "No 401K Monthly Balance(s) with '401k account' "
                    "Category have been deleted for this user"
                )
            )

    def _write_assets_to_db_and_print_res(self, assets):
        asset_count = m.Asset.objects.bulk_create(assets)
        if len(asset_count) > 0:
            self.stdout.write(
                self.style.SUCCESS(
                    f"{len(asset_count)} Assets(s) created from the 401K "
                    "Monthly Balances object."
                )
            )
        else:
            self.stdout.write(
                self.style.NOTICE(
                    "No 401K Assets create created for this user"
                )
            )

    def _delete_mb_cat_and_print_res(self, mbcat_401k):
        deleted_mb_cat = mbcat_401k.delete()
        if deleted_mb_cat[0] > 0:
            self.stdout.write(
                self.style.SUCCESS(
                    f"{deleted_mb_cat[0]} '401k account' Monthly Balance"
                    "Category(ies) have been deleted."
                )
            )
        else:
            self.stdout.write(
                self.style.NOTICE(
                    "No '401k account' Monthly Balance Category has been"
                    "deleted for this user"
                )
            )

    def _print_goals_to_screen(self, user):
        if m.Goal.objects.filter(created_by=user).count() > 0:
            self.stdout.write(
                self.style.SUCCESS(
                  f"{m.Goal.objects.filter(created_by=user).count()} Goal(s)"
                  "created.")
            )
        else:
            self.stdout.write(
                self.style.WARNING(f"No goals created for this user")
            )

    def _print_special_goals_to_screen(self, user, before):
        after = m.Goal.objects.filter(created_by=user).count()
        special_goals_amount = after - before
        if special_goals_amount > 0:
            self.stdout.write(
                self.style.SUCCESS(
                    f"{special_goals_amount} Special Goal(s) created."
                )
            )
        else:
            self.stdout.write(
                self.style.WARNING("No special goals created for this user")
            )

    def _calculate_yearly_income_sum(self, data):
        yearly_income_sum = {}
        for entry in data:
            year = entry.date.year
            if year in yearly_income_sum:
                yearly_income_sum[year] += entry.amount
            else:
                yearly_income_sum[year] = entry.amount
        # IMPORTANT: in versions prior to Python3.7 'years' will not
        # be oredered, use years = sorted(str(key) for key in
        # yearly_income_sum.keys()) instead
        years = [int(key) for key in yearly_income_sum.keys()]
        return [years, yearly_income_sum]

    def _calculate_yearly_expense_sum(self, expenses):
        yearly_expense_sum = {}
        for date, amount in expenses.items():
            year = datetime.datetime.strptime(date, "%Y-%m").year
            if year in yearly_expense_sum:
                yearly_expense_sum[year] += amount
            else:
                yearly_expense_sum[year] = amount
        # IMPORTANT: in versions prior to Python3.7 'years' will not be
        # oredered, use years = sorted(str(key) for key in
        # yearly_income_sum.keys()) instead
        years = [int(key) for key, value in yearly_expense_sum.items()]
        return [years, yearly_expense_sum]

    def create_recent_budgets(self, user):
        last_year = datetime.date.today().year - 1

        cats = m.Category.objects.all()
        category_mapping = {category.id: category for category in cats}

        # 1. Select previous year expenses yearly total by category

        # 2. Create yearly budgets for each category. Make only Food and
        # Lesiure2 categories budgets' amount too low, (i.e. spend > budgeted)
        # Make Rent budget amount equal to the sum of the expenses, and for the
        # remaining categories, the budgets' amount should be 3% higher than
        # the expenses sum
        self._generate_yearly_budgets(user, last_year, category_mapping)

        # 3. Create monthly budgets for each category, and choose two cats
        # for which budgets will be always not enough. expenses > budgeted for
        # each month (it should be the same cat as above)

        self._generate_monthly_budgets(user, last_year)

        # 4. For the current year, implement monthly budgets based on the
        # generated expenses, and generate the remaining months (unless
        # the seed db command has been run on december) projecting said amount.
        # i.e. if we run the command on May, divide the sum by 5 and multiply
        # it by 12 when generating the yearly amount
        curr_year = datetime.date.today().year

        self._generate_yearly_budgets(user, curr_year, category_mapping)

        # 5. For the current year generate a yearly budget to be just the sum
        # of the above created monthly budgets. For 2 cats, substract a few
        # percentage point (to make it oveflow).
        self._generate_monthly_budgets(user, curr_year)

        # Log the results to console
        self._print_budgets_to_screen(user)

    def _generate_yearly_budgets(self, user, year, category_mapping):
        curr_year = datetime.date.today().year
        curr_month = datetime.date.today().month
        filters = {'date__year': year, 'created_by': user}

        exp_by_cat = m.Expense.objects.filter(**filters)\
          .values("category__text", "category__id")\
          .annotate(total_amount=Sum("amount"))\
          .order_by("category__id", "-total_amount")

        yearly_budgs = []
        for e in exp_by_cat:
            category_obj = category_mapping[e['category__id']]
            budget_amount = e['total_amount']

            # Adjust the expenses only for the current year
            if year == curr_year and curr_month != 12:
                # Since seed-db commands only generate expenses till the
                # current month, as in the month of the year when the command
                # was executed, we need to forecast that to be a full year

                # i.e. if we execute the command on May, we do amount/5 * 12
                # to obtain a full year of how much the expenses would be
                budget_amount = budget_amount / curr_month * 12

            # Make some adjustment so that the data will feel more "real"
            # i.e. the demo user has some categories underbudgeted, and some
            # overbudgeted
            if category_obj.text in ["Food", "Leisure"]:
                # Leisure and Food should be underbudgeted
                budget_amount = max(budget_amount * 0.8, budget_amount * 0.9)
            elif category_obj.text != "Rent":
                # Everyt other category is a bit overbudgeted
                budget_amount *= 1.03

            # Rent should just match the expenses amount
            # so we do not alter it

            # Budget uses Decimals, however yen has no decimals.
            # Because of that, no budget can achieve 100% if there are
            # decimal digits in the amount field
            # NOTE: This is here for when (if) we'll ever support other
            # currencies
            budget_amount = Decimal(budget_amount)\
                            .quantize(Decimal('1.'), rounding=ROUND_DOWN)

            b = m.Budget(
                category=category_obj,
                savingcategory=None,
                year=year,
                month=None,
                amount=budget_amount,
                allocated_amount=None,
                created_by=user,
            )

            yearly_budgs.append(b)

        m.Budget.objects.bulk_create(yearly_budgs)

    def _generate_monthly_budgets(self, user, year):
        curr_year_monthly_budgs = []
        for b in m.Budget.objects.filter(created_by=user, year=year,
                                         month=None).all():
            monthly_budget_amount = Decimal(b.amount / 12)\
                                    .quantize(Decimal('1.'),
                                              rounding=ROUND_DOWN)
            # In order to have the sum of all the Budgets for each month for a
            # given category to be equal to the yearly one, we have to keep
            # track of the remainer of yearly.amount / 12 division
            remainder = b.amount - (monthly_budget_amount * 12)
            for month in range(1, 13):
                am = monthly_budget_amount + Decimal(remainder) if month == 12\
                     else monthly_budget_amount
                b = m.Budget(
                    category=b.category,
                    savingcategory=None,
                    year=year,
                    month=month,
                    amount=am,
                    allocated_amount=None,
                    created_by=user,
                )
                curr_year_monthly_budgs.append(b)

        m.Budget.objects.bulk_create(curr_year_monthly_budgs)

    def _print_accounts_to_screen(self, user):
        acc_count = m.Account.objects.filter(created_by=user).count()
        if acc_count > 0:
            self.stdout.write(
                self.style.SUCCESS(f"{acc_count} Account(s) created.")
            )
        else:
            self.stdout.write(
                self.style.WARNING("No Account created for this user")
            )

    def _print_budgets_to_screen(self, user):
        # Log the results to console
        if m.Budget.objects.filter(created_by=user).count() > 0:
            self.stdout.write(
                self.style.SUCCESS(
                  f"{m.Budget.objects.filter(created_by=user).count()}"
                  " Budget(s) created.")
            )
        else:
            self.stdout.write(
                self.style.WARNING("No Budget created for this user")
            )

    def _write_inc_cat_res_to_screen(self, user):
        if m.IncomeCategory.objects.filter(created_by=user).count() > 0:
            self.stdout.write(
                self.style.SUCCESS(
                  f"{m.IncomeCategory.objects.filter(created_by=user).count()}"
                  " IncomeCategory(s) created.")
            )
        else:
            self.stdout.write(
                self.style.WARNING("No IncomeCategory created for this user")
            )

    def _write_inc_res_to_screen(self, user):
        if m.Income.objects.filter(created_by=user).count() > 0:
            self.stdout.write(
                self.style.SUCCESS(
                  f"{m.Income.objects.filter(created_by=user).count()} "
                  "Income(s)" " created.")
            )
        else:
            self.stdout.write(
                self.style.WARNING("No Income created for this user")
            )

    def _this_might_be_a_bug(self):
        self.stdout.write(
            self.style.WARNING(
                """Hmmm this is funny. You have nothing to worry about,
but if you feel like it, please take a dump of the database (at least the data
linked to the user generated by this command), the full raw output of this
command, and open a ticket upstream. The seed-db logic might have a very minor
issue. For more details see app/budgets/commands/seed-db.py create_assets()."""
            )
        )

    def _generate_note(self, category_text):
        note = ""
        if category_text == "Food":
            if random.random() > 0.2:  # Magic number
                note = "Guilty pleasure food"
        if category_text == "Leisure":
            if random.random() > 0.4:  # Magic number
                note = "Luxurious habit to break"
        return note

    def _generate_expenses(self, expense_data, expenses_to_be_created,
                           expenses_sum):
        if expense_data['category'].text == "rent":
            # Spend 30% of the current month income in rent.
            # i.e. make the rent cost rise as the salary goes up
            # in order to avoid the seeded monthly balance
            # data to have an exponential growth. The salary is
            # increased evry 3 years in this seed db data creation
            # logic
            amount = expense_data['total_income'] // 3
            exp = m.Expense(
                category=expense_data['category'],
                date=expense_data['date'],
                note="expense",
                amount=amount,
                created_by=expense_data['user'],
            )
            expenses_to_be_created.append(exp)
            expenses_sum[expense_data['date'].strftime("%Y-%m")] += amount
        else:
            for _ in range(random.randint(1, 5)):
                # overspend coefficent is a 0 to 1 number, against
                # wich we do perform a random check
                # for every expense creation. If the random result
                # is lower than the overspend coefficent, then we
                # do not proceed to create said expense
                if (
                    expenses_sum[expense_data['date'].strftime("%Y-%m")]
                    > expense_data['total_income']
                    and random.random() < expense_data['overspend_coefficent']
                ):
                    continue
                # NOTE: with 15 categories and 1 to 5 random() we can have 15
                # to 75 expenses items per month (plus 1 for rent)
                amount = random.randint(
                    (expense_data['total_income'] // 93),  # Magic number
                    (expense_data['total_income'] // 87 + 5)  # Magic numbers
                )

                note = self._generate_note(expense_data['category'].text)

                exp = m.Expense(
                    category=expense_data['category'],
                    date=expense_data['date'],
                    note=note,
                    amount=amount,
                    created_by=expense_data['user'],
                )
                expenses_to_be_created.append(exp)
                expenses_sum[expense_data['date'].strftime("%Y-%m")] += amount

    def _get_demo_user_spending_limits(self):
        # If the intial_monthly_income is 2500, these magic numbers should do
        return (
            "SPENDING_LIMITS=38,JPY,30,Food,guilty pleasure food;"
            "49,JPY,60,Leisure,Luxurious habit"
        )

    def _write_exp_to_db_and_log_res_to_screen(self, exp_to_be_created, user):
        # Since we create a few thousand Expense objects, it's better to create
        # them in one chunk
        m.Expense.objects.bulk_create(exp_to_be_created)
        exp_count = m.Expense.objects.filter(created_by=user).count()
        if exp_count > 0:
            self.stdout.write(
                self.style.SUCCESS(f"{exp_count} Expense(s) created.")
            )
        else:
            self.stdout.write(
                self.style.WARNING("No Expense created for this user")
            )

    def _print_mbcat_to_screen(self, user):
        mb_cat_count = m.MonthlyBalanceCategory.objects.filter(
            created_by=user
        ).count()
        if mb_cat_count > 0:
            self.stdout.write(
                self.style.SUCCESS(
                    f"{mb_cat_count} MonthlyBalanceCategory(s) created."
                )
            )
        else:
            self.stdout.write(
                self.style.WARNING(
                    "No MonthlyBalanceCategory created for this user"
                )
            )

    def _crate_initial_empty_mbs(self, user, cats):
        for c in cats:
            m.MonthlyBalance.objects.create(
                created_by=user,
                date=datetime.datetime(2014, 12, 1),
                amount=0,
                category=c,
            )

    def _create_and_return_mb_cats_and_print_to_screen(self, user):
        mbcat_bank01 = m.MonthlyBalanceCategory.objects.create(
            text="Bank A - Savings account", created_by=user
        )
        mbcat_401k = m.MonthlyBalanceCategory.objects.create(
            text="401k account", created_by=user
        )
        mbcat_mattress = m.MonthlyBalanceCategory.objects.create(
            text="Cash under the mattress", created_by=user
        )
        mbcat_bank02 = m.MonthlyBalanceCategory.objects.create(
            text="Bank B - Savings account", created_by=user
        )

        self._print_mbcat_to_screen(user)

        return [mbcat_bank01, mbcat_401k, mbcat_mattress, mbcat_bank02]

    def _create_monthly_balances(self, user, cats, amounts, start_date, mbs):
        mbcat_bank01 = cats[0]
        mbcat_401k = cats[1]
        mbcat_mattress = cats[2]
        mbcat_bank02 = cats[3]

        mb401 = m.MonthlyBalance.objects.create(
            date=start_date,
            amount=amounts[0],
            category=mbcat_401k,
            created_by=user,
        )
        mbs.append(mb401)

        mb01 = m.MonthlyBalance.objects.create(
            date=start_date,
            amount=amounts[1],
            category=mbcat_bank01,
            created_by=user,
        )
        mbs.append(mb01)

        mb_mat = m.MonthlyBalance.objects.create(
            date=start_date,
            amount=amounts[2],
            category=mbcat_mattress,
            created_by=user,
        )
        mbs.append(mb_mat)

        mb02 = m.MonthlyBalance.objects.create(
            date=start_date,
            amount=amounts[3],
            category=mbcat_bank02,
            created_by=user,
        )
        mbs.append(mb02)

        start_date += timedelta(days=32)
        start_date = start_date.replace(day=1)
        return start_date

    def _print_mb_to_screen(self, user):
        mb_count = m.MonthlyBalance.objects.filter(created_by=user).count()
        if mb_count > 0:
            self.stdout.write(
                self.style.SUCCESS(f"{mb_count} MonthlyBalance(s) created.")
            )
        else:
            self.stdout.write(
                self.style.WARNING("No MonthlyBalance created for this user")
            )

    def _allocate_money_between_categories(self, amount):
        money_401 = int(amount * 0.1)
        money_bank01 = random.randint(int(amount * 0.2), int(amount * 0.4))
        money_mattress = random.randint(int(amount * 0.03), int(amount * 0.09))
        money_bank02 = amount - (money_401 + money_bank01 + money_mattress)
        return [money_401, money_bank01, money_mattress, money_bank02]

    def _generate_divided(self, analysis, details, rate_hist,
                          dividends_to_be_created):
        (
            symbol,
            user,
            acc,
            date,
        ) = details
        yymmdd_date = date.strftime("%Y-%m-%d")

        foreign_amount = (
            analysis[symbol]["current_foreign_value"]
            * analysis[symbol]["return"]
            * analysis[symbol]["amount"]
        )
        local_amount = (
            foreign_amount
            * rate_hist[yymmdd_date]
        )
        dividend = m.Dividend(
            account=acc,
            asset_text=symbol,
            local_amount=local_amount,
            foreign_amount=foreign_amount,
            currency=analysis[symbol]["foreign_currency"],
            rate=rate_hist[yymmdd_date],
            created_by=user,
            record_date=date,
        )
        dividends_to_be_created.append(dividend)

    def _alter_asset(self, analysis, asset, rate_hist, yymmdd_date):
        bought_foreign_value_total = analysis[asset][
            "bought_price_foreign"
        ]
        bought_local_value_total = analysis[asset][
            "bought_price_local"
        ]

        # Alter asset value: let it slightly increase over time
        # Note if the assets does NOT pay dividend, make its spread
        # wider
        fluctuation_rate = 1
        if analysis[asset]["return"] == 0:
            fluctuation_rate = random.uniform(1 - 0.15, 1 + 0.18)
        else:
            fluctuation_rate = random.uniform(1 - 0.02, 1 + 0.05)

        new_foreign_value_total = (
            analysis[asset]["current_foreign_value"]
            * fluctuation_rate
        )
        new_local_value_total = (
            new_foreign_value_total * rate_hist[yymmdd_date]
        )

        net_loss_foreign = (
            new_foreign_value_total - bought_foreign_value_total
        )
        net_loss_local = (
            new_local_value_total - bought_local_value_total
        )

        analysis[asset][
            "current_foreign_value"
        ] = new_foreign_value_total
        analysis[asset][
            "current_local_value"
        ] = new_local_value_total
        analysis[asset][
            "net_loss_foreign"
        ] = net_loss_foreign
        analysis[asset]["net_loss_local"] = net_loss_local
        analysis[asset]["record_date"] = yymmdd_date

    def _generate_dividend_and_rate_history(self, assets, assets_analysis, acc,
                                            user, today, rate, start_date):
        assets_to_be_created = []
        dividends_to_be_created = []
        yymmdd_start_date = start_date.strftime("%Y-%m-%d")

        # assets_historyical holds data saved to the db. This dictionary has
        # assets name as keys, and a dictionary as values
        # the inner dictionary has dates as keys, and Assets object as values
        assets_hist = {}
        # holds the change of the currency exchange rate over time
        rate_history = {}
        rate_history[yymmdd_start_date] = rate

        increase_tick = 3
        for asset in assets:
            assets_hist[asset] = {}
            for year in range(start_date.year, today.year + 1):
                for month in range(1, 13, 1):
                    first_day_of_the_month = datetime.date(year, month, 1)
                    yymmdd_date = first_day_of_the_month.strftime("%Y-%m-%d")

                    # Do not generate assets or dividends with future dates!
                    if first_day_of_the_month > datetime.date.today():
                        break

                    # Make USD/JPY exchange rate fluctuate monthly
                    # If this is the first asset we loop through for this month
                    # alter the rate, and save the generated fluctation/date.
                    # In this way all the assets will use the same exchange
                    # rate for every given month
                    if yymmdd_date not in rate_history:
                        # Simulate a small crash in the exchange rate of USD
                        # against JPY if USD gets too strong against JPY
                        # (i.e. more than 150)
                        if rate > 150:
                            increase_tick = -30
                        else:
                            # Othweise simulate USD steadily getting stronger
                            # over JPY
                            increase_tick = 3
                        rate += increase_tick
                        rate_history[yymmdd_date] = rate
                    else:
                        rate = rate_history[yymmdd_date]

                    # Create the asset for this month
                    asset_obj = self._create_asset(acc, assets_analysis, asset,
                                                   user,
                                                   rate_history,
                                                   first_day_of_the_month)
                    assets_to_be_created.append(asset_obj)
                    assets_hist[asset][yymmdd_date] = asset_obj

                    # Genereate dividends every three months. Note this will
                    # generate dividends on the first month of the asset
                    # ownership as well... but let let it be so. We do not
                    # have to be 100% realistic in this data generation
                    if month % 3 == 0 and assets_analysis[asset]["return"] > 0:
                        details = [asset, user, acc, first_day_of_the_month]
                        self._generate_divided(assets_analysis, details,
                                               rate_history,
                                               dividends_to_be_created)

                    self._alter_asset(assets_analysis, asset,
                                      rate_history, yymmdd_date)

        return (
          assets_to_be_created,
          dividends_to_be_created,
          assets_hist,
          rate_history
        )

    def _generate_assets_for_not_reinvested_cash(self,
                                                 dividends_to_be_created,
                                                 assets_to_be_created,
                                                 rate_hist, extra):
        (
          acc,
          user,
        ) = extra
        # IMPORTANT: Assume that assets only generate dividends in USD
        extra_assets = {}
        for a in dividends_to_be_created:
            if a.record_date in extra_assets:
                extra_assets[
                    a.record_date.strftime("%Y-%m-%d")
                ] += a.foreign_amount
            else:
                extra_assets[
                    a.record_date.strftime("%Y-%m-%d")
                ] = a.foreign_amount

        cumulative_exta_assets = {}
        cumulative_sum = 0
        for date, amount in extra_assets.items():
            cumulative_sum += amount
            cumulative_exta_assets[date] = cumulative_sum

        latest_asset_record_date = max(
            [a.record_date for a in assets_to_be_created]
        )
        latest_date_for_cash_usd_asset = None
        for date, amount in cumulative_exta_assets.items():
            latest_date_for_cash_usd_asset = date
            rate = rate_hist.get(date)
            # This should never happen, but let's set a value in casse
            if rate is None:
                rate = 112
                # and let's see if anyone is kind enough to report edge cases
                self._this_might_be_a_bug()
            asset_obj = m.Asset(
                account=acc,
                amount=1,
                text="Cash(USD)",
                created_by=user,
                foreign_currency="USD",
                current_foreign_value=amount,
                current_local_value=amount * rate,
                bought_price_foreign=amount,
                bought_price_local=amount * rate,
                net_loss_foreign=0,
                net_loss_local=0,
                rate=rate,
                record_date=date,
            )
            assets_to_be_created.append(asset_obj)

        # Since the Asset view ('/spa/assets') lists only assets for  the
        # latest date assets, we need to create one more assets for the 'raw'
        # USD cash, otherwise it will not be displayed
        if latest_date_for_cash_usd_asset < latest_asset_record_date.strftime(
            "%Y-%m-%d"
        ):
            details = [date, acc, amount, user, latest_asset_record_date]
            self._create_curr_month_of_usd_cash(rate_hist,
                                                details, assets_to_be_created)

    def _create_curr_month_of_usd_cash(self, currency_exchange_rate, details,
                                       assets_to_be_created):
        (
          date,
          acc,
          amount,
          user,
          latest_asset_record_date
        ) = details
        rate = currency_exchange_rate.get(date)
        asset_obj = m.Asset(
            account=acc,
            amount=amount,
            text="Cash(USD)",
            created_by=user,
            foreign_currency="USD",
            current_foreign_value=amount,
            current_local_value=amount * rate,
            bought_price_foreign=amount,
            bought_price_local=amount * rate,
            net_loss_foreign=0,
            net_loss_local=0,
            rate=rate,
            record_date=latest_asset_record_date,
        )
        assets_to_be_created.append(asset_obj)

    def _create_asset(self, acc, analysis, symbol, user, rate_hist, date_obj):
        needle = date_obj.strftime("%Y-%m-%d")
        # Create the asset for this month
        asset_obj = m.Asset(
            account=acc,
            amount=analysis[symbol]["amount"],
            text=symbol,
            created_by=user,
            foreign_currency=analysis[symbol][
                "foreign_currency"
            ],
            current_foreign_value=analysis[symbol][
                "current_foreign_value"
            ],
            current_local_value=analysis[symbol][
                "current_local_value"
            ],
            bought_price_foreign=analysis[symbol][
                "bought_price_foreign"
            ],
            bought_price_local=analysis[symbol][
                "bought_price_local"
            ],
            net_loss_foreign=analysis[symbol][
                "net_loss_foreign"
            ],
            rate=rate_hist[needle],
            net_loss_local=analysis[symbol]["net_loss_local"],
            record_date=date_obj,
        )
        return asset_obj

    def _write_assets_to_screen(self, user):
        ass_count = m.Asset.objects.filter(created_by=user).count()
        if ass_count > 0:
            self.stdout.write(
                self.style.SUCCESS(f"{ass_count} Asset(s) created.")
            )
        else:
            self.stdout.write(
                self.style.WARNING("No Asset created for this user")
            )

    def _write_dividends_to_screen(self, user):
        divid_count = m.Dividend.objects.filter(created_by=user).count()
        if divid_count > 0:
            self.stdout.write(
                self.style.SUCCESS(f"{divid_count} Dividend(s) created.")
            )
        else:
            self.stdout.write(
                self.style.WARNING("No Dividends created for this user")
            )

