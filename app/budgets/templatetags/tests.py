from django.template import Template, Context
from django.test import SimpleTestCase

class BudgetsTagsTest(SimpleTestCase):
    def test_help_text_to_div_filter_with_ul(self):
        input_template = Template('{% load budgets_tags %}{{ input_text|help_text_to_div|safe }}')
        input_text = '<ul><li>This is item 1</li><li>This is item 2</li></ul>'
        context = Context({'input_text': input_text})
        output = input_template.render(context)

        expected_output = '<div class="help-text"><small class="form-text text-muted">This is item 1</small><small class="form-text text-muted">This is item 2</small></div>'

        self.assertEqual(output, expected_output)

    def test_help_text_to_div_filter(self):
        input_template = Template('{% load budgets_tags %}{{ input_text|help_text_to_div|safe }}')
        inupts = [
          'Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.',
          'Enter the same password as before, for verification.'
        ]
        outputs = [
          '<small class="form-text text-muted">Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.</small>',
          '<small class="form-text text-muted">Enter the same password as before, for verification.</small>'
        ]

        for i in range(len(inupts)): # pylint: disable=C0200; # noqa
            input_text = inupts[i]
            context = Context({'input_text': input_text})
            output = input_template.render(context)
            expected_output = outputs[i]
            self.assertEqual(output, expected_output)
