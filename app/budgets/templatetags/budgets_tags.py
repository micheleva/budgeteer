# Copyright: (c) 2023, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

from django import template

register = template.Library()

# Credits https://stackoverflow.com/a/8000078
@register.filter(name='lookup')
def lookup(value, arg):
    return value[arg]

@register.filter
def help_text_to_div(value):
    if '<ul>' in value :
        value = value.replace('<ul>', '<div class="help-text">')\
        .replace('</ul>', '</div>')\
        .replace('<li>', '<small class="form-text text-muted">')\
        .replace('</li>', '</small>')
    else:
        value = f'<small class="form-text text-muted">{value}</small>'
    return value
