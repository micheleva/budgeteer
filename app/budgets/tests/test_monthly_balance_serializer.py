from django.test import TestCase, override_settings
from django.contrib.auth.models import User
from rest_framework.exceptions import MethodNotAllowed
from rest_framework.test import APIRequestFactory
from datetime import datetime
from budgets.models import MonthlyBalance, MonthlyBalanceCategory
from budgets.serializers import MonthlyBalanceSerializer
from budgets.tests.base import BaseTest


class MonthlyBalanceSerializerTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username=BaseTest.generate_string(20),
            password=BaseTest.generate_string(20),
        )
        self.category_local = MonthlyBalanceCategory.objects.create(
            text=BaseTest.generate_string(20), is_foreign_currency=False
        )
        self.category_foreign = MonthlyBalanceCategory.objects.create(
            text=BaseTest.generate_string(20), is_foreign_currency=True
        )
        self.monthly_balance_local = MonthlyBalance.objects.create(
            amount=1000,
            category=self.category_local,
            date=datetime(2023, 1, 1).date(),
            created_by=self.user,
        )
        self.monthly_balance_foreign = MonthlyBalance.objects.create(
            amount=100,
            category=self.category_foreign,
            date=datetime(2023, 1, 1).date(),
            created_by=self.user,
        )
        self.factory = APIRequestFactory()
        self.request = self.factory.get("/")
        self.request.user = self.user

    def test_serializer_fields(self):
        serializer = MonthlyBalanceSerializer(
            instance=self.monthly_balance_local
        )
        self.assertEqual(
            set(serializer.data.keys()),
            {
                "id",
                "amount",
                "category_id",
                "category_text",
                "category_is_foreign_currency",
                "date",
            },
        )

    def test_category_text_field(self):
        serializer = MonthlyBalanceSerializer(
            instance=self.monthly_balance_local
        )
        self.assertEqual(
            serializer.data["category_text"], self.category_local.text
        )

    def test_category_is_foreign_currency_field(self):
        serializer = MonthlyBalanceSerializer(
            instance=self.monthly_balance_foreign
        )
        self.assertTrue(serializer.data["category_is_foreign_currency"])

    @override_settings(EXCHANGE_RATE=2)
    def test_get_amount_local_currency(self):
        serializer = MonthlyBalanceSerializer(
            instance=self.monthly_balance_local
        )
        self.assertEqual(serializer.data["amount"], 1000)

    @override_settings(EXCHANGE_RATE=2)
    def test_get_amount_foreign_currency(self):
        serializer = MonthlyBalanceSerializer(
            instance=self.monthly_balance_foreign
        )
        self.assertEqual(serializer.data["amount"], 200)

    def test_get_amount_no_exchange_rate(self):
        with self.settings(EXCHANGE_RATE=None):
            serializer = MonthlyBalanceSerializer(
                instance=self.monthly_balance_foreign
            )
            self.assertEqual(serializer.data["amount"], 100)

    def test_create_method_not_allowed(self):
        serializer = MonthlyBalanceSerializer(
            data={}, context={"request": self.request}
        )
        with self.assertRaises(MethodNotAllowed):
            serializer.create({})

    def test_update_method_not_allowed(self):
        serializer = MonthlyBalanceSerializer(
            instance=self.monthly_balance_local,
            data={},
            context={"request": self.request},
        )
        with self.assertRaises(MethodNotAllowed):
            serializer.update(self.monthly_balance_local, {})
