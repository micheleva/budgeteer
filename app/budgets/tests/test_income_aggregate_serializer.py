from budgets.serializers import AggregateIncomeSerializer
from budgets.models import Income
from django.db.models import Sum
from django.db.models.functions import ExtractYear, ExtractMonth
from budgets.tests.base import BaseTest


class AggregateIncomeSerializerTestCase(BaseTest):
    def setUp(self):
        # Create some sample Income objects
        Income.objects.create(amount=1000, date="2023-01-15")
        Income.objects.create(amount=1500, date="2023-01-20")
        Income.objects.create(amount=2000, date="2023-02-10")
        Income.objects.create(amount=2500, date="2024-01-05")

    def test_serializer_output(self):
        # Aggregate the data as it would be in the view
        aggregated_data = (
            Income.objects.annotate(
                year=ExtractYear("date"), month=ExtractMonth("date")
            )
            .values("year", "month")
            .annotate(total_amount=Sum("amount"))
            .order_by("year", "month")
        )

        serializer = AggregateIncomeSerializer(aggregated_data, many=True)

        expected_data = [
            {"year": 2023, "month": 1, "total_amount": 2500},
            {"year": 2023, "month": 2, "total_amount": 2000},
            {"year": 2024, "month": 1, "total_amount": 2500},
        ]

        self.assertEqual(serializer.data, expected_data)

    def test_get_year_method(self):
        serializer = AggregateIncomeSerializer()
        obj = {"year": 2023}
        self.assertEqual(serializer.get_year(obj), 2023)

    def test_get_month_method(self):
        serializer = AggregateIncomeSerializer()
        obj = {"month": 6}
        self.assertEqual(serializer.get_month(obj), 6)

    def test_fields(self):
        serializer = AggregateIncomeSerializer()
        self.assertEqual(
            set(serializer.fields.keys()), {"year", "month", "total_amount"}
        )

    def test_model(self):
        self.assertEqual(AggregateIncomeSerializer.Meta.model, Income)
