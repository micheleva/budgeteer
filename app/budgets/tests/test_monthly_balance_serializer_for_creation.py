from django.test import TestCase
from django.contrib.auth.models import User
from rest_framework.exceptions import ValidationError
from datetime import datetime
from budgets.models import MonthlyBalance, MonthlyBalanceCategory
from budgets.serializers import MonthlyBalanceSerializerForCreation
from budgets.tests.base import BaseTest


class MonthlyBalanceSerializerForCreationTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username=BaseTest.generate_string(20),
            password=BaseTest.generate_string(20),
        )
        self.category = MonthlyBalanceCategory.objects.create(
            text=BaseTest.generate_string(20), created_by=self.user
        )

    def test_validate_date_first_day(self):
        serializer = MonthlyBalanceSerializerForCreation()
        date = datetime(2023, 1, 1).date()
        self.assertEqual(serializer.validate_date(date), date)

    def test_validate_date_not_first_day(self):
        serializer = MonthlyBalanceSerializerForCreation()
        date = datetime(2023, 1, 15).date()
        expected_date = datetime(2023, 1, 1).date()
        self.assertEqual(serializer.validate_date(date), expected_date)

    def test_validate_category_valid(self):
        serializer = MonthlyBalanceSerializerForCreation(
            context={"user_id": self.user.id}
        )
        self.assertEqual(
            serializer.validate_category(self.category), self.category
        )

    def test_validate_category_none(self):
        serializer = MonthlyBalanceSerializerForCreation()
        with self.assertRaises(ValidationError):
            serializer.validate_category(None)

    def test_validate_category_empty_string(self):
        serializer = MonthlyBalanceSerializerForCreation()
        with self.assertRaises(ValidationError):
            serializer.validate_category("")

    def test_validate_category_not_exists(self):
        non_existent_category = MonthlyBalanceCategory(id=9999)
        serializer = MonthlyBalanceSerializerForCreation(
            context={"user_id": self.user.id}
        )
        with self.assertRaises(ValidationError):
            serializer.validate_category(non_existent_category)

    def test_validate_unique_constraint(self):
        _ = MonthlyBalance.objects.create(
            amount=1000,
            date=datetime(2023, 1, 1).date(),
            category=self.category,
            created_by=self.user,
        )
        data = {
            "amount": 2000,
            "date": datetime(2023, 1, 1).date(),
            "category": self.category,
        }
        serializer = MonthlyBalanceSerializerForCreation(
            context={"user_id": self.user.id}
        )
        with self.assertRaises(ValidationError):
            serializer.validate(data)

    def test_validate_unique_constraint_different_date(self):
        _ = MonthlyBalance.objects.create(
            amount=1000,
            date=datetime(2023, 1, 1).date(),
            category=self.category,
            created_by=self.user,
        )
        data = {
            "amount": 2000,
            "date": datetime(2023, 2, 1).date(),
            "category": self.category,
        }
        serializer = MonthlyBalanceSerializerForCreation(
            context={"user_id": self.user.id}
        )
        self.assertEqual(serializer.validate(data), data)

    def test_validate_missing_category(self):
        data = {"amount": 1000, "date": datetime(2023, 1, 1).date()}
        serializer = MonthlyBalanceSerializerForCreation(
            context={"user_id": self.user.id}
        )
        with self.assertRaises(ValidationError):
            serializer.validate(data)

    def test_validate_none_category(self):
        data = {
            "amount": 1000,
            "date": datetime(2023, 1, 1).date(),
            "category": None,
        }
        serializer = MonthlyBalanceSerializerForCreation(
            context={"user_id": self.user.id}
        )
        with self.assertRaises(ValidationError):
            serializer.validate(data)

    def test_serializer_fields(self):
        serializer = MonthlyBalanceSerializerForCreation()
        self.assertEqual(
            set(serializer.fields.keys()), {"id", "amount", "date", "category"}
        )
