from django.test import TestCase

from rest_framework.exceptions import ValidationError

from budgets.views_utils import check_for_none_values


class CheckForNoneValuesTestCase(TestCase):
    class MockSerializer:
        def __init__(self, validated_data):
            self.validated_data = validated_data

    def setUp(self):
        # Base data containing all required fields
        self.base_data = {
            "text": "Test Account",
            "current_foreign_value": 100,
            "current_local_value": 100,
            "bought_price_local": 50,
            "bought_price_foreign": 50,
            "net_loss_foreign": 10,
            "net_loss_local": 10,
            "foreign_currency": "USD",
            "record_date": "2023-01-01",
        }

    def test_missing_text(self):
        data = self.base_data.copy()
        data["text"] = None  # Set to None for this test

        serializer = self.MockSerializer(data)

        with self.assertRaises(ValidationError) as context:
            check_for_none_values(serializer)

        self.assertEqual(
            context.exception.detail, {"text": ["This field is required."]}
        )

    def test_missing_current_foreign_value(self):
        data = self.base_data.copy()
        data["current_foreign_value"] = None  # Set to None for this test

        serializer = self.MockSerializer(data)

        with self.assertRaises(ValidationError) as context:
            check_for_none_values(serializer)

        self.assertEqual(
            context.exception.detail,
            {"current_foreign_value": ["This field is required."]},
        )

    def test_missing_current_local_value(self):
        data = self.base_data.copy()
        data["current_local_value"] = None  # Set to None for this test

        serializer = self.MockSerializer(data)

        with self.assertRaises(ValidationError) as context:
            check_for_none_values(serializer)

        self.assertEqual(
            context.exception.detail,
            {"current_local_value": ["This field is required."]},
        )

    def test_missing_bought_price_local(self):
        data = self.base_data.copy()
        data["bought_price_local"] = None  # Set to None for this test

        serializer = self.MockSerializer(data)

        with self.assertRaises(ValidationError) as context:
            check_for_none_values(serializer)

        self.assertEqual(
            context.exception.detail,
            {"bought_price_local": ["This field is required."]},
        )

    def test_missing_bought_price_foreign(self):
        data = self.base_data.copy()
        data["bought_price_foreign"] = None  # Set to None for this test

        serializer = self.MockSerializer(data)

        with self.assertRaises(ValidationError) as context:
            check_for_none_values(serializer)

        self.assertEqual(
            context.exception.detail,
            {"bought_price_foreign": ["This field is required."]},
        )

    def test_missing_net_loss_foreign(self):
        data = self.base_data.copy()
        data["net_loss_foreign"] = None  # Set to None for this test

        serializer = self.MockSerializer(data)

        with self.assertRaises(ValidationError) as context:
            check_for_none_values(serializer)

        self.assertEqual(
            context.exception.detail,
            {"net_loss_foreign": ["This field is required."]},
        )

    def test_missing_net_loss_local(self):
        data = self.base_data.copy()
        data["net_loss_local"] = None  # Set to None for this test

        serializer = self.MockSerializer(data)

        with self.assertRaises(ValidationError) as context:
            check_for_none_values(serializer)

        self.assertEqual(
            context.exception.detail,
            {"net_loss_local": ["This field is required."]},
        )

    def test_missing_foreign_currency(self):
        data = self.base_data.copy()
        data["foreign_currency"] = None  # Set to None for this test

        serializer = self.MockSerializer(data)

        with self.assertRaises(ValidationError) as context:
            check_for_none_values(serializer)

        self.assertEqual(
            context.exception.detail,
            {"foreign_currency": ["This field is required."]},
        )

    def test_missing_record_date(self):
        data = self.base_data.copy()
        data["record_date"] = None  # Set to None for this test

        serializer = self.MockSerializer(data)

        with self.assertRaises(ValidationError) as context:
            check_for_none_values(serializer)

        self.assertEqual(
            context.exception.detail,
            {"record_date": ["This field is required."]},
        )
