from django.test import TestCase
from django.contrib.auth.models import User
from rest_framework.exceptions import ValidationError
from rest_framework.test import APIRequestFactory
from budgets.models import (
    SpendingLimit,
    Category,
)  # Adjust the import based on your project structure
from budgets.serializers import SpendingLimitPartialUpdateSerializer
from budgets.tests.base import BaseTest


class SpendingLimitPartialUpdateSerializerTestCase(TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.user = User.objects.create_user(
            username=BaseTest.generate_string(20),
            password=BaseTest.generate_string(20),
        )
        self.category = Category.objects.create(
            text=BaseTest.generate_string(10), created_by=self.user
        )
        self.existing_limit = SpendingLimit.objects.create(
            amount=100,
            currency="USD",
            days=30,
            text="Monthly Limit",
            category=self.category,
            created_by=self.user,
        )

    def test_partial_update_valid_data(self):
        data = {
            "amount": 150,
            "currency": "USD",
            "days": 30,
            "text": "Updated Limit",
        }

        # Create a request object with the user
        request = self.factory.patch(
            "/spending-limits/{}/".format(self.existing_limit.id)
        )
        request.user = self.user

        serializer = SpendingLimitPartialUpdateSerializer(
            instance=self.existing_limit,
            data=data,
            context={"request": request},
        )

        self.assertTrue(serializer.is_valid())
        updated_limit = serializer.save()

        self.assertEqual(updated_limit.amount, 150)
        self.assertEqual(updated_limit.text, "Updated Limit")

    def test_partial_update_with_invalid_category(self):
        invalid_category = Category.objects.create(
            text=BaseTest.generate_string(10),
            created_by=User.objects.create_user(
                username="other_user", password="12345"
            ),
        )

        data = {
            "category": invalid_category.id,  # Pass the ID of the invalid category
            "amount": 150,
            "currency": "USD",
        }

        # Create a request object with the user
        request = self.factory.patch(
            "/spending-limits/{}/".format(self.existing_limit.id)
        )
        request.user = self.user

        serializer = SpendingLimitPartialUpdateSerializer(
            instance=self.existing_limit,
            data=data,
            context={"request": request},
        )

        with self.assertRaises(ValidationError) as context:
            serializer.is_valid(raise_exception=True)

        # Check if the error message is in the category field
        self.assertIn("category", serializer.errors)
        self.assertEqual(
            serializer.errors["category"][0],
            f'Invalid pk "{invalid_category.id}" - object does not exist.',
        )

    def test_prevent_duplicate_spending_limit(self):
        # Create a duplicate spending limit that matches all fields
        duplicate_limit = SpendingLimit.objects.create(
            amount=100,
            currency="USD",
            days=30,
            text="Monthly Limit",
            category=self.category,
            created_by=self.user,
        )

        # Prepare data that would create a duplicate
        data = {
            "amount": 100,
            "currency": "USD",
            "days": 30,
            "text": "Monthly Limit",
            "category": self.category.id,  # Ensure category is included
        }

        # Create a request object with the user
        request = self.factory.patch(
            "/spending-limits/{}/".format(self.existing_limit.id)
        )
        request.user = self.user

        serializer = SpendingLimitPartialUpdateSerializer(
            instance=self.existing_limit,
            data=data,
            context={"request": request},
        )

        with self.assertRaises(ValidationError) as context:
            serializer.is_valid(raise_exception=True)

        # Check if the error message is in non_field_errors
        self.assertIn("non_field_errors", serializer.errors)
        self.assertEqual(
            serializer.errors["non_field_errors"][0],
            "A SpendingLimit for this Category with the same currency, amount, days, and text already exists for this user.",
        )

    def test_update_with_no_changes(self):
        data = {"category": self.category.id}  # No changes in other fields

        # Create a request object with the user
        request = self.factory.patch(
            "/spending-limits/{}/".format(self.existing_limit.id)
        )
        request.user = self.user

        serializer = SpendingLimitPartialUpdateSerializer(
            instance=self.existing_limit,
            data=data,
            context={"request": request},
        )

        self.assertTrue(serializer.is_valid())

    def test_update_with_missing_fields(self):
        data = {
            # Intentionally leaving out fields to test partial update
            # Omit amount and currency entirely instead of setting them to None
            "days": 30,  # Keeping this field for testing
            "text": "Updated Limit",  # Keeping this field for testing
            # Other fields omitted
        }

        # Create a request object with the user
        request = self.factory.patch(
            "/spending-limits/{}/".format(self.existing_limit.id)
        )
        request.user = self.user

        serializer = SpendingLimitPartialUpdateSerializer(
            instance=self.existing_limit,
            data=data,
            context={"request": request},
        )

        self.assertTrue(serializer.is_valid())  # Should pass validation
