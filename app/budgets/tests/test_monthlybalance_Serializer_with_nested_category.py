from django.test import TestCase
from django.contrib.auth.models import User
from rest_framework.exceptions import MethodNotAllowed, ValidationError
from rest_framework.test import APIRequestFactory
from datetime import datetime
from budgets.models import MonthlyBalance, MonthlyBalanceCategory
from budgets.serializers import (
    MonthlyBalanceSerializerWithNestedCategory,
    MonthlyBalanceCategoryBasicSerializer,
)
from budgets.tests.base import BaseTest


class MonthlyBalanceSerializerWithNestedCategoryTestCase(TestCase):
    def setUp(self):
        super().setUp()
        self.user = User.objects.create_user(
            username=BaseTest.generate_string(20),
            password=BaseTest.generate_string(20),
        )
        self.category_text = BaseTest.generate_string(20)
        self.category = MonthlyBalanceCategory.objects.create(
            text=self.category_text, created_by=self.user
        )
        self.monthly_balance = MonthlyBalance.objects.create(
            amount=1000,
            category=self.category,
            date=datetime(2023, 1, 1).date(),
            created_by=self.user,
        )
        self.factory = APIRequestFactory()
        self.request = self.factory.get("/")
        self.request.user = self.user

    def test_to_representation(self):
        serializer = MonthlyBalanceSerializerWithNestedCategory(
            self.monthly_balance, context={"request": self.request}
        )
        data = serializer.to_representation(self.monthly_balance)
        self.assertIn("category", data)
        self.assertEqual(
            data["category"],
            MonthlyBalanceCategoryBasicSerializer(self.category).data,
        )

    def test_validate_date(self):
        serializer = MonthlyBalanceSerializerWithNestedCategory()
        date = datetime(2023, 1, 15).date()
        validated_date = serializer.validate_date(date)
        self.assertEqual(validated_date, datetime(2023, 1, 1).date())

    def test_create_method_not_allowed(self):
        serializer = MonthlyBalanceSerializerWithNestedCategory(
            data={}, context={"request": self.request}
        )
        with self.assertRaises(MethodNotAllowed):
            serializer.create({})

    def test_validate_unique_constraint(self):
        data = {
            "amount": 2000,
            "category": self.category,
            "date": datetime(2023, 1, 1).date(),
        }
        serializer = MonthlyBalanceSerializerWithNestedCategory(
            data=data, context={"request": self.request}
        )
        with self.assertRaises(ValidationError):
            serializer.validate(data)

    def test_validate_unique_constraint_update(self):
        new_monthly_balance = MonthlyBalance.objects.create(
            amount=2000,
            category=self.category,
            date=datetime(2023, 2, 1).date(),  # New date
            created_by=self.user,
        )
        data = {
            "amount": 3000,
            "category": self.category,
            # Same date as existing monthly balance (self.monthly_balance)
            "date": datetime(2023, 1, 1).date(),
        }
        serializer = MonthlyBalanceSerializerWithNestedCategory(
            instance=new_monthly_balance,
            data=data,
            context={"request": self.request},
        )
        with self.assertRaises(ValidationError):
            print(serializer.validate(data))
            serializer.validate(data)

    def test_fields(self):
        serializer = MonthlyBalanceSerializerWithNestedCategory()
        self.assertEqual(
            set(serializer.fields.keys()), {"id", "amount", "category", "date"}
        )

    def test_read_only_fields(self):
        serializer = MonthlyBalanceSerializerWithNestedCategory()
        self.assertTrue(serializer.fields["category"].read_only)
        self.assertTrue(serializer.fields["date"].read_only)
