# Copyright: (c) 2019, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
# pylint: disable=E1101

from budgets.tests.base import BaseTest
import budgets.views as v


class HomePageTest(BaseTest):
    """Unit tests related to the home page"""

    @BaseTest.login
    def test_title_is_displayed(self):
        """Check the view title"""
        self.check_if_title_is_displayed("budgets:home", "Budgeteer")

    @BaseTest.login
    def test_uses_correct_view(self):
        self.check_if_correct_view("budgets:home", v.spa)

    @BaseTest.login
    def test_uses_correct_template(self):
        response = self.get_response_from_named_url("budgets:home")
        self.assertTemplateUsed(response, "spa.html")
