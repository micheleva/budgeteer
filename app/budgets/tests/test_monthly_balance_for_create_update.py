import random
from django.test import TestCase
from django.contrib.auth.models import User
from rest_framework.exceptions import ValidationError
from datetime import datetime
from budgets.models import MonthlyBalance, MonthlyBalanceCategory
from budgets.serializers import MonthlyBalanceSerializerForCreateUpdate
from budgets.tests.base import BaseTest


class MonthlyBalanceSerializerForCreateUpdateTestCase(TestCase):
    def setUp(self):
        self.user = User.objects.create_user(
            username=BaseTest.generate_string(20),
            password=BaseTest.generate_string(20),
        )
        self.category = MonthlyBalanceCategory.objects.create(
            text=BaseTest.generate_string(20), created_by=self.user
        )

    def test_validate_date_first_day(self):
        serializer = MonthlyBalanceSerializerForCreateUpdate()
        date = datetime(2023, 1, 1).date()
        self.assertEqual(serializer.validate_date(date), date)

    def test_validate_date_not_first_day(self):
        serializer = MonthlyBalanceSerializerForCreateUpdate()
        date = datetime(2023, 1, 15).date()
        expected_date = datetime(2023, 1, 1).date()
        self.assertEqual(serializer.validate_date(date), expected_date)

    def test_validate_category_valid(self):
        serializer = MonthlyBalanceSerializerForCreateUpdate(
            context={"user_id": self.user.id}
        )
        self.assertEqual(
            serializer.validate_category(self.category), self.category
        )

    def test_validate_category_none(self):
        serializer = MonthlyBalanceSerializerForCreateUpdate()
        with self.assertRaises(ValidationError):
            serializer.validate_category(None)

    def test_validate_category_empty_string(self):
        serializer = MonthlyBalanceSerializerForCreateUpdate()
        with self.assertRaises(ValidationError):
            serializer.validate_category("")

    def test_validate_category_not_exists(self):
        non_existent_category = MonthlyBalanceCategory(id=9999)
        serializer = MonthlyBalanceSerializerForCreateUpdate(
            context={"user_id": self.user.id}
        )
        with self.assertRaises(ValidationError):
            serializer.validate_category(non_existent_category)

    def test_create_new_monthly_balance(self):
        amount = random.randint(1, 90000)
        data = {
            "amount": amount,
            "date": datetime(2023, 1, 1).date(),
            "category": self.category,
            "created_by": self.user,
        }
        serializer = MonthlyBalanceSerializerForCreateUpdate(
            context={"user_id": self.user.id}
        )
        instance = serializer.create(data)
        self.assertTrue(instance.created)
        self.assertEqual(instance.amount, amount)
        self.assertEqual(instance.date, datetime(2023, 1, 1).date())
        self.assertEqual(instance.category, self.category)
        self.assertEqual(instance.created_by, self.user)

    def test_update_existing_monthly_balance(self):
        initial_amount = random.randint(1, 90000)
        existing_balance = MonthlyBalance.objects.create(
            amount=initial_amount,
            date=datetime(2023, 1, 1).date(),
            category=self.category,
            created_by=self.user,
        )
        new_amount = random.randint(1, 90000)
        data = {
            "amount": new_amount,
            "date": datetime(2023, 1, 1).date(),
            "category": self.category,
            "created_by": self.user,
        }
        serializer = MonthlyBalanceSerializerForCreateUpdate(
            context={"user_id": self.user.id}
        )
        instance = serializer.create(data)
        self.assertFalse(instance.created)
        self.assertEqual(instance.amount, new_amount)
        self.assertEqual(instance.date, datetime(2023, 1, 1).date())
        self.assertEqual(instance.category, self.category)
        self.assertEqual(instance.created_by, self.user)

    def test_serializer_fields(self):
        serializer = MonthlyBalanceSerializerForCreateUpdate()
        self.assertEqual(
            set(serializer.fields.keys()), {"amount", "date", "category"}
        )
