from datetime import date
import random

from django.contrib.auth.models import User

from api.v1.tests.test_base import BaseApiTest
from budgets.models import Income, IncomeCategory
from budgets.serializers import IncomeSerializer
from budgets.tests.base import BaseTest


class IncomeSerializerTestCase(BaseTest):
    def setUp(self):
        # Instrument the unit tests to fail if they run for too long
        super().setUp()
        self.user = User.objects.create_user(
            username=BaseTest.generate_string(20),
            password=BaseTest.generate_string(20),
        )
        self.category_text = BaseTest.generate_string(20)
        self.category = IncomeCategory.objects.create(
            text=self.category_text, created_by=self.user
        )
        self.incme_text = BaseTest.generate_string(20)
        self.income_amount = random.randint(1, 1000)
        self.income_date = date.fromisoformat(
            str(BaseApiTest.generate_date(364, 0).date())
        )
        self.income_with_category = Income.objects.create(
            amount=self.income_amount,
            category=self.category,
            note=self.incme_text,
            date=self.income_date,
            created_by=self.user,
        )
        self.income_amount_without_cat = random.randint(1, 50)
        self.income_without_cat_note = BaseTest.generate_string(20)
        self.income_without_category = Income.objects.create(
            amount=self.income_amount_without_cat,
            category=None,
            note=self.income_without_cat_note,
            date=self.income_date,
            created_by=self.user,
        )

    def test_get_category_text_with_category(self):
        serializer = IncomeSerializer(instance=self.income_with_category)
        self.assertEqual(serializer.data["category_text"], self.category_text)

    def test_get_category_text_without_category(self):
        serializer = IncomeSerializer(instance=self.income_without_category)
        self.assertIsNone(serializer.data["category_text"])

    def test_serializer_fields(self):
        serializer = IncomeSerializer(instance=self.income_with_category)
        expected_fields = set(
            ["id", "amount", "category_id", "category_text", "note", "date"]
        )
        self.assertEqual(set(serializer.data.keys()), expected_fields)

    def test_serializer_data_with_category(self):
        serializer = IncomeSerializer(instance=self.income_with_category)
        self.assertEqual(serializer.data["amount"], self.income_amount)
        self.assertEqual(serializer.data["category_id"], self.category.id)
        self.assertEqual(serializer.data["category_text"], self.category_text)
        self.assertEqual(serializer.data["note"], self.incme_text)
        self.assertEqual(serializer.data["date"], str(self.income_date))

    def test_serializer_data_without_category(self):
        serializer = IncomeSerializer(instance=self.income_without_category)
        self.assertEqual(
            serializer.data["amount"], self.income_amount_without_cat
        )
        self.assertIsNone(serializer.data["category_id"])
        self.assertIsNone(serializer.data["category_text"])
        self.assertEqual(serializer.data["note"], self.income_without_cat_note)
        self.assertEqual(serializer.data["date"], str(self.income_date))
