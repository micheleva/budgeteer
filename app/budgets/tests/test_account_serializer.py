from unittest import skip

from django.test import TestCase
from django.contrib.auth.models import User

from rest_framework.exceptions import ValidationError
from rest_framework.test import APIRequestFactory

from budgets.models import (
    Account,
)  # Adjust the import based on your project structure
from budgets.serializers import AccountSerializer
from budgets.tests.base import BaseTest


class AccountSerializerTestCase(TestCase):
    def setUp(self):
        self.factory = APIRequestFactory()
        self.user = User.objects.create_user(
            username=BaseTest.generate_string(20),
            password=BaseTest.generate_string(20),
        )

    def test_validate_unique_account(self):
        # Create an account to test against
        existing_text = BaseTest.generate_string(10)
        existing_credit_entity = BaseTest.generate_string(10)

        Account.objects.create(
            text=existing_text,
            credit_entity=existing_credit_entity,
            created_by=self.user,
        )

        # Prepare data for a new account with the same text and credit entity
        data = {
            "text": existing_text,
            "credit_entity": existing_credit_entity,
        }

        # Create a request object with the user
        request = self.factory.post("/accounts/", data)
        request.user = self.user

        serializer = AccountSerializer(data=data, context={"request": request})

        # Validate should raise a ValidationError due to duplicate account
        with self.assertRaises(ValidationError) as context:
            serializer.is_valid(raise_exception=True)

        # Check if the error message is in non_field_errors
        self.assertIn("non_field_errors", serializer.errors)
        self.assertEqual(
            serializer.errors["non_field_errors"][0],
            "An Account with the same text AND credit_entity already exists for this user.",
        )

    def test_validate_unique_account_different_credit_entity(self):
        # Create an account with a different credit entity
        existing_text = BaseTest.generate_string(10)

        Account.objects.create(
            text=existing_text,
            credit_entity=BaseTest.generate_string(
                10
            ),  # Different credit entity
            created_by=self.user,
        )

        # Prepare data for a new account with the same text but different credit entity
        data = {
            "text": existing_text,
            "credit_entity": BaseTest.generate_string(10),  # New credit entity
        }

        # Create a request object with the user
        request = self.factory.post("/accounts/", data)
        request.user = self.user

        serializer = AccountSerializer(data=data, context={"request": request})

        # Validate should succeed since the credit entity is different
        self.assertTrue(serializer.is_valid())

    def test_validate_update_existing_account(self):
        # Create an initial account
        existing_text = BaseTest.generate_string(10)
        existing_credit_entity = BaseTest.generate_string(10)

        existing_account = Account.objects.create(
            text=existing_text,
            credit_entity=existing_credit_entity,
            created_by=self.user,
        )

        # Prepare data for updating the existing account with the same text and credit entity
        data = {
            "text": existing_text,
            "credit_entity": existing_credit_entity,
        }

        # Create a request object with the user
        request = self.factory.put(f"/accounts/{existing_account.id}/", data)
        request.user = self.user

        serializer = AccountSerializer(
            instance=existing_account, data=data, context={"request": request}
        )

        # Validate should succeed since we are updating the existing account
        self.assertTrue(serializer.is_valid())

    # FIXME BUGFIX-257: catch db integrity exceptions in  api.v1.views.AccountDetail()
    @skip
    def test_validate_update_to_duplicate_account(self):
        # Create two accounts to test against
        duplicate_text = BaseTest.generate_string(10)
        duplicate_credit_entity = BaseTest.generate_string(10)

        existing_account = Account.objects.create(
            text=duplicate_text,
            credit_entity=duplicate_credit_entity,
            created_by=self.user,
        )

        duplicate_account = Account.objects.create(
            text=duplicate_text,
            credit_entity=duplicate_credit_entity,
            created_by=self.user,
        )

        # Prepare data for updating the existing account to duplicate values
        data = {
            "text": duplicate_text,
            "credit_entity": duplicate_credit_entity,
        }

        # Create a request object with the user
        request = self.factory.put(f"/accounts/{existing_account.id}/", data)
        request.user = self.user

        serializer = AccountSerializer(
            instance=existing_account, data=data, context={"request": request}
        )

        # Validate should raise a ValidationError due to duplicate account values
        with self.assertRaises(ValidationError) as context:
            serializer.is_valid(raise_exception=True)

        self.assertEqual(
            str(context.exception),
            "An Account with the same text AND credit_entity already exists for this user.",
        )

    def test_serializer_fields(self):
        serializer = AccountSerializer()

        # Check if all expected fields are present in the serializer
        expected_fields = {
            "id",
            "text",
            "credit_entity",
            "is_foreign_currency",
        }

        self.assertEqual(set(serializer.fields.keys()), expected_fields)
