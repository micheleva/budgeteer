# Copyright: (c) 2019, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from django.db import transaction
from django.core.exceptions import ValidationError
from decimal import Decimal

import budgets.models as m
from budgets.tests.base import BaseTest


class ModelsTest(BaseTest):
    def test_models_to_string(self):
        text = "Rent"
        category = self.create_category(text)
        string_repr = f"{category.text}"
        self.assertEqual("%s" % category, text)

        amount = Decimal("5000.01")
        note = "First month of rent"
        date = "2019-08-04"
        _ = self.create_expense(
            category=category, amount=amount, note=note, date=date
        )
        second_expense = self.create_expense(
            category=category,
            amount=amount,
            note=note,
            date=date,
        )
        string_repr = f"{category.id}: ({note}), {amount:.2f}, {date}"
        self.assertEqual("%s" % second_expense, string_repr)

        inc_category = self.create_income_category("Wage")
        string_repr = f"{inc_category.text}"
        self.assertEqual("%s" % inc_category, "Wage")

        inc_amount = 5000
        inc_note = "August Wage"
        inc_date = "2019-08-10"
        first_income = self.create_income(
            category=inc_category,
            amount=inc_amount,
            note=inc_note,
            date=inc_date,
        )
        string_repr = (
            f"{first_income.category.id}: {inc_amount}, {inc_note}, {inc_date}"
        )
        self.assertEqual("%s" % first_income, string_repr)

        b_category = self.create_monthly_balance_category("Bank")
        string_repr = f"{b_category.text}"
        self.assertEqual("%s" % b_category, string_repr)

        b_amount = 5000
        m_balance = self.create_monthly_balance(b_category, b_amount, date)
        string_repr = f"{m_balance.id}: {b_amount}, {date}"
        self.assertEqual("%s" % m_balance, string_repr)

        amount = 5000
        text = "My first goal"
        note = "Rainy days founds"
        goal = self.create_goal(amount, text, note)
        string_repr = f"{goal.id}: {amount}, {text}"
        self.assertEqual("%s" % goal, string_repr)

    def test_saving_and_retrieving_expenses(self):
        category = self.create_category("Rent")

        first_expense = self.create_expense(
            category=category,
            amount=5000,
            note="First month of rent",
            date="2019-08-04",
        )
        # Storing the string here to fit into 79 chars limit (PEP8)
        note_field_2 = "Second month of rent (discounted) in advance"
        second_expense = self.create_expense(
            category=category,
            amount=4200,
            note=note_field_2,
            date="2019-09-04",
        )

        saved_category = (
            m.Category.objects.first()
        )  # pylint: disable=E1101; # noqa
        saved_expenses = (
            m.Expense.objects.all()
        )  # pylint: disable=E1101; # noqa
        saved_item_1 = saved_expenses[0]
        saved_item_2 = saved_expenses[1]

        self.assertEqual(saved_category, category)
        self.assertEqual(saved_expenses.count(), 2)

        self.assertEqual(saved_item_1.category, category)
        self.assertEqual(saved_item_1.amount, first_expense.amount)
        self.assertEqual(saved_item_1.note, first_expense.note)
        self.assertEqual(str(saved_item_1.date), str(first_expense.date))

        self.assertEqual(saved_item_2.category, category)
        self.assertEqual(saved_item_2.amount, second_expense.amount)
        self.assertEqual(saved_item_2.note, note_field_2)
        self.assertEqual(str(saved_item_2.date), str(second_expense.date))

    def test_saving_and_retrieving_incomes(self):
        category = self.create_income_category("Rent")
        first_income = self.create_income(
            category=category,
            amount=10000,
            note="August Wage",
            date="2019-08-10",
        )
        second_income = self.create_income(
            category=category,
            amount=30000,
            note="September Wage",
            date="2019-09-10",
        )

        saved_category = (
            m.IncomeCategory.objects.first()
        )  # pylint: disable=E1101; # noqa
        saved_expenses = (
            m.Income.objects.all()
        )  # pylint: disable=E1101; # noqa
        saved_item_1 = saved_expenses[0]
        saved_item_2 = saved_expenses[1]

        self.assertEqual(saved_category, category)
        self.assertEqual(saved_expenses.count(), 2)

        self.assertEqual(saved_item_1.category, category)
        self.assertEqual(saved_item_1.amount, first_income.amount)
        self.assertEqual(saved_item_1.note, first_income.note)
        self.assertEqual(
            str(saved_item_1.date), str(first_income.date)
        )  #'2019-08-10'

        self.assertEqual(saved_item_2.category, category)
        self.assertEqual(saved_item_2.amount, second_income.amount)
        self.assertEqual(saved_item_2.note, second_income.note)
        self.assertEqual(
            str(saved_item_2.date), str(second_income.date)
        )  # 2019-09-10'

    def test_malformed_categories_triggers_errors(self):
        # None is not allowed
        with transaction.atomic():
            with self.assertRaises(ValidationError) as e:
                _ = self.create_category(None)
            self.assertEqual(ValidationError, type(e.exception))

        # Empty strigs are not allowed
        with transaction.atomic():
            with self.assertRaises(ValidationError) as e:
                _ = self.create_category("")
            self.assertEqual(ValidationError, type(e.exception))

        # String longer than 20 chas are not allowed either
        with transaction.atomic():
            with self.assertRaises(ValidationError) as e:
                category = self.create_category(
                    "this text is exactly 1 char more than 40!"
                )
            self.assertEqual(ValidationError, type(e.exception))

    def test_duplicates_categories_triggers_errors(self):
        _ = self.create_category("Rent")
        with self.assertRaises(ValidationError) as e:
            _ = self.create_category("Rent")

    def test_malformed_income_categories_triggers_errors(self):
        # None is not allowed
        with transaction.atomic():
            with self.assertRaises(ValidationError) as e:
                _ = self.create_income_category(None)
            self.assertEqual(ValidationError, type(e.exception))

        # Empty strigs are not allowed
        with transaction.atomic():
            with self.assertRaises(ValidationError) as e:
                _ = self.create_income_category("")
            self.assertEqual(ValidationError, type(e.exception))

        # String longer than 20 chas are not allowed either
        with transaction.atomic():
            with self.assertRaises(ValidationError) as e:
                _ = self.create_income_category(
                    "----------------text longer than constraints--------------"
                )
            self.assertEqual(ValidationError, type(e.exception))

    def test_duplicates_income_categories_triggers_errors(self):
        _ = self.create_income_category("Wage")
        with self.assertRaises(ValidationError) as e:
            _ = self.create_income_category("Wage")

    def test_malformed_expenses_triggers_errors(self):
        category = self.create_category("Rent")

        # No category
        with transaction.atomic():
            with self.assertRaises(ValidationError) as e:
                _ = self.create_expense(
                    category=None,
                    amount=5000,
                    note="Rent for August 2019",
                    date="2019-08-04",
                )
            self.assertEqual(ValidationError, type(e.exception))

        # amount field: None is not allowed
        with transaction.atomic():
            with self.assertRaises(ValidationError) as e:
                _ = self.create_expense(
                    category=category,
                    amount=None,
                    note="Rent for August 2019",
                    date="2019-08-04",
                )
            self.assertEqual(ValidationError, type(e.exception))

        # amount field: negative numbers are not allowed
        with transaction.atomic():
            with self.assertRaises(ValidationError) as e:
                _ = self.create_expense(
                    category=category,
                    amount=-42,
                    note="Rent for August 2019",
                    date="2019-08-01",
                )
            self.assertEqual(ValidationError, type(e.exception))
            self.assertEqual(
                str(e.exception),
                "{'amount': ['%s should be more than 0.']}" % -42,
            )

        # date field: None is not allowed
        with transaction.atomic():
            with self.assertRaises(ValidationError) as e:
                expense = self.create_expense(
                    category=category,
                    amount=5000,
                    note="Rent for August 2019",
                    date=None,
                )
            self.assertEqual(ValidationError, type(e.exception))

        # date field: Malformed dates are not allowed
        with transaction.atomic():
            with self.assertRaises(ValidationError) as e:
                expense = self.create_expense(
                    category=category,
                    amount=5000,
                    note="Rent for August 2019",
                    date="I am a string, not a date!",
                )
            self.assertEqual(ValidationError, type(e.exception))

    def test_malformed_income_triggers_errors(self):
        category = self.create_income_category("Wage")

        # No category
        with transaction.atomic():
            with self.assertRaises(ValidationError) as e:
                _ = self.create_income(
                    category=None,
                    amount=10000,
                    note="Wage for August 2019",
                    date="2019-08-01",
                )
            self.assertEqual(ValidationError, type(e.exception))

        # amount field: None is not allowed
        with transaction.atomic():
            with self.assertRaises(ValidationError) as e:
                income = self.create_income(
                    category=category,
                    amount=None,
                    note="Wage for August 2019",
                    date="2019-08-01",
                )
            self.assertEqual(ValidationError, type(e.exception))

        # date field: None is not allowed
        with transaction.atomic():
            with self.assertRaises(ValidationError) as e:
                income = self.create_income(
                    category=category,
                    amount=10000,
                    note="Wage for August 2019",
                    date=None,
                )
            self.assertEqual(ValidationError, type(e.exception))

        # date field: Malformed dates are not allowed
        with transaction.atomic():
            with self.assertRaises(ValidationError) as e:
                income = self.create_income(
                    category=category,
                    amount=10000,
                    note="Rent for August 2019",
                    date="I am a string, not a date!",
                )
            self.assertEqual(ValidationError, type(e.exception))

        # amount field: zero is not allowed
        with transaction.atomic():
            with self.assertRaises(ValidationError) as e:
                income = self.create_income(
                    category=category,
                    amount=0,
                    note="Rent for August 2019",
                    date="2019-08-01",
                )
            self.assertEqual(ValidationError, type(e.exception))
            self.assertEqual(
                str(e.exception),
                "{'amount': ['%s should be more than 0.']}" % 0,
            )

        # amount field: negative numbers are not allowed
        with transaction.atomic():
            with self.assertRaises(ValidationError) as e:
                income = self.create_income(
                    category=category,
                    amount=-42,
                    note="Rent for August 2019",
                    date="2019-08-01",
                )
            self.assertEqual(ValidationError, type(e.exception))
            self.assertEqual(
                str(e.exception),
                "{'amount': ['%s should be more than 0.']}" % -42,
            )

    # TODO: Write me
    def test_saving_and_retrieving_monthly_balance_categories(self):
        pass

    def test_malformed_monthly_balance_categories_triggers_errors(self):
        # None is not allowed
        with transaction.atomic():
            with self.assertRaises(ValidationError) as e:
                _ = self.create_monthly_balance_category(None)
            self.assertEqual(ValidationError, type(e.exception))

        # Empty strigs are not allowed
        with transaction.atomic():
            with self.assertRaises(ValidationError) as e:
                _ = self.create_monthly_balance_category("")
            self.assertEqual(ValidationError, type(e.exception))

        # String longer than 40 chas are not allowed either
        with transaction.atomic():
            with self.assertRaises(ValidationError) as e:
                _ = self.create_monthly_balance_category(
                    "this text is exactly 1 char more than 40!"
                )
            self.assertEqual(ValidationError, type(e.exception))

    # TODO: Write me
    def test_duplicates_monthly_balance_categories_triggers_errors(self):
        pass

    # TODO: Write me
    def test_saving_and_retrieving_monthly_balances(self):
        pass

    # TODO: write me
    def test_malformed_monthly_balance_triggers_errors(self):
        category = self.create_monthly_balance_category("Test category")

        # amount field: negative numbers are not allowed
        with transaction.atomic():
            with self.assertRaises(ValidationError) as e:
                _ = self.create_monthly_balance(
                    category=category, amount=-42, date="2019-09-01"
                )
            self.assertEqual(ValidationError, type(e.exception))
            self.assertEqual(
                str(e.exception),
                "{'amount': ['%s should be more than 0.']}" % -42,
            )

    # TODO: Write me
    def test_duplicates_monthly_balances_triggers_errors(self):
        pass

    # TODO: Write me
    def test_saving_and_retrieving_goals(self):
        pass

    def test_malformed_goals_triggers_errors(self):
        # amount field: None is not allowed
        with transaction.atomic():
            with self.assertRaises(ValidationError) as e:
                _ = self.create_goal(
                    amount=None,
                    text="My first goal",
                    note="My first goal's note",
                    is_archived=False,
                )
            self.assertEqual(ValidationError, type(e.exception))
            self.assertEqual(
                str(e.exception), "{'amount': ['This field cannot be null.']}"
            )

        # amount field: zero is not allowed
        with transaction.atomic():
            with self.assertRaises(ValidationError) as e:
                _ = self.create_goal(
                    amount=0,
                    text="My second goal",
                    note="My second goal's note",
                    is_archived=False,
                )
            self.assertEqual(ValidationError, type(e.exception))
            self.assertEqual(
                str(e.exception),
                "{'amount': ['%s should be more than 0.']}" % 0,
            )

        # amount field: negative numbers are not allowed
        with transaction.atomic():
            with self.assertRaises(ValidationError) as e:
                _ = self.create_goal(
                    amount=-42,
                    text="My third goal",
                    note="My third goal's note",
                    is_archived=False,
                )
            self.assertEqual(ValidationError, type(e.exception))
            self.assertEqual(
                str(e.exception),
                "{'amount': ['%s should be more than 0.']}" % -42,
            )

    # TODO: Write me
    def test_duplicates_goals_triggers_errors(self):
        pass

    # TODO: Write me
    def test_duplicates_goals_among_different_users_succeeds(self):
        pass

    # TODO: Write me
    def test_saving_and_retrieving_asset(self):
        pass

    # TODO: Write me
    def test_malformed_assets_triggers_errors(self):
        pass

    # TODO: Write me
    def test_saving_and_retrieving_profit(self):
        pass

    # TODO: Write me
    def test_malformed_profits_triggers_errors(self):
        pass
