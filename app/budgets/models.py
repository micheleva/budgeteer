# Copyright: (c) 2019, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
# pylint: disable=C0115,E1101,C0103,R0903
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.core.validators import MinLengthValidator
from django.db import models
from django.urls import reverse

# TODO: when we'll have multiple users, we might want to expose UUID instead
# of ID  in the urls/API
# How to do migrations that include unique fields
# https://docs.djangoproject.com/en/4.2/howto/writing-migrations/#migrations-that-add-unique-fields  # noqa: E501


def validate_positive_number(value):
    if value <= 0:
        raise ValidationError(f"{value} should be more than 0.")


def validate_day_of_month(value):
    if value < 1 or value > 31:
        raise ValidationError(
            f"{value} should be a valid day of the month" " (1 to 31)."
        )


def validate_positive_number_allow_zero(value):
    if value is None:
        msg = "This field may not be null."
        raise ValidationError(msg)
    if value < 0:
        raise ValidationError(f"{value} should be more than 0.")


# NOTE: any major change in this file should be double checked against
#       (or reflected into) "seed-db" and "wipe-user-id" django.Commands
# see app/budgets/managements/commands/<seed-db|wipe-user-id>.py
class Category(models.Model):
    text = models.CharField(max_length=40, default=None)
    is_archived = models.BooleanField(default=False)
    created_by = models.ForeignKey(
        User, default=None, null=True, on_delete=models.SET_NULL
    )

    class Meta:
        # Different users can have categories with the same text
        constraints = [
            models.UniqueConstraint(
                fields=["text", "created_by"], name="cat-per-user"
            )
        ]

    def __str__(self):
        return f"{self.text}"


# NOTE: any major change in this file should be double checked against
#       (or reflected into) "seed-db" and "wipe-user-id" django.Commands
# see app/budgets/managements/commands/<seed-db|wipe-user-id>.py
class Expense(models.Model):
    category = models.ForeignKey(
        Category, default=None, null=True, on_delete=models.SET_NULL
    )
    # Do we really want to allow zero?
    amount = models.DecimalField(
        max_digits=10,
        decimal_places=2,
        validators=[validate_positive_number_allow_zero],
    )
    # Warning: keep in mind this will allow both empty strings AND NULL
    # https://docs.djangoproject.com/en/2.2/ref/models/fields/#null
    note = models.CharField(null=True, blank=True, max_length=150, default="")
    date = models.DateField()
    created_by = models.ForeignKey(
        User, default=None, null=True, on_delete=models.SET_NULL
    )

    def __str__(self):
        id = self.category.id  # pylint: disable=w0622; # noqa
        amount = f"{self.amount:.2f}"
        note = self.note
        date = self.date
        return f"{id}: ({note}), {amount}, {date}"

    class Meta:
        constraints = [
            models.CheckConstraint(
                check=models.Q(amount__gte=0),
                name="expense-amount-is-positive-allow-zero",
            )
        ]


class SavingCategory(models.Model):
    # NOTE: we do we have min length 1 on purpose ?
    # In some languages one char might be enough : e.g. 車 (Car)
    text = models.CharField(
        max_length=40,
        null=False,
        blank=False,
        validators=[MinLengthValidator(1)],
    )
    is_archived = models.BooleanField(default=False)
    created_by = models.ForeignKey(
        User, default=None, null=True, on_delete=models.SET_NULL
    )

    class Meta:
        constraints = [
            # Unique per user, SavingCategory, year, month
            # NOTE: as SavingCategory is already per-user unique
            # having created_by in this constraint
            # is a bit redundant. It's here for a consistent style
            models.UniqueConstraint(
                fields=["text", "created_by"],
                name="budget-savingcategory-text-unique",
            ),
        ]


class Budget(models.Model):
    category = models.ForeignKey(
        Category, null=True, blank=True, on_delete=models.CASCADE
    )
    savingcategory = models.ForeignKey(
        SavingCategory, null=True, blank=True, on_delete=models.CASCADE
    )
    year = models.IntegerField()
    month = models.IntegerField(null=True)
    amount = models.DecimalField(
        max_digits=10,
        decimal_places=3,
        validators=[validate_positive_number_allow_zero],
    )
    allocated_amount = models.DecimalField(
        max_digits=10,
        decimal_places=3,
        null=True,
        default=None,
        validators=[validate_positive_number_allow_zero],
    )
    created_by = models.ForeignKey(
        User, default=None, null=True, on_delete=models.SET_NULL
    )

    class Meta:
        constraints = [
            models.CheckConstraint(
                # Budgets can have a Category or a SavingCategory, not both
                check=(
                    models.Q(
                        category__isnull=False, savingcategory__isnull=True
                    )
                    | models.Q(
                        category__isnull=True, savingcategory__isnull=False
                    )
                ),
                name="valid_budget",
            ),
            # When budgets has a SavingCategory it must have a non null
            # allocated_amount greater or equal to zero. On the other hand
            # When the budgets has a category, the allocated_amount must be
            # None
            models.CheckConstraint(
                check=(
                    models.Q(
                        category__isnull=False,
                        allocated_amount=models.Value(None),
                    )
                    | models.Q(
                        savingcategory__isnull=False,
                        allocated_amount__isnull=False,
                        allocated_amount__gte=0,
                    )
                ),
                name="amount_constraints",
            ),
            # Unique per user, SavingCategory, year, month
            # NOTE: as SavingCategory is already per-user unique
            # having created_by in this constraint
            # is a bit redundant. It's here for a consistent style
            models.UniqueConstraint(
                fields=["savingcategory", "year", "month", "created_by"],
                name="budget-savingcategory-date-unique",
            ),
            # Unique per user, category, year, month
            # NOTE: as category is already per-user unique, having created_by
            # in this constraint is a bit redundant.
            # It's here for a consistent style
            models.UniqueConstraint(
                fields=["category", "year", "month", "created_by"],
                name="budget-saving-date-unique",
            ),
            # Budgeted amount must be greater than zero
            models.CheckConstraint(
                check=models.Q(amount__gte=0),
                name="budget-amount-is-positive-allow-zero",
            ),
            # Allocated amount must be greater than zero
            models.CheckConstraint(
                check=models.Q(allocated_amount__gte=0),
                name="budget-allocated-amount-is-positive-allow-zero",
            ),
            # Year should be greater than 1970
            models.CheckConstraint(
                check=models.Q(year__gte=1970), name="valid_year"
            ),
            # Month should be between 1 and 12
            models.CheckConstraint(
                check=models.Q(month__gte=1, month__lte=12), name="valid_month"
            ),
        ]


# NOTE: any major change in this file should be double checked against
#       (or reflected into) "seed-db" and "wipe-user-id" django.Commands
# see app/budgets/managements/commands/<seed-db|wipe-user-id>.py
class IncomeCategory(models.Model):
    text = models.CharField(max_length=40, default=None)
    created_by = models.ForeignKey(
        User, default=None, null=True, on_delete=models.SET_NULL
    )

    class Meta:
        # Different users can have monthly balance categories with the
        # same text
        constraints = [
            models.UniqueConstraint(
                fields=["text", "created_by"], name="income-cat-per-user"
            )
        ]

    def __str__(self):
        return f"{self.text}"


# NOTE: any major change in this file should be double checked against
#       (or reflected into) "seed-db" and "wipe-user-id" django.Commands
# see app/budgets/managements/commands/<seed-db|wipe-user-id>.py
class Income(models.Model):
    category = models.ForeignKey(
        IncomeCategory, default=None, null=True, on_delete=models.SET_NULL
    )
    amount = models.IntegerField(validators=[validate_positive_number])
    # Warning: keep in mind this will allow both empty strings AND NULL
    # https://docs.djangoproject.com/en/4.2/ref/models/fields/#null
    note = models.CharField(null=True, blank=True, max_length=150, default="")
    date = models.DateField()
    created_by = models.ForeignKey(
        User, default=None, null=True, on_delete=models.SET_NULL
    )

    def __str__(self):
        id = self.category.id  # pylint: disable=w0622; # noqa
        amount = self.amount
        note = self.note
        date = self.date
        return f"{id}: {amount}, {note}, {date}"

    class Meta:
        constraints = [
            models.CheckConstraint(
                check=models.Q(amount__gte=1), name="income-amount-is-positive"
            )
        ]


# NOTE: any major change in this file should be double checked against
#       (or reflected into) "seed-db" and "wipe-user-id" django.Commands
# see app/budgets/managements/commands/<seed-db|wipe-user-id>.py
class MonthlyBalanceCategory(models.Model):
    text = models.CharField(max_length=40, default=None)
    is_foreign_currency = models.BooleanField(default=False)
    created_by = models.ForeignKey(
        User, default=None, null=True, on_delete=models.SET_NULL
    )

    class Meta:
        # Different users can have monthly balance categories w/ the same text
        constraints = [
            models.UniqueConstraint(
                fields=["text", "created_by"],
                name="monthly-balance-cat-per-user",
            )
        ]

    def __str__(self):
        return f"{self.text}"

    def get_absolute_url(self):
        named_url = "budgets:monthly_balance_categories_details"
        return reverse(named_url, args=[str(self.pk)])


# NOTE: any major change in this file should be double checked against
#       (or reflected into) "seed-db" and "wipe-user-id" django.Commands
#       see app/budgets/managements/commands/<seed-db|wipe-user-id>.py
class MonthlyBalance(models.Model):
    category = models.ForeignKey(
        MonthlyBalanceCategory,
        default=None,
        null=True,
        on_delete=models.SET_NULL,
    )
    amount = models.IntegerField(
        validators=[validate_positive_number_allow_zero]
    )
    date = models.DateField()
    created_by = models.ForeignKey(
        User, default=None, null=True, on_delete=models.SET_NULL
    )

    def __str__(self):
        id = self.id  # pylint: disable=w0622; # noqa
        amount = self.amount
        date = self.date
        return f"{id}: {amount}, {date}"

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["category", "date"], name="monthly-balance-date-unique"
            ),
            models.CheckConstraint(
                check=models.Q(amount__gte=0),
                name="monthly-balance-amount-is-positive-allow-zero",
            ),
        ]

    def get_absolute_url(self):
        return reverse("budgets:edit_monthly_balance", args=[str(self.pk)])


# NOTE: any major change in this file should be double checked against
# (or reflected into) "seed-db" and "wipe-user-id" django.Commands
# see app/budgets/managements/commands/<seed-db|wipe-user-id>.py
class Goal(models.Model):
    amount = models.IntegerField(validators=[validate_positive_number])
    # TODO: Add 2 unit tests for different users creating Goals with the
    # same text or the same note account: one test for text, one for note
    # Add unit testS for different user with the same text or same note
    # account: one test for text, one for note
    text = models.CharField(max_length=20, default=None, null=False)
    note = models.CharField(max_length=50, default=None, null=True)
    is_archived = models.BooleanField(default=False)
    created_by = models.ForeignKey(
        User, default=None, null=True, on_delete=models.SET_NULL
    )

    def __str__(self):
        id = self.id  # pylint: disable=w0622; # noqa
        amount = self.amount
        text = self.text
        return f"{id}: {amount}, {text}"

    class Meta:
        # Different users can have goals from the same text
        # ref:
        # https://docs.djangoproject.com/en/4.2/ref/models/options/#unique-together
        # Use UniqueConstraint with the constraints option instead.
        # UniqueConstraint provides more functionality than unique_together.
        # unique_together may be deprecated in the future.
        # ref: https://blog.hackajob.com/djangos-new-database-constraints/
        # Implement the same check as above with the new feature
        # (‘UniqueConstraint’). The recent database constraints are still
        # implemented in the Meta class, yet a key difference is that they are
        # defined under the variable name ‘constraints’.
        # This contains an iterable of all the checks placed on a model.
        # In this case, we have the UniqueConstraint method that is derived
        # from the model class.
        constraints = [
            models.UniqueConstraint(
                fields=["text", "created_by"], name="goal-text-per-user"
            ),
            models.CheckConstraint(
                check=models.Q(amount__gte=1), name="goal-amount-is-positive"
            ),
        ]


class Obligation(models.Model):
    OBLIGATION_TYPE_CHOICES = [
        ("regular", "Fixed Obligation"),  # e.g. a fixed rate loan
        ("variable", "Variable Obligation"),  # e.g. variable rate loan
    ]

    name = models.CharField(max_length=100)
    total_amount = models.DecimalField(
        max_digits=10, decimal_places=2, null=True, blank=True
    )
    obligation_type = models.CharField(
        max_length=10, choices=OBLIGATION_TYPE_CHOICES, default="regular"
    )
    monthly_payment = models.DecimalField(max_digits=10, decimal_places=2)
    start_date = models.DateField(null=False, blank=False)
    payment_day = models.IntegerField(
        null=False, validators=[validate_day_of_month]
    )
    end_date = models.DateField(null=False, blank=False)
    created_by = models.ForeignKey(
        User, default=None, null=True, on_delete=models.SET_NULL
    )

    def __str__(self):
        return f"{self.name} - {self.get_obligation_type_display()}"

    def save(self, *args, **kwargs):
        # Ensure total_amount is not set for variable payments
        if self.obligation_type == "variable":
            self.total_amount = None
        super().save(*args, **kwargs)


class ObligationPaymentChange(models.Model):
    obligation = models.ForeignKey(
        Obligation, on_delete=models.CASCADE, related_name="changes"
    )
    new_monthly_payment = models.DecimalField(max_digits=10, decimal_places=2)
    change_date = models.DateField()
    created_by = models.ForeignKey(
        User, default=None, null=True, on_delete=models.SET_NULL
    )

    def __str__(self):
        return (
            f"Change for Obligation {self.obligation.id} - New Monthly:"
            f" {self.new_monthly_payment} on {self.change_date}"
        )


# NOTE: any major change in this file should be double checked against
# (or reflected into) "seed-db" and "wipe-user-id" django.Commands
# see app/budgets/managements/commands/<seed-db|wipe-user-id>.py
class Account(models.Model):
    text = models.CharField(max_length=20, default=None)
    # e.g. account.credit_entity: "Broker YYY" / "Bank YYY"
    credit_entity = models.CharField(max_length=20, default=None)
    created_by = models.ForeignKey(
        User, default=None, null=True, on_delete=models.SET_NULL
    )
    is_foreign_currency = models.BooleanField(default=False)

    class Meta:
        constraints = [
            models.UniqueConstraint(
                fields=["text", "credit_entity", "created_by"],
                name="unique-account-credit_entity-per-user",
            )
        ]


# NOTE: any major change in this file should be double checked against
# (or reflected into) "seed-db" and "wipe-user-id" django.Commands
# see app/budgets/managements/commands/<seed-db|wipe-user-id>.py
class Asset(models.Model):
    account = models.ForeignKey(
        Account,
        default=None,
        null=True,
        on_delete=models.SET_NULL,
        related_name="assets",
    )
    amount = models.FloatField(validators=[validate_positive_number])
    text = models.CharField(
        max_length=60, default=None, blank=False, null=False
    )
    created_by = models.ForeignKey(
        User, default=None, null=True, on_delete=models.SET_NULL
    )
    # Length 3 as per ISO 4217
    # TODO: rename "foreign_currency" to "currency"
    foreign_currency = models.CharField(
        max_length=3, default=None, blank=False, null=False
    )

    current_foreign_value = models.FloatField(
        null=False,
        default=None,
        validators=[validate_positive_number_allow_zero],
    )
    current_local_value = models.FloatField(
        null=False,
        default=None,
        validators=[validate_positive_number_allow_zero],
    )

    # We could have got assets for free: hence the allow zero in
    # bought_price_{foreign,local}
    bought_price_foreign = models.FloatField(
        null=False,
        default=None,
        validators=[validate_positive_number_allow_zero],
    )
    bought_price_local = models.FloatField(
        null=False,
        default=None,
        validators=[validate_positive_number_allow_zero],
    )

    net_loss_foreign = models.FloatField(null=False, default=None)
    net_loss_local = models.FloatField(null=False, default=None)

    # Foreign currency to local currency rate
    rate = models.DecimalField(
        max_digits=10, decimal_places=6, null=True
    )  # true for now
    # validators=[validate_positive_number])

    record_date = models.DateField()

    class Meta:
        constraints = [
            models.CheckConstraint(
                check=models.Q(current_foreign_value__gte=0),
                name="asset-current_foreign_value-is-positive-allow-zero",
            ),
            models.CheckConstraint(
                check=models.Q(current_local_value__gte=0),
                name="asset-current_local_value-is-positive-allow-zero",
            ),
            models.CheckConstraint(
                check=models.Q(bought_price_foreign__gte=0),
                name="asset-bought_price_foreign-is-positive-allow-zero",
            ),
            models.CheckConstraint(
                check=models.Q(bought_price_local__gte=0),
                name="asset-bought_price_local-is-positive-allow-zero",
            ),
            # models.CheckConstraint(check=models.Q(rate__gt=0),
            #                        name="asset-rate-is-positive"),
        ]


# NOTE: any major change in this file should be double checked against
# (or reflected into) "seed-db" and "wipe-user-id" django.Commands
# see app/budgets/managements/commands/<seed-db|wipe-user-id>.py
class Dividend(models.Model):
    """
    This model stores income for assets.
    e.g dividends for stocks, yield from bonds, interests for
    saving accounts, etc.
    """

    account = models.ForeignKey(
        Account,
        default=None,
        null=True,
        on_delete=models.SET_NULL,
        related_name="dividends",
    )
    # asset_text is NOT a foreign key, as the same asset might different IDs
    # e.g. The same assets was owned by a given user for mulitple days:
    # Asset.id: 42  text: "XXX" record_date: "2021-04-02"
    # Asset.id: 420 text: "XXX" record_date: "2021-04-03"
    asset_text = models.CharField(max_length=60, default=None, null=False)
    local_amount = models.FloatField(
        null=True,
        default=None,
        validators=[validate_positive_number_allow_zero],
    )
    foreign_amount = models.FloatField(
        null=True,
        default=None,
        validators=[validate_positive_number_allow_zero],
    )

    currency = models.CharField(max_length=3, default=None, null=False)
    # NOTE: saving rate for redundancy (Other models do not store the
    # rate _yet).
    # FIXME When having dividends in the local currenty, use rate of 1
    rate = models.FloatField(
        null=True, default=None, validators=[validate_positive_number]
    )

    created_by = models.ForeignKey(
        User, default=None, null=True, on_delete=models.SET_NULL
    )

    record_date = models.DateField()

    class Meta:
        constraints = [
            models.CheckConstraint(
                check=models.Q(foreign_amount__gte=0),
                name="dividends-foreign_amount_value-is-positive-allow-zero",
            ),
            models.CheckConstraint(
                check=models.Q(local_amount__gte=0),
                name="dividends-local_amount_value-is-positive-allow-zero",
            ),
            models.CheckConstraint(
                check=models.Q(rate__gt=0), name="dividends-rate-is-positive"
            ),
        ]


class SpendingLimit(models.Model):
    id = models.AutoField(primary_key=True)
    amount = models.IntegerField(validators=[validate_positive_number])
    currency = models.CharField(
        max_length=3,
        null=False,
        blank=False,
    )
    days = models.IntegerField(validators=[validate_positive_number])
    category = models.ForeignKey(
        Category, on_delete=models.SET_NULL, null=True
    )
    # This field length matches Expense.note
    text = models.CharField(
        null=False, blank=False, max_length=150, default=""
    )
    created_by = models.ForeignKey(
        User, default=None, null=True, on_delete=models.SET_NULL
    )

    def __str__(self):
        return f"{self.category} - {self.amount} {self.currency}"
