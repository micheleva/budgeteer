#!/bin/bash
echo ${1:-$BACKUP_PATH}

# Check if path exists and is a readable file
if [[ -e "${1:-$BACKUP_PATH}" && -f "${1:-$BACKUP_PATH}" && -r "${1:-$BACKUP_PATH}" ]]; then
    echo "Path exists and is a readable file."
    
    # Restore the database using psql
    PGPASSWORD="$POSTGRES_PASSWORD" psql -h db -d budgeteer_db -U "$POSTGRES_USER" < "${1:-$BACKUP_PATH}"
else
    echo "Path does not exist or is not a readable file."
fi
