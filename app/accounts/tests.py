# Copyright: (c) 2024, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

from django.conf import settings
from django.urls import reverse
from django.test import TestCase
from django.test.utils import override_settings

from budgets.tests.base import BaseTest


class SignupTest(TestCase):
    def extract_csrf_token(self, html):
        """Extract the CSRF token from HTML content."""
        start = html.find('name="csrfmiddlewaretoken"')
        if start == -1:
            return None
        start = html.find('value="', start) + len('value="')
        end = html.find('"', start)
        return html[start:end]

    @override_settings(SIGNUP_ENABLED=False)
    def test_cant_access_singup_page_when_signup_is_disabled(self):
        response = self.client.get(reverse("accounts:signup"))
        self.assertEqual(response.status_code, 403)

    @override_settings(SIGNUP_ENABLED=False)
    def test_signup_link_not_present_on_landing_page_when_singup_is_disabled(
        self,
    ):
        response = self.client.get(reverse("budgets:landing_page"))
        self.assertEqual(response.status_code, 200)

        # Any reference to the signup should NOT be present
        self.assertNotContains(response, "signup")

        msg = "No email needed, no tracking, no ads. <b>Make sure to harden the app if you expose it to the internet 😜"
        url = reverse("accounts:signup")
        self.assertNotContains(response, msg)
        self.assertNotContains(response, url)

    @override_settings(SIGNUP_ENABLED=True)
    def test_signuup_form_does_not_suceed_with_valid_data_when_signup_is_disabled(
        self,
    ):
        response = self.client.get(reverse("accounts:signup"))
        content = response.content.decode("utf-8")
        csrf_token = self.extract_csrf_token(content)

        pwd = BaseTest.generate_string(20)
        with override_settings(SIGNUP_ENABLED=False):
            response = self.client.post(
                reverse("accounts:signup"),
                data={
                    "username": BaseTest.generate_string(20),
                    "csrfmiddlewaretoken": csrf_token,
                    "password1": pwd,
                    "password2": pwd,
                },
            )
            self.assertEqual(response.status_code, 403)

    @override_settings(SIGNUP_ENABLED=True)
    def test_signuup_form_does_not_suceed_with_invalid_data_when_signup_is_disabled(
        self,
    ):
        response = self.client.get(reverse("accounts:signup"))
        content = response.content.decode("utf-8")
        csrf_token = self.extract_csrf_token(content)

        pwd = BaseTest.generate_string(20)
        with override_settings(SIGNUP_ENABLED=False):
            response = self.client.post(
                reverse("accounts:signup"),
                data={
                    "username": BaseTest.generate_string(20),
                    "csrfmiddlewaretoken": csrf_token,
                    "not-a-field-i-should-be-password1": pwd,
                    "i-am-surely-not-password2-field": pwd,
                },
            )
            self.assertEqual(response.status_code, 403)

    @override_settings(SIGNUP_ENABLED=True)
    def test_signup_post_not_get_processed_when_signup_is_disabled(
        self,
    ):
        pwd = BaseTest.generate_string(20)
        response = self.client.post(
            reverse("accounts:signup"),
            data={
                "username": BaseTest.generate_string(20),
                "csrfmiddlewaretoken": "not-needed-as-signup-is-disabled",
                "password1": pwd,
                "password2": pwd,
            },
        )

        self.assertNotEqual(response.status_code, 403)

    @override_settings(SIGNUP_ENABLED=True)
    def test_signup_link_present_on_landing_page_when_disabled(self):
        # Simulate accessing a different page where the signup link might be present
        response = self.client.get(reverse("budgets:landing_page"))

        self.assertEqual(response.status_code, 200)

        # Confirm signup references are present
        self.assertContains(response, "signup")

        msg = "No email needed, no tracking, no ads. <b>Make sure to harden the app if you expose it to the internet 😜"
        url = reverse("accounts:signup")
        self.assertContains(response, msg)
        self.assertContains(response, url)

    @override_settings(SIGNUP_ENABLED=True)
    def test_signuup_form_does_suceed_with_valid_data_when_signup_is_enabled(
        self,
    ):
        response = self.client.get(reverse("accounts:signup"))
        content = response.content.decode("utf-8")
        csrf_token = self.extract_csrf_token(content)

        pwd = BaseTest.generate_string(20)
        response = self.client.post(
            reverse("accounts:signup"),
            data={
                "username": BaseTest.generate_string(20),
                "csrfmiddlewaretoken": csrf_token,
                "password1": pwd,
                "password2": pwd,
            },
        )
        print(response.content)
        self.assertEqual(response.status_code, 302)
        self.assertRedirects(response, "/app/")
