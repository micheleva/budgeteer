# Copyright: (c) 2021, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
# pylint: disable=R0903,C0115
from django import forms
from django.conf import settings
from django.contrib.auth import login
from django.contrib.auth.decorators import login_required
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponseForbidden
from django.urls import reverse_lazy
from django.views.generic import CreateView


class LoginForm(AuthenticationForm):
    username = forms.CharField(
        widget=forms.TextInput(attrs={"class": "form-control"})
    )
    password = forms.CharField(
        widget=forms.PasswordInput(attrs={"class": "form-control"})
    )

    def __init__(self, *args, **kwargs):
        super(LoginForm, self).__init__(*args, **kwargs)


class SignUpView(CreateView):
    # https://docs.djangoproject.com/en/dev/releases/4.2/ to change to BaseUserCreationForm
    form_class = UserCreationForm
    success_url = "/app/"
    template_name = "registration/signup.html"

    def get(self, request, *args, **kwargs):
        if not settings.SIGNUP_ENABLED:
            return HttpResponseForbidden(
                "Signup is disabled in this environment."
            )
        return super().get(request, *args, **kwargs)

    def form_valid(self, form):
        if not settings.SIGNUP_ENABLED:
            return HttpResponseForbidden(
                "Signup is disabled in this environment."
            )
        valid = super().form_valid(form)
        login(self.request, self.object)
        return valid

    def form_invalid(self, form):
        if not settings.SIGNUP_ENABLED:
            return HttpResponseForbidden(
                "Signup is disabled in this environment."
            )
        response = super().form_invalid(form)
        return response

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        return context
