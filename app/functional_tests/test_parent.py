# Copyright: (c) 2019, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
import os
import signal

from django.test import LiveServerTestCase
from selenium import webdriver
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities


class IntegrationTest(LiveServerTestCase):
    """Base Functional test class"""

    # Any other value other than "Firefox" will tigger Chromium usage
    BROWSER = "Firefox"
    HEADLESS = True
    CONTAINARIZED = True
    # When testing inside gitlab-ci, the test container needs to listen to all
    # inrefaces, so that the firefox remote container can access it
    # By default, this value is localhost, and "localhost" inside a continer
    # is not reachable from other containers in gitlabc-i
    # References https://marcgibbons.com/posts/selenium-in-docker
    #            https://stackoverflow.com/questions/44240139
    #            https://stackoverflow.com/questions/24319662/from-inside-of-a-docker-container-how-do-i-connect-to-the-localhost-of-the-mach  # pylint: disable=C0301; # noqa
    host = "0.0.0.0"

    # Setup
    @classmethod
    def setUpClass(self):  # pylint: disable=C0103,C0202; # noqa
        # IMPORTANT: set the class 'host' value back to 'localhost' BEFORE
        # calling setUpClass() when NOT inside gitlab-ci
        is_containarized = self.CONTAINARIZED
        if os.environ.get("TEST_HOSTNAME") is None:
            is_containarized = False
            IntegrationTest.host = "localhost"

        super().setUpClass()
        # TEST ME: Confirm this works as intented with Chromium as well
        container_domain = self.BROWSER.lower()
        self.setup_browser(
            self.BROWSER, self.HEADLESS, is_containarized, container_domain
        )

    # Static timeout for all functional tests
    # The timeout is intentionally set to a high value to allow for slow test
    # execution on the CI server, but to also catch any infinite loops or prevent
    # the test from hanging indefinitely consuming CI resources
    TIMEOUT = 180

    def timeout_handler(self, signum, frame):
        """
        Raise a TimeoutError when a test takes too long
        """
        raise TimeoutError(
            f"Infinite loop or slow test detected: intentionally timing out! Signal number {signum} frame: {frame}"
        )

    def setUp(self):
        """
        Override the setUp method to apply signal.alarm to every test
        """
        super().setUp()

        # Set the signal handler and alarm before each test method
        signal.signal(signal.SIGALRM, self.timeout_handler)
        signal.alarm(self.TIMEOUT)  # Set alarm to go off after TIMEOUT seconds

    def tearDown(self):
        signal.alarm(0)  # Disable the alarm after each test
        super().tearDown()

    @classmethod
    def setup_browser(
        self,
        browser="Firefox",
        headless=True,
        containarized=True,  # pylint: disable=C0202; # noqa
        container_domain="firefox",
    ):
        """
        Initialize the browser
        """
        options = Options()
        caps = DesiredCapabilities.FIREFOX

        if containarized:
            options.set_capability("cloud:options", caps)
            # if headless:
            #     options.add_argument('--headless')
            remote_selenium = f"http://{container_domain}:4444/wd/hub"
            self.browser = webdriver.Remote(
                command_executor=remote_selenium,
                options=webdriver.FirefoxOptions(),
            )
        else:
            if browser == "Firefox":
                if headless:
                    options.add_argument("--headless")
                self.browser = webdriver.Firefox(options=options)
            else:  # fallback to Chromium otherwise
                options = webdriver.ChromeOptions()
                if headless:
                    options.add_argument("--headless")
                options.add_argument("--ignore-certificate-errors")
                options.add_argument("--test-type")
                options.add_argument("--no-proxy-server")
                # Note: if running the tests as root (why would we!?) add: options.add_argument('--no-sandbox')  # pylint: disable=C0202; # noqa
                # TODO: make chromium binary cusotmizable, or verify that we
                # really need to specify its path (not detected from $PATH ?)
                options.binary_location = "/usr/bin/chromium-browser"
                self.browser = webdriver.Chrome(
                    chrome_options=options
                )  # pylint: disable=E1123; # noqa

            self.original_url = self.live_server_url

        if os.environ.get("TEST_HOSTNAME") is not None:
            container_name = os.environ.get("TEST_HOSTNAME")
            new_url = self.live_server_url.replace("0.0.0.0", container_name)
        else:
            new_url = self.live_server_url.replace("0.0.0.0", "localhost")

        self.live_server_url = new_url

    @classmethod
    def tearDownClass(self):  # pylint: disable=C0103,C0202; # noqa
        super().tearDownClass()  # TEST ME!
        self.browser.quit()
