# Copyright: (c) 2019, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
# Docs at https://selenium-python.readthedocs.io/waits.html
from decimal import Decimal

from datetime import date, datetime
from datetime import timedelta
from functools import wraps

# import locale
import os
import string
import random
import re
import time
import decimal

# Docs at https://seleniumhq.github.io/selenium/docs/api/py/webdriver_support/
#  selenium.webdriver.support.expected_conditions.html
from django.urls import reverse

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import Select
from selenium.webdriver.support.ui import WebDriverWait

from api.v1.tests.test_base import BaseApiTest

# locale.setlocale(locale.LC_ALL, "")
# Maximum amount of seconds to wait before giving up
MAX_WAIT_SECONDS = 2


# TODO: this is duplicate code from unit tests
def generate_string(length=10):
    """Generate a random string of a given length"""
    char = string.ascii_lowercase
    return "".join(random.choice(char) for i in range(length))


def check_amount_and_cat_name(tester, amount, category_name):
    # Frank sees all the details sum  displayed on the page
    table = tester.browser.find_element(By.ID, "id_expenses_total")
    find_text_inside_table(tester, str(amount), table)
    find_text_inside_table(tester, category_name, table)


# Credits to Tommy Beadle: http://disq.us/p/x1r1v2
def wait_for_page_to_reload(tester):
    if tester.BROWSER == "Firefox":
        wait = WebDriverWait(tester.browser, MAX_WAIT_SECONDS)
        old_page = tester.browser.find_element(By.TAG_NAME, "html")
        _ = wait.until(EC.staleness_of(old_page))
    else:
        # TODO: investigate why chromium does work without this
        pass


def wait_for_required_input(tester, elem_id):
    _ = WebDriverWait(tester.browser, MAX_WAIT_SECONDS)
    invalid = tester.browser.find_elements(
        By.CSS_SELECTOR, f"#{elem_id}:invalid"
    )
    tester.assertEqual(len(invalid), 1)


# TODO merge the two functions below
def find_text_inside_table(tester, text, table):
    rows = table.find_elements(By.TAG_NAME, "td")
    tester.assertTrue(
        any(text in row.text for row in rows),
        f"No {text} in rows. Contents were\n{table.text}",
    )


def assert_text_is_not_inside_table(tester, text, table):
    rows = table.find_elements(By.TAG_NAME, "td")
    tester.assertFalse(
        any(text in row.text for row in rows),
        f"Text {text} has been found! Contents were\n{table.text}",
    )


def hover_over_items_in_nav_bar(tester):
    # React will generate the links on mouse hover on the menu
    # hence we need selenium to preform such action,
    # otherwise the only 'a' items to be found are the navigation bar
    # parent items (and not its '<a />' children)
    react_nav_bar_class_name = "navbar-nav"
    react_nav_bar_item_tag_name = "div"
    elements_to_hover_over = tester.browser.find_element(
        By.CLASS_NAME, react_nav_bar_class_name
    ).find_elements(By.TAG_NAME, react_nav_bar_item_tag_name)
    for menu_item in elements_to_hover_over:
        hover = ActionChains(tester.browser).move_to_element(menu_item)
        hover.perform()


def find_urls_in_home_page(tester, urls_to_find):
    url = reverse("budgets:home")
    tester.browser.get(f"{tester.live_server_url}{url}")

    # React will generate the links on mouse hover on the menu
    # hence we need selenium to preform such action,
    # otherwise the only 'a' items to be found are the navigation bar parent
    # items (and not its '<a />' children)
    hover_over_items_in_nav_bar(tester)

    links = tester.browser.find_elements(By.TAG_NAME, "a")

    for url_to_find in urls_to_find:
        tester.assertTrue(
            any(url_to_find in link.get_attribute("href") for link in links),
            f"No url: {url_to_find} was found in the Page."
            " It was supposed to be there",
        )


def confirm_urls_are_missing_in_home_page(tester, urls_to_find):
    url = reverse("budgets:home")
    tester.browser.get(f"{tester.live_server_url}{url}")

    # React will generate the links on mouse hover on the menu
    # hence we need selenium to preform such action,
    # otherwise the only 'a' items to be found are the navigation bar parent
    # items (and not its '<a />' children)
    hover_over_items_in_nav_bar(tester)

    links = tester.browser.find_elements(By.TAG_NAME, "a")

    for url_to_find in urls_to_find:
        tester.assertTrue(
            any(
                url_to_find not in link.get_attribute("href") for link in links
            ),
            f"Url: {url_to_find} was found in the Page."
            " It was NOT supposed to be there",
        )


def find_error(
    tester, errorText, printErrors=False, check_lack_of_error=False
):
    if check_lack_of_error:
        with tester.assertRaises(NoSuchElementException):
            error_container = tester.browser.find_element(
                By.CLASS_NAME, "errorlist"
            )
        return

    error_container = tester.browser.find_element(By.CLASS_NAME, "errorlist")
    errors = error_container.find_elements(By.TAG_NAME, "li")

    if printErrors:
        for error in errors:
            print(error.text, flush=True)

    tester.assertTrue(
        any(errorText in error.text for error in errors),
        f"{errorText} was not present. Contents were\n{errors}",
    )


def find_error_by_selector(tester, selector_type, selector, err_msg):
    # sel = "span.help-inline"
    err = WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 3).until(
        EC.presence_of_element_located((selector_type, selector))
    )
    tester.assertEqual(err_msg, err.text)


def register_and_login(func):
    """
    Decorator that create a random user, log in as it, and log out at the end
    """

    @wraps(func)
    def wrapper(*args, **kwargs):
        create_user(*args, **kwargs)
        func(*args, **kwargs)
        logout_user(*args, **kwargs)

    return wrapper


# this func should be create user and login
def create_user(tester, username=None, password=None):
    """Sign up, and automatically log in as said user"""
    if not username:
        username = generate_string() + str(datetime.now().timestamp())
    if not password:
        password = generate_string()

    url = reverse("accounts:signup")
    tester.browser.get(f"{tester.live_server_url}{url}")

    # Frank sign up: first he enters a gibberish username, he likes privacy
    user_input_box = tester.browser.find_element(By.ID, "id_username")
    user_input_box.send_keys(username)

    # Frank then insert a random password... Frank likes security
    pwd_input_1 = tester.browser.find_element(By.ID, "id_password1")
    pwd_input_1.send_keys(password)

    # Frank re-inputs the same password, Frank really knows how web form works!
    pwd_input_2 = tester.browser.find_element(By.ID, "id_password2")
    pwd_input_2.send_keys(password)

    # Frank press the submit button
    submit = 'input[type="submit"]'
    submit_input = tester.browser.find_element(By.CSS_SELECTOR, submit)
    submit_input.click()

    return (username, password)


def login_user(tester, username, password):
    """Log in an user"""
    url = reverse("accounts:login")
    tester.browser.get(f"{tester.live_server_url}{url}")

    # Frank sign up: first he enters a gibberish username, he likes privacy
    user_input_box = tester.browser.find_element(By.ID, "id_username")
    user_input_box.send_keys(username)

    # Frank then insert his favourite password
    pwd_input = tester.browser.find_element(By.ID, "id_password")
    pwd_input.send_keys(password)

    # Frank see a submit button
    submit_button = tester.browser.find_elements(By.TAG_NAME, "input")[3]
    submit_button.click()


def logout_user(tester):
    """Log out"""
    url = reverse("accounts:logout")
    tester.browser.get(f"{tester.live_server_url}{url}")


# FIXME: is_income flag is making the logic of this function too complex
# refactor me
def create_a_category(
    tester,
    category_name,
    is_income=False,
    create_check=True,
    midway_check=False,
    wait_for_reload=True,
    lack_of_error=False,
    is_balance=False,
):
    print(
        "Deprecated create_a_category, write a create category for react instead",
        flush=True,
    )
    if is_income:
        url = reverse("budgets:income_categories_create")
    elif is_balance:
        url = reverse("budgets:new_monthly_balance_category")
    else:
        url = reverse("budgets:categories_create")

    # Frank creates a category
    tester.browser.get(f"{tester.live_server_url}{url}")
    try:
        WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 3).until(
            EC.presence_of_element_located((By.ID, "id_text"))
        )
    except NoSuchElementException:
        tester.fail("Create a category UI did not load timely. Fail!")

    inputbox = tester.browser.find_element(By.ID, "id_text")

    placeholder_text = "Enter a new category"
    if is_balance:
        placeholder_text = "Enter a new balance category (i.e. savings, cash)"

    tester.assertEqual(inputbox.get_attribute("placeholder"), placeholder_text)

    inputbox.send_keys(category_name)
    inputbox.send_keys(Keys.ENTER)

    if wait_for_reload:
        wait_for_page_to_reload(tester)

    # Check for error right after submitting the form
    if midway_check:
        if is_income:
            error_text = "Income category with this Text already exists"
        elif is_income:
            error_text = "MonthlyBalanceCategory with this Text already exists"
        else:
            error_text = "Category with this Text already exists"
        find_error(tester, error_text, check_lack_of_error=lack_of_error)

    # Check if the categoy page is showing created category
    if create_check:
        visit_and_verify_categories(
            tester, category_name, is_income=is_income, is_balance=is_balance
        )


def create_expense_category(
    tester,
    category_name,
    check_creation_popup=False,
    check_failure_popup=False,
):
    # NOTE: we hard code the URI because Django has no idea of React routes
    # (i.e. react internal virtual routes, not API endpoints)
    url = "/app/categories/creation"

    # Frank creates a category
    tester.browser.get(f"{tester.live_server_url}{url}")

    # Make this test less flaky by allowing some time for the UI to fully load
    try:
        WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
            EC.presence_of_element_located((By.ID, "text_field"))
        )
    except NoSuchElementException:
        tester.fail("Category text input field was not displayed: Fail!")

    inputbox = tester.browser.find_element(By.ID, "text_field")
    placeholder_text = "Enter the category name"

    tester.assertEqual(inputbox.get_attribute("placeholder"), placeholder_text)

    inputbox.send_keys(category_name)
    inputbox.send_keys(Keys.ENTER)

    if check_creation_popup:
        # Frank likes very much confirmation green modals
        # He expects one to be shown, as he filled in everything correctly
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located(
                    (By.CLASS_NAME, "alert-success")
                )
            )
        except NoSuchElementException:
            tester.fail("Success popup was not displayed. Fail!")

    if check_failure_popup:
        # Frank likes very much confirmation green modals. He expects one to
        # be shown as he filled in everything correctly
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located((By.CLASS_NAME, "alert-danger"))
            )
        except NoSuchElementException:
            tester.fail("Failure popup was not displayed. Fail!")


def create_an_expense_using_react(
    self, cat_id, check_creation_popup, check_failure_popup
):
    amount = BaseApiTest.generate_number(max_digits=10, decimal_places=2)
    expense_date = str(date.today())
    note = generate_string()
    check_creation_popup = True
    check_failure_popup = False
    create_an_expense(
        self,
        cat_id,
        amount,
        expense_date,
        note,
        check_creation_popup=check_creation_popup,
        check_failure_popup=check_failure_popup,
    )

    return {
        "amount": amount,
        "category_id": cat_id,
        "note": note,
        "date": expense_date,
    }


def check_category_existence(tester, text, url, expected_to_fail=False):
    """
    Check if a category with a given name exists
    Returns the category ID if it exists, -1 otherwise
    """
    complete_url = f"/app/{url}"
    tester.browser.get(f"{tester.live_server_url}{complete_url}")
    category_id = -1

    try:
        table = WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
            EC.presence_of_element_located((By.ID, "categories-table-body"))
        )

        found = False
        for _, tr in enumerate(table.find_elements(By.TAG_NAME, "tr")):
            if found:
                break
            tds = tr.find_elements(By.TAG_NAME, "td")
            if len(tds) != 3:
                tester.fail(
                    "Each row in the categories table should have 3 columns"
                )
            if text == tds[1].text:
                found = True
                try:
                    category_id = int(tds[0].text)
                except ValueError:
                    tester.fail(
                        "Category was not displayed with the format we did"
                        " expect. Failed to convert ID to integer. Fail!"
                    )

        if expected_to_fail and found:
            tester.fail(
                f"Category with name {text} was NOT supposed"
                " to be displayed... but it was: fail!"
            )

        if not expected_to_fail and not found:
            tester.fail(f"Category with name {text} was not" " found: fail!")

    except TimeoutException:
        if not expected_to_fail:
            tester.fail(
                "Either categories API has not completed (yet), or the"
                "list of categories was not displayed. Fail!"
            )

    except NoSuchElementException:
        tester.fail(
            "Either categories API has not completed (yet), or the"
            "list of categories was not displayed. Fail!"
        )

    return category_id


def _select_data_range(tester, start_date, end_date):
    # Frank select the input with id start, he waits for 3 seconds before timing out
    start = None
    end = None
    try:
        start = WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
            EC.presence_of_element_located((By.ID, "start"))
        )
        end = WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
            EC.presence_of_element_located((By.ID, "end"))
        )
    except NoSuchElementException as e:
        print(e, flush=True)
        msg = "Start or End date input field did not load properly"
        tester.fail(msg)
    except TimeoutException:
        tester.fail(
            "UI has not completed (yet), or the start and end fields"
            " were not displayed. Fail!"
        )

    if start is None or end is None:
        tester.fail("Start date input field was not found")

    # Frank submits the form
    submit = WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 3).until(
        EC.presence_of_element_located((By.ID, "dateFilterSubmit"))
    )

    # Frank enters the start and end date
    start.send_keys(start_date)

    # NOTE: Since the frontend will, as soon as we change the start date,
    # issue a query to the backend, we need to wait a bit before entering
    # the end date. Otherwise the frontend will silently not issue the
    # second query, as the first one might still be pending
    time.sleep(2)

    end.send_keys(end_date)
    submit.click()


def find_expense_in_date_range(tester, data, ranges, expected_to_fail=False):
    # NOTE: we hard code the URI because Django has no idea of React routes
    # (i.e. react internal virtual routes, not API endpoints)
    url = "/app/expenses"

    # Frank visits the expenses page
    tester.browser.get(f"{tester.live_server_url}{url}")
    _select_data_range(tester, ranges["start"], ranges["end"])

    return _check_expense_existence(
        tester, data, expected_to_fail, check_decimals=False
    )


def find_spending_limit(tester, data, expected_to_fail=False):
    # NOTE: we hard code the URI because Django has no idea of React routes
    # (i.e. react internal virtual routes, not API endpoints)
    url = "/app/spending-limits"

    # Frank visits the spending limits page
    tester.browser.get(f"{tester.live_server_url}{url}")
    found = False
    try:
        table = WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 3).until(
            EC.presence_of_element_located(
                (By.ID, "spendinglimits-table-body")
            )
        )
        for _, e in enumerate(table.find_elements(By.TAG_NAME, "tr")):
            if found:
                break
            tds = e.find_elements(By.TAG_NAME, "td")
            if len(tds) != 8:
                tester.fail(
                    "Each row in the spending limits table should have 8"
                    " columns"
                )

            # If we're checking by spending_limit_id, we do a direct comparison
            # with the first column, and we're done
            if "id" in data:
                if tds[0].text == data["id"]:
                    found = True
                    break
            else:
                # Focus on JPY only for now
                # currency_symbol_dict = {
                #     "JPY": "￥",
                #     "EUR": "€",
                #     "USD": "$",
                # }
                # == f"{currency_symbol_dict[data['currency']]}{data['amount']:,}"
                if (
                    tds[1].text == data["cat_text"]
                    and tds[2].text == f"￥{data['amount']:,}"
                    and tds[3].text == data["currency"]
                    and tds[4].text == str(data["days"])
                    and tds[5].text == data["text"]
                ):
                    found = True
                    break
        if expected_to_fail and found:
            tester.fail(
                f"Spending Limit with amount {data['amount']},"
                f" category {data['cat_text']}, days {data['days']},"
                f" currency {data['currency']} and text {data['text']} was NOT"
                " supposed to be displayed... but it was: fail!"
            )

        if not expected_to_fail and not found:
            tester.fail(
                f"Spending Limit with amount {data['amount']}"
                f" category {data['cat_text']}, days {data['days']},"
                f" currency {data['currency']} and text {data['text']} "
                " was not found: fail!"
            )
    except NoSuchElementException:
        tester.fail("Spending Limit table was not displayed. Fail!")
    except TimeoutException:
        if not expected_to_fail:
            tester.fail(
                "Either Spending Limit API has not completed (yet), or the table"
                " of Spending Limit was not displayed. Fail!"
            )


def find_spending_limit_in_dashboard(tester, data, expected_to_fail=False):
    url = "/app"
    tester.browser.get(f"{tester.live_server_url}{url}")
    found = False
    # find h5 with class card-title (inside the card body) and text Spending Limits
    # then find the sibling div with class card-text, and search inside the <li>
    # elements for the text that contains The spending limits.
    try:
        title = "Spending Limits"
        currency_symbol_dict = {
            "JPY": "￥",
            "EUR": "€",
            "USD": "$",
        }
        print(
            "find_spending_limit_in_dashboard is horribly long and ugly,"
            " REFACTOR ME"
        )
        import locale

        old_locale = locale.getlocale()
        try:
            # USD have the symbol in front of the amount
            locale.setlocale(locale.LC_ALL, "en_US.UTF-8")
            formatted_amount = "$" + locale.format_string(
                "%.2f",
                data["amount"],
                grouping=True,
                monetary=True,
            )
            formatted_spent = "$" + locale.format_string(
                "%.2f",
                data["actually_spent"],
                grouping=True,
                monetary=True,
            )

            # JPY have the symbol in front of the amount, and have no decimals
            if data["currency"] == "JPY":
                formatted_amount = f"{currency_symbol_dict[data['currency']]}{int(data['amount']):,}"
                formatted_spent = f"{currency_symbol_dict[data['currency']]}{int(data['actually_spent']):,}"

            # EUR have the symbol in front of the amount, and have decimals
            if data["currency"] == "EUR":
                locale.setlocale(locale.LC_ALL, "it_IT.UTF-8")
                formatted_amount = (
                    locale.format_string(
                        "%.2f", data["amount"], grouping=True, monetary=True
                    )
                ) + f" {currency_symbol_dict[data['currency']]}"
                formatted_spent = (
                    locale.format_string(
                        "%.2f",
                        data["actually_spent"],
                        grouping=True,
                        monetary=True,
                    )
                ) + f" {currency_symbol_dict[data['currency']]}"

            limit_text = (
                f"Amount spent in {data['text']} in {data['cat_text']} the last"
                f" {data['days']} day(s) : {formatted_spent}/{formatted_amount}"
            )

            # Give some time to the card to be fully loaded
            xpath_selector_list = (
                f"//h5[@class='card-title' and text()='{title}']/"
                "ancestor::div[@class='card h-100']"
                "//li"
            )
            card_text_elements = WebDriverWait(
                tester.browser, MAX_WAIT_SECONDS * 4
            ).until(
                EC.presence_of_all_elements_located(
                    (By.XPATH, xpath_selector_list)
                )
            )
            for _, e in enumerate(card_text_elements):
                if limit_text in e.text:
                    found = True
                    if "color" in data:
                        # Confirm the color of the text is the expected one
                        tester.assertIn(
                            data["color"], e.get_attribute("class")
                        )
                    break

            if not found:
                tester.fail("Spending Limits card was not found. Fail!")
            if expected_to_fail and found:
                tester.fail("Spending Limits card was found. Fail!")
            if not expected_to_fail and not found:
                tester.fail("Spending Limits card was not found. Fail!")
        finally:
            locale.setlocale(locale.LC_ALL, old_locale)

    except NoSuchElementException:
        tester.fail("Spending Limit table was not displayed. Fail!")
    except TimeoutException:
        if not expected_to_fail:
            tester.fail(
                "Either Spending Limit API has not completed (yet), or the table"
                " of Spending Limit was not displayed. Fail!"
            )


def _check_expense_existence(
    tester, data, expected_to_fail, check_decimals=False
):
    expense_id = -1

    try:
        table = WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
            EC.presence_of_element_located((By.ID, "expenses-table-body"))
        )

        found = False
        for _, e in enumerate(table.find_elements(By.TAG_NAME, "tr")):
            if found:
                break
            tds = e.find_elements(By.TAG_NAME, "td")
            if len(tds) != 6:
                tester.fail(
                    "Each row in the expenses table should have 6 columns"
                )

            # Ingore decimals for JPY or any other currency that does not
            # have them
            amount = f"${data['amount']:.2f}"
            if not check_decimals:
                amount = f"￥{int(data['amount']):,}"

            # If we're checking by expense_id, we do a direct comparison
            # with the hidden id property on the row, and we're done
            possible_id = e.get_property("id")
            if "expense_id" in data:
                if (
                    "exp-" in possible_id
                    and possible_id.replace("exp-", "") == data["id"]
                ):
                    found = True
                    expense_id = int(possible_id.replace("exp-", ""))
                    break
            else:
                if (
                    tds[0].text == data["category_text"]
                    and tds[1].text == amount
                    and tds[2].text == data["note"]
                    and tds[3].text == data["date"]
                ):
                    found = True
                    if "exp-" in possible_id:
                        expense_id = int(possible_id.replace("exp-", ""))
                    break

        if expected_to_fail and found:
            tester.fail(
                f"Expense with amount {data['amount']} and"
                f" category {data['category_text']}, date {data['date']}"
                " was NOT supposed to be displayed... but it was: fail!"
            )

        if not expected_to_fail and not found:
            tester.fail(
                f"Expense with amount {data['amount']}"
                f" and category {data['category_text']}, date {data['date']}"
                " was not found: fail!"
            )

    except TimeoutException:
        if not expected_to_fail:
            tester.fail(
                "Either expenses API has not completed (yet), or the table of"
                " expenses was not displayed. Fail!"
            )

    return expense_id


def edit_an_expense_category(
    tester,
    category_id,
    category_name,
    is_archived,
    check_edit_success_popup=False,
    check_edit_failure_popup=False,
    check_inability_to_load_category=False,
):
    # NOTE: we hard code the URI because Django has no idea of React
    # routes (i.e. react internal virtual routes, not API endpoints)
    url = f"/app/categories/{category_id}"
    tester.browser.get(f"{tester.live_server_url}{url}")

    # If this user does not own the category, or the category does not exist
    # yet no data will be loaded, and a special banner will be displayed
    if check_inability_to_load_category:
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 5).until(
                EC.presence_of_element_located((By.CLASS_NAME, "alert-danger"))
            )
            failure_banner = tester.browser.find_element(
                By.CLASS_NAME, "alert-danger"
            )
            expected_error = (
                "Sorry, it seems the category you're trying to"
                " editing does not exist...or there was an"
                " error. Please try again later, or reload the"
                " page"
            )
            tester.assertEqual(failure_banner.text, expected_error)
        except NoSuchElementException:
            tester.fail("Failure popup was not displayed. Fail!")
    # If the category exist, proceed with the rest of the logic
    else:
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 5).until(
                EC.presence_of_element_located((By.ID, "text_field"))
            )
        except NoSuchElementException:
            tester.fail("Edit category UI did not load properly. Fail!")

        inputbox = tester.browser.find_element(By.ID, "text_field")
        inputbox.send_keys(category_name)

        is_archived_goal_checkbox = tester.browser.find_element(
            By.ID, "is_archived_field"
        )
        if (is_archived_goal_checkbox.is_selected() and not is_archived) or (
            not is_archived_goal_checkbox.is_selected() and is_archived
        ):
            # Toggle the checkbox only if the current state does not match
            # the desired one
            is_archived_goal_checkbox.click()

        inputbox.send_keys(Keys.ENTER)

        if check_edit_success_popup:
            # Frank likes very much confirmation green modals.
            # He expects one to be shown, as he filled in everything correctly
            try:
                WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 5).until(
                    EC.presence_of_element_located(
                        (By.CLASS_NAME, "alert-success")
                    )
                )
            except NoSuchElementException:
                tester.fail("Success popup was not displayed. Fail!")

        if check_edit_failure_popup:
            # Frank likes very much confirmation green modals.
            # He expects one to be shown, as he filled in everything correctly
            try:
                WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 5).until(
                    EC.presence_of_element_located(
                        (By.CLASS_NAME, "alert-danger")
                    )
                )
            except NoSuchElementException:
                tester.fail("Failure popup was not displayed. Fail!")


def create_saving_category(
    tester,
    category_name,
    check_create_success_popup=False,
    check_create_failure_popup=False,
):
    # NOTE: we hard code the URI because Django has no idea of React
    # routes (i.e. react internal virtual routes, not API endpoints)
    # url = f"/app/categories/{category_id}"
    url = "/app/saving-categories/creation"
    tester.browser.get(f"{tester.live_server_url}{url}")

    try:
        WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 5).until(
            EC.presence_of_element_located((By.ID, "text_field"))
        )
    except NoSuchElementException:
        tester.fail("Edit category UI did not load properly. Fail!")

    inputbox = tester.browser.find_element(By.ID, "text_field")
    inputbox.send_keys(category_name)
    inputbox.send_keys(Keys.ENTER)

    if check_create_success_popup:
        # Frank likes very much confirmation green modals.
        # He expects one to be shown, as he filled in everything correctly
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 5).until(
                EC.presence_of_element_located(
                    (By.CLASS_NAME, "alert-success")
                )
            )
        except NoSuchElementException:
            tester.fail("Success popup was not displayed. Fail!")

    if check_create_failure_popup:
        # Frank likes very much confirmation green modals.
        # He expects one to be shown, as he filled in everything correctly
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 5).until(
                EC.presence_of_element_located((By.CLASS_NAME, "alert-danger"))
            )
        except NoSuchElementException:
            tester.fail("Failure popup was not displayed. Fail!")


def input_saving_category_name_and_submit_form(tester, category_name):
    """
    Submit the saving category form.
    This method does NOT navigate to the creation page
    - it expects the selenium driver to have already navigated there,
    - it expects the form to be already fully loaded
    """
    inputbox = tester.browser.find_element(By.ID, "text_field")
    inputbox.clear()
    inputbox.send_keys(category_name)
    inputbox.send_keys(Keys.ENTER)

    # Frank likes very much confirmation green modals.
    # He expects one to be shown, as he filled in everything correctly
    try:
        WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 5).until(
            EC.presence_of_element_located((By.CLASS_NAME, "alert-success"))
        )
    except NoSuchElementException:
        tester.fail("Success popup was not displayed. Fail!")


def edit_saving_category(
    tester,
    cat_id,
    text,
    is_archived,
    check_successful_edit=False,
    check_failed_edit=False,
):
    url = f"/app/saving-category/{cat_id}"
    tester.browser.get(f"{tester.live_server_url}{url}")

    try:
        WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 5).until(
            EC.presence_of_element_located((By.ID, "text_field"))
        )
    except NoSuchElementException:
        tester.fail("Edit category UI did not load properly. Fail!")

    inputbox = tester.browser.find_element(By.ID, "text_field")
    inputbox.clear()
    inputbox.send_keys(text)

    is_archived = tester.browser.find_element(By.ID, "is_archived_field")

    if (is_archived.is_selected() and not is_archived) or (
        not is_archived.is_selected() and is_archived
    ):
        # Toggle the checkbox only if the current state does NOT match
        # the desired one
        is_archived.click()

    inputbox.send_keys(Keys.ENTER)

    if check_successful_edit:
        # Frank likes very much confirmation green modals.
        # He expects one to be shown, as he filled in everything correctly
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 5).until(
                EC.presence_of_element_located(
                    (By.CLASS_NAME, "alert-success")
                )
            )
        except NoSuchElementException:
            tester.fail("Success popup was not displayed. Fail!")

    if check_failed_edit:
        # Frank likes very much confirmation green modals.
        # He expects one to be shown, as he filled in everything correctly
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 5).until(
                EC.presence_of_element_located((By.CLASS_NAME, "alert-danger"))
            )
        except NoSuchElementException:
            tester.fail("Failure popup was not displayed. Fail!")


def guidance_check(
    tester,
    title,
    guidance,
    button_text,
    header_class,
    button_url,
    expect_to_be_missing=False,
    check_links=True,
    is_widget=True,
    button_order=0,
):
    xpath_selector = (
        f"//h5[@class='card-title' and text()='{title}']/"
        "ancestor::div[@class='card h-100']"
    )

    # Frank waits for the guidance to be shown
    try:
        # Check the guidance for a widget, in any page
        if is_widget:
            card_element = WebDriverWait(
                tester.browser, MAX_WAIT_SECONDS * 2
            ).until(EC.presence_of_element_located((By.XPATH, xpath_selector)))

            xpath = f".//h5[@class='{header_class}']"
            card_text_element = WebDriverWait(
                card_element, MAX_WAIT_SECONDS * 2
            ).until(EC.presence_of_element_located((By.XPATH, xpath)))

            card_text = card_text_element.text.strip()
        # Check the guidance for a list, edit, or create page
        else:
            # HACK: Make the test less flickery by waiting a bit for the page to load
            # It seems the Spending Limit creation view is a bit slow to load
            # TODO: find out why
            time.sleep(2)
            h5 = WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 2).until(
                EC.presence_of_element_located((By.TAG_NAME, "h5"))
            )
            card_text = h5.text.strip()
            card_element = h5.find_element(By.XPATH, "..")

        if expect_to_be_missing:
            tester.assertNotEqual(card_text, guidance)
        else:
            tester.assertEqual(card_text, guidance)

        if check_links:
            _check_links(
                tester,
                expect_to_be_missing,
                card_element,
                button_text,
                button_url,
                button_order=button_order,
            )

    except TimeoutException as e:
        if not expect_to_be_missing:
            print(e, flush=True)
            tester.fail("Guidance was not properly displayed. Fail!")
    except NoSuchElementException as e:
        if not expect_to_be_missing:
            print(e, flush=True)
            tester.fail("Guidance was not properly displayed. Fail!")


def _check_links(
    tester,
    expect_to_be_missing,
    card_element,
    button_text,
    button_url,
    button_order=0,
):
    try:
        link_element = WebDriverWait(card_element, MAX_WAIT_SECONDS * 2).until(
            EC.presence_of_element_located((By.TAG_NAME, "a"))
        )

        # Assets widget has two buttons, handle this edge case
        if button_order > 0:
            link_element = card_element.find_elements(By.TAG_NAME, "a")[
                button_order
            ]

        link_text = link_element.text.strip()
        link_href = link_element.get_attribute("href")

        if expect_to_be_missing:
            tester.assertNotEqual(
                link_href, f"{tester.live_server_url}{button_url}"
            )
            tester.assertNotEqual(link_text, button_text)
        else:
            tester.assertEqual(
                link_href, f"{tester.live_server_url}{button_url}"
            )
            tester.assertEqual(link_text, button_text)
    except NoSuchElementException as e:
        if not expect_to_be_missing:
            print(e, flush=True)
            tester.fail("Guidance was not properly displayed. Fail!")
    except TimeoutException as e:
        if not expect_to_be_missing:
            print(e, flush=True)
            tester.fail("Guidance was not timely displayed (yet?). Fail!")


def check_guidance_content(tester, title, cateories):
    xpath_selector = (
        f"//h5[@class='card-title' and text()='{title}']/"
        "ancestor::div[@class='card h-100']"
    )

    try:
        card_element = WebDriverWait(
            tester.browser, MAX_WAIT_SECONDS * 5
        ).until(EC.presence_of_element_located((By.XPATH, xpath_selector)))
        list_group = WebDriverWait(card_element, MAX_WAIT_SECONDS * 5).until(
            EC.presence_of_element_located((By.CLASS_NAME, "list-group"))
        )
        _ = WebDriverWait(list_group, MAX_WAIT_SECONDS * 5).until(
            EC.presence_of_element_located((By.TAG_NAME, "li"))
        )
        cat_lis = (
            card_element.find_element(By.XPATH, xpath_selector)
            .find_element(By.CLASS_NAME, "list-group")
            .find_elements(By.TAG_NAME, "li")
        )
        for _, c in enumerate(cateories):
            found = False
            for _, li in enumerate(cat_lis):
                if li.text == c:
                    found = True

            # Assert each Category is appearing
            msg = (
                "Some (Spending or Saving) Categories were not displyed."
                " Failing!"
            )
            tester.assertTrue(found, msg=msg)

    except NoSuchElementException as e:
        print(e, flush=True)
        tester.fail("Guidance was not properly displayed. Fail!")


def check_guidance_text_and_ui(
    tester, title, class_name, guidance, secondary_text=None
):
    xpath_selector = (
        f"//h5[@class='card-title' and text()='{title}']/"
        "ancestor::div[@class='card h-100']"
    )
    # HACK: Make the test less flickery by waiting a bit for the page to load
    time.sleep(4)
    parent_element = WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 3).until(
        EC.presence_of_element_located((By.XPATH, xpath_selector))
    )
    card_text_element = WebDriverWait(
        parent_element, MAX_WAIT_SECONDS * 3
    ).until(EC.presence_of_element_located((By.CLASS_NAME, "card-text")))

    # Regrab the element as it might have became stale due to react
    # re-rendering
    parent_element = WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 3).until(
        EC.presence_of_element_located((By.XPATH, xpath_selector))
    )
    text_of_selected_class_element = WebDriverWait(
        parent_element, MAX_WAIT_SECONDS * 3
    ).until(EC.presence_of_element_located((By.CLASS_NAME, class_name)))
    # The category has now a budget, so no more warnings
    tester.assertIn(
        guidance,
        text_of_selected_class_element.text,
    )
    if secondary_text:
        card_text_element = parent_element.find_element(
            By.CLASS_NAME, "card-text"
        )
        tester.assertIn(secondary_text, card_text_element.text)


def create_an_income_category(
    tester,
    category_name,
    check_creation_popup=False,
    check_failure_popup=False,
):
    # NOTE: we hard code the URI because Django has no idea of React
    # routes (i.e. react internal virtual routes, not API endpoints)
    url = "/app/income-categories/creation"

    # Frank creates a category
    tester.browser.get(f"{tester.live_server_url}{url}")
    inputbox = tester.browser.find_element(By.ID, "text_field")
    placeholder_text = "Enter the income category name"

    tester.assertEqual(inputbox.get_attribute("placeholder"), placeholder_text)

    inputbox.send_keys(category_name)
    inputbox.send_keys(Keys.ENTER)

    if check_creation_popup:
        # Frank likes very much confirmation green modals.
        # He expects one to be shown, as he filled in everything correctly
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located(
                    (By.CLASS_NAME, "alert-success")
                )
            )
            expected_text = "Income Category created!"
            modal_text = tester.browser.find_element(
                By.CLASS_NAME, "alert-heading"
            )
            tester.assertEqual(modal_text.text, expected_text)

        except NoSuchElementException:
            tester.fail("Success popup was not displayed. Fail!")

    if check_failure_popup:
        # Frank likes very much confirmation red modals.
        # He is shown that something went wrong, and why
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located((By.CLASS_NAME, "alert-danger"))
            )
            expected_text = "Income Category creation failed :("
            modal_text = tester.browser.find_element(
                By.CLASS_NAME, "alert-heading"
            )
            tester.assertEqual(modal_text.text, expected_text)
        except NoSuchElementException:
            tester.fail("Failure popup was not displayed. Fail!")


def edit_an_income_category(
    tester,
    category_id,
    category_name,
    check_edit_success_popup=False,
    check_edit_failure_popup=False,
    check_inability_to_load_category=False,
    expected_error_message=None,
):
    # NOTE: we hard code the URI because Django has no idea of React
    # routes (i.e. react internal virtual routes, not API endpoints)
    url = f"/app/income-categories/{category_id}"

    # Frank creates a category
    tester.browser.get(f"{tester.live_server_url}{url}")

    # If this user does not own the category, or the category does not exist
    # yet no data will be loaded, and a special banner will be displayed
    if check_inability_to_load_category:
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located((By.CLASS_NAME, "alert-danger"))
            )
            failure_banner = tester.browser.find_element(
                By.CLASS_NAME, "alert-danger"
            )
            expected_error = (
                "Sorry, it seems the income category you're try"
                "ing to editing does not exist...or there was"
                " an error. Please try again later, or reload"
                " the page"
            )
            tester.assertEqual(failure_banner.text, expected_error)
        except NoSuchElementException:
            tester.fail("Failure popup was not displayed. Fail!")
    # If the category exist, proceed with the rest of the logic
    else:
        # Let's wait for all the APIs to come back
        # and let's give the frontend some time to properly render
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located((By.ID, "text_field"))
            )
        except NoSuchElementException:
            tester.fail("input field was not displayed. Fail!")

        inputbox = tester.browser.find_element(By.ID, "text_field")

        # IMPORTANT: we need to clear the input before sending keys, or the
        # input field will end up having the original input value AND the keys
        # we send (this is not a problem ONLY if the input field was
        # originally empty)
        inputbox.clear()
        inputbox.send_keys(category_name)
        inputbox.send_keys(Keys.ENTER)

        if check_edit_success_popup:
            # Frank likes very much confirmation green modals.
            # He expects one to be shown, as he filled in everything correctly
            try:
                WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                    EC.presence_of_element_located(
                        (By.CLASS_NAME, "alert-success")
                    )
                )
            except NoSuchElementException:
                tester.fail("Success popup was not displayed. Fail!")

        if check_edit_failure_popup:
            # Frank likes very much confirmation green modals.
            # He expects one to be shown, as he filled in everything correctly
            try:
                WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                    EC.presence_of_element_located(
                        (By.CLASS_NAME, "alert-danger")
                    )
                )
                form_err = tester.browser.find_element(
                    By.CLASS_NAME, "text-danger"
                )
                tester.assertEqual(form_err.text, expected_error_message)
            except NoSuchElementException:
                tester.fail("Failure popup was not displayed. Fail!")


def check_income_category_existence(tester, text, expected_to_fail=False):
    """
    Check if an income category with a given name exists
    Returns the category ID if it exists, -1 otherwise
    """
    complete_url = f"/app/income-categories"
    tester.browser.get(f"{tester.live_server_url}{complete_url}")
    category_id = -1

    try:
        table = WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 3).until(
            EC.presence_of_element_located((By.ID, "categories-table-body"))
        )

        found = False
        for _, tr in enumerate(table.find_elements(By.TAG_NAME, "tr")):
            if found:
                break
            tds = tr.find_elements(By.TAG_NAME, "td")
            if len(tds) != 3:
                tester.fail(
                    "Each row in the income categories table should have 3 columns"
                )
            if text == tds[1].text:
                found = True
                try:
                    category_id = int(tds[0].text)
                except ValueError:
                    tester.fail(
                        "Income Category was not displayed with the format we"
                        " did expect. Failed to convert ID to integer. Fail!"
                    )

        if expected_to_fail and found:
            tester.fail(
                f"Income Category with name {text} was NOT supposed"
                " to be displayed... but it was: fail!"
            )

        if not expected_to_fail and not found:
            tester.fail(
                f"Income Category with name {text} was not" " found: fail!"
            )

    except TimeoutException:
        if not expected_to_fail:
            tester.fail(
                "Either Income Categories API has not completed (yet), or the"
                "list of Income Categories was not displayed. Fail!"
            )

    except NoSuchElementException:
        tester.fail(
            "Either Income Categories API has not completed (yet), or the"
            "list of Income Categories was not displayed. Fail!"
        )

    return category_id


def create_an_account(
    tester,
    acc_name,
    credit_entity_name,
    is_foreign_currency,
    check_create_success_popup=False,
    check_create_failure_popup=False,
):
    # NOTE: we hard code the URI because Django has no idea of React
    # routes (i.e. react internal virtual routes, not API endpoints)
    url = "/app/accounts/creation"
    tester.browser.get(f"{tester.live_server_url}{url}")

    try:
        WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 3).until(
            EC.presence_of_element_located((By.ID, "text_field"))
        )
    except NoSuchElementException:
        tester.fail("Account creation UI did not load in time. Fail!")

    inputbox = tester.browser.find_element(By.ID, "text_field")
    entity = tester.browser.find_element(By.ID, "credit_entity")
    foreign_curr_checkbox = tester.browser.find_element(
        By.ID, "is_foreign_currency"
    )

    inputbox.send_keys(acc_name)
    entity.send_keys(credit_entity_name)
    if is_foreign_currency:
        foreign_curr_checkbox.click()

    submit_button = tester.browser.find_element(By.TAG_NAME, "button")
    submit_button.click()

    if check_create_success_popup:
        # Frank likes very much confirmation green modals.
        # He expects one to be shown, as he filled in everything correctly
        try:
            alert = WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located(
                    (By.CLASS_NAME, "alert-success")
                )
            )
            assert "Account created!" in alert.text
        except NoSuchElementException:
            tester.fail("Success popup was not displayed. Fail!")
        except TimeoutException:
            tester.fail("Success popup was not displayed (in time?). Fail!")

    if check_create_failure_popup:
        # Frank very likes the alert red modals. He is shown that something
        # went wrong, and why
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located((By.CLASS_NAME, "alert-danger"))
            )
        except NoSuchElementException:
            tester.fail("Failure popup was not displayed. Fail!")
        except TimeoutException:
            tester.fail("Failue popup was not displayed (in time?). Fail!")


def check_account_existence(
    tester, text, credit_entity, is_foreign_currency, expected_to_fail=False
):
    """
    Check if a account with given text, credit entity and foreign currency status exists
    Returns the account ID if it exists, -1 otherwise
    """
    complete_url = f"/app/accounts"
    tester.browser.get(f"{tester.live_server_url}{complete_url}")
    account_id = -1
    is_foreign_currency_str = "Yes" if is_foreign_currency else "No"

    try:
        table = WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
            EC.presence_of_element_located((By.ID, "accounts-table-body"))
        )

        found = False
        for _, tr in enumerate(table.find_elements(By.TAG_NAME, "tr")):
            if found:
                break
            tds = tr.find_elements(By.TAG_NAME, "td")
            if len(tds) != 4:
                tester.fail(
                    "Each row in the account table should have 3 columns"
                )
            if (
                tds[1].text == text
                and tds[2].text == credit_entity
                and tds[3].text == is_foreign_currency_str
            ):
                found = True
                try:
                    account_id = int(tds[0].text)
                except ValueError:
                    tester.fail(
                        "Account was not displayed with the format we did"
                        " expect. Failed to convert ID to integer. Fail!"
                    )

        if expected_to_fail and found:
            tester.fail(
                f"Account with name {text} was NOT supposed"
                " to be displayed... but it was: fail!"
            )

        if not expected_to_fail and not found:
            tester.fail(f"Account with name {text} was not" " found: fail!")

    except TimeoutException:
        if not expected_to_fail:
            tester.fail(
                "Either accounts API has not completed (yet), or the"
                "list of accounts was not displayed. Fail!"
            )

    except NoSuchElementException:
        tester.fail(
            "Either accounts API has not completed (yet), or the"
            "list of accounts was not displayed. Fail!"
        )

    return account_id


def list_accounts(tester, accounts):
    url = "/app/accounts"
    tester.browser.get(f"{tester.live_server_url}{url}")

    try:
        table = WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
            EC.presence_of_element_located((By.ID, "accounts-table-body"))
        )
        rows = table.find_elements(By.TAG_NAME, "tr")
        tester.assertEqual(
            len(rows), len(accounts) + 1
        )  # +1 for the header row

        for i, acc in enumerate(accounts):
            expected_text = f"{acc['credit_entity']}: {acc['text']}" + (
                " (Foreign currency)" if acc["is_foreign_currency"] else ""
            )
            actual_text = rows[i + 1].text  # +1 to skip the header row
            tester.assertEqual(expected_text, actual_text)

        # for _, tr in enumerate(table.find_elements(By.TAG_NAME, "tr")):
        #     if found:
        #         break
        #     tds = tr.find_elements(By.TAG_NAME, "td")
        #     if len(tds) != 3:
        #         tester.fail(
        #             "Each row in the categories table should have 3 columns"
        #         )
        #     if text == tds[1].text:
        #         found = True
        #         try:
        #             category_id = int(tds[0].text)
        #         except ValueError:
        #             tester.fail(
        #                 "Category was not displayed with the format we did"
        #                 " expect. Failed to convert ID to integer. Fail!"
        #             )

    except TimeoutException:
        tester.fail("Account table was not displayed (in time?). Fail!")

    except NoSuchElementException:
        tester.fail(
            "Either account API has not completed (yet),"
            " or the list of accounts was not displayed. Fail!"
        )


def import_dividends_from_third_party_api(
    tester, apiURL, target_date, check_success_text=True
):
    print(
        "import_dividends_from_third_party_api is calling the API directly."
        " This makes the test flaky. Address it to close #46"
    )
    url = "/app/dividends/import"
    tester.browser.get(f"{tester.live_server_url}{url}")

    # Allow some time for the UI to render properly
    try:
        WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 10).until(
            EC.presence_of_element_located((By.ID, "importUrl"))
        )
    except NoSuchElementException:
        tester.fail("The UI has not properly loaded (yet?). Fail!")

    url_input = tester.browser.find_element(By.ID, "importUrl")
    date_picker = tester.browser.find_element(By.ID, "targetDate")
    button = tester.browser.find_element(
        By.XPATH, '//button[text()="Fetch and create"]'
    )

    url_input.clear()
    url_input.send_keys(apiURL)

    date_picker.clear()
    date_picker.send_keys(target_date)

    button.click()

    # Dividend API endpoint can be very slow if the third party API returns
    # a lot of data
    try:
        WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 15).until(
            EC.presence_of_element_located((By.CLASS_NAME, "alert"))
        )
        alert = tester.browser.find_element(By.CLASS_NAME, "alert")
        if check_success_text:
            # FIXME: confirm we're checking the correct alert: we have a
            # green success alert (?) and the results recap alert!
            tester.assertTrue(
                f"accounts have data for {target_date}" in alert.text
            )
            tester.assertTrue(
                "All dividends have been created successfully!" in alert.text
            )
    except NoSuchElementException:
        tester.fail(
            "Either dividends import API has not completed, or the alert was"
            " not displayed. Fail!"
        )


def import_assets_from_third_party_api(
    tester, apiURL, target_date, check_success_text=True
):
    url = "/app/assets/import"
    tester.browser.get(f"{tester.live_server_url}{url}")

    # Allow some time for the UI to render properly
    try:
        WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 10).until(
            EC.presence_of_element_located((By.ID, "importUrl"))
        )
    except NoSuchElementException:
        tester.fail("The UI has not properly loaded (yet?). Fail!")

    url_input = tester.browser.find_element(By.ID, "importUrl")
    date_picker = tester.browser.find_element(By.ID, "targetDate")
    button = tester.browser.find_element(By.XPATH, '//button[text()="Fetch"]')

    url_input.clear()
    url_input.send_keys(apiURL)

    date_picker.clear()
    date_picker.send_keys(target_date)

    button.click()

    # Dividend API endpoint can be very slow if the third party API
    # returns a lot of data
    try:
        WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 15).until(
            EC.presence_of_element_located((By.CLASS_NAME, "alert"))
        )
        alert = tester.browser.find_element(By.CLASS_NAME, "alert")
        if check_success_text:
            tester.assertTrue(
                f"accounts have data for {target_date}" in alert.text
            )
            tester.assertTrue(
                "All assets have been created successfully!" in alert.text
            )
    except NoSuchElementException:
        msg = (
            "Either assets import API has not completed (yet), or the alert"
            "was not displayed. Fail!"
        )
        tester.fail(msg)


def list_dividends(tester, dividends):
    url = "/app/dividends"
    tester.browser.get(f"{tester.live_server_url}{url}")

    try:
        # We use this dirty way to select the dividends details widget, as
        # we have three widget loading async, waiting for a 'card-title' class
        # would not be sufficient, as we might select the wrong widget
        xpath_def = "//h5[@class='card-title' and text()='Dividends details']"
        element_with_profit_details = WebDriverWait(
            tester.browser, MAX_WAIT_SECONDS * 5
        ).until(EC.presence_of_element_located((By.XPATH, xpath_def)))

        element_with_profit_details = tester.browser.find_element(
            By.XPATH, xpath_def
        )
        # See above comment, this is a horrible hack
        path2 = "//h5[text()='Dividends details']/following-sibling::div"
        dividend_rows = element_with_profit_details.find_elements(
            By.XPATH, path2
        )[0].find_elements(By.TAG_NAME, "tr")
        num_siblings = len(dividend_rows) - 1  # Subtract 1 for the header row
        tester.assertEqual(num_siblings, len(dividends))

        # Loop through all the dividends, and check for all the <tr> content
        # This very suboptimal procedure is it due to the fact that the
        # dividends might be displayed in a different order compared to the
        # dividends's argument (array) arg depending on how table is sorted
        # (i.e. which dividend create API did complete first)
        for _, dividend in enumerate(dividends):
            found = False
            for _, row in enumerate(dividend_rows[1:]):  # Skip the header row
                cells = row.find_elements(By.TAG_NAME, "td")
                if len(cells) < 2:
                    continue
                asset_text = cells[0].text
                record_date = cells[1].text
                amount = cells[2].text
                formatted_amount = f"${dividend['amount']:.2f}"
                if (
                    asset_text == dividend["ticker"]
                    and record_date == dividend["date"]
                    and amount == formatted_amount
                ):
                    found = True

            # Assert each dividend is appearing
            msg = "Some dividends were not displayed. Failing!"
            tester.assertTrue(found, msg=msg)

    except NoSuchElementException:
        err_msg = (
            "Either dividends API has not completed (yet), or the list "
            "of dividends was not displayed. Fail!"
        )
        tester.fail(err_msg)


def list_assets(tester, assets, target_date):
    url = "/app/assets"
    tester.browser.get(f"{tester.live_server_url}{url}")

    try:
        element_with_profit_details = WebDriverWait(
            tester.browser, MAX_WAIT_SECONDS * 10
        ).until(EC.presence_of_element_located((By.ID, "assets-table")))
        table = tester.browser.find_element(By.ID, "assets-table")
        assets_trs = table.find_elements(By.CLASS_NAME, "asset")
        # Table row with all assets total value
        _ = assets_trs[0]
        # The actual table rows displaying assets
        assetOnlyTrs = assets_trs[1:]
        tester.assertEqual(len(assetOnlyTrs), len(assets))

        # Loop through all the assets, and check for all the <tr> content
        # This very suboptimal procedure is it due to the fact that the
        # assets might  be displayed in a different order compared to the
        # asset's argument (array) arg depending on how table is sorted
        for _, asset in enumerate(assets):
            found = False
            for _, tr in enumerate(assetOnlyTrs):
                needle = asset["ticker"]
                if tr.text.startswith(needle):
                    found = True
                    tds = tr.find_elements(By.TAG_NAME, "td")

                    # We have 10 columns in the table as we've introduced the
                    # delete checkbox
                    tester.assertEqual(len(tds), 10)
                    tester.assertEqual(
                        int(tds[3].text.replace(",", "")), asset["amount"]
                    )
                    tester.assertEqual(tds[8].text, asset["currency"])
                    tester.assertEqual(tds[9].text, target_date)

            # Assert each asset is appearing
            tester.assertTrue(
                found, msg="Some assets were not displyed. Failing!"
            )

    except NoSuchElementException:
        err_msg = (
            "Either assets API has not completed, or the AssetTable"
            " component was not displayed. Fail!"
        )
        tester.fail(err_msg)


def create_an_expense(
    tester,
    category_id,
    amount=0,
    date="",
    note="",
    check_creation_popup=False,
    check_failure_popup=False,
):
    # NOTE: we hard code the URI because Django has no idea of React
    # routes (i.e. react internal virtual routes, not API endpoints)
    url = "/app/expenses/creation"

    # Frank creates a category
    tester.browser.get(f"{tester.live_server_url}{url}")

    # The category API might take some time to get back
    # let's make this test less flaky by allowing it some slack
    try:
        WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
            EC.presence_of_element_located((By.ID, "category_field"))
        )
    except NoSuchElementException:
        tester.fail("Success popup was not displayed. Fail!")

    dropdown = Select(tester.browser.find_element(By.ID, "category_field"))
    # The dropdown includes the Category Frank wants to set the expense for
    # Frank chooses that category
    # Note: selenium requires the value to be a string, hence the str() casting
    dropdown.select_by_value(str(category_id))

    # Let's allow this method to accept both strings and decimals
    if type(amount) == decimal.Decimal:
        amount = str(amount)
    amount_inputbox = tester.browser.find_element(By.ID, "amount_field")
    amount_inputbox.send_keys(amount)

    date_inputbox = tester.browser.find_element(By.ID, "date_entity")
    date_inputbox.send_keys(date)

    note_inputbox = tester.browser.find_element(By.ID, "note_field")
    note_inputbox.send_keys(note)

    amount_inputbox.send_keys(Keys.ENTER)

    if check_creation_popup:
        # Frank likes very much confirmation green modals.
        # He expects one to be shown, as he filled in everything correctly
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located(
                    (By.CLASS_NAME, "alert-success")
                )
            )
            expected_text = "Expense created!"
            modal_text = tester.browser.find_element(
                By.CLASS_NAME, "alert-heading"
            )
            tester.assertEqual(modal_text.text, expected_text)
        except NoSuchElementException:
            tester.fail("Success popup was not displayed. Fail!")

    if check_failure_popup:
        # Frank very likes confirmation red modals. He is shown that
        # something went wrong, and why
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located((By.CLASS_NAME, "alert-danger"))
            )
            failure_banner = tester.browser.find_element(
                By.CLASS_NAME, "alert-danger"
            )
            expected_text = "Expense deletion failed :("
            modal_text = tester.browser.find_element(
                By.CLASS_NAME, "alert-heading"
            )
            tester.assertEqual(failure_banner.text, expected_text)
        except NoSuchElementException:
            tester.fail("Failure popup was not displayed. Fail!")


def edit_an_expense(
    tester,
    expense_id,
    category_id,
    amount=0,
    date="",
    note="",
    check_denied_permission_popup=False,
    check_creation_popup=False,
    check_failure_popup=False,
):
    # NOTE: we hard code the URI because Django has no idea of React
    # routes (i.e. react internal virtual routes, not API endpoints)
    url = f"/app/expenses/{expense_id}"

    # Frank accesses the expense page
    tester.browser.get(f"{tester.live_server_url}{url}")

    if check_denied_permission_popup:
        # Frank very likes the alert red modal: he is shown that something
        # went wrong, and why
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located((By.CLASS_NAME, "alert-danger"))
            )
            alert_text = tester.browser.find_element(
                By.ID, "invalid-expense-marker"
            )
            err_msg = (
                "Sorry, it seems the expense you're trying to editing"
                " does not exist...or there was an error. Please try"
                " again later, or reload the page"
            )
            expected_alert_text = err_msg
            tester.assertEqual(alert_text.text, expected_alert_text)
        except NoSuchElementException:
            tester.fail("Failure popup was not displayed. Fail!")
    else:
        try:
            # This test depends on an api: give some time for the UI to
            # properly loadthe data and render accordingly
            # FIXME: confirm this id is correct
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located((By.ID, "category_field"))
            )
            # Frank methodically alters each field
            dropdown = Select(
                tester.browser.find_element(By.ID, "category_field")
            )
            # The dropdown includes the Category Frank wants to set a budget
            # for Frank chooses that category
            # Note, selenium requires the value to be a string, hence
            # the str() casting
            dropdown.select_by_value(str(category_id))

            amount_inputbox = tester.browser.find_element(
                By.ID, "amount_field"
            )
            amount_inputbox.clear()
            amount_inputbox.send_keys(str(amount))

            date_inputbox = tester.browser.find_element(By.ID, "date_entity")
            date_inputbox.clear()
            date_inputbox.send_keys(date)

            note_inputbox = tester.browser.find_element(By.ID, "note_field")
            note_inputbox.clear()
            note_inputbox.send_keys(note)

            amount_inputbox.send_keys(Keys.ENTER)

            if check_creation_popup:
                # Frank likes very much confirmation green modals.
                # He expects one to be shown as he filled in everything
                # correctly
                try:
                    WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                        EC.presence_of_element_located(
                            (By.CLASS_NAME, "alert-success")
                        )
                    )
                    expected_text = "Expense updated!"
                    modal_text = tester.browser.find_element(
                        By.CLASS_NAME, "alert-heading"
                    )
                    tester.assertEqual(modal_text.text, expected_text)
                except NoSuchElementException:
                    tester.fail("Success popup was not displayed. Fail!")

            if check_failure_popup:
                # Frank very likes confirmation red modals.
                # He is shown that something went wrong, and why
                try:
                    WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                        EC.presence_of_element_located(
                            (By.CLASS_NAME, "alert-danger")
                        )
                    )
                    expected_text = "An error has accourred. Please try again."
                    modal_text = tester.browser.find_element(
                        By.CLASS_NAME, "alert-heading"
                    )
                    tester.assertEqual(modal_text.text, expected_text)

                except NoSuchElementException:
                    tester.fail("Failure popup was not displayed. Fail!")
        except NoSuchElementException:
            tester.fail("Category dropdown did not load timely. Fail!")


def visit_an_expense(
    tester,
    expense_id,
    category_name,
    category_id,
    amount=0,
    date="",
    note="",
    check_failure_popup=True,
):
    # NOTE: we hard code the URI because Django has no idea of React routes
    # (i.e. react internal virtual routes, not API endpoints)
    url = f"/app/expenses/{expense_id}"

    # Frank accesses the expense page
    tester.browser.get(f"{tester.live_server_url}{url}")

    if check_failure_popup:
        # Frank very likes the alert red modal: he knows something went
        # wrong, and why
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located((By.CLASS_NAME, "alert-danger"))
            )
            alert_text = tester.browser.find_element(
                By.ID, "invalid-expense-marker"
            )
            expected_alert_text = (
                "Sorry, it seems the expense you're trying"
                " to editing does not exist...or there was"
                " an error. Please try again later, or"
                " reload the page"
            )
            tester.assertEqual(alert_text.text, expected_alert_text)
        except NoSuchElementException:
            tester.fail("Failure popup was not displayed. Fail!")
    else:
        # Frank methodically checks each field: they should all match!
        dropdown = Select(tester.browser.find_element(By.ID, "category_field"))
        selected_option = dropdown.first_selected_option
        selected_value = selected_option.get_attribute("value")

        tester.assertEqual(selected_option, category_name)
        tester.assertEqual(selected_value, category_id)

        amount_inputbox = tester.browser.find_element(By.ID, "amount_field")
        prefilled_amount = amount_inputbox.get_attribute("value")
        # Let's allow this method to accept both strings and decimals
        if type(amount) == decimal.Decimal:
            amount = str(amount)
        tester.assertEqual(prefilled_amount, amount)

        date_inputbox = tester.browser.find_element(By.ID, "date_entity")
        prefilled_date = date_inputbox.get_attribute("value")
        tester.assertEqual(prefilled_date, date)

        note_inputbox = tester.browser.find_element(By.ID, "note_field")
        prefilled_note = note_inputbox.get_attribute("value")
        tester.assertEqual(prefilled_note, note)


def delete_an_expense(
    tester,
    expense_id,
    exp_date,
    check_successful_deletion_popup=False,
    check_failed_deletion_popup=False,
):
    # NOTE: we hard code the URI because Django has no idea of React routes
    # (i.e. react internal virtual routes, not API endpoints)
    url = "/app/expenses"
    print(
        "REFACTOR ME: delete_an_expense could be refactored to a more generic delete_an_object",
        flush=True,
    )

    # Frank creates a category
    tester.browser.get(f"{tester.live_server_url}{url}")
    _select_data_range(tester, exp_date, exp_date)

    # The UI needs to wait for all the API calls to return before rendering
    # correcly: let's give the test some slack
    try:
        WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
            EC.presence_of_element_located((By.ID, "expenses-table-body"))
        )
    except NoSuchElementException:
        tester.fail("Expense table body was not displayed. Fail!")
    except TimeoutException:
        tester.fail("Expense table body was not displayed (in time?). Fail!")

    delete_button = tester.browser.find_element(
        By.ID, f"deleteExpenseButton-{expense_id}"
    )
    delete_button.click()

    # Frank very likes that the app asks him to confirm before actually
    # deleting the expense
    try:
        WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
            EC.presence_of_element_located((By.ID, "confirmDeletion"))
        )
        confirm_deletion = tester.browser.find_element(
            By.ID, "confirmDeletion"
        )
        confirm_deletion.click()
    except NoSuchElementException:
        tester.fail("Delete modal was not displayed. Fail!")
    except TimeoutException:
        tester.fail("Delete modal was not displayed (in time?). Fail!")

    if check_successful_deletion_popup:
        # Frank very likes confirmation green modals. He expects one to be
        # shown as he all the rights to delete his own expenses
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located(
                    (By.CLASS_NAME, "alert-success")
                )
            )
            expected_text = "Expense deleted successfully!"
            modal_text = tester.browser.find_element(
                By.CLASS_NAME, "alert-heading"
            )
            tester.assertEqual(modal_text.text, expected_text)
        except NoSuchElementException:
            tester.fail("Success popup was not displayed. Fail!")

    if check_failed_deletion_popup:
        # Frank very likes confirmation red modals.
        # He expects one to be shown when something is not OK!
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located((By.CLASS_NAME, "alert-danger"))
            )
            expected_text = "Expense deletion failed :("
            modal_text = tester.browser.find_element(
                By.CLASS_NAME, "alert-heading"
            )
            tester.assertEqual(modal_text.text, expected_text)
        except NoSuchElementException:
            tester.fail("Failure popup was not displayed. Fail!")


def create_an_income(
    tester,
    category_id,
    amount=0,
    date="",
    note="",
    check_creation_popup=False,
    check_failure_popup=False,
):
    # NOTE: we hard code the URI because Django has no idea of React routes
    # (i.e. react internal virtual routes, not API endpoints)
    url = "/app/incomes/creation"

    # Frank creates a category
    tester.browser.get(f"{tester.live_server_url}{url}")
    # Categories are loaded with another API call
    # the UI might not be displaying the dropdown yet, when the page is loaded

    try:
        WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
            EC.presence_of_element_located((By.ID, "category_field"))
        )
        dropdown = Select(tester.browser.find_element(By.ID, "category_field"))
        # The dropdown includes the Category Frank wants to set the income for
        # Frank chooses that category
        # Note, selenium requires the value to be a string, hence the str()
        # casting
        dropdown.select_by_value(str(category_id))
    except TimeoutException:
        tester.fail("Category dropdown did not load timely. Fail!")

    amount_inputbox = tester.browser.find_element(By.ID, "amount_field")
    amount_inputbox.send_keys(amount)

    date_inputbox = tester.browser.find_element(By.ID, "date_entity")
    date_inputbox.send_keys(date)

    note_inputbox = tester.browser.find_element(By.ID, "note_field")
    note_inputbox.send_keys(note)

    amount_inputbox.send_keys(Keys.ENTER)

    if check_creation_popup:
        # Frank likes very much confirmation green modals.
        # He expects one to be shown, as he filled in everything correctly
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located(
                    (By.CLASS_NAME, "alert-success")
                )
            )
            expected_text = "Income created!"
            modal_text = tester.browser.find_element(
                By.CLASS_NAME, "alert-heading"
            )
            tester.assertEqual(modal_text.text, expected_text)
        except NoSuchElementException:
            tester.fail("Success popup was not displayed. Fail!")

    if check_failure_popup:
        # Frank very likes confirmation red modals. He is shown that
        # something went wrong, and why
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located((By.CLASS_NAME, "alert-danger"))
            )
            expected_text = "Income creation failed :("
            modal_text = tester.browser.find_element(
                By.CLASS_NAME, "alert-heading"
            )
            tester.assertEqual(modal_text.text, expected_text)
        except NoSuchElementException:
            tester.fail("Failure popup was not displayed. Fail!")


def edit_an_income(
    tester,
    income_id,
    category_id,
    amount=0,
    date="",
    note="",
    check_denied_permission_popup=False,
    check_successful_creation_popup=False,
    check_failure_popup=False,
):
    # NOTE: we hard code the URI because Django has no idea of React routes
    # (i.e. react internal virtual routes, not API endpoints)
    url = f"/app/incomes/{income_id}"

    # Frank accesses the expense page
    tester.browser.get(f"{tester.live_server_url}{url}")

    if check_denied_permission_popup:
        # Frank very likes the alert red modal: he knows something went wrong, and why
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located((By.CLASS_NAME, "alert-danger"))
            )
            alert_text = tester.browser.find_element(
                By.ID, "invalid-income-marker"
            )
            expected_alert_text = "Sorry, it seems the income you're trying to editing does not exist...or there was an error. Please try again later, or reload the page"
            tester.assertEqual(alert_text.text, expected_alert_text)
        except NoSuchElementException:
            tester.fail("Success popup was not displayed. Fail!")
    else:
        # Category API might a bit slow to come back.
        # Let's make this test less flaky by waiting a bit more
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 3).until(
                EC.presence_of_element_located((By.ID, "category_field"))
            )
        except NoSuchElementException:
            tester.fail(
                "The Income category API did not returned timely, and the income pulldown was not displayed (yet). Fail!"
            )

        # Frank methodically alters each field
        dropdown = Select(tester.browser.find_element(By.ID, "category_field"))
        # The dropdown includes the Category Frank wants to set a the income for
        # Frank chooses that category
        # Note, selenium requires the value to be a string, hence the str() casting
        dropdown.select_by_value(str(category_id))

        amount_inputbox = tester.browser.find_element(By.ID, "amount_field")
        amount_inputbox.clear()
        amount_inputbox.send_keys(str(amount))

        date_inputbox = tester.browser.find_element(By.ID, "date_entity")
        date_inputbox.clear()
        date_inputbox.send_keys(date)

        note_inputbox = tester.browser.find_element(By.ID, "note_field")
        note_inputbox.clear()
        note_inputbox.send_keys(note)
        amount_inputbox.send_keys(Keys.ENTER)

        if check_successful_creation_popup:
            # Frank likes very much confirmation green modals.
            # He expects one to be shown, as he filled in everything correctly
            try:
                WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                    EC.presence_of_element_located(
                        (By.CLASS_NAME, "alert-success")
                    )
                )
                expected_text = "Income updated!"
                modal_text = tester.browser.find_element(
                    By.CLASS_NAME, "alert-heading"
                )
                tester.assertEqual(modal_text.text, expected_text)
            except NoSuchElementException:
                tester.fail("Success popup was not displayed. Fail!")

        if check_failure_popup:
            # Frank very likes confirmation red modals.
            # He is shown that something went wrong, and why
            try:
                WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                    EC.presence_of_element_located(
                        (By.CLASS_NAME, "alert-danger")
                    )
                )
                expected_text = "Income update failed :("
                modal_text = tester.browser.find_element(
                    By.CLASS_NAME, "alert-heading"
                )
                tester.assertEqual(modal_text.text, expected_text)
            except NoSuchElementException:
                tester.fail("Failure popup was not displayed. Fail!")


def find_income_in_date_range(tester, data, ranges, expected_to_fail=False):
    # NOTE: we hard code the URI because Django has no idea of React routes
    # (i.e. react internal virtual routes, not API endpoints)
    url = "/app/incomes"

    # Frank visits the incomes page
    tester.browser.get(f"{tester.live_server_url}{url}")

    _select_data_range(tester, ranges["start"], ranges["end"])

    return _check_income_existence(tester, data, expected_to_fail)


def _check_income_existence(tester, data, expected_to_fail):
    income_id = -1

    try:
        table = WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
            EC.presence_of_element_located((By.ID, "incomes-table-body"))
        )

        found = False
        for _, e in enumerate(table.find_elements(By.TAG_NAME, "tr")):
            if found:
                break
            tds = e.find_elements(By.TAG_NAME, "td")
            if len(tds) != 7:
                tester.fail(
                    "Each row in the income table should have 7 columns"
                )

            # If we're checking by income_id, we do a direct comparison
            # with the first column, and we're done
            if "income_id" in data:
                if tds[0].text == data["id"]:
                    found = True
                    income_id = data["id"]
                    break
            else:
                if (
                    tds[1].text == data["category_text"]
                    # JPY do not have decimals, and use thousands separators
                    and tds[2].text == f"￥{data['amount']:,}"
                    and tds[3].text == data["note"]
                    and tds[4].text == data["date"]
                ):
                    found = True
                    income_id = int(tds[0].text)
                    break

        if expected_to_fail and found:
            tester.fail(
                f"Income with amount {data['amount']} and"
                f" category {data['category_text']}, date {data['date']}"
                " was NOT supposed to be displayed... but it was: fail!"
            )

        if not expected_to_fail and not found:
            tester.fail(
                f"Income with amount {data['amount']}"
                f" and category {data['category_text']}, date {data['date']}"
                " was not found: fail!"
            )

    except TimeoutException:
        if not expected_to_fail:
            tester.fail(
                "Either Incomes API has not completed (yet), or the table of"
                " incomes was not displayed. Fail!"
            )

    return income_id


def visit_a_saving_category(
    tester,
    cat_id,
    text,
    is_archived,
    check_failure_popup=True,
):
    print(
        "visit_a_saving_category could be refactored to a"
        " more generic visit_a_category. Or look into"
        " visit_and_verify_categories",
        flush=True,
    )
    # NOTE: we hard code the URI because Django has no idea of React routes
    # (i.e. react internal virtual routes, not API endpoints)
    url = f"/app/saving-category/{cat_id}"

    # Frank accesses the expense page
    tester.browser.get(f"{tester.live_server_url}{url}")

    if check_failure_popup:
        # Frank very likes the alert red modal: he knows something went
        # wrong, and why
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 5).until(
                EC.presence_of_element_located((By.CLASS_NAME, "alert-danger"))
            )
            alert_text = tester.browser.find_element(
                By.ID, "invalid-category-marker"
            )
            expected_alert_text = (
                "Sorry, it seems the Saving Category you're trying"
                " to edit does not exist...or there was"
                " an error. Please try again later, or"
                " reload the page"
            )
            tester.assertEqual(alert_text.text, expected_alert_text)
        except NoSuchElementException:
            tester.fail("Failure popup was not displayed. Fail!")
    else:
        # Frank methodically checks each field: they should all match!
        try:
            checkbox = WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located((By.ID, "text_field"))
            )
            checkbox = tester.browser.find_element(By.ID, "is_archived_field")
            check_value = checkbox.is_selected()
            tester.assertEqual(check_value, is_archived)

            note_inputbox = tester.browser.find_element(By.ID, "text_field")
            prefilled_note = note_inputbox.get_attribute("value")
            tester.assertEqual(prefilled_note, text)
        except NoSuchElementException:
            tester.fail("Success popup was not displayed. Fail!")


def visit_an_income(
    tester,
    income_id,
    category_name,
    category_id,
    amount=0,
    date="",
    note="",
    check_failure_popup=True,
):
    # NOTE: we hard code the URI because Django has no idea of React routes
    # (i.e. react internal virtual routes, not API endpoints)
    url = f"/app/incomes/{income_id}"

    # Frank accesses the expense page
    tester.browser.get(f"{tester.live_server_url}{url}")

    if check_failure_popup:
        # Frank very likes the alert red modal: he knows something went
        # wrong, and why
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located((By.CLASS_NAME, "alert-danger"))
            )
            alert_text = tester.browser.find_element(
                By.ID, "invalid-income-marker"
            )
            expected_alert_text = (
                "Sorry, it seems the income you're trying"
                " to editing does not exist...or there was"
                " an error. Please try again later, or"
                " reload the page"
            )
            tester.assertEqual(alert_text.text, expected_alert_text)
        except NoSuchElementException:
            tester.fail("Failure popup was not displayed. Fail!")
    else:
        # Income category API might take a while to return
        # and provide the data to populate the pulldown.
        # Let's make this test less flaky by allowing the UI some time to
        # properly render
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 3).until(
                EC.presence_of_element_located((By.ID, "category_field"))
            )
            # Frank methodically checks each field: they should all match!
            dropdown = Select(
                tester.browser.find_element(By.ID, "category_field")
            )
            selected_option = dropdown.first_selected_option
            selected_value = selected_option.get_attribute("value")
            tester.assertEqual(selected_option.text, category_name)
            tester.assertEqual(int(selected_value), category_id)

            amount_inputbox = tester.browser.find_element(
                By.ID, "amount_field"
            )
            prefilled_amount = amount_inputbox.get_attribute("value")
            tester.assertEqual(int(prefilled_amount), amount)

            date_inputbox = tester.browser.find_element(By.ID, "date_entity")
            prefilled_date = date_inputbox.get_attribute("value")
            tester.assertEqual(prefilled_date, date)

            note_inputbox = tester.browser.find_element(By.ID, "note_field")
            prefilled_note = note_inputbox.get_attribute("value")
            tester.assertEqual(prefilled_note, note)
        except NoSuchElementException:
            tester.fail(
                "The income category API did not load (yet?), "
                "hence the UI was not properly rendered. Fail!"
            )


def delete_an_income(
    tester,
    income_id,
    income_date,
    check_successful_deletion_popup=False,
    check_failed_deletion_popup=False,
):
    # NOTE: we hard code the URI because Django has no idea of React routes
    # (i.e. react internal virtual routes, not API endpoints)
    url = "/app/incomes"
    print(
        "REFACTOR ME: delete_an_income could be refactored to a more generic delete_an_object",
        flush=True,
    )

    # Frank creates a category
    tester.browser.get(f"{tester.live_server_url}{url}")
    _select_data_range(tester, income_date, income_date)

    # The UI needs to wait for all the API calls to return before rendering
    # correcly: let's give the test some slack
    try:
        WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
            EC.presence_of_element_located((By.ID, "incomes-table-body"))
        )
    except NoSuchElementException:
        tester.fail("Income table body was not displayed. Fail!")
    except TimeoutException:
        tester.fail("Income table body was not displayed (in time?). Fail!")

    delete_button = tester.browser.find_element(
        By.ID, f"deleteIncomeButton-{income_id}"
    )
    delete_button.click()

    # Frank very likes that the app asks him to confirm before actually
    # deleting the expense
    try:
        WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
            EC.presence_of_element_located((By.ID, "confirmDeletion"))
        )
        confirm_deletion = tester.browser.find_element(
            By.ID, "confirmDeletion"
        )
        confirm_deletion.click()
    except NoSuchElementException:
        tester.fail("Delete modal was not displayed. Fail!")
    except TimeoutException:
        tester.fail("Delete modal was not displayed (in time?). Fail!")

    if check_successful_deletion_popup:
        # Frank very likes confirmation green modals. He expects one to be
        # shown as he all the rights to delete his own incomes
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located(
                    (By.CLASS_NAME, "alert-success")
                )
            )
            expected_text = "Income deleted successfully!"
            modal_text = tester.browser.find_element(
                By.CLASS_NAME, "alert-heading"
            )
            tester.assertEqual(modal_text.text, expected_text)
        except NoSuchElementException:
            tester.fail("Success popup was not displayed. Fail!")

    if check_failed_deletion_popup:
        # Frank very likes confirmation red modals.
        # He expects one to be shown when something is not OK!
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located((By.CLASS_NAME, "alert-danger"))
            )
            expected_text = "Income deletion failed :("
            modal_text = tester.browser.find_element(
                By.CLASS_NAME, "alert-heading"
            )
            tester.assertEqual(modal_text.text, expected_text)
        except NoSuchElementException:
            tester.fail("Failure popup was not displayed. Fail!")


def create_spending_limit(
    tester,
    category_id,
    amount=Decimal(0),
    days=0,
    currency="",
    text="",
    check_creation_popup=False,
    check_failure_popup=False,
):
    """
    Create a spending limit for a category
    """
    # NOTE: we hard code the URI because Django has no idea of React routes
    # (i.e. react internal virtual routes, not API endpoints)
    url = "/app/spending-limits/creation"

    # Frank creates a category
    tester.browser.get(f"{tester.live_server_url}{url}")

    create_a_spending_limit_helper(
        tester,
        category_id,
        amount,
        days,
        currency,
        text,
        check_creation_popup,
        check_failure_popup,
    )


def create_a_spending_limit_helper(
    tester,
    category_id,
    amount=Decimal(0),
    days=0,
    currency="",
    text="",
    check_creation_popup=False,
    check_failure_popup=False,
):
    """
    Fill in the spending limit creation form
    """
    # The category API might take some time to get back
    # let's make this test less flaky by allowing it some slack
    try:
        WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 90).until(
            EC.presence_of_element_located((By.ID, "category_field"))
        )
    except NoSuchElementException:
        tester.fail("Success popup was not displayed. Fail!")
    except TimeoutException:
        tester.fail("Success popup was not displayed (in time). Fail!")

    dropdown = Select(tester.browser.find_element(By.ID, "category_field"))
    # The dropdown includes the Category Frank wants to set the spending limit for
    # Frank chooses that category
    # Note: selenium requires the value to be a string, hence the str() casting
    dropdown.select_by_value(str(category_id))

    amount_inputbox = tester.browser.find_element(By.ID, "amount_field")
    amount_inputbox.send_keys(str(amount))

    days_inputbox = tester.browser.find_element(By.ID, "days_field")
    days_inputbox.send_keys(days)

    currency_inputbox = tester.browser.find_element(By.ID, "currency_field")
    currency_inputbox.send_keys(currency)

    text_inputbox = tester.browser.find_element(By.ID, "text_field")
    text_inputbox.send_keys(text)

    amount_inputbox.send_keys(Keys.ENTER)

    if check_creation_popup:
        # Frank likes very much confirmation green modals.
        # He expects one to be shown, as he filled in everything correctly
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 3).until(
                EC.presence_of_element_located(
                    (By.CLASS_NAME, "alert-success")
                )
            )
            expected_text = "Spending Limit created!"
            modal_text = tester.browser.find_element(
                By.CLASS_NAME, "alert-heading"
            )
            tester.assertEqual(modal_text.text, expected_text)
        except NoSuchElementException:
            tester.fail("Success popup was not displayed. Fail!")

    if check_failure_popup:
        # Frank very likes confirmation red modals. He is shown that
        # something went wrong, and why
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 3).until(
                EC.presence_of_element_located((By.CLASS_NAME, "alert-danger"))
            )
            expected_text = "Spending Limit creation failed :("
            modal_text = tester.browser.find_element(
                By.CLASS_NAME, "alert-heading"
            )
            tester.assertEqual(modal_text.text, expected_text)
        except NoSuchElementException:
            tester.fail("Failure popup was not displayed. Fail!")


# Old app ?
def get_category_id_from_category_name(
    tester, category_name, is_income=False, is_balance=False
):
    """
    Find the categories id for a given the text

    This does not work if the said category is not in the first page"""
    if is_income:
        url = reverse("budgets:income_categories")
    elif is_balance:
        url = reverse("budgets:monthly_balance_categories")
    else:
        url = reverse("budgets:categories")
    tester.browser.get(f"{tester.live_server_url}{url}")

    table = tester.browser.find_element(By.ID, "id_categories")

    rows = table.find_elements(By.TAG_NAME, "tr")
    for row in rows:
        if category_name in row.text:
            # Expected format for row text "'<id> <category_name>'"
            return int(row.text.replace(category_name, "").strip())
    return None


def create_a_budget(
    tester,
    cat_type,
    cat_id,
    amount,
    allocated_amount,
    year,
    month=None,
    check_creation_popup=False,
    check_failure_popup=False,
):
    # NOTE: we hard code the URI because Django has no idea of React
    # routes (i.e. react internal virtual routes, not API endpoints)
    url = "/app/budgets/creation"
    field_id = "category_field"
    if cat_type == "saving":
        url = "/app/budgets/saving/creation"
        field_id = "savingcategory_field"

    # Frank creates a category
    tester.browser.get(f"{tester.live_server_url}{url}")

    # The category API might take some time to get back
    # let's make this test less flaky by allowing it some slack
    try:
        WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 2).until(
            EC.presence_of_element_located((By.ID, field_id))
        )
    except NoSuchElementException:
        tester.fail(
            f"Field with ID:{field_id} was not displayed in" " time. Fail!"
        )

    _helper_create_a_budget(
        tester,
        cat_type=cat_type,
        cat_id=cat_id,
        amount=amount,
        allocated_amount=allocated_amount,
        year=year,
        month=month,
        check_creation_popup=check_creation_popup,
        check_failure_popup=check_failure_popup,
    )


def _helper_create_a_budget(
    tester,
    cat_type,
    cat_id,
    amount,
    allocated_amount,
    year,
    month=None,
    check_creation_popup=False,
    check_failure_popup=False,
):
    field_id = "category_field"
    if cat_type == "saving":
        field_id = "savingcategory_field"

    dropdown = Select(tester.browser.find_element(By.ID, field_id))
    # The dropdown includes the Category Frank wants to set the budget for
    # Frank chooses that category
    # Note: selenium requires the value to be a string, hence the str() casting
    dropdown.select_by_value(str(cat_id))

    amount_inputbox = tester.browser.find_element(By.ID, "amount_field")
    amount_inputbox.clear()
    amount_inputbox.send_keys(amount)

    if cat_type == "saving":
        try:
            amount_inputbox = WebDriverWait(
                tester.browser, MAX_WAIT_SECONDS * 2
            ).until(
                EC.presence_of_element_located(
                    (By.ID, "allocated_amount_field")
                )
            )
        except NoSuchElementException:
            tester.fail(
                f"Field with ID:{field_id} was not displayed" " in time. Fail!"
            )

        amount_inputbox.clear()
        amount_inputbox.send_keys(allocated_amount)

    # If we create a yearly budget, we do not care about the month we input
    if month is None:
        yyyymmdd = f"{year}-01-01"
        yearly_checkbox = tester.browser.find_element(
            By.NAME, "isYearlyBudget"
        )

        if not yearly_checkbox.is_selected():
            # Toggle the checkbox only if the current state does not match the
            # desired one
            yearly_checkbox.click()

    else:
        yyyymmdd = f"{year}-{month:02d}-01"

    date_inputbox = tester.browser.find_element(By.ID, "date_entity")
    date_inputbox.send_keys(yyyymmdd)
    amount_inputbox.send_keys(Keys.ENTER)

    if check_creation_popup:
        # Frank likes very much confirmation green modals.
        # He expects one to be shown, as he filled in everything correctly
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 5).until(
                EC.presence_of_element_located(
                    (By.CLASS_NAME, "alert-success")
                )
            )
            expected_text = "Budget created!"
            modal_text = tester.browser.find_element(
                By.CLASS_NAME, "alert-success"
            ).find_element(By.CLASS_NAME, "alert-heading")
            tester.assertIn(expected_text, modal_text.text)
        except NoSuchElementException:
            tester.fail("Success popup was not displayed. Fail!")

    if check_failure_popup:
        # Frank very likes confirmation red modals. He is shown that
        # something went wrong, and why
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 3).until(
                EC.presence_of_element_located((By.CLASS_NAME, "alert-danger"))
            )
            failure_banner = tester.browser.find_element(
                By.CLASS_NAME, "alert-danger"
            )
            expected_text = "Budget creation failed :("
            modal_text = tester.browser.find_element(
                By.CLASS_NAME, "alert-danger"
            ).find_element(By.CLASS_NAME, "alert-heading")
            tester.assertIn(expected_text, failure_banner.text)
        except NoSuchElementException:
            tester.fail("Failure popup was not displayed. Fail!")


def create_a_budget_without_changing_page(
    tester,
    cat_type,
    cat_id,
    amount,
    allocated_amount,
    year,
    month=None,
    check_creation_popup=False,
    check_failure_popup=False,
    check_duplicated_error=False,
):
    _helper_create_a_budget(
        tester,
        cat_type=cat_type,
        cat_id=cat_id,
        amount=amount,
        year=year,
        allocated_amount=allocated_amount,
        month=month,
        check_creation_popup=check_creation_popup,
        check_failure_popup=check_failure_popup,
    )

    if check_duplicated_error:
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 3).until(
                EC.presence_of_element_located((By.TAG_NAME, "form"))
            )
            form = tester.browser.find_element(By.TAG_NAME, "form")
            msg = (
                "A Budget for this Category for this year/month (or year)"
                " already exists for this user."
            )
            if cat_type == "saving":
                msg = (
                    "A Budget for this Saving Category for this year/month"
                    " (or year) already exists for this user."
                )
            tester.assertIn(msg, form.text)
        except NoSuchElementException:
            tester.fail("Form was not displayed. Fail!")
        except TimeoutException:
            tester.fail("Form was not displayed in time. Fail!")


def confirm_budget_exists_and_get_its_id(
    tester,
    cat_text,
    cat_type,
    year,
    month,
    amount,
    allocated_amount,
    check_failure_popup,
    expect_missing_budgets=False,
):
    # NOTE: we hard code the URI because Django has no idea of React
    # routes (i.e. react internal virtual routes, not API endpoints)
    url = f"/app/budgets/date/{year}"
    if month is not None:
        url = f"/app/budgets/date/{year}/{month}"

    tester.browser.get(f"{tester.live_server_url}{url}")

    if expect_missing_budgets:
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 3).until(
                EC.presence_of_element_located((By.ID, "budgetsContainer"))
            )
        except NoSuchElementException:
            pass
        except TimeoutException:
            pass

        return -1

    load_error = (
        "No budgets exists for this period, or the page did not"
        " load properly.. or it took too long"
    )
    try:
        WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 3).until(
            EC.presence_of_element_located((By.ID, "budgetsContainer"))
        )
    except NoSuchElementException:
        tester.fail(load_error)
    except TimeoutException:
        tester.fail(load_error)

    # Frank check whether this budget exists or not
    bdgts = tester.browser.find_element(
        By.ID, "budgetsContainer"
    ).find_elements(By.CLASS_NAME, "card-title")
    if len(bdgts) == 0:
        tester.fail("There are no budgets at all for this period!")

    tester.assertTrue(
        any((cat_text in cat.text) for cat in bdgts),
        f"No {cat_text} in rows. Contents were\n{[ cat.text for cat in bdgts if (cat.text)]}",
    )

    link = None
    for bdgt in bdgts:
        a = bdgt.find_element(By.TAG_NAME, "a")
        if cat_text in a.text:
            link = a
            break

    if not link:
        tester.fail("Budget not found.")
        return -1

    url = link.get_attribute("href")
    bdgt_id = url.split("/")[-1]
    # bdgt_details_url = f"/app/budgets/{bdgt_id}"

    link.click()
    # tester.browser.get(f"{tester.live_server_url}{bdgt_details_url}")
    visit_a_budget(
        tester,
        bdgt_id=bdgt_id,
        cat_name=cat_text,
        cat_type=cat_type,
        year=year,
        month=month,
        amount=amount,
        allocated_amount=allocated_amount,
        check_failure_popup=check_failure_popup,
    )

    return bdgt_id


def visit_a_budget(
    tester,
    bdgt_id,
    cat_name,
    cat_type,
    year,
    month,
    amount,
    allocated_amount=None,
    check_failure_popup=True,
):
    # NOTE: we hard code the URI because Django has no idea of React routes
    # (i.e. react internal virtual routes, not API endpoints)
    url = f"/app/budgets/edit/{bdgt_id}"
    if cat_type == "saving":
        url = f"/app/budgets/saving/edit/{bdgt_id}"

    # Frank accesses the budget detail page
    tester.browser.get(f"{tester.live_server_url}{url}")

    if check_failure_popup:
        # Frank very likes the alert red modal: he knows something went
        # wrong, and why
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located((By.CLASS_NAME, "alert-danger"))
            )
            alert_text = tester.browser.find_element(
                By.ID, "invalid-budget-marker"
            )
            expected_alert_text = (
                "Sorry, it seems the Budget you're trying to"
                " editing does not exist...or there was an "
                "error. Please try again later, or reload "
                "the page"
            )
            tester.assertEqual(alert_text.text, expected_alert_text)
        except NoSuchElementException:
            tester.fail("Failure popup was not displayed. Fail!")
        # If we expect the budget not to be displayed, return early here
        return

    # Budget detail API might take a while to return
    # and provide the data to populate the form.
    # Let's make this test less flaky by allowing the UI some time to
    #  properly render
    try:
        WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 3).until(
            EC.presence_of_element_located((By.ID, "amount_field"))
        )
        # Frank methodically checks each field: they should all match!

        # Check the title (h4) text for the category_text year and month
        #  (if not None)
        h3s = tester.browser.find_elements(By.TAG_NAME, "h3")
        # Remove 'Work in progress' alert title from the candidates
        form_title = [i for i in h3s if i.text != "Work in progress"][0]
        expect = (
            f'Editing Spending Budget for category "{cat_name}"'
            f" for the whole {year} year"
        )
        if month is not None:
            expect = (
                f'Editing Spending Budget for category "{cat_name}"'
                f" for the month of {year}-{month}"
            )

        if cat_type == "saving":
            expect = expect.replace(
                "Spending Budget for category", "Budget for Saving category"
            )

        tester.assertEqual(expect, form_title.text)

        amount_inputbox = tester.browser.find_element(By.ID, "amount_field")
        prefilled_amount = amount_inputbox.get_attribute("value")
        tester.assertEqual(int(prefilled_amount), amount)

    except NoSuchElementException:
        msg = (
            "The Budget detail API did not load (yet?), hence the UI"
            "was was not properly rendered. Fail!"
        )
        tester.fail(msg)

    if cat_type == "saving":
        allocated = WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 3).until(
            EC.presence_of_element_located((By.ID, "allocated_amount_field"))
        )
        # Frank methodically checks each field: they should all match!

        prefilled_amount = allocated.get_attribute("value")
        tester.assertEqual(int(prefilled_amount), allocated_amount)


def edit_a_budget(
    tester,
    bdgt_id,
    amount,
    check_edit_success_popup=False,
    check_edit_failure_popup=False,
    check_inability_to_load_budget=False,
):
    # NOTE: we hard code the URI because Django has no idea of React
    # routes (i.e. react internal virtual routes, not API endpoints)
    url = f"/app/budgets/edit/{bdgt_id}"

    tester.browser.get(f"{tester.live_server_url}{url}")

    # If this user does not own the budget, or the budget does not exist yet
    # no data will be loaded, and a special banner will be displayed
    if check_inability_to_load_budget:
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located((By.CLASS_NAME, "alert-danger"))
            )
            failure_banner = tester.browser.find_element(
                By.CLASS_NAME, "alert-danger"
            )
            expected_error = (
                "Sorry, it seems the Budget you're trying to editing does "
                "not exist...or there was an error. Please try again later,"
                " or reload the page"
            )
            tester.assertEqual(failure_banner.text, expected_error)
            return
        except NoSuchElementException:
            tester.fail("Failure popup was not displayed. Fail!")
            return
    # If the budget exist, proceed with the rest of the logic

    # Let's wait for all the APIs to come back
    # and let's give the frontend some time to properly render
    try:
        WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
            EC.presence_of_element_located((By.ID, "amount_field"))
        )
    except NoSuchElementException:
        tester.fail("input field was not displayed. Fail!")

    inputbox = tester.browser.find_element(By.ID, "amount_field")

    # IMPORTANT: we need to clear the input before sending keys, or the input
    # field will end up having the original input value AND the keys we send
    # (this is not a problem ONLY if the input field was originally empty)
    inputbox.clear()
    inputbox.send_keys(str(amount))
    inputbox.send_keys(Keys.ENTER)

    if check_edit_success_popup:
        # Frank likes very much confirmation green modals.
        # He expects one to be shown, as he filled in everything correctly
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located(
                    (By.CLASS_NAME, "alert-success")
                )
            )
            expected_text = "Budget updated!"
            modal_text = tester.browser.find_element(
                By.CLASS_NAME, "alert-success"
            ).find_element(By.CLASS_NAME, "alert-heading")
            tester.assertEqual(modal_text.text, expected_text)
        except NoSuchElementException:
            tester.fail("Success popup was not displayed. Fail!")

    if check_edit_failure_popup:
        # Frank likes very much confirmation green modals.
        # He expects one to be shown, as he filled in everything correctly
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located((By.CLASS_NAME, "alert-danger"))
            )
            expected_text = "Budget update failed :("
            modal_text = tester.browser.find_element(
                By.CLASS_NAME, "alert-danger"
            ).find_element(By.CLASS_NAME, "alert-heading")
            tester.assertEqual(modal_text.text, expected_text)
        except NoSuchElementException:
            tester.fail("Failure popup was not displayed. Fail!")


def create_a_monthly_budget(
    tester, category_name, amount, date, create_check=True
):
    # Frank visits the monthly expenses page
    url = reverse("budgets:monthly_budgets_create")

    tester.browser.get(f"{tester.live_server_url}{url}")

    # Frank sees an input box
    inputbox = tester.browser.find_element(By.ID, "id_amount")
    # Frank inputs the price of the expense item
    inputbox.send_keys(amount)
    # Frank sees a dropdown
    dropdown = Select(tester.browser.find_element(By.ID, "id_category"))
    # The dropdown includes the Category Frank wants to set a budget for
    # Frank chooses that category
    dropdown.select_by_visible_text(category_name)

    # Frank sees another input box
    date_inputbox = tester.browser.find_element(By.ID, "id_date")
    # Frank enters the date for the budget
    date_inputbox.send_keys(date.strftime("%Y-%m-%d"))

    inputbox.send_keys(Keys.ENTER)
    # Frank finds the submit button
    # submit_button = tester.browser.find_element(By.ID,'id_submit')
    # Frank clicks the button to save the entry
    # submit_button.click()

    if create_check:
        formatted_amount = f"{amount:.2f}"
        wait_for_page_to_reload(tester)
        verify_monthly_budget_was_created(
            tester, category_name, formatted_amount, date
        )


def get_monthly_balance_category_id_from_text(tester, text):
    # NOTE: we hard code the URI because Django has no idea of React routes
    # (i.e. react internal virtual routes, not API endpoints)
    url = "/app/monthly-balances/categories"

    # Frank visits the goals page
    tester.browser.get(f"{tester.live_server_url}{url}")

    try:
        WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
            EC.presence_of_element_located((By.TAG_NAME, "ul"))
        )

    except NoSuchElementException:
        msg = (
            "No Monthly Balance Categories exists, or the page did not load"
            " properly.. or it took too long"
        )
        tester.fail(msg)

    # Frank check whether this goal exists or not
    cat_li = tester.browser.find_element(By.TAG_NAME, "ul").find_elements(
        By.TAG_NAME, "li"
    )
    if len(cat_li) == 0:
        tester.fail("There are no categories at all!")

    tester.assertTrue(
        any((text in cat.text) for cat in cat_li),
        f"No {text} in rows. Contents were\n{[ cat.text for cat in cat_li if (cat.text)]}",
    )

    # NOTE: Regex expression to grab the cat ID if from the text
    # In the react view, the format is
    # let isArchived = cat.is_archived ? '- Archived' : null
    # "(ID: {cat.id}) {cat.text}: edit"
    try:
        # FIXME: re-write this horror using a simple for loop, for better readability
        return [
            int(re.search(r"ID:\s(\d*)\)", cat.text)[1])
            for cat in cat_li
            if (text in cat.text)
        ][0]
    except TimeoutException as e:
        print(e, flush=True)
        return -1
    except IndexError as e:
        print(e, flush=True)
        return -1


# TODO: is_income is making the logic complex, refactor
def create_entry(
    tester,
    amount,
    category_name,
    note,
    expense_date,
    is_income=False,
    verify_creation=True,
    js_to_execute=None,
):
    # Frank visits the expenses page
    url = reverse("budgets:expenses_create")
    if is_income:
        url = reverse("budgets:incomes_create")

    tester.browser.get(f"{tester.live_server_url}{url}")

    if js_to_execute:
        # Franks manually edits the form option values (Frank know some js)
        tester.browser.execute_script(js_to_execute)

    # Frank sees an input box
    inputbox = tester.browser.find_element(By.ID, "id_amount")
    # Frank inputs the price of the expense item
    inputbox.send_keys(amount)
    # Frank sees a dropdown
    dropdown = Select(tester.browser.find_element(By.ID, "id_category"))
    # The dropdown includes the Category they've just created
    # Frank chooses that category
    dropdown.select_by_visible_text(category_name)

    # Frank sees another input box
    note_inputbox = tester.browser.find_element(By.ID, "id_note")
    # Frank enters a note abotu the expenses, so that later they remember what
    # this was about
    note_inputbox.send_keys(note)

    # Frank sees one more input box
    date_inputbox = tester.browser.find_element(By.ID, "id_date")
    # Frank enters the date of when the expense was made
    date_inputbox.send_keys(expense_date)

    # Frank see a submit button
    submit_button = tester.browser.find_element(By.ID, "id_submit")
    # Frank clicks the button to save the entry
    submit_button.click()

    if verify_creation:
        formatted_amount = f"{amount:.2f}"

        to_be_checked_url = reverse("budgets:expenses")
        tester.browser.get(f"{tester.live_server_url}{to_be_checked_url}")
        # Frank can see the information the correct information on the page
        verify_expense_was_created(
            tester, formatted_amount, category_name, note
        )

    # The page reload and the expense entered item is displayed correctly
    # TODO code smell, this wait_for_page_to_reload() should be needed
    # wait_for_page_to_reload(tester)


def create_category_and_two_expenses(
    tester, first_item, second_item, category_name, is_income=False
):
    create_a_category(tester, category_name, is_income)

    first_date = date.today().replace(day=1)
    delta = timedelta(weeks=10)
    second_date = first_date - delta
    method = create_entry

    # Frank visits the expenses url and enters the expense details
    method(
        tester,
        first_item[0],
        category_name,
        first_item[1],
        first_date.strftime("%Y-%m-%d"),
        is_income,
    )

    # Frank visits the expenses url again and enters a second expense with its
    # details
    method(
        tester,
        second_item[0],
        category_name,
        second_item[1],
        second_date.strftime("%Y-%m-%d"),
        is_income,
        # Note False, since this expense is in the past, it should not appear
        False,
    )


# FIX ME: refactor me, two flags (is_income, is_balance) are a bad code smell
# use a 'type' instead
def visit_and_verify_categories(
    tester, category_name, is_income=False, should_exist=True, is_balance=False
):
    print("Deprecated function, please use check_category_existence instead")
    if is_income:
        url = reverse("budgets:income_categories")
    elif is_balance:
        url = reverse("budgets:monthly_balance_categories")
    else:
        url = reverse("budgets:categories")
    tester.browser.get(f"{tester.live_server_url}{url}")

    table = tester.browser.find_element(By.ID, "id_categories")

    if should_exist:
        find_text_inside_table(tester, category_name, table)
    else:
        assert_text_is_not_inside_table(tester, category_name, table)


# NOTE: deprecated
def verify_category_was_created(tester, category_name):
    # Frank sees the category name is present on the page
    table = tester.browser.find_element(By.ID, "id_categories")
    find_text_inside_table(tester, category_name, table)


# NOTE: to be deprecated
def verify_expense_was_created(tester, amount, category_name, note):
    # Frank sees all the details about the expense displayed on the page
    table = tester.browser.find_element(By.ID, "id_expenses")
    find_text_inside_table(tester, str(amount), table)
    find_text_inside_table(tester, category_name, table)
    find_text_inside_table(tester, note, table)
    # TODO: check for the date as well. Beware that different browsers
    # might use different date formats!!
    # find_text_inside_table('2019-08-04', table)


def visit_and_verify_expense(
    tester, amount, category_name, note, should_exist
):  # FIX ME: what should be should_exist default value?
    formatted_amount = f"{amount:.2f}"
    to_be_checked_url = reverse("budgets:expenses")
    tester.browser.get(f"{tester.live_server_url}{to_be_checked_url}")

    if should_exist:
        verify_expense_was_created(
            tester, formatted_amount, category_name, note
        )
    else:
        table = tester.browser.find_element(By.ID, "id_expenses")
        assert_text_is_not_inside_table(tester, category_name, table)


def check_guidance(self, h5_text, a_text, a_href):
    try:
        WebDriverWait(self.browser, 4).until(
            EC.presence_of_element_located(
                (By.XPATH, f'//h5[contains(text(),"{h5_text}")]')
            )
        )
    except NoSuchElementException:
        msg = (
            "The correct guidance was not shown. We have a regression:"
            " Fail!"
        )
        self.fail(msg)
    except TimeoutException:
        msg = (
            "The correct guidance was not shown timely. We might have a"
            "regression: Fail!"
        )
        self.fail(msg)

    # Frank find the button leading to the correct page
    try:
        xpath = (
            f'//h5[contains(text(), "{h5_text}")]/following-sibling'
            f'::a[contains(text(), "{a_text}") and @href="{a_href}"]'
        )

        WebDriverWait(self.browser, 4).until(
            EC.presence_of_element_located((By.XPATH, xpath))
        )
    except NoSuchElementException:
        msg = (
            "The correct guidance was not shown. We have a regression:"
            " Fail!"
        )
        self.fail(msg)
    except TimeoutException:
        msg = (
            "The correct guidance was not shown timely. We might have a"
            "regression: Fail!"
        )
        self.fail(msg)


def check_missing_guidance(self, cat_text, data_text):
    try:
        WebDriverWait(self.browser, 4).until(
            EC.presence_of_element_located(
                (By.XPATH, f'//h5[contains(text(),"{cat_text}")]')
            )
        )
        WebDriverWait(self.browser, 2).until(
            EC.presence_of_element_located(
                (By.XPATH, f'//h5[contains(text(),"{data_text}")]')
            )
        )
        self.fail("Guidance was present")
    except TimeoutException:
        # The element must be absent, this is good
        pass


def create_monthly_balance_category(
    tester,
    category_name,
    check_creation_popup=False,
    check_failure_popup=False,
):
    # NOTE: we hard code the URI because Django has no idea of React routes
    # (i.e. react internal virtual routes, not API endpoints)
    url = "/app/monthly-balances/categories/creation"

    # Frank creates a category
    tester.browser.get(f"{tester.live_server_url}{url}")

    # Make this test less flaky by allowing some time for the UI to fully load
    try:
        WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 5).until(
            EC.presence_of_element_located((By.ID, "text_field"))
        )
    except NoSuchElementException:
        msg = (
            "Monthly Balance Category text input field was not"
            " displayed: Fail!"
        )
        tester.fail(msg)

    inputbox = tester.browser.find_element(By.ID, "text_field")
    placeholder_text = "Enter the monthly balance category name"

    tester.assertEqual(inputbox.get_attribute("placeholder"), placeholder_text)

    inputbox.send_keys(category_name)
    inputbox.send_keys(Keys.ENTER)

    if check_creation_popup:
        # Frank likes very much confirmation green modals
        # He expects one to be shown, as he filled in everything correctly
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located(
                    (By.CLASS_NAME, "alert-success")
                )
            )
        except NoSuchElementException:
            tester.fail("Success popup was not displayed. Fail!")

    if check_failure_popup:
        # Frank likes very much confirmation green modals. He expects one to
        # be shown as he filled in everything correctly
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located((By.CLASS_NAME, "alert-danger"))
            )
        except NoSuchElementException:
            tester.fail("Failure popup was not displayed. Fail!")


def create_monthly_balance_category(
    tester,
    category_name,
    check_creation_popup=False,
    check_failure_popup=False,
):
    # NOTE: we hard code the URI because Django has no idea of React routes
    # (i.e. react internal virtual routes, not API endpoints)
    url = "/app/monthly-balances/categories/creation"

    # Frank creates a category
    tester.browser.get(f"{tester.live_server_url}{url}")

    # Make this test less flaky by allowing some time for the UI to fully load
    try:
        WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
            EC.presence_of_element_located((By.ID, "text_field"))
        )
    except NoSuchElementException:
        tester.fail(
            "Monthly Balance Category text input field was not displayed: Fail!"
        )

    inputbox = tester.browser.find_element(By.ID, "text_field")
    placeholder_text = "Enter the monthly balance category name"

    tester.assertEqual(inputbox.get_attribute("placeholder"), placeholder_text)

    inputbox.send_keys(category_name)
    inputbox.send_keys(Keys.ENTER)

    if check_creation_popup:
        # Frank likes very much confirmation green modals
        # He expects one to be shown, as he filled in everything correctly
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located(
                    (By.CLASS_NAME, "alert-success")
                )
            )
        except NoSuchElementException:
            tester.fail("Success popup was not displayed. Fail!")

    if check_failure_popup:
        # Frank likes very much confirmation green modals. He expects one to
        # be shown as he filled in everything correctly
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located((By.CLASS_NAME, "alert-danger"))
            )
        except NoSuchElementException:
            tester.fail("Failure popup was not displayed. Fail!")


def create_a_monthly_balance(
    tester, category_id, amount, date, create_check=True
):
    # Frank visits the monthly balance creation page

    # NOTE: we hard code the URI because Django has no idea of React
    # routes (i.e. react internal virtual routes, not API endpoints)
    url = f"/app/monthly-balances/multiple-creation/date/{date}"
    tester.browser.get(f"{tester.live_server_url}{url}")

    # Frank sees an input box
    input_id = f"text_field_{category_id}"
    try:
        WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
            EC.presence_of_element_located((By.ID, input_id))
        )
    except NoSuchElementException:
        tester.fail("Success popup was not displayed. Fail!")
    inputbox = tester.browser.find_element(By.ID, input_id)
    # Frank inputs the price of the expense item
    inputbox.clear()
    inputbox.send_keys(amount)

    # Frank press the enter button to submit the form
    inputbox.send_keys(Keys.ENTER)

    if create_check:
        # breakpoint()
        success_id = f"text_field_{category_id}_success"
        # Frank likes very much confirmation green modals
        # He expects one to be shown, as he filled in everything correctly
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located((By.ID, success_id))
            )
        except NoSuchElementException:
            tester.fail("Success popup was not displayed. Fail!")


# NOTE: to be deprecated
def verify_monthly_budget_was_created(
    self, category_name, amount, date, should_exist=True
):
    # Frank sees all the details about the monghtly budget displayed on the
    # page
    table = self.browser.find_element(By.ID, "id_monthly_budgets")
    formatted_amount = str(amount)
    if should_exist:
        find_text_inside_table(self, formatted_amount, table)
        find_text_inside_table(self, category_name, table)
    else:
        assert_text_is_not_inside_table(self, formatted_amount, table)
        assert_text_is_not_inside_table(self, category_name, table)


def visit_and_verify_month_budget_creation(*args, **kwargs):
    url = reverse("budgets:monthly_budgets")
    self = kwargs.get("self")
    self.browser.get(f"{self.live_server_url}{url}")

    verify_monthly_budget_was_created(*args, **kwargs)


def check_whether_current_month_date_is_displayed(tester):
    """
    Current page should display the current date in '%Y-%m' format
    """
    today_string = date.today().strftime("%Y-%m")
    date_container = tester.browser.find_element(
        By.ID, "id_current_month_date"
    )
    tester.assertIn(today_string, date_container.text)


def check_current_month(tester, current_month_amount, category_name):
    """
    Current page should show only expenses from the current month when
    acccessed without paramters.
    """
    table = tester.browser.find_element(By.ID, "id_expenses_total")
    find_text_inside_table(tester, str(current_month_amount), table)
    # TODO add two expense in different months (current, past) and confirm
    # only the expense for the current month is displayed


def create_a_goal(
    tester,
    is_archived=False,
    amount=0,
    text="",
    note="",
    check_creation_popup=True,
    check_successful_creation=True,
):
    """
    Create a goal using the UI
    Returns:
     - the created Goal id if check_successful_creation is True
     - Otherwise -1
    """
    # NOTE: we hard code the URI because Django has no idea of React
    # routes (i.e. react internal virtual routes, not API endpoints)
    url = "/app/goals/creation"

    # Frank creates a goal
    tester.browser.get(f"{tester.live_server_url}{url}")

    try:
        WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
            EC.presence_of_element_located((By.ID, "text_field"))
        )
    except NoSuchElementException:
        tester.fail("Goal creation UI did not load. Fail!")
    except TimeoutException:
        tester.fail("Goal creation UI was not displayed (in time?). Fail!")

    text_inputbox = tester.browser.find_element(By.ID, "text_field")
    amount_inputbox = tester.browser.find_element(By.ID, "amount_field")
    note_inputbox = tester.browser.find_element(By.ID, "note_field")
    is_archived_goal_checkbox = tester.browser.find_element(
        By.ID, "is_archived_field"
    )
    # NOTE: as soon as we have a second button, this will fail hard
    submit_button = tester.browser.find_element(By.ID, "CreateButton")

    amount_inputbox.send_keys(amount)
    text_inputbox.send_keys(text)
    note_inputbox.send_keys(note)

    if is_archived:
        is_archived_goal_checkbox.click()

    submit_button.click()

    if check_creation_popup:
        # Frank likes very much confirmation green modals.
        # He expects one to be shown, as he filled in everything correctly
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located(
                    (By.CLASS_NAME, "alert-success")
                )
            )
        except NoSuchElementException:
            tester.fail("Success popup was not displayed. Fail!")
        except TimeoutException:
            tester.fail("Success popup was not displayed (in time?). Fail!")

    # Check if the categoy page shows the created goal
    # and return the goal id we get from visit_and_verify_goal, or return -1
    data = {
        "text": text,
        "amount": amount,
        "note": note,
        "is_archived": is_archived,
    }
    if check_successful_creation:
        return visit_and_verify_goal(
            tester,
            data=data,
            expected_to_fail=False,
        )
    return -1


def create_a_goal_and_return_all_its_data(
    self, check_creation_popup=True, check_successful_creation=True
):
    is_archived = random.choice([True, False])
    amount = random.randint(1, 90000)
    text = generate_string()
    note = generate_string()

    goal_id = create_a_goal(
        self,
        is_archived=is_archived,
        amount=amount,
        text=text,
        note=note,
        check_creation_popup=check_creation_popup,
        check_successful_creation=check_successful_creation,
    )

    return {
        "id": goal_id,
        "is_archived": is_archived,
        "amount": amount,
        "text": text,
        "note": note,
    }


def edit_a_goal(
    tester,
    old_goal_obj,
    goal_id,
    is_archived=False,
    amount=0,
    text="",
    note="",
    check_successful_edit_popup=True,
    check_successful_edit=True,
):
    url = f"/app/goals/{goal_id}"

    # Frank visits the goals page
    tester.browser.get(f"{tester.live_server_url}{url}")

    # Let's wait for all the APIs to come back
    # and let's give the frontend some time to properly render
    try:
        WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
            EC.presence_of_element_located((By.ID, "text_field"))
        )
    except NoSuchElementException:
        tester.fail("The UI did not load timely")
    except TimeoutException:
        tester.fail("The UI did not load timely")

    # Frank updates the value of the fields
    text_inputbox = tester.browser.find_element(By.ID, "text_field")
    amount_inputbox = tester.browser.find_element(By.ID, "amount_field")
    note_inputbox = tester.browser.find_element(By.ID, "note_field")
    is_archived_goal_checkbox = tester.browser.find_element(
        By.ID, "is_archived_field"
    )
    # NOTE: as soon as we have a second button, this will fail hard
    submit_button = tester.browser.find_element(By.TAG_NAME, "button")

    # Frank sees the goal field being the same as what he was expecting
    # them to be
    tester.assertEqual(
        text_inputbox.get_property("value"), old_goal_obj["text"]
    )
    tester.assertEqual(
        int(amount_inputbox.get_property("value")), old_goal_obj["amount"]
    )
    tester.assertEqual(
        note_inputbox.get_property("value"), old_goal_obj["note"]
    )
    tester.assertEqual(
        is_archived_goal_checkbox.is_selected(), old_goal_obj["is_archived"]
    )

    # Frank updates the value of the fields
    # Note: Frank knows that he has to clean the field with Control+A before
    # inserting the new value
    amount_inputbox.send_keys(Keys.CONTROL, "a")
    amount_inputbox.send_keys(amount)
    text_inputbox.send_keys(Keys.CONTROL, "a")
    text_inputbox.send_keys(text)
    note_inputbox.send_keys(Keys.CONTROL, "a")
    note_inputbox.send_keys(note)

    if (is_archived_goal_checkbox.is_selected() and not is_archived) or (
        not is_archived_goal_checkbox.is_selected() and is_archived
    ):
        # Toggle the checkbox only if the current state does NOT match
        # the desired one
        is_archived_goal_checkbox.click()

    # Submit the form
    submit_button.click()

    if check_successful_edit_popup:
        # Frank likes very much confirmation green modals.
        # He expects one to be shown, as he filled in everything correctly
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located(
                    (By.CLASS_NAME, "alert-success")
                )
            )
        except NoSuchElementException:
            tester.fail("Success popup was not displayed. Fail!")
        except TimeoutException:
            tester.fail("Success popup was not displayed (in time?). Fail!")

    if check_successful_edit:
        data = {
            "text": text,
            "amount": amount,
            "note": note,
            "is_archived": is_archived,
        }
        # Frank also goes and sees if the goal list pages has the new values
        return visit_and_verify_goal(
            tester,
            data=data,
            expected_to_fail=False,
        )


def access_goal_details(tester, goal_id):  # Refactor me out to complete #46
    """
    DEPRECATED: This method makes the tests flaky, do not use anymore

    Execute a small js script inside the browser to retrieve the goal details
    from an API endpoint
    """
    print(
        "DEPRECATED: access_goal_details method makes the tests flaky,"
        " do not use anymore"
    )
    # NOTE: we hard code the URI because Django has no idea of React routes
    # (i.e. react internal virtual routes, not API endpoints)
    url = f"/app/goals/{goal_id}/"
    token = [
        cookie
        for cookie in tester.browser.get_cookies()
        if cookie["name"] == "csrftoken"
    ][0]["value"]
    complete_script = (
        f"return fetch('/api/v1/goals/{goal_id}').then(res=>res.json())"
    )
    # FIXME: shouldn't we need to add X-CSRFToken header with the above
    # extracted token?
    return tester.browser.execute_script(complete_script)


def visit_and_verify_goal(tester, data, expected_to_fail=False):
    """
    Visit the goals page and verify the goal was created
    Returns the goal id if the goal was found, -1 otherwise
    """
    # NOTE: we hard code the URI because Django has no idea of React routes
    # (i.e. react internal virtual routes, not API endpoints)
    url = "/app/goals"

    # Frank visits the goals page
    tester.browser.get(f"{tester.live_server_url}{url}")
    goal_id = -1

    try:
        table = WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
            EC.presence_of_element_located((By.ID, "goals-table-body"))
        )

        found = False
        for _, e in enumerate(table.find_elements(By.TAG_NAME, "tr")):
            if found:
                break
            tds = e.find_elements(By.TAG_NAME, "td")
            if len(tds) != 7:
                tester.fail("Each row in the Goal table should have 7 columns")

            # If we're checking by goal_id, we do a direct comparison
            # with the first column, and we're done
            is_archived_text = "Yes" if data["is_archived"] else "No"
            if "goal_id" in data:
                if tds[0].text == data["id"]:
                    found = True
                    print("INFO: Shouldn't we return an int here?", flush=True)
                    print(
                        "INFO: Or maybe we're passing a string in data['id'] ?",
                        flush=True,
                    )
                    goal_id = data["id"]
                    # If we found the goal, but it was not supposed to be
                    # missing, we need to fail
                    if expected_to_fail:
                        tester.fail(
                            f"Goal with id {goal_id} was found, but it"
                            " was not supposed to be there... fail!"
                        )
                    return goal_id
            else:
                if (
                    tds[1].text == data["text"]
                    and tds[2].text == f"￥{data['amount']:,}"
                    and tds[3].text == data["note"]
                    and tds[4].text == is_archived_text
                ):
                    found = True
                    # If we found the goal, but it was not supposed to be
                    # missing, we need to fail
                    goal_id = tds[0].text
                    if expected_to_fail:
                        tester.fail(
                            f"Goal with id {goal_id} was found, but it"
                            " was not supposed to be there... fail!"
                        )
                    return goal_id

        # TODO: one of these two blocks is likely unnecessary, refactor
        if expected_to_fail and found:
            tester.fail(
                f"Goal with amount ￥{data['amount']:,}, text {data['text']}"
                f", note {data['note']} was NOT supposed to be displayed"
                "... but it was: fail!"
            )

        if not expected_to_fail and not found:
            tester.fail(
                f"Goal with amount ￥{data['amount']:,}"
                f",text {data['text']}, note {data['note']}"
                " was not found: fail!"
            )

    except TimeoutException:
        if not expected_to_fail:
            tester.fail(
                "Either Goals API has not completed (yet), or the table with"
                " Goals was not displayed. Fail!"
            )
    except NoSuchElementException:
        if not expected_to_fail:
            tester.fail("The goal UI did not load in time. Fail!")

    return goal_id


def delete_goal(tester, goal_id, check_deletion_popup):
    # NOTE: we hard code the URI because Django has no idea of React
    # routes (i.e. react internal virtual routes, not API endpoints)
    url = "/app/goals"

    # Frank visits the goals page
    tester.browser.get(f"{tester.live_server_url}{url}")

    # Since the delete buttons is shown after the goal list API has returned
    # let's give the frontend sometime to call said API
    # and render the view accordingly
    table = None
    try:
        table = WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
            EC.presence_of_element_located((By.ID, "goals-table-body"))
        )

        for _, e in enumerate(table.find_elements(By.TAG_NAME, "tr")):
            tds = e.find_elements(By.TAG_NAME, "td")
            if len(tds) != 7:
                tester.fail("Each row in the Goal table should have 7 columns")

            if tds[0].text == goal_id:
                delete_button = tds[6].find_element(By.TAG_NAME, "button")
                # Frank clik the delete button for the goal he wants to delete
                delete_button.click()

                # Frank is very sure about his choice: he goes ahead and confirms it
                confirm_deletion_button = tester.browser.find_element(
                    By.CLASS_NAME, "modal-footer"
                ).find_elements(By.TAG_NAME, "button")[1]
                confirm_deletion_button.click()

                if check_deletion_popup:
                    # Frank likes very much the green confirmation modals, green is  his
                    # favourite colour! He expects a modal to be shown, as he filled in
                    # everything correctly
                    try:
                        WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                            EC.presence_of_element_located(
                                (By.CLASS_NAME, "alert-success")
                            )
                        )
                    except NoSuchElementException:
                        tester.fail("Success popup was not displayed. Fail!")

    except NoSuchElementException:
        tester.fail("The goal UI did not load in time. Fail!")
    except TimeoutException:
        tester.fail(
            "Either Goals API has not completed (yet), or the table with"
            " Goals was not displayed. Fail!"
        )


def edit_fixed_obligation_to_be_invalid(
    tester,
    id,
    name,
    total_amount,
    obligation_type,
    monthly_payment,
    start_date_str,
    end_date_str,
    payment_day,
    element_path,
    error_message,
):
    edit_a_fixed_obligation(
        tester,
        id,
        name,
        total_amount,
        obligation_type,
        monthly_payment,
        start_date_str,
        end_date_str,
        payment_day,
        check_success_popup=False,
        check_failure_popup=False,
        check_successful_edit=False,
    )
    check_for_validation_error(
        tester,
        element_path,
        error_message,
    )


def create_invalid_fixed_obligation(
    tester,
    name,
    total_amount,
    obligation_type,
    monthly_payment,
    start_date_str,
    end_date_str,
    payment_day,
    element_path,
    error_message,
):
    create_a_fixed_obligation(
        tester,
        name=name,
        total_amount=total_amount,
        obligation_type=obligation_type,
        monthly_payment=monthly_payment,
        start_date=start_date_str,
        end_date=end_date_str,
        payment_day=payment_day,
        check_creation_popup=False,
        check_successful_creation=False,
    )
    check_for_validation_error(
        tester,
        element_path,
        error_message,
    )


def check_for_validation_error(tester, element_path, error_message):
    # The inline form validation is not working properly for the name field
    try:
        WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, element_path))
        )
        assert (
            tester.browser.find_element(By.CSS_SELECTOR, element_path).text
            == error_message
        )
    except TimeoutException:
        tester.fail("The inline form validation was not displayed")


def create_a_fixed_obligation(
    tester,
    name="",
    total_amount=0,
    obligation_type="",
    monthly_payment=0,
    start_date="",
    end_date="",
    payment_day=0,
    check_creation_popup=True,
    check_failure_popup=False,
    check_successful_creation=True,
):
    # NOTE: we hard code the URI because Django has no idea of React
    # routes (i.e. react internal virtual routes, not API endpoints)
    url = "/app/obligations/creation"

    # Frank creates an Obligation
    tester.browser.get(f"{tester.live_server_url}{url}")

    try:
        WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 5).until(
            EC.presence_of_element_located((By.ID, "name_field"))
        )
    except NoSuchElementException:
        tester.fail("Obligation creation UI did not load timely. Fail!")

    name_inputbox = tester.browser.find_element(By.ID, "name_field")
    total_amount_inputbox = tester.browser.find_element(
        By.ID, "total_amount_field"
    )
    obligation_type_inputbox = tester.browser.find_element(
        By.ID, "obligation_type_field"
    )
    monthly_payment_inputbox = tester.browser.find_element(
        By.ID, "monthly_payment_field"
    )
    start_date_inputbox = tester.browser.find_element(
        By.ID, "start_date_field"
    )
    end_date_inputbox = tester.browser.find_element(By.ID, "end_date_field")
    payment_date_inputbox = tester.browser.find_element(
        By.ID, "payment_day_field"
    )

    # NOTE: as soon as we have a second button, this will fail hard
    submit_button = tester.browser.find_element(By.ID, "submit_button")

    name_inputbox.send_keys(name)
    total_amount_inputbox.send_keys(total_amount)
    obligation_type_inputbox.send_keys(obligation_type)
    monthly_payment_inputbox.send_keys(monthly_payment)
    start_date_inputbox.send_keys(start_date)
    end_date_inputbox.send_keys(end_date)
    payment_date_inputbox.send_keys(payment_day)

    submit_button.click()

    if check_creation_popup:
        # Frank likes very much confirmation green modals.
        # He expects one to be shown, as he filled in everything correctly
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located(
                    (By.CLASS_NAME, "alert-success")
                )
            )
        except NoSuchElementException:
            tester.fail("Success popup was not displayed. Fail!")

    if check_failure_popup:
        # Frank likes very much confirmation green modals.
        # He expects one to be shown, as he filled in everything correctly
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located(
                    (By.CLASS_NAME, "alert-failure")
                )
            )
        except NoSuchElementException:
            tester.fail("Failure popup was not displayed. Fail!")

    # Check if the categoy page shows the created ob
    # and return the obligation id we get from obligation, or return -1
    if check_successful_creation:
        return find_a_fixed_obligation_in_list(
            tester,
            name=name,
            total_amount=total_amount,
            obligation_type=obligation_type,
            monthly_payment=monthly_payment,
            start_date=start_date,
            end_date=end_date,
            payment_day=payment_day,
            should_exist=True,
            should_be_missing=False,
        )
    return -1


def edit_a_fixed_obligation(
    tester,
    id,
    name,
    total_amount,
    obligation_type,
    monthly_payment,
    start_date,
    end_date,
    payment_day,
    check_success_popup=True,
    check_failure_popup=False,
    check_successful_edit=True,
):
    # NOTE: we hard code the URI because Django has no idea of React
    # routes (i.e. react internal virtual routes, not API endpoints)
    url = f"/app/obligations/{id}"

    # Frank creates an Obligation
    tester.browser.get(f"{tester.live_server_url}{url}")

    try:
        WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 5).until(
            EC.presence_of_element_located((By.ID, "name_field"))
        )
    except NoSuchElementException:
        tester.fail("Obligation creation UI did not load timely. Fail!")

    name_inputbox = tester.browser.find_element(By.ID, "name_field")
    total_amount_inputbox = tester.browser.find_element(
        By.ID, "total_amount_field"
    )
    obligation_type_inputbox = tester.browser.find_element(
        By.ID, "obligation_type_field"
    )
    monthly_payment_inputbox = tester.browser.find_element(
        By.ID, "monthly_payment_field"
    )
    start_date_inputbox = tester.browser.find_element(
        By.ID, "start_date_field"
    )
    end_date_inputbox = tester.browser.find_element(By.ID, "end_date_field")
    payment_date_inputbox = tester.browser.find_element(
        By.ID, "payment_day_field"
    )

    # NOTE: as soon as we have a second button, this will fail hard
    submit_button = tester.browser.find_element(By.ID, "submit_button")

    name_inputbox.clear()
    name_inputbox.send_keys(name)
    total_amount_inputbox.clear()
    total_amount_inputbox.send_keys(total_amount)
    obligation_type_inputbox.send_keys(obligation_type)
    monthly_payment_inputbox.clear()
    monthly_payment_inputbox.send_keys(monthly_payment)
    start_date_inputbox.clear()
    start_date_inputbox.send_keys(start_date)
    end_date_inputbox.clear()
    end_date_inputbox.send_keys(end_date)
    payment_date_inputbox.clear()
    payment_date_inputbox.send_keys(payment_day)

    submit_button.click()

    if check_success_popup:
        # Frank likes very much confirmation green modals.
        # He expects one to be shown, as he filled in everything correctly
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located(
                    (By.CLASS_NAME, "alert-success")
                )
            )
        except NoSuchElementException:
            tester.fail("Success popup was not displayed. Fail!")

    if check_failure_popup:
        # Frank likes very much confirmation green modals.
        # He expects one to be shown, as he filled in everything correctly
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located(
                    (By.CLASS_NAME, "alert-failure")
                )
            )
        except NoSuchElementException:
            tester.fail("Failure popup was not displayed. Fail!")

    # Check if the categoy page shows the created ob
    # and return the obligation id we get from obligation, or return -1
    if check_successful_edit:
        find_a_fixed_obligation_in_list(
            tester,
            name=name,
            total_amount=total_amount,
            obligation_type=obligation_type,
            monthly_payment=monthly_payment,
            start_date=start_date,
            end_date=end_date,
            payment_day=payment_day,
            should_exist=True,
            should_be_missing=False,
        )


def find_a_fixed_obligation_in_list(
    tester,
    name,
    total_amount,
    obligation_type,
    monthly_payment,
    start_date,
    end_date,
    payment_day,
    should_exist=True,
    should_be_missing=False,
):
    # NOTE: we hard code the URI because Django has no idea of React routes
    # (i.e. react internal virtual routes, not API endpoints)
    url = f"/app/obligations/"
    tester.browser.get(f"{tester.live_server_url}{url}")

    # Obligation list detail API might take a while to return
    # and provide the data to populate the table.
    # Let's make this test less flaky by allowing the UI some time to
    # properly render
    try:
        WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 1.1).until(
            EC.presence_of_element_located((By.ID, "obligations-table"))
        )
        # Frank methodically checks each field: they should all match!

        h3 = tester.browser.find_element(By.TAG_NAME, "h3")
        expect = "List Obligations"
        tester.assertEqual(expect, h3.text)

        table = tester.browser.find_element(By.ID, "obligations-table")
        rows = table.find_elements(By.TAG_NAME, "tr")

        found = False
        ob_id = None
        for _, c in enumerate(rows):
            # Ignore the table header, only check the table rows
            table_header = c.find_elements(By.TAG_NAME, "th")
            if len(table_header) > 0:
                continue

            tds = c.find_elements(By.TAG_NAME, "td")
            tester.assertEqual(len(tds), 10)
            if tds[1].text != name:
                continue
            # NOTE: the total amount will be formatted in JP locale as '￥67,933'
            total_amount_td = int(
                tds[2].text.replace("￥", "").replace(",", "")
            )
            if total_amount_td != total_amount:
                continue
            if tds[3].text != obligation_type:
                continue

            if tds[4].text != start_date:
                continue
            if tds[5].text != end_date:
                continue

            # NOTE: the monthly_payment will be formatted in JP locale as '￥3,200'
            monthly_payment_td = int(
                tds[6].text.replace("￥", "").replace(",", "")
            )
            if monthly_payment_td != monthly_payment:
                continue

            payment_day_td = int(tds[7].text)
            if payment_day_td != payment_day:
                continue
            found = True
            ob_id = int(tds[0].text)
            break

        if should_be_missing and found:
            tester.fail(
                f"Obligation with name {name} was NOT supposed"
                " to be displayed... but it was: fail!"
            )
            return ob_id

        if not should_exist and not found:
            tester.fail(f"Obligation with name {name} was not" " found: fail!")
            return -1

        return ob_id

    except TimeoutException:
        root = tester.browser.find_element(By.ID, "root")
        # If this user has zero obligations, and the one we're looking for is
        # NOT supposed to be there do nothing, otherwise fail
        msg = "Your Obglitation will appear here after you create any!"
        if not should_be_missing and msg in root.text:
            tester.fail(
                "We were looking for Obligation with name"
                f" {name}, but this user seems to"
                " have no obligations at all: fail!"
            )
        return -1  # Not called, just to make the style consistent

    except NoSuchElementException:
        msg = (
            "The Obligation list API did not load (yet?), hence the UI"
            "was was not properly rendered. Fail!"
        )
        tester.fail(msg)
        return -1  # Not called, just to make the style consistent


def visit_and_verify_a_fixed_obligation(
    tester,
    fixed_ob,
    should_be_missing,
):
    # NOTE: we hard code the URI because Django has no idea of React routes
    # (i.e. react internal virtual routes, not API endpoints)
    url = f"/app/obligations/"
    tester.browser.get(f"{tester.live_server_url}{url}")

    name = fixed_ob["name"]
    total_amount = fixed_ob["total_amount"]
    obligation_type = fixed_ob["obligation_type"]
    monthly_payment = fixed_ob["monthly_payment"]
    start_date = fixed_ob["start_date"]
    end_date = fixed_ob["end_date"]
    payment_day = fixed_ob["payment_day"]

    obligation_id = find_a_fixed_obligation_in_list(
        tester,
        name,
        total_amount,
        obligation_type,
        monthly_payment,
        start_date,
        end_date,
        payment_day,
        should_exist=True,
        should_be_missing=False,
    )

    if obligation_id == -1 and not should_be_missing:
        tester.fail("Obligation was not found. Fail!")
        return

    # NOTE: we hard code the URI because Django has no idea of React routes
    # (i.e. react internal virtual routes, not API endpoints)
    url = f"/app/obligations/{obligation_id}"
    tester.browser.get(f"{tester.live_server_url}{url}")

    # Budget detail API might take a while to return
    # and provide the data to populate the form.
    # Let's make this test less flaky by allowing the UI some time to
    #  properly render
    try:
        WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 1.1).until(
            EC.presence_of_element_located((By.ID, "name_field"))
        )
        # Frank methodically checks each field: they should all match!

        name_inputbox = tester.browser.find_element(By.ID, "name_field")
        tester.assertEqual(name_inputbox.get_attribute("value"), name)
        total_amount_inputbox = tester.browser.find_element(
            By.ID, "total_amount_field"
        )
        tester.assertEqual(
            int(
                total_amount_inputbox.get_attribute("value").replace(".00", "")
            ),
            total_amount,
        )
        obligation_type_inputbox = tester.browser.find_element(
            By.ID, "obligation_type_field"
        )
        tester.assertEqual(
            obligation_type_inputbox.get_attribute("value"), obligation_type
        )
        monthly_payment_inputbox = tester.browser.find_element(
            By.ID, "monthly_payment_field"
        )
        tester.assertEqual(
            int(
                monthly_payment_inputbox.get_attribute("value").replace(
                    ".00", ""
                )
            ),
            monthly_payment,
        )
        start_date_inputbox = tester.browser.find_element(
            By.ID, "start_date_field"
        )
        tester.assertEqual(
            start_date_inputbox.get_attribute("value"), start_date
        )
        end_date_inputbox = tester.browser.find_element(
            By.ID, "end_date_field"
        )
        tester.assertEqual(end_date_inputbox.get_attribute("value"), end_date)
        payment_date_inputbox = tester.browser.find_element(
            By.ID, "payment_day_field"
        )
        tester.assertEqual(
            int(payment_date_inputbox.get_attribute("value")), payment_day
        )
    except TimeoutException:
        tester.fail("The Obligation detail API did not load (yet?). Fail!")
    except NoSuchElementException:
        msg = (
            "The Obligation detail API did not load (yet?), hence the UI"
            "was was not properly rendered. Fail!"
        )
        tester.fail(msg)

    if obligation_type == "variable":
        tester.fail(
            "implement me. We should look for missing 'changes' section or for a list of one or more changes'"
        )
        allocated = WebDriverWait(
            tester.browser, MAX_WAIT_SECONDS * 1.1
        ).until(
            EC.presence_of_element_located((By.ID, "allocated_amount_field"))
        )
        # Frank methodically checks each field: they should all match!

        prefilled_amount = allocated.get_attribute("value")
        tester.assertEqual(int(prefilled_amount), total_amount)


def directly_access_a_fixed_obligation_edit_page(
    tester, obligation_id, check_failure_popup
):
    # NOTE: we hard code the URI because Django has no idea of React routes
    # (i.e. react internal virtual routes, not API endpoints)
    url = f"/app/obligations/{obligation_id}"
    tester.browser.get(f"{tester.live_server_url}{url}")

    if check_failure_popup:
        # Frank likes very much confirmation red modals.
        # He is shown that something went wrong, and why
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located((By.CLASS_NAME, "alert-danger"))
            )
        except NoSuchElementException:
            tester.fail("Failure popup was not displayed. Fail!")

        # Frank likes very much confirmation red modals.
        # He is shown that something went wrong, and why
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located((By.CLASS_NAME, "alert-danger"))
            )
            expected_text = (
                "Sorry, it seems the Obligation you're trying to"
                " editing does not exist...or there was an error."
                " Please try again later, or reload the page"
            )
            modal_text = tester.browser.find_element(
                By.CLASS_NAME, "alert-heading"
            )
            tester.assertEqual(modal_text.text, expected_text)
        except NoSuchElementException:
            tester.fail("Failure popup was not displayed. Fail!")

    else:
        # Obligation detail API might take a while to return
        # and provide the data to populate the form.
        # Let's make this test less flaky by allowing the UI some time to
        # properly render
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located((By.ID, "name_field"))
            )
        except TimeoutException:
            tester.fail("The Obligation detail API did not load (yet?). Fail!")
        except NoSuchElementException:
            tester.fail(
                "The Obligation detail API did not load (yet?), hence the UI"
                "was was not properly rendered. Fail!"
            )


def delete_a_fixed_obligation(
    tester,
    obligation_id,
    check_successful_delete_popup,
    check_successful_delete,
):
    # NOTE: we hard code the URI because Django has no idea of React
    # routes (i.e. react internal virtual routes, not API endpoints)
    url = "/app/obligations"

    # Frank visits the obligations page
    tester.browser.get(f"{tester.live_server_url}{url}")

    # Since the delete buttons is shown after the obligation list API has returned
    # let's give the frontend sometime to call said API
    # and render the view accordingly
    try:
        WebDriverWait(tester.browser, MAX_WAIT_SECONDS * 3).until(
            EC.presence_of_element_located((By.ID, "obligations-table"))
        )
    except NoSuchElementException:
        tester.fail("The obligation UI did not load in time. Fail!")

    # Franks finds the row with the obligation he wants to delete
    table = tester.browser.find_element(By.ID, "obligations-table")
    # We use a special marker in the table to speed up integration tests
    row = table.find_element(By.ID, f"ob-{obligation_id}")
    delete_button = row.find_element(By.CLASS_NAME, "btn-danger")
    delete_button.click()

    try:
        WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
            EC.presence_of_element_located((By.CLASS_NAME, "modal-dialog"))
        )
    except NoSuchElementException:
        tester.fail("The deletion modal did not display in time. Fail!")

    # Frank is very sure about his choice: he goes ahead and confirms it
    confirm_deletion_button = tester.browser.find_element(
        By.CLASS_NAME, "modal-content"
    ).find_element(By.CLASS_NAME, "btn-primary")
    confirm_deletion_button.click()

    if check_successful_delete_popup:
        # Frank likes very much the green confirmation modals, green is  his
        # favourite colour! He expects a modal to be shown, as he filled in
        # everything correctly
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located(
                    (By.CLASS_NAME, "alert-success")
                )
            )
        except NoSuchElementException:
            tester.fail("Success popup was not displayed. Fail!")
        except TimeoutException:
            tester.fail("Success popup was not displayed (yet?). Fail!")


def get_live_server_test_case_socket_name():
    """
    When testing inside gitlab-ci, the test container needs to listen to all
    interfaces, so that the firefox remote container can access it
    By default, this value is localhost, but "localhost" inside a continer
    is not reachable from other containers in gitlab-ci. Hence get the container name
    References https://marcgibbons.com/posts/selenium-in-docker
               https://stackoverflow.com/questions/44240139
               https://stackoverflow.com/questions/24319662/from-inside-of-a-docker-container-how-do-i-connect-to-the-localhost-of-the-mach  # pylint: disable=C0301; # noqa
    """
    domain = "localhost"
    if os.environ.get("TEST_HOSTNAME") is not None:
        domain = os.environ.get("TEST_HOSTNAME")
    return domain


def select_data_by_account_name_from_pulldown(
    tester, account_name, expect_no_data=False
):
    try:
        pulldown = WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
            EC.presence_of_element_located((By.ID, "filterAccount"))
        )
    except TimeoutException:
        tester.fail("The account list did not load in time. Fail!")
    except NoSuchElementException:
        tester.fail("The account list did not load in time. Fail!")

    # Select the account by name
    Select(pulldown).select_by_visible_text(account_name)

    if expect_no_data:
        missing_data_marker = "assetTableNoData"
        try:
            WebDriverWait(tester.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located((By.ID, missing_data_marker))
            )
        except TimeoutException:
            tester.fail("The account was not supposed to be there. Fail!")
