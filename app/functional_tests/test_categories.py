# Copyright: (c) 2019, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
import json

from django.urls import reverse

from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

import functional_tests.helpers as Helpers
from functional_tests.test_parent import IntegrationTest

from unittest import skip

# This is the maximum time that Frank and Guido are able to wait (in seconds)
# before giving up and throw a tantrum
MAX_WAIT_SECONDS = 2


class CategoriesIntegrationTest(IntegrationTest):
    """
    Integration test suite for Categories related views
    """

    def setUp(self):
        # Instrument the unit tests to fail if they run for too long
        super().setUp()

    # FIX ME: this test will succeed for ANY failure,
    # not only for creation failure
    @Helpers.register_and_login
    def test_cant_create_an_empty_expense_category(self):
        cat = ""

        Helpers.create_expense_category(
            self, category_name=cat, check_creation_popup=False
        )

    @Helpers.register_and_login
    def test_can_create_multiple_expense_categories(self):
        """Check that creation of multiple categories is successful"""
        # Frank can create a category to log expenses related to his rent
        c1 = Helpers.generate_string()

        Helpers.create_expense_category(
            self, category_name=c1, check_creation_popup=True
        )

        # Frank can create a category to log his food expenses
        c2 = Helpers.generate_string()
        Helpers.create_expense_category(
            self, category_name=c2, check_creation_popup=True
        )

    @Helpers.register_and_login
    def test_different_users_can_create_categories_with_the_same_name(self):
        # Frank can create a category to log his expenses

        c1 = Helpers.generate_string()
        Helpers.create_expense_category(
            self, category_name=c1, check_creation_popup=True
        )

        Helpers.logout_user(self)

        # Guido can create a category with the same name
        Helpers.create_user(self)
        Helpers.create_expense_category(
            self, category_name=c1, check_creation_popup=True
        )

    @Helpers.register_and_login
    def test_cant_create_duplicate_expense_categories(self):
        # Frank can create a category to log expenses related to his rent
        c1 = Helpers.generate_string()
        Helpers.create_expense_category(
            self, category_name=c1, check_creation_popup=True
        )

        # Frank is not paying attention to what he is doing, and he tries
        # to create the same category
        Helpers.create_expense_category(
            self,
            category_name=c1,
            check_creation_popup=False,
            check_failure_popup=True,
        )

    @Helpers.register_and_login
    def test_users_cant_see_other_users_expense_categories(self):
        # Frank can create a category to log his expenses
        c_text = Helpers.generate_string()

        Helpers.create_expense_category(
            self, category_name=c_text, check_creation_popup=True
        )
        Helpers.logout_user(self)

        # Guido can not see Frank's expense category
        Helpers.create_user(self)

        _ = Helpers.check_category_existence(
            self, c_text, "categories", expected_to_fail=True
        )

    @Helpers.register_and_login
    def test_can_create_and_retrieve_expense_categories(self):
        """
        Check that creation and retrival of categories works correctly
        """
        # Frank can create a category to log his food expenses
        c1_text = Helpers.generate_string()

        Helpers.create_expense_category(
            self, c1_text, check_creation_popup=True
        )

        _ = Helpers.check_category_existence(
            self, c1_text, "categories", expected_to_fail=False
        )

        # Frank does not trust the app, and he wants to check if new Categories
        # will appear in his next call or not
        c2 = Helpers.generate_string()

        Helpers.create_expense_category(self, c2, check_creation_popup=True)
        _ = Helpers.check_category_existence(
            self, c2, "categories", expected_to_fail=False
        )

    @Helpers.register_and_login
    def test_users_can_not_see_other_users_expense_categories(self):
        """
        Check that only current user categories are returned
        """
        # Frank can create a category to log his expenses
        c_text = Helpers.generate_string()

        Helpers.create_expense_category(
            self, category_name=c_text, check_creation_popup=True
        )
        _ = Helpers.check_category_existence(
            self, c_text, "categories", expected_to_fail=False
        )

        # Guido registers to the site
        Helpers.create_user(tester=self)

        # Guido can NOT see the category
        _ = Helpers.check_category_existence(
            self, c_text, "categories", expected_to_fail=True
        )

        url = url = f"/app/categories"
        accounts_url = f"{self.live_server_url}{url}"
        self.browser.get(accounts_url)

        msg = "You need to create a Category first!"

        try:
            h5 = WebDriverWait(self.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located((By.TAG_NAME, "h5"))
            )
            self.assertIn(msg, h5.text)
        except NoSuchElementException:
            self.fail("Missing Category guidance was not displayed. Fail!")

    # Flaky test: see issue #287
    @skip
    def test_same_user_can_not_have_two_expense_categories_with_the_same_name(
        self,
    ):  # Refactor me out to complete #46
        print(
            "test_same_user_can_not_have_two_expense_categories_with_the_same_name"
            "is calling the API directly. This makes the test flaky."
            " Address it to close #46"
        )
        c_name = Helpers.generate_string()

        Helpers.create_expense_category(
            self, c_name, check_creation_popup=True
        )

        _ = Helpers.check_category_existence(
            self, c_name, "categories", expected_to_fail=False
        )

        # Frank tries to force the app to display an error by creating another
        # category with the same name
        Helpers.create_expense_category(self, c_name, check_failure_popup=True)

        url = reverse(
            "api:v1:categories"
        )  # FIXME: we should test the frontend, not the APIs!!!!
        # Frank has knowledge of how to force the site to display json
        # BUGFIX-287 Cleanup this test, and stop calling the api to confirm the
        # entity creation: access the list view instead  <==================================================
        secret_url = f"{self.live_server_url}{url}?format=json&json=true"
        self.browser.get(secret_url)

        html = self.browser.find_element(By.TAG_NAME, "html")
        json_res = json.loads(html.text)

        # Frank is happy that the app prevented duplicates: duplicates should
        # not be allowed as per the specs!
        self.assertEqual(json_res["count"], 1)
        self.assertEqual(json_res["results"][0]["text"], c_name)
        self.assertEqual(json_res["count"], 1)

    @Helpers.register_and_login
    def test_different_users_can_have_two_expense_categories_with_the_same_name(
        self,
    ):
        cat_name = Helpers.generate_string()

        Helpers.create_expense_category(
            self, cat_name, check_creation_popup=True
        )

        frank_cat_id = Helpers.check_category_existence(
            self, cat_name, "categories"
        )

        # Frank logs off as he's done with today's tasks
        Helpers.logout_user(self)

        # Guido feels like trying to break the app.
        # He registers to the site, and creates a category with the same name
        _ = Helpers.create_user(self)

        # Guido has found Frank's category's name. Guido wants to imitate Frank
        # and create a category with the same name
        Helpers.create_expense_category(
            self, cat_name, check_creation_popup=True
        )

        # Guido is so upset that the app did not break: it seems the app now allows different users
        # to have categories with the same name
        guido_cat__id = Helpers.check_category_existence(
            self, cat_name, "categories"
        )
        self.assertNotEqual(frank_cat_id, guido_cat__id)

    @Helpers.register_and_login
    def test_users_can_entirely_edit_expense_categories_successfully(self):
        cat_name = Helpers.generate_string()

        Helpers.create_expense_category(
            self, cat_name, check_creation_popup=True
        )

        cat_id = Helpers.check_category_existence(self, cat_name, "categories")

        is_archived = True
        check_edit_success_popup = True
        check_edit_failure_popup = False
        new_cat_name = Helpers.generate_string()
        Helpers.edit_an_expense_category(
            self,
            cat_id,
            new_cat_name,
            is_archived,
            check_edit_success_popup=check_edit_success_popup,
            check_edit_failure_popup=check_edit_failure_popup,
        )

    @Helpers.register_and_login
    def test_users_can_partially_edit_expense_categories(self):
        cat_name = Helpers.generate_string()

        # Frank creates a category using the React app. He likes to download
        # a big blob of javascript
        Helpers.create_expense_category(
            self, cat_name, check_creation_popup=True
        )
        cat_id = Helpers.check_category_existence(
            self, cat_name, "categories", expected_to_fail=False
        )

        # Note: we do not use edit_category helper as we want to test partial editing
        url = f"/app/categories/{cat_id}"
        self.browser.get(f"{self.live_server_url}{url}")

        # Frank only edits the checkbox: he has heard that once a bug prevented category
        # to be only half updated
        # Frank is a nice boy scout, and decides to checkout if the bug is gone or not.
        # He'd have to report a regression otherwise!

        try:
            WebDriverWait(self.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located((By.ID, "is_archived_field"))
            )
        except NoSuchElementException:
            self.fail("The UI did not loaded in time. Fail!")

        is_archived_goal_checkbox = self.browser.find_element(
            By.ID, "is_archived_field"
        )
        is_archived_goal_checkbox.click()

        inputbox = self.browser.find_element(By.ID, "text_field")
        inputbox.send_keys(Keys.ENTER)

        # Frank very likes confirmation green modals. He expects one to be shown
        # as he the regression was just a rumor
        try:
            WebDriverWait(self.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located(
                    (By.CLASS_NAME, "alert-success")
                )
            )
        except NoSuchElementException:
            self.fail("Success popup was not displayed. Fail!")

        # Franks is wondering whether if only changing the text will trigger the rumored regression.
        # Frank is very methodical, so he creates a new category
        # to make sure he's not chasing ghost bugs
        # (Frank has read about software engineers recently, he wants to pretend to be one himself)
        second_cat_name = Helpers.generate_string()

        Helpers.create_expense_category(
            self, second_cat_name, check_creation_popup=True
        )

        cat_id2 = Helpers.check_category_existence(
            self, second_cat_name, "categories"
        )

        # Note: we do not use edit_category helper as we want to test partial editing

        # Frank then proceeds to go ahead and edit his second category
        edit_url = f"/app/categories/{cat_id2}"
        self.browser.get(f"{self.live_server_url}{edit_url}")

        # This test depends on an api: give some time for the UI to properly load the
        # data and render accordingly
        try:
            WebDriverWait(self.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located((By.ID, "text_field"))
            )
        except NoSuchElementException:
            self.fail("UI has not properly loaded yet Fail!")

        # Frank only edits the text: enters a completely different string and press enter
        inputbox = self.browser.find_element(By.ID, "text_field")
        updated_cat_name = Helpers.generate_string()
        inputbox.send_keys(updated_cat_name)
        inputbox.send_keys(Keys.ENTER)

        # Frank very likes confirmation green modals. He expects one to be shown
        # as the regression was just a rumor afterall!
        try:
            WebDriverWait(self.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located(
                    (By.CLASS_NAME, "alert-success")
                )
            )
        except NoSuchElementException:
            self.fail("Success popup was not displayed. Fail!")

    @Helpers.register_and_login
    def test_users_can_not_edit_other_users_categories(self):
        cat_name = Helpers.generate_string()

        Helpers.create_expense_category(
            self, cat_name, check_creation_popup=True
        )
        cat_id = Helpers.check_category_existence(self, cat_name, "categories")

        Helpers.logout_user(self)

        # Guido has found Frank's category's id on a post it, and tries to alter its data
        _ = Helpers.create_user(self)

        is_archived = True
        check_edit_success_popup = False
        check_edit_failure_popup = False
        check_inability_to_load_category = True
        Helpers.edit_an_expense_category(
            self,
            cat_id,
            cat_name,
            is_archived,
            check_edit_success_popup=check_edit_success_popup,
            check_edit_failure_popup=check_edit_failure_popup,
            check_inability_to_load_category=check_inability_to_load_category,
        )

    # WRITE ME
    @skip
    def test_regression_216_can_resubmit_a_form_after_a_failed_submission(
        self,
    ):
        """
        WRITE ME
        """
        pass
