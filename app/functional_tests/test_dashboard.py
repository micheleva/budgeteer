# Copyright: (c) 2019, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from datetime import date
import random
from unittest import skip

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC

import functional_tests.helpers as Helpers
from functional_tests.test_assets import MockAssetsAPI
from functional_tests.test_dividends import MockDividendsAPI
from functional_tests.test_parent import IntegrationTest

# Maximum amount of seconds to wait before giving up
MAX_WAIT_SECONDS = 2


class DashboardIntegrationTest(IntegrationTest):
    """
    Integration test for the Dashboard guidance banners.
    """

    def setUp(self):
        # Instrument the unit tests to fail if they run for too long
        super().setUp()

    @Helpers.register_and_login
    def test_correct_guidance_for_expenses_in_dashboard(self):
        title = "Expenses"
        guidance = "No expenses logged for the current month yet!"
        header_class = "text-info"
        button_text = "Create one"
        button_url = "/app/expenses/creation"

        # Frank can see a clear guidance in the Dashboard: he has no Categories yet!
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
        )

        cat_text = Helpers.generate_string()
        Helpers.create_expense_category(
            self, cat_text, check_creation_popup=True
        )
        # Grab category ID
        cat_id = Helpers.check_category_existence(self, cat_text, "categories")

        # Frank actually creates the expense
        check_creation_popup = True
        check_failure_popup = False
        _ = Helpers.create_an_expense_using_react(
            self,
            cat_id,
            check_creation_popup=check_creation_popup,
            check_failure_popup=check_failure_popup,
        )

        url = "/"
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see the original guidance is not displayed anymore
        # as he has created a Spending Limit
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=True,
        )

    @Helpers.register_and_login
    def test_correct_guidance_for_spending_limits_in_dashboard(self):
        title = "Spending Limits"
        guidance = "No Spending Limits created yet!"
        header_class = "text-info"
        button_text = "Create a Spending Limit"
        button_url = "/app/spending-limits/creation"

        # Frank can see a clear guidance in the Dashboard: he has no Categories yet!
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
        )

        cat_text = Helpers.generate_string()
        Helpers.create_expense_category(
            self,
            cat_text,
            check_creation_popup=True,
            check_failure_popup=False,
        )
        cat_id = Helpers.check_category_existence(self, cat_text, "categories")

        Helpers.create_spending_limit(
            self,
            cat_id,
            amount=random.randint(1, 90000),
            days=random.randint(1, 27),
            currency=Helpers.generate_string(3),
            text=Helpers.generate_string(),
            check_creation_popup=True,
            check_failure_popup=False,
        )

        url = "/"
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see the original guidance is not displayed anymore
        # as he has created a Spending Limit
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=True,
            # Spending limit widget has no links, hence we skip the check
            check_links=False,
        )

    @Helpers.register_and_login
    def test_correct_guidance_for_monthly_balances_in_dashboard(self):
        title = "Monthly balances trend"
        guidance = "You have no monthly balances yet!"
        header_class = "text-info"
        button_text = "Create a Monthly Balance"
        button_url = f"/app/monthly-balances/multiple-creation/date/{date.today().replace(day=1).isoformat()}"

        # Frank can see a clear guidance in the Dashboard: he has no Categories yet!
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
        )

        cat_name = Helpers.generate_string()
        this_month = date.today().replace(day=1).strftime("%Y-%m")
        cat_name = Helpers.generate_string()

        # Frank creates the category
        Helpers.create_monthly_balance_category(
            self,
            cat_name,
            check_creation_popup=True,
            check_failure_popup=False,
        )
        cat_id = Helpers.check_category_existence(
            self,
            text=cat_name,
            url="monthly-balances/categories",
            expected_to_fail=False,
        )

        # Frank creates the necessary data
        amount = random.randint(1, 90000)
        Helpers.create_a_monthly_balance(
            self, cat_id, amount, this_month, create_check=True
        )

        url = "/"
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see the original guidance is not displayed anymore
        # as he has created a Spending Limit
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=True,
        )

    @Helpers.register_and_login
    def test_correct_guidance_for_assets_in_dashboard(self):
        print(
            "REFACTOR test_correct_guidance_for_assets_in_dashboard: it is "
            "an horribly long function without helpers."
        )
        title = "Assets stats"
        guidance = "You need to create some Monthly Balance or Assets first!"
        header_class = "text-info"
        button_text = "Import Assets data"
        button_url = "/app/assets/import"
        second_button_text = "Create Monthly Balances"
        second_button_url = f"/app/monthly-balances/multiple-creation/date/{date.today().replace(day=1).isoformat()}"

        # Frank can see a clear guidance in the Dashboard: he has no Assets nor Monthly Balance data yet!
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
        )

        # Frank can see a clear guidance in the Dashboard: he has no Assets nor Monthly Balance data yet!
        Helpers.guidance_check(
            self,
            title,
            guidance,
            second_button_text,
            header_class,
            second_button_url,
            expect_to_be_missing=False,
            button_order=1,
        )

        cat_name = Helpers.generate_string()

        Helpers.create_user(self)
        Helpers.create_monthly_balance_category(
            self,
            category_name=cat_name,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        c_id = Helpers.check_category_existence(
            self,
            text=cat_name,
            url="monthly-balances/categories",
            expected_to_fail=False,
        )

        Helpers.create_a_monthly_balance(
            self,
            category_id=c_id,
            amount=random.randint(1, 1000),
            date=date.today().replace(day=1).strftime("%Y-%m"),
            create_check=True,
        )

        acc_name = Helpers.generate_string(20)
        credit_entity_name = Helpers.generate_string(20)
        is_foreign_currency = random.choice([True, False])
        Helpers.create_an_account(
            self,
            acc_name=acc_name,
            credit_entity_name=credit_entity_name,
            is_foreign_currency=is_foreign_currency,
            check_create_success_popup=True,
            check_create_failure_popup=False,
        )

        try:
            port = 13508
            self.mock_server = MockAssetsAPI(port)
            self.mock_server.daemon = True
            self.mock_server.start()

            socket_name = Helpers.get_live_server_test_case_socket_name()
            target_date = (
                "2023-07-01"  # REFACTOR ME: Is this the correct date?
            )
            mock_server_url = f"http://{socket_name}:{port}/assets"
            Helpers.import_assets_from_third_party_api(
                tester=self, apiURL=mock_server_url, target_date=target_date
            )
        except Exception as e:
            self.mock_server.stop()
        self.mock_server.stop()

        url = "/app/"
        self.browser.get(f"{self.live_server_url}{url}")

        # Frank can see the original guidance about Assets is not displayed anymore
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=True,
        )

    @Helpers.register_and_login
    def test_correct_guidance_for_dividends_in_dashboard(self):
        print(
            "REFACTOR test_correct_guidance_for_dividends_in_dashboard: it is "
            "an horribly long function without helpers."
        )
        title = "Dividends yearly stats"
        guidance = "You need to create some Dividends first!"
        header_class = "text-info"
        button_text = "Import Dividends"
        button_url = "/app/dividends/import"

        # Frank can see a clear guidance in the Dashboard: he has no Dividends data yet!
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
        )

        acc_name = Helpers.generate_string(20)
        credit_entity_name = Helpers.generate_string(20)
        is_foreign_currency = True
        Helpers.create_an_account(
            self,
            acc_name=acc_name,
            credit_entity_name=credit_entity_name,
            is_foreign_currency=is_foreign_currency,
            check_create_success_popup=True,
            check_create_failure_popup=False,
        )

        try:
            port = 13509
            self.mock_server = MockDividendsAPI(port)
            self.mock_server.daemon = True
            self.mock_server.start()

            socket_name = Helpers.get_live_server_test_case_socket_name()
            target_date = (
                "2023-07-01"  # REFACTOR ME: Is this the correct date?
            )
            mock_server_url = f"http://{socket_name}:{port}/dividends"
            Helpers.import_dividends_from_third_party_api(
                tester=self, apiURL=mock_server_url, target_date=target_date
            )

        except Exception as e:
            self.mock_server.stop()

        self.mock_server.stop()
        url = "/"
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see the original guidance is not displayed anymore
        # as he has created a Spending Limit
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=True,
        )

    @Helpers.register_and_login
    def test_correct_guidance_for_monthly_spending_budgets_in_dashboard(self):
        title = "Monthly Spending Budgets"
        guidance = "You need to create a category first!"
        header_class = "text-info"
        button_text = "Create a Category"
        button_url = "/app/categories/creation"

        # Frank can see a clear guidance in the Dashboard: he has no Categories data yet!
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
        )

        cat_text = Helpers.generate_string()
        Helpers.create_expense_category(
            self, cat_text, check_creation_popup=True
        )

        # Frank can see the guidance in the Dashboard for Monthly Spending Budgets has disapperead
        url = "/"
        self.browser.get(f"{self.live_server_url}{url}")
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=True,
        )

    @Helpers.register_and_login
    def test_correct_guidance_for_yearly_spending_budgets_in_dashboard(self):
        title = "Yearly Spending Budgets"
        guidance = "You need to create a category first!"
        header_class = "text-info"
        button_text = "Create a Category"
        button_url = "/app/categories/creation"

        # Frank can see a clear guidance in the Dashboard: he has no Categories data yet!
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
        )

        cat_text = Helpers.generate_string()
        Helpers.create_expense_category(
            self, cat_text, check_creation_popup=True
        )

        # Frank can see the guidance in the Dashboard for Yearly Spending Budgets has disapperead
        url = "/"
        self.browser.get(f"{self.live_server_url}{url}")
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=True,
        )

    @Helpers.register_and_login
    def test_correct_guidance_for_missing_monthly_spending_budgets_in_dashboard(
        self,
    ):
        print(
            "REFACTOR test_correct_guidance_for_missing_monthly_spending_bu"
            "dgets_in_dashboard: it has blocking waits."
        )
        cat_text = Helpers.generate_string()
        Helpers.create_expense_category(
            self, cat_text, check_creation_popup=True
        )
        cat_id = Helpers.check_category_existence(self, cat_text, "categories")
        today = date.today()
        date_text = today.strftime("%Y-%m")

        # Frank can see a clear guidance in the Dashboard: his Category has no
        # Spending Budget for the current month
        url = "/"
        self.browser.get(f"{self.live_server_url}{url}")

        try:
            Helpers.check_guidance_text_and_ui(
                self,
                title="Monthly Spending Budgets",
                guidance=f"These Spending Categories do not have a Budget for {date_text}",
                class_name="text-danger",
                secondary_text=f"These Spending Categories do not have a Budget for {date_text}\n{cat_text}",
            )

            # Frank creates a Spending Budget for this month/Category
            amount = random.randint(1, 90000)
            Helpers.create_a_budget(
                self,
                cat_type="spending",
                cat_id=cat_id,
                amount=amount,
                allocated_amount=None,
                year=today.year,
                month=today.month,
                check_creation_popup=True,
                check_failure_popup=False,
            )

            url = "/"
            self.browser.get(f"{self.live_server_url}{url}")

            # Frank is happy to see the guidance has changed
            # All is green now!
            Helpers.check_guidance_text_and_ui(
                self,
                title="Monthly Spending Budgets",
                guidance="All Spending Categories have a budget. You're all set!",
                class_name="text-success",
                secondary_text=None,
            )

        except TimeoutException:
            self.fail(
                "Either Monthly Spending API has not completed (yet), or the"
                "guidance for Monthly Spending was not displayed. Fail!"
            )

        except NoSuchElementException:
            self.fail(
                "Either Monthly Spending API has not completed (yet), or the"
                "guidance for Monthly Spending was not displayed. Fail!"
            )

    @Helpers.register_and_login
    def test_correct_guidance_for_missing_yearly_spending_budgets_in_dashboard(
        self,
    ):
        print(
            "REFACTOR test_correct_guidance_for_missing_yearly_spending_bud"
            "dgets_in_dashboard: it has blocking waits."
        )
        cat_text = Helpers.generate_string()
        Helpers.create_expense_category(
            self, cat_text, check_creation_popup=True
        )
        cat_id = Helpers.check_category_existence(self, cat_text, "categories")
        today = date.today()
        date_text = today.strftime("%Y")

        # Frank can see a clear guidance in the Dashboard: his Category has no
        # Spending Budget for the current month
        url = "/"
        self.browser.get(f"{self.live_server_url}{url}")

        try:
            Helpers.check_guidance_text_and_ui(
                self,
                title="Yearly Spending Budgets",
                guidance=f"These Spending Categories do not have a Budget for {date_text}",
                class_name="text-danger",
                secondary_text=f"These Spending Categories do not have a Budget for {date_text}\n{cat_text}",
            )

            # Frank creates a Spending Budget for this month/Category
            amount = random.randint(1, 90000)
            Helpers.create_a_budget(
                self,
                cat_type="spending",
                cat_id=cat_id,
                amount=amount,
                allocated_amount=None,
                year=today.year,
                month=None,
                check_creation_popup=True,
                check_failure_popup=False,
            )

            url = "/"
            self.browser.get(f"{self.live_server_url}{url}")

            # Frank is happy to see the guidance has changed
            # All is green now!
            Helpers.check_guidance_text_and_ui(
                self,
                title="Yearly Spending Budgets",
                guidance="All Spending Categories have a budget. You're all set!",
                class_name="text-success",
                secondary_text=None,
            )

        except TimeoutException:
            self.fail(
                "Either Yearly Spending API has not completed (yet), or the"
                "guidance for Yearly Spending was not displayed. Fail!"
            )

        except NoSuchElementException:
            self.fail(
                "Either Yearly Spending API has not completed (yet), or the"
                "guidance for Yearly Spending was not displayed. Fail!"
            )

    # WRITE ME
    @skip
    @Helpers.register_and_login
    def test_correct_guidance_for_monthly_overbudgeted_spending_budgets_in_dashboard(
        self,
    ):
        self.fail("I should be checking for montlhy overbudgets")

    # WRITE ME
    @skip
    def test_correct_guidance_for_yearly_overbudgeted_spending_budgets_in_dashboard(
        self,
    ):
        self.fail(
            "I should be checking for overbudgeted yearly spending budgets"
        )

    @Helpers.register_and_login
    def test_correct_guidance_for_monthly_saving_budgets_in_dashboard(
        self,
    ):
        title = "Monthly Saving Budgets"
        guidance = "You need to create a Saving Category first!"
        header_class = "text-info"
        button_text = "Create a Saving Category"
        button_url = "/app/saving-categories/creation"

        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
        )

        c1 = Helpers.generate_string()
        Helpers.create_saving_category(
            self, c1, check_create_success_popup=True
        )

        url = "/"
        self.browser.get(f"{self.live_server_url}{url}")
        header_class = "text-danger"
        # Frank can see the original guidance is not displayed anymore
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=True,
        )

        # Also, Frank can see there is a different guidance
        year = date.today().year
        month = date.today().month
        guidance = (
            "These Saving Categories do not have a Budget for"
            f" {year}-{month:02d}"
        )
        button_text = "See Details"
        button_url = f"/app/budgets/date/{year}/{month}"
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
        )

        # Frank sees the app is requesting a Yearly budget for the Saving
        # Category he just created
        Helpers.check_guidance_content(self, title, [c1])

        # Frank creates a Yearly Budget for said category
        cat_id = Helpers.check_category_existence(
            self,
            text=c1,
            url="saving-categories",
            expected_to_fail=False,
        )
        amount = random.randint(1, 90000)
        allocated_amount = random.randint(1, amount)
        Helpers.create_a_budget(
            self,
            cat_type="saving",
            cat_id=cat_id,
            amount=amount,
            allocated_amount=allocated_amount,
            year=year,
            month=month,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        # Frank cheks the guidance has changed
        url = "/"
        self.browser.get(f"{self.live_server_url}{url}")

        header_class = "text-success"
        guidance = "All Saving Categories have a budget. You're all set!"
        button_text = "See Details"
        button_url = f"/app/budgets/date/{year}/{month}"
        Helpers.guidance_check(
            self,
            title=title,
            guidance=guidance,
            button_text=button_text,
            header_class=header_class,
            button_url=button_url,
            expect_to_be_missing=False,
        )

    @Helpers.register_and_login
    def test_correct_guidance_for_yearly_saving_budgets_in_dashboard(self):
        title = "Yearly Saving Budgets"
        guidance = "You need to create a Saving Category first!"
        header_class = "text-info"
        button_text = "Create a Saving Category"
        button_url = "/app/saving-categories/creation"

        # Frank can see a clear guidance in the Dashboard: he has no
        # Saving Categories yet!
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
        )

        # Frank creates one Saving Category
        c1 = Helpers.generate_string()
        Helpers.create_saving_category(
            self, c1, check_create_success_popup=True
        )

        url = "/"
        self.browser.get(f"{self.live_server_url}{url}")
        # Also, Frank can see there is a different guidance
        title = "Yearly Saving Budgets"
        header_class = "text-danger"
        year = date.today().year
        guidance = f"These Saving Categories do not have a Budget for {year}"
        button_text = "See Details"
        button_url = f"/app/budgets/date/{year}"
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
        )

        # Frank sees the app is requesting a Yearly budget for the Saving
        # Category he just created
        Helpers.check_guidance_content(self, title, [c1])

        # Frank creates one more Saving Category: he is curious to see if the
        # warning will warn about this category missing a Yearly budget as well
        c2 = Helpers.generate_string()
        Helpers.create_saving_category(
            self, c2, check_create_success_popup=True
        )

        # Frank sees the app is requesting a Yearly budget for the Saving
        # Category he just created
        url = "/"
        self.browser.get(f"{self.live_server_url}{url}")
        Helpers.check_guidance_content(self, title, [c1, c2])

        # Frank creates a Yearly Budget for the first category
        cat1_id = Helpers.check_category_existence(
            self,
            text=c1,
            url="saving-categories",
            expected_to_fail=False,
        )
        amount = random.randint(1, 90000)
        month = None
        allocated_amount = random.randint(1, amount)
        Helpers.create_a_budget(
            self,
            cat_type="saving",
            cat_id=cat1_id,
            amount=amount,
            allocated_amount=allocated_amount,
            year=year,
            month=month,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        # Frank creates a Yearly Budget for the second category as well
        cat2_id = Helpers.check_category_existence(
            self,
            text=c2,
            url="saving-categories",
            expected_to_fail=False,
        )
        amount = random.randint(1, 90000)
        month = None
        allocated_amount = random.randint(1, amount)
        Helpers.create_a_budget(
            self,
            cat_type="saving",
            cat_id=cat2_id,
            amount=amount,
            allocated_amount=allocated_amount,
            year=year,
            month=month,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        # Frank cheks the guidance has changed
        url = "/"
        self.browser.get(f"{self.live_server_url}{url}")

        header_class = "text-success"
        guidance = "All Saving Categories have a budget. You're all set!"
        button_text = "See Details"
        button_url = f"/app/budgets/date/{year}"
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
        )

    # WRITE ME
    @skip
    def test_correct_guidance_for_monthly_overbudgeted_saving_budgets_in_dashboard(
        self,
    ):
        self.fail(
            "I should be checking for overbudgeted saving monthly budgets"
        )

    @skip
    def test_correct_guidance_for_yearly_overbudgeted_saving_budgets_in_dashboard(
        self,
    ):
        self.fail("I should be checking for yearly overbudgets")

    @Helpers.register_and_login
    def test_regression_gitlab_69_missing_navigation_buttons(self):
        """
        Regression test for GitLab issue #69: Missing month navigation buttons
        in Monthly Balances view
        """

        cat_name = Helpers.generate_string()
        Helpers.create_monthly_balance_category(
            self,
            category_name=cat_name,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        c_id = Helpers.check_category_existence(
            self,
            text=cat_name,
            url="monthly-balances/categories",
            expected_to_fail=False,
        )

        this_month = date.today().replace(day=1).strftime("%Y-%m")
        Helpers.create_a_monthly_balance(
            self,
            category_id=c_id,
            amount=random.randint(1, 1000),
            date=this_month,
            create_check=True,
        )

        balances_list_url = (
            f"/app/monthly-balances/multiple-creation/date/{this_month}"
        )

        # Frank navigates to the Monthly Balances view
        self.browser.get(f"{self.live_server_url}{balances_list_url}")

        # Frank can see the navigation buttons
        next_month_text = (
            date.today()
            .replace(day=1)
            .replace(month=date.today().month + 1)
            .isoformat()
        )
        prev_month_text = (
            date.today()
            .replace(day=1)
            .replace(month=date.today().month - 1)
            .isoformat()
        )
        _check_month_navigation_buttons(self, prev_month_text, next_month_text)

        # Try on each month of the year
        _try_each_month_of_the_year(self)


def _check_month_navigation_buttons(self, prev_month_text, next_month_text):
    """
    Check the presence of the month navigation buttons in the Monthly Balances view.
    """

    # Frank can see the navigation buttons
    try:
        prev_button = WebDriverWait(self.browser, MAX_WAIT_SECONDS).until(
            EC.presence_of_element_located((By.ID, "previousMonth"))
        )
        next_button = WebDriverWait(self.browser, MAX_WAIT_SECONDS).until(
            EC.presence_of_element_located((By.ID, "nextMonth"))
        )
        self.assertEqual(prev_button.text, prev_month_text)
        self.assertEqual(next_button.text, next_month_text)

        # prev_month_link = (
        #     f"/app/monthly-balances/multiple-creation/date/{prev_month_text}"
        # )
        # next_month_link = (
        #     f"/app/monthly-balances/multiple-creation/date/{next_month_text}"
        # )

    except NoSuchElementException:
        self.fail("Month navigation buttons are missing.")
    except TimeoutException:
        self.fail("Timed out waiting for month navigation buttons to appear.")


def _try_each_month_of_the_year(self):
    """
    Try to navigate to each month of the year.
    """
    for month in range(1, 13):
        if month == 1:
            prev_month_text = (
                date.today()
                .replace(day=1)
                .replace(month=12)
                .replace(year=date.today().year - 1)
                .isoformat()
            )
        else:
            prev_month_text = (
                date.today()
                .replace(day=1)
                .replace(month=month - 1)
                .isoformat()
            )

        if month == 12:
            next_month_text = (
                date.today()
                .replace(day=1)
                .replace(month=1)
                .replace(year=date.today().year + 1)
                .isoformat()
            )
        else:
            next_month_text = (
                date.today()
                .replace(day=1)
                .replace(month=month + 1)
                .isoformat()
            )
        this_month = (
            date.today().replace(day=1).replace(month=month).strftime("%Y-%m")
        )
        balances_list_url = (
            f"/app/monthly-balances/multiple-creation/date/{this_month}"
        )

        # Frank navigates to the Monthly Balances view
        self.browser.get(f"{self.live_server_url}{balances_list_url}")
        _check_month_navigation_buttons(self, prev_month_text, next_month_text)
