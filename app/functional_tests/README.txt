We have two personas for the functional tests:
- User1 is named Frank
- User2 is named Guido

If a functional test requires interaction between users, they always appear in the order: Frank, then Guido.

Both are average users with a general understanding of what this app should do. Frank is not very tech-savvy and proceeds by double-checking everything. Sometimes, he refers to the app manual to execute tasks. He really enjoys when the app works as intended.

Guido often tries to view, edit, or delete Frank's data. Like Frank, Guido is also not tech-savvy. However, in some scenarios, Guido will execute raw JavaScript to test the app's security limits.

The functional tests aim to be as explicit as possible when describing what our personas are thinking while interacting with the app. This should give developers an idea of what the average user would expect in a given scenario and provide hints on how to fix issues when a functional test fails.
