# Copyright: (c) 2019, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
import http.server
import threading

from unittest import skip

from selenium.webdriver.common.by import By

from functional_tests.helpers import get_live_server_test_case_socket_name
import functional_tests.helpers as Helpers
from functional_tests.test_parent import IntegrationTest


class AssetsIntegrationTest(IntegrationTest):
    """
    Integration test suite for Dividend related views
    """

    def setUp(self):
        # Instrument the unit tests to fail if they run for too long
        super().setUp()

    @skip
    @Helpers.register_and_login
    def test_assets_view_can_filter_by_date(
        self,
    ):
        self.fail("Write me")

    @skip
    @Helpers.register_and_login
    def test_assets_view_can_filter_by_asset_name(
        self,
    ):
        self.fail("Write me")

    @Helpers.register_and_login
    def test_gitlab_issue_65_regression_assets_view_can_filter_by_account(
        self,
    ):  # pylint: disable=R0914; # noqa
        """
        Regression test for Asset view filter does not apply when using account pulldown selector
        https://gitlab.com/micheleva/budgeteer/-/issues/65
        """
        # Frank creates an account
        acc_name = Helpers.generate_string(10)
        credit_entity_name = Helpers.generate_string(10)

        # Let's only test foreign currencies for now
        is_foreign_currency = True
        Helpers.create_an_account(
            tester=self,
            acc_name=acc_name,
            credit_entity_name=credit_entity_name,
            is_foreign_currency=is_foreign_currency,
            check_create_success_popup=True,
            check_create_failure_popup=False,
        )

        try:
            port = 13510
            self.mock_server = MockAssetsAPI(port)
            self.mock_server.daemon = True
            self.mock_server.start()

            socket_name = Helpers.get_live_server_test_case_socket_name()
            target_date = (
                "2023-07-01"  # REFACTOR ME: Is this the correct date?
            )
            mock_server_url = f"http://{socket_name}:{port}/assets"
            Helpers.import_assets_from_third_party_api(
                tester=self, apiURL=mock_server_url, target_date=target_date
            )

            expected_assets = self.mock_server.valid_data()["json_data"]
            Helpers.list_assets(
                tester=self, assets=expected_assets, target_date=target_date
            )

            # Frank creates a second account
            acc_name2 = Helpers.generate_string(10)
            while acc_name == acc_name2:
                acc_name2 = Helpers.generate_string(10)
            credit_entity_name2 = Helpers.generate_string(10)
            while credit_entity_name == credit_entity_name2:
                credit_entity_name2 = Helpers.generate_string(10)
            is_foreign_currency = True
            Helpers.create_an_account(
                tester=self,
                acc_name=acc_name2,
                credit_entity_name=credit_entity_name2,
                is_foreign_currency=is_foreign_currency,
                check_create_success_popup=True,
                check_create_failure_popup=False,
            )

            # Frank confirm the assets are filtered by account correctly:
            url = "/app/assets"
            self.browser.get(f"{self.live_server_url}{url}")

            # When Frank selects the second account from the pulldown
            # No assets are displayed, as the second account has none
            Helpers.select_data_by_account_name_from_pulldown(
                tester=self, account_name=acc_name2, expect_no_data=True
            )

        except Exception as e:
            self.mock_server.stop()

    @skip
    @Helpers.register_and_login
    def test_assets_view_can_filter_by_multiple_filters(
        self,
    ):
        self.fail("Write me")

    @skip
    @Helpers.register_and_login
    def test_assets_view_can_filter_by_multiple_filters(
        self,
    ):
        self.fail("Write me")

    @skip
    @Helpers.register_and_login
    def test_assets_view_can_delete_single_assets(
        self,
    ):
        self.fail("Write me")

    @skip
    @Helpers.register_and_login
    def test_assets_view_can_delete_multiple_assets(
        self,
    ):
        self.fail("Write me")

    @skip
    @Helpers.register_and_login
    def test_assets_filters_work_correctly_when_aggregate_by_account_is_selected(
        self,
    ):
        self.fail("Write me: I am not working atm")

    # Write me
    @skip
    def test_import_assets_view_has_a_popup_with_the_correct_format(
        self,
    ):  # pylint: disable=R0914; # noqa
        pass


class MockAssetsAPI(threading.Thread):
    """
    Simple HTTP server to serve fake data
    """

    # Add argument for port number, if none is provided, use 13505
    def __init__(self, port=13505):
        super().__init__()
        self.server = None
        self.port = port

    def run(self):
        class MockServerHandler(http.server.SimpleHTTPRequestHandler):
            def end_headers(self):
                self.send_header("Access-Control-Allow-Origin", "*")
                super().end_headers()

            def do_GET(self):
                if self.path.endswith(".json"):
                    self.send_response(200)
                    self.send_header("Content-type", "application/json")
                    self.end_headers()
                    if "malformed_data_only" in self.path:
                        fake_data = MockAssetsAPI.invalid_data()["string_data"]
                    elif "mixed_data" in self.path:
                        fake_data = MockAssetsAPI.partially_invalid_data()[
                            "string_data"
                        ]
                    else:
                        fake_data = MockAssetsAPI.valid_data()["string_data"]
                    self.wfile.write(fake_data.encode())
                else:
                    # TODO, should we return an empty array instead?
                    self.send_response(404)
                    self.end_headers()
                    self.wfile.write(b"Not Found")

        socket_name = get_live_server_test_case_socket_name()
        self.server = http.server.ThreadingHTTPServer(
            (socket_name, self.port), MockServerHandler
        )
        self.server.serve_forever()

    def stop(self):
        if self.server:
            self.server.shutdown()
            self.server.server_close()

    @classmethod
    def invalid_data(cls):
        string_data = """[
          {"ticker": "AAA",
           "amount": "A string is not a number",
           "currency": null,
           "sum_current": {"foreign": 420.69, "local": 45013.83},
           "sum_bought": {"foreign": 4206.9, "local": 450138},
           "sum_net": {"foreign": -3786.21, "local": -405124.17},
           "rate": 160.1
          },
          {"ticker": "BBB",
           "extra_field": "I am not required.. actually I should not be here",
           "amount": 42,
           "currency": "JPY",
           "sum_current": {"foreign": 0.0, "local": 420000},
           "sum_bought": {"foreign": 0.0, "local": 413100},
           "sum_net": {"foreign": 0.0, "local": 6900},
           "rate": 110
          },
          {"ticker": "CCC",
           "sum_current": {"foreign": 12.00, "local": 1200},
           "sum_bought": {"foreign": 11.59, "local": 1159},
           "sum_net": {"foreign": 0.41, "local": 41},
           "rate": 110
          },
          {"ticker": "DDD",
           "sum_current": {"asd": 12.00, "lol": 1200},
           "sum_bought": {"rotfl": 11.59, "meh": 1159},
           "sum_net": {"i_am_not_correct": 0.41, "local": 41},
           "rate": 97
          },
          {"ticker": "EEE",
           "amount": {},
           "currency": {"foreign": 0.41, "local": 41},
           "sum_current": {"asd": 12.00, "lol": 1200},
           "sum_bought": {"rotfl": 11.59, "meh": 1159},
           "sum_net": {"foreign": 0.41, "local": 41, "extra_nested_field": 42},
           "rate": 112
          }
        ]"""
        json_data = []
        return {"string_data": string_data, "json_data": json_data}

    @classmethod
    def partially_invalid_data(cls):
        string_data = """[
          {"ticker": "AAA",
           "amount": {"foreign": {"foreign": 420.69, "local": 45013.83}, "local": 45013.83},
           "currency": "USD",
           "sum_current": {"foreign": 420.69, "local": 45013.83},
           "sum_bought": {"foreign": 4206.9, "local": 450138},
           "sum_net": {"foreign": -3786.21, "local": -405124.17}
          },
          {"ticker": "BBB",
           "amount": {"foreign": 0.0, "local": 420000},
           "currency": {"foreign": 0.0, "local": 420000},
           "sum_current": 42,
           "sum_bought": 1337,
           "sum_net": null,
           "rate": null
          },
          {"ticker": "CCC",
           "amount": {"foreign": 0.0, "local": 420000},
           "currency": "USD",
           "sum_current": {"foreign": 420.69, "local": 45013.83},
           "sum_bought": {"foreign": 4206.9, "local": 450138},
           "sum_net": {"foreign": -3786.21, "local": -405124.17},
           "rate": "i am a string, not a number"
          },
          {
          },
          {
           "ticker": "DDD",
           "extra_field": {"nested_extra_field": null},
           "sum_current": 42,
           "sum_bought": 1337,
           "sum_net": null,
           "rate": 42
          },
          {"ticker": "EEE",
           "amount": 4,
           "currency": "USD",
           "sum_current": {"foreign": 12.00, "local": 1200},
           "sum_bought": {"foreign": 11.59, "local": 1159},
           "sum_net": {"foreign": 0.41, "local": 41},
           "rate": 100
          },
          {
           "ticker": "FFF",
           "amount": 1337,
           "currency": "USD",
           "sum_current": {"foreign": 420.69, "local": 45013.83},
           "sum_bought": {"foreign": 4206.9, "local": 450138},
           "sum_net": {"foreign": -3786.21, "local": -405124.17}
          }
        ]"""
        json_data = [
            {
                "ticker": "EEE",
                "amount": 4,
                "currency": "USD",
                "sum_current": {"USD": 12.00, "JPY": 1200},
                "sum_bought": {"USD": 11.59, "JPY": 1159},
                "sum_net": {"USD": 0.41, "JPY": 41},
                "rate": 100,
            }
        ]
        return {"string_data": string_data, "json_data": json_data}

    @classmethod
    def valid_data(cls):
        string_data = """[
          {"ticker": "AAA",
           "amount": 1337,
           "currency": "USD",
           "sum_current": {"foreign": 420.69, "local": 45013.83},
           "sum_bought": {"foreign": 4206.9, "local": 450138},
           "sum_net": {"foreign": -3786.21, "local": -405124.17},
           "rate": 107
          },
          {"ticker": "BBB",
           "amount": 42,
           "currency": "JPY",
           "sum_current": {"foreign": 0.0, "local": 420000},
           "sum_bought": {"foreign": 0.0, "local": 413100},
           "sum_net": {"foreign": 0.0, "local": 6900},
           "rate": 110
          },
          {"ticker": "CCC",
           "amount": 4,
           "currency": "USD",
           "sum_current": {"foreign": 12.00, "local": 1200},
           "sum_bought": {"foreign": 11.59, "local": 1159},
           "sum_net": {"foreign": 0.41, "local": 41},
           "rate" :100
          }
        ]"""
        json_data = [
            {
                "ticker": "AAA",
                "amount": 1337,
                "currency": "USD",
                "sum_current": {"USD": 420.69, "JPY": 45013.83},
                "sum_bought": {"USD": 4206.9, "JPY": 450138},
                "sum_net": {"USD": -3786.21, "JPY": -405124.17},
                "rate": 107,
            },
            {
                "ticker": "BBB",
                "amount": 42,
                "currency": "JPY",
                "sum_current": {"USD": 0.0, "JPY": 420000},
                "sum_bought": {"USD": 0.0, "JPY": 413100},
                "sum_net": {"USD": 0.0, "JPY": 6900},
                # This is going to be finalized around v1.0. i.e. even local
                # currencies should have the correct rate for the foreign_currency
                # amount
                "rate": 110,
            },
            {
                "ticker": "CCC",
                "amount": 4,
                "currency": "USD",
                "sum_current": {"USD": 12.00, "JPY": 1200},
                "sum_bought": {"USD": 11.59, "JPY": 1159},
                "sum_net": {"USD": 0.41, "JPY": 41},
                "rate": 100,
            },
        ]
        return {"string_data": string_data, "json_data": json_data}


class ThirdPartyAPIIntegrationTest(IntegrationTest):
    """Integration test for third party APIs"""

    def setUp(self):
        # Instrument the unit tests to fail if they run for too long
        super().setUp()
        self.mock_server = MockAssetsAPI()
        self.mock_server.daemon = True
        self.mock_server.start()

    def tearDown(self):
        self.mock_server.stop()

    @Helpers.register_and_login
    def test_can_import_and_list_correctly_formatted_assets_api_result(self):
        acc_name = Helpers.generate_string(10)
        credit_entity_name = Helpers.generate_string(10)
        # Let's only test foreign currencies for now
        is_foreign_currency = True
        Helpers.create_an_account(
            tester=self,
            acc_name=acc_name,
            credit_entity_name=credit_entity_name,
            is_foreign_currency=is_foreign_currency,
            check_create_success_popup=True,
            check_create_failure_popup=False,
        )

        socket_name = get_live_server_test_case_socket_name()
        target_date = "2023-07-01"  # REFACTOR ME: Is this the correct date?
        url = f"http://{socket_name}:13505/assets"
        Helpers.import_assets_from_third_party_api(
            tester=self, apiURL=url, target_date=target_date
        )
        expected_assets = self.mock_server.valid_data()["json_data"]
        Helpers.list_assets(
            tester=self, assets=expected_assets, target_date=target_date
        )

    @Helpers.register_and_login
    def test_importing_correct_and_malformed_data_will_properly_show_errors(
        self,
    ):
        acc_name = "mixed_data"
        credit_entity_name = Helpers.generate_string(10)
        # Let's only test foreign currencies for now
        is_foreign_currency = True
        Helpers.create_an_account(
            tester=self,
            acc_name=acc_name,
            credit_entity_name=credit_entity_name,
            is_foreign_currency=is_foreign_currency,
            check_create_success_popup=True,
            check_create_failure_popup=False,
        )

        socket_name = get_live_server_test_case_socket_name()
        target_date = "2023-07-01"
        url = f"http://{socket_name}:13505/assets"
        Helpers.import_assets_from_third_party_api(
            tester=self,
            apiURL=url,
            target_date=target_date,
            check_success_text=False,
        )

        alerts = self.browser.find_elements(By.CLASS_NAME, "alert")
        self.assertTrue(len(alerts), 2)

        expected_assets = self.mock_server.partially_invalid_data()[
            "json_data"
        ]
        alerts = self.browser.find_elements(By.CLASS_NAME, "alert")
        self.assertTrue(len(alerts), 1)
        results_alert_text = alerts[0].text

        substrings_to_check = [
            f"1/1 accounts have data for {target_date}",
            # NOTE: comment out the following line to make the test less flaky
            # when running the test with --parallel.
            # i.e. the request to create the asset might take a while to come
            # back, and being reflected in the UI "Created 1/7 assets entries",
            "returned 6 malformed data point(s)",
        ]
        for substring in substrings_to_check:
            self.assertIn(substring, results_alert_text)

        Helpers.list_assets(
            tester=self, assets=expected_assets, target_date=target_date
        )

    @Helpers.register_and_login
    def test_importing_only_malformed_data_will_properly_show_errors(self):
        acc_name = "malformed_data_only"
        credit_entity_name = Helpers.generate_string(10)
        # Let's only test foreign currencies for now
        is_foreign_currency = True
        Helpers.create_an_account(
            tester=self,
            acc_name=acc_name,
            credit_entity_name=credit_entity_name,
            is_foreign_currency=is_foreign_currency,
            check_create_success_popup=True,
            check_create_failure_popup=False,
        )

        socket_name = get_live_server_test_case_socket_name()
        target_date = "2023-07-01"
        url = f"http://{socket_name}:13505/assets/"
        Helpers.import_assets_from_third_party_api(
            tester=self,
            apiURL=url,
            target_date=target_date,
            check_success_text=False,
        )

        alerts = self.browser.find_elements(By.CLASS_NAME, "alert")

        # No assets were created, so no error should be returned by the server
        self.assertTrue(len(alerts), 1)
        results_alert_text = alerts[0].text
        self.assertNotIn("asset(s) creation failed.", results_alert_text)

        substrings_to_check = [
            f"1/1 accounts have data for {target_date}",
            "Created 0/5 assets entries",
            "returned 5 malformed data point(s)",
        ]
        results_alert_text = alerts[0].text
        for substring in substrings_to_check:
            self.assertIn(substring, results_alert_text)
