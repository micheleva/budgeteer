# Copyright: (c) 2023, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from unittest import skip

from django.urls import reverse

from selenium.webdriver.common.by import By

import functional_tests.helpers as Helpers
from functional_tests.test_parent import IntegrationTest

# This is the maximum time that Frank and Guido are able to wait (in seconds)
# before giving up and throw a tantrum
MAX_WAIT_SECONDS = 2


class IncomeCategoriesIntegrationTest(IntegrationTest):
    """
    Integration test suite for IncomeCategories related views
    """

    def setUp(self):
        # Instrument the unit tests to fail if they run for too long
        super().setUp()

    # @Helpers.register_and_login
    @skip  # Rewrite me for the the react frontend
    def test_cant_create_an_empty_income_category(self):
        text = ""
        Helpers.create_a_category(
            self,
            category_name=text,
            is_income=True,
            create_check=False,
            wait_for_reload=False,
        )

        # Frank notices his browser his forcing him to input something
        Helpers.wait_for_required_input(self, "id_text")

        url = reverse("budgets:categories")
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank does not follows the browser instruction, and stares at
        # the screen in disdain. He thought he could create an empty Income
        # Category

        table = self.browser.find_element(By.ID, "id_categories")
        Helpers.assert_text_is_not_inside_table(self, text, table)

    @Helpers.register_and_login
    def test_can_create_multiple_income_categories(self):
        # Frank can create a category to log his wages
        c = Helpers.generate_string()
        Helpers.create_an_income_category(self, c, check_creation_popup=True)

        # Frank actually create the same category... but the app prevents it!
        # All Frank can say is "Nice! I am a birdbrain, thank you app!"
        Helpers.create_an_income_category(self, c, check_failure_popup=True)

    @Helpers.register_and_login
    def test_diff_users_can_create_income_categories_with_the_same_name(self):
        # Frank can create a category to log his expenses
        cat_name = Helpers.generate_string()

        Helpers.create_an_income_category(
            self, cat_name, check_creation_popup=True
        )
        Helpers.logout_user(self)

        # Guido can create a category with the same name
        Helpers.create_user(self)
        Helpers.create_an_income_category(
            self, cat_name, check_creation_popup=True
        )
        Helpers.logout_user(self)

    @skip
    @Helpers.register_and_login
    def test_cant_create_duplicate_income_categories(self):
        # Frank can create a category to log expenses related to his rent
        Helpers.create_a_category(self, "Rent", is_income=True)

        # Frank is not paying attention to what he is doing, and he tries
        # to create the same category
        Helpers.create_a_category(
            self, "Rent", is_income=True, midway_check=True
        )

    @skip  # Rewrite me for the the react frontend
    def test_users_cant_see_other_users_income_categories(self):
        # Frank can create a category to log his incomes
        cat_name = Helpers.generate_string()

        Helpers.create_a_category(self, category_name=cat_name, is_income=True)
        Helpers.logout_user(self)

        # Guido can not see Frank's income category
        Helpers.create_user(self)

        Helpers.visit_and_verify_categories(
            self,
            cat_name,
            is_income=True,
            should_exist=False,
            is_balance=False,
        )

        Helpers.logout_user(self)

    # Functional test for Income categoriy entity, on the react frontend
    # side of the app
    @Helpers.register_and_login
    def test_can_create_and_retrieve_income_categories(self):
        """Check that creation and retrival of categories works correctly"""
        # Frank creates an income category to record his wage
        cat_name = Helpers.generate_string()

        Helpers.create_an_income_category(
            self, cat_name, check_creation_popup=True
        )

        _ = Helpers.check_category_existence(
            self,
            text=cat_name,
            url="income-categories",
            expected_to_fail=False,
        )

        # Frank does not trust the API, and he wants to check if a
        # second Income Categories will appear in his next call or not
        cat_name_2 = Helpers.generate_string()

        Helpers.create_an_income_category(
            self, cat_name_2, check_creation_popup=True
        )
        _ = Helpers.check_category_existence(
            self,
            text=cat_name_2,
            url="income-categories",
            expected_to_fail=False,
        )

    @Helpers.register_and_login
    def test_different_users_can_have_two_income_categories_with_the_same_name(
        self,
    ):
        cat_name = Helpers.generate_string()
        Helpers.create_an_income_category(
            self, cat_name, check_creation_popup=True
        )

        _ = Helpers.check_category_existence(
            self,
            text=cat_name,
            url="income-categories",
            expected_to_fail=False,
        )

        # Frank logs off as he's done with today's tasks
        Helpers.logout_user(self)

        # Guido feels like trying to break the app
        _ = Helpers.create_user(self)

        # Guido has found Frank's Income Category's name.
        # Guido wants to imitate Frank, and create an Income Category with
        # the same name
        Helpers.create_an_income_category(
            self, cat_name, check_creation_popup=True
        )

        _ = Helpers.check_category_existence(
            self,
            text=cat_name,
            url="income-categories",
            expected_to_fail=False,
        )

    @Helpers.register_and_login
    def test_users_can_edit_categories(self):
        cat_name = Helpers.generate_string()

        # Frank creates a category using the React app. He likes to download
        # a big blob of javascript
        Helpers.create_an_income_category(
            self, cat_name, check_creation_popup=True
        )

        cat_id = Helpers.check_category_existence(
            self,
            text=cat_name,
            url="income-categories",
            expected_to_fail=False,
        )

        new_cat_name = Helpers.generate_string()
        Helpers.edit_an_income_category(
            self,
            cat_id,
            new_cat_name,
            check_edit_success_popup=True,
            check_edit_failure_popup=False,
            check_inability_to_load_category=False,
        )

        _ = Helpers.check_category_existence(
            self,
            text=new_cat_name,
            url="income-categories",
            expected_to_fail=False,
        )

    @Helpers.register_and_login
    def test_users_can_not_edit_categories_to_have_duplicated_text(
        self,
    ):
        cat_name = Helpers.generate_string()
        Helpers.create_an_income_category(
            self, cat_name, check_creation_popup=True
        )

        # Frank creates a second category: he heard that the developer of the
        # app fixed a possible bug!
        sec_cat_name = Helpers.generate_string()
        while sec_cat_name == cat_name:
            sec_cat_name = Helpers.generate_string()

        Helpers.create_an_income_category(
            self, sec_cat_name, check_creation_popup=True
        )

        cat_id = Helpers.check_income_category_existence(
            self,
            text=cat_name,
            expected_to_fail=False,
        )

        _ = Helpers.check_income_category_existence(
            self,
            text=sec_cat_name,
            expected_to_fail=False,
        )

        url = f"/app/incomez-category/{cat_id}"
        self.browser.get(f"{self.live_server_url}{url}")

        # Frank tries to use the update the first income category text to
        # match the second one's
        # Franks immediately understand that the developers thought
        # everything through: having multiple income categories with the
        # same text is not allowed!
        err_msg = (
            "An Income category with this text already exists for this user."
        )
        Helpers.edit_an_income_category(
            self,
            cat_id,
            sec_cat_name,
            check_edit_success_popup=False,
            check_edit_failure_popup=True,
            check_inability_to_load_category=False,
            expected_error_message=err_msg,
        )

        # Frank does not really trust that his changes have been rejected.
        # What if the frontend developer was lazy, and the code was not tested?
        _ = Helpers.check_income_category_existence(
            self,
            text=cat_name,
            expected_to_fail=False,
        )

        _ = Helpers.check_income_category_existence(
            self,
            text=sec_cat_name,
            expected_to_fail=False,
        )
        # Frank is happy with the results: the frontend team seems to have
        # implemented the feature according to the specs

        # The first income category text has not been replaced, and it is not
        # the same as the second category

    # WRITE ME
    @skip
    def test_regression_216_can_resubmit_a_form_after_a_failed_submission(
        self,
    ):
        """
        WRITE ME
        """
        pass
