# Copyright: (c) 2019, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
import random

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

import functional_tests.helpers as Helpers
from functional_tests.test_parent import IntegrationTest

from unittest import skip

# IMPLEMENT ME: in each test confirm react is loaded, and no error in JS
# for this we need to use Chromium driver, update to Selenium 4
# and check for the networking tab XHR requests    goal_name = Helpers.generate_string()

# Maximum amount of seconds to wait for an element to appear
MAX_WAIT_SECONDS = 2


class GoalsIntegrationTest(IntegrationTest):
    """
    Integration test suite for Goal related views
    """

    def setUp(self):
        # Instrument the unit tests to fail if they run for too long
        super().setUp()

    @Helpers.register_and_login
    def test_create_and_retrieve_goals(self):
        check_creation_popup = True
        check_successful_creation = True
        _ = Helpers.create_a_goal_and_return_all_its_data(
            self,
            check_creation_popup=check_creation_popup,
            check_successful_creation=check_successful_creation,
        )

    @Helpers.register_and_login
    def test_create_and_edit_goals(self):
        check_creation_popup = True
        check_successful_creation = True
        goal = Helpers.create_a_goal_and_return_all_its_data(
            self,
            check_creation_popup=check_creation_popup,
            check_successful_creation=check_successful_creation,
        )

        # New values
        is_archived = not goal["is_archived"]
        # NOTE: we increase the number to make sure we actually change the amount
        amount = goal["amount"] + random.randint(1, 42)
        text = Helpers.generate_string()
        note = Helpers.generate_string()

        check_successful_edit_popup = True
        check_successful_edit = True
        Helpers.edit_a_goal(
            self,
            old_goal_obj=goal,
            goal_id=goal["id"],
            is_archived=is_archived,
            amount=amount,
            text=text,
            note=note,
            check_successful_edit_popup=check_successful_edit_popup,
            check_successful_edit=check_successful_edit,
        )

    @Helpers.register_and_login
    def test_create_and_delete_goals(self):
        check_creation_popup = True
        check_successful_creation = True
        goal = Helpers.create_a_goal_and_return_all_its_data(
            self,
            check_creation_popup=check_creation_popup,
            check_successful_creation=check_successful_creation,
        )

        goal2 = Helpers.create_a_goal_and_return_all_its_data(
            self,
            check_creation_popup=check_creation_popup,
            check_successful_creation=check_successful_creation,
        )

        # As the goals list will display a creation guidance page in case no
        # goals are there, we need to check both patters:
        # 1. The goal is deleted, and nothing is shown in the goals list
        # 2. The goal is deleted, and the remaining goal is shown in the goals list

        check_deletion_popup = False
        Helpers.delete_goal(
            self, goal_id=goal["id"], check_deletion_popup=check_deletion_popup
        )

        # The goal is deleted, and this goal is gone for good!
        Helpers.visit_and_verify_goal(
            self,
            data=goal,
            expected_to_fail=True,
        )
        Helpers.delete_goal(
            self,
            goal_id=goal2["id"],
            check_deletion_popup=check_deletion_popup,
        )

        # The goal is deleted, and nothing at all is shown in the goals list
        Helpers.visit_and_verify_goal(
            self,
            data=goal2,
            expected_to_fail=True,
        )

    @Helpers.register_and_login
    def test_can_not_create_malformed_goals(self):
        # NOTE: we hard code the URI because Django has no idea of React routes
        # (i.e. react internal virtual routes, not API endpoints)
        url = "/app/goals/creation"

        # Frank creates a goal without typing anything in the text or
        #  note fields
        self.browser.get(f"{self.live_server_url}{url}")

        try:
            WebDriverWait(self.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located((By.ID, "text_field"))
            )
        except TimeoutException:
            self.fail("Goals creation UI did not load timely. Fail!")

        text_inputbox = self.browser.find_element(By.ID, "text_field")
        amount_inputbox = self.browser.find_element(By.ID, "amount_field")
        note_inputbox = self.browser.find_element(By.ID, "note_field")
        # NOTE: as soon as we have a second button, this will fail hard
        submit_button = self.browser.find_element(By.TAG_NAME, "button")
        # NOTE: ignore the checkbox as it is not a required field

        submit_button.click()

        # Frank is happy that the app is stopping him from creating an
        # empty goal
        try:
            spans = self.browser.find_element(
                By.TAG_NAME, "form"
            ).find_elements(By.TAG_NAME, "span")
            self.assertTrue(
                all("This field is required." == span.text for span in spans),
                "Not all required fields threw an error!",
            )
        except NoSuchElementException:
            self.fail(
                "The form or the span elements were not there. Did you refact"
                "or the react frontend create goal view?"
            )

        # Frank tries to input a string in the amount field, and see if the
        # pp breaks
        text = Helpers.generate_string()
        note = Helpers.generate_string()

        amount_inputbox.send_keys(text)
        text_inputbox.send_keys(text)
        note_inputbox.send_keys(note)
        submit_button.click()

        # Frank very likes that no alert is shown: Frank knows that his
        # browser should enforce this before the request is actually sent
        # (Frank has a general understanding of how web apps work)
        try:
            WebDriverWait(self.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located((By.CLASS_NAME, "alert-danger"))
            )
            self.fail(
                "Failure popup was displayed. The browser did not enforced the"
                ' "number" property of the field! Did you refactor react'
                " goal view??"
            )
        except TimeoutException:
            # Frank is happy that his browser is smart, and it is preventing him from inputting
            # garbage in the app
            pass

    @Helpers.register_and_login
    def test_can_not_create_goals_with_same_name(self):
        check_creation_popup = True
        check_successful_creation = False
        goal = Helpers.create_a_goal_and_return_all_its_data(
            self, check_successful_creation=check_successful_creation
        )

        check_creation_popup = False
        check_successful_creation = False
        Helpers.create_a_goal(
            tester=self,
            is_archived=goal["is_archived"],
            amount=goal["amount"],
            text=goal["text"],
            note=goal["note"],
            check_creation_popup=check_creation_popup,
            check_successful_creation=check_successful_creation,
        )

        # Frank appreciates that the app prevents goals with the same name to be created
        # "That would be very confusing!" Frank says aloud
        try:
            WebDriverWait(self.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located((By.CLASS_NAME, "alert-danger"))
            )
        except NoSuchElementException:
            self.fail("Failure popup was not displayed. Fail!")

        form = self.browser.find_element(By.TAG_NAME, "form")
        self.assertIn(
            "A Goal with the same text already exists for this user", form.text
        )

    # TODO:write me after we change the unique constraints on the goal model
    @Helpers.register_and_login
    def test_different_user_can_create_goals_with_the_same_name(self):
        check_creation_popup = True
        check_successful_creation = False
        goal = Helpers.create_a_goal_and_return_all_its_data(
            self, check_successful_creation=check_successful_creation
        )

        # Frank appreciates that the app notifies him of the successful creation
        try:
            WebDriverWait(self.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located(
                    (By.CLASS_NAME, "alert-success")
                )
            )
        except NoSuchElementException:
            self.fail("Success popup was not displayed. Fail!")

        Helpers.logout_user(self)

        # Guido has seen Frank's goal name. Guido decides to break the app by creating
        # a goal with the same name!
        Helpers.create_user(self)

        check_creation_popup = True
        check_successful_creation = False
        Helpers.create_a_goal(
            tester=self,
            is_archived=goal["is_archived"],
            amount=goal["amount"],
            text=goal["text"],
            note=goal["note"],
            check_creation_popup=check_creation_popup,
            check_successful_creation=check_successful_creation,
        )

    # WRITE ME
    def test_can_rename_goals_to_have_conflicting_names_among_different_users(
        self,
    ):
        # Special goals are still only existing in the .env file, no API for them.
        # Hence no need for this test yet
        print(
            "IMPLEMENT ME: test_can_rename_goals_to_have_conflicting_names_"
            "among_different_users",
            flush=True,
        )
        pass

    @Helpers.register_and_login
    def test_users_can_not_see_other_users_goals(self):
        print(
            "test_users_can_not_see_other_users_goals is calling the API dire"
            "ctly. This makes the test flaky. Address it to close #46"
        )
        check_creation_popup = True
        check_successful_creation = True
        goal = Helpers.create_a_goal_and_return_all_its_data(
            self,
            check_creation_popup=check_creation_popup,
            check_successful_creation=check_successful_creation,
        )

        Helpers.logout_user(self)

        # Guido can not see Frank's goal
        Helpers.create_user(self)

        # Guido visits the goals page
        # NOTE: we hard code the URI because Django has no idea of React routes
        # (i.e. react internal virtual routes, not API endpoints)
        url = f"/app/goal-edit/{goal['id']}"
        self.browser.get(f"{self.live_server_url}{url}")
        print(
            "IMPLEMENT ME: look for the goal in the goals list as well, not just the edit page",
            flush=True,
        )

        # Guido can not see Frank's goal
        try:
            data = goal
            other_user_goal_id = Helpers.visit_and_verify_goal(
                self,
                data=data,
                expected_to_fail=True,
            )
            if other_user_goal_id != -1:
                self.fail(
                    f"We can see the other user goal id, which is {other_user_goal_id}"
                )
        except TimeoutException:
            # If there are no 'ul', and it timesout, it means this Guido page contains no 'ul',
            # hence the test has passed
            pass

    @skip
    def test_users_cant_edit_other_users_goals(self):
        """
        We do not need this test yet, as we already check that users can not see other users goals.
        Also, if users could edit other users' goals using the API, we should catch that in the API tests
        (and not in the frontend tests).
        """
        pass

    @Helpers.register_and_login
    def test_can_not_edit_goal_to_be_malformed(self):
        check_creation_popup = True
        check_successful_creation = True

        goal = Helpers.create_a_goal_and_return_all_its_data(
            self,
            check_creation_popup=check_creation_popup,
            check_successful_creation=check_successful_creation,
        )

        # Frank tries to edit the created goal, and give it a long title (i.e. text field)
        # as per the app manual, this should not be allowed
        too_long_text = Helpers.generate_string(21)
        Helpers.edit_a_goal(
            self,
            old_goal_obj=goal,
            goal_id=goal["id"],
            is_archived=goal["is_archived"],
            amount=goal["amount"],
            text=too_long_text,
            note=goal["note"],
            check_successful_edit_popup=False,
            check_successful_edit=False,
        )

        # Frank is happy that the app is explaning clearly what is wrong
        try:
            spans = self.browser.find_element(
                By.TAG_NAME, "form"
            ).find_elements(By.TAG_NAME, "span")
            self.assertTrue(
                all("Max 20 chars" == span.text for span in spans),
                "Text field did not throw an error!",
            )
        except NoSuchElementException:
            self.fail(
                "The form or the span elements were not there. Did you refactor the react frontend edit goal view?"
            )

        # Frank tries to edit the created goal, and makes sure the node input field is filled with
        # a long input. As per the app manual, this should not be allowed
        check_successful_edit_popup = False
        check_successful_edit = False
        too_long_note = Helpers.generate_string(51)
        Helpers.edit_a_goal(
            self,
            old_goal_obj=goal,
            goal_id=goal["id"],
            is_archived=goal["is_archived"],
            amount=goal["amount"],
            text=goal["text"],
            note=too_long_note,
            check_successful_edit_popup=check_successful_edit_popup,
            check_successful_edit=check_successful_edit,
        )

        # Frank is happy that the app is explaning clearly what is wrong
        try:
            spans = self.browser.find_element(
                By.TAG_NAME, "form"
            ).find_elements(By.TAG_NAME, "span")
            self.assertTrue(
                all("Max 50 chars" == span.text for span in spans),
                "Note field did not throw an error!",
            )
        except NoSuchElementException:
            self.fail(
                "The form or the span elements were not there. Did you refactor the react frontend edit goal view?"
            )

        # Frank tries to edit the created goal, writing some text in the amount field.
        # Frank murmurs that a properly written app should throw an error, and alert the user that such thing should not be done!
        check_successful_edit_popup = False
        check_successful_edit = False
        text_in_amount_field = Helpers.generate_string(20)
        Helpers.edit_a_goal(
            self,
            old_goal_obj=goal,
            goal_id=goal["id"],
            is_archived=goal["is_archived"],
            amount=text_in_amount_field,
            text=goal["text"],
            note=goal["note"],
            check_successful_edit_popup=check_successful_edit_popup,
            check_successful_edit=check_successful_edit,
        )

        # Frank very likes that no alert is shown: Frank knows that his browser should enforce
        # this before the request is actually sent
        # (Frank has a general understanding of how web apps work)
        try:
            WebDriverWait(self.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located((By.CLASS_NAME, "alert-danger"))
            )
            self.fail(
                'Failure popup was displayed. The browser did not enforced the "number" property of the field! Did you refactor react goal view??'
            )
        except TimeoutException:
            # Frank is happy that his browser is smart
            # and it is preventing him from inputting the wrong data type in the fields
            pass

    @Helpers.register_and_login
    def test_can_not_rename_goals_to_have_conflicting_names(self):
        # Frank creates a goal
        check_creation_popup = True
        check_successful_creation = True
        first_goal = Helpers.create_a_goal_and_return_all_its_data(
            self,
            check_creation_popup=check_creation_popup,
            check_successful_creation=check_successful_creation,
        )

        # Frank then goes and creates a second goal: he has so many bright dreams for the future,
        #  and he want to be sure to have a goal, in this app, for each dream he has.
        # This wat it will be easy to work towards them!
        check_creation_popup = True
        check_successful_creation = True
        second_goal = Helpers.create_a_goal_and_return_all_its_data(
            self, check_successful_creation=check_successful_creation
        )

        # Frank has some esitation. Maybe the second goal's should be more descriptive:
        #    Frank decides to edit the second goal's title (i.e. text field)
        # Frank accidentally types in the name of the first goal
        second_goal_new_text = first_goal["text"]

        # Frank notices that the app has thrown an error, and he realizes how silly he is:
        #    there is already a goal with said name!
        check_successful_edit_popup = False
        check_successful_edit = False
        text_in_amount_field = Helpers.generate_string(20)
        Helpers.edit_a_goal(
            self,
            old_goal_obj=second_goal,
            goal_id=second_goal["id"],
            is_archived=second_goal["is_archived"],
            amount=second_goal["amount"],
            text=first_goal["text"],
            note=second_goal["note"],
            check_successful_edit_popup=check_successful_edit_popup,
            check_successful_edit=check_successful_edit,
        )

        # Frank is happy that the app notified him before saving such change
        # as he was going to have serious hedaches figuring out which goal
        # was for what, as they would have ended up having the same name!
        try:
            WebDriverWait(self.browser, MAX_WAIT_SECONDS).until(
                EC.presence_of_element_located((By.CLASS_NAME, "alert-danger"))
            )
        except NoSuchElementException:
            self.fail("Failure popup was not displayed. Fail!")

        # Frank is happy that the app is explaning clearly what is wrong
        try:
            spans = self.browser.find_element(
                By.TAG_NAME, "form"
            ).find_elements(By.TAG_NAME, "span")
            self.assertTrue(
                all(
                    "A Goal with the same text already exists for this user."
                    == span.text
                    for span in spans
                ),
                f"Text field did not throw an error!",
            )
        except NoSuchElementException:
            msg = "The form or the span elements were not there. Did you refactor the react frontend edit goal view?"
            self.fail(msg)

    @skip
    def test_users_cant_delete_other_users_goals(self):
        """
        We do not need this test yet, as we already check that users can not see other users goals.
        Also, if users could delete other users' goals using the API, we should catch that in the API tests
        (and not in the frontend tests).
        """
        pass

    # WRITE ME
    @skip
    def test_regression_216_can_resubmit_a_form_after_a_failed_submission(
        self,
    ):
        """
        WRITE ME
        """
        pass
