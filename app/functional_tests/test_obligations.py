# Copyright: (c) 2019, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from datetime import date
from datetime import timedelta
import random

import functional_tests.helpers as Helpers
from functional_tests.test_parent import IntegrationTest

# Maximum amount of seconds to wait for an element to appear
MAX_WAIT_SECONDS = 2


class ObligationsIntegrationTest(IntegrationTest):
    """
    Integration test suite for Obligation related views
    """

    def setUp(self):
        # Instrument the unit tests to fail if they run for too long
        super().setUp()

    def create_a_fixed_obligation(
        self, check_creation_popup=True, check_successful_creation=True
    ):
        name = Helpers.generate_string()
        total_amount = random.randint(32000, 90000)
        obligation_type = "regular"
        monthly_payment = random.randint(1, 32000)
        start_date = date.today() + timedelta(days=random.randint(42, 365))
        # Make sure the randomly generated end date is after the start date
        end_date_str = (
            start_date + timedelta(days=random.randint(42, 365))
        ).isoformat()
        start_date_str = start_date.isoformat()
        payment_day = random.randint(1, 28)

        ob_id = Helpers.create_a_fixed_obligation(
            self,
            name=name,
            total_amount=total_amount,
            obligation_type=obligation_type,
            monthly_payment=monthly_payment,
            start_date=start_date_str,
            end_date=end_date_str,
            payment_day=payment_day,
            check_creation_popup=check_creation_popup,
            check_successful_creation=check_successful_creation,
        )

        return {
            "id": ob_id,
            "name": name,
            "total_amount": total_amount,
            "obligation_type": obligation_type,
            "monthly_payment": monthly_payment,
            "start_date": start_date_str,
            "end_date": end_date_str,
            "payment_day": payment_day,
        }

    @Helpers.register_and_login
    def test_create_and_retrieve_fixed_obligations(self):
        check_creation_popup = True
        check_successful_creation = True
        _ = self.create_a_fixed_obligation(
            check_creation_popup=check_creation_popup,
            check_successful_creation=check_successful_creation,
        )

    @Helpers.register_and_login
    def test_cant_create_malformed_fixed_obligations(self):
        """
        Check that the users can't create fixed obligations with invalid values
        i.e. confirm the proper validation of the form
        """
        # Invalid name
        name = ""
        total_amount = random.randint(32000, 90000)
        obligation_type = "regular"
        monthly_payment = random.randint(1, 32000)
        start_date = date.today() + timedelta(days=random.randint(42, 365))
        # Make sure the randomly generated end date is after the start date
        end_date_str = (
            start_date + timedelta(days=random.randint(42, 365))
        ).isoformat()
        start_date_str = start_date.isoformat()
        payment_day = random.randint(1, 28)

        Helpers.create_invalid_fixed_obligation(
            self,
            name=name,
            total_amount=total_amount,
            obligation_type=obligation_type,
            monthly_payment=monthly_payment,
            start_date_str=start_date_str,
            end_date_str=end_date_str,
            payment_day=payment_day,
            element_path='input[id="name_field"] + span',
            error_message="This field is required.",
        )

        # Invalid total amount
        total_amount = -1
        Helpers.create_invalid_fixed_obligation(
            self,
            name=name,
            total_amount=total_amount,
            obligation_type=obligation_type,
            monthly_payment=monthly_payment,
            start_date_str=start_date_str,
            end_date_str=end_date_str,
            payment_day=payment_day,
            element_path='input[id="total_amount_field"] + span',
            error_message="Total Amount should be more than 0",
        )

        # Invalid monthly payment
        monthly_payment = -1
        Helpers.create_invalid_fixed_obligation(
            self,
            name=name,
            total_amount=total_amount,
            obligation_type=obligation_type,
            monthly_payment=monthly_payment,
            start_date_str=start_date_str,
            end_date_str=end_date_str,
            payment_day=payment_day,
            element_path='input[id="monthly_payment_field"] + span',
            error_message="Monthly Payment Amount should be more than 0",
        )

        # Invalid start date
        start_date_str = "invalid"
        Helpers.create_invalid_fixed_obligation(
            self,
            name=name,
            total_amount=total_amount,
            obligation_type=obligation_type,
            monthly_payment=monthly_payment,
            start_date_str=start_date_str,
            end_date_str=end_date_str,
            payment_day=payment_day,
            element_path='input[id="start_date_field"] + span',
            error_message="This field is required.",
        )

        # Invalid end date
        end_date_str = "invalid"
        Helpers.create_invalid_fixed_obligation(
            self,
            name=name,
            total_amount=total_amount,
            obligation_type=obligation_type,
            monthly_payment=monthly_payment,
            start_date_str=start_date_str,
            end_date_str=end_date_str,
            payment_day=payment_day,
            element_path='input[id="end_date_field"] + span',
            error_message="This field is required.",
        )

        # Invalid payment_day
        payment_day = 32
        Helpers.create_invalid_fixed_obligation(
            self,
            name=name,
            total_amount=total_amount,
            obligation_type=obligation_type,
            monthly_payment=monthly_payment,
            start_date_str=start_date_str,
            end_date_str=end_date_str,
            payment_day=payment_day,
            element_path='input[id="payment_day_field"] + span',
            error_message="Payment Day should be less than 31",
        )
        payment_day = 0
        Helpers.create_invalid_fixed_obligation(
            self,
            name=name,
            total_amount=total_amount,
            obligation_type=obligation_type,
            monthly_payment=monthly_payment,
            start_date_str=start_date_str,
            end_date_str=end_date_str,
            payment_day=payment_day,
            element_path='input[id="payment_day_field"] + span',
            error_message="Payment Day should be more than 1",
        )

        # Confirm the start and end date validation logic
        start_date = date.today() + timedelta(days=random.randint(42, 365))
        end_date_str = (
            start_date - timedelta(days=random.randint(42, 365))
        ).isoformat()
        start_date_str = start_date.isoformat()
        Helpers.create_invalid_fixed_obligation(
            self,
            name=name,
            total_amount=total_amount,
            obligation_type=obligation_type,
            monthly_payment=monthly_payment,
            start_date_str=start_date_str,
            end_date_str=end_date_str,
            payment_day=payment_day,
            element_path='input[id="start_date_field"] + span',
            error_message="Start date must be before end date",
        )

        earliest_date_str = (
            date.today() - timedelta(days=random.randint(42, 365))
        ).isoformat()
        latest_date_str = (
            date.today() + timedelta(days=random.randint(42, 365))
        ).isoformat()

        Helpers.create_invalid_fixed_obligation(
            self,
            name=name,
            total_amount=total_amount,
            obligation_type=obligation_type,
            monthly_payment=monthly_payment,
            start_date_str=latest_date_str,
            end_date_str=earliest_date_str,
            payment_day=payment_day,
            element_path='input[id="end_date_field"] + span',
            error_message="End date must be after start date",
        )

    @Helpers.register_and_login
    def test_users_cant_see_other_users_fixed_obligations(self):
        """Check that the fixed obligations are displayed only for the correct owner"""
        check_creation_popup = True
        check_successful_creation = True

        fixed_obl = self.create_a_fixed_obligation(
            check_creation_popup=check_creation_popup,
            check_successful_creation=check_successful_creation,
        )
        # Frank is now tired, thinking about the obligation made him sleepy
        Helpers.logout_user(self)

        # Guido wants to see Frank's expense data: Guido is curious about
        # Frank's fixed obligations :D
        _ = Helpers.create_user(self)

        Helpers.find_a_fixed_obligation_in_list(
            self,
            fixed_obl["name"],
            fixed_obl["total_amount"],
            fixed_obl["obligation_type"],
            fixed_obl["monthly_payment"],
            fixed_obl["start_date"],
            fixed_obl["end_date"],
            fixed_obl["payment_day"],
            should_exist=False,
            should_be_missing=True,
        )

        # Guido also tries to access the fixed obligation directly, just in case
        # the developers forgot to hide the page
        Helpers.directly_access_a_fixed_obligation_edit_page(
            self,
            fixed_obl["id"],
            check_failure_popup=True,
        )

    @Helpers.register_and_login
    def test_create_and_edit_fixed_obligations(self):
        check_creation_popup = True
        check_successful_creation = True
        fixed_ob = self.create_a_fixed_obligation(
            check_creation_popup=check_creation_popup,
            check_successful_creation=check_successful_creation,
        )

        Helpers.visit_and_verify_a_fixed_obligation(
            self,
            fixed_ob,
            should_be_missing=False,
        )

        # Edit the obligation with new values
        # NOTE: we do not just generate a random number, but we increase it
        # so we are sure to get a different number
        name = Helpers.generate_string()
        total_amount = fixed_ob["total_amount"] + random.randint(1, 42)
        monthly_payment = fixed_ob["monthly_payment"] + random.randint(1, 42)
        start_date = date.fromisoformat(fixed_ob["start_date"]) + timedelta(
            days=random.randint(1, 42)
        )
        start_date_str = start_date.isoformat()
        end_date_str = (
            start_date + timedelta(days=random.randint(42, 365))
        ).isoformat()
        payment_day = random.randint(1, 28)
        # Keep generating the payment day until it is different from the previous one
        while payment_day == fixed_ob["payment_day"]:
            payment_day = random.randint(1, 28)

        Helpers.edit_a_fixed_obligation(
            self,
            fixed_ob["id"],
            name=name,
            total_amount=total_amount,
            obligation_type=fixed_ob["obligation_type"],
            monthly_payment=monthly_payment,
            start_date=start_date_str,
            end_date=end_date_str,
            payment_day=payment_day,
            check_success_popup=True,
            check_failure_popup=False,
            check_successful_edit=True,
        )

    def test_users_cant_edit_other_users_fixed_obligations(self):
        """Check that the users can only edit their own fixed obligations"""
        # We do not need to implement this, as the test is already covered by
        # the test_users_cant_see_other_users_fixed_obligations and by unit
        # tests
        pass

    @Helpers.register_and_login
    def test_users_can_delete_fixed_obligations(self):
        """Check that the users can delete their own fixed obligations"""
        fixed_ob = self.create_a_fixed_obligation(
            check_creation_popup=True,
            check_successful_creation=True,
        )

        # We create a second obligation, since as the moment there is an isssue
        # in the UI, and deleting the last Obligation will not properly
        # display the success popup
        _ = self.create_a_fixed_obligation(
            check_creation_popup=True,
            check_successful_creation=True,
        )

        Helpers.delete_a_fixed_obligation(
            self,
            fixed_ob["id"],
            check_successful_delete_popup=True,
            check_successful_delete=True,
        )

    def test_users_cant_edit_other_users_fixed_obligations(self):
        """Check that the users can only edit their own fixed obligations"""
        # We do not need to implement this, as the test is already covered by
        # the test_users_cant_see_other_users_fixed_obligations and by unit
        # tests
        pass

    @Helpers.register_and_login
    def test_users_cant_edit_fixed_obligations_to_invalid_values(self):
        """Check that the users can only edit the fixed obligations with valid values"""
        # Create a subfunction similar to create_invalid_fixed_obligation, but that edits a given obligation, to confirm the edit form has the correct validation logic
        check_creation_popup = True
        check_successful_creation = True
        fixed_ob = self.create_a_fixed_obligation(
            check_creation_popup=check_creation_popup,
            check_successful_creation=check_successful_creation,
        )

        # Invalid total amount
        total_amount = -1
        Helpers.edit_fixed_obligation_to_be_invalid(
            self,
            id=fixed_ob["id"],
            name=fixed_ob["name"],
            total_amount=total_amount,
            obligation_type=fixed_ob["obligation_type"],
            monthly_payment=fixed_ob["monthly_payment"],
            start_date_str=fixed_ob["start_date"],
            end_date_str=fixed_ob["end_date"],
            payment_day=fixed_ob["payment_day"],
            element_path='input[id="total_amount_field"] + span',
            error_message="Total Amount should be more than 0",
        )

        # Invalid monthly payment
        monthly_payment = -1
        Helpers.edit_fixed_obligation_to_be_invalid(
            self,
            id=fixed_ob["id"],
            name=fixed_ob["name"],
            total_amount=fixed_ob["total_amount"],
            obligation_type=fixed_ob["obligation_type"],
            monthly_payment=monthly_payment,
            start_date_str=fixed_ob["start_date"],
            end_date_str=fixed_ob["end_date"],
            payment_day=fixed_ob["payment_day"],
            element_path='input[id="monthly_payment_field"] + span',
            error_message="Monthly Payment Amount should be more than 0",
        )

        # Invalid start date
        start_date_str = "invalid"
        Helpers.edit_fixed_obligation_to_be_invalid(
            self,
            id=fixed_ob["id"],
            name=fixed_ob["name"],
            total_amount=fixed_ob["total_amount"],
            obligation_type=fixed_ob["obligation_type"],
            monthly_payment=fixed_ob["monthly_payment"],
            start_date_str=start_date_str,
            end_date_str=fixed_ob["end_date"],
            payment_day=fixed_ob["payment_day"],
            element_path='input[id="start_date_field"] + span',
            error_message="This field is required.",
        )

        # Invalid end date
        end_date_str = "invalid"
        Helpers.edit_fixed_obligation_to_be_invalid(
            self,
            id=fixed_ob["id"],
            name=fixed_ob["name"],
            total_amount=fixed_ob["total_amount"],
            obligation_type=fixed_ob["obligation_type"],
            monthly_payment=fixed_ob["monthly_payment"],
            start_date_str=fixed_ob["start_date"],
            end_date_str=end_date_str,
            payment_day=fixed_ob["payment_day"],
            element_path='input[id="end_date_field"] + span',
            error_message="This field is required.",
        )

        # Invalid payment_day
        payment_day = 32
        Helpers.edit_fixed_obligation_to_be_invalid(
            self,
            id=fixed_ob["id"],
            name=fixed_ob["name"],
            total_amount=fixed_ob["total_amount"],
            obligation_type=fixed_ob["obligation_type"],
            monthly_payment=fixed_ob["monthly_payment"],
            start_date_str=fixed_ob["start_date"],
            end_date_str=fixed_ob["end_date"],
            payment_day=payment_day,
            element_path='input[id="payment_day_field"] + span',
            error_message="Payment Day should be less than 31",
        )
        payment_day = 0
        Helpers.edit_fixed_obligation_to_be_invalid(
            self,
            id=fixed_ob["id"],
            name=fixed_ob["name"],
            total_amount=fixed_ob["total_amount"],
            obligation_type=fixed_ob["obligation_type"],
            monthly_payment=fixed_ob["monthly_payment"],
            start_date_str=fixed_ob["start_date"],
            end_date_str=fixed_ob["end_date"],
            payment_day=payment_day,
            element_path='input[id="payment_day_field"] + span',
            error_message="Payment Day should be more than 1",
        )

        # Confirm the start and date validation logic
        start_date = date.today() + timedelta(days=random.randint(42, 365))
        end_date_str = (
            start_date - timedelta(days=random.randint(42, 365))
        ).isoformat()
        start_date_str = start_date.isoformat()
        Helpers.edit_fixed_obligation_to_be_invalid(
            self,
            id=fixed_ob["id"],
            name=fixed_ob["name"],
            total_amount=fixed_ob["total_amount"],
            obligation_type=fixed_ob["obligation_type"],
            monthly_payment=fixed_ob["monthly_payment"],
            start_date_str=fixed_ob["end_date"],
            end_date_str=fixed_ob["start_date"],
            payment_day=fixed_ob["payment_day"],
            element_path='input[id="start_date_field"] + span',
            error_message="Start date must be before end date",
        )
        Helpers.edit_fixed_obligation_to_be_invalid(
            self,
            fixed_ob["id"],
            name=fixed_ob["name"],
            total_amount=fixed_ob["total_amount"],
            obligation_type=fixed_ob["obligation_type"],
            monthly_payment=fixed_ob["monthly_payment"],
            start_date_str=fixed_ob["end_date"],
            end_date_str=fixed_ob["start_date"],
            payment_day=fixed_ob["payment_day"],
            element_path='input[id="end_date_field"] + span',
            error_message="End date must be after start date",
        )
