# Copyright: (c) 2019, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from datetime import date
import random
from unittest import skip

import functional_tests.helpers as Helpers
from functional_tests.test_assets import MockAssetsAPI
from functional_tests.test_dividends import MockDividendsAPI
from functional_tests.test_parent import IntegrationTest

# Maximum amount of seconds to wait before giving up
MAX_WAIT_SECONDS = 2


class GuidanceIntegrationTest(IntegrationTest):
    """
    Integration test guidance banners
    """

    def setUp(self):
        # Instrument the unit tests to fail if they run for too long
        super().setUp()

    @Helpers.register_and_login
    def test_correct_guidance_for_spending_limits_creation(self):
        title = "Spending Limits"
        guidance = "You need to create a Category first!"
        header_class = "text-info"
        button_text = "Create a Category"
        button_url = "/app/categories/creation"

        url = "/app/spending-limits/creation"
        self.browser.get(f"{self.live_server_url}{url}")

        # Frank can see a clear guidance in the Dashboard: he has no Categories yet!
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
            is_widget=False,
        )

        # Frank creates a Category
        cat_text = Helpers.generate_string()
        Helpers.create_expense_category(
            self,
            cat_text,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        url = "/app/spending-limits/creation"
        self.browser.get(f"{self.live_server_url}{url}")

        # Frank can see the original guidance is not displayed anymore
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=True,
            is_widget=False,
        )

    @Helpers.register_and_login
    def test_correct_guidance_for_expenses_in_create_expenses(self):
        title = "Create an Expense"
        guidance = "You need to create a Category first!"
        header_class = "text-info"
        button_text = "Create a Category"
        button_url = "/app/categories/creation"

        url = "/app/expenses/creation"
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see a clear guidance in the page: he has no Categories yet!
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
            is_widget=False,
        )

        # Frank creates a category
        cat_text = Helpers.generate_string()
        Helpers.create_expense_category(
            self, cat_text, check_creation_popup=True
        )

        # Frank goes back to the original page
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see the original guidance is not displayed anymore
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=True,
            is_widget=False,
        )

    # We do not warn the user if they have no Categories in the list expenses
    # page
    @skip
    def test_correct_guidance_for_expenses_in_list_expenses(self):
        self.fail("/app/expenses is requesting an Expense Category to exist")

    @Helpers.register_and_login
    def test_correct_guidance_for_expenses_in_list_expenses_by_category(self):
        title = "Expenses by Category ID"
        guidance = "You need to create a Category first!"
        header_class = "text-info"
        button_text = "Create a Category"
        button_url = "/app/categories/creation"

        url = "/app/expenses/by-category"
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see a clear guidance in the page: he has no Categories yet!
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
            is_widget=False,
        )

        # Frank creates a category
        cat_text = Helpers.generate_string()
        Helpers.create_expense_category(
            self, cat_text, check_creation_popup=True
        )

        # Frank goes back to the original page
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see the original guidance is not displayed anymore
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=True,
            is_widget=False,
        )

    @Helpers.register_and_login
    def test_correct_guidance_for_expenses_in_list_expenses_vs_income(self):
        title = "Expenses vs Income"
        guidance = (
            "Your Expenses and Incomes will appear here after you create any!"
        )
        header_class = "text-info"
        button_text = "Create an Expense"
        button_url = "/app/expenses/creation"

        url = "/app/expenses/graphs/expvsinc"
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see a clear guidance in the page: he has no Categories yet!
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
            is_widget=False,
        )

        # Frank creates a category
        cat_text = Helpers.generate_string()
        Helpers.create_expense_category(
            self, cat_text, check_creation_popup=True
        )
        cat_id = Helpers.check_category_existence(self, cat_text, "categories")

        check_creation_popup = True
        check_failure_popup = False
        _ = Helpers.create_an_expense_using_react(
            self,
            cat_id,
            check_creation_popup=check_creation_popup,
            check_failure_popup=check_failure_popup,
        )

        # Frank goes back to the original page
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see the original guidance is not displayed anymore
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=True,
            is_widget=False,
        )

    @Helpers.register_and_login
    def test_correct_guidance_for_categories_in_list_categories(self):
        title = "List Categories"
        guidance = "You need to create a Category first!"
        header_class = "text-info"
        button_text = "Create a Category"
        button_url = "/app/categories/creation"

        url = "/app/categories"
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see a clear guidance in the page: he has no Categories yet!
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
            is_widget=False,
        )

        # Frank creates a category
        cat_text = Helpers.generate_string()
        Helpers.create_expense_category(
            self, cat_text, check_creation_popup=True
        )

        # Frank goes back to the original page
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see the original guidance is not displayed anymore
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=True,
            is_widget=False,
        )

    # In the list monhtly budgets We do not warn the user yet if they have
    # no Categories
    @skip
    def test_correct_guidance_for_budgets_in_list_monthly_budgets(self):
        pass

    # In the list yearly budgets We do not warn the user yet if they have
    # no Categories
    @skip
    def test_correct_guidance_for_budgets_in_list_yearly_budgets(self):
        pass

    @Helpers.register_and_login
    def test_archived_monthly_saving_budgets_do_not_trigger_warning_in_dashboard(
        self,
    ):
        title = "Monthly Saving Budgets"
        guidance = "You need to create a Saving Category first!"
        header_class = "text-info"
        button_text = "Create a Saving Category"
        button_url = "/app/saving-categories/creation"

        # Frank can see a clear guidance in the Dashboard: he has no
        # Saving Categories yet!
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
        )

        # Frank creates one Saving Category
        c1 = Helpers.generate_string()
        Helpers.create_saving_category(
            self, c1, check_create_success_popup=True
        )
        c1_id = Helpers.check_category_existence(
            self,
            text=c1,
            url="saving-categories",
            expected_to_fail=False,
        )
        c2 = Helpers.generate_string()
        # Pevent this test from becoming flaky, and assure the two Saving
        # Categoris never happen to generate the same text
        while c1 == c2:
            c2 = Helpers.generate_string()

        # Frank generates a second Saving Category, as he wants to see if
        # the app handle correctly warning for multiple Saving Categories
        Helpers.create_saving_category(
            self, c2, check_create_success_popup=True
        )
        c2_id = Helpers.check_category_existence(
            self,
            text=c2,
            url="saving-categories",
            expected_to_fail=False,
        )
        is_archived = True
        Helpers.edit_saving_category(
            self,
            cat_id=c1_id,
            text=c1,
            is_archived=is_archived,
            check_successful_edit=True,
            check_failed_edit=False,
        )

        is_archived = True
        Helpers.edit_saving_category(
            self,
            cat_id=c2_id,
            text=c2,
            is_archived=is_archived,
            check_successful_edit=True,
            check_failed_edit=False,
        )

        url = "/"
        self.browser.get(f"{self.live_server_url}{url}")

        # Franks is relieved to know that archived guidance will not trigger
        # unnecessary warnings in the dahboard
        header_class = "text-success"
        guidance = "All Saving Categories have a budget. You're all set!"
        button_text = "See Details"
        year = date.today().year
        month = date.today().month
        button_url = f"/app/budgets/date/{year}/{month}"
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
        )

    @Helpers.register_and_login
    def test_archived_yearly_saving_budgets_do_not_trigger_warning_in_dashboard(
        self,
    ):
        title = "Yearly Saving Budgets"
        guidance = "You need to create a Saving Category first!"
        header_class = "text-info"
        button_text = "Create a Saving Category"
        button_url = "/app/saving-categories/creation"

        # Frank can see a clear guidance in the Dashboard: he has no
        # Saving Categories yet!
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
        )

        # Frank creates one Saving Category
        c1 = Helpers.generate_string()
        Helpers.create_saving_category(
            self, c1, check_create_success_popup=True
        )
        c1_id = Helpers.check_category_existence(
            self,
            text=c1,
            url="saving-categories",
            expected_to_fail=False,
        )
        c2 = Helpers.generate_string()
        # Pevent this test from becoming flaky, and assure the two Saving
        # Categoris never happen to generate the same text
        while c1 == c2:
            c2 = Helpers.generate_string()

        # Frank generates a second Saving Category, as he wants to see if
        # the app handle correctly warning for multiple Saving Categories
        Helpers.create_saving_category(
            self, c2, check_create_success_popup=True
        )
        c2_id = Helpers.check_category_existence(
            self,
            text=c2,
            url="saving-categories",
            expected_to_fail=False,
        )
        # Franks edits the first Saving Category to be archived
        is_archived = True
        Helpers.edit_saving_category(
            self,
            cat_id=c1_id,
            text=c1,
            is_archived=is_archived,
            check_successful_edit=True,
            check_failed_edit=False,
        )

        # Franks edits the second Saving Category to be archived
        is_archived = True
        Helpers.edit_saving_category(
            self,
            cat_id=c2_id,
            text=c2,
            is_archived=is_archived,
            check_successful_edit=True,
            check_failed_edit=False,
        )

        url = "/"
        self.browser.get(f"{self.live_server_url}{url}")

        # Franks is relieved to know that archived guidance will not trigger
        # unnecessary warnings in the dahboard
        header_class = "text-success"
        guidance = "All Saving Categories have a budget. You're all set!"
        button_text = "See Details"
        year = date.today().year
        month = date.today().month
        button_url = f"/app/budgets/date/{year}"
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
        )

    @Helpers.register_and_login
    def test_correct_guidance_for_list_saving_categories(self):
        title = "List Saving Categories"
        guidance = (
            "Your Saving Categories will appear here after you create any!"
        )
        header_class = "text-info"
        button_text = "Create a Saving Category"
        button_url = "/app/saving-categories/creation"

        url = "/app/saving-categories"
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see a clear guidance in the page: he has no Saving
        # Categories yet!
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
            is_widget=False,
        )

        # Frank creates a Saving Category
        cat_text = Helpers.generate_string()
        Helpers.create_saving_category(
            self, cat_text, check_create_success_popup=True
        )

        # Frank goes back to the original page
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see the original guidance is not displayed anymore
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=True,
            is_widget=False,
        )

    @Helpers.register_and_login
    def test_correct_guidance_for_creating_spending_budgets(self):
        title = "Creating Spending Budget"
        guidance = "You need to create a Category first!"
        header_class = "text-info"
        button_text = "Create a Category"
        button_url = "/app/categories/creation"

        url = "/app/budgets/creation"
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see a clear guidance in the page: he has no Categories yet!
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
            is_widget=False,
        )

        # Frank creates a  Category
        cat_text = Helpers.generate_string()
        Helpers.create_expense_category(
            self, cat_text, check_creation_popup=True
        )

        # Frank goes back to the original page
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see the original guidance is not displayed anymore
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=True,
            is_widget=False,
        )

    @Helpers.register_and_login
    def test_correct_guidance_for_creating_saving_budgets(self):
        title = "Creating Saving Budget"
        guidance = "You need to create a Saving Category first!"
        header_class = "text-info"
        button_text = "Create a Saving Category"
        button_url = "/app/saving-categories/creation"

        url = "/app/budgets/saving/creation"
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see a clear guidance in the page: he has no Saving
        # Categories yet!
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
            is_widget=False,
        )

        # Frank creates a Saving Category
        cat_text = Helpers.generate_string()
        Helpers.create_saving_category(
            self, cat_text, check_create_success_popup=True
        )

        # Frank goes back to the original page
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see the original guidance is not displayed anymore
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=True,
            is_widget=False,
        )

    @Helpers.register_and_login
    def test_correct_guidance_for_list_monthly_balance_categories(self):
        title = "List Monthly Balance Categories"
        guidance = "You need to create a Monthly Balance Category first!"
        header_class = "text-info"
        button_text = "Create a Monthly Balance Category"
        button_url = "/app/monthly-balances/categories/creation"

        url = "/app/monthly-balances/categories"
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see a clear guidance in the page: he has no Monthly Balance
        # Categories yet!
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
            is_widget=False,
        )

        # Frank creates a Saving Category
        cat_text = Helpers.generate_string()
        Helpers.create_monthly_balance_category(
            self, cat_text, check_creation_popup=True
        )

        # Frank goes back to the original page
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see the original guidance is not displayed anymore
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=True,
            is_widget=False,
        )

    @Helpers.register_and_login
    def test_correct_guidance_for_create_update_monthly_balances(self):
        date_text = date.today().strftime("%Y-%m")
        title = (
            f"Create/Update multiple Monthly Balances for month {date_text}"
        )
        guidance = "You need to create a Monthly Balance Category first!"
        header_class = "text-info"
        button_text = "Create a Monthly Balance Category"
        button_url = "/app/monthly-balances/categories/creation"

        # NOTE: we hard code the URI because Django has no idea of React
        # routes (i.e. react internal virtual routes, not API endpoints)

        url = f"/app/monthly-balances/multiple-creation/date/{date_text}"
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see a clear guidance in the page: he has no Monthly Balance
        # Categories yet!
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
            is_widget=False,
        )

        # Frank creates a Saving Category
        cat_text = Helpers.generate_string()
        Helpers.create_monthly_balance_category(
            self, cat_text, check_creation_popup=True
        )

        # Frank goes back to the original page
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see the original guidance is not displayed anymore
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=True,
            is_widget=False,
        )

    @Helpers.register_and_login
    def test_correct_guidance_for_list_spending_limits(self):
        title = "List Spending Limits"
        guidance = (
            "Your Spending Limits will appear here after you create any!"
        )
        header_class = "text-info"
        button_text = "Create a Spending Limit"
        button_url = "/app/spending-limits/creation"

        # NOTE: we hard code the URI because Django has no idea of React
        # routes (i.e. react internal virtual routes, not API endpoints)

        url = f"/app/spending-limits"
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see a clear guidance in the page: he has no Monthly Balance
        # Categories yet!
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
            is_widget=False,
        )

        # Frank creates a Category
        cat_text = Helpers.generate_string()
        Helpers.create_expense_category(
            self, cat_text, check_creation_popup=True
        )
        cat_id = Helpers.check_category_existence(self, cat_text, "categories")

        Helpers.create_spending_limit(
            self,
            cat_id,
            amount=random.randint(1, 90000),
            days=random.randint(1, 27),
            currency=random.choice(["USD", "JPY", "EUR"]),
            text=Helpers.generate_string(),
            check_creation_popup=False,
            check_failure_popup=False,
        )

        # Frank goes back to the original page
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see the original guidance is not displayed anymore
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=True,
            is_widget=False,
        )

    # WRITE ME
    @skip
    @Helpers.register_and_login
    def test_correct_guidance_for_list_spending_obligations(self):
        pass

    # WRITE ME
    @skip
    @Helpers.register_and_login
    def test_correct_guidance_for_monthly_balances_in_list_monthly_balance_categories(
        self,
    ):
        self.fail(
            "/app/monthly-balances/categories is requesting a Monthly Balance Category to exist"
        )

    # FIXME: refactor this to use guidance_check() and check_guidance_content()
    @Helpers.register_and_login
    def test_correct_guidance_for_multiple_monthly_balances_in_list_monthly_balances_for_a_given_month(
        self,
    ):
        # Frank logs in

        # Frank creates the category
        this_month = date.today().replace(day=1).isoformat()
        cat_name = Helpers.generate_string()
        Helpers.create_monthly_balance_category(
            self,
            cat_name,
            check_creation_popup=True,
            check_failure_popup=False,
        )
        cat_id = Helpers.check_category_existence(
            self,
            text=cat_name,
            url="monthly-balances/categories",
            expected_to_fail=False,
        )

        # Frank creates the necessary data
        amount = random.randint(1, 90000)
        Helpers.create_a_monthly_balance(
            self, cat_id, amount, this_month, create_check=True
        )

        # Frank creates a second category
        cat_name2 = Helpers.generate_string()
        Helpers.create_monthly_balance_category(
            self,
            cat_name2,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        # Frank goes back to the original page
        date_text = date.today().strftime("%Y-%m")
        url = f"/app/monthly-balances/date/{date_text}"
        # NOTE: we hard code the URI because Django has no idea of React
        # routes (i.e. react internal virtual routes, not API endpoints)
        self.browser.get(f"{self.live_server_url}{url}")

        # And the guidance is not shown, the app seems to work!
        # There is no more guidance, and the correct data is displayed
        cat_text = "You do not have any MonthlyBalance Category yet!"
        data_text = "You do not have any MonthlyBalance for this month yet!"
        Helpers.check_missing_guidance(self, cat_text, data_text)

    # WRITE ME
    @skip
    def test_correct_guidance_for_monthly_balances_in_monthly_balances_graph(
        self,
    ):
        self.fail(
            "/app/monthly-balances/graph is requesting a Monthly Balance Category to exist"
        )

    @Helpers.register_and_login
    def test_correct_guidance_for_goals_in_list_goals(self):
        title = "List Goals"
        guidance = "Your goals will appear here after you create any!"
        header_class = "text-info"
        button_text = "Create a Goal"
        button_url = "/app/goals/creation"

        url = "/app/goals"
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see a clear guidance in the page: he has no Goals yet!
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
            is_widget=False,
        )

        _ = Helpers.create_a_goal_and_return_all_its_data(
            self, check_successful_creation=True
        )

        # Frank goes back to the original page
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see the original guidance is not displayed anymore
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=True,
            is_widget=False,
        )

    @Helpers.register_and_login
    def test_correct_guidance_for_accounts_in_list_accounts(self):
        title = "List Accounts"
        guidance = "You need to create an Account first!"
        header_class = "text-info"
        button_text = "Create an Account"
        button_url = "/app/accounts/creation"

        # NOTE: we hard code the URI because Django has no idea of React
        # routes (i.e. react internal virtual routes, not API endpoints)

        url = f"/app/accounts"
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see a clear guidance in the page: he has no Accounts yet!
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
            is_widget=False,
        )

        # Frank creates an Account
        Helpers.create_an_account(
            self,
            acc_name=Helpers.generate_string(20),
            credit_entity_name=Helpers.generate_string(20),
            is_foreign_currency=random.choice([True, False]),
            check_create_success_popup=True,
            check_create_failure_popup=False,
        )

        # Frank goes back to the original page
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see the original guidance is not displayed anymore
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=True,
            is_widget=False,
        )

    @Helpers.register_and_login
    def test_correct_guidance_for_accounts_in_list_assets(self):
        title = "List Assets"
        guidance = "You need to create some Monthly Balance or Assets first!"
        header_class = "text-info"
        button_text = "Import Assets Data"
        button_url = "/app/assets/import"
        button_text = "Import Assets data"
        second_button_text = "Create Monthly Balances"
        date_text = date.today().strftime("%Y-%m")
        second_button_url = (
            f"/app/monthly-balances/multiple-creation/date/" f"{date_text}"
        )

        url = f"/app/assets"
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see a clear guidance in the Dashboard: he has no Assets yet!
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
            is_widget=False,
        )

        # Frank can see a clear guidance in the Dashboard: he has Monthly Balance data yet!
        Helpers.guidance_check(
            self,
            title,
            guidance,
            second_button_text,
            header_class,
            second_button_url,
            expect_to_be_missing=False,
            is_widget=False,
            button_order=1,
        )

        cat_name = Helpers.generate_string()

        Helpers.create_user(self)
        Helpers.create_monthly_balance_category(
            self,
            category_name=cat_name,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        c_id = Helpers.check_category_existence(
            self,
            text=cat_name,
            url="monthly-balances/categories",
            expected_to_fail=False,
        )

        Helpers.create_a_monthly_balance(
            self,
            category_id=c_id,
            amount=random.randint(1, 1000),
            date=date.today().replace(day=1).isoformat(),
            create_check=True,
        )

        acc_name = Helpers.generate_string(20)
        credit_entity_name = Helpers.generate_string(20)
        is_foreign_currency = random.choice([True, False])
        Helpers.create_an_account(
            self,
            acc_name=acc_name,
            credit_entity_name=credit_entity_name,
            is_foreign_currency=is_foreign_currency,
            check_create_success_popup=True,
            check_create_failure_popup=False,
        )

        try:
            port = 13506
            self.mock_server = MockAssetsAPI(port=port)
            self.mock_server.daemon = True
            self.mock_server.start()

            socket_name = Helpers.get_live_server_test_case_socket_name()
            target_date = (
                "2023-07-01"  # REFACTOR ME: Is this the correct date?
            )
            mock_server_url = f"http://{socket_name}:{port}/assets"
            Helpers.import_assets_from_third_party_api(
                tester=self, apiURL=mock_server_url, target_date=target_date
            )
        except Exception as e:
            self.mock_server.stop()
        self.mock_server.stop()

        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see the original guidance is not displayed anymore
        # as he has created a Spending Limit
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            is_widget=False,
            expect_to_be_missing=True,
        )

    @Helpers.register_and_login
    def test_correct_guidance_for_accounts_in_import_assets(self):
        title = "Automatically import assets data"
        guidance = "You need to create an Account first!"
        header_class = "text-info"
        button_text = "Create an Account"
        button_url = "/app/accounts/creation"

        url = "/app/assets/import"
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see a clear guidance in the page: he has no Categories yet!
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
            is_widget=False,
        )

        # Frank creates an Account
        check_creation_popup = True
        check_failure_popup = False

        acc_name = Helpers.generate_string(20)
        credit_entity_name = Helpers.generate_string(20)
        is_foreign_currency = random.choice([True, False])
        Helpers.create_an_account(
            self,
            acc_name=acc_name,
            credit_entity_name=credit_entity_name,
            is_foreign_currency=is_foreign_currency,
            check_create_success_popup=check_creation_popup,
            check_create_failure_popup=check_failure_popup,
        )

        # Frank goes back to the original page
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see the original guidance is not displayed anymore
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=True,
            is_widget=False,
        )

    @Helpers.register_and_login
    def test_correct_guidance_for_accounts_in_list_dividends(self):
        title = "List Dividends"
        guidance = "You need to create some Dividends first!"
        header_class = "text-info"
        button_text = "Import Dividends"
        button_url = "/app/dividends/import"

        url = "/app/dividends"
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see a clear guidance in the page: he has no Dividends yet!
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
            is_widget=False,
        )

        # Frank creates an Account
        acc_name = Helpers.generate_string(20)
        credit_entity_name = Helpers.generate_string(20)
        is_foreign_currency = random.choice([True, False])
        Helpers.create_an_account(
            self,
            acc_name=acc_name,
            credit_entity_name=credit_entity_name,
            is_foreign_currency=is_foreign_currency,
            check_create_success_popup=True,
            check_create_failure_popup=False,
        )

        try:
            port = 13507
            self.mock_server = MockDividendsAPI(port=port)
            self.mock_server.daemon = True
            self.mock_server.start()

            socket_name = Helpers.get_live_server_test_case_socket_name()
            mock_server_url = f"http://{socket_name}:{port}/dividends"
            target_date = (
                "2023-07-01"  # REFACTOR ME: Is this the correct date?
            )
            Helpers.import_dividends_from_third_party_api(
                tester=self, apiURL=mock_server_url, target_date=target_date
            )

        except Exception as e:
            self.mock_server.stop()

        # Frank goes back to the original page
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see the original guidance is not displayed anymore
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=True,
            is_widget=False,
        )

    @Helpers.register_and_login
    def test_correct_guidance_for_accounts_in_import_dividends(self):
        title = "Automatically import dividends data"
        guidance = "You need to create an Account first!"
        header_class = "text-info"
        button_text = "Create an Account"
        button_url = "/app/accounts/creation"

        url = "/app/dividends/import"
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see a clear guidance in the page: he has no Categories yet!
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
            is_widget=False,
        )

        # Frank creates an Account
        check_creation_popup = True
        check_failure_popup = False

        acc_name = Helpers.generate_string(20)
        credit_entity_name = Helpers.generate_string(20)
        is_foreign_currency = random.choice([True, False])
        Helpers.create_an_account(
            self,
            acc_name=acc_name,
            credit_entity_name=credit_entity_name,
            is_foreign_currency=is_foreign_currency,
            check_create_success_popup=check_creation_popup,
            check_create_failure_popup=check_failure_popup,
        )

        # Frank goes back to the original page
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see the original guidance is not displayed anymore
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=True,
            is_widget=False,
        )

    @Helpers.register_and_login
    def test_correct_guidance_for_create_incomes(self):
        title = "Create an Income"
        guidance = "You need to create a Income Category first!"
        header_class = "text-info"
        button_text = "Create an Income Category"
        button_url = "/app/income-categories/creation"

        url = "/app/incomes/creation"
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see a clear guidance in the page: he has no Categories yet!
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
            is_widget=False,
        )

        # Frank creates a category
        cat_text = Helpers.generate_string()
        Helpers.create_an_income_category(
            self, cat_text, check_creation_popup=True
        )

        # Frank goes back to the original page
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see the original guidance is not displayed anymore
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=True,
            is_widget=False,
        )

    @Helpers.register_and_login
    def test_correct_guidance_for_list_income_categories(self):
        title = "List Income Categories"
        guidance = "You need to create an Income Category first!"
        header_class = "text-info"
        button_text = "Create an Income Category"
        button_url = "/app/income-categories/creation"

        url = "/app/income-categories"
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see a clear guidance in the page: he has no Categories yet!
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
            is_widget=False,
        )

        # Frank creates a category
        cat_text = Helpers.generate_string()
        Helpers.create_an_income_category(
            self, cat_text, check_creation_popup=True
        )

        # Frank goes back to the original page
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see the original guidance is not displayed anymore
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=True,
            is_widget=False,
        )

    # In the list incomes we do not warn the user yet if they have
    # no Income Categories
    @skip
    def test_correct_guidance_for_list_incomes(self):
        pass

    @Helpers.register_and_login
    def test_correct_guidance_for_budgets_in_create_spending_budgets(
        self,
    ):
        title = "Create a Spending Budget"
        guidance = "You need to create a Category first!"
        header_class = "text-info"
        button_text = "Create a Category"
        button_url = "/app/categories/creation"

        url = "/app/budgets/creation"
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see a clear guidance in the page: he has no Categories yet!
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
            is_widget=False,
        )

        # Frank creates a Category
        cat_text = Helpers.generate_string(20)
        Helpers.create_expense_category(
            self, category_name=cat_text, check_creation_popup=True
        )

        # Frank goes back to the original page
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see the original guidance is not displayed anymore
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=True,
            is_widget=False,
        )

    @Helpers.register_and_login
    def test_correct_guidance_for_budgets_in_create_saving_budgets(
        self,
    ):
        title = "Create a Saving Budget"
        guidance = "You need to create a Saving Category first!"
        header_class = "text-info"
        button_text = "Create a Saving Category"
        button_url = "/app/saving-categories/creation"

        url = "/app/budgets/saving/creation"
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see a clear guidance in the Dashboard: he has no Categories yet!
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=False,
            is_widget=False,
        )

        # Frank creates a Saving Category
        cat_text = Helpers.generate_string(20)
        Helpers.create_saving_category(
            self, cat_text, check_create_success_popup=True
        )

        # Frank goes back to the original page
        self.browser.get(f"{self.live_server_url}{url}")
        # Frank can see the original guidance is not displayed anymore
        Helpers.guidance_check(
            self,
            title,
            guidance,
            button_text,
            header_class,
            button_url,
            expect_to_be_missing=True,
            is_widget=False,
        )
