# Copyright: (c) 2019, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from datetime import date
from datetime import timedelta
import random
from unittest import skip

from django.urls import reverse
from selenium.webdriver.common.by import By

import functional_tests.helpers as Helpers
from functional_tests.test_parent import IntegrationTest


class IncomeIntegrationTest(IntegrationTest):
    """
    Integration test suite for IncomeCategories related views
    """

    def setUp(self):
        # Instrument the unit tests to fail if they run for too long
        super().setUp()

    @skip
    def test_cant_create_malformed_incomes(self):
        print("IMPLEMENT ME: test_cant_create_malformed_incomes")
        pass

    @Helpers.register_and_login
    def test_create_and_retrieve_incomes(self):
        cat_text = Helpers.generate_string()
        Helpers.create_an_income_category(
            self, cat_text, check_creation_popup=True
        )
        cat_id = Helpers.check_category_existence(
            self,
            text=cat_text,
            url="income-categories",
            expected_to_fail=False,
        )
        amount = random.randint(1, 90000)
        income_date = (
            date.today() - timedelta(days=random.randint(300, 365))
        ).isoformat()
        note = Helpers.generate_string()
        Helpers.create_an_income(
            self,
            cat_id,
            amount,
            income_date,
            note,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        inc_id = Helpers.find_income_in_date_range(
            self,
            {
                "category_text": cat_text,
                "amount": amount,
                "date": income_date,
                "note": note,
            },
            {
                "start": income_date,
                "end": income_date,
            },
            expected_to_fail=False,
        )
        assert inc_id is not None and inc_id != -1

        # Frank does not trust the API, and he wants to check if can also
        # display a second income
        amount2 = random.randint(1, 90000)
        income_date2 = (
            date.today() - timedelta(days=random.randint(300, 365))
        ).isoformat()
        note2 = Helpers.generate_string()
        Helpers.create_an_income(
            self,
            cat_id,
            amount2,
            income_date2,
            note2,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        inc_id2 = Helpers.find_income_in_date_range(
            self,
            {
                "category_text": cat_text,
                "amount": amount2,
                "date": income_date2,
                "note": note2,
            },
            {
                "start": income_date2,
                "end": income_date2,
            },
            expected_to_fail=False,
        )
        assert inc_id2 is not None and inc_id2 != -1

    # # WRITE ME (NICE TO HAVE)
    # @skip
    # def test_only_incomes_in_range_are_shown(self):
    #     """Check that only incomes within the date are displayed"""
    #     # TODO: Create a few incomes with different dates, and confirm that
    #     #       start/end params are filtering them correctly
    #     pass

    # # WRITE ME
    # @skip
    # def test_users_cant_edit_other_users_incomes(self):
    #     """
    #     Check that the incomes can not be edited to use other users's
    #     categories
    #     """
    #     pass

    @Helpers.register_and_login
    def test_users_can_not_see_other_users_incomes(self):
        """Check that only current user incomes are returned"""
        cat_text = Helpers.generate_string()
        Helpers.create_an_income_category(
            self, cat_text, check_creation_popup=True
        )
        cat_id = Helpers.check_category_existence(
            self,
            text=cat_text,
            url="income-categories",
            expected_to_fail=False,
        )
        amount = random.randint(1, 90000)
        income_date = (
            date.today() - timedelta(days=random.randint(300, 365))
        ).isoformat()
        note = Helpers.generate_string()
        Helpers.create_an_income(
            self,
            cat_id,
            amount,
            income_date,
            note,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        inc_id = Helpers.find_income_in_date_range(
            self,
            {
                "category_text": cat_text,
                "amount": amount,
                "date": income_date,
                "note": note,
            },
            {
                "start": income_date,
                "end": income_date,
            },
            expected_to_fail=False,
        )
        assert inc_id is not None and inc_id != -1

        # Frank logs off
        Helpers.logout_user(self)

        # Guido was snooping over Frank's shoulder and he wants to see Frank's
        # income details
        Helpers.create_user(self)

        inc_id2 = Helpers.find_income_in_date_range(
            self,
            {
                "category_text": cat_text,
                "amount": amount,
                "date": income_date,
                "note": note,
            },
            {
                "start": income_date,
                "end": income_date,
            },
            expected_to_fail=True,
        )
        assert inc_id2 == -1

    @Helpers.register_and_login
    def test_users_can_edit_incomes(self):
        """Confirm that the income owner can edit them"""
        _, _ = Helpers.create_user(self)
        cat_text = Helpers.generate_string()
        Helpers.create_an_income_category(
            self, cat_text, check_creation_popup=True
        )
        cat_id = Helpers.check_category_existence(
            self,
            text=cat_text,
            url="income-categories",
            expected_to_fail=False,
        )
        # Frank proceeds to create the income for this month: he worked hard
        # and he'd like to register said event into the app
        amount = random.randint(1, 90000)
        income_date = (
            date.today() - timedelta(days=random.randint(300, 365))
        ).isoformat()
        note = Helpers.generate_string()
        Helpers.create_an_income(
            self,
            cat_id,
            amount,
            income_date,
            note,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        inc_id = Helpers.find_income_in_date_range(
            self,
            {
                "category_text": cat_text,
                "amount": amount,
                "date": income_date,
                "note": note,
            },
            {
                "start": income_date,
                "end": income_date,
            },
            expected_to_fail=False,
        )
        assert inc_id is not None and inc_id != -1

        # Frank realizes that he inserted some wrong data: THE HORROR! Data can
        # not be wrong! Franks feels very anxious...but then he remembers that
        # the app's manual mentioned that income object data can be edited
        # seamlessly
        second_amount = random.randint(1, 90000)
        second_income_date = (
            date.today() - timedelta(days=random.randint(300, 365))
        ).isoformat()
        second_note = Helpers.generate_string()

        # Frank proceeds to edit the data: he's happy to see that the app
        # allows him to do so
        Helpers.edit_an_income(
            self,
            inc_id,
            cat_id,
            amount=second_amount,
            date=second_income_date,
            note=second_note,
            check_denied_permission_popup=False,
            check_successful_creation_popup=True,
            check_failure_popup=False,
        )

        # Frank confirms the income has been edited, he's happy again
        inc_id2 = Helpers.find_income_in_date_range(
            self,
            {
                "category_text": cat_text,
                "amount": second_amount,
                "date": second_income_date,
                "note": second_note,
            },
            {
                "start": second_income_date,
                "end": second_income_date,
            },
            expected_to_fail=False,
        )
        assert inc_id2 is not None and inc_id2 != -1

        # Frank confirms that the income ID has not changed, he's very happy :D
        self.assertEqual(inc_id, inc_id2)

    def test_users_can_not_edit_other_users_incomes(self):
        """
        We do not need to test this as we already confirmed that only the owner
        can see the income. If the user can't see the income, he can't edit it.
        If the user can edit it through the APIs, we should not check this
        in the functional tests, but in the unit tests.
        """
        pass

    @Helpers.register_and_login
    def test_users_can_delete_incomes(self):
        """
        Check that income entities can be deleted by their owner using the react app
        """
        cat_text = Helpers.generate_string()
        Helpers.create_an_income_category(
            self, cat_text, check_creation_popup=True
        )
        cat_id = Helpers.check_category_existence(
            self,
            text=cat_text,
            url="income-categories",
            expected_to_fail=False,
        )
        # Frank proceeds to create the income for this month: he worked hard
        # and he'd like to register said event into the app
        amount = random.randint(1, 90000)
        income_date = (
            date.today() - timedelta(days=random.randint(300, 365))
        ).isoformat()
        note = Helpers.generate_string()
        Helpers.create_an_income(
            self,
            cat_id,
            amount,
            income_date,
            note,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        inc_id = Helpers.find_income_in_date_range(
            self,
            {
                "category_text": cat_text,
                "amount": amount,
                "date": income_date,
                "note": note,
            },
            {
                "start": income_date,
                "end": income_date,
            },
            expected_to_fail=False,
        )
        assert inc_id is not None and inc_id != -1

        # Frank suddenly realizes that the income has not been wired to him
        # yet: he was just day dreaming about this month income!
        # Frank decides to delete said income object, as the app manual says
        # that deletion of income object is supported
        Helpers.delete_an_income(
            self,
            inc_id,
            income_date,
            check_successful_deletion_popup=False,
            check_failed_deletion_popup=False,
        )

        inc_id2 = Helpers.find_income_in_date_range(
            self,
            {
                "category_text": cat_text,
                "amount": amount,
                "date": income_date,
                "note": note,
            },
            {
                "start": income_date,
                "end": income_date,
            },
            expected_to_fail=True,
        )
        assert inc_id2 == -1

    # WRITE ME
    @skip
    def test_users_can_see_error_if_category_is_missing_on_creation(self):
        pass

    # WRITE ME
    @skip
    def test_users_can_see_error_if_amount_is_missing_on_creation(self):
        pass

    # WRITE ME
    @skip
    def test_users_can_see_error_if_amount_is_missing_on_edit(self):
        pass

    # WRITE ME
    @skip
    def test_regression_can_resubmit_a_form_after_a_failed_submission(self):
        """
        WRITE ME
        """
        pass
