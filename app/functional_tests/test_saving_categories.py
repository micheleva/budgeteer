# Copyright: (c) 2019, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from selenium.webdriver.common.by import By

import functional_tests.helpers as Helpers
from functional_tests.test_parent import IntegrationTest

# This is the maximum time that Frank and Guido are able to wait (in seconds)
# before giving up and throw a tantrum
MAX_WAIT_SECONDS = 2


class SavingCategoriesIntegrationTest(IntegrationTest):
    """
    Integration test suite for SavingCategories related views
    """

    def setUp(self):
        # Instrument the unit tests to fail if they run for too long
        super().setUp()

    @Helpers.register_and_login
    def test_can_create_and_retrieve_saving_categories(self):
        """Check that creation of of a saving category is successful"""
        # Frank can create a category to log his savings
        cat_text = Helpers.generate_string(20)
        Helpers.create_saving_category(
            self, cat_text, check_create_success_popup=True
        )
        _ = Helpers.check_category_existence(
            self,
            text=cat_text,
            url="saving-categories",
            expected_to_fail=False,
        )

    @Helpers.register_and_login
    def test_users_can_not_see_other_users_saving_categories(self):
        cat_text = Helpers.generate_string(20)
        Helpers.create_saving_category(
            self, cat_text, check_create_success_popup=True
        )

        Helpers.logout_user(self)

        # Guido wants to alter Frank's expense data: Guido feels like pranks
        # are a great way to bond with people
        _ = Helpers.create_user(self)

        _ = Helpers.check_category_existence(
            self,
            text=cat_text,
            url="saving-categories",
            expected_to_fail=True,
        )

    @Helpers.register_and_login
    def test_can_not_create_an_empty_saving_category(self):
        Helpers.create_saving_category(self, "")

        expect = "This field is required."
        sel_type = By.CSS_SELECTOR
        sel = "span.help-inline"
        Helpers.find_error_by_selector(self, sel_type, sel, expect)

    @Helpers.register_and_login
    def test_can_create_multiple_saving_categories(self):
        """Check that creation of multiple categories is successful"""
        c1 = Helpers.generate_string(20)
        c2 = Helpers.generate_string(20)
        # Let's prevent this test from randomly failing: let's assure we have
        # two different strings
        while c1 == c2:
            c2 = Helpers.generate_string(20)

        Helpers.create_saving_category(
            self, c1, check_create_success_popup=True
        )
        _ = Helpers.check_category_existence(
            self,
            text=c1,
            url="saving-categories",
            expected_to_fail=False,
        )
        Helpers.create_saving_category(
            self, c2, check_create_success_popup=True
        )
        for cat in [c1, c2]:
            Helpers.check_category_existence(
                self,
                text=cat,
                url="saving-categories",
                expected_to_fail=False,
            )

    @Helpers.register_and_login
    def test_different_users_can_create_categories_with_the_same_name(self):
        cat_text = Helpers.generate_string(20)
        Helpers.create_saving_category(
            self, cat_text, check_create_success_popup=True
        )
        _ = Helpers.check_category_existence(
            self,
            text=cat_text,
            url="saving-categories",
            expected_to_fail=False,
        )
        Helpers.logout_user(self)

        # Guido logs in
        _ = Helpers.create_user(self)
        # Guido can not see Frank's Saving Category
        _ = Helpers.check_category_existence(
            self,
            text=cat_text,
            url="saving-categories",
            expected_to_fail=True,
        )

        # But he can create a Saving Category with the same text
        Helpers.create_saving_category(
            self, cat_text, check_create_success_popup=True
        )
        _ = Helpers.check_category_existence(
            self,
            text=cat_text,
            url="saving-categories",
            expected_to_fail=False,
        )

    @Helpers.register_and_login
    def test_can_not_create_duplicate_saving_categories(self):
        cat_text = Helpers.generate_string(20)
        Helpers.create_saving_category(
            self, cat_text, check_create_success_popup=True
        )
        _ = Helpers.check_category_existence(
            self,
            text=cat_text,
            url="saving-categories",
            expected_to_fail=False,
        )

        Helpers.create_saving_category(self, cat_text)

        sel_type = By.CSS_SELECTOR
        sel = "span.help-inline"
        expect = "A SavingCategory with the text already exists for this user."
        Helpers.find_error_by_selector(self, sel_type, sel, expect)

    @Helpers.register_and_login
    def test_users_can_entirely_edit_saving_categories_successfully(self):
        cat_text = Helpers.generate_string(20)
        Helpers.create_saving_category(
            self, cat_text, check_create_success_popup=True
        )
        c_id = Helpers.check_category_existence(
            self,
            text=cat_text,
            url="saving-categories",
            expected_to_fail=False,
        )
        new_text = Helpers.generate_string(20)
        # By Default new Saving categorys have is_archived set to False
        is_archived = True
        Helpers.edit_saving_category(
            self,
            c_id,
            new_text,
            is_archived,
            check_successful_edit=True,
            check_failed_edit=False,
        )

        Helpers.visit_a_saving_category(
            self, c_id, new_text, is_archived, check_failure_popup=False
        )

    @Helpers.register_and_login
    def test_users_can_partially_edit_saving_categories(self):
        pass

    @Helpers.register_and_login
    def test_users_can_not_edit_other_users_categories(self):
        cat_text = Helpers.generate_string(20)
        Helpers.create_saving_category(
            self, cat_text, check_create_success_popup=True
        )
        c_id = Helpers.check_category_existence(
            self,
            text=cat_text,
            url="saving-categories",
            expected_to_fail=False,
        )
        Helpers.logout_user(self)

        # Guido wants to alter Frank's expense data: Guido feels like pranks
        # are a great way to bond with people
        _ = Helpers.create_user(self)
        is_archived = False
        Helpers.visit_a_saving_category(
            self, c_id, cat_text, is_archived, check_failure_popup=True
        )

    @Helpers.register_and_login
    def test_regression_216_can_resubmit_a_form_after_a_failed_submission(
        self,
    ):
        c1 = Helpers.generate_string(20)
        Helpers.create_saving_category(
            self, c1, check_create_success_popup=True
        )
        _ = Helpers.check_category_existence(
            self,
            text=c1,
            url="saving-categories",
            expected_to_fail=False,
        )
        # Try to re-create the same Saving Category
        Helpers.create_saving_category(self, c1)

        sel_type = By.CSS_SELECTOR
        sel = "span.help-inline"
        expect = "A SavingCategory with the text already exists for this user."
        Helpers.find_error_by_selector(self, sel_type, sel, expect)

        c2 = Helpers.generate_string(20)
        # Let's prevent this test from randomly failing: let's assure we have
        # two different strings
        while c1 == c2:
            c2 = Helpers.generate_string(20)

        Helpers.input_saving_category_name_and_submit_form(self, c2)
        for cat in [c1, c2]:
            Helpers.check_category_existence(
                self,
                text=cat,
                url="saving-categories",
                expected_to_fail=False,
            )
