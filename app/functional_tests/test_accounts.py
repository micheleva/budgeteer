# Copyright: (c) 2019, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
import random
from unittest import skip

import functional_tests.helpers as Helpers
from functional_tests.test_parent import IntegrationTest


class AccountsIntegrationTest(IntegrationTest):
    """
    Integration test suite for Account related views
    """

    def setUp(self):
        # Instrument the unit tests to fail if they run for too long
        super().setUp()

    @Helpers.register_and_login
    def test_can_create_and_list_accounts(
        self,
    ):  # pylint: disable=R0914; # noqa
        """Check that creation and retrival of categories works correctly"""

        check_creation_popup = True
        check_failure_popup = False

        acc_name = Helpers.generate_string(20)
        credit_entity_name = Helpers.generate_string(20)
        is_foreign_currency = random.choice([True, False])
        Helpers.create_an_account(
            self,
            acc_name=acc_name,
            credit_entity_name=credit_entity_name,
            is_foreign_currency=is_foreign_currency,
            check_create_success_popup=check_creation_popup,
            check_create_failure_popup=check_failure_popup,
        )

        acc_name2 = Helpers.generate_string(20)
        credit_entity_name2 = Helpers.generate_string(20)
        is_foreign_currency2 = random.choice([True, False])
        Helpers.create_an_account(
            self,
            acc_name=acc_name2,
            credit_entity_name=credit_entity_name2,
            is_foreign_currency=is_foreign_currency2,
            check_create_success_popup=check_creation_popup,
            check_create_failure_popup=check_failure_popup,
        )

        Helpers.check_account_existence(
            self,
            acc_name,
            credit_entity_name,
            is_foreign_currency,
            expected_to_fail=False,
        )
        Helpers.check_account_existence(
            self,
            acc_name2,
            credit_entity_name2,
            is_foreign_currency2,
            expected_to_fail=False,
        )

    # WRITE ME
    @skip
    def test_can_not_create_malformed_accounts(
        self,
    ):  # pylint: disable=R0914; # noqa
        """
        WRITE ME
        """
        pass

    # WRITE ME
    @skip
    def test_users_can_entirely_edit_accounts(self):
        """
        WRITE ME
        """
        pass

    # WRITE ME
    @skip
    def test_users_can_partially_edit_accounts(self):
        """
        WRITE ME
        """
        pass

    # WRITE ME
    @skip
    def test_users_can_not_see_other_users_accounts(self):
        """
        WRITE ME
        """
        # check for accouns (i.e. list)
        # check for account (i.e. details)
        pass

    # WRITE ME
    @skip
    def test_users_can_not_entirely_edit_other_users_accounts(self):
        """
        WRITE ME
        """
        pass

    # WRITE ME
    @skip
    def test_users_can_not_partially_edit_other_users_accounts(self):
        """
        WRITE ME
        """
        pass

    # WRITE ME
    @skip
    def test_regression_216_can_resubmit_a_form_after_a_failed_submission(
        self,
    ):
        """
        WRITE ME
        """
        pass
