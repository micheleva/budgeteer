# Copyright: (c) 2019, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from decimal import Decimal
import random
import time
from unittest import skip

from selenium.webdriver.common.by import By

import functional_tests.helpers as Helpers
from functional_tests.test_parent import IntegrationTest

# This is the maximum time that Frank and Guido are able to wait (in seconds)
# before giving up and throw a tantrum
MAX_WAIT_SECONDS = 2


class SpendingLimitsIntegrationTest(IntegrationTest):
    """
    Integration test suite for SpendingLimits related views
    """

    def setUp(self):
        # Instrument the unit tests to fail if they run for too long
        super().setUp()

    @Helpers.register_and_login
    def test_can_create_and_retrieve_spending_limits(self):
        """Check that creation of a spending limit is successful"""
        # Frank can create a category to log his savings
        cat_text = Helpers.generate_string(20)
        Helpers.create_expense_category(
            self,
            cat_text,
            check_creation_popup=True,
            check_failure_popup=False,
        )
        cat_id = Helpers.check_category_existence(self, cat_text, "categories")

        # Frank can create a spending limit for the category
        amount = random.randint(1, 90000)
        days = random.randint(1, 27)
        currency = random.choice(["JPY", "EUR", "USD"])
        text = Helpers.generate_string()
        Helpers.create_spending_limit(
            self,
            cat_id,
            amount=amount,
            days=days,
            currency=currency,
            text=text,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        # Frank can see the spending limit in the list
        Helpers.find_spending_limit(
            self,
            {
                "cat_id": cat_id,
                "cat_text": cat_text,
                "amount": amount,
                "currency": currency,
                "days": days,
                "text": text,
            },
            expected_to_fail=False,
        )

    @Helpers.register_and_login
    def test_users_can_not_see_other_users_spending_limits(self):
        """
        Confirm permissions are enforced for spending limits objects
        """
        # Frank can create a category to log his savings
        cat_text = Helpers.generate_string(20)
        Helpers.create_expense_category(
            self,
            cat_text,
            check_creation_popup=True,
            check_failure_popup=False,
        )
        cat_id = Helpers.check_category_existence(self, cat_text, "categories")

        # Frank can create a spending limit for the category
        amount = random.randint(1, 90000)
        days = random.randint(1, 27)
        currency = random.choice(
            [
                "JPY",
                "EUR",
                "USD",
            ]
        )
        text = Helpers.generate_string()
        Helpers.create_spending_limit(
            self,
            cat_id,
            amount=amount,
            days=days,
            currency=currency,
            text=text,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        # Frank logs out
        Helpers.logout_user(self)

        # Guido logs in
        _ = Helpers.create_user(self)

        # Guido can not see Frank's spending limit
        Helpers.find_spending_limit(
            self,
            {
                "cat_id": cat_id,
                "cat_text": cat_text,
                "amount": amount,
                "currency": currency,
                "days": days,
                "text": text,
            },
            expected_to_fail=True,
        )

    @Helpers.register_and_login
    def test_can_not_create_an_empty_spending_limits(self):
        """Check that creation of an empty spending limit fails"""
        # Frank can create a category to log his savings
        cat_text = Helpers.generate_string(20)
        Helpers.create_expense_category(
            self,
            cat_text,
            check_creation_popup=True,
            check_failure_popup=False,
        )
        cat_id = Helpers.check_category_existence(self, cat_text, "categories")

        # Frank can not create a spending limit for the category
        Helpers.create_spending_limit(
            self,
            cat_id,
            amount=0,
            days=0,
            currency="",
            text="",
            check_creation_popup=False,
            check_failure_popup=False,
        )
        # Check the form errors
        expected_errors = [
            "Amount should be more than 0",
            "Days should be more than 0",
            "This field is required.",
            "This field is required.",
        ]
        text_danger = self.browser.find_elements(By.CLASS_NAME, "text-danger")

        # Verify we have the correct number of errors
        self.assertEqual(len(text_danger), len(expected_errors))

        # Get all error messages
        actual_errors = [error.text for error in text_danger]

        # Count occurrences of each expected error
        error_counts = {}
        for error in actual_errors:
            error_counts[error] = error_counts.get(error, 0) + 1

        # Verify expected errors match actual counts
        expected_counts = {}
        for error in expected_errors:
            expected_counts[error] = expected_counts.get(error, 0) + 1

        self.assertEqual(error_counts, expected_counts)

    @Helpers.register_and_login
    def test_can_create_multiple_spending_limits(self):
        """Check that creation of multiple spending limits is successful"""
        # Frank can create a category to log his savings
        cat_text = Helpers.generate_string(20)
        Helpers.create_expense_category(
            self,
            cat_text,
            check_creation_popup=True,
            check_failure_popup=False,
        )
        cat_id = Helpers.check_category_existence(self, cat_text, "categories")

        # Frank can create multiple spending limits for the category
        for _ in range(3):
            amount = random.randint(1, 90000)
            days = random.randint(1, 27)
            currency = random.choice(
                [
                    "JPY",
                    "EUR",
                    "USD",
                ]
            )
            text = Helpers.generate_string()
            Helpers.create_spending_limit(
                self,
                cat_id,
                amount=amount,
                days=days,
                currency=currency,
                text=text,
                check_creation_popup=True,
                check_failure_popup=False,
            )

        # Frank can see all the spending limits in the list
        for _ in range(3):
            Helpers.find_spending_limit(
                self,
                {
                    "cat_id": cat_id,
                    "cat_text": cat_text,
                    "amount": amount,
                    "currency": currency,
                    "days": days,
                    "text": text,
                },
                expected_to_fail=False,
            )

    @Helpers.register_and_login
    def test_different_users_can_create_spending_limits_with_the_same_content(
        self,
    ):
        """
        Check that different users can create spending limits with the same
        name
        """
        # Frank can create a category to log his savings
        cat_text = Helpers.generate_string(20)
        Helpers.create_expense_category(
            self,
            cat_text,
            check_creation_popup=True,
            check_failure_popup=False,
        )
        cat_id = Helpers.check_category_existence(self, cat_text, "categories")

        # Frank can create a spending limit for the category
        amount = random.randint(1, 90000)
        days = random.randint(1, 27)
        currency = random.choice(
            [
                "JPY",
                "EUR",
                "USD",
            ]
        )
        text = Helpers.generate_string()
        Helpers.create_spending_limit(
            self,
            cat_id,
            amount=amount,
            days=days,
            currency=currency,
            text=text,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        # Guido logs in
        _ = Helpers.create_user(self)

        Helpers.create_expense_category(
            self,
            cat_text,
            check_creation_popup=True,
            check_failure_popup=False,
        )
        cat_id2 = Helpers.check_category_existence(
            self, cat_text, "categories"
        )

        # Guido can create a spending limit with the same content
        Helpers.create_spending_limit(
            self,
            cat_id2,
            amount=amount,
            days=days,
            currency=currency,
            text=text,
            check_creation_popup=True,
            check_failure_popup=False,
        )

    @Helpers.register_and_login
    def test_can_not_create_duplicate_spending_limits(self):
        """Check that creation of duplicate spending limits fails"""
        # Frank can create a category to log his savings
        cat_text = Helpers.generate_string(20)
        Helpers.create_expense_category(
            self,
            cat_text,
            check_creation_popup=True,
            check_failure_popup=False,
        )
        cat_id = Helpers.check_category_existence(self, cat_text, "categories")

        # Frank can create a spending limit for the category
        amount = random.randint(1, 90000)
        days = random.randint(1, 27)
        currency = random.choice(
            [
                "JPY",
                "EUR",
                "USD",
            ]
        )
        text = Helpers.generate_string()
        Helpers.create_spending_limit(
            self,
            cat_id,
            amount=amount,
            days=days,
            currency=currency,
            text=text,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        # Frank can not create a duplicate spending limit
        Helpers.create_spending_limit(
            self,
            cat_id,
            amount=amount,
            days=days,
            currency=currency,
            text=text,
            check_creation_popup=False,
            check_failure_popup=True,
        )
        # Check the form errors
        expected_errors = [
            "A SpendingLimit for this Category with the same currency, amount,"
            " days, and text already exists for this user.",
        ]
        text_danger = self.browser.find_elements(By.CLASS_NAME, "text-danger")

        # Verify we have the correct number of errors
        self.assertEqual(len(text_danger), len(expected_errors))

        # Get all error messages
        actual_errors = [error.text for error in text_danger]

        # Count occurrences of each expected error
        error_counts = {}
        for error in actual_errors:
            error_counts[error] = error_counts.get(error, 0) + 1

        # Verify expected errors match actual counts
        expected_counts = {}
        for error in expected_errors:
            expected_counts[error] = expected_counts.get(error, 0) + 1

        self.assertEqual(error_counts, expected_counts)

    @skip
    @Helpers.register_and_login
    def test_users_can_entirely_edit_spending_limits_successfully(self):
        self.fail("copy pasted from test_can_create_and_retrieve_expenses")

    @skip
    @Helpers.register_and_login
    def test_users_can_partially_edit_spending_limits(self):
        self.fail("copy pasted from test_can_create_and_retrieve_expenses")

    @skip
    @Helpers.register_and_login
    def test_users_can_not_edit_other_users_spending_limits(self):
        self.fail("copy pasted from test_can_create_and_retrieve_expenses")

    @Helpers.register_and_login
    def test_regression_216_can_resubmit_a_form_after_a_failed_submission(
        self,
    ):
        """
        Regression test for issue #216
        """
        # Frank can create a category to log his savings
        cat_text = Helpers.generate_string(20)
        Helpers.create_expense_category(
            self,
            cat_text,
            check_creation_popup=True,
            check_failure_popup=False,
        )
        cat_id = Helpers.check_category_existence(self, cat_text, "categories")

        # Frank can create a spending limit for the category
        amount = random.randint(1, 90000)
        days = random.randint(1, 27)
        currency = random.choice(
            [
                "JPY",
                "EUR",
                "USD",
            ]
        )
        text = Helpers.generate_string()
        Helpers.create_spending_limit(
            self,
            cat_id,
            amount=amount,
            days=days,
            currency=currency,
            text=text,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        # As long as the amount is different, the spending limit can be created
        # hence we do not need to worry about days, currency, and text
        # accidentally being the same due to random generation
        amount2 = amount + 1
        days2 = random.randint(1, 27)
        currency2 = random.choice(
            [
                "JPY",
                "EUR",
                "USD",
            ]
        )
        text2 = Helpers.generate_string()
        # Let the form succeed and reset the input fields
        # and have the Submit button enabled
        time.sleep(3)
        Helpers.create_a_spending_limit_helper(
            self,
            cat_id,
            amount=amount2,
            days=days2,
            currency=currency2,
            text=text2,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        # Confirm both spending limits are in the list
        spending_data = {
            "cat_id": cat_id,
            "cat_text": cat_text,
            "amount": amount,
            "currency": currency,
            "days": days,
            "text": text,
        }
        Helpers.find_spending_limit(
            self,
            spending_data,
        )
        spending_data2 = {
            "cat_id": cat_id,
            "cat_text": cat_text,
            "amount": amount2,
            "currency": currency2,
            "days": days2,
            "text": text2,
        }
        Helpers.find_spending_limit(
            self,
            spending_data2,
        )

    @Helpers.register_and_login
    def test_spending_limits_are_correctly_displayed_in_dashboard(self):
        """
        Check that spending limits are correctly displayed in the
        dashboard
        """
        # Frank can create a category to log his savings
        cat_text = Helpers.generate_string(20)
        Helpers.create_expense_category(
            self,
            cat_text,
            check_creation_popup=True,
            check_failure_popup=False,
        )
        cat_id = Helpers.check_category_existence(self, cat_text, "categories")

        # Frank can create a spending limit for the category
        amount = random.randint(2, 90000)
        days = random.randint(1, 27)
        currency = random.choice(
            [
                "JPY",
                "EUR",
            ]
        )
        text = Helpers.generate_string()
        Helpers.create_spending_limit(
            self,
            cat_id,
            amount=amount,
            days=days,
            currency=currency,
            text=text,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        spending_data = {
            "cat_id": cat_id,
            "cat_text": cat_text,
            "amount": amount,
            "actually_spent": 0,
            "currency": currency,
            "days": days,
            "text": text,
        }
        # Frank can see the spending limit in the list
        Helpers.find_spending_limit(
            self,
            spending_data,
        )

        # Frank can see the spending limit in the dashboard
        Helpers.find_spending_limit_in_dashboard(
            self,
            spending_data,
        )

    @Helpers.register_and_login
    def test_spending_limits_are_showing_correct_colors_in_dashboard(self):
        """
        Check that spending limits are using the correct classes in the
        dashboard
        """
        # Frank can create a category to log his savings
        cat_text = Helpers.generate_string(20)
        Helpers.create_expense_category(
            self,
            cat_text,
            check_creation_popup=True,
            check_failure_popup=False,
        )
        cat_id = Helpers.check_category_existence(self, cat_text, "categories")

        # Create an expense for the category to confirm the correct usage
        # is displayed in the dashboard
        exp = Helpers.create_an_expense_using_react(
            self,
            cat_id,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        amount = random.randint(2, 90000)
        days = 27  # Let's make it 27 days, so that we do not have to fiddle
        # with the date in the expense

        # Since JPY, EUR, and USD all have different localizations, we need to
        # test all of them
        for curr in [
            "JPY",
            "EUR",
            "USD",
        ]:
            currency = curr

            text = exp["note"]

            Helpers.create_spending_limit(
                self,
                cat_id,
                amount=amount,
                days=days,
                currency=currency,
                text=text,
                check_creation_popup=True,
                check_failure_popup=False,
            )

            spending_data = {
                "cat_id": cat_id,
                "cat_text": cat_text,
                "amount": Decimal(str(amount)),
                "actually_spent": exp["amount"],
                "color": (
                    "text-success"
                    if int(exp["amount"]) < amount
                    else "text-danger"
                ),
                "currency": currency,
                "days": days,
                "text": exp["note"],
            }
            # Frank can see the spending limit in the list
            Helpers.find_spending_limit(
                self,
                spending_data,
            )

            # Frank can see the spending limit in the dashboard
            Helpers.find_spending_limit_in_dashboard(
                self,
                spending_data,
            )

    @skip
    @Helpers.register_and_login
    def test_spending_limits_are_showing_correct_currency_in_dashboard(self):
        self.fail("copy pasted from test_can_create_and_retrieve_expenses")
