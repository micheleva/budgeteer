# Copyright: (c) 2019, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from django.urls import reverse
from selenium.webdriver.common.by import By

import functional_tests.helpers as Helpers
from functional_tests.test_parent import IntegrationTest


class PageAccessIntegrationTest(IntegrationTest):
    """
    Integration test suite for MonthlyBalanceCategories related views
    """

    def setUp(self):
        # Instrument the unit tests to fail if they run for too long
        super().setUp()

    def _can_access_page(self, named_url, title):
        url = reverse(named_url)
        self.browser.get(f"{self.live_server_url}{url}")
        self.assertIn(title, self.browser.title)

    def _cant_access_admin_page(self):
        self.browser.get(f"{self.live_server_url}/admin")
        header_text = self.browser.find_element(By.TAG_NAME, "h1").text
        self.assertIn("not found", header_text.lower())

    @Helpers.register_and_login
    def test_access_to_all_pages(self):
        self._can_access_page("budgets:landing_page", "Budgeteer")
        self._cant_access_admin_page()
