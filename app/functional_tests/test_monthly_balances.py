# Copyright: (c) 2019, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from datetime import date
import random
from unittest import skip

from django.urls import reverse
from selenium.webdriver.common.by import By

import functional_tests.helpers as Helpers
from functional_tests.test_parent import IntegrationTest


# class MonthlyBalancesIntegrationTest(IntegrationTest):
#     """
#     Integration test suite for MonthlyBalanceCategories related views
#     """

#     @skip
#     def test_image_is_displayed_with_data(self):
#         # Frank enters some data (he wants to get his hands on the new graphs asap)
#         # Frank finally opens the monthly balances page and see a shiny graph
#         pass

#     # TODO:write me
#     @skip
#     def test_all_data_table_is_ordered_date_desc(self):
#         pass

#     # TODO:write me
#     @skip
#     def test_specific_month_data_shows_no_graph(self):
#         pass

#     # TODO:write me
#     @skip
#     def test_specific_month_data_shows_total(self):
#         pass

#     # TODO:write me
#     @skip
#     def test_check_whether_image_contains_correct_currency(self):
#         pass

#     # TODO:write me
#     @skip
#     def test_check_both_graph_and_right_side_table_are_shown(self):
#         pass

#     # TODO:write me
#     @skip
#     def test_can_create_multiple_monthly_balance_at_once(self):
#         pass
#         # TODO: add tests for mass creation

#     # @skip  # Rewrite me for the the react frontend
#     # def test_users_cant_see_other_users_monthly_balance_entry(self):
#     #     # Frank can create a category to log his balance
#     #     cat_name = Helpers.generate_string()

#     #     Helpers.create_user(self)
#     #     Helpers.create_a_category(
#     #         self, category_name=cat_name, is_balance=True
#     #     )
#     #     Helpers.logout_user(self)

#     #     # Guido can not see Frank's balance income category
#     #     Helpers.create_user(self)
#     #     Helpers.visit_and_verify_categories(
#     #         self, cat_name, is_balance=True, should_exist=False
#     #     )
#     #     Helpers.logout_user(self)

#     # TODO:write me
#     @skip
#     def test_users_cant_edit_other_users_monthly_balance_entry(self):
#         pass

#     # TODO:write me
#     @skip
#     def test_users_cant_delete_other_users_monthly_balance_entry(self):
#         pass

#     # WRITE ME
#     @skip
#     def test_regression_216_can_resubmit_a_create_form_after_a_failed_submission(
#         self,
#     ):
#         """
#         WRITE ME
#         """
#         pass

#     # WRITE ME
#     @skip
#     def test_regression_216_can_resubmit_an_edit_form_after_a_failed_submission(
#         self,
#     ):
#         """
#         WRITE ME
#         """
#         pass

#     # WRITE ME
#     @skip
#     def test_graph_is_displayed_if_user_has_data(self):
#         """
#         WRITE ME
#         """
#         pass
