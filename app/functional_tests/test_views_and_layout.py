# Copyright: (c) 2019, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from django.urls import reverse
from selenium.webdriver.common.by import By
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

from datetime import date
import os
import subprocess
from unittest import skip

import functional_tests.helpers as Helpers
from functional_tests.test_parent import IntegrationTest

MAX_WAIT_SECONDS = 5


class ViewsAndLayoutIntegrationTest(IntegrationTest):
    """
    Integration test suite for MonthlyBalanceCategories related views
    """

    def setUp(self):
        # Instrument the unit tests to fail if they run for too long
        super().setUp()

    @Helpers.register_and_login
    def test_spa_home_page_has_the_correct_links_in_nav(self):
        # These URL are handled by the react frontend app, hence django has
        # no way to generate them. That's why we use hardcoded strings
        # FIXME add all the new react pages
        today = date.today()
        present_urls = [
            reverse("budgets:home"),
            "/app/categories/creation",
            "/app/categories",
            "app/expenses",
            "/app/expenses/creation",
            "/app/expenses/by-category",
            "/app/expenses/graphs/expvsinc",
            "/app/monthly-balances/categories/creation",
            "/app/monthly-balances/categories",
            f"/app/monthly-balances/multiple-creation/date/{today.strftime('%Y-%m-01')}",
            "/app/monthly-balances/graph",
            "/app/goals/creation",
            "/app/goals",
            "/app/obligations/creation",
            "/app/obligations",
            "/app/accounts/creation",
            "/app/accounts",
            "/app/assets",
            "/app/assets/import",
            "/app/dividends",
            "/app/dividends/import",
            "/app/income-categories/creation",
            "/app/income-categories",
            "/app/incomes/creation",
            "/app/incomes",
            "/app/saving-categories/creation",
            "/app/saving-categories",
            "/app/budgets/creation",
            "/app/budgets/saving/creation",
            f"/app/budgets/date/{today.year}/{today.month}",
            f"/app/budgets/date/{today.strftime('%Y')}",
            "/app/docs",
            "/app/settings",
            "/app/logout",
        ]

        Helpers.find_urls_in_home_page(self, present_urls)

    # Credits http://www.obeythetestinggoat.com/book/
    #         chapter_prettification.html#_static_files_in_django
    # Verify css is properly loaded
    def test_layout_and_styling(self):
        # Frank loads the page
        url = reverse("budgets:landing_page")
        self.browser.get(f"{self.live_server_url}{url}")
        self.browser.set_window_size(1024, 768)

        # Frank notices the logo at the bottom is nicely centered
        inputbox = self.browser.find_element(By.ID, "footer_text")
        self.assertAlmostEqual(
            inputbox.location["x"] + inputbox.size["width"] / 2, 512, delta=10
        )

    # https://docs.djangoproject.com/en/4.2/releases/4.1/
    # Features deprecated in 4.1 => Log out via GET
    @skip
    def test_logout_actually_logs_the_user_out(self):
        pass

    @skip
    def test_create_budgets_for_saving_categories_with_year_only_using_malformed_data_properly_show_error(
        self,
    ):
        pass

    @skip
    def test_create_budgets_for_saving_categories_with_year_and_month_using_malformed_data_properly_show_error(
        self,
    ):
        pass

    @skip
    def test_users_spending_budget_edit_has_correct_back_link_for_year_only_budget(
        self,
    ):
        pass

    @skip
    def test_users_saving_budget_edit_has_correct_back_link_for_year_only_budget(
        self,
    ):
        pass

    @skip
    def test_users_spending_budget_edit_has_correct_back_link_for_year_and_month_budget(
        self,
    ):
        pass

    @skip
    def test_users_saving_budget_edit_has_correct_back_link_for_year_and_month_budget(
        self,
    ):
        pass

    @skip
    def test_year_and_month_budgets_list_view_previous_and_back_buttons_have_the_correct_text(
        self,
    ):
        pass

    @skip
    def test_year_only_budgets_list_view_previous_and_back_buttons_have_the_correct_text(
        self,
    ):
        pass

    def _get_git_commit_hash(self):
        # Check if running in GitLab CI/CD environment
        gitlab_short_commit_sha = os.getenv("CI_COMMIT_SHORT_SHA")

        # Use GitLab environment variable if available
        if gitlab_short_commit_sha is not None:
            return gitlab_short_commit_sha

        try:
            result = subprocess.run(
                ["git", "rev-parse", "--short", "HEAD"],
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE,
                text=True,
                check=True,
            )
            return result.stdout.strip()
        except subprocess.CalledProcessError as e:
            print(f"Error occurred while getting the commit hash: {e.stderr}")
            return None

    @Helpers.register_and_login
    def test_the_navigation_bar_includes_the_git_commit_hash(self):
        # Frank loads the page
        url = "/app"
        self.browser.get(f"{self.live_server_url}{url}")

        try:
            # Frank notices the git commit hash in the navbar
            commit_hash = self._get_git_commit_hash()
            commit_hash_element = WebDriverWait(
                self.browser, MAX_WAIT_SECONDS
            ).until(EC.presence_of_element_located((By.ID, "git_commit_hash")))

            self.assertEqual(
                commit_hash_element.text,
                f"Version: {commit_hash}",
                f"Expected 'Version: {commit_hash}', but got '{commit_hash_element.text}'",
            )
        except TimeoutException:
            self.fail(
                "The git commit hash has not been displayed within the expected time frame,"
                " or the script responsible for injecting it has failed. Fail!"
            )

        except NoSuchElementException:
            self.fail(
                "The git commit hash element was not found, which may indicate that the"
                " layout has changed or the element does not exist. Fail!"
            )
