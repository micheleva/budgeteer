# Copyright: (c) 2019, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
import random
from unittest import skip

from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

import functional_tests.helpers as Helpers
from functional_tests.test_parent import IntegrationTest


class BudgetsIntegrationTest(IntegrationTest):
    """
    Integration test suite for Budgets related views
    """

    def setUp(self):
        # Instrument the unit tests to fail if they run for too long
        super().setUp()

    @Helpers.register_and_login
    def test_can_create_and_list_budgets_for_spending_categories_with_year_and_month(
        self,
    ):  # pylint: disable=R0914; # noqa
        """
        Check that creation and retrival of budgets (with categories, and both
        a year and a month) works correctly
        """
        txt = Helpers.generate_string(10)
        Helpers.create_expense_category(
            self,
            category_name=txt,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        cat_id = Helpers.check_category_existence(self, txt, "categories")

        amount = random.randint(1, 90000)
        year = random.randint(1970, 2025)
        month = random.randint(1, 12)
        Helpers.create_a_budget(
            self,
            cat_type="spending",
            cat_id=cat_id,
            amount=amount,
            allocated_amount=None,
            year=year,
            month=month,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        _ = Helpers.confirm_budget_exists_and_get_its_id(
            self,
            cat_type="spending",
            cat_text=txt,
            year=year,
            month=month,
            amount=amount,
            allocated_amount=None,
            check_failure_popup=False,
            expect_missing_budgets=False,
        )

    @Helpers.register_and_login
    def test_can_create_and_list_budgets_for_saving_categories_with_year_and_month(
        self,
    ):  # pylint: disable=R0914; # noqa
        """
        Check that creation and retrival of budgets (with categories, and both
        a year and a month) works correctly
        """
        txt = Helpers.generate_string(20)
        Helpers.create_saving_category(
            self, txt, check_create_success_popup=True
        )

        cat_id = Helpers.check_category_existence(
            self,
            text=txt,
            url="saving-categories",
            expected_to_fail=False,
        )
        amount = random.randint(1, 90000)
        year = random.randint(1970, 2025)
        month = random.randint(1, 12)
        allocated_amount = random.randint(1, 42000)
        Helpers.create_a_budget(
            self,
            cat_type="saving",
            cat_id=cat_id,
            amount=amount,
            allocated_amount=allocated_amount,
            year=year,
            month=month,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        _ = Helpers.confirm_budget_exists_and_get_its_id(
            self,
            cat_text=txt,
            cat_type="saving",
            year=year,
            month=month,
            amount=amount,
            allocated_amount=allocated_amount,
            check_failure_popup=False,
            expect_missing_budgets=False,
        )

    @Helpers.register_and_login
    def test_can_create_and_list_budgets_for_spending_categories_with_year_only(
        self,
    ):  # pylint: disable=R0914; # noqa
        """
        Check that creation and retrival of budgets (with categories, and
        with only year but no month) works correctly
        """
        cat_text = Helpers.generate_string(10)

        Helpers.create_expense_category(
            self,
            category_name=cat_text,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        cat_id = Helpers.check_category_existence(self, cat_text, "categories")

        amount = random.randint(1, 90000)
        year = random.randint(1970, 2025)
        month = None
        Helpers.create_a_budget(
            self,
            cat_type="spending",
            cat_id=cat_id,
            amount=amount,
            allocated_amount=None,
            year=year,
            month=month,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        _ = Helpers.confirm_budget_exists_and_get_its_id(
            self,
            cat_type="spending",
            cat_text=cat_text,
            amount=amount,
            allocated_amount=None,
            year=year,
            month=month,
            check_failure_popup=False,
            expect_missing_budgets=False,
        )

    @Helpers.register_and_login
    def test_can_create_and_list_budgets_for_saving_categories_with_year_only(
        self,
    ):  # pylint: disable=R0914; # noqa
        """
        Check that creation and retrival of budgets (with categories, and both
        a year and a month) works correctly
        """
        txt = Helpers.generate_string(20)
        Helpers.create_saving_category(
            self, txt, check_create_success_popup=True
        )

        cat_id = Helpers.check_category_existence(
            self,
            text=txt,
            url="saving-categories",
            expected_to_fail=False,
        )
        amount = random.randint(1, 90000)
        year = random.randint(1970, 2025)
        month = None
        allocated_amount = random.randint(1, 42000)
        Helpers.create_a_budget(
            self,
            cat_type="saving",
            cat_id=cat_id,
            amount=amount,
            allocated_amount=allocated_amount,
            year=year,
            month=month,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        _ = Helpers.confirm_budget_exists_and_get_its_id(
            self,
            cat_text=txt,
            cat_type="saving",
            year=year,
            month=month,
            amount=amount,
            allocated_amount=allocated_amount,
            check_failure_popup=False,
            expect_missing_budgets=False,
        )

    @Helpers.register_and_login
    def test_users_can_not_see_other_users_budgets_with_spending_categories(
        self,
    ):
        """Check that a given user can not see other users' budgets"""
        cat_text = Helpers.generate_string(10)

        Helpers.create_expense_category(
            self,
            category_name=cat_text,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        cat_id = Helpers.check_category_existence(self, cat_text, "categories")

        # Create a year/month budget
        amount = random.randint(1, 90000)
        year = random.randint(1970, 2025)
        month = random.randint(1, 12)
        Helpers.create_a_budget(
            self,
            cat_type="spending",
            cat_id=cat_id,
            amount=amount,
            allocated_amount=None,
            year=year,
            month=month,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        _ = Helpers.confirm_budget_exists_and_get_its_id(
            self,
            cat_text=cat_text,
            cat_type="spending",
            year=year,
            month=month,
            amount=amount,
            allocated_amount=None,
            check_failure_popup=False,
            expect_missing_budgets=False,
        )

        # Create a year only budget
        amount2 = random.randint(1, 90000)
        year2 = random.randint(1970, 2025)
        month2 = None
        Helpers.create_a_budget(
            self,
            cat_type="spending",
            cat_id=cat_id,
            amount=amount2,
            allocated_amount=None,
            year=year2,
            month=month2,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        _ = Helpers.confirm_budget_exists_and_get_its_id(
            self,
            cat_text=cat_text,
            cat_type="spending",
            year=year2,
            month=month2,
            amount=amount2,
            allocated_amount=None,
            check_failure_popup=False,
            expect_missing_budgets=False,
        )

        # Frank logs out, he's done today
        Helpers.logout_user(self)

        # Guido logs in: he was spying Frank from behind the sofa, and now
        # Guido knows Frank's budget ID
        usr2_usrname, usr2_pwd = Helpers.create_user(self)
        Helpers.login_user(self, usr2_usrname, usr2_pwd)

        # TODO: access year/month and confirm nothing is there
        # Guido tries to access Frank's budget
        bdgt_id = Helpers.confirm_budget_exists_and_get_its_id(
            self,
            cat_text=cat_text,
            cat_type="spending",
            year=year,
            month=month,
            amount=amount,
            allocated_amount=None,
            check_failure_popup=False,
            expect_missing_budgets=True,
        )
        self.assertEqual(bdgt_id, -1)

        bdgt_id2 = Helpers.confirm_budget_exists_and_get_its_id(
            self,
            cat_text=cat_text,
            cat_type="spending",
            year=year,
            month=month,
            amount=amount,
            allocated_amount=None,
            check_failure_popup=False,
            expect_missing_budgets=True,
        )
        self.assertEqual(bdgt_id2, -1)

    @Helpers.register_and_login
    def test_users_can_edit_own_budgets_with_spendingcategories(self):
        """
        Check that users can edit their own budgets
        """
        cat_text = Helpers.generate_string(10)

        Helpers.create_expense_category(
            self,
            category_name=cat_text,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        cat_id = Helpers.check_category_existence(self, cat_text, "categories")

        # Create and edit a Budget with a category, with a year but without
        # a month
        amount = random.randint(1, 90000)
        year = random.randint(1970, 2025)
        month = None
        Helpers.create_a_budget(
            self,
            cat_type="spending",
            cat_id=cat_id,
            amount=amount,
            allocated_amount=None,
            year=year,
            month=month,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        bdgt_id = Helpers.confirm_budget_exists_and_get_its_id(
            self,
            cat_type="spending",
            cat_text=cat_text,
            amount=amount,
            allocated_amount=None,
            year=year,
            month=month,
            check_failure_popup=False,
            expect_missing_budgets=False,
        )

        new_amount = random.randint(1, 90000)
        # Make sure that the new random value is different from the old value
        while new_amount == amount:
            new_amount += 1
        Helpers.edit_a_budget(
            self,
            bdgt_id,
            new_amount,
            check_edit_success_popup=True,
            check_edit_failure_popup=False,
            check_inability_to_load_budget=False,
        )

        # Confirm the amount edited has been correctly saved
        bdgt_id_conf = Helpers.confirm_budget_exists_and_get_its_id(
            self,
            cat_type="spending",
            cat_text=cat_text,
            amount=new_amount,
            allocated_amount=None,
            year=year,
            month=month,
            check_failure_popup=False,
            expect_missing_budgets=False,
        )
        self.assertEqual(bdgt_id, bdgt_id_conf)

        # Create and edit a Budget with a category witha year and a month
        amount = random.randint(1, 90000)
        year = random.randint(1970, 2025)
        month = random.randint(1, 12)
        Helpers.create_a_budget(
            self,
            cat_type="spending",
            cat_id=cat_id,
            amount=amount,
            allocated_amount=None,
            year=year,
            month=month,
            check_creation_popup=True,
            check_failure_popup=False,
        )
        bdgt_id = Helpers.confirm_budget_exists_and_get_its_id(
            self,
            cat_type="spending",
            cat_text=cat_text,
            amount=amount,
            allocated_amount=None,
            year=year,
            month=month,
            check_failure_popup=False,
            expect_missing_budgets=False,
        )

        new_amount = random.randint(1, 90000)
        # Make sure that the new random value is different from the old value
        while new_amount == amount:
            new_amount += 1
        Helpers.edit_a_budget(
            self,
            bdgt_id,
            new_amount,
            check_edit_success_popup=True,
            check_edit_failure_popup=False,
            check_inability_to_load_budget=False,
        )

        # Confirm the amount edited has been correctly saved
        bdgt_id_conf = Helpers.confirm_budget_exists_and_get_its_id(
            self,
            cat_type="spending",
            cat_text=cat_text,
            amount=new_amount,
            allocated_amount=None,
            year=year,
            month=month,
            check_failure_popup=False,
            expect_missing_budgets=False,
        )
        self.assertEqual(bdgt_id, bdgt_id_conf)

    @Helpers.register_and_login
    def test_users_can_not_edit_other_users_budgets_with_spending_categories(
        self,
    ):
        """Check that a given user can not edit other users' budgets"""
        cat_text = Helpers.generate_string(10)

        Helpers.create_expense_category(
            self,
            category_name=cat_text,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        cat_id = Helpers.check_category_existence(self, cat_text, "categories")

        amount = random.randint(1, 90000)
        year = random.randint(1970, 2025)
        month = random.randint(1, 12)
        Helpers.create_a_budget(
            self,
            cat_type="spending",
            cat_id=cat_id,
            amount=amount,
            allocated_amount=None,
            year=year,
            month=month,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        bdgt_id = Helpers.confirm_budget_exists_and_get_its_id(
            self,
            cat_text=cat_text,
            cat_type="spending",
            year=year,
            month=month,
            amount=amount,
            allocated_amount=None,
            check_failure_popup=False,
            expect_missing_budgets=False,
        )

        # Frank logs out, he's done today
        Helpers.logout_user(self)

        # Guido logs in: he was spying Frank from behind the sofa, and now
        # Guido knows Frank's budget ID
        usr2_usrname, usr2_pwd = Helpers.create_user(self)
        Helpers.login_user(self, usr2_usrname, usr2_pwd)

        # Guido tries to access Frank's budget
        bdgt_id2 = Helpers.confirm_budget_exists_and_get_its_id(
            self,
            cat_text=cat_text,
            cat_type="spending",
            year=year,
            month=month,
            amount=amount,
            allocated_amount=None,
            check_failure_popup=True,
            expect_missing_budgets=True,
        )
        self.assertEqual(bdgt_id2, -1)

        Helpers.visit_a_budget(
            self,
            bdgt_id,
            cat_text,
            cat_type="spending",
            year=year,
            month=month,
            amount=amount,
            allocated_amount=None,
            check_failure_popup=True,
        )

    @Helpers.register_and_login
    def test_regression_216_can_resubmit_a_form_after_a_failed_submission_spending(
        self,
    ):
        """
        Check that it is still possible to submit a form after a failed attempt
        """
        cat_text = Helpers.generate_string(10)

        Helpers.create_expense_category(
            self,
            category_name=cat_text,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        cat_id = Helpers.check_category_existence(self, cat_text, "categories")

        # Create and edit a Budget with a category, with a year but without
        # a month
        amount = random.randint(1, 90000)
        year = random.randint(1970, 2025)
        month = None
        Helpers.create_a_budget(
            self,
            cat_type="spending",
            cat_id=cat_id,
            amount=amount,
            allocated_amount=None,
            year=year,
            month=month,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        # Explicitly wait for the alert to automatically close
        # If we have a success alert opening immediately a failure one, when
        # the timer of the original failure alert will timeout, any open alert
        # will close, not giving enough time to selenium
        # to actually see the success alert (i.e. the second one)
        try:
            WebDriverWait(self.browser, 4).until(
                EC.invisibility_of_element_located(
                    (By.CLASS_NAME, "alert-success")
                )
            )
        except NoSuchElementException:
            # If the alert is not there, it means we're OK to proceed further
            # as the alert has disappeared
            pass
        except TimeoutException:
            # If this times out, it means we're OK to proceed further as the
            # alert has disappeared
            pass

        # Create then a budget with the same data (without refreshing or
        # changing page) and confitm it fails
        Helpers.create_a_budget_without_changing_page(
            self,
            cat_type="spending",
            cat_id=cat_id,
            amount=amount,
            allocated_amount=None,
            year=year,
            month=month,
            check_creation_popup=False,
            check_failure_popup=True,
            check_duplicated_error=True,
        )

        amount2 = random.randint(1, 90000)
        year2 = random.randint(1970, 2025)
        month2 = None
        # Prevent the second item to accidentally have a year/month equal to
        # the first one (e.g. both random end up having the same year)
        while year == year2:
            year2 = random.randint(1970, 2025)

        # Explicitly wait for the alert to automatically close
        # If we have a success alert opening immediately a failure one, when
        # the timer of the original failure alert will timeout, any open alert
        # will close, not giving enough time to selenium to actually see the
        # success alert (i.e. the second one)
        try:
            WebDriverWait(self.browser, 4).until(
                EC.invisibility_of_element_located(
                    (By.CLASS_NAME, "alert-danger")
                )
            )
        except NoSuchElementException:
            # If the alert is not there, it means we're OK to proceed further
            # as the alert has disappeared
            pass
        except TimeoutException:
            # If this times out, it means we're OK to proceed further as the
            # alert has disappeared
            pass

        # dismiss close button
        # Create then a budget with different data, and confitm it suceeds
        # (without refreshing or changing page)
        Helpers.create_a_budget_without_changing_page(
            self,
            cat_type="spending",
            cat_id=cat_id,
            amount=amount2,
            allocated_amount=None,
            year=year2,
            month=month2,
            check_creation_popup=True,
            check_failure_popup=False,
            check_duplicated_error=False,
        )

        # Create and edit a Budget with a category, with a year and a month
        amount = random.randint(1, 90000)
        year = random.randint(1970, 2025)
        month = random.randint(1, 12)
        Helpers.create_a_budget(
            self,
            cat_type="spending",
            cat_id=cat_id,
            amount=amount,
            allocated_amount=None,
            year=year,
            month=month,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        # Explicitly wait for the alert to automatically close
        # If we have a success alert opening immediately a failure one, when
        # the timer of the original failure alert will timeout, any open alert
        # will close, not giving enough time to selenium to actually see the
        # success alert (i.e. the second one)
        try:
            WebDriverWait(self.browser, 4).until(
                EC.invisibility_of_element_located(
                    (By.CLASS_NAME, "alert-success")
                )
            )
        except NoSuchElementException:
            # If the alert is not there, it means we're OK to proceed further
            # as the alert has disappeared
            pass
        except TimeoutException:
            # If this times out, it means we're OK to proceed further as the
            # alert has disappeared
            pass

        # Create then a budget with the same data (without refreshing or
        # changing page) and confitm it fails
        Helpers.create_a_budget_without_changing_page(
            self,
            cat_type="spending",
            cat_id=cat_id,
            amount=amount,
            allocated_amount=None,
            year=year,
            month=month,
            check_creation_popup=False,
            check_failure_popup=True,
            check_duplicated_error=True,
        )

        amount2 = random.randint(1, 90000)
        year2 = random.randint(1970, 2025)
        month2 = random.randint(1, 12)
        # Prevent the second item to accidentally have a year/month equal to
        # the first one (e.g. both random end up having the same year)
        while year == year2:
            year2 = random.randint(1970, 2025)

        # Explicitly wait for the alert to automatically close
        # If we have a success alert opening immediately a failure one, when
        # the timer of the original failure alert will timeout, any open alert
        # will close, not giving enough time  to selenium to actually see the
        # success alert (i.e. the second one)
        try:
            WebDriverWait(self.browser, 4).until(
                EC.invisibility_of_element_located(
                    (By.CLASS_NAME, "alert-danger")
                )
            )
        except NoSuchElementException:
            # If the alert is not there, it means we're OK to proceed further
            # as the alert has disappeared
            pass
        except TimeoutException:
            # If this times out, it means we're OK to proceed further as the
            # alert has disappeared
            pass

        # dismiss close button
        # Create then a budget with different data, and confitm it suceeds
        # (without refreshing or changing page)
        Helpers.create_a_budget_without_changing_page(
            self,
            cat_type="spending",
            cat_id=cat_id,
            amount=amount2,
            allocated_amount=None,
            year=year2,
            month=month2,
            check_creation_popup=True,
            check_failure_popup=False,
            check_duplicated_error=False,
        )

    @Helpers.register_and_login
    def test_regression_216_can_resubmit_a_form_after_a_failed_submission_saving(
        self,
    ):
        """
        Check that it is still possible to submit a form after a failed attempt
        """
        cat_text = Helpers.generate_string(10)

        Helpers.create_saving_category(
            self, cat_text, check_create_success_popup=True
        )

        cat_id = Helpers.check_category_existence(
            self,
            text=cat_text,
            url="saving-categories",
            expected_to_fail=False,
        )
        # Create and edit a Budget with a  Savingcategory, with a year but
        # without a month
        amount = random.randint(1, 90000)
        allocated_amount = random.randint(1, 42)
        year = random.randint(1970, 2025)
        month = None
        Helpers.create_a_budget(
            self,
            cat_type="saving",
            cat_id=cat_id,
            amount=amount,
            allocated_amount=allocated_amount,
            year=year,
            month=month,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        # Explicitly wait for the alert to automatically close
        # If we have a success alert opening immediately a failure one, when
        # the timer of the original failure alert will timeout, any open alert
        # will close, not giving enough time to selenium
        # to actually see the success alert (i.e. the second one)
        try:
            WebDriverWait(self.browser, 4).until(
                EC.invisibility_of_element_located(
                    (By.CLASS_NAME, "alert-success")
                )
            )
        except NoSuchElementException:
            # If the alert is not there, it means we're OK to proceed further
            # as the alert has disappeared
            pass
        except TimeoutException:
            # If this times out, it means we're OK to proceed further as the
            # alert has disappeared
            pass

        # Create then a Saving budget with the same data (without refreshing or
        # changing page) and confitm it fails
        Helpers.create_a_budget_without_changing_page(
            self,
            cat_type="saving",
            cat_id=cat_id,
            amount=amount,
            allocated_amount=allocated_amount,
            year=year,
            month=month,
            check_creation_popup=False,
            check_failure_popup=True,
            check_duplicated_error=True,
        )

        amount2 = random.randint(1, 90000)
        allocated_amount = random.randint(1, 42)
        year2 = random.randint(1970, 2025)
        month2 = None
        # Prevent the second item to accidentally have a year/month equal to
        # the first one (e.g. both random end up having the same year)
        while year == year2:
            year2 = random.randint(1970, 2025)

        # Explicitly wait for the alert to automatically close
        # If we have a success alert opening immediately a failure one, when
        # the timer of the original failure alert will timeout, any open alert
        # will close, not giving enough time to selenium to actually see the
        # success alert (i.e. the second one)
        try:
            WebDriverWait(self.browser, 4).until(
                EC.invisibility_of_element_located(
                    (By.CLASS_NAME, "alert-danger")
                )
            )
        except NoSuchElementException:
            # If the alert is not there, it means we're OK to proceed further
            # as the alert has disappeared
            pass
        except TimeoutException:
            # If this times out, it means we're OK to proceed further as the
            # alert has disappeared
            pass

        # dismiss close button
        # Create then a Saving budget with different data, and confitm it
        # suceeds (without refreshing or changing page)
        Helpers.create_a_budget_without_changing_page(
            self,
            cat_type="saving",
            cat_id=cat_id,
            amount=amount2,
            allocated_amount=allocated_amount,
            year=year2,
            month=month2,
            check_creation_popup=True,
            check_failure_popup=False,
            check_duplicated_error=False,
        )

        # Create and edit a Budget with a Saving category,
        #  with a year and a month
        amount = random.randint(1, 90000)
        allocated_amount = random.randint(1, amount)
        year = random.randint(1970, 2025)
        month = random.randint(1, 12)
        Helpers.create_a_budget(
            self,
            cat_type="saving",
            cat_id=cat_id,
            amount=amount,
            allocated_amount=allocated_amount,
            year=year,
            month=month,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        # Explicitly wait for the alert to automatically close
        # If we have a success alert opening immediately a failure one, when
        # the timer of the original failure alert will timeout, any open alert
        # will close, not giving enough time to selenium to actually see the
        # success alert (i.e. the second one)
        try:
            WebDriverWait(self.browser, 4).until(
                EC.invisibility_of_element_located(
                    (By.CLASS_NAME, "alert-success")
                )
            )
        except NoSuchElementException:
            # If the alert is not there, it means we're OK to proceed further
            # as the alert has disappeared
            pass
        except TimeoutException:
            # If this times out, it means we're OK to proceed further as the
            # alert has disappeared
            pass

        # Create then a Saving budget with the same data (without refreshing or
        # changing page) and confitm it fails
        Helpers.create_a_budget_without_changing_page(
            self,
            cat_type="saving",
            cat_id=cat_id,
            amount=amount,
            allocated_amount=allocated_amount,
            year=year,
            month=month,
            check_creation_popup=False,
            check_failure_popup=True,
            check_duplicated_error=True,
        )

        amount2 = random.randint(1, 90000)
        allocated_amount2 = random.randint(1, amount2)
        year2 = random.randint(1970, 2025)
        month2 = random.randint(1, 12)
        # Prevent the second item to accidentally have a year/month equal to
        # the first one (e.g. both random end up having the same year)
        while year == year2:
            year2 = random.randint(1970, 2025)

        # Explicitly wait for the alert to automatically close
        # If we have a success alert opening immediately a failure one, when
        # the timer of the original failure alert will timeout, any open alert
        # will close, not giving enough time  to selenium to actually see the
        # success alert (i.e. the second one)
        try:
            WebDriverWait(self.browser, 4).until(
                EC.invisibility_of_element_located(
                    (By.CLASS_NAME, "alert-danger")
                )
            )
        except NoSuchElementException:
            # If the alert is not there, it means we're OK to proceed further
            # as the alert has disappeared
            pass
        except TimeoutException:
            # If this times out, it means we're OK to proceed further as the
            # alert has disappeared
            pass

        # dismiss close button
        # Create then a  Savingbudget with different data, and confitm it
        # suceeds (without refreshing or changing page)
        Helpers.create_a_budget_without_changing_page(
            self,
            cat_type="saving",
            cat_id=cat_id,
            amount=amount2,
            allocated_amount=allocated_amount2,
            year=year2,
            month=month2,
            check_creation_popup=True,
            check_failure_popup=False,
            check_duplicated_error=False,
        )

    @Helpers.register_and_login
    def test_can_create_and_list_multiple_budgets_for_spending_categories_with_year_and_month(
        self,
    ):  # pylint: disable=R0914; # noqa
        """
        Check that creation and retrival of budgets (with categories, and both
        a year and a month) works correctly
        """
        txt = Helpers.generate_string(10)
        Helpers.create_expense_category(
            self,
            category_name=txt,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        cat_id = Helpers.check_category_existence(self, txt, "categories")

        amount = random.randint(1, 90000)
        year = random.randint(1970, 2025)
        month = random.randint(1, 12)
        Helpers.create_a_budget(
            self,
            cat_type="spending",
            cat_id=cat_id,
            amount=amount,
            allocated_amount=None,
            year=year,
            month=month,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        _ = Helpers.confirm_budget_exists_and_get_its_id(
            self,
            cat_type="spending",
            cat_text=txt,
            year=year,
            month=month,
            amount=amount,
            allocated_amount=None,
            check_failure_popup=False,
            expect_missing_budgets=False,
        )

        txt2 = Helpers.generate_string(10)
        Helpers.create_expense_category(
            self,
            category_name=txt2,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        # Create a spending budget for the same year/month, but with a
        # different Spending Category
        cat_id2 = Helpers.check_category_existence(self, txt2, "categories")
        amount = random.randint(1, 90000)
        Helpers.create_a_budget(
            self,
            cat_type="spending",
            cat_id=cat_id2,
            amount=amount,
            allocated_amount=None,
            year=year,
            month=month,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        _ = Helpers.confirm_budget_exists_and_get_its_id(
            self,
            cat_type="spending",
            cat_text=txt2,
            year=year,
            month=month,
            amount=amount,
            allocated_amount=None,
            check_failure_popup=False,
            expect_missing_budgets=False,
        )

    @Helpers.register_and_login
    def test_can_create_and_list_multiple_budgets_for_saving_categories_with_year_and_month(
        self,
    ):  # pylint: disable=R0914; # noqa
        """
        Check that creation and retrival of budgets (with categories, and both
        a year and a month) works correctly
        """
        txt = Helpers.generate_string(20)
        Helpers.create_saving_category(
            self, txt, check_create_success_popup=True
        )
        cat_id = Helpers.check_category_existence(
            self,
            text=txt,
            url="saving-categories",
            expected_to_fail=False,
        )
        amount = random.randint(1, 90000)
        allocated_amount = random.randint(1, amount)
        year = random.randint(1970, 2025)
        month = random.randint(1, 12)
        Helpers.create_a_budget(
            self,
            cat_type="saving",
            cat_id=cat_id,
            amount=amount,
            allocated_amount=allocated_amount,
            year=year,
            month=month,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        _ = Helpers.confirm_budget_exists_and_get_its_id(
            self,
            cat_type="saving",
            cat_text=txt,
            year=year,
            month=month,
            amount=amount,
            allocated_amount=allocated_amount,
            check_failure_popup=False,
            expect_missing_budgets=False,
        )

        # Create a Saving budget for the same year/month, but with a
        # different Saving Category
        txt2 = Helpers.generate_string(20)
        Helpers.create_saving_category(
            self, txt2, check_create_success_popup=True
        )
        cat_id2 = Helpers.check_category_existence(
            self,
            text=txt2,
            url="saving-categories",
            expected_to_fail=False,
        )
        amount2 = random.randint(1, 90000)
        allocated_amount2 = random.randint(1, amount)

        Helpers.create_a_budget(
            self,
            cat_type="saving",
            cat_id=cat_id2,
            amount=amount2,
            allocated_amount=allocated_amount2,
            year=year,
            month=month,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        _ = Helpers.confirm_budget_exists_and_get_its_id(
            self,
            cat_type="saving",
            cat_text=txt2,
            year=year,
            month=month,
            amount=amount2,
            allocated_amount=allocated_amount2,
            check_failure_popup=False,
            expect_missing_budgets=False,
        )

    # WRITE ME
    @skip
    def test_correct_guidance_for_spending_budegts_in_dashboard(self):
        pass

    # WRITE ME
    @skip
    def test_correct_guidance_for_saving_budegts_in_dashboard(self):
        pass

    # WRITE ME
    @skip
    def test_correct_guidance_for_saving_budegts_in_list_monhtly_budgets(self):
        pass

    # WRITE ME
    @skip
    def test_correct_guidance_for_spending_budgets_in_list_monhtly_budgets(
        self,
    ):
        self.fail(
            "Monthly Saving Budgets You need to create a Saving Category first! "
        )
        self.fail(
            "Yearly Saving Budgets You need to create a Saving Category first! "
        )
        self.fail(
            "Monthly Overbudgeted Amounts You have no Monthly Spending Budgets for this year yet: go and create some!"
        )

    @Helpers.register_and_login
    def test_correct_guidance_for_saving_budgets_in_list_yearly_budgets(self):
        title = "Yearly Saving Budgets"
        guidance = "You need to create a Saving Category first!"
        button_text = "Create a Saving Category"
        button_url = "/app/saving-categories/creation"
        Helpers.guidance_check(
            self,
            title=title,
            guidance=guidance,
            button_text=button_text,
            header_class="text-info",
            button_url=button_url,
            expect_to_be_missing=False,
        )

        title = "Monthly Saving Budgets"
        guidance = "You need to create a Saving Category first!"
        button_text = "Create a Saving Category"
        button_url = "/app/saving-categories/creation"
        Helpers.guidance_check(
            self,
            title=title,
            guidance=guidance,
            button_text=button_text,
            header_class="text-info",
            button_url=button_url,
            expect_to_be_missing=False,
        )

    # WRITE ME
    @skip
    def test_correct_guidance_for_spending_budgets_in_list_yearly_budgets(
        self,
    ):
        pass

    # WRITE ME
    @skip
    def test_saving_budgets_are_showed_in_monthly_budgets_page(self):
        pass

    # WRITE ME
    @skip
    def test_saving_budgets_are_showed_in_yearly_budgets_page(self):
        pass

    # WRITE ME
    @skip
    def test_saving_budgets_allocated_amount_are_showed_in_monthly_budgets_page(
        self,
    ):
        pass

    # WRITE ME
    @skip
    def test_saving_budgets_allocated_amount_are_showed_in_yearly_budgets_page(
        self,
    ):
        pass

    # WRITE ME
    @skip
    def test_create_duplicated_budgets_for_categories_with_year_and_months_properly_show_errors(
        self,
    ):
        """
        WRITE ME
        """
        self.fail("write me")

    # WRITE ME
    @skip
    def test_create_duplicated_budgets_for_categories_with_year_only_properly_show_errors(
        self,
    ):
        """
        WRITE ME
        """
        self.fail("write me")

    # WRITE ME
    @skip
    def test_create_budgets_for_categories_with_year_only_using_malformed_data_properly_show_errors(
        self,
    ):
        """
        WRITE ME
        """
        self.fail("write me")

    # WRITE ME
    @skip
    def test_create_budgets_for_categories_with_year_and_month_using_malformed_data_properly_show_errors(
        self,
    ):
        """
        WRITE ME
        """
        self.fail("write me")

    # WRITE ME
    @skip
    def test_users_budget_edit_has_correct_back_link_for_year_only_budgets(
        self,
    ):
        """
        WRITE ME
        """
        self.fail("write me")

    # WRITE ME
    @skip
    def test_users_budget_edit_has_correct_back_link_for_year_and_month_budgets(
        self,
    ):
        """
        WRITE ME
        """
        self.fail("write me")

    # WRITE ME
    @skip
    def test_year_and_month_budgets_list_view_previous_and_back_buttons_have_the_correct_text(
        self,
    ):
        """
        WRITE ME
        """
        self.fail("write me")

    # WRITE ME
    @skip
    def test_year_only_budgets_list_view_previous_and_back_buttons_have_the_correct_text(
        self,
    ):
        """
        WRITE ME
        """
        self.fail("write me")
