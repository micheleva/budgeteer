# Copyright: (c) 2019, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
import http.server
import threading
from unittest import skip

from selenium.webdriver.common.by import By

import functional_tests.helpers as Helpers
from functional_tests.helpers import get_live_server_test_case_socket_name
from functional_tests.test_parent import IntegrationTest


class MockDividendsAPI(threading.Thread):
    """
    Simple HTTP server to serve fake data
    """

    # TODO: Add argument for port number, if none is provided, use 13504
    def __init__(self, port=13504):
        super().__init__()
        self.server = None
        self.port = port

    def setUp(self):
        # Instrument the unit tests to fail if they run for too long
        super().setUp()

    def run(self):
        class MockServerHandler(http.server.SimpleHTTPRequestHandler):
            def end_headers(self):
                self.send_header("Access-Control-Allow-Origin", "*")
                super().end_headers()

            def do_GET(self):
                if self.path.endswith(".json"):
                    self.send_response(200)
                    self.send_header("Content-type", "application/json")
                    self.end_headers()
                    if "malformed_data_only" in self.path:
                        fake_data = MockDividendsAPI.invalid_data()[
                            "string_data"
                        ]
                    elif "mixed_data" in self.path:
                        fake_data = MockDividendsAPI.partially_invalid_data()[
                            "string_data"
                        ]
                    else:
                        fake_data = MockDividendsAPI.valid_data()[
                            "string_data"
                        ]
                    self.wfile.write(fake_data.encode())
                else:
                    # TODO, should we return an empty array instead?
                    self.send_response(404)
                    self.end_headers()
                    self.wfile.write(b"Not Found")

        socket_name = get_live_server_test_case_socket_name()
        self.server = http.server.ThreadingHTTPServer(
            (socket_name, self.port), MockServerHandler
        )
        self.server.serve_forever()

    def stop(self):
        if self.server:
            self.server.shutdown()
            self.server.server_close()

    @classmethod
    def invalid_data(cls):
        string_data = """[
        {
            "da": "2023-10-04",
            "cu": "USD",
            "ti": "AAA",
            "am": 9.41,
            "ra": 110.19
        },
        {
            "date": "2023-10-04",
            "un_necessary_field": 42,
            "currency": "USD",
            "ticker": "BBB",
            "amount": 2.55,
            "rate": 110.19
        },
        {
            "date": {"nested_prop": 1337},
            "currency": 0.42,
            "ticker": null,
            "amount": "i am a string",
            "rate": "i am another string"
        },
        {
            "date": "2023-10-04",
            "un_necessary_field": 42,
            "currency": "USD",
            "ticker": "CCC",
            "amount": 42,
            "rate": 110.19
        }
        ]"""
        json_data = []
        return {"string_data": string_data, "json_data": json_data}

    @classmethod
    def partially_invalid_data(cls):
        string_data = """[
        {
            "da": "2023-10-04",
            "cu": "USD",
            "ti": "ROTFL",
            "am": 9.41,
            "ra": 110.19
        },
        {
            "date": "2023-10-04",
            "currency": "USD",
            "ticker": "LOL",
            "amount": 42,
            "rate": 110.19
        },
        {
            "date": {"nested_prop": 1337},
            "currency": 0.42,
            "ticker": null,
            "amount": "i am a string",
            "rate": "i am another string"
        }
        ]"""
        json_data = [
            {
                "date": "2023-10-04",
                "currency": "USD",
                "ticker": "LOL",
                "amount": 42,
                "rate": 110.19,
            }
        ]
        return {"string_data": string_data, "json_data": json_data}

    @classmethod
    def valid_data(cls):
        string_data = """[
        {
            "date": "2023-10-04",
            "currency": "USD",
            "ticker": "AAA",
            "amount": 9.41,
            "rate": 110.19
        },
        {
            "date": "2023-10-04",
            "currency": "USD",
            "ticker": "BBB",
            "amount": 2.55,
            "rate": 110.19
        }
        ]"""
        json_data = [
            {
                "date": "2023-10-04",
                "currency": "USD",
                "ticker": "AAA",
                "amount": 9.41,
                "rate": 110.19,
            },
            {
                "date": "2023-10-04",
                "currency": "USD",
                "ticker": "BBB",
                "amount": 2.55,
                "rate": 110.19,
            },
        ]
        return {"string_data": string_data, "json_data": json_data}


class DividendsIntegrationTest(IntegrationTest):
    """
    Integration test suite for Dividend related views
    """

    def setUp(self):
        # Instrument the unit tests to fail if they run for too long
        super().setUp()

    @skip
    def test_can_create_dividends(self):  # pylint: disable=R0914; # noqa
        pass

    # WRITE ME
    @skip
    def test_user_can_see_correct_yearly_aggregate(self):
        pass

    # WRITE ME
    @skip
    def test_user_can_see_correct_symbol_aggregate(self):
        pass

    # WRITE ME
    @skip
    def test_displays_calendar_if_user_has_future_dividend_dates(self):
        """
        WRITE ME
        """
        pass


class ThirdPartyAPIIntegrationTest(IntegrationTest):
    """Integration test for third party APIs"""

    def setUp(self):
        # Instrument the unit tests to fail if they run for too long
        super().setUp()
        self.mock_server = MockDividendsAPI()
        self.mock_server.daemon = True
        self.mock_server.start()

    def tearDown(self):
        self.mock_server.stop()

    @Helpers.register_and_login
    def test_can_import_and_list_correctly_formatted_dividends_api_results(
        self,
    ):
        acc_name = Helpers.generate_string(10)
        credit_entity_name = Helpers.generate_string(10)
        # Let's only test foreign currencies for now
        is_foreign_currency = True
        Helpers.create_an_account(
            tester=self,
            acc_name=acc_name,
            credit_entity_name=credit_entity_name,
            is_foreign_currency=is_foreign_currency,
            check_create_success_popup=True,
            check_create_failure_popup=False,
        )

        socket_name = get_live_server_test_case_socket_name()
        url = f"http://{socket_name}:13504/dividends/"
        Helpers.import_dividends_from_third_party_api(
            tester=self, apiURL=url, target_date="2023-07-01"
        )
        # Use hardcoded values from above for now
        expected_dividends = self.mock_server.valid_data()["json_data"]
        Helpers.list_dividends(tester=self, dividends=expected_dividends)

    @Helpers.register_and_login
    def test_importing_correct_and_malformed_data_will_properly_show_errors(
        self,
    ):
        # IMPROTANT: The account name is used by the helper to return good and
        # bad data:DO NOT change this
        acc_name = "mixed_data"
        credit_entity_name = Helpers.generate_string(10)
        # Let's only test foreign currencies for now
        is_foreign_currency = True
        Helpers.create_an_account(
            tester=self,
            acc_name=acc_name,
            credit_entity_name=credit_entity_name,
            is_foreign_currency=is_foreign_currency,
            check_create_success_popup=True,
            check_create_failure_popup=False,
        )

        socket_name = get_live_server_test_case_socket_name()
        target_date = "2023-07-01"
        url = f"http://{socket_name}:13504/dividends/"
        Helpers.import_dividends_from_third_party_api(
            tester=self,
            apiURL=url,
            target_date=target_date,
            check_success_text=False,
        )

        alerts = self.browser.find_elements(By.CLASS_NAME, "alert")
        self.assertTrue(len(alerts), 2)

        # Since the error popus displaying timing might alter the order in
        # whichthey're showed in the DOM, we confirm we're selecting the
        # correct one
        results_alert_text = alerts[0].text
        if "An error has accourred. Please try again." in results_alert_text:
            results_alert_text = alerts[1].text

        substrings_to_check = [
            f"1/1 accounts have data for {target_date}",
            "Created 1/3 dividends entries",
            "returned 2 malformed data point(s)",
        ]
        for substring in substrings_to_check:
            self.assertIn(substring, results_alert_text)

        # Use hardcoded values from above for now
        expected_dividends = self.mock_server.partially_invalid_data()[
            "json_data"
        ]
        Helpers.list_dividends(tester=self, dividends=expected_dividends)

    @Helpers.register_and_login
    def test_importing_only_malformed_data_will_properly_show_errors(self):
        # The account name is used by the helper to return only bad data
        # do not change this
        acc_name = "malformed_data_only"
        credit_entity_name = Helpers.generate_string(10)
        # Let's only test foreign currencies for now
        is_foreign_currency = True
        Helpers.create_an_account(
            tester=self,
            acc_name=acc_name,
            credit_entity_name=credit_entity_name,
            is_foreign_currency=is_foreign_currency,
            check_create_success_popup=True,
            check_create_failure_popup=False,
        )

        socket_name = socket_name = get_live_server_test_case_socket_name()
        url = f"http://{socket_name}:13504/dividends/"
        Helpers.import_dividends_from_third_party_api(
            tester=self,
            apiURL=url,
            target_date="2023-07-01",
            check_success_text=False,
        )

        alerts = self.browser.find_elements(By.CLASS_NAME, "alert")

        # No dividends were created, so no error should be returned
        # by the server
        self.assertTrue(len(alerts), 1)
        results_alert_text = alerts[0].text
        self.assertNotIn("dividend(s) creation failed.", results_alert_text)

        substrings_to_check = [
            "1/1 accounts have data for",
            "Created 0/4 dividends entries",
            "returned 4 malformed data point(s)",
        ]
        results_alert_text = alerts[0].text
        for substring in substrings_to_check:
            self.assertIn(substring, results_alert_text)
