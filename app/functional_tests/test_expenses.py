# Copyright: (c) 2019, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from datetime import date
from datetime import timedelta
import random

from unittest import skip

from api.v1.tests.test_base import BaseApiTest

import functional_tests.helpers as Helpers
from functional_tests.test_parent import IntegrationTest

MAX_WAIT_SECONDS = 3


class ExpensesIntegrationTest(IntegrationTest):
    """
    Integration test suite for Expenses related views
    """

    def setUp(self):
        # Instrument the unit tests to fail if they run for too long
        super().setUp()

    @Helpers.register_and_login
    def test_can_create_and_retrieve_expenses(self):
        """Check that creation and retrival of expenses works correctly in the react app"""
        # Frank creates a category to log his food expenses
        cat_text = Helpers.generate_string()
        Helpers.create_expense_category(
            self, cat_text, check_creation_popup=True
        )
        # Grab category ID
        cat_id = Helpers.check_category_existence(self, cat_text, "categories")

        # Frank actually creates the expense
        check_creation_popup = True
        check_failure_popup = False
        exp = Helpers.create_an_expense_using_react(
            self,
            cat_id,
            check_creation_popup=check_creation_popup,
            check_failure_popup=check_failure_popup,
        )

        # Frank is happy with the results
        _ = Helpers.find_expense_in_date_range(
            self,
            {
                "category_text": cat_text,
                "amount": int(exp["amount"]),
                "date": exp["date"],
                "note": exp["note"],
            },
            {
                "start": exp["date"],
                "end": exp["date"],
            },
            expected_to_fail=False,
        )

        # Frank creates a second expense, he wants to make sure the app can
        # handle multiple expenses
        exp2 = Helpers.create_an_expense_using_react(
            self,
            cat_id,
            check_creation_popup=check_creation_popup,
            check_failure_popup=check_failure_popup,
        )

        # Frank is happy with the results
        _ = Helpers.find_expense_in_date_range(
            self,
            {
                "category_text": cat_text,
                "amount": int(exp2["amount"]),
                "date": exp2["date"],
                "note": exp2["note"],
            },
            {
                "start": exp["date"],
                "end": exp["date"],
            },
            expected_to_fail=False,
        )

    @Helpers.register_and_login
    def test_users_can_not_see_other_users_expenses(self):
        """Check that only current user expenses are returned"""
        # Frank creates an expense category to log his expenses
        cat_text = Helpers.generate_string()
        Helpers.create_expense_category(
            self, cat_text, check_creation_popup=True
        )
        # Grab category ID
        cat_id = Helpers.check_category_existence(self, cat_text, "categories")

        check_creation_popup = True
        check_failure_popup = False
        exp = Helpers.create_an_expense_using_react(
            self,
            cat_id,
            check_creation_popup=check_creation_popup,
            check_failure_popup=check_failure_popup,
        )

        # Frank is happy with the results
        exp_id = Helpers.find_expense_in_date_range(
            self,
            {
                "category_text": cat_text,
                "amount": int(exp["amount"]),
                "date": exp["date"],
                "note": exp["note"],
            },
            {
                "start": exp["date"],
                "end": exp["date"],
            },
            expected_to_fail=False,
        )

        # Guido wants to reference Frank's expense data
        # and tell said info to everyone in the world!
        Helpers.create_user(self)

        # Guido hopes to find Frank's expense data in the expense list
        _ = Helpers.find_expense_in_date_range(
            self,
            {
                "category_text": cat_text,
                "amount": int(exp["amount"]),
                "date": exp["date"],
                "note": exp["note"],
            },
            {
                "start": exp["date"],
                "end": exp["date"],
            },
            expected_to_fail=True,
        )

        # Guido then tries to access the expense edit page:
        # maybe the developers forgot to check about privileges in here!
        check_failure_popup = True
        # Guido is sad, as a red alert is alerting him that
        # he's not able to reference this particular expense

        Helpers.visit_an_expense(
            self,
            exp_id,
            cat_text,
            cat_id,
            exp["date"],
            exp["note"],
            check_failure_popup,
        )

    # No need to be implemented
    @skip
    def test_users_cant_edit_other_users_expenses(self):
        """
        Check that the expenses can not be edited to use other users's categories
        """
        # As users can't see other users expenses, this test is not needed
        pass

    # No need to be implemented
    @skip
    def test_users_cant_delete_other_users_expenses(self):
        """Check that only the expenses owner can delete them"""
        # As users can't see other users expenses, this test is not needed
        pass

    # No need to be implemented
    @skip
    def test_users_can_not_edit_other_users_expenses(self):
        """
        Check that the expenses can not be edited to use other users's
        categories when using the react app
        """
        # As users can't see other users expenses, this test is not needed
        pass

    @Helpers.register_and_login
    def test_users_can_edit_expenses(self):
        """
        Check the expenses owner can edit them
        """
        # Frank creates an expense category to log his expenses
        cat_text = Helpers.generate_string()
        Helpers.create_expense_category(
            self, cat_text, check_creation_popup=True
        )
        cat_id = Helpers.check_category_existence(
            self, text=cat_text, url="categories", expected_to_fail=False
        )
        assert cat_id is not None and cat_id != -1

        check_creation_popup = True
        check_failure_popup = False
        exp = Helpers.create_an_expense_using_react(
            self,
            cat_id,
            check_creation_popup=check_creation_popup,
            check_failure_popup=check_failure_popup,
        )

        exp_id = Helpers.find_expense_in_date_range(
            self,
            {
                "category_text": cat_text,
                "amount": exp["amount"],
                "date": exp["date"],
                "note": exp["note"],
            },
            {
                "start": exp["date"],
                "end": exp["date"],
            },
            expected_to_fail=False,
        )
        assert exp_id is not None and exp_id != -1

        # Frank changes his mind, and he decides to alter the expense details:
        # he was looking at the wrong receipt!
        cat2_text = Helpers.generate_string()
        Helpers.create_expense_category(
            self,
            category_name=cat2_text,
            check_creation_popup=True,
            check_failure_popup=False,
        )
        cat2_id = Helpers.check_category_existence(
            self, cat2_text, "categories", expected_to_fail=False
        )
        assert cat2_id is not None and cat2_id != -1

        second_amount = BaseApiTest.generate_number(
            max_digits=10, decimal_places=2
        )
        # Gererate a random date that is between 42 and 365 days ago
        more_than_six_months_ago = date.today() - timedelta(
            days=random.randint(42, 365)
        )
        second_expense_date = more_than_six_months_ago.isoformat()
        second_note = Helpers.generate_string()
        Helpers.edit_an_expense(
            expense_id=exp_id,
            category_id=cat2_id,
            amount=second_amount,
            date=second_expense_date,
            note=second_note,
            tester=self,
            check_denied_permission_popup=False,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        # Franks is very methodical, and decideds to confirm the expens
        # details were saved correctly after his editing
        _ = Helpers.find_expense_in_date_range(
            self,
            {
                "category_text": cat2_text,
                "amount": second_amount,
                "date": second_expense_date,
                "note": second_note,
            },
            {
                "start": second_expense_date,
                "end": second_expense_date,
            },
            expected_to_fail=False,
        )

        _ = Helpers.find_expense_in_date_range(
            self,
            {
                "category_text": cat_text,
                "amount": exp["amount"],
                "date": exp["date"],
                "note": exp["note"],
            },
            {
                "start": exp["date"],
                "end": exp["date"],
            },
            expected_to_fail=True,
        )

    @Helpers.register_and_login
    def test_users_can_delete_expenses(self):
        """Check that the expenses owner can delete them"""
        cat_name = Helpers.generate_string()
        Helpers.create_expense_category(
            self,
            category_name=cat_name,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        cat_id = Helpers.check_category_existence(self, cat_name, "categories")

        # Frank reads on his notebook what he bought today
        check_creation_popup = True
        check_failure_popup = False
        exp = Helpers.create_an_expense_using_react(
            self,
            cat_id,
            check_creation_popup=check_creation_popup,
            check_failure_popup=check_failure_popup,
        )
        print("REFACTOR ME: Randomize the date of the expense", flush=True)

        amount = exp["amount"]
        note = exp["note"]
        exp_date = exp["date"]
        exp_id = Helpers.find_expense_in_date_range(
            self,
            {
                "category_text": cat_name,
                "amount": amount,
                "date": exp_date,
                "note": note,
            },
            {
                "start": exp_date,
                "end": exp_date,
            },
            expected_to_fail=False,
        )
        assert exp_id is not None and exp_id != -1

        Helpers.delete_an_expense(
            self,
            exp_id,
            exp_date,
            check_successful_deletion_popup=True,
            check_failed_deletion_popup=False,
        )

        # Frank is happy that his expenses is gone forever
        exp_id = Helpers.find_expense_in_date_range(
            self,
            {
                "category_text": cat_name,
                "amount": amount,
                "date": exp_date,
                "note": note,
            },
            {
                "start": exp_date,
                "end": exp_date,
            },
            expected_to_fail=True,
        )
        self.assertEqual(exp_id, -1)

    # WRITE ME
    @skip
    def test_cant_create_malformed_expenses(self):
        pass

    @Helpers.register_and_login
    def test_only_expenses_in_range_are_shown(self):
        """Check that expenses are correctly filtered when using a given url"""

        # Frank creates an expense
        cat_text = Helpers.generate_string()
        Helpers.create_expense_category(
            self, cat_text, check_creation_popup=True
        )
        # Grab category ID
        cat_id = Helpers.check_category_existence(self, cat_text, "categories")

        # Frank actually creates the expenses
        amount = BaseApiTest.generate_number(max_digits=10, decimal_places=2)
        expense_date = date.today() - timedelta(days=random.randint(300, 365))
        expense_date_str = expense_date.isoformat()
        note = Helpers.generate_string()
        Helpers.create_an_expense(
            self,
            cat_id,
            amount=amount,
            date=expense_date_str,
            note=note,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        amount2 = BaseApiTest.generate_number(max_digits=10, decimal_places=2)
        expense_date_str2 = date.today().isoformat()
        note2 = Helpers.generate_string()
        Helpers.create_an_expense(
            self,
            cat_id,
            amount=amount2,
            date=expense_date_str2,
            note=note2,
            check_creation_popup=True,
            check_failure_popup=False,
        )

        # Franks expects the first expense to be shown if he filters
        # the expenses by the first expense date
        Helpers.find_expense_in_date_range(
            self,
            {
                "category_text": cat_text,
                "amount": amount,
                "date": expense_date_str,
                "note": note,
            },
            {
                "start": expense_date_str,
                "end": expense_date_str,
            },
            expected_to_fail=False,
        )

        # Franks expects the second expense not to be shown if he filters
        # the expenses by the first expense date
        Helpers.find_expense_in_date_range(
            self,
            {
                "category_text": cat_text,
                "amount": amount2,
                "date": expense_date_str2,
                "note": note2,
            },
            {
                "start": expense_date_str,
                "end": expense_date_str,
            },
            expected_to_fail=True,
        )

    @skip  # Rewrite me for the the react frontend
    def test_users_cant_see_other_users_expenses(self):
        """Check that the expenses are displayed only for the correct owner"""
        pass

    @skip  # Rewrite me for the the react frontend
    def test_users_cant_create_expenses_with_other_users_categories(self):
        """Check that the expenses can not be created with other users's categories"""
        pass

    # No need to be implemented
    @skip
    def test_users_can_see_error_if_category_is_missing_when_submitting_form(
        self,
    ):
        """
        Check that the user is alerted if the category is missing when submitting the form
        This is actually not possible, as by design a category is always selected
        """
        pass

    # WRITE ME
    @skip
    def test_users_can_see_error_if_amount_is_less_than_1(self):
        pass

    # WRITE ME
    @skip
    def test_regression_216_can_resubmit_a_form_after_a_failed_submission(
        self,
    ):
        """
        WRITE ME
        """
        pass

    # WRITE ME
    @skip
    def test_expenses_limit_is_empty_if_no_env_for_this_user(self):
        """
        WRITE ME
        """
        pass

    # WRITE ME
    @skip
    def test_expenses_limit_is_well_formatted_if_env_is_present(self):
        """
        WRITE ME
        """
        pass
