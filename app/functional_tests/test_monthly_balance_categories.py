# Copyright: (c) 2019, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from unittest import skip

import functional_tests.helpers as Helpers
from functional_tests.test_parent import IntegrationTest


class MonthlyBalanceCategoriesIntegrationTest(IntegrationTest):
    """
    Integration test suite for MonthlyBalanceCategories related views
    """

    def setUp(self):
        # Instrument the unit tests to fail if they run for too long
        super().setUp()

    @Helpers.register_and_login
    def test_can_create_monthly_balance_categories(
        self,
    ):  # pylint: disable=R0914; # noqa  #Refactor me out to complete #46
        pass

    @skip  # Rewrite me for the the react frontend
    def test_diff_users_can_create_monthly_balance_cat_with_the_same_name(
        self,
    ):
        # Frank can create a category to log his expenses
        cat_name = Helpers.generate_string()

        Helpers.create_user(self)
        Helpers.create_a_category(
            self, category_name=cat_name, is_balance=True
        )
        Helpers.logout_user(self)

        # Guido can create a category with the same name
        Helpers.create_user(self)
        Helpers.create_a_category(
            self,
            category_name=cat_name,
            midway_check=True,
            create_check=False,
            lack_of_error=True,
            is_balance=True,
        )
        Helpers.logout_user(self)

    # @skip  # Rewrite me for the the react frontend
    # def test_users_cant_see_other_users_balance_categories(self):
    #     # Frank can create a category to log his balance
    #     cat_name = Helpers.generate_string()

    #     Helpers.create_user(self)
    #     Helpers.create_a_category(
    #         self, category_name=cat_name, is_balance=True
    #     )
    #     _ = Helpers.check_category_existence(
    #         self,
    #         text=cat_name,
    #         url="balance-categories",
    #         expected_to_fail=False,
    #     )

    #     Helpers.logout_user(self)
    #     # Guido can not see Frank's balance income category
    #     Helpers.create_user(self)

    #     _ = Helpers.check_category_existence(
    #         self,
    #         text=cat_name,
    #         url="balance-categories",
    #         expected_to_fail=True,
    #     )

    #     Helpers.logout_user(self)

    # @skip  # Write me for the the react frontend
    # def test_users_cant_edit_other_users_balance_categories(self):
    #     pass

    # @skip  # Write me for the the react frontend
    # def test_users_cant_delete_other_users_balance_categories(self):
    #     pass

    # @skip  # Write me for the the react frontend
    # def test_regression_can_resubmit_a_form_after_a_failed_submission(self):
    #     pass
