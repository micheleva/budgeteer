#!/bin/sh
python manage.py wait-for-db
python manage.py migrate
python manage.py collectstatic --no-input
exec "$@"
