###########
# BUILDER #
###########

# NOTE: take advantage of the local cache to avoid to fetch pip and apt data on every single build
# NOTE: you need env DOCKER_BUILDKIT and BUILDKIT_INLINE_CACHE to be set to 1 in order for this Dockerfile to be built correctly

# i.e. FROM 3.12.5-slim-bullseye
# FROM 3.12.5-slim-bullseye as builder
# Latest as per 2024-09-29
# https://hub.docker.com/layers/library/python/3.12.5-slim-bullseye/images/sha256-48018eda0e7feaa52817f43d2b409003b001a8fb331b974fcbcedf722fec2130?context=explore
FROM python@sha256:cf26ec2ab19cd38e57658f28f616050f854bf9d19d7530574aed86918e4d2ccd as builder

WORKDIR /usr/src/app

ENV PYTHONDONTWRITEBYTECODE 1
ENV PYTHONUNBUFFERED 1

COPY ./requirements.txt .

# install system dependencies
RUN --mount=type=cache,target=/root/.cache/pip,sharing=locked \
    --mount=type=cache,target=/var/cache/apt,sharing=locked \
    --mount=type=cache,target=/var/lib/apt,sharing=locked \
    apt-get update && \
    apt-get install -y --no-install-recommends gcc && \
    pip wheel --no-deps --wheel-dir /usr/src/app/wheels -r requirements.txt

#########
# FINAL #
#########
# i.e. FROM 3.12.5-slim-bullseye
# FROM 3.12.5-slim-bullseye

FROM python@sha256:cf26ec2ab19cd38e57658f28f616050f854bf9d19d7530574aed86918e4d2ccd

LABEL maintainer="Michele Valsecchi"
LABEL Vendor="budgeteer"
LABEL Version=1.5

RUN addgroup --system app && adduser --system --group app

ENV HOME=/home/app
ENV APP_HOME=/home/app/web
RUN mkdir $APP_HOME
WORKDIR $APP_HOME

# install dependencies, setup log folders and their permissions
COPY --from=builder /usr/src/app/wheels /wheels
# this line is likely not required
COPY --from=builder /usr/src/app/requirements.txt .

RUN --mount=type=cache,target=/root/.cache/pip,sharing=locked \
    --mount=type=cache,target=/var/cache/apt,sharing=locked \
    --mount=type=cache,target=/var/lib/apt,sharing=locked \
     pip install --upgrade pip && \
     pip install /wheels/* && \
     apt-get update && \
     apt-get install -y --no-install-recommends gcc gnupg2 curl wget ca-certificates

COPY . $APP_HOME

# FIXME: We are INTENTIONALLY runinng multiple RUN commands as it seems that as Dockerfile has no tty, and chanining `apt-key add -` with `&& \` will fail.
RUN wget --quiet -O - https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add -
RUN echo "deb http://apt.postgresql.org/pub/repos/apt/ bullseye-pgdg main 14" | tee /etc/apt/sources.list.d/pgdg.list

# Avoid permission issues on /static from nginx container by doing `chmod -R 755 $APP_HOME/static`
# FIXME: We should have a shard uid:guid between containers instead of using 755
# example: https://github.com/rowdybeaver/sample-django-docker/blob/master/Dockerfile
RUN --mount=type=cache,target=/root/.cache/pip,sharing=locked \
    --mount=type=cache,target=/var/cache/apt,sharing=locked \
    --mount=type=cache,target=/var/lib/apt,sharing=locked \
    apt-get update && \
    apt-get install -y --no-install-recommends postgresql-client-14 && \
    chmod +x $APP_HOME/backup.sh && \
    chown -R app:app $APP_HOME && \
    chmod -R 755 $APP_HOME/static && \ 
    mkdir -p /var/log/gunicorn && \
    chmod o+w /var/log/gunicorn && \
    chown -R app:app $APP_HOME 

USER app

ENTRYPOINT ["/home/app/web/entrypoint.sh"]
