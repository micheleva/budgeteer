# Copyright: (c) 2019, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from django.urls import path

from api.v1.views import views
from api.v1.views import account_views
from api.v1.views import asset_views
from api.v1.views import budget_views
from api.v1.views import category_views
from api.v1.views import dividend_views
from api.v1.views import expense_views
from api.v1.views import income_views
from api.v1.views import income_category_view
from api.v1.views import monthly_balance_category_views
from api.v1.views import monthly_balance_views
from api.v1.views import goal_views
from api.v1.views import obligation_views
from api.v1.views import saving_category_views
from api.v1.views import spending_limit_views

app_name = "v1"  # pylint: disable=C0103; # noqa
urlpatterns = [
    # Function views
    path(
        "expenses/made_often",
        expense_views.get_expenses_made_often,
        name="expenses_made_often",
    ),
    path(
        "monthly_balances_and_assets",
        views.monthly_balances_and_assets,
        name="monthly_balances_and_assets",
    ),
    path(
        "accounts/create_list",
        account_views.AccountList.as_view(),
        name="accounts",
    ),
    path(
        "accounts/update/<int:pk>/",
        account_views.AccountDetail.as_view(),
        name="account",
    ),
    path("assets", asset_views.AssetsList.as_view(), name="assets"),
    path(
        "assets/create",
        asset_views.AssetsCreate.as_view(),
        name="asset_create",
    ),
    path(
        "assets/list_dates", asset_views.get_assets_dates, name="assets_dates"
    ),
    path(
        "assets/delete",
        asset_views.BulkDeleteAssets.as_view(),
        name="bulk-delete-assets",
    ),
    path("dividends", dividend_views.DividendList.as_view(), name="dividends"),
    path(
        "dividends/aggregate",
        dividend_views.DividendsAggregateList.as_view(),
        name="dividends_aggregate",
    ),
    path(
        "dividends/aggregate-by-symbol",
        dividend_views.DividendsAggregateListBySymbol.as_view(),
        name="dividends_aggregate_by_symbol",
    ),
    path(
        "dividends/create",
        dividend_views.DividendList.as_view(),
        name="dividends_create",
    ),
    path(
        "dividends/delete",
        dividend_views.DividendsDeleteAll.as_view(),
        name="dividends_delete",
    ),
    path(
        "spending-limits",
        spending_limit_views.SpendingLimitList.as_view(),
        name="spending_limits",
    ),
    path(
        "spending-limits/<int:pk>",
        spending_limit_views.SpendingLimitDetail.as_view(),
        name="spending_limit",
    ),
    path("goals/special", goal_views.special_goal_list, name="special_goals"),
    # Class views
    path(
        "categories", category_views.CategoryList.as_view(), name="categories"
    ),
    # FIXME: this should end with a slash, and change react api call to patch
    # to include a trailing slash
    path(
        "category/<int:pk>",
        category_views.CategoryDetail.as_view(),
        name="category_detail",
    ),
    path("goals", goal_views.GoalList.as_view(), name="goals"),
    path(
        "goals/<int:pk>/", goal_views.GoalDetail.as_view(), name="goal_detail"
    ),
    path(
        "obligations/",
        obligation_views.ObligationViewSet.as_view(
            {"get": "list", "post": "create"}
        ),
        name="obligations",
    ),
    path(
        "obligations/<int:pk>/",
        obligation_views.ObligationViewSet.as_view(
            {"get": "retrieve", "patch": "update", "delete": "destroy"}
        ),
        name="obligation_detail",
    ),
    path(
        "obligations-changes/",
        obligation_views.ObligationPaymentChangeViewSet.as_view(
            {"get": "list", "post": "create"}
        ),
        name="obligationpaymentchanges",
    ),
    path(
        "obligations-changes/<int:pk>/",
        obligation_views.ObligationPaymentChangeViewSet.as_view(
            {"get": "retrieve", "put": "update", "delete": "destroy"}
        ),
        name="obligationpaymentchange_detail",
    ),
    path(
        "monthly_balance_categories",
        monthly_balance_category_views.MonthlyBalanceCategoryList.as_view(),
        name="monthly_balance_categories",
    ),
    path(
        "monthly_balance_category/<int:pk>/",
        monthly_balance_category_views.MonthlyBalanceCategoryDetail.as_view(),
        name="monthly_balance_category",
    ),
    path(
        "monthly_balances",
        monthly_balance_views.MonthlyBalanceList.as_view(),
        name="monthly_balances",
    ),
    path(
        "monthly_balances/<int:pk>/",
        monthly_balance_views.MonthlyBalanceDetail.as_view(),
        name="monthly_balances",
    ),
    path(
        "monthly_balances/create-update",
        monthly_balance_views.MonthlyBalanceCreateOrUpdate.as_view(),
        name="monthly_balance_category_create_or_update",
    ),
    path(
        "expenses/create-list",
        expense_views.ExpenseList.as_view(),
        name="expenses",
    ),
    path(
        "expenses/<int:pk>/",
        expense_views.ExpenseDetail.as_view(),
        name="expense",
    ),
    path(
        "expenses/aggregate/by_month",
        expense_views.ExpenseListAggregate.as_view(),
        name="expenses_aggregate_by_month",
    ),
    path(
        "expenses/aggregate/by_category",
        expense_views.ExpenseListAggregateByCategory.as_view(),
        name="expenses_aggregate_by_cat",
    ),
    path(
        "income-categories",
        income_category_view.IncomeCategoryList.as_view(),
        name="income_categories",
    ),
    path(
        "income-categories/<int:pk>/",
        income_category_view.IncomeCategoryDetail.as_view(),
        name="income_category",
    ),
    path("incomes", income_views.IncomeList.as_view(), name="incomes"),
    path(
        "incomes/aggregate",
        income_views.IncomeListAggregate.as_view(),
        name="incomes_aggregate",
    ),
    path(
        "incomes/<int:pk>/", income_views.IncomeDetail.as_view(), name="income"
    ),
    path(
        "savingcategories",
        saving_category_views.SavingCategoryList.as_view(),
        name="savingcategories",
    ),
    path(
        "savingcategory/<int:pk>/",
        saving_category_views.SavingCategoryDetails.as_view(),
        name="savingcategory",
    ),
    path("budgets", budget_views.BudgetList.as_view(), name="budgets"),
    path(
        "budgets/<int:pk>/", budget_views.BudgetDetail.as_view(), name="budget"
    ),
    path(
        "budgets/yearly-aggregate/<int:year>/",
        budget_views.MonthlyBudgetSumAPI.as_view(),
        name="budget-yearly-aggregate",
    ),
]
