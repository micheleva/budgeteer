# Copyright: (c) 2025, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
# pylint: disable=E1101,W0611,R0901

import datetime
import logging

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Case
from django.db.models.functions import ExtractYear
from django.db.models import ExpressionWrapper
from django.db.models import F
from django.db.models import FloatField
from django.db.models import Min
from django.db.models import Sum
from django.db.models import Value
from django.db.models import When
from django.db.utils import IntegrityError

from rest_framework import generics
from rest_framework import serializers
from rest_framework import status
from rest_framework.permissions import SAFE_METHODS
from rest_framework.response import Response

from api.v1.views.helpers import calculate_daily_stats

from budgets.models import Dividend, Account
from budgets.serializers import AggregateDividendSerializer
from budgets.serializers import AggregateDividendSerializerBySymbol
from budgets.serializers import DividendSerializer
from budgets.serializers import DividendSerializerWithAccountText


logger = logging.getLogger(__name__)


class DividendsAggregateList(LoginRequiredMixin, generics.ListAPIView):
    """
    View returning the aggregate daily stats for all earned dividends per year
    i.e. $3.2/day in 2020, $1.6/day in 2021, etc.
    """

    serializer_class = AggregateDividendSerializer

    def get_queryset(self):
        user = self.request.user
        current_year = datetime.date.today().year

        queryset = (
            Dividend.objects.filter(created_by=user)
            .annotate(year=ExtractYear("record_date"))
            .values("year")
            .annotate(
                local_total=ExpressionWrapper(
                    Sum(
                        Case(
                            When(
                                local_amount__isnull=False,
                                then=F("local_amount"),
                            ),
                            default=Value(0),
                            output_field=FloatField(),
                        )
                    ),
                    output_field=FloatField(),
                )
            )
            .order_by("year")
        )

        # Get the oldest dividend date for this user
        oldest_dividend_date = Dividend.objects.filter(
            created_by=user,
        ).aggregate(min_date=Min("record_date"))["min_date"]

        # Add yearly stats
        return calculate_daily_stats(
            queryset, oldest_dividend_date, current_year, datetime.date.today()
        )


class DividendsAggregateListBySymbol(LoginRequiredMixin, generics.ListAPIView):
    """
    View returning the total amount of earnings by symbol
    e.g. XXX $213, YYY $21, etc.
    """

    serializer_class = AggregateDividendSerializerBySymbol

    def get_queryset(self):
        user = self.request.user
        queryset = (
            Dividend.objects.filter(created_by=user)
            .values("asset_text")
            .annotate(
                total_local_amount=Sum("local_amount"),
                total_foreign_amount=Sum("foreign_amount"),
            )
            .order_by(
                "-total_local_amount", "-total_foreign_amount", "asset_text"
            )
        )
        return queryset


class DividendList(LoginRequiredMixin, generics.ListCreateAPIView):
    """
    View to create and list Dividend objects
    """

    pagination_class = None

    def list(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        serializer = self.get_serializer(queryset, many=True)

        data = {
            "count": len(serializer.data),
            "next": None,
            "previous": None,
            "results": serializer.data,
        }

        return Response(data)

    def get_serializer_class(self):
        # DividendSerializerWithAccountText validates the field "account_text" as well
        # while DividendSerializer does not, as its used on create/edit
        return (
            DividendSerializerWithAccountText
            if self.request.method in SAFE_METHODS
            else DividendSerializer
        )

    def get_serializer(self, *args, **kwargs):
        serializer_class = self.get_serializer_class()
        kwargs["context"] = self.get_serializer_context()
        return serializer_class(*args, **kwargs)

    def get_queryset(self):
        filters = {"created_by": self.request.user}
        return Dividend.objects.filter(**filters).order_by("id")

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )

        account = serializer.validated_data.get("account", None)
        # Check if the account exists and belongs to the requesting user
        if not account:
            # Do NOT Mimic ListCreateAPIView error format
            error_message = "This field is required."
            return Response(
                {"account": [error_message]},
                status=status.HTTP_400_BAD_REQUEST,
            )

        a = Account.objects.filter(pk=account.pk, created_by=self.request.user)
        # Check account ownership
        if not a.exists():
            # DO NOT Mimic ListCreateAPIView error format
            error_message = "This field is required"

            raise serializers.ValidationError({"account": [error_message]})

        if serializer.is_valid(raise_exception=False):
            self.perform_create(serializer)
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def perform_create(self, serializer):
        # Inject ownership
        try:
            serializer.save(created_by=self.request.user)
        except IntegrityError as e:
            msg_to_devs = (
                "Exception catched in DividendList.perform_create()"
                f", user ID: {self.request.user.id}. Alert the devs,"
                " we have a constraint not working properly"
            )
            logger.critical(msg_to_devs)
            logger.critical(e)
            msg = [
                (
                    "Generic error thrown while creating the Dividend. "
                    "Refer the web container logs if you're the admin"
                )
            ]
            return Response(
                {"non_field_errors": msg},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )


class DividendsDeleteAll(LoginRequiredMixin, generics.DestroyAPIView):
    """
    View to destroy Dividends
    """

    serializer_class = DividendSerializer

    def get_queryset(self):
        filters = {
            "created_by": self.request.user,
        }
        return Dividend.objects.filter(**filters).order_by("id")

    def destroy(self, request, *args, **kwargs):
        queryset = self.get_queryset()
        deleted_count = queryset.count()
        self.perform_destroy(queryset)
        return Response(
            {"records_deleted": deleted_count},
            status=status.HTTP_204_NO_CONTENT,
        )

    def perform_destroy(self, instance):
        try:
            return instance.delete()
        except IntegrityError as e:
            msg_to_devs = (
                "Exception catched in DividendsDeleteAll.destroy(), user ID: "
                f"{self.request.user.id}. Alert the devs, we have a bug."
            )
            logger.critical(msg_to_devs)
            logger.critical(e)
            msg = [
                (
                    "Generic error thrown while deleting all your Dividends. "
                    "Refer the web container logs if you're the admin"
                )
            ]
            return Response(
                {"non_field_errors": msg},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )
