# Copyright: (c) 2025, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
# pylint: disable=E1101,W0611,R0901

import datetime
import logging

from django.db import connection
from django.db.utils import IntegrityError
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin

from rest_framework import generics
from rest_framework import serializers
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.decorators import renderer_classes
from rest_framework.permissions import SAFE_METHODS
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response
from rest_framework.response import Response
from rest_framework.views import APIView

from budgets.models import Account
from budgets.models import Asset
from budgets.serializers import AssetSerializer
from budgets.serializers import AssetSerializerWithAccount

from budgets.views_utils import check_for_none_values

###############################################################################
# API: Class based
###############################################################################

logger = logging.getLogger(__name__)


class AssetsList(LoginRequiredMixin, generics.ListAPIView):
    """
    View to list the most recent Assets and create new Assets
    """

    serializer_class = AssetSerializerWithAccount

    def get_queryset(self):
        filters = {
            "created_by": self.request.user,
        }

        account_id = get_account_id_from_query(self)
        if account_id:
            filters["account"] = account_id

        date = get_date_from_query(self)
        if date:
            filters["record_date"] = date

        name = get_asset_name_from_query(self)
        if name:
            filters["text"] = name

        return (
            Asset.objects.select_related("account")
            .filter(**filters)
            .order_by("id", "-record_date", "-account")
            .all()
        )


def get_account_id_from_query(self):
    account_id = self.request.GET.get("account_id")
    if account_id:
        try:
            account_id = int(account_id)
            return account_id
        except ValueError:
            raise serializers.ValidationError(
                {"account_id": ["Invalid account ID format."]}
            )
    return None


def get_date_from_query(self):
    date = self.request.GET.get("date")
    if date:
        try:
            date = datetime.datetime.strptime(date, "%Y-%m-%d").date()
            return date
        except ValueError:
            raise serializers.ValidationError(
                {"date": ["Invalid date format."]}
            )
    return None


def get_asset_name_from_query(self):
    asset_name = self.request.GET.get("name")
    if asset_name:
        try:
            asset_name = str(asset_name)
            return asset_name
        except ValueError:
            raise serializers.ValidationError(
                {"asset_name": ["Invalid name format."]}
            )
    return None


class AssetsCreate(LoginRequiredMixin, generics.CreateAPIView):
    """
    View to list the most recent Assets and create new Assets
    """

    def get_serializer_class(self):
        # AssetSerializer validates the field "ID" as well
        # while AssetSerializerWithAccount does not, as its used on create/edit
        return (
            AssetSerializerWithAccount
            if self.request.method in SAFE_METHODS
            else AssetSerializer
        )

    def get_serializer(self, *args, **kwargs):
        serializer_class = self.get_serializer_class()
        kwargs["context"] = self.get_serializer_context()
        return serializer_class(*args, **kwargs)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )

        account = serializer.validated_data.get("account", None)
        # Check if the account exists and belongs to the requesting user
        if not account:
            raise serializers.ValidationError(
                {"account": ["This field is required."]}
            )

        a = Account.objects.filter(pk=account.pk, created_by=self.request.user)
        if not a.exists():
            # Do NOT mimic ListCreateAPIView error format
            error_message = "This field is required."
            raise serializers.ValidationError({"account": [error_message]})

        # Throw exceptions if anything is null
        # FIXME(238): review and update Assets nullable fields
        check_for_none_values(serializer)

        if serializer.is_valid(raise_exception=False):
            self.perform_create(serializer)
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    # def create(self, request, *args, **kwargs):
    # def perform_create(self, serializer):

    def perform_create(self, serializer):
        # Inject ownership
        try:
            serializer.save(created_by=self.request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        except IntegrityError as e:
            msg_to_devs = (
                "Exception catched in AssetsList.perform_create(), user ID: "
                f"{self.request.user.id}. Alert the devs, we have a constraint "
                "not working properly"
            )
            logger.critical(msg_to_devs)
            logger.critical(e)
            msg = [
                (
                    "Generic error thrown while creating the Asset. "
                    "Refer the web container logs if you're the admin"
                )
            ]
            return Response(
                {"non_field_errors": msg},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )


class BulkDeleteAssets(LoginRequiredMixin, APIView):
    """
    View to handle bulk deletion of assets.
    """

    def post(self, request, *args, **kwargs):
        ids = request.data.get("ids", [])

        if not isinstance(ids, list) or not ids:
            return Response(
                {"error": "Invalid or empty 'ids' list."},
                status=status.HTTP_400_BAD_REQUEST,
            )

        # Ensure only assets owned by the current user are deleted
        user = request.user
        assets_to_delete = Asset.objects.filter(id__in=ids, created_by=user)

        if not assets_to_delete.exists():
            return Response(
                {"error": "No matching assets found to delete."},
                status=status.HTTP_404_NOT_FOUND,
            )

        count = assets_to_delete.count()
        assets_to_delete.delete()

        return Response({"records_deleted": count}, status=status.HTTP_200_OK)


###############################################################################
# API: Function based
###############################################################################


def get_assets_formatted_dates(user_id):
    formatted_dates = []
    with connection.cursor() as cursor:
        query = """
          SELECT MAX(tt.record_date) AS "maxtimestamp"
          FROM public.budgets_asset tt
          WHERE tt.created_by_id = %s
          GROUP BY EXTRACT(YEAR FROM tt.record_date), EXTRACT(MONTH FROM tt.record_date)
          ORDER BY "maxtimestamp" desc;
        """
        cursor.execute(query, [user_id])
        dates = cursor.fetchall()

        if len(dates) == 0:
            return formatted_dates
        formatted_dates = [x[0].strftime("%Y-%m-%d") for x in dates]
    return formatted_dates


@login_required
@api_view(["GET"])
@renderer_classes([JSONRenderer])
def get_assets_dates(request):
    """
    FIXME: confirm the spec of this function: I believe this functions returns
    a date per month where each date containts the latest entry for that month
    for assets, per user
    i.e. ["2022-01-17","2022-02-09"] if the user has assets marked for
         2022-01-01, 2022-01-17, 2022-02-05,2022-02-09
    """
    return Response(get_assets_formatted_dates(request.user.id))
