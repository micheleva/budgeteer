# Copyright: (c) 2025, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
# pylint: disable=E1101,W0611,R0901

import logging

from django.db import IntegrityError
from django.contrib.auth.mixins import LoginRequiredMixin

from rest_framework import generics, status
from rest_framework.response import Response

from budgets.serializers import CategorySerializer
from budgets.models import Category

logger = logging.getLogger(__name__)


class CategoryList(LoginRequiredMixin, generics.ListCreateAPIView):
    """
    View to list and create expense categories
    """

    serializer_class = CategorySerializer

    def get_queryset(self):
        filters = {"created_by": self.request.user}
        if self.request.GET.get("name"):
            filters["text__icontains"] = self.request.GET["name"]
        return Category.objects.filter(**filters).order_by("id")

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )
        self.perform_create(serializer)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def perform_create(self, serializer):
        try:
            return serializer.save(created_by=self.request.user)
        except IntegrityError as e:
            msg_to_devs = (
                "Exception catched in CategoryList.perform_create(), user ID: "
                f"{self.request.user.id}. Alert the devs, we have a constraint"
                "not working properly"
            )
            logger.critical(msg_to_devs)
            logger.critical(e)
            msg = [
                (
                    "Generic error thrown while creating the Category. "
                    "Refer the web container logs if you're the admin"
                )
            ]
            return Response(
                {"non_field_errors": msg},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )


class CategoryDetail(LoginRequiredMixin, generics.RetrieveUpdateAPIView):
    """
    View to list, update, delete a single expense categories
    """

    serializer_class = CategorySerializer

    def get_queryset(self):
        filters = {"created_by": self.request.user}
        return Category.objects.filter(**filters).order_by("id")
