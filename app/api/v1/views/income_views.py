# Copyright: (c) 2025, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
# pylint: disable=E1101,W0611,R0901

import datetime
import logging

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models.functions import ExtractMonth
from django.db.models.functions import ExtractYear
from django.db.models import Sum
from django.db.utils import IntegrityError

from rest_framework import generics
from rest_framework import serializers
from rest_framework import status
from rest_framework.permissions import SAFE_METHODS
from rest_framework.response import Response

from api.v1.views.helpers import CustomPagination

from budgets.models import Income, IncomeCategory
from budgets.serializers import AggregateIncomeSerializer
from budgets.serializers import BasicIncomeSerializer
from budgets.serializers import IncomeSerializer


logger = logging.getLogger(__name__)


class IncomeList(LoginRequiredMixin, generics.ListCreateAPIView):
    """
    View to list or create Income Categories
    """

    def get_serializer_class(self):
        # IncomeSerializer validates the field "ID" as well
        # while BasicIncomeSerializer does not, as its used on create/edit
        return (
            IncomeSerializer
            if self.request.method in SAFE_METHODS
            else BasicIncomeSerializer
        )

    pagination_class = CustomPagination

    def get_queryset(self):
        filters = {"created_by": self.request.user}

        if self.request.GET.get("note"):
            filters["note__icontains"] = self.request.GET["note"]

        # Filter by single category
        if self.request.GET.get("category_id"):
            filters["category__id"] = self.request.GET["category_id"]
        elif self.request.GET.get("category_name"):
            filters["category__text__iexact"] = self.request.GET[
                "category_name"
            ]

        # NOTE: filter by date: both extremes are included
        if self.request.GET.get("start", None):
            filters["date__gte"] = self.request.GET.get("start", None)
            try:
                datetime.datetime.strptime(filters["date__gte"], "%Y-%m-%d")
            except ValueError:
                raise serializers.ValidationError(
                    {"start": ["Invalid start date format. Use 'YYYY-MM-DD'."]}
                )

        if self.request.GET.get("end", None):
            filters["date__lte"] = self.request.GET.get("end", None)

            try:
                datetime.datetime.strptime(filters["date__lte"], "%Y-%m-%d")
            except ValueError:
                raise serializers.ValidationError(
                    {"end": ["Invalid date end format. Use 'YYYY-MM-DD'."]}
                )

        if ("date__gte" in filters and "date__lte") in filters and (
            filters["date__gte"] > filters["date__lte"]
        ):
            raise serializers.ValidationError(
                {"non_field_errors": ["Start date must be before end date"]}
            )

        # Case insensitive: "where note ILIKE '%xxx%'"
        if self.request.GET.get("note"):
            filters["note__icontains"] = self.request.GET["note"]

        return (
            Income.objects.select_related("category")
            .filter(**filters)
            .order_by("id")
        )

    # BUGFIX-257 Shouldn't we also override create() as well?
    def perform_create(self, serializer):
        cat = serializer.validated_data.get("category", None)
        # Check if the category exists and belongs to the requesting user
        if not cat:
            raise serializers.ValidationError(
                {"category": ["This field is required."]}
            )

        cx = IncomeCategory.objects.filter(
            pk=cat.pk, created_by=self.request.user
        )
        if not cx.exists():
            # Mimic ListCreateAPIView error format
            error_message = f'Invalid pk "{cat.pk}" - object does not exist.'
            raise serializers.ValidationError({"category": [error_message]})

        try:
            serializer.save(created_by=self.request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        except IntegrityError as e:
            logger.critical(e)
            msg_to_devs = (
                "Exception catched in IncomeList.perform_create(), "
                "user ID: "
                f"{self.request.user.id}. Alert the devs, we have a constraint"
                " not working properly"
            )
            logger.critical(msg_to_devs)
            msg = [
                (
                    "Generic error thrown while creating the Income. "
                    "Refer the web container logs if you're the admin"
                )
            ]
            return Response(
                {"non_field_errors": msg},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )


class IncomeListAggregate(LoginRequiredMixin, generics.ListAPIView):
    """
    View to list aggregate by year,month of all the incomes for a given user
    """

    serializer_class = AggregateIncomeSerializer
    pagination_class = None

    def get_queryset(self):
        filters = {"created_by": self.request.user}

        return (
            Income.objects.filter(**filters)
            .annotate(year=ExtractYear("date"), month=ExtractMonth("date"))
            .values("year", "month")
            .annotate(total_amount=Sum("amount"))
            .order_by("year", "month")
        )


class IncomeDetail(LoginRequiredMixin, generics.RetrieveUpdateDestroyAPIView):
    """
    View to update, delete or view details of Incomes
    """

    serializer_class = BasicIncomeSerializer

    def get_queryset(self):
        filters = {
            "created_by": self.request.user,
        }
        return (
            Income.objects.select_related("category")
            .filter(**filters)
            .order_by("id")
        )

    # BUGFIX-257 Shouldn't we also override update() as well?
    def perform_update(self, serializer):
        # <---- we are allowing the update without categories!
        if not serializer.is_valid():
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )

        # FIXME: should we make BasicIncomeSerializer to have category field
        # be read only? <===
        instance = serializer.instance
        cat = serializer.validated_data.get("category", instance.category)
        if not cat:
            raise serializers.ValidationError(
                {"category": ["This field is required."]}
            )

        c = IncomeCategory.objects.filter(
            pk=cat.pk, created_by=self.request.user
        )
        if not c.exists():
            error_message = f'Invalid pk "{cat.pk}" - object does not exist.'
            raise serializers.ValidationError({"category": [error_message]})

        try:
            serializer.save(created_by=self.request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        except IntegrityError as e:
            msg_to_devs = (
                "Exception catched in IncomeDetail.perform_update()"
                f", user ID: {self.request.user.id}. Alert the "
                "devs, we have a constraint not working properly"
            )
            logger.critical(msg_to_devs)
            logger.critical(e)
            msg = [
                (
                    "Generic error thrown while updating the Income. "
                    "Refer the web container logs if you're the admin"
                )
            ]
            return Response(
                {"non_field_errors": msg},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )
