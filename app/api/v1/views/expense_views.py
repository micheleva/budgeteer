# Copyright: (c) 2025, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
# pylint: disable=E1101,W0611,R0901

import datetime
from dateutil.relativedelta import relativedelta
import logging

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db import connection
from django.db.models.functions import ExtractMonth
from django.db.models.functions import ExtractYear
from django.db.models import F
from django.db.models import Sum
from django.db.models import DecimalField
from django.db.models.functions import Cast
from django.db.utils import IntegrityError

from rest_framework import generics
from rest_framework import serializers
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.decorators import renderer_classes
from rest_framework.permissions import SAFE_METHODS
from rest_framework.renderers import JSONRenderer
from rest_framework.response import Response

from api.v1.views.helpers import CustomPagination

from budgets.models import Category
from budgets.models import Expense
from budgets.serializers import AggregateExpensesByCategorySerializer
from budgets.serializers import AggregateExpensesByMonthSerializer
from budgets.serializers import BasicExpenseSerializer
from budgets.serializers import ExpenseSerializer

logger = logging.getLogger(__name__)

###############################################################################
# API: Class based
###############################################################################


class ExpenseList(LoginRequiredMixin, generics.ListCreateAPIView):
    """
    View to create and list expenses
    """

    def get_serializer_class(self):
        # ExpenseSerializer validates the field "ID" as well
        # while BasicExpenseSerializer does not, as its used on create/edit
        return (
            ExpenseSerializer
            if self.request.method in SAFE_METHODS
            else BasicExpenseSerializer
        )

    pagination_class = CustomPagination

    def get_queryset(self):
        filters = {"created_by": self.request.user}

        if self.request.GET.get("name"):
            filters["note__icontains"] = self.request.GET["name"]

        # Filter by single category
        if self.request.GET.get("category_id"):
            filters["category__id"] = self.request.GET["category_id"]
        elif self.request.GET.get("category_name"):
            filters["category__text__iexact"] = self.request.GET[
                "category_name"
            ]

        # FIXME! if undefined is passed in category_id this route returns 500,
        # it should return 200, and 0 results (and maybe an error?)

        # NOTE: filter by date: both extremes are included
        if self.request.GET.get("start", None):
            filters["date__gte"] = self.request.GET.get("start", None)
            try:
                datetime.datetime.strptime(filters["date__gte"], "%Y-%m-%d")
            except ValueError:
                raise serializers.ValidationError(
                    {"start": ["Invalid start date format. Use 'YYYY-MM-DD'."]}
                )

        if self.request.GET.get("end", None):
            filters["date__lte"] = self.request.GET.get("end", None)

            try:
                datetime.datetime.strptime(filters["date__lte"], "%Y-%m-%d")
            except ValueError:
                raise serializers.ValidationError(
                    {"end": ["Invalid date end format. Use 'YYYY-MM-DD'."]}
                )

        if ("date__gte" in filters and "date__lte" in filters) and (
            filters["date__gte"] > filters["date__lte"]
        ):
            raise serializers.ValidationError(
                {"non_field_errors": ["Start date must be before end date"]}
            )

        # Case insensitive: "where note ILIKE '%xxx%'"
        if self.request.GET.get("note"):
            filters["note__icontains"] = self.request.GET["note"]

        return (
            Expense.objects.select_related("category")
            .filter(**filters)
            .order_by("id")
        )

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )

        category = serializer.validated_data.get("category", None)
        if not category:
            raise serializers.ValidationError(
                {"category": ["This field is required."]}
            )

        c = Category.objects.filter(
            pk=category.pk, created_by=self.request.user
        )
        if not c.exists():
            error_message = (
                f'Invalid pk "{category.pk}" - object does not exist.'
            )
            raise serializers.ValidationError({"category": [error_message]})

        self.perform_create(serializer)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def perform_create(self, serializer):
        try:
            return serializer.save(created_by=self.request.user)
        except IntegrityError as e:
            msg_to_devs = (
                "Exception catched in ExpenseList.perform_create(), user ID: "
                f"{self.request.user.id}. Alert the devs, we have a constraint"
                " not working properly"
            )
            logger.critical(msg_to_devs)
            logger.critical(e)
            msg = [
                (
                    "Generic error thrown while creating the Expense. "
                    "Refer the web container logs if you're the admin"
                )
            ]
            return Response(
                {"non_field_errors": msg},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )


class ExpenseDetail(LoginRequiredMixin, generics.RetrieveUpdateDestroyAPIView):
    """
    View to list, update, delete expenses
    """

    serializer_class = BasicExpenseSerializer

    def get_queryset(self):
        filters = {
            "created_by": self.request.user,
        }
        return (
            Expense.objects.select_related("category")
            .filter(**filters)
            .order_by("id")
        )

    # On PUT and on PATCH, when updating the category, we should confirm that
    # the category exists, and it belongs to this user!
    # Implementation https://github.com/encode/django-rest-framework/blob/71e6c30034a1dd35a39ca74f86c371713e762c79/rest_framework/mixins.py#L59-L82  # noqa: E501
    # BUGFIX-257 Shouldn't we have this logic inside update() and use
    # perform_update() to  simply inject the ownership instead?
    # See https://stackoverflow.com/a/69395096 on create vs perform_create
    # Override perform_create() when you want to change the "behind-the-
    # scenes" behavior of how your object is created. For example, performing
    # some extra actions before or after the object is created.
    #  Override create() when you want to modify the response.
    # For example, if you want to re-structure the response, add extra data,
    # extra headers, etc.
    def perform_update(self, serializer):
        # Confirm category ownership onfy if PUT, or
        # (if PATCH and we have a category parameter)
        if self.request.method == "PUT" or (
            self.request.method == "PATCH"
            and "category" in serializer.validated_data
        ):
            if not "category" in serializer.validated_data:
                raise serializers.ValidationError(
                    {"category": ["This field is required."]}
                )

            filters = {
                "created_by": self.request.user,
                "id": serializer.validated_data["category"].id,
            }
            category = (
                Category.objects.filter(**filters).order_by("id").first()
            )
            if not category or self.request.user.id != category.created_by.id:
                raise serializers.ValidationError(
                    {"category": ["This field is required."]}
                )

        try:
            if not serializer.is_valid():
                return Response(
                    serializer.errors, status=status.HTTP_400_BAD_REQUEST
                )

            return serializer.save()
        except IntegrityError as e:
            msg_to_devs = (
                "Exception catched in ExpenseDetail.perform_update(), user ID:"
                f" {self.request.user.id}. Alert the devs, we have a constraint"
                " not working properly"
            )
            logger.critical(msg_to_devs)
            logger.critical(e)
            msg = [
                (
                    "Generic error thrown while updating the Expense. "
                    "Refer the web container logs if you're the admin"
                )
            ]
            return Response(
                {"non_field_errors": msg},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )


class ExpenseListAggregate(LoginRequiredMixin, generics.ListAPIView):
    """
    View to list aggregate by (year, month) all the expenses of a given user
    """

    serializer_class = AggregateExpensesByMonthSerializer
    pagination_class = None

    def get_queryset(self):
        filters = {"created_by": self.request.user}

        return (
            # We do not need a select_related() here, as we're not using the
            # category in the serializer
            Expense.objects.filter(**filters)
            .annotate(year=ExtractYear("date"), month=ExtractMonth("date"))
            .values("year", "month")
            .annotate(total_amount=Sum("amount"))
            .order_by("year", "month")
        )


class ExpenseListAggregateByCategory(LoginRequiredMixin, generics.ListAPIView):
    """
    View to list aggregate by category for a given user passing YYYY-MM-DD dates
    i.e. the client has to calculate the last day of a given month if it wants to
    gather all the expenses of a given month
    """

    serializer_class = AggregateExpensesByCategorySerializer
    pagination_class = None

    def get_queryset(self):
        filters = {"created_by": self.request.user}

        # NOTE: filter by date: both extremes are included
        if self.request.GET.get("start", None):
            filters["date__gte"] = self.request.GET.get("start", None)
            try:
                datetime.datetime.strptime(filters["date__gte"], "%Y-%m-%d")
            except ValueError:
                raise serializers.ValidationError(
                    {"start": ["Invalid start date format. Use 'YYYY-MM-DD'."]}
                )

        if self.request.GET.get("end", None):
            filters["date__lte"] = self.request.GET.get("end", None)

            try:
                datetime.datetime.strptime(filters["date__lte"], "%Y-%m-%d")
            except ValueError:
                raise serializers.ValidationError(
                    {"end": ["Invalid date end format. Use 'YYYY-MM-DD'."]}
                )

        if ("date__gte" in filters and "date__lte" in filters) and (
            filters["date__gte"] > filters["date__lte"]
        ):
            raise serializers.ValidationError(
                {"non_field_errors": ["Start date must be before end date"]}
            )

        return (
            Expense.objects.filter(**filters)
            .values(category_text=F("category__text"))
            .annotate(
                total_amount=Cast(
                    Sum("amount"),
                    output_field=DecimalField(max_digits=10, decimal_places=2),
                )
            )
            .order_by("-total_amount")
        )


class ExpenseListAggregateByCategoryWithoutDay(
    LoginRequiredMixin, generics.ListAPIView
):
    """
    View to list aggregate by Category for a given user, using only year and month
    i.e. YYYY-MM-DD format is not accepted here, use ExpenseListAggregateByCategory instead
    """

    serializer_class = AggregateExpensesByCategorySerializer
    pagination_class = None

    def get_queryset(self):
        filters = {"created_by": self.request.user}

        # NOTE: filter by date: both extremes are included
        # These 4 params can not be passed together with start and end
        # start_year
        # start_month
        # end_year
        # end_month

        # confirm they are all integers and that start year,monthis before end year.month
        # if they're equal select all the expenses for that month only
        # leave comments to explain we DO allow this behaviouts (ie start and end params to be the same)

        return (
            Expense.objects.select_related("category")
            .filter(**filters)
            .values(category_text=F("category__text"))
            .values(category_id=F("category__id"))
            .annotate(
                total_amount=Cast(
                    Sum("amount"),
                    output_field=DecimalField(max_digits=10, decimal_places=2),
                )
            )
            .order_by("-category_text", "-total_amount")
        )


###############################################################################
# API: Function based
###############################################################################


@login_required
@api_view(["GET"])
@renderer_classes([JSONRenderer])
def get_expenses_made_often(request):
    recurrent_expenses = []
    today = datetime.date.today()
    six_months_ago = (today + relativedelta(months=-6)).strftime("%Y-%m-%d")
    with connection.cursor() as cursor:
        # Select the top 100 expenses made by this user in the last 6 months,
        # considering only those expenses that occurred at least MORE than twice.
        query = """
          SELECT e.amount, COUNT(*) AS magnitude, e.note, e.category_id, c.text
          FROM public.budgets_expense AS e
          JOIN public.budgets_category AS c ON
          e.category_id = c.id
          WHERE e.created_by_id = %s
          AND e.date > %s
          GROUP BY e.amount, e.note, e.category_id, c.text
          HAVING COUNT(*) > 2
          ORDER BY magnitude DESC
          LIMIT 100
        """
        cursor.execute(query, [request.user.id, six_months_ago])
        recurrent_expenses = cursor.fetchall()

        if len(recurrent_expenses) == 0:
            recurrent_expenses = []
        else:
            recurrent_expenses = [
                {
                    "amount": str(x[0]),
                    "magnitude": x[1],
                    "note": x[2],
                    "categoryId": x[3],
                    "categoryName": x[4],
                }
                for x in recurrent_expenses
            ]

    # Mimic ListCreateAPIView response
    formatted_res = {
        "count": len(recurrent_expenses),
        "results": recurrent_expenses,
    }
    return Response(formatted_res)
