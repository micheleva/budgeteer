# Copyright: (c) 2025, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
# pylint: disable=E1101,W0611,R0901

import logging

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.utils import IntegrityError

from rest_framework import generics
from rest_framework import status
from rest_framework.response import Response
from rest_framework.response import Response

import budgets.models as m
from budgets.serializers import AccountSerializer

logger = logging.getLogger(__name__)


# No need to harden perform_update or perform_delete as this model is not
# depending on other models (i.e. it has no foreign keys)
class AccountList(LoginRequiredMixin, generics.ListCreateAPIView):
    """
    View to create or list Account objects
    """

    serializer_class = AccountSerializer

    def get_queryset(self):
        filters = {"created_by": self.request.user}

        return m.Account.objects.filter(**filters).order_by("id")

    # BUGFIX-257 Shouldn't we also override create() as well?
    def perform_create(self, serializer):
        if not serializer.is_valid():
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )

        try:
            return serializer.save(created_by=self.request.user)
        except IntegrityError as e:
            msg_to_devs = (
                "Exception catched in AccountList.perform_create(),"
                f" user ID: {self.request.user.id}. Alert the devs, "
                "we have a constraint not working properly"
            )
            logger.critical(msg_to_devs)
            logger.critical(e)
            msg = [
                (
                    "Generic error thrown while creating the Account. "
                    "Refer the web container logs if you're the admin"
                )
            ]
            return Response(
                {"non_field_errors": msg},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )


# No need to harden perform_update or perform_delete as this model is not
# depending on other models (i.e. it has no foreign keys)
class AccountDetail(LoginRequiredMixin, generics.RetrieveUpdateAPIView):
    """
    View to delete, update or view Account objects
    """

    serializer_class = AccountSerializer

    def get_queryset(self):
        filters = {"created_by": self.request.user}
        return m.Account.objects.filter(**filters).order_by("id")

    # FIXME BUGFIX-257: catch db integrity exceptions
