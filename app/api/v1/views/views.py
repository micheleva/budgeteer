# Copyright: (c) 2025, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
# pylint: disable=E1101,W0611,R0901

from collections import OrderedDict
import logging

from django.contrib.auth.decorators import login_required
from django.db import connection
from django.db.models import Case
from django.db.models import F
from django.db.models import FloatField
from django.db.models import Sum
from django.db.models import Value
from django.db.models import When

from rest_framework.decorators import api_view
from rest_framework.response import Response

import budgets.models as m
from budgets.serializers import MonthlyBalanceSerializer

logger = logging.getLogger(__name__)

###############################################################################
# API: Function based
###############################################################################


@login_required
@api_view(["GET"])
def monthly_balances_and_assets(request):
    """
    List all monthly balances for a given month,
    """
    filters = {"created_by": request.user}
    m_b = (
        m.MonthlyBalance.objects.select_related("category")
        .filter(**filters)
        .order_by("id")
    )
    serializer = MonthlyBalanceSerializer(m_b, many=True)

    # Store here the python Ordered dictionary objects created from Assets data
    # that will act like Monthly Budgets
    mb_like_assets = []

    # Get the initial ID either from monthly balances or start from 0
    initial_id = max([a["id"] for a in serializer.data], default=0)

    # Get assets data regardless of monthly balances existence
    assets = assets_monthly_combined(request.user)

    # Transform asset data to resemble monthly balances
    for x in assets:
        ordered_dict = OrderedDict()
        initial_id += 1
        ordered_dict["id"] = initial_id
        ordered_dict["amount"] = x["real_amount"]
        ordered_dict["category_id"] = x["account__id"]
        ordered_dict["category_text"] = x["account__text"]
        ordered_dict["is_investing"] = True
        ordered_dict["category_is_foreign_currency"] = True
        assets_month = f'{x["record_date"].year}-{x["record_date"]:%m}-01'
        ordered_dict["date"] = assets_month
        mb_like_assets.append(ordered_dict)

    # Merge assets with monthly balances (if any)
    merged_arr = mb_like_assets + serializer.data
    return Response(merged_arr)


def get_assets_formatted_dates(user_id):
    formatted_dates = []
    with connection.cursor() as cursor:
        query = """
          SELECT MAX(tt.record_date) AS "maxtimestamp"
          FROM public.budgets_asset tt
          WHERE tt.created_by_id = %s
          GROUP BY EXTRACT(YEAR FROM tt.record_date), EXTRACT(MONTH FROM tt.record_date)
          ORDER BY "maxtimestamp" desc;
        """
        cursor.execute(query, [user_id])
        dates = cursor.fetchall()

        if len(dates) == 0:
            return formatted_dates
        formatted_dates = [x[0].strftime("%Y-%m-%d") for x in dates]
    return formatted_dates


# NOTE: if this function is updated, also reflect it into
# `docs/assets-and-dividends-quirks.md`
def assets_monthly_combined(user):
    formatted_dates = get_assets_formatted_dates(user.id)

    # First calculate real_amount for each asset
    assets_with_real_amount = (
        m.Asset.objects.select_related("account")
        .annotate(
            real_amount=Case(
                When(
                    foreign_currency="USD",
                    then=Case(
                        When(current_foreign_value=0, then=Value(0)),
                        default=F("current_foreign_value") * F("rate"),
                        output_field=FloatField(),
                    ),
                ),
                When(foreign_currency="JPY", then=F("current_local_value")),
                default=F("current_local_value"),
                output_field=FloatField(),
            )
        )
        .filter(created_by=user.id, record_date__in=formatted_dates)
    )

    # Then group by account and sum the real_amount
    return (
        assets_with_real_amount.values(
            "record_date", "account__text", "account__id"
        )
        .annotate(real_amount=Sum("real_amount"))
        .order_by("record_date", "account__text", "account__id")
    )
