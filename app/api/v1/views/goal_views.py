# Copyright: (c) 2025, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
# pylint: disable=E1101,W0611,R0901
import logging

from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.utils import IntegrityError

from rest_framework import generics
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from rest_framework.response import Response

import budgets.models as m
from budgets.serializers import GoalSerializer

logger = logging.getLogger(__name__)


# No need to harden perform_update or perform_delete as this model is not depending on other models
# (i.e. it has no foreign keys)
class GoalList(LoginRequiredMixin, generics.ListCreateAPIView):
    """
    List or create Goal objects
    """

    serializer_class = GoalSerializer

    def get_queryset(self):
        filters = {"created_by": self.request.user}
        # Case insensitive: "where name ILIKE '%xxx%'"
        if self.request.GET.get("name"):
            filters["text__icontains"] = self.request.GET["name"]

        return m.Goal.objects.filter(**filters).order_by("id")

    # BUGFIX-257 Shouldn't we also override create() as well?
    def perform_create(self, serializer):
        if not serializer.is_valid():
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )

        try:
            serializer.save(created_by=self.request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        except IntegrityError as e:
            msg_to_devs = (
                "Exception catched in GoalList.perform_create(),"
                f" user ID: {self.request.user.id}. Alert the devs, "
                "we have a constraint not working properly"
            )
            logger.critical(msg_to_devs)
            logger.critical(e)
            msg = [
                (
                    "Generic error thrown while creating the Goal. "
                    "Refer the web container logs if you're the admin"
                )
            ]
            return Response(
                {"non_field_errors": msg},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )


# No need to harden perform_update or perform_delete as this model is not
# depending on other models (i.e. it has no foreign keys)
class GoalDetail(LoginRequiredMixin, generics.RetrieveUpdateDestroyAPIView):
    """
    Delete, update or view the Goal details view
    """

    serializer_class = GoalSerializer

    def get_queryset(self):
        filters = {
            "created_by": self.request.user,
        }
        return m.Goal.objects.filter(**filters).order_by("id")

    # FIXME BUGFIX-257: catch db integrity exceptions


# ################### LEGACY API ENDPOINTS: remove me on 2.x


@login_required
@api_view(["GET"])
def special_goal_list(request):
    """
    Return user unique hard coded details for user's goals
    """
    formatted_res = {"count": 0, "results": []}
    return Response(formatted_res)
