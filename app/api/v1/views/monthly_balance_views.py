# Copyright: (c) 2025, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
# pylint: disable=E1101,W0611,R0901

import datetime
import logging

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.utils import IntegrityError

from rest_framework import generics
from rest_framework import serializers
from rest_framework import status
from rest_framework.permissions import SAFE_METHODS
from rest_framework.response import Response
from rest_framework.response import Response

from budgets.models import MonthlyBalance
from budgets.models import MonthlyBalanceCategory
from budgets.serializers import MonthlyBalanceSerializerForCreateUpdate
from budgets.serializers import MonthlyBalanceSerializerForCreation
from budgets.serializers import MonthlyBalanceSerializerWithNestedCategory

logger = logging.getLogger(__name__)


class MonthlyBalanceCreateOrUpdate(LoginRequiredMixin, generics.CreateAPIView):
    """
    View to update, or create if not existing yet, a MonthlyBalance object
    without using its primary key
    """

    serializer_class = MonthlyBalanceSerializerForCreateUpdate

    def get_queryset(self):
        filters = {"created_by": self.request.user}
        return (
            MonthlyBalance.objects.select_related("category")
            .filter(**filters)
            .order_by("date", "category_id")
        )

    def get_serializer_context(self):
        return {"user_id": self.request.user.id}

    def get_serializer(self, *args, **kwargs):
        serializer_class = self.get_serializer_class()
        kwargs["context"] = self.get_serializer_context()
        return serializer_class(*args, **kwargs)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )

        category = serializer.validated_data.get("category", None)
        if not category:
            raise serializers.ValidationError(
                {"category": ["This field is required."]}
            )

        c = MonthlyBalanceCategory.objects.filter(
            pk=category.pk, created_by=self.request.user
        )
        if not c.exists():
            error_message = (
                f'Invalid pk "{category.pk}" - object does not' " exist."
            )
            raise serializers.ValidationError({"category": [error_message]})

        # FIXME: shouldn't we check the res before proceeding?
        res = self.perform_create(serializer)
        res_code = status.HTTP_200_OK
        if res.created:
            res_code = status.HTTP_201_CREATED
        del res.created
        return Response(serializer.data, status=res_code)

    def perform_create(self, serializer):
        try:
            # Inject ownership
            return serializer.save(created_by=self.request.user)
        except IntegrityError as e:
            msg_to_devs = (
                "Exception catched in MonthlyBalanceCreateOrUpdate"
                "Invalid.perform_create(), user ID: "
                f"{self.request.user.id}. Alert the devs, we have a"
                " constraint not working properly"
            )
            logger.critical(msg_to_devs)
            logger.critical(e)
            msg = [
                (
                    "Generic error thrown while creating or updating the"
                    " MonthlyBalance. Refer the web container logs if"
                    " you're the admin"
                )
            ]
            return Response(
                {"non_field_errors": msg},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )


class MonthlyBalanceList(LoginRequiredMixin, generics.ListCreateAPIView):
    """
    View to list and create Monthly Balances
    """

    def get_serializer_class(self):
        return (
            MonthlyBalanceSerializerWithNestedCategory
            if self.request.method in SAFE_METHODS
            else MonthlyBalanceSerializerForCreation
        )

    def get_queryset(self):
        filters = {"created_by": self.request.user}

        if self.request.GET.get("date", None):
            try:
                filters["date"] = datetime.date.fromisoformat(
                    self.request.GET.get("date", None)
                )
            except:
                # If the date is malformed, return nothing
                return []

        return (
            MonthlyBalance.objects.select_related("category")
            .filter(**filters)
            .order_by("date", "category_id")
        )

    def get_serializer_context(self):
        return {"user_id": self.request.user.id}

    def get_serializer(self, *args, **kwargs):
        serializer_class = self.get_serializer_class()
        kwargs["context"] = self.get_serializer_context()
        return serializer_class(*args, **kwargs)

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid(raise_exception=False):
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )

        cat = serializer.validated_data.get("category", None)
        if not cat:
            # TEST ME: add new test for this
            err = serializers.ValidationError(
                {"category": ["This field is required."]}
            )
            serializer.errors["category"].append(err)
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )

        c = MonthlyBalanceCategory.objects.filter(
            pk=cat.pk, created_by=self.request.user
        )
        if not c.exists():
            # Mimic ListCreateAPIView error format
            error_message = f'Invalid pk "{cat.pk}" - object does not exist.'
            serializer.errors["category"].append(error_message)
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )

        self.perform_create(serializer)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def perform_create(self, serializer):
        try:
            # Inject ownership
            return serializer.save(created_by=self.request.user)
        except IntegrityError as e:
            msg_to_devs = (
                "Exception catched in MonthlyBalanceList.perform_cr"
                "eate(), user ID: "
                f"{self.request.user.id}. Alert the devs, we have a"
                " constraint not working properly"
            )
            logger.critical(msg_to_devs)
            logger.critical(e)
            msg = [
                (
                    "Generic error thrown while creating the MonthlyBalance. "
                    "Refer the web container logs if you're the admin"
                )
            ]
            return Response(
                {"non_field_errors": msg},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )


class MonthlyBalanceDetail(
    LoginRequiredMixin, generics.RetrieveUpdateDestroyAPIView
):
    """
    View to list or update MonthlyBalances
    """

    serializer_class = MonthlyBalanceSerializerWithNestedCategory

    def get_queryset(self):
        filters = {
            "created_by": self.request.user,
        }
        return (
            MonthlyBalance.objects.select_related("category")
            .filter(**filters)
            .order_by("id")
        )

    def perform_update(self, serializer):
        if not serializer.is_valid(raise_exception=False):
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )

        # NOTE: no need to check category ownwership here as
        # MonthlyBalanceSerializerWithNestedCategory has category set to read only
        try:
            return serializer.save(created_by=self.request.user)
        except IntegrityError as e:
            logger.error(e)
            msg_to_devs = (
                "Exception catched in MonthlyBalanceDetail.perform_"
                "update(), user ID: "
                f"{self.request.user.id}. Alert the devs, we have a constraint "
                "not working properly"
            )
            logger.critical(msg_to_devs)
            logger.critical(e)
            msg = [
                (
                    "Generic error thrown while updating or deleting the MonthlyBalance. "
                    "Refer the web container logs if you're the admin"
                )
            ]
            return Response(
                {"non_field_errors": msg},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )
