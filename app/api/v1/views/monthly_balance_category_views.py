# Copyright: (c) 2025, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
# pylint: disable=E1101,W0611,R0901

import logging

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.utils import IntegrityError

from rest_framework import generics
from rest_framework import status
from rest_framework.response import Response

import budgets.models as m
from budgets.serializers import MonthlyBalanceCategorySerializer

logger = logging.getLogger(__name__)


class MonthlyBalanceCategoryList(
    LoginRequiredMixin, generics.ListCreateAPIView
):
    """
    View to list and create MonthlyBalance Categories
    """

    serializer_class = MonthlyBalanceCategorySerializer

    def get_queryset(self):
        filters = {"created_by": self.request.user}
        # Case insensitive: "where name ILIKE '%xxx%'"
        if self.request.GET.get("name"):
            filters["text__icontains"] = self.request.GET["name"]

        return m.MonthlyBalanceCategory.objects.filter(**filters).order_by(
            "id"
        )

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )

        self.perform_create(serializer)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def perform_create(self, serializer):
        try:
            # Inject ownership
            return serializer.save(created_by=self.request.user)
        except IntegrityError as e:
            msg_to_devs = (
                "Exception catched in MonthlyBalanceCategory."
                "perform_create(), user ID: "
                f"{self.request.user.id}. Alert the devs, we have a"
                " constraint not working properly"
            )
            logger.critical(msg_to_devs)
            logger.critical(e)
            msg = [
                (
                    "Generic error thrown while creating the Monthly Balance "
                    "Category. Refer the web container logs if you're the admin"
                )
            ]
            return Response(
                {"non_field_errors": msg},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )


# No need to harden perform_update or perform_delete as this model is not
# depending on other models (i.e. it has no foreign keys)
class MonthlyBalanceCategoryDetail(
    LoginRequiredMixin, generics.RetrieveUpdateAPIView
):
    """
    View to list, update and delete Monthly Balances
    """

    serializer_class = MonthlyBalanceCategorySerializer

    def get_queryset(self):
        filters = {
            "created_by": self.request.user,
        }
        return m.MonthlyBalanceCategory.objects.filter(**filters).order_by(
            "id"
        )

    # FIXME BUGFIX-257: catch db integrity exceptions
