# Copyright: (c) 2025, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
# pylint: disable=E1101,W0611,R0901

import contextlib
import datetime
import logging
import sys

from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.response import Response

###############################################################################
# API: Class based
###############################################################################

logger = logging.getLogger(__name__)


# Ref https://github.com/encode/django-rest-framework/blob/3.14.0/rest_framework/pagination.py#L22-L31 # pylint: disable=C0301; # noqa
def _positive_int(integer_string, strict=False, cutoff=None):
    """
    Cast a string to a strictly positive integer.
    """
    ret = int(integer_string)
    if ret < 0 or (ret == 0 and strict):
        raise ValueError()
    if cutoff:
        return min(ret, cutoff)
    return ret


# Ref https://github.com/encode/django-rest-framework/blob/3.14.0/rest_framework/pagination.py#L258-L269 pylint: disable=C0301; # noqa
# Ref https://www.django-rest-framework.org/api-guide/pagination/#pagination-in-the-generic-views  # noqa: E501
class CustomPagination(PageNumberPagination):
    """
    Custom pagination class for API views.
    Passing the query parameter "huge_page=yes" will allow the user to
    request a huge page, with a maximum of sys.maxsize items
    """

    page_size_query_param = "page"
    max_page_size = 500  # Maximum number of items per page

    offset_cutoff = 3000

    # Ref https://github.com/encode/django-rest-framework/blob/3.14.0/rest_framework/pagination.py#L258-L269  # noqa: E501
    def get_page_size(self, request):
        # NOTE: this will allow users to DDOS the app:
        # create a gazilion expenses and keep querying using ?huge_page=yes
        if (
            request.GET.get("huge_page")
            and request.GET.get("huge_page") == "yes"
        ):
            return sys.maxsize

        if self.page_size_query_param:
            with contextlib.suppress(KeyError, ValueError):
                return _positive_int(
                    request.query_params[self.page_size_query_param],
                    strict=True,
                    cutoff=self.max_page_size,
                )
        return 500

    # Ref https://github.com/encode/django-rest-framework/blob/3.14.0/rest_framework/pagination.py#L258-L269  # noqa: E501
    def get_paginated_response(self, data):
        return Response(
            {
                # 'links': {
                #     'next': self.get_next_link(),
                #     'previous': self.get_previous_link()
                # },
                "count": self.page.paginator.count,
                "results": data,
            }
        )


def calculate_daily_stats(
    queryset, oldest_dividend_date, current_year, today_date
):
    # If the user has no dividends, return early, or we'll end up returning
    if len(queryset) == 0:
        return []

    result = []
    total_local = 0
    total_days = 0
    for year_data in queryset:
        year = year_data["year"]
        local_total = year_data["local_total"]
        total_local += local_total
        days = 0
        local_per_day = 0

        # Calculate amount per day, considering current year
        if year == current_year:
            days_elapsed = (
                today_date
                - datetime.date(
                    current_year, 1, 1
                )  # Take date as a parameter, so that we can test it
            ).days
            days = days_elapsed
            total_days += days
            # Avoid dividing by zero if today is January first :D
            if days_elapsed > 0:
                local_per_day = round(local_total / days_elapsed, 2)

        # Calculate days for the first year we've received didivdends
        elif year == oldest_dividend_date.year:
            # i.e. calculate only the days between said event and Dec 31st
            first_year_days = (
                datetime.date(year, 12, 31) - oldest_dividend_date
            ).days + 1
            days = first_year_days
            total_days += days
            # Avoid dividing by zero if we're on day 0 if the firs year :D
            if first_year_days > 0:
                local_per_day = round(local_total / first_year_days, 3)
        # Any other year: consider as well whether it's a leap year or not
        else:
            days_in_year = get_days_in_year(year)
            days = days_in_year
            total_days += days
            # Avoid didiving by zero if this is the first day of any other year
            # (i.e. not the fist, not the current) :D
            if days_in_year > 0:
                local_per_day = round(local_total / days_in_year, 2)

        result.append(
            {
                "year": year,
                "local_total": local_total,
                "local_per_day": local_per_day,
                "days": days,
            }
        )

    total_local_per_day = 0
    if total_days > 0:
        total_local_per_day = round(total_local / total_days, 2)

    # Append total stats (let's avoid mixing numbers and strings
    # and let's use -1 instead of "Total" in the year field)
    # NOTE: The frontend handles -1 as "Grand total" when displaying it
    result.append(
        {
            "year": -1,
            "local_total": total_local,
            "local_per_day": total_local_per_day,
            "days": total_days,
        }
    )
    return result


def get_days_in_year(year):
    if (year % 4 == 0 and year % 100 != 0) or (year % 400 == 0):
        return 366  # Leap year
    return 365  # Regular year
