# Copyright: (c) 2025, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
# pylint: disable=E1101,W0611,R0901

import logging

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.utils import IntegrityError

from rest_framework import generics
from rest_framework import status
from rest_framework.response import Response

from budgets.models import SavingCategory
from budgets.serializers import SavingCategorySerializer


###############################################################################
# API: Class based
###############################################################################

logger = logging.getLogger(__name__)


# No need to harden perform_update or perform_delete as this model is not
# depending on other models (i.e. it has no foreign keys)
class SavingCategoryList(LoginRequiredMixin, generics.ListCreateAPIView):
    """
    List or create Goal objects
    """

    serializer_class = SavingCategorySerializer

    def get_queryset(self):
        filters = {"created_by": self.request.user}
        return SavingCategory.objects.filter(**filters).order_by("id")

    # BUGFIX-257 Shouldn't we also override create() as well?
    def perform_create(self, serializer):
        if not serializer.is_valid():
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )

        try:
            serializer.save(created_by=self.request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        except IntegrityError as e:
            msg_to_devs = (
                "Exception catched in SavingCategoryList.perform_create(),"
                f" user ID: {self.request.user.id}. Alert the devs, we have"
                " a constraint not working properly"
            )
            logger.critical(msg_to_devs)
            logger.critical(e)
            msg = [
                (
                    "Generic error thrown while creating the SavingCategory."
                    " Refer the web container logs if you're the admin"
                )
            ]
            return Response(
                {"non_field_errors": msg},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )


# No need to harden perform_update or perform_delete as this model is
# not depending on other models (i.e. it has no foreign keys)
class SavingCategoryDetails(
    LoginRequiredMixin, generics.RetrieveUpdateAPIView
):
    """
    View to list, update, delete a single expense categories
    """

    serializer_class = SavingCategorySerializer

    def get_queryset(self):
        filters = {"created_by": self.request.user}
        return SavingCategory.objects.filter(**filters).order_by("id")

    # BUGFIX-257 Shouldn't we have this logic inside update() and
    # use perform_update() to  simply inject the ownership instead?
    # See https://stackoverflow.com/a/69395096 on create vs perform_create
    # Override perform_create() when you want to change the "behind-the-scenes"
    # behavior of how your object is created. For example, performing some
    # extra actions before or after the object is created.
    # Override create() when you want to modify the response.For example, if
    # you want to re-structure the response, add extra data, extra headers,etc.
    def perform_update(self, serializer):
        if not serializer.is_valid(raise_exception=False):
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )
        # NOTE: no need to check category ownwership here as get_query
        # already checks the user issuing the request if the owner of the
        # object
        try:
            # IMPORTANT: do not inject the ownership on update.
            # It introduces un-necessary risks
            return serializer.save()
        except IntegrityError as e:
            logger.error(e)
            msg_to_devs = (
                "Exception catched in SavingCategoryDetails.perform_update(),"
                f" user ID: {self.request.user.id}. Alert the devs,"
                " we have a constraint not working properly"
            )
            logger.critical(msg_to_devs)
            logger.critical(e)
            msg = [
                (
                    "Generic error thrown while updating the SavingCategory."
                    " Refer the web container logs if you're the admin"
                )
            ]
            return Response(
                {"non_field_errors": msg},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )
