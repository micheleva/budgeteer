# Copyright: (c) 2025, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
# pylint: disable=E1101,W0611,R0901

import logging

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.models import Sum
from django.db.utils import IntegrityError

from rest_framework import generics
from rest_framework import status
from rest_framework.permissions import SAFE_METHODS
from rest_framework.response import Response

from budgets.models import Budget
from budgets.serializers import BudgetSerializer
from budgets.serializers import BudgetSerializerAllowOnlyAmountsUpdate
from budgets.serializers import BudgetSerializerWithCategory
from budgets.serializers import YearlyAggregateBudgetSerializer


###############################################################################
# API: Class based
###############################################################################

logger = logging.getLogger(__name__)


class BudgetList(generics.ListCreateAPIView):
    """
    View to list and create Budgets
    """

    def get_serializer_class(self):
        # BudgetSerializerWithCategoryOrSedAsideCategory has the
        # Category/SavingCategory as nested object while BudgetSerializer
        # does not, as its used on create/edit
        return (
            BudgetSerializerWithCategory
            if self.request.method in SAFE_METHODS
            else BudgetSerializer
        )

    def get_queryset(self):
        filters = {"created_by": self.request.user}
        # Only filter by year and month if, at least, year is present
        # If only year is passed, automatically only return Budgets having
        # month=Null. If both year and month are passed, filter by both
        if (
            self.request.GET.get("year", None)
            and str(self.request.GET.get("year", None)).isdigit()
        ):
            filters["year"] = self.request.GET.get("year")
            if (
                self.request.GET.get("month", None)
                and str(self.request.GET.get("month", None)).isdigit()
            ):
                filters["month"] = self.request.GET.get("month")
            else:
                filters["month__isnull"] = True

        return (
            Budget.objects.select_related("category", "savingcategory")
            .filter(**filters)
            .order_by("year", "month", "category_id", "savingcategory", "id")
        )

    def perform_create(self, serializer):
        try:
            return serializer.save(created_by=self.request.user)
        except IntegrityError as e:
            msg_to_devs = (
                "Exception catched in BudgetList.perform_create(),"
                " user ID: "
                f"{self.request.user.id}. Alert the devs, we have a "
                "constraint not working properly"
            )
            logger.critical(msg_to_devs)
            logger.critical(e)
            msg = [
                "Generic error thrown while creating the Budget. "
                "Refer to the web container logs if you're the admin."
            ]
            return Response(
                {"non_field_errors": msg},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid(raise_exception=False):
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )

        self.perform_create(serializer)
        return Response(serializer.data, status=status.HTTP_201_CREATED)


# NOTE: we intentionally do not allow deletion: users should set the amount to 0 instead
class BudgetDetail(LoginRequiredMixin, generics.RetrieveUpdateAPIView):
    """
    View to list, update, delete a single expense
    """

    def get_serializer_class(self):
        # BudgetSerializerWithCategoryOrSedAsideCategory has the
        # Category/SavingCategory as nested object while
        # BudgetSerializerAllowOnlyAmountsUpdate does not, as its used on
        # create/edit
        return (
            BudgetSerializerWithCategory
            if self.request.method in SAFE_METHODS
            else BudgetSerializerAllowOnlyAmountsUpdate
        )

    def get_queryset(self):
        filters = {
            "created_by": self.request.user,
        }
        return (
            Budget.objects.select_related("category", "savingcategory")
            .filter(**filters)
            .order_by("year", "month", "id")
        )

    # BUGFIX-257 Shouldn't we have this logic inside update() and
    # use perform_update() to  simply inject the ownership instead?
    # See https://stackoverflow.com/a/69395096 on create vs perform_create
    # Override perform_create() when you want to change the "behind-the-scenes"
    # behavior of how your object is created. For example, performing some
    # extra actions before or after the object is created.
    # Override create() when you want to modify the response.For example, if
    # you want to re-structure the response, add extra data, extra headers,etc.
    def perform_update(self, serializer):
        if not serializer.is_valid(raise_exception=False):
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )
        # NOTE: no need to check category ownwership here as
        # MonthlyBalanceSerializerWithNestedCategory has
        # category/savingcategory set to read only
        try:
            # IMPORTANT: do not inject the ownership on update.
            # It introduces un-necessary risks
            return serializer.save()
        except IntegrityError as e:
            logger.error(e)
            msg_to_devs = (
                "Exception catched in BudgetDetail.perform_update(),"
                f" user ID: {self.request.user.id}. Alert the devs,"
                " we have a constraint not working properly"
            )
            logger.critical(msg_to_devs)
            logger.critical(e)
            msg = [
                (
                    "Generic error thrown while updating the Budget. "
                    "Refer the web container logs if you're the admin"
                )
            ]
            return Response(
                {"non_field_errors": msg},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )


class MonthlyBudgetSumAPI(generics.ListAPIView):
    serializer_class = YearlyAggregateBudgetSerializer

    def get_queryset(self):
        year = self.kwargs.get("year")
        user = self.request.user
        queryset = (
            Budget.objects.filter(
                created_by=user, year=year, month__isnull=False
            )
            .values("category")
            .annotate(sum_amount=Sum("amount"))
        )

        return list(queryset)

    def list(self, request, *args, **kwargs):
        res = self.get_queryset()
        response_data = {
            "results": res,
            "count": len(res),
        }
        return Response(response_data)
