# Copyright: (c) 2025, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
# pylint: disable=E1101,W0611,R0901

import datetime
import logging

from django.db.utils import IntegrityError

from rest_framework import serializers
from rest_framework import status
from rest_framework.permissions import SAFE_METHODS
from rest_framework.response import Response
from rest_framework import viewsets
from rest_framework.response import Response

import budgets.models as m
from budgets.serializers import ObligationSerializer
from budgets.serializers import ObligationPaymentChangeSerializer
from budgets.serializers import ObligationPaymentChangeSerializerForCreation
from budgets.serializers import ObligationPaymentChangeSerializerForUpdate

logger = logging.getLogger(__name__)


class ObligationViewSet(viewsets.ModelViewSet):
    serializer_class = ObligationSerializer

    def get_queryset(self):
        return m.Obligation.objects.filter(
            created_by=self.request.user
        ).order_by("id")

    def perform_create(self, serializer):
        serializer.save(created_by=self.request.user)


class ObligationPaymentChangeViewSet(viewsets.ModelViewSet):
    # serializer_class = ObligationPaymentChangeSerializer

    def get_serializer_class(self):
        if self.request.method in SAFE_METHODS:
            return ObligationPaymentChangeSerializer
        if self.request.method in ["POST"]:
            return ObligationPaymentChangeSerializerForCreation
        return ObligationPaymentChangeSerializerForUpdate
        # FIXME: delete is not being handled separately here!

    def perform_create(self, serializer):
        # Inject ownership
        try:
            obligation = serializer.validated_data["obligation"]
            serializer.save(obligation=obligation)
            serializer.save(created_by=self.request.user)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        except IntegrityError as e:  # <============= test from here
            msg_to_devs = (
                "Exception catched in ObligationPaymentChangeViewSet.perform"
                f"_create(), user ID: {self.request.user.id}. Alert the devs"
                ", we have a constraint not working properly"
            )
            logger.critical(msg_to_devs)
            logger.critical(e)
            msg = [
                (
                    "Generic error thrown while creating the Asset. "
                    "Refer the web container logs if you're the admin"
                )
            ]
            return Response(
                {"non_field_errors": msg},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )  # <============= test to here

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )
        obligation_id = self.request.data.get("obligation", None)
        if not obligation_id:
            return Response(
                {"error": "Obligation ID is required."},
                status=status.HTTP_400_BAD_REQUEST,
            )

        if not serializer.is_valid():
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )

        # Ensure the obligation belongs to the current user
        o = m.Obligation.objects.filter(
            id=obligation_id, created_by=self.request.user
        )
        if not o.exists():
            msg = f'Invalid pk "{obligation_id}" - object does not exist.'
            return Response(
                {"error": msg},
                status=status.HTTP_404_NOT_FOUND,
            )

        # THIS CODE SHOULD BE MOVED TO THE SERIALIZEEEEEEEEEEEEEEEEEEEEEEER!!!
        o = o.first()
        if o.obligation_type != "variable":
            return Response(
                {
                    "error": "You can only create payment changes"
                    " for obligations of type 'variable'."
                },
                status=status.HTTP_400_BAD_REQUEST,
            )

        # THIS CODE SHOULD BE MOVED TO THE SERIALIZEEEEEEEEEEEEEEEEEEEEEEER!!!
        # Check if the change date is after the end date of the obligation
        change_date_str = self.request.data.get("change_date", None)
        msg = "The change date cannot be after the end date of the obligation."
        if change_date_str:
            try:
                change_date = datetime.datetime.strptime(
                    change_date_str, "%Y-%m-%d"
                ).date()
                if change_date > o.end_date:
                    return Response(
                        {"error": msg},
                        status=status.HTTP_400_BAD_REQUEST,
                    )
            except ValueError:
                return Response(
                    {"error": "Invalid date format for change date."},
                    status=status.HTTP_400_BAD_REQUEST,
                )
        # THIS CODE SHOULD BE MOVED TO THE SERIALIZEEEEEEEEEEEEEEEEEEEEEEER!!!
        # Check if the change date is after the start date of the obligation
        msg = "The change date must be after the start date of the obligation."
        if change_date_str:
            try:
                change_date = datetime.datetime.strptime(
                    change_date_str, "%Y-%m-%d"
                ).date()
                if change_date <= o.start_date:
                    return Response(
                        {"error": msg},
                        status=status.HTTP_400_BAD_REQUEST,
                    )
            except ValueError:
                return Response(
                    {"error": "Invalid date format for change date."},
                    status=status.HTTP_400_BAD_REQUEST,
                )

        if serializer.is_valid(raise_exception=False):
            self.perform_create(serializer)
            return Response(serializer.data, status=status.HTTP_201_CREATED)

        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def perform_update(self, serializer):
        instance = self.get_object()
        # Check if the user is the owner of the payment change
        if self.request.user != instance.obligation.created_by:
            oid = instance.obligation.id
            msg = f'Invalid pk "{oid}" - object does not exist.'
            return Response({"error": msg}, status=status.HTTP_404_NOT_FOUND)

        if not serializer.is_valid():
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )
        # THIS CODE SHOULD BE MOVED TO THE SERIALIZEEEEEEEEEEEEEEEEEEEEEEER!!!
        # Check if the change date is after the end date of the obligation
        change_date_str = self.request.data.get("change_date", None)
        if change_date_str:
            try:
                change_date = datetime.datetime.strptime(
                    change_date_str, "%Y-%m-%d"
                ).date()
                if change_date > instance.obligation.end_date:
                    return Response(
                        {
                            "error": "The change date cannot be after the end"
                            " date of the obligation."
                        },
                        status=status.HTTP_400_BAD_REQUEST,
                    )
            except ValueError:
                return Response(
                    {"error": "Invalid date format for change date."},
                    status=status.HTTP_400_BAD_REQUEST,
                )
        # THIS CODE SHOULD BE MOVED TO THE SERIALIZEEEEEEEEEEEEEEEEEEEEEEER!!!
        # Check if the change date is after the start date of the obligation
        msg = "The change date must be after the start date of the obligation."
        if change_date_str:
            try:
                change_date = datetime.datetime.strptime(
                    change_date_str, "%Y-%m-%d"
                ).date()
                if change_date <= instance.obligation.start_date:
                    return Response(
                        {"error": msg},
                        status=status.HTTP_400_BAD_REQUEST,
                    )
            except ValueError:
                return Response(
                    {"error": "Invalid date format for change date."},
                    status=status.HTTP_400_BAD_REQUEST,
                )

        ser_res = serializer.save()
        if not ser_res:
            print(f"Error updating payment change: {serializer.errors}")
            return Response(
                {"error": serializer.errors},
                status=status.HTTP_400_BAD_REQUEST,
            )

        return Response(serializer.data)

    def perform_destroy(self, instance):
        # Check if the user is the owner of the payment change
        # TODO: check if this ownership check is really needed, as we do it
        # already in the get_queryset()
        if self.request.user != instance.obligation.created_by:
            oid = instance.obligation.id
            msg = f'Invalid pk "{oid}" - object does not exist.'
            return Response(
                {
                    "error": msg,
                },
                status=status.HTTP_404_NOT_FOUND,
            )
        instance.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

    def get_queryset(self):
        return m.ObligationPaymentChange.objects.filter(
            obligation__created_by=self.request.user
        )

    def get_object(self):
        obj = super().get_object()
        if obj.obligation.created_by != self.request.user:
            # Mimic ListCreateAPIView error format
            error_message = f'Invalid pk "{obj.pk}" - object does not exist.'
            raise serializers.ValidationError({"obligation": [error_message]})
        return obj
