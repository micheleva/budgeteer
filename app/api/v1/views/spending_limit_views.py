# Copyright: (c) 2025, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
# pylint: disable=E1101,W0611,R0901

import logging

from django.contrib.auth.mixins import LoginRequiredMixin
from django.db.utils import IntegrityError

from rest_framework import generics
from rest_framework import serializers
from rest_framework import status
from rest_framework.response import Response

from budgets.models import Category
from budgets.models import SpendingLimit
from budgets.serializers import SpendingLimitSerializer
from budgets.serializers import SpendingLimitPartialUpdateSerializer

###############################################################################
# API: Class based
###############################################################################

logger = logging.getLogger(__name__)


class SpendingLimitList(LoginRequiredMixin, generics.ListCreateAPIView):
    """
    View to create and list spending limits
    """

    serializer_class = SpendingLimitSerializer

    def get_queryset(self):
        filters = {"created_by": self.request.user}
        return (
            SpendingLimit.objects.select_related("category")
            .filter(**filters)
            .order_by("id")
        )

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        if not serializer.is_valid():
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )

        category = serializer.validated_data.get("category", None)
        if not category:
            raise serializers.ValidationError(
                {"category": ["This field is required."]}
            )

        c = Category.objects.filter(
            pk=category.pk, created_by=self.request.user
        )
        if not c.exists():
            error_message = (
                f'Invalid pk "{category.pk}" - object does not exist.'
            )
            raise serializers.ValidationError({"category": [error_message]})

        self.perform_create(serializer)
        return Response(serializer.data, status=status.HTTP_201_CREATED)

    def perform_create(self, serializer):
        try:
            return serializer.save(created_by=self.request.user)
        except IntegrityError as e:
            msg_to_devs = (
                "Exception caught in SpendingLimitList.perform_create(), user ID: "
                f"{self.request.user.id}. Alert the devs, we have a constraint"
                " not working properly."
            )
            logger.critical(msg_to_devs)
            logger.critical(e)
            msg = [
                (
                    "Generic error thrown while creating the SpendingLimit. "
                    "Refer the web container logs if you're the admin."
                )
            ]
            return Response(
                {"non_field_errors": msg},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )


class SpendingLimitDetail(
    LoginRequiredMixin, generics.RetrieveUpdateDestroyAPIView
):
    """
    View to retrieve, update, or delete a spending limit
    """

    def get_serializer_class(self):
        if self.request.method == "PATCH":
            return SpendingLimitPartialUpdateSerializer
        return SpendingLimitSerializer

    lookup_field = "pk"

    def get_queryset(self):
        # Ensure the user can only access their own objects
        filters = {"created_by": self.request.user}
        return SpendingLimit.objects.select_related("category").filter(
            **filters
        )

    def _helper_update(self, serializer):
        try:
            self.perform_update(serializer)
        except IntegrityError as e:  # <============= test from here
            msg_to_devs = f"Exception caught in SpendingLimitDetail.update(), user ID: {self.request.user.id}."
            logger.critical(msg_to_devs)
            logger.critical(e)
            msg = [
                (
                    "Generic error thrown while updating the SpendingLimit. "
                    "Refer the web container logs if you're the admin."
                )
            ]
            return Response(
                {"non_field_errors": msg},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )
        return Response(serializer.data)  # <============= test to here

    # Used for PATCH requests
    def partial_update(self, request, *args, **kwargs):
        kwargs["partial"] = True
        instance = self.get_object()
        serializer = self.get_serializer(
            instance, data=request.data, partial=True
        )

        if not serializer.is_valid():
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )

        category = serializer.validated_data.get("category", None)
        if category:
            # Validate that the category belongs to the same user
            c = Category.objects.filter(
                pk=category.pk, created_by=self.request.user
            )
            if not c.exists():
                error_message = (
                    f'Invalid pk "{category.pk}" - object does not exist.'
                )
                raise serializers.ValidationError(
                    {"category": [error_message]}
                )

        return self._helper_update(serializer)

    # Used for PUT requests
    def update(self, request, *args, **kwargs):
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data)

        if not serializer.is_valid():
            return Response(
                serializer.errors, status=status.HTTP_400_BAD_REQUEST
            )

        category = serializer.validated_data.get("category", None)
        if not category:
            raise serializers.ValidationError(
                {"category": ["This field is required."]}
            )

        # Validate that the category belongs to the same user
        c = Category.objects.filter(
            pk=category.pk, created_by=self.request.user
        )
        if not c.exists():
            error_message = (
                f'Invalid pk "{category.pk}" - object does not exist.'
            )
            raise serializers.ValidationError({"category": [error_message]})

        return self._helper_update(serializer)

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        self.perform_destroy(instance)
        return Response(status=status.HTTP_204_NO_CONTENT)

    def perform_update(self, serializer):
        try:
            return serializer.save(created_by=self.request.user)
        except IntegrityError as e:
            msg_to_devs = (
                "Exception caught in SpendingLimitDetail.perform_update(), user ID: "
                f"{self.request.user.id}. Alert the devs, we have a constraint"
                " not working properly."
            )
            logger.critical(msg_to_devs)
            logger.critical(e)
            msg = [
                (
                    "An error occurred while updating the SpendingLimit."
                    " Refer the web container logs if you're the admin."
                )
            ]
            return Response(
                {"non_field_errors": msg},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )

    # We do not check for ownership here as the get_queryset() method already
    # does that
    def perform_destroy(self, instance):  # <============= test from here
        try:
            instance.delete()
        except IntegrityError as e:
            msg_to_devs = (
                "Exception caught in SpendingLimitDetail.perform_destroy(), user ID: "
                f"{self.request.user.id}. Alert the devs, we have a constraint"
                " not working properly."
            )
            logger.critical(msg_to_devs)
            logger.critical(e)
            raise serializers.ValidationError(
                {
                    "non_field_errors": [
                        "An error occurred while deleting the SpendingLimit."
                    ]
                }
            )  # <============= test to here
