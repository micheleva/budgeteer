# Copyright: (c) 2024, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
import datetime
from datetime import date
from datetime import timedelta
import logging
import random
from decimal import Decimal

from unittest.mock import patch
from unittest import skip
from urllib.parse import urlencode

from django.contrib.auth.models import User
from django.db.models import F
from django.db.models import Sum
from django.db.models import DecimalField
from django.db.models.functions import Cast
from django.db.models.functions import ExtractMonth
from django.db.models.functions import ExtractYear
from django.db.utils import IntegrityError
from django.urls import reverse
from rest_framework import status

from api.v1.tests.test_base import BaseApiTest
from api.v1.views.expense_views import ExpenseDetail
from api.v1.views.expense_views import ExpenseList
from budgets.serializers import (
    AggregateExpensesByMonthSerializer,
    ExpenseSerializer,
)
import budgets.models as m
from budgets.tests.base import BaseTest

logger = logging.getLogger(__name__)


def _create_users():
    """
    Creates two users.

    This function generates two users with random usernames and passwords.

    Returns:
        tuple: A tuple containing the created User objects in the
               following order: (user1, user2).
    """
    user = User.objects.create_user(
        username=BaseTest.generate_string(20),
        password=BaseTest.generate_string(20),
    )
    user2 = User.objects.create_user(
        username=BaseTest.generate_string(20),
        password=BaseTest.generate_string(20),
    )
    return user, user2


class ExpenseTest(BaseApiTest):  # pylint: disable=R0904; # noqa
    """
    Test Expenses model related v1 rest api behaviour
    """

    def setUp(self):  # pylint: disable=C0103; # noqa
        # Instrument the unit tests to fail if they run for too long
        super().setUp()
        self.user, self.user2 = _create_users()

    def test_serializer(self):
        # NOTE: we use 40 chars instead of 20 to avoid conflicts with the
        # category text in other tests
        # IMPORTANT: if you change the length of the text, you will make this test flaky
        text = BaseTest.generate_string(40)
        category = m.Category.objects.create(
            text=text, is_archived=False, created_by=self.user
        )
        # Test the serializer with a category
        self.expense = m.Expense.objects.create(
            category=category,
            amount=self.generate_number(max_digits=10, decimal_places=2),
            note="Lunch",
            date=date.today(),
            created_by=self.user,
        )
        serializer = ExpenseSerializer(instance=self.expense)
        self.assertEqual(serializer.data["category_text"], text)

        # Test the serializer without a category
        self.expense.category = None
        self.expense.save()
        serializer = ExpenseSerializer(instance=self.expense)
        self.assertEqual(serializer.data["category_text"], None)

    def test_aggregate_serializer(self):
        # NOTE: we use 40 chars instead of 20 to avoid conflicts with the
        # Category text in other tests
        # IMPORTANT: if you change the length of the text, you will make this test flaky
        text = BaseTest.generate_string(40)
        category = m.Category.objects.create(text=text, created_by=self.user)
        # Loop through an array of fixed data instead of creating new objects one at the time

        hard_coded_data = [
            {
                "amount": Decimal("100.01"),
                "note": BaseTest.generate_string(20),
                "date": date(2022, 1, 1),
            },
            {
                "amount": Decimal("100.03"),
                "note": BaseTest.generate_string(20),
                "date": date(2022, 1, 2),
            },
            {
                "amount": Decimal("7.07"),
                "note": BaseTest.generate_string(20),
                "date": date(2022, 1, 2),
            },
            {
                "amount": Decimal("100.00"),
                "note": BaseTest.generate_string(20),
                "date": date(2022, 2, 1),
            },
            {
                "amount": Decimal("100.06"),
                "note": BaseTest.generate_string(20),
                "date": date(2022, 2, 2),
            },
            {
                "amount": Decimal("300.01"),
                "note": BaseTest.generate_string(20),
                "date": date(2022, 3, 1),
            },
        ]
        for data in hard_coded_data:
            m.Expense.objects.create(
                category=category,
                amount=data["amount"],
                note=data["note"],
                date=data["date"],
                created_by=self.user,
            )

        filters = {"created_by": self.user}
        data = (
            m.Expense.objects.filter(**filters)
            .annotate(year=ExtractYear("date"), month=ExtractMonth("date"))
            .values("year", "month")
            .annotate(
                total_amount=Cast(
                    Sum("amount"),
                    output_field=DecimalField(max_digits=10, decimal_places=2),
                )
            )
            .order_by("year", "month")
        )
        serializer = AggregateExpensesByMonthSerializer(data=data, many=True)
        serializer.is_valid()
        serialized_data = serializer.data

        expected_data = [
            {
                "year": 2022,
                "month": 1,
                "total_amount": Decimal("207.11"),
            },
            {
                "year": 2022,
                "month": 2,
                "total_amount": Decimal("200.06"),
            },
            {
                "year": 2022,
                "month": 3,
                "total_amount": Decimal("300.01"),
            },
        ]
        self.assertEqual(serialized_data, expected_data)
        for i in range(len(serialized_data)):
            serialized_data[i]["year"] = serializer.data[i].get("year")
            serialized_data[i]["month"] = serializer.data[i].get("month")

    def test_can_create_expense(self):
        """
        Ensure we can create a new category object
        """
        self.client.force_login(self.user)
        category_response, _ = self.create_category()
        expense_response, expense_input = self.create_expense(
            category_response.data["id"]
        )

        self.assertEqual(expense_response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            expense_response.data["category"], category_response.data["id"]
        )
        self.assertEqual(expense_response.data["note"], expense_input["note"])
        self.assertEqual(expense_response.data["date"], expense_input["date"])
        # fmt: off
        self.assertEqual(
            m.Expense.objects.first().created_by,
            self.user,  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        expenses_list_url = reverse("api:v1:expenses")
        response = self.client.get(expenses_list_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)
        self.assertEqual(
            response.data["results"][0]["category_text"],
            category_response.data["text"],
        )
        self.assertEqual(
            response.data["results"][0]["note"], expense_input["note"]
        )
        self.assertEqual(
            response.data["results"][0]["date"], expense_input["date"]
        )

        if "errors" in expense_response.data:
            self.fail(
                "Expense creation response's body included at least one error!"
            )

    def test_can_not_create_expenses_for_other_users(self):
        """
        Ensure we can NOT create a new expense object linked to another user
        by posting other users' ID in the payload
        """
        self.client.force_login(self.user)

        # Create a category for the initial user
        init_cat_res, _ = self.create_category()
        init_cat_id = init_cat_res.data["id"]

        # Another user tries to create an Expense object and have it linked
        # to the initial user's ID
        self.client.force_login(self.user2)

        create_url = reverse("api:v1:expenses")
        # Generate data for the new Expense
        amount = self.generate_number(max_digits=10, decimal_places=2)
        note = BaseTest.generate_string(20)
        record_date = date.today().isoformat()
        # Try to inject first user ownserhip using a category belonging to
        # the intial user: the expense creation should fail hard
        data = {
            "category": init_cat_id,
            "amount": amount,
            "note": note,
            "date": record_date,
            "created_by": self.user.id,
        }
        res = self.client.post(create_url, data, format="json")

        try:
            # fmt: off
            _ = m.Expense.objects.get(  # pylint: disable=E1101; # noqa
                created_by=self.user2.id
            )
            # fmt: on
            self.assertEqual(res.status_code, status.HTTP_201_CREATED)
            logger.critical(
                (
                    "ERROR! The expense ownership logic on creation has a bug!"
                    " using 'created_by' in the body request allows injections"
                    " in other accounts. FIX ME!"
                )
            )
        # fmt: off
        except m.Expense.DoesNotExist:  # pylint: disable=E1101; # noqa
            self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertEqual(len(res.data["category"]), 1)
            err_msg = f'Invalid pk "{init_cat_id}" - object does not exist.'
            self.assertEqual(res.data["category"][0], err_msg)
            self.assertEqual(res.data["category"][0].code, "invalid")
        # fmt: on
        # Try to inject first user ownserhip using a category belonging to the other user:
        # creation should suceed
        # However the expense.created_by should be other_user.id
        category_response, _ = self.create_category()
        cat_id = category_response.data["id"]

        create_url = reverse("api:v1:expenses")
        amount = self.generate_number(max_digits=10, decimal_places=2)
        note = BaseTest.generate_string(20)
        record_date = date.today().isoformat()

        data = {
            "category": cat_id,
            "amount": amount,
            "note": note,
            "date": record_date,
            "created_by": self.user.id,
        }
        res = self.client.post(create_url, data, format="json")
        # fmt: off
        orm_exp = m.Expense.objects.get(  # pylint: disable=E1101; # noqa
            created_by=self.user2.id
        )
        # fmt: on
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        self.assertEqual(orm_exp.note, note)
        self.assertEqual(orm_exp.amount, amount)
        self.assertEqual(orm_exp.created_by.id, self.user2.id)

        expense_list_url = reverse("api:v1:expenses")
        response = self.client.get(expense_list_url, format="json")

        #  The expense was correctly linked to the other user account
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["note"], note)

        self.assertEqual(
            Decimal(response.data["results"][0]["amount"]), amount
        )
        self.assertEqual(response.data["count"], 1)

        # The initial user has no expenses, as expected
        self.client.force_login(self.user)

        response = self.client.get(expense_list_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 0)

    def test_can_not_created_malformed_expenses(self):
        self.client.force_login(self.user)
        create_url = reverse("api:v1:expenses")
        amount = self.generate_number(max_digits=10, decimal_places=2)
        note = BaseTest.generate_string(20)
        record_date = date.today().isoformat()

        # Try without category
        data = {"amount": amount, "note": note, "date": record_date}
        res = self.client.post(create_url, data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(res.data["category"]), 1)
        self.assertEqual(res.data["category"][0], "This field is required.")
        # NOTE: we raise serializers.ValidationError(
        # {"category": ['This field is required.']}), hence its code is 'invalid', not 'required'
        self.assertEqual(res.data["category"][0].code, "invalid")

        category_response, _ = self.create_category()
        category_id = category_response.data["id"]

        # Try with too long text
        long_note = BaseTest.generate_string(151)
        data = {
            "category": category_id,
            "amount": amount,
            "note": long_note,
            "date": record_date,
        }
        res = self.client.post(create_url, data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(res.data["note"]), 1)
        self.assertEqual(
            res.data["note"][0],
            "Ensure this field has no more than 150 characters.",
        )
        self.assertEqual(res.data["note"][0].code, "max_length")

        # Try with no amount
        data = {"category": category_id, "note": note, "date": record_date}
        res = self.client.post(create_url, data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(res.data["amount"]), 1)
        self.assertEqual(res.data["amount"][0], "This field is required.")
        self.assertEqual(res.data["amount"][0].code, "required")

        # Try with negative amount
        data = {
            "category": category_id,
            "amount": -42,
            "note": note,
            "date": record_date,
        }
        res = self.client.post(create_url, data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(res.data["amount"]), 1)
        self.assertEqual(
            res.data["amount"][0], "-42.00 should be more than 0."
        )
        self.assertEqual(res.data["amount"][0].code, "invalid")

        # Try with malfomed date
        data = {
            "category": category_id,
            "amount": amount,
            "note": note,
            "date": "89019-13-32",
        }
        res = self.client.post(create_url, data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(res.data["date"]), 1)
        err_msg = "Date has wrong format. Use one of these formats instead: YYYY-MM-DD."
        self.assertEqual(res.data["date"][0], err_msg)
        self.assertEqual(res.data["date"][0].code, "invalid")

        # Try with boolean instead of a date
        data = {
            "category": category_id,
            "amount": amount,
            "note": note,
            "date": True,
        }
        res = self.client.post(create_url, data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(res.data["date"]), 1)
        err_msg = "Date has wrong format. Use one of these formats instead: YYYY-MM-DD."
        self.assertEqual(res.data["date"][0], err_msg)
        self.assertEqual(res.data["date"][0].code, "invalid")

        # Try with a string instead of a date
        data = {
            "category": category_id,
            "amount": amount,
            "note": note,
            "date": "I am surely not a correctly formatte date",
        }
        res = self.client.post(create_url, data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(res.data["date"]), 1)
        err_msg = "Date has wrong format. Use one of these formats instead: YYYY-MM-DD."
        self.assertEqual(res.data["date"][0], err_msg)
        self.assertEqual(res.data["date"][0].code, "invalid")

        data = {
            "category": category_id,
            "amount": amount,
            "note": note,
            "date": record_date,
        }
        return (self.client.post(create_url, data, format="json"), data)

    # TODO: write me
    @skip
    def test_creating_expenses_with_malformed_date_will_not_throw_error_500(
        self,
    ):
        pass

    def test_get_expense_using_malformed_date_will_not_throw_error_500(self):
        self.client.force_login(self.user)

        get_expenses_url = reverse("api:v1:expenses")
        malformed_date = "I am definitely not a valid date value"

        filtered_url = (
            get_expenses_url + "?" + urlencode({"start": malformed_date})
        )
        response = self.client.get(filtered_url, format="json")
        self.assertNotEqual(response.status_code, 500)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(len(response.data["start"]), 1)
        err_msg = "Invalid start date format. Use 'YYYY-MM-DD'."
        self.assertEqual(response.data["start"][0], err_msg)
        self.assertEqual(response.data["start"][0].code, "invalid")

        filtered_url = (
            get_expenses_url + "?" + urlencode({"end": malformed_date})
        )
        response = self.client.get(filtered_url, format="json")
        self.assertNotEqual(response.status_code, 500)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(len(response.data["end"]), 1)
        err_msg = "Invalid date end format. Use 'YYYY-MM-DD'."
        self.assertEqual(response.data["end"][0], err_msg)
        self.assertEqual(response.data["end"][0].code, "invalid")

        malf_page_num = "I am definitely not a page number, but a string!"
        filtered_url = (
            get_expenses_url + "?" + urlencode({"page": malf_page_num})
        )
        response = self.client.get(filtered_url, format="json")
        self.assertNotEqual(response.status_code, 500)
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.data["detail"], "Invalid page.")
        self.assertEqual(response.data["detail"].code, "not_found")

    def test_can_not_use_other_people_categories_in_expense_creation(self):
        """
        Ensure we can create a new category object, and other users can't see it
        """
        self.client.force_login(self.user)

        # Create a category using the wrong field name
        category_response, _ = self.create_category()
        self.create_expense(category_response.data["id"])

        # Another user CANNOT use that category
        self.client.force_login(self.user2)

        other_expense_response, _ = self.create_expense(
            category_response.data["id"]
        )

        self.assertEqual(
            other_expense_response.status_code, status.HTTP_400_BAD_REQUEST
        )
        self.assertEqual(len(other_expense_response.data["category"]), 1)
        err_msg = f"Invalid pk \"{category_response.data['id']}\" - object does not exist."
        self.assertEqual(other_expense_response.data["category"][0], err_msg)
        self.assertEqual(
            other_expense_response.data["category"][0].code, "invalid"
        )

    def test_error_is_returned_if_category_is_missing(self):
        """
        Ensure we get an error if 'category' field is missing in the post body
        """
        self.client.force_login(self.user)

        # Create a category
        category_response, _ = self.create_category()

        create_url = reverse("api:v1:expenses")
        amount = self.generate_number(max_digits=10, decimal_places=2)
        note = BaseTest.generate_string(20)
        record_date = record_date = date.today().isoformat()

        data = {
            "bad-field": category_response.data["id"],
            "amount": amount,
            "note": note,
            "date": record_date,
        }
        other_expense_response, _ = (
            self.client.post(create_url, data, format="json"),
            data,
        )

        self.assertEqual(
            other_expense_response.status_code, status.HTTP_400_BAD_REQUEST
        )
        self.assertEqual(len(other_expense_response.data["category"]), 1)
        err_msg = "This field is required."
        self.assertEqual(other_expense_response.data["category"][0], err_msg)
        # NOTE: we raise serializers.ValidationError(
        # {"category": ['This field is required.']}), hence its code is 'invalid', not 'required'
        self.assertEqual(
            other_expense_response.data["category"][0].code, "invalid"
        )

    def test_deleted_expense_is_gone(self):
        """
        Ensure we can delete an expense
        """
        self.client.force_login(self.user)
        category_response, _ = self.create_category()
        _, _ = self.create_expense(category_response.data["id"])

        # Get the created expense's id (create API's response does not contain it, yet)
        expenses_list_url = reverse("api:v1:expenses")
        response = self.client.get(expenses_list_url, format="json")
        self.assertEqual(response.data["count"], 1)

        delete_expense_result, _ = self.delete_expense(
            response.data["results"][0]["id"]
        )
        self.assertEqual(
            delete_expense_result.status_code, status.HTTP_204_NO_CONTENT
        )

        # If the expense was deleted, we should now have 0 expenses left
        expenses_list_url = reverse("api:v1:expenses")
        response = self.client.get(expenses_list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 0)

    def test_delete_other_users_expenses_fail_with_404(self):
        self.client.force_login(self.user)
        category_response, _ = self.create_category()
        _, _ = self.create_expense(category_response.data["id"])

        # Get the created expense's id (create API's response does not contain it, yet)
        expenses_list_url = reverse("api:v1:expenses")
        response = self.client.get(expenses_list_url, format="json")
        expense_id = response.data["results"][0]["id"]

        # The other user CANNOT delete that expense
        self.client.force_login(self.user2)

        delete_expense_result, _ = self.delete_expense(expense_id)
        self.assertEqual(
            delete_expense_result.status_code, status.HTTP_404_NOT_FOUND
        )

    def test_delete_non_existing_expenses_return_404(self):
        self.client.force_login(self.user)

        # Assure no expenses exist
        expenses_list_url = reverse("api:v1:expenses")
        response = self.client.get(expenses_list_url, format="json")
        self.assertEqual(response.data["count"], 0)

        # Generate a non existing expense id, and confirm its deletion fails
        expense_id = random.randint(1, 90000)
        delete_expense_result, _ = self.delete_expense(expense_id)
        self.assertEqual(
            delete_expense_result.status_code, status.HTTP_404_NOT_FOUND
        )

    def test_can_update_all_expense_fields_at_once(self):
        self.client.force_login(self.user)
        category_response, _ = self.create_category()
        category_id = category_response.data["id"]
        _, _ = self.create_expense(category_id)

        # Get the created expense's id (create API's response does not
        # contain it, yet)
        expenses_list_url = reverse("api:v1:expenses")
        response = self.client.get(expenses_list_url, format="json")
        self.assertEqual(response.data["count"], 1)
        expense_id = response.data["results"][0]["id"]

        amount = self.generate_number()
        note = BaseTest.generate_string(20)
        record_date = date.today().isoformat()

        data = {
            "category": category_id,
            "amount": amount,
            "note": note,
            "date": record_date,
        }
        update_response, _ = self.replace_all_expense_fields(expense_id, data)
        self.assertEqual(update_response.status_code, status.HTTP_200_OK)

        expenses_list_url = reverse("api:v1:expenses")
        second_response = self.client.get(expenses_list_url, format="json")
        # Assure that since we did an update, and nothing was created
        self.assertEqual(second_response.data["count"], 1)
        self.assertEqual(second_response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            second_response.data["results"][0]["amount"], f"{amount}"
        )
        self.assertEqual(second_response.data["results"][0]["note"], note)
        self.assertEqual(
            second_response.data["results"][0]["date"], record_date
        )

    # Update the model with PATCH requests
    def test_can_partially_update_expense(self):
        self.client.force_login(self.user)
        category_response, _ = self.create_category()
        category_id = category_response.data["id"]
        _, _ = self.create_expense(category_id)

        # Get the created expense's id (create API's response does not
        # contain it, yet)
        expenses_list_url = reverse("api:v1:expenses")
        response = self.client.get(expenses_list_url, format="json")

        self.assertEqual(response.data["count"], 1)
        expense_id = response.data["results"][0]["id"]

        # Try updating only one field
        # No need to try each field singularly
        # otherwise we'd be testing django rest frametwork itself
        amount = self.generate_number()

        data = {"amount": amount}
        update_response, _ = self.partial_update_expense(expense_id, data)
        self.assertEqual(update_response.status_code, status.HTTP_200_OK)

        expenses_list_url = reverse("api:v1:expenses")
        fourth_response = self.client.get(expenses_list_url, format="json")

        # Assure that since we did an update, and nothing was created
        self.assertEqual(fourth_response.data["count"], 1)
        self.assertEqual(fourth_response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            fourth_response.data["results"][0]["amount"], f"{amount}"
        )

    # Update the model with PATCH requests
    def test_can_not_partially_update_expense_using_other_user_category(self):
        self.client.force_login(self.user)
        category_response, _ = self.create_category()
        # Another user CANNOT use this category
        category_id = category_response.data["id"]

        self.client.force_login(self.user2)

        second_category_response, _ = self.create_category()
        second_category_id = second_category_response.data["id"]
        _, _ = self.create_expense(second_category_id)

        # Get the created expense's id (create API's response does not contain
        # it, yet)
        expenses_list_url = reverse("api:v1:expenses")
        response = self.client.get(expenses_list_url, format="json")

        self.assertEqual(response.data["count"], 1)
        expense_id = response.data["results"][0]["id"]

        data = {"category": category_id}
        update_response, _ = self.partial_update_expense(expense_id, data)

        self.assertEqual(
            update_response.status_code, status.HTTP_400_BAD_REQUEST
        )
        self.assertEqual(
            update_response.data["category"][0], "This field is required."
        )

    def test_updating_with_malformed_data_returns_400(self):
        self.client.force_login(self.user)
        category_response, _ = self.create_category()
        # Another user CANNOT use this category
        category_id = category_response.data["id"]

        _, _ = self.create_expense(category_id)

        # Get the created expense's id (create API's response does not contain it, yet)
        expenses_list_url = reverse("api:v1:expenses")
        response = self.client.get(expenses_list_url, format="json")

        self.assertEqual(response.data["count"], 1)
        expense_id = response.data["results"][0]["id"]

        data = {
            "category": category_id,
            # FIXME: why is the too long text not triggering validation errors?
            "text": BaseTest.generate_string(151),
            "amount": -1,
        }
        update_response, _ = self.partial_update_expense(expense_id, data)
        self.assertEqual(
            update_response.status_code, status.HTTP_400_BAD_REQUEST
        )
        self.assertEqual(
            update_response.data["amount"][0], "-1.00 should be more than 0."
        )
        self.assertEqual(update_response.data["amount"][0].code, "invalid")

    # Update the model with PATCH requests
    def test_can_not_update_partially_using_non_existing_category(self):
        self.client.force_login(self.user)
        category_response, _ = self.create_category()
        category_id = category_response.data["id"]

        _, _ = self.create_expense(category_id)

        # Get the created expense's id (create API's response does not contain it, yet)
        expenses_list_url = reverse("api:v1:expenses")
        response = self.client.get(expenses_list_url, format="json")

        self.assertEqual(response.data["count"], 1)
        expense_id = response.data["results"][0]["id"]

        data = {"category": (category_id + random.randint(1, 90000))}
        update_response, _ = self.partial_update_expense(expense_id, data)

        # NICE TO HAVE: the api is returning too many details:
        # i.e. it says whether the category id does exists or not
        self.assertEqual(
            update_response.status_code, status.HTTP_400_BAD_REQUEST
        )
        # Uncomment the following line once the backend has been fixed to return
        # "Need a valid category id" for non exisitng categories
        # self.assertEqual(update_response.data[0], 'Need a valid category id')
        # Remove the following two lines once the backend has been fixed to return
        #  "Need a valid category id" for non exisitng categories
        err_msg = f"Invalid pk \"{data['category']}\" - object does not exist."
        self.assertEqual(update_response.data["category"][0], err_msg)
        self.assertEqual(
            update_response.data["category"][0].code, "does_not_exist"
        )

    # Can update the model with PUT requests
    def test_can_not_update_entirely_without_category(self):
        self.client.force_login(self.user)
        category_response, _ = self.create_category()
        category_id = category_response.data["id"]
        _, _ = self.create_expense(category_id)

        # Get the created expense's id (create API's response does not contain it, yet)
        expenses_list_url = reverse("api:v1:expenses")
        response = self.client.get(expenses_list_url, format="json")

        self.assertEqual(response.data["count"], 1)
        expense_id = response.data["results"][0]["id"]

        amount = self.generate_number(max_digits=10, decimal_places=2)
        note = BaseTest.generate_string(20)
        record_date = date.today().isoformat()

        data = {"amount": amount, "note": note, "date": record_date}
        update_response, _ = self.replace_all_expense_fields(expense_id, data)

        self.assertEqual(
            update_response.status_code, status.HTTP_400_BAD_REQUEST
        )
        self.assertEqual(
            update_response.data["category"][0], "This field is required."
        )

    # Can update the model with PUT requests
    def test_can_not_update_entirely_using_other_user_category(self):
        self.client.force_login(self.user)
        category_response, _ = self.create_category()
        # Another user CANNOT use this category
        category_id = category_response.data["id"]

        self.client.force_login(self.user2)

        second_category_response, _ = self.create_category()
        second_category_id = second_category_response.data["id"]
        expense_response, _ = self.create_expense(second_category_id)

        # Get the created expense's id (create API's response does not contain it, yet)
        expenses_list_url = reverse("api:v1:expenses")
        expense_response = self.client.get(expenses_list_url, format="json")

        self.assertEqual(expense_response.data["count"], 1)
        self.assertEqual(
            expense_response.data["results"][0]["category_id"],
            second_category_id,
        )
        expense_id = expense_response.data["results"][0]["id"]

        amount = self.generate_number(max_digits=10, decimal_places=2)
        note = BaseTest.generate_string(20)
        record_date = date.today().isoformat()

        data = {
            "category": category_id,
            "amount": amount,
            "note": note,
            "date": record_date,
        }
        update_response, _ = self.replace_all_expense_fields(expense_id, data)
        self.assertEqual(
            update_response.status_code, status.HTTP_400_BAD_REQUEST
        )
        self.assertEqual(
            update_response.data["category"][0], "This field is required."
        )

    # Can update the model with PUT requests
    # WRITE ME
    @skip
    def test_can_not_update_entirely_using_non_existing_category(self):
        pass

    def test_expenes_made_often_return_no_data_without_expenses(self):
        # Test for users with no expenses
        self.client.force_login(self.user)
        response = self.client.get(reverse("api:v1:expenses_made_often"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data["results"]), 0)

    def test_expenes_made_often_return_no_data_if_expenses_are_not_repeating_at_least_thrice_or_more(
        self,
    ):
        # Test for users with 100 completely different expenses
        self.client.force_login(self.user)
        for i in range(100):
            (
                category,
                _,  # fmt: off
            ) = m.Category.objects.get_or_create(  # pylint: disable=E1101; # noqa
                text=f"Category {i % 5 + 1}"
            )
            m.Expense.objects.create(  # pylint: disable=E1101; # noqa
                # Vary the amount for each expense
                amount=i * Decimal("10.01"),
                note=f"Test expense {i}",
                category=category,  # Use the created category
                date=date.today() - timedelta(days=i),  # Vary the date
                created_by=self.user,
            )
            # fmt: on
        response = self.client.get(reverse("api:v1:expenses_made_often"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data["results"]), 0)

    def test_expenses_made_often_return_no_data_for_users_with_3_same_expenses_more_than_6_months_ago(
        self,
    ):
        # Test for users with 3 expenses with the same amount, note, category_id
        # and text but made more than 6 months ago
        self.client.force_login(self.user)
        today = date.today()
        # NOTE: 192 (6*32) days ago should always be more than 6 months ago
        more_than_six_months_ago = today - timedelta(days=6 * 32)
        # fmt: off
        # Create a category for the expenses
        (
            category,
            _,
        ) = m.Category.objects.get_or_create(  # pylint: disable=E1101; # noqa
            text=BaseTest.generate_string(20)
        )

        for _ in range(3):
            m.Expense.objects.create(  # pylint: disable=E1101; # noqa
                amount=Decimal('10.1'),
                note="Test expense",
                category=category,  # Use the same category for all expenses
                date=more_than_six_months_ago,
                created_by=self.user,
            )
        # fmt: on
        response = self.client.get(reverse("api:v1:expenses_made_often"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data["results"]), 0)

    def test_expenses_made_often_does_not_show_other_users_expenses(self):
        # Test that other user data is not taken in account from this endpoint
        self.client.force_login(self.user)

        # fmt: off
        yesterday = (date.today() - timedelta(days=1)).strftime("%Y-%m-%d")
        (
            category,
            _,
        ) = m.Category.objects.get_or_create(  # pylint: disable=E1101; # noqa
            text=BaseTest.generate_string(20)
        )

        exp = None
        for _ in range(3):
            exp = m.Expense.objects.create(  # pylint: disable=E1101; # noqa
                amount=Decimal('10.1'),
                note="Test expense",
                category=category,
                date=yesterday,
                created_by=self.user,
            )
        # fmt: on
        # Initial user can see a made often response
        response1 = self.client.get(reverse("api:v1:expenses_made_often"))
        self.assertEqual(response1.status_code, 200)
        self.assertEqual(len(response1.data["results"]), 1)
        self.assertEqual(
            Decimal(response1.data["results"][0]["amount"]),
            Decimal(exp.amount),
        )
        self.assertEqual(
            response1.data["results"][0]["categoryId"], category.id
        )
        self.assertEqual(response1.data["results"][0]["note"], exp.note)

        self.client.force_login(self.user2)

        # The other user can not
        response2 = self.client.get(reverse("api:v1:expenses_made_often"))
        self.assertEqual(response2.status_code, 200)
        self.assertEqual(len(response2.data["results"]), 0)

    def test_expenses_made_of_return_only_100_pattern_even_if_there_are_101_patterns_of_expenses(
        self,
    ):
        # Test for users with 101 different patterns of expenses
        self.client.force_login(self.user)
        today = date.today()
        six_months_ago = today - timedelta(days=6 * 30)

        # fmt: off
        # Create 101 different patterns of expenses with 3 expenses each
        for i in range(101):
            (
                category,
                _,
            ) = m.Category.objects.get_or_create(  # pylint: disable=E1101; # noqa
                text=f"Category {i % 5 + 1}"
            )
            for _ in range(3):
                m.Expense.objects.create(  # pylint: disable=E1101; # noqa
                    amount=(i * Decimal('15.01')),
                    note=f"Test expense {i}",
                    category=category,
                    date=six_months_ago,
                    created_by=self.user,
                )
        # fmt: on
        response = self.client.get(reverse("api:v1:expenses_made_often"))
        self.assertEqual(response.status_code, 200)
        self.assertEqual(len(response.data["results"]), 100)

    def test_expense_made_often_correcly_handle_decmials(self):
        self.client.force_login(self.user)
        category_response, _ = self.create_category()
        category_id = category_response.data["id"]

        # Create expenses with decimal amounts
        m.Expense.objects.create(
            category_id=category_id,
            amount=Decimal("10.01"),
            note="Test expense 1",
            date=date.today(),
            created_by=self.user,
        )
        m.Expense.objects.create(
            category_id=category_id,
            amount=Decimal("10.01"),
            note="Test expense 1",
            date=date.today(),
            created_by=self.user,
        )
        m.Expense.objects.create(
            category_id=category_id,
            amount=Decimal("10.01"),
            note="Test expense 1",
            date=date.today(),
            created_by=self.user,
        )

        response = self.client.get(reverse("api:v1:expenses_made_often"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 1)
        self.assertEqual(
            Decimal(response.data["results"][0]["amount"]), Decimal("10.01")
        )

    def test_expense_made_often_correcly_serialize_decimals_to_strings(self):
        self.client.force_login(self.user)
        category_response, _ = self.create_category()
        category_id = category_response.data["id"]

        # Create expenses with decimal amounts
        m.Expense.objects.create(
            category_id=category_id,
            amount=Decimal("10.01"),
            note="Test expense 1",
            date=date.today(),
            created_by=self.user,
        )
        m.Expense.objects.create(
            category_id=category_id,
            amount=Decimal("10.01"),
            note="Test expense 1",
            date=date.today(),
            created_by=self.user,
        )
        m.Expense.objects.create(
            category_id=category_id,
            amount=Decimal("10.01"),
            note="Test expense 1",
            date=date.today(),
            created_by=self.user,
        )

        response = self.client.get(reverse("api:v1:expenses_made_often"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 1)
        self.assertEqual(
            type(response.data["results"][0]["amount"]), type("String")
        )

    def test_expense_url_returns_only_similar_text_ones_if_name_param_is_passed(
        self,
    ):
        self.client.force_login(self.user)
        category_response, _ = self.create_category()
        expense_response, expense_input = self.create_expense(
            category_response.data["id"]
        )
        self.assertEqual(expense_response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            expense_response.data["category"], category_response.data["id"]
        )
        self.assertEqual(expense_response.data["note"], expense_input["note"])
        self.assertEqual(expense_response.data["date"], expense_input["date"])
        # fmt: off
        self.assertEqual(
            m.Expense.objects.first().created_by, self.user
        )  # pylint: disable=E1101; # noqa
        # fmt: on
        expense_response2, expense_input2 = self.create_expense(
            category_response.data["id"]
        )
        self.assertEqual(
            expense_response2.status_code, status.HTTP_201_CREATED
        )
        # fmt: off
        self.assertEqual(
            m.Expense.objects.count(), 2 # pylint: disable=E1101; # noqa
        )
        # fmt: on
        # Passing no filters will return 2 expenses
        expenses_list_url = reverse("api:v1:expenses")
        response = self.client.get(expenses_list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data["results"][0]["category_text"],
            category_response.data["text"],
        )
        self.assertEqual(
            response.data["results"][0]["note"], expense_input["note"]
        )
        self.assertEqual(
            response.data["results"][0]["amount"],
            f'{expense_input["amount"]}',
        )
        self.assertEqual(
            response.data["results"][0]["date"], expense_input["date"]
        )
        self.assertEqual(
            response.data["results"][1]["category_text"],
            category_response.data["text"],
        )
        self.assertEqual(
            response.data["results"][1]["note"], expense_input2["note"]
        )
        self.assertEqual(
            response.data["results"][1]["amount"],
            f'{expense_input2["amount"]}',
        )
        self.assertEqual(
            response.data["results"][1]["date"], expense_input2["date"]
        )
        self.assertEqual(response.data["count"], 2)

        # Passing note filter will only return the first expense
        filter_by_name_url = (
            expenses_list_url + f"?name={expense_input['note']}"
        )
        response = self.client.get(filter_by_name_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data["results"][0]["category_text"],
            category_response.data["text"],
        )
        self.assertEqual(
            response.data["results"][0]["note"], expense_input["note"]
        )
        self.assertEqual(
            response.data["results"][0]["date"], expense_input["date"]
        )
        self.assertEqual(response.data["count"], 1)

    def test_expense_url_returns_no_similar_text_ones_if_no_similar_expenses_exist_with_similar_name_when_name_param_is_passed(
        self,
    ):
        self.client.force_login(self.user)
        category_response, _ = self.create_category()
        expense_response, expense_input = self.create_expense(
            category_response.data["id"]
        )
        self.assertEqual(expense_response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            expense_response.data["category"], category_response.data["id"]
        )
        self.assertEqual(expense_response.data["note"], expense_input["note"])
        self.assertEqual(expense_response.data["date"], expense_input["date"])
        # fmt: off
        self.assertEqual(
            m.Expense.objects.first().created_by, self.user
        )  # pylint: disable=E1101; # noqa
        # fmt: on
        expense_response2, expense_input2 = self.create_expense(
            category_response.data["id"]
        )
        self.assertEqual(
            expense_response2.status_code, status.HTTP_201_CREATED
        )
        # fmt: off
        self.assertEqual(
            m.Expense.objects.count(), 2
        )  # pylint: disable=E1101; # noqa
        # fmt: on
        # Passing no filters will return 2 expenses
        expenses_list_url = reverse("api:v1:expenses")
        response = self.client.get(expenses_list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data["results"][0]["category_text"],
            category_response.data["text"],
        )
        self.assertEqual(
            response.data["results"][0]["note"], expense_input["note"]
        )
        self.assertEqual(
            response.data["results"][0]["amount"],
            f'{expense_input["amount"]}',
        )
        self.assertEqual(
            response.data["results"][0]["date"], expense_input["date"]
        )
        self.assertEqual(
            response.data["results"][1]["category_text"],
            category_response.data["text"],
        )
        self.assertEqual(
            response.data["results"][1]["note"], expense_input2["note"]
        )
        self.assertEqual(
            response.data["results"][1]["amount"],
            f'{expense_input2["amount"]}',
        )
        self.assertEqual(
            response.data["results"][1]["date"], expense_input2["date"]
        )
        self.assertEqual(response.data["count"], 2)

        # Passing note filter will return no expenses if no match (we purposedly use
        # the two expenses notes combined, so that none should match)
        params = f"?name={expense_input['note']+expense_input2['note']}"
        filter_by_name_url = expenses_list_url + params
        response = self.client.get(filter_by_name_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 0)
        self.assertEqual(len(response.data["results"]), 0)

    def test_can_filter_expenses_by_expense_category_name(self):
        self.client.force_login(self.user)

        category_response, _ = self.create_category()
        _, _ = self.create_expense(category_response.data["id"])

        category_response2, _ = self.create_category()
        _, _ = self.create_expense(category_response2.data["id"])

        expenses_list_url = reverse("api:v1:expenses")
        response = self.client.get(expenses_list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 2)
        self.assertEqual(len(response.data["results"]), 2)

        param = urlencode({"category_name": category_response2.data["text"]})
        filtered_url = reverse("api:v1:expenses") + "?" + param
        list_response = self.client.get(filtered_url, format="json")
        self.assertEqual(list_response.status_code, status.HTTP_200_OK)
        self.assertEqual(list_response.data["count"], 1)
        self.assertEqual(len(list_response.data["results"]), 1)
        self.assertEqual(
            list_response.data["results"][0]["category_text"],
            category_response2.data["text"],
        )

    def test_can_filter_expenses_by_expense_category_id(self):
        self.client.force_login(self.user)
        category_response, _ = self.create_category()
        _, _ = self.create_expense(category_response.data["id"])

        category_response2, _ = self.create_category()
        _, _ = self.create_expense(category_response2.data["id"])

        expenses_list_url = reverse("api:v1:expenses")
        response = self.client.get(expenses_list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 2)
        self.assertEqual(len(response.data["results"]), 2)

        param = urlencode({"category_id": category_response2.data["id"]})
        filtered_url = reverse("api:v1:expenses") + "?" + param
        list_response = self.client.get(filtered_url, format="json")
        self.assertEqual(list_response.status_code, status.HTTP_200_OK)
        self.assertEqual(list_response.data["count"], 1)
        self.assertEqual(len(list_response.data["results"]), 1)
        self.assertEqual(
            list_response.data["results"][0]["category_id"],
            category_response2.data["id"],
        )

    def test_can_filter_expenses_by_expense_note(self):
        self.client.force_login(self.user)

        category_response, _ = self.create_category()
        _, expense_input = self.create_expense(category_response.data["id"])

        category_response2, _ = self.create_category()
        _, _ = self.create_expense(category_response2.data["id"])

        expenses_list_url = reverse("api:v1:expenses")
        response = self.client.get(expenses_list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 2)
        self.assertEqual(len(response.data["results"]), 2)

        filtered_url = (
            reverse("api:v1:expenses")
            + "?"
            + urlencode({"note": expense_input["note"]})
        )
        list_response = self.client.get(filtered_url, format="json")
        self.assertEqual(list_response.status_code, status.HTTP_200_OK)
        self.assertEqual(list_response.data["count"], 1)
        self.assertEqual(len(list_response.data["results"]), 1)
        self.assertEqual(
            list_response.data["results"][0]["note"], expense_input["note"]
        )

    def test_can_filter_expenses_fails_if_start_and_end_filters_are_conflicting(
        self,
    ):
        self.client.force_login(self.user)

        category_response, _ = self.create_category()
        _, today_exp = self.create_expense(category_response.data["id"])

        category_response2, _ = self.create_category()
        _, past_exp = self.create_expense(
            category_response2.data["id"], date_in_the_past=True
        )
        expenses_list_url = reverse("api:v1:expenses")
        response = self.client.get(expenses_list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 2)
        self.assertEqual(len(response.data["results"]), 2)

        start_param = urlencode({"start": today_exp["date"]})
        end_param = urlencode({"end": past_exp["date"]})
        filtered_url = (
            reverse("api:v1:expenses") + "?" + start_param + "&" + end_param
        )

        list_response = self.client.get(filtered_url, format="json")
        self.assertEqual(
            list_response.status_code, status.HTTP_400_BAD_REQUEST
        )
        self.assertEqual(
            list_response.data["non_field_errors"][0].code, "invalid"
        )
        self.assertEqual(
            str(list_response.data["non_field_errors"][0]),
            "Start date must be before end date",
        )

    def _generate_category_expenses_summary(self, n):
        """
        Helper function to generate two categories and n expenses for each category.
        Returns:
          - an object with the sum of all expenses per category
          - the earliest date in format "YYYY-MM-DD"
          - the latest date in format "YYYY-MM-DD"
        """
        category_response, _ = self.create_category()
        category_response2, _ = self.create_category()
        earliest_date = latest_date = date.today().strftime("%Y-%m-%d")
        sum_per_category = {
            category_response.data["text"]: 0,
            category_response2.data["text"]: 0,
        }
        for _ in range(n):
            for category in [category_response, category_response2]:
                _, e = self.create_expense(
                    category.data["id"], date_in_the_past=True
                )
                sum_per_category[category.data["text"]] += Decimal(e["amount"])
                latest_date = max(
                    datetime.datetime.strptime(e["date"], "%Y-%m-%d")
                    .date()
                    .strftime("%Y-%m-%d"),
                    latest_date,
                )
                earliest_date = min(
                    datetime.datetime.strptime(e["date"], "%Y-%m-%d")
                    .date()
                    .strftime("%Y-%m-%d"),
                    earliest_date,
                )

        return (
            sum_per_category,
            earliest_date,
            latest_date,
        )

    def _generate_expenses_data(self, n, user):
        """
        Helper function to generate two categories and at least n expenses for each.
        Makes sure we have at least two distinct expense dates.
        Returns:
            - the id of the first category
            - the id of the second category
        """
        category_response, _ = self.create_category()
        category_response2, _ = self.create_category()
        # Prevent we might accidentally create two categories with the same name
        while category_response.status_code != status.HTTP_201_CREATED:
            category_response2, _ = self.create_category()

        for _ in range(n):
            for category in [category_response, category_response2]:
                _, _ = self.create_expense(
                    category.data["id"], date_in_the_past=True
                )
        distinct_dates = list(
            (
                m.Expense.objects.filter(created_by=user)
                .values_list("date", flat=True)
                .distinct()
            )
        )

        # We keep creating expenses until we have at least two distinct dates
        # Hence we need to keep track of the actual number of expenses created
        actual_n = n
        while len(distinct_dates) < 2:
            for _ in range(n):
                for cat in [category_response, category_response2]:
                    _, e = self.create_expense(
                        cat.data["id"], date_in_the_past=True
                    )
                distinct_dates = list(
                    (
                        m.Expense.objects.filter(created_by=self.user)
                        .values_list("date", flat=True)
                        .distinct()
                    )
                )
                actual_n += 1

        # Confirm we have actual_n * 2 (i.e. 2 categories) expenses
        expenses_list_url = reverse("api:v1:expenses")
        response = self.client.get(expenses_list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], actual_n * 2)
        self.assertEqual(len(response.data["results"]), actual_n * 2)

        return (
            category_response.data["id"],
            category_response2.data["id"],
        )

    def test_expense_list_aggregate_by_category(self):
        """
        Test that the API returns the correct data when no dates are passed
        """
        self.client.force_login(self.user)

        (
            sum_per_category,
            _,
            _,
        ) = self._generate_category_expenses_summary(30)

        # Confirm we have 60 expenses
        expenses_list_url = reverse("api:v1:expenses")
        response = self.client.get(expenses_list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 60)
        self.assertEqual(len(response.data["results"]), 60)

        # Confirm we only have two results, one per category
        aggregate_url = reverse("api:v1:expenses_aggregate_by_cat")
        aggregate_response = self.client.get(aggregate_url, format="json")
        self.assertEqual(aggregate_response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(aggregate_response.data), 2)

        # Confirm the sum of all expenses per category is correct
        for category in aggregate_response.data:
            self.assertEqual(
                category["total_amount"],
                sum_per_category[category["category_text"]],
            )

    def test_expense_list_aggregate_by_category_with_end_and_start_dates(self):
        """
        Test that the API returns the correct data when both start and end dates are passed
        """
        self.client.force_login(self.user)

        (
            sum_per_category,
            earliest_date,
            latest_date,
        ) = self._generate_category_expenses_summary(30)

        # Confirm we have 60 expenses
        expenses_list_url = reverse("api:v1:expenses")
        # Add a start and end date to the request
        start_param = urlencode({"start": earliest_date})
        end_param = urlencode({"end": latest_date})
        filtered_url = expenses_list_url + "?" + start_param + "&" + end_param

        response = self.client.get(filtered_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 60)
        self.assertEqual(len(response.data["results"]), 60)

        # Confirm we only have two results, one per category
        aggregate_url = reverse("api:v1:expenses_aggregate_by_cat")
        filtered_url = aggregate_url + "?" + start_param + "&" + end_param
        aggregate_response = self.client.get(filtered_url, format="json")

        self.assertEqual(aggregate_response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(aggregate_response.data), 2)

        # Confirm the sum of all expenses per category is correct
        for category in aggregate_response.data:
            self.assertEqual(
                category["total_amount"],
                sum_per_category[category["category_text"]],
            )

    def _select_two_random_dates(self, distinct_dates):
        """
        Helper function to randomly select two dates from a list of distinct dates.
        It assumes we have at least two distinct dates.
        It makes sure that:
            - the start date is before the end date
            - the start date is not the same as the end date
            - we do not choose the first and the last dates
        Returns:
            - a start date in format "YYYY-MM-DD"
            - an end date in format "YYYY-MM-DD"
        """
        # Use the orm to calculate distinct dates from the list of all dates
        # This is needed to test the filtering
        distinct_dates_str = [
            dat.strftime("%Y-%m-%d") for dat in distinct_dates
        ]

        # NOTE: we do not know whether we do not actually know how many dates we have
        # so we need to check how many dates we have, and THEN randomly select two dates
        # from the list. We must make sure NOT to choose the first and the last dates
        # as we are testinf filtering a subset of the expenses
        amount_of_dates = len(distinct_dates_str)
        start_date = distinct_dates_str[random.randint(1, amount_of_dates - 2)]
        # We must make sure start_date is before end_date
        end_date = distinct_dates_str[random.randint(1, amount_of_dates - 2)]
        while start_date == end_date:
            end_date = distinct_dates_str[
                random.randint(1, amount_of_dates - 2)
            ]
        if start_date > end_date:
            start_date, end_date = end_date, start_date

        return start_date, end_date

    def _compute_aggreate_per_category(self, start_date, end_date, user):
        """
        Helper function to compute the sum of all expenses per category
        within the start and end dates.
        Returns:
            - a dictionary with the category_id as key and the total amount as value
        """
        # Use the orm to get the sum of all expenses per category
        orm_aggregate = (
            m.Expense.objects.filter(
                date__gte=start_date, date__lte=end_date, created_by=user
            )
            .values("category_id")
            .annotate(total_amount=Sum(F("amount")))
        )

        sum_per_category_filtered = {}
        for item in orm_aggregate:
            sum_per_category_filtered[item["category_id"]] = item[
                "total_amount"
            ]
        return sum_per_category_filtered

    # This is still a flaky test as per 2025-02-25
    # https://gitlab.com/micheleva/budgeteer/-/jobs/9219517196
    def test_expense_list_aggregate_subset_filtering(
        self,
    ):  # Refactor me out to complete #46
        """
        Test that the API actually does filter by start and end dates
        """
        print(
            "api.v1.tests.test_expenses.ExpenseTest.test_expense_list_aggregat"
            "e_subset_filtering is flaky!"
        )
        local_user, self.user2 = _create_users()
        self.client.force_login(local_user)

        # The number 30 was picked arbitrarily, to balance between having a slow test
        # and having a flaky test. Note that the helper will generate more than 30 expenses
        # or more, as it will keep creating expenses until we have at least two distinct dates
        (
            cat1_id,
            cat2_id,
        ) = self._generate_expenses_data(30, local_user)

        # Use the orm to calculate distinct dates from the list of all dates
        distinct_dates = list(
            (
                m.Expense.objects.filter(created_by=local_user)
                .values_list("date", flat=True)
                .distinct()
            )
        )
        start_date, end_date = self._select_two_random_dates(distinct_dates)

        sum_per_category_filtered = self._compute_aggreate_per_category(
            start_date, end_date, local_user
        )

        # Actually test the API
        print(
            "test_expense_list_aggregate_subset_filtering is flaky,"
            " address it to close #46"
            " See https://gitlab.com/micheleva/budgeteer/-/jobs/9102457853"
        )
        start_param = urlencode({"start": start_date})
        end_param = urlencode({"end": end_date})
        aggregate_url = reverse("api:v1:expenses_aggregate_by_cat")
        filtered_url = aggregate_url + "?" + start_param + "&" + end_param
        aggregate_response = self.client.get(filtered_url, format="json")

        self.assertEqual(aggregate_response.status_code, status.HTTP_200_OK)

        print(
            "Forensic data to debug the flakiness of test_expense_list_agg"
            "regate_subset_filtering",
            flush=True,
        )
        print(f"start_date: {start_date}, end_date: {end_date}", flush=True)
        print(
            f"sum_per_category_filtered: {sum_per_category_filtered}",
            flush=True,
        )
        print(
            f"aggregate_response.data: {aggregate_response.data}", flush=True
        )
        print(
            f"category 1 id: {cat1_id}, category 2 id: {cat2_id}", flush=True
        )

        # We must check if both cateegories actually have expenses within the start and end dates
        # i.e. if sum_per_category_filtered has one category to be 0, then we only expect one result
        # in the response
        if (
            sum_per_category_filtered[cat1_id] > 0
            and sum_per_category_filtered[cat2_id] > 0
        ):  # This is where the test is flaky
            # sum_per_category_filtered[cat1_id] > 0
            #     ^^^^^^^^^^^^^^^^^
            # KeyError: 54
            expected_results = 2
        else:
            expected_results = 1
        self.assertEqual(len(aggregate_response.data), expected_results)

        for category in aggregate_response.data:
            # NOTE: category looks like
            # OrderedDict([('category_text', 'vqyggytzwcrwknikshgg'),
            #              ('total_amount', 778204.0)
            # ])
            # We need to get to use the category_text to get the category_id
            category_id = m.Category.objects.get(
                text=category["category_text"],
                created_by=local_user,
            ).id

            # Confirm the sum of all expenses per category is correct
            self.assertEqual(
                Decimal(category["total_amount"]),
                sum_per_category_filtered[category_id],
            )

    def test_expense_list_aggregate_by_category_with_end_date_only(self):
        """
        Test that the API returns the correct data when only an end date is passed
        """
        self.client.force_login(self.user)

        (
            sum_per_category,
            _,
            latest_date,
        ) = self._generate_category_expenses_summary(30)

        # Confirm we have 60 expenses
        expenses_list_url = reverse("api:v1:expenses")
        # Add a start and end date to the request
        end_param = urlencode({"end": latest_date})
        filtered_url = expenses_list_url + "?" + end_param

        response = self.client.get(filtered_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 60)
        self.assertEqual(len(response.data["results"]), 60)

        # Confirm we only have two results, one per category
        aggregate_url = reverse("api:v1:expenses_aggregate_by_cat")
        filtered_url = aggregate_url + "?" + end_param
        aggregate_response = self.client.get(filtered_url, format="json")

        self.assertEqual(aggregate_response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(aggregate_response.data), 2)

        # Confirm the sum of all expenses per category is correct
        for category in aggregate_response.data:
            self.assertEqual(
                category["total_amount"],
                sum_per_category[category["category_text"]],
            )

    def test_expense_list_aggregate_by_category_with_start_date_only(self):
        """
        Test that the API returns the correct data when only a start date is passed
        """
        self.client.force_login(self.user)

        (
            sum_per_category,
            earliest_date,
            _,
        ) = self._generate_category_expenses_summary(30)

        # Confirm we have 60 expenses
        expenses_list_url = reverse("api:v1:expenses")
        # Add a start and end date to the request
        start_param = urlencode({"start": earliest_date})
        filtered_url = expenses_list_url + "?" + start_param

        response = self.client.get(filtered_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 60)
        self.assertEqual(len(response.data["results"]), 60)

        # Confirm we only have two results, one per category
        aggregate_url = reverse("api:v1:expenses_aggregate_by_cat")
        filtered_url = aggregate_url + "?" + start_param
        aggregate_response = self.client.get(filtered_url, format="json")

        self.assertEqual(aggregate_response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(aggregate_response.data), 2)

        # Confirm the sum of all expenses per category is correct
        for category in aggregate_response.data:
            self.assertEqual(
                category["total_amount"],
                sum_per_category[category["category_text"]],
            )

    def test_expense_list_aggregate_by_category_fail_if_start_and_end_dates_are_conflicting(
        self,
    ):
        """
        Test that the API returns a 400 error if the start and end dates are conflicting
        """

        self.client.force_login(self.user)

        (
            _,
            earliest_date,
            latest_date,
        ) = self._generate_category_expenses_summary(30)

        # Confirm we have 60 expenses
        expenses_list_url = reverse("api:v1:expenses")
        # Add a start and end date to the request
        start_param = urlencode({"start": latest_date})
        end_param = urlencode({"end": earliest_date})
        filtered_url = expenses_list_url + "?" + start_param + "&" + end_param

        response = self.client.get(filtered_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data["non_field_errors"][0].code, "invalid")
        self.assertEqual(
            str(response.data["non_field_errors"][0]),
            "Start date must be before end date",
        )

    @skip
    def test_expenses_does_not_throw_error_500_if_undefined_is_passed_as_category_id_param(
        self,
    ):
        self.fail("write me")

    @skip
    def test_expenses_does_not_throw_error_500_if_undefined_is_passed_as_start_param(
        self,
    ):
        self.fail("write me")

    @skip
    def test_expenses_does_not_throw_error_500_if_undefined_is_passed_as_end_param(
        self,
    ):
        self.fail("write me")

    @skip
    def test_expenses_does_not_throw_error_500_if_undefined_is_passed_as_start_and_as_end_param(
        self,
    ):
        self.fail("write me")

    def test_react_expenses_limit_res_is_empty_if_no_env_for_this_user(self):
        """
        WRITE ME
        """
        self.client.force_login(self.user)
        limits = reverse("api:v1:expenses")
        response = self.client.get(limits, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 0)
        self.assertEqual(len(response.data["results"]), 0)

    @patch("api.v1.views.expense_views.ExpenseList")
    @patch("budgets.serializers.BasicExpenseSerializer")
    def test_integrity_errors_are_catched_on_creation(
        self, moc_exp_serializer, _
    ):
        self.client.force_login(self.user)
        cat_r, _ = self.create_category()
        self.assertEqual(cat_r.status_code, status.HTTP_201_CREATED)

        exp_r, _ = self.create_expense(cat_r.data["id"], date_in_the_past=True)
        self.assertEqual(exp_r.status_code, status.HTTP_201_CREATED)

        # Force the serializer to Throw an Integrity error
        serializer = moc_exp_serializer.return_value
        msg = "I AM A MOCKED INTEGRITY ERROR. I HOPE YOU CATCH ME: ROAAAARR!"
        serializer.save.side_effect = IntegrityError(msg)

        # Create a mock request
        ok_data = {
            "category": cat_r.data["id"],
            "amount": self.generate_number(max_digits=10, decimal_places=2),
            "note": BaseTest.generate_string(20),
            "date": date.today().isoformat(),
        }

        # Create an instance of the class view
        view = ExpenseList()
        request = self.client.post(reverse("api:v1:expenses"), ok_data)

        # Set the request user
        view.request = request
        request.user = self.user

        # Confirm that the mocks works, and that ok_data actually thew an exception
        # and confirm it was correctly handled by the backend
        failed_res = view.perform_create(serializer)
        self.assertEqual(
            failed_res.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR
        )
        msg = (
            "Generic error thrown while creating the Expense. "
            "Refer the web container logs if you're the admin"
        )
        self.assertEqual(failed_res.data["non_field_errors"][0], msg)

    @patch("api.v1.views.expense_views.ExpenseDetail")
    @patch("budgets.serializers.BasicExpenseSerializer")
    def test_integrity_errors_are_catched_on_update(
        self, moc_exp_serializer, _
    ):
        self.client.force_login(self.user)
        cat_r, _ = self.create_category()
        self.assertEqual(cat_r.status_code, status.HTTP_201_CREATED)

        exp_r, _ = self.create_expense(cat_r.data["id"], date_in_the_past=True)
        self.assertEqual(exp_r.status_code, status.HTTP_201_CREATED)

        exp_list_url = reverse("api:v1:expenses")
        list_exp_r = self.client.get(exp_list_url, format="json")
        self.assertEqual(list_exp_r.data["count"], 1)
        exp_id = list_exp_r.data["results"][0]["id"]

        # Gererate a random date that is between 42 and 365 days ago
        record_date = (
            date.today() - timedelta(days=random.randint(42, 365))
        ).isoformat()
        data = {
            "category": cat_r.data["id"],
            "amount": self.generate_number(max_digits=10, decimal_places=2),
            "note": BaseTest.generate_string(20),
            "date": record_date,
        }
        update_r, _ = self.replace_all_expense_fields(exp_id, data)
        self.assertEqual(update_r.status_code, status.HTTP_200_OK)

        # Force the serializer to Throw an Integrity error
        serializer = moc_exp_serializer.return_value
        msg = "I AM A MOCKED INTEGRITY ERROR. I HOPE YOU CATCH ME: ROAAAARR!"
        serializer.save.side_effect = IntegrityError(msg)

        # Create a mock request
        record_date = (
            date.today() - timedelta(days=random.randint(42, 365))
        ).isoformat()
        ok_data = {
            "category": cat_r.data["id"],
            "amount": self.generate_number(max_digits=10, decimal_places=2),
            "note": BaseTest.generate_string(20),
            "date": record_date,
        }
        update_r, _ = self.replace_all_expense_fields(exp_id, data)
        self.assertEqual(update_r.status_code, status.HTTP_200_OK)

        # Create an instance of the class view
        view = ExpenseDetail()
        request = self.client.patch(reverse("api:v1:expenses"), ok_data)

        # Set the request user
        request.user = self.user
        # Mock the put request
        view.request = request
        view.request.method = "PACTH"

        # Confirm that the mocks works, and that ok_data actually thew an exception
        # and confirm it was correctly handled by the backend
        failed_res = view.perform_update(serializer)
        self.assertEqual(
            failed_res.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR
        )
        msg = (
            "Generic error thrown while updating the Expense. "
            "Refer the web container logs if you're the admin"
        )
        self.assertEqual(failed_res.data["non_field_errors"][0], msg)

    # WRITE ME
    @skip
    def test_custom_paginator_returns_all_items_with_huge_pages_param(self):
        """
        WRITE ME
        """
        pass
