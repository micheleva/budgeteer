# Copyright: (c) 2021, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from datetime import date
from datetime import timedelta
from decimal import Decimal
import logging

import random
from unittest.mock import patch
from unittest import skip

from django.contrib.auth.models import User
from django.db.utils import IntegrityError
from django.urls import reverse
from rest_framework import status
from api.v1.views.dividend_views import DividendsDeleteAll
from api.v1.views.dividend_views import DividendList
from api.v1.tests.test_base import BaseApiTest

from budgets.models import Account, Dividend
from budgets.tests.base import BaseTest


logger = logging.getLogger(__name__)


def _create_dividends(self, asset_text, year):
    dividends_to_create = []
    for month in range(1, 13, 4):
        local = random.randint(1, 900)
        rate = random.randint(90, 200)
        foreign = local * rate
        record_date = date(year, month, 1)

        dividend = Dividend(
            account=self.account,
            asset_text=asset_text,
            local_amount=local,
            foreign_amount=foreign,
            currency="USD",
            rate=rate,
            created_by=self.user,
            record_date=record_date,
        )
        dividends_to_create.append(dividend)

    return dividends_to_create


def _create_dividends_and_compute_symbol_aggregate(self):
    asset_texts = [f"Asset_{i}" for i in range(10)]
    local_sums = {}
    foreign_sums = {}

    for asset_text in asset_texts:
        local_sums[asset_text] = 0
        foreign_sums[asset_text] = 0

    for year in range(2015, 2025):
        for asset_text in asset_texts:
            ds = _create_dividends(self, asset_text, year)
            local_sum = sum(dividend.local_amount for dividend in ds)
            foreign_sum = sum(dividend.foreign_amount for dividend in ds)

            local_sums[asset_text] += local_sum
            foreign_sums[asset_text] += foreign_sum
            # fmt: off
            Dividend.objects.bulk_create(ds)  # pylint: disable=E1101; # noqa
            # fmt: on
    return (local_sums, foreign_sums, asset_texts)


def _create_dividends_and_compute_yearly_aggregate(self):
    dividends_to_create = []
    local_yearly_sums = {}
    # Generate 20 dividends from 2015 to 2023, and locally store their values
    for year in range(2015, 2024):
        local_yearly_sums[year] = 0

        for _ in range(20):
            local = random.randint(1, 900)
            rate = random.randint(90, 200)
            foreign = local * rate
            asset_text = f"Asset_{year}_{_ + 1}"
            record_date = date(
                year, random.randint(1, 12), random.randint(1, 27)
            )

            dividend = Dividend(
                account=self.account,
                asset_text=asset_text,
                local_amount=local,
                foreign_amount=foreign,
                currency="USD",
                rate=rate,
                created_by=self.user,
                record_date=record_date,
            )

            dividends_to_create.append(dividend)
            local_yearly_sums[year] += local

    # fmt: off
    Dividend.objects.bulk_create(  # pylint: disable=E1101; # noqa
        dividends_to_create
    )
    # fmt: on
    return local_yearly_sums


class DividendTest(BaseApiTest):
    """
    Test dividend model related v1 rest api behaviour
    """

    def setUp(self):  # pylint: disable=C0103; # noqa
        # Instrument the unit tests to fail if they run for too long
        super().setUp()
        # fmt: off
        self.user = User.objects.create_user(  # pylint: disable=E1101; # noqa
            username=BaseTest.generate_string(20),
            password=BaseTest.generate_string(20),
        )
        self.user2 = User.objects.create_user(  # pylint: disable=E1101; # noqa
            username=BaseTest.generate_string(20),
            password=BaseTest.generate_string(20),
        )
        self.account = Account.objects.create(  # pylint: disable=E1101; # noqa
            text="Account X",
            credit_entity="Broker 1",
            created_by=self.user,
        )
        self.account2 = Account.objects.create(  # pylint: disable=E1101; # noqa
            text="Account Y",
            credit_entity="Broker 1",
            created_by=self.user2,
        )
        # fmt: on

    def test_create_and_list_dividends(self):
        """
        Write me
        """
        self.client.force_login(self.user)

        # Create a Dividend
        dividend_create_response, data = self.create_dividend(self.account.id)

        # Same user (the owner) can see that Dividend
        # fmt: off
        d = Dividend.objects.filter(  # pylint: disable=E1101; # noqa
            created_by=self.user
        ).get()
        # fmt: on
        self.assertEqual(
            dividend_create_response.status_code, status.HTTP_201_CREATED
        )
        # fmt: off
        self.assertEqual(
            Dividend.objects.filter(created_by=self.user).count(), 1  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        self.assertEqual(d.account.id, self.account.id)
        self.assertEqual(d.asset_text, data["asset_text"])
        self.assertEqual(d.local_amount, data["local_amount"])
        self.assertEqual(d.foreign_amount, data["foreign_amount"])
        self.assertEqual(d.currency, data["currency"])
        self.assertEqual(d.rate, data["rate"])
        self.assertEqual(d.record_date.isoformat(), data["record_date"])

        # Same user (the owner) can see that Dividend
        list_url = reverse("api:v1:dividends")
        d_list_res = self.client.get(list_url, data, format="json")

        self.assertEqual(d_list_res.status_code, status.HTTP_200_OK)
        self.assertEqual(d_list_res.data["count"], 1)
        self.assertEqual(len(d_list_res.data["results"]), 1)
        self.assertEqual(
            d_list_res.data["results"][0]["account"], self.account.id
        )
        self.assertEqual(
            d_list_res.data["results"][0]["asset_text"], data["asset_text"]
        )
        self.assertEqual(
            d_list_res.data["results"][0]["local_amount"], data["local_amount"]
        )
        self.assertEqual(
            d_list_res.data["results"][0]["foreign_amount"],
            data["foreign_amount"],
        )
        self.assertEqual(
            d_list_res.data["results"][0]["currency"], data["currency"]
        )
        self.assertEqual(
            Decimal(d_list_res.data["results"][0]["rate"]),
            Decimal(data["rate"]),
        )
        self.assertEqual(
            d_list_res.data["results"][0]["record_date"], data["record_date"]
        )

        # Another user CANNOT see that Dividend
        self.client.force_login(self.user2)
        response = self.client.get(list_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 0)
        self.assertEqual(len(response.data["results"]), 0)

    def test_can_not_create_dividend_for_other_users(self):
        """
        Ensure we can NOT create a new dividend object linked to another user
        by posting other users' ID in the payload
        """
        self.client.force_login(self.user)
        rate = random.randint(1, 200)
        local_amount = random.randint(1, 900)
        foreign_amount = local_amount * rate

        acc_text = self.account.text
        data = {
            "account": self.account.id,
            "account_text": acc_text,
            "asset_text": BaseTest.generate_string(5),
            "foreign_amount": foreign_amount,
            "local_amount": local_amount,
            "currency": "USD",
            "rate": rate,
            "record_date": "2023-01-02",
            "created_by": self.user2.id,
        }

        response = self.client.post(
            reverse("api:v1:dividends_create"), data, format="json"
        )

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            Dividend.objects.filter(created_by=self.user).count(), 1  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        self.client.force_login(self.user2)
        response = self.client.get(reverse("api:v1:dividends"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 0)
        # fmt: off
        self.assertEqual(
            Dividend.objects.filter(created_by=self.user2).count(), 0  # pylint: disable=E1101; # noqa
        )
        # fmt: on

    def test_creating_malformed_dividends_throw_an_error(self):
        self.client.force_login(self.user)

        asset_text = BaseTest.generate_string(20)
        local_amount = random.randint(1, 90000)
        rate = random.randint(88, 170)
        foreign_amount = local_amount * rate
        currency = random.choice(["USD", "EUR"])
        record_date = date.today().isoformat()
        original_data = {
            "account": self.account.id,
            "asset_text": asset_text,
            "local_amount": local_amount,
            "foreign_amount": foreign_amount,
            "currency": currency,
            "rate": rate,
            "record_date": record_date,
        }

        # Try without account
        malformed_data_without_account = self.modify_data(
            original_data, "account", remove_field=True
        )
        res, _ = self.create_dividend_from_data(malformed_data_without_account)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(res.data["account"][0], "This field is required.")
        # FIXME: find out why Asset is not throwing errors anymore
        # (we had to change the view to simply return the error as string)
        # self.assertEqual(res.data['account'][0].code, 'invalid')

        # Try with None account
        malformed_account_none = self.modify_data(
            original_data, "account", modification_value=None
        )
        res, _ = self.create_dividend_from_data(malformed_account_none)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(res.data["account"][0], "This field is required.")
        # FIXME: find out why Asset is not throwing errors anymore
        # (we had to change the view to simply return the error as string)
        # self.assertEqual(res.data['account'][0].code, 'invalid')

        # Try with non existing account
        malformed_account_none = self.modify_data(
            original_data,  # FIXME: Why is this not "This field is required" ?
            "account",
            modification_value=573478988,
        )
        res, _ = self.create_dividend_from_data(malformed_account_none)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = 'Invalid pk "573478988" - object does not exist.'
        self.assertEqual(res.data["account"][0], msg)
        # FIXME: find out why Asset is not throwing errors anymore
        # (we had to change the view to simply return the error as string)
        # self.assertEqual(res.data['account'][0].code, 'invalid')

    def test_create_dividens_with_the_same_data_does_not_create_multiple_objects(
        self,
    ):
        self.client.force_login(self.user)

        # Create Dividend linked to the above created account
        _, data = self.create_dividend(self.account.id)
        # fmt: off
        self.assertEqual(
            Dividend.objects.filter(created_by=self.user).count(), 1  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        # Recreate the same dividend, and confirm no Dividend are actually created
        self.create_dividend_from_data(data)
        # fmt: off
        self.assertEqual(
            Dividend.objects.filter(created_by=self.user).count(), 1  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        # Another user can create an object with the same data
        # (apart from the account id)
        self.client.force_login(self.user2)
        account_response2, _ = self.create_account()
        account_id2 = account_response2.data["id"]

        # Update the data to be posted by the second user
        # to use user2's account id
        data2 = data
        data2["account"] = account_id2
        self.create_dividend_from_data(data2)
        # fmt: off
        self.assertEqual(
            Dividend.objects.filter(created_by=self.user2).count(), 1  # pylint: disable=E1101; # noqa
        )
        # fmt: on

    def test_dividend_deletion_does_not_affect_other_users(self):
        self.client.force_login(self.user)

        # Create Dividend linked to the above created account
        self.create_dividend(self.account.id)
        # fmt: off
        self.assertEqual(
            Dividend.objects.filter(created_by=self.user).count(), 1  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        # Another user create another dividend object
        self.client.force_login(self.user2)

        other_account_response, _ = self.create_account()
        other_account_id = other_account_response.data["id"]

        self.create_dividend(other_account_id)
        # fmt: off
        self.assertEqual(
            Dividend.objects.count(), 2  # pylint: disable=E1101; # noqa
        )
        # Only other user dividends are deleted
        self.delete_all_dividends()
        self.assertEqual(
            Dividend.objects.count(), 1  # pylint: disable=E1101; # noqa
        )
        # fmt: on

    def test_user_with_no_dividends_should_get_zero_results(self):
        # fmt: off
        _ = Dividend.objects.create(  # pylint: disable=E1101; # noqa
            account=self.account,
            asset_text=BaseTest.generate_string(5),
            local_amount=random.randint(1, 900),
            currency="USD",
            rate=random.randint(1, 200),
            created_by=self.user,
            record_date="2023-01-01",
        )
        # fmt: on
        self.client.force_login(self.user2)
        response = self.client.get(reverse("api:v1:dividends"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 0)

    def test_users_should_not_see_other_users_dividends(self):
        # fmt: off
        _ = Dividend.objects.create(  # pylint: disable=E1101; # noqa
            account=self.account,
            asset_text=BaseTest.generate_string(5),
            local_amount=random.randint(1, 900),
            currency="USD",
            rate=random.randint(1, 200),
            created_by=self.user,
            record_date="2023-01-01",
        )
        # fmt: on
        self.client.force_login(self.user)
        response = self.client.get(reverse("api:v1:dividends"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 1)

        self.client.force_login(self.user2)
        response = self.client.get(reverse("api:v1:dividends"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 0)

    def test_user_with_no_dividends_get_zero_results_in_yearly_aggregate(self):
        self.client.force_login(self.user)
        response = self.client.get(reverse("api:v1:dividends_aggregate"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 0)
        self.assertEqual(response.data["count"], 0)

    def test_user_should_see_the_correct_amount_for_year_aggregate(self):
        self.client.force_login(self.user)

        # Generate 20 dividends from 2015 to 2023, and locally store
        # their values
        local_yearly_sums = _create_dividends_and_compute_yearly_aggregate(
            self
        )

        response = self.client.get(reverse("api:v1:dividends_aggregate"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            len(response.data["results"]), 10
        )  # Stats for 9, and the grand total
        self.assertEqual(
            response.data["count"], 10
        )  # Stats for 9, and the grand total

        for index, year in enumerate(range(2015, 2024), 1):
            self.assertEqual(response.data["results"][index - 1]["year"], year)
            self.assertEqual(
                response.data["results"][index - 1]["local_total"],
                local_yearly_sums[year],
            )

        total_local_sum = sum(local_yearly_sums.values())
        self.assertEqual(response.data["results"][-1]["year"], -1)
        self.assertEqual(
            response.data["results"][-1]["local_total"], total_local_sum
        )

    def test_user_should_not_see_other_users_dividends_in_yearly_aggregate(
        self,
    ):
        self.client.force_login(self.user)

        # Generate 20 dividends from 2015 to 2023, and locally store their
        # values
        local_yearly_sums = _create_dividends_and_compute_yearly_aggregate(
            self
        )
        response = self.client.get(reverse("api:v1:dividends_aggregate"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            len(response.data["results"]), 10
        )  # Stats for 9, and the grand total
        self.assertEqual(
            response.data["count"], 10
        )  # Stats for 9, and the grand total

        self.client.force_login(self.user2)
        response = self.client.get(reverse("api:v1:dividends"))
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 0)

    def test_user_with_no_dividends_get_zero_results_in_symbol_aggregate(self):
        self.client.force_login(self.user)
        response = self.client.get(
            reverse("api:v1:dividends_aggregate_by_symbol")
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 0)
        self.assertEqual(response.data["count"], 0)

    def test_user_should_see_the_correct_amount_for_symbol_aggregate(self):
        self.client.force_login(self.user)

        (
            local_sums,
            foreign_sums,
            asset_texts,
        ) = _create_dividends_and_compute_symbol_aggregate(self)

        response = self.client.get(
            reverse("api:v1:dividends_aggregate_by_symbol")
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), len(asset_texts))
        self.assertEqual(response.data["count"], len(asset_texts))

        # Since the API will return dividends aggregate in a different order
        # i.e. desc by sum of local_amount, we can't just loop through
        # asset_text, we need another loop
        for asset_text in asset_texts:
            found = False
            for result in response.data["results"]:
                if result["asset_text"] == asset_text:
                    found = True
                    self.assertEqual(
                        result["total_local_amount"], local_sums[asset_text]
                    )
                    self.assertEqual(
                        result["total_foreign_amount"],
                        foreign_sums[asset_text],
                    )
                    break
            self.assertTrue(
                found, f"No results found for asset_text: {asset_text}"
            )

    def test_user_should_not_see_other_users_dividends_in_symbol_aggregate(
        self,
    ):
        self.client.force_login(self.user)

        _, _, asset_texts = _create_dividends_and_compute_symbol_aggregate(
            self
        )

        response = self.client.get(
            reverse("api:v1:dividends_aggregate_by_symbol")
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), len(asset_texts))
        self.assertEqual(response.data["count"], len(asset_texts))

        self.client.force_login(self.user2)
        response = self.client.get(
            reverse("api:v1:dividends_aggregate_by_symbol")
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 0)

    @patch("api.v1.views.dividend_views.DividendList")
    @patch("budgets.serializers.DividendSerializer")
    def test_integrity_errors_are_catched_on_create(
        self, mock_cat_serializer, _
    ):
        self.client.force_login(self.user)

        # Create an account
        response, data = self.create_account()
        # fmt: off
        acc = Account.objects.filter(  # pylint: disable=E1101; # noqa
            created_by=self.user.id
        ).first()
        # fmt: on
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Confirm we can create a dividend before mocking
        # Create a Dividend
        dividend_create_response, data = self.create_dividend(self.account.id)
        # Same user (the owner) can see that Dividend
        # fmt: off
        d = Dividend.objects.filter(  # pylint: disable=E1101; # noqa
            created_by=self.user
        ).get()
        self.assertEqual(
            dividend_create_response.status_code, status.HTTP_201_CREATED
        )
        self.assertEqual(
            Dividend.objects.filter(created_by=self.user).count(), 1  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        self.assertEqual(d.account.id, self.account.id)
        self.assertEqual(d.asset_text, data["asset_text"])
        self.assertEqual(d.local_amount, data["local_amount"])
        self.assertEqual(d.foreign_amount, data["foreign_amount"])
        self.assertEqual(d.currency, data["currency"])
        self.assertEqual(d.rate, data["rate"])
        self.assertEqual(d.record_date.isoformat(), data["record_date"])

        # Force the serializer to Throw an Integrity error
        serializer = mock_cat_serializer.return_value
        msg = "I AM A MOCKED INTEGRITY ERROR. I HOPE YOU CATCH ME: ROAAAARR!"
        serializer.save.side_effect = IntegrityError(msg)

        # Create a mock request
        local_amount = random.randint(1, 90000)
        rate = random.randint(88, 170)
        foreign_amount = local_amount * rate
        ok_data = {
            "account": acc.id,
            "asset_text": BaseTest.generate_string(20),
            "local_amount": local_amount,
            "foreign_amount": local_amount * rate,
            "currency": random.choice(["USD", "EUR"]),
            "rate": rate,
            "record_date": (
                date.today() - timedelta(days=random.randint(42, 365))
            ).isoformat(),
        }

        # Create an instance of the view
        view = DividendList()
        request = self.client.post(reverse("api:v1:dividends_create"), ok_data)

        # Set the request user
        view.request = request
        request.user = self.user

        # Confirm that the mocks works, and that ok_data actually thew an
        # exception and confirm it was correctly handled by the backend
        failed_res = view.perform_create(serializer)
        self.assertEqual(
            failed_res.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR
        )
        msg = (
            "Generic error thrown while creating the Dividend. "
            "Refer the web container logs if you're the admin"
        )
        self.assertEqual(failed_res.data["non_field_errors"][0], msg)

    # WRITE ME
    @skip
    def test_integrity_errors_are_catched_on_update(
        self, mock_cat_serializer, _
    ):
        pass

    @patch("api.v1.views.dividend_views.DividendsDeleteAll")
    @patch("django.db.models.query.QuerySet")
    def test_integrity_errors_are_catched_on_delete(
        self, mock_cat_serializer, _
    ):
        self.client.force_login(self.user)
        acc_r, _ = self.create_account()
        self.assertEqual(acc_r.status_code, status.HTTP_201_CREATED)

        # Confirm dividends can be deleted before the view is mocked
        res, _ = self.create_dividend(acc_r.data["id"])
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        res = self.client.get(reverse("api:v1:dividends"), format="json")
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data["count"], 1)
        self.assertEqual(len(res.data["results"]), 1)

        # Confirm assets can be deleted before the view is mocked
        res = self.delete_all_dividends()
        self.assertEqual(res.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(res.data["records_deleted"], 1)

        res = self.client.get(reverse("api:v1:dividends"), format="json")
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data["count"], 0)
        self.assertEqual(len(res.data["results"]), 0)

        res, _ = self.create_dividend(acc_r.data["id"])
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        # Force the serializer to Throw an Integrity error
        serializer = mock_cat_serializer.return_value
        msg = "I AM A MOCKED INTEGRITY ERROR. I HOPE YOU CATCH ME: ROAAAARR!"
        serializer.delete.side_effect = IntegrityError(msg)

        # Create an instance of the view
        view = DividendsDeleteAll()
        request = self.client.delete(reverse("api:v1:dividends_delete"))

        # Set the request user
        view.request = request
        request.user = self.user

        # Confirm that the mocks works, and that ok_data actually thew an
        # exception and confirm it was correctly handled by the backend
        failed_res = view.perform_destroy(serializer)
        self.assertEqual(
            failed_res.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR
        )
        msg = (
            "Generic error thrown while deleting all your Dividends. "
            "Refer the web container logs if you're the admin"
        )
        self.assertEqual(failed_res.data["non_field_errors"][0], msg)
