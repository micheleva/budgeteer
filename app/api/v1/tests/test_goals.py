# Copyright: (c) 2021, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
import logging
import os
import random
from unittest.mock import patch
from unittest import skip
from urllib.parse import urlencode

from django.contrib.auth.models import User
from django.db.utils import IntegrityError

from django.urls import reverse
from rest_framework import status

from api.v1.views.goal_views import GoalDetail
from api.v1.views.goal_views import GoalList
from api.v1.tests.test_base import BaseApiTest

import budgets.models as m
from budgets.tests.base import BaseTest


logger = logging.getLogger(__name__)


class GoalTest(BaseApiTest):
    """
    Test Goal model related v1 rest api behaviour
    """

    def setUp(self):  # pylint: disable=C0103; # noqa
        # Instrument the unit tests to fail if they run for too long
        super().setUp()
        self.user = User.objects.create_user(
            username=BaseTest.generate_string(20),
            password=BaseTest.generate_string(20),
        )
        self.user2 = User.objects.create_user(
            username=BaseTest.generate_string(20),
            password=BaseTest.generate_string(20),
        )

    def test_can_create_goals(self):
        """
        Ensure we can create a new goal
        """
        self.client.force_login(self.user)
        res, goal = self.create_goal()
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        if "errors" in res.data:
            self.fail(
                "Goal creation response's body included at least one error!"
            )

        goal_list_url = reverse("api:v1:goals")
        list_response = self.client.get(goal_list_url, format="json")

        self.assertEqual(list_response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            list_response.data["results"][0]["text"], goal["text"]
        )
        self.assertEqual(
            list_response.data["results"][0]["amount"], goal["amount"]
        )
        self.assertEqual(
            list_response.data["results"][0]["note"], goal["note"]
        )
        self.assertEqual(
            list_response.data["results"][0]["is_archived"],
            goal["is_archived"],
        )
        self.assertEqual(list_response.data["count"], 1)
        self.assertEqual(len(list_response.data["results"]), 1)

    def test_can_create_goal_with_null_note(self):
        """
        Ensure we can create a new goal that has None in the note field
        """
        self.client.force_login(self.user)
        create_url = reverse("api:v1:goals")

        # POST None value for note field
        text = BaseTest.generate_string(20)
        amount = random.randint(1, 90000)
        is_archived = bool(random.randint(0, 1))
        data = {
            "text": text,
            "amount": amount,
            "note": None,
            "is_archived": is_archived,
        }
        res = self.client.post(create_url, data, format="json")
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        if "errors" in res.data:
            self.fail(
                "Goal creation response's body included at least one error!"
            )

        goal_list_url = reverse("api:v1:goals")
        list_response = self.client.get(goal_list_url, format="json")

        self.assertEqual(list_response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            list_response.data["results"][0]["text"], data["text"]
        )
        self.assertEqual(
            list_response.data["results"][0]["amount"], data["amount"]
        )
        self.assertEqual(
            list_response.data["results"][0]["note"], data["note"]
        )
        self.assertEqual(
            list_response.data["results"][0]["is_archived"],
            data["is_archived"],
        )
        self.assertEqual(list_response.data["count"], 1)
        self.assertEqual(len(list_response.data["results"]), 1)

        # Do not post note field at all
        text2 = BaseTest.generate_string(20)
        amount2 = random.randint(1, 90000)
        is_archived2 = bool(random.randint(0, 1))
        data2 = {"text": text2, "amount": amount2, "is_archived": is_archived2}
        res2 = self.client.post(create_url, data2, format="json")
        self.assertEqual(res2.status_code, status.HTTP_201_CREATED)

        if "errors" in res2.data:
            self.fail(
                "Goal creation response's body included at least one error!"
            )

        list_response2 = self.client.get(goal_list_url, format="json")

        self.assertEqual(list_response2.status_code, status.HTTP_200_OK)
        self.assertEqual(
            list_response2.data["results"][1]["text"], data2["text"]
        )
        self.assertEqual(
            list_response2.data["results"][1]["amount"], data2["amount"]
        )
        self.assertEqual(list_response2.data["results"][1]["note"], None)
        self.assertEqual(
            list_response2.data["results"][1]["is_archived"],
            data2["is_archived"],
        )
        self.assertEqual(list_response2.data["count"], 2)
        self.assertEqual(len(list_response2.data["results"]), 2)

    # WRITE ME
    @skip
    def test_can_not_create_goals_for_other_users(self):
        """
        Ensure we can NOT create a new goal object linked to another user
        by posting other users' ID in the payload
        """
        pass

    def test_can_edit_goals(self):
        self.client.force_login(self.user)
        _, _ = self.create_goal()

        goal_list_url = reverse("api:v1:goals")
        list_response = self.client.get(goal_list_url, format="json")
        # Get the created goal's id (create API's response does
        # not contain it, yet)
        goal_id = list_response.data["results"][0]["id"]

        self.assertEqual(list_response.status_code, status.HTTP_200_OK)
        self.assertEqual(list_response.data["count"], 1)
        self.assertEqual(len(list_response.data["results"]), 1)

        text = BaseTest.generate_string(20)
        amount = random.randint(1, 90000)
        note = BaseTest.generate_string(20)
        is_archived = bool(random.randint(0, 1))
        data = {
            "text": text,
            "amount": amount,
            "note": note,
            "is_archived": is_archived,
        }
        edit_respone = self.edit_goal(goal_id, data)
        self.assertEqual(edit_respone.status_code, status.HTTP_200_OK)

        edited_goal_response = self.get_goal(goal_id)
        self.assertEqual(edited_goal_response.status_code, status.HTTP_200_OK)
        self.assertEqual(edited_goal_response.data["text"], text)
        self.assertEqual(edited_goal_response.data["amount"], amount)
        self.assertEqual(edited_goal_response.data["note"], note)
        self.assertEqual(edited_goal_response.data["is_archived"], is_archived)

        if "errors" in list_response.data:
            self.fail("Goal edit response's body included at least one error!")

    def test_can_delete_goals(self):
        self.client.force_login(self.user)
        _, _ = self.create_goal()

        goal_list_url = reverse("api:v1:goals")
        list_response = self.client.get(goal_list_url, format="json")
        self.assertEqual(list_response.status_code, status.HTTP_200_OK)
        self.assertEqual(list_response.data["count"], 1)
        self.assertEqual(len(list_response.data["results"]), 1)

        # Get the created goal's id (create API's response does not contain it, yet)
        goal_id = list_response.data["results"][0]["id"]
        delete_response = self.delete_goal(goal_id)
        self.assertEqual(
            delete_response.status_code, status.HTTP_204_NO_CONTENT
        )

        second_list_reponse = self.client.get(goal_list_url, format="json")
        self.assertEqual(second_list_reponse.status_code, status.HTTP_200_OK)

        self.assertEqual(second_list_reponse.data["count"], 0)
        self.assertEqual(len(second_list_reponse.data["results"]), 0)

    def test_can_not_create_malformed_goals(self):
        self.client.force_login(self.user)
        create_url = reverse("api:v1:goals")

        # Try with a too long text
        text = BaseTest.generate_string(21)
        amount = random.randint(1, 90000)
        note = BaseTest.generate_string(20)
        is_archived = random.choice([True, False])
        data = {
            "text": text,
            "amount": amount,
            "note": note,
            "is_archived": is_archived,
        }
        response = self.client.post(create_url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        err_msg = "Ensure this field has no more than 20 characters."
        self.assertEqual(response.data["text"][0], err_msg)
        self.assertEqual(response.data["text"][0].code, "max_length")

        # Try with a too long note
        text = BaseTest.generate_string(20)
        amount = random.randint(1, 90000)
        note = BaseTest.generate_string(51)
        is_archived = random.choice([True, False])
        data = {
            "text": text,
            "amount": amount,
            "note": note,
            "is_archived": is_archived,
        }
        response = self.client.post(create_url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        err_msg = "Ensure this field has no more than 50 characters."
        self.assertEqual(response.data["note"][0], err_msg)
        self.assertEqual(response.data["note"][0].code, "max_length")

        # Try with a string in the amount field
        text = BaseTest.generate_string(20)
        amount = BaseTest.generate_string(25)
        note = BaseTest.generate_string(20)
        is_archived = random.choice([True, False])
        data = {
            "text": text,
            "amount": amount,
            "note": note,
            "is_archived": is_archived,
        }
        response = self.client.post(create_url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.data["amount"][0], "A valid integer is required."
        )
        self.assertEqual(response.data["amount"][0].code, "invalid")

        # amount: missing
        text = BaseTest.generate_string(20)
        note = BaseTest.generate_string(20)
        is_archived = random.choice([True, False])
        data = {"text": text, "note": note, "is_archived": is_archived}
        response = self.client.post(create_url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data["amount"][0], "This field is required.")
        self.assertEqual(response.data["amount"][0].code, "required")

        # amount: 0
        text = BaseTest.generate_string(20)
        amount = 0
        note = BaseTest.generate_string(20)
        is_archived = random.choice([True, False])
        data = {
            "text": text,
            "amount": amount,
            "note": note,
            "is_archived": is_archived,
        }
        response = self.client.post(create_url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.data["amount"][0], "%s should be more than 0." % amount
        )
        self.assertEqual(response.data["amount"][0].code, "invalid")

        # amount: negative number
        text = BaseTest.generate_string(20)
        amount = random.randint(-1337, -42)
        note = BaseTest.generate_string(20)
        is_archived = random.choice([True, False])
        data = {
            "text": text,
            "amount": amount,
            "note": note,
            "is_archived": is_archived,
        }
        response = self.client.post(create_url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.data["amount"][0], "%s should be more than 0." % amount
        )
        self.assertEqual(response.data["amount"][0].code, "invalid")

        # Try with missing text
        amount = random.randint(1, 90000)
        note = BaseTest.generate_string(20)
        is_archived = random.choice([True, False])
        data = {"amount": amount, "note": note, "is_archived": is_archived}
        response = self.client.post(create_url, data, format="json")

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(response.data["text"][0], "This field is required.")
        self.assertEqual(response.data["text"][0].code, "required")

    def test_can_not_create_goals_with_same_name(self):
        self.client.force_login(self.user)
        _, goal = self.create_goal()

        goal_list_url = reverse("api:v1:goals")
        list_response = self.client.get(goal_list_url, format="json")
        self.assertEqual(list_response.status_code, status.HTTP_200_OK)
        self.assertEqual(list_response.data["count"], 1)
        self.assertEqual(len(list_response.data["results"]), 1)

        create_url = reverse("api:v1:goals")
        text = goal["text"]
        amount = goal["amount"]
        note = goal["note"]
        is_archived = goal["is_archived"]
        data = {
            "text": text,
            "amount": amount,
            "note": note,
            "is_archived": is_archived,
        }
        create_duplicated_response = self.client.post(
            create_url, data, format="json"
        )
        self.assertEqual(
            create_duplicated_response.status_code, status.HTTP_400_BAD_REQUEST
        )

        self.assertEqual(
            create_duplicated_response.data["text"][0].code, "invalid"
        )
        self.assertEqual(
            create_duplicated_response.data["text"][0],
            "A Goal with the same text already exists for this user.",
        )

        second_list_response = self.client.get(goal_list_url, format="json")
        self.assertEqual(second_list_response.status_code, status.HTTP_200_OK)
        self.assertEqual(second_list_response.data["count"], 1)
        self.assertEqual(len(second_list_response.data["results"]), 1)

    def test_different_users_can_have_goals_with_the_same_name(self):
        self.client.force_login(self.user)

        # Create a goal
        response, goal = self.create_goal()

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            m.Goal.objects.get(created_by=self.user).text, goal["text"]  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        categoriy_list_url = reverse("api:v1:goals")
        response = self.client.get(categoriy_list_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["text"], goal["text"])
        self.assertEqual(response.data["count"], 1)

        # Another user can create a goal with the same name
        self.client.force_login(self.user2)

        create_url = reverse("api:v1:goals")
        text = goal["text"]
        amount = random.randint(1, 90000)
        note = BaseTest.generate_string(20)
        is_archived = bool(random.randint(0, 1))
        data = {
            "text": text,
            "amount": amount,
            "note": note,
            "is_archived": is_archived,
        }
        same_text_goal_response = self.client.post(
            create_url, data, format="json"
        )
        self.assertEqual(
            same_text_goal_response.status_code, status.HTTP_201_CREATED
        )
        # fmt: off
        self.assertEqual(
            m.Goal.objects.filter(created_by=self.user2).get().text,  # pylint: disable=E1101; # noqa
            goal["text"],
        )
        # fmt: on
        categoriy_list_url = reverse("api:v1:goals")
        response = self.client.get(categoriy_list_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["text"], goal["text"])
        self.assertEqual(response.data["count"], 1)

        # fmt: off
        self.assertEqual(
            m.Goal.objects.filter(text=goal["text"]).count(), 2  # pylint: disable=E1101; # noqa
        )
        # fmt: on

    def test_can_not_see_other_users_goals(self):
        self.client.force_login(self.user)
        _, _ = self.create_goal()

        goal_list_url = reverse("api:v1:goals")
        list_reponse = self.client.get(goal_list_url, format="json")
        self.assertEqual(list_reponse.data["count"], 1)
        self.assertEqual(len(list_reponse.data["results"]), 1)

        self.client.logout()

        # Another user CANNOT see that goal
        self.client.force_login(self.user2)

        second_list_reponse = self.client.get(goal_list_url, format="json")
        self.assertEqual(second_list_reponse.data["count"], 0)
        self.assertEqual(len(second_list_reponse.data["results"]), 0)

    def test_can_not_edit_other_users_goals(self):
        self.client.force_login(self.user)
        _, _ = self.create_goal()

        goal_list_url = reverse("api:v1:goals")
        list_reponse = self.client.get(goal_list_url, format="json")
        # Get the created goal's id (create API's response does not
        # contain it, yet)
        goal_id = list_reponse.data["results"][0]["id"]
        goal_obj = list_reponse.data["results"][0]

        self.assertEqual(list_reponse.status_code, status.HTTP_200_OK)
        self.assertEqual(list_reponse.data["count"], 1)
        self.assertEqual(len(list_reponse.data["results"]), 1)

        # Another user CANNOT edit that goal
        self.client.force_login(self.user2)
        text = BaseTest.generate_string(20)
        amount = random.randint(1, 90000)
        note = BaseTest.generate_string(20)
        is_archived = bool(random.randint(0, 1))
        data = {
            "text": text,
            "amount": amount,
            "note": note,
            "is_archived": is_archived,
        }
        edit_respone = self.edit_goal(goal_id, data)
        self.assertEqual(edit_respone.status_code, status.HTTP_404_NOT_FOUND)

        # The original goal is unaltered
        self.client.force_login(self.user)
        edited_goal_response = self.get_goal(goal_id)
        self.assertEqual(edited_goal_response.status_code, status.HTTP_200_OK)
        self.assertEqual(edited_goal_response.data["text"], goal_obj["text"])
        self.assertEqual(
            edited_goal_response.data["amount"], goal_obj["amount"]
        )
        self.assertEqual(edited_goal_response.data["note"], goal_obj["note"])
        self.assertEqual(
            edited_goal_response.data["is_archived"], goal_obj["is_archived"]
        )

        if "errors" in list_reponse.data:
            self.fail(
                "Goal retrival response's body included at least one error!"
            )

    def test_can_not_delete_other_users_goals(self):
        self.client.force_login(self.user)
        _, _ = self.create_goal()

        goal_list_url = reverse("api:v1:goals")
        list_reponse = self.client.get(goal_list_url, format="json")
        self.assertEqual(list_reponse.status_code, status.HTTP_200_OK)
        self.assertEqual(list_reponse.data["count"], 1)
        self.assertEqual(len(list_reponse.data["results"]), 1)

        # Get the created goal's id (create API's response does not
        # contain it, yet)
        goal_id = list_reponse.data["results"][0]["id"]

        self.client.logout()

        # Another user CANNOT see that goal
        self.client.force_login(self.user2)

        delete_response = self.delete_goal(goal_id)
        self.assertEqual(
            delete_response.status_code, status.HTTP_404_NOT_FOUND
        )
        self.client.logout()

        self.client.force_login(self.user)
        second_list_reponse = self.client.get(goal_list_url, format="json")
        self.assertEqual(second_list_reponse.status_code, status.HTTP_200_OK)
        self.assertEqual(second_list_reponse.data["count"], 1)
        self.assertEqual(len(second_list_reponse.data["results"]), 1)

    def test_can_not_edit_goal_to_be_malformed(self):
        self.client.force_login(self.user)
        _, goal = self.create_goal()

        goal_list_url = reverse("api:v1:goals")
        list_reponse = self.client.get(goal_list_url, format="json")
        # Get the created goal's id (create API's response does not
        # contain it, yet)
        goal_id = list_reponse.data["results"][0]["id"]

        # Try with a too long text
        text = BaseTest.generate_string(21)
        amount = random.randint(1, 90000)
        note = BaseTest.generate_string(20)
        is_archived = bool(random.randint(0, 1))
        data = {
            "text": text,
            "amount": amount,
            "note": note,
            "is_archived": is_archived,
        }

        edit_response = self.edit_goal(goal_id, data)
        self.assertEqual(
            edit_response.status_code, status.HTTP_400_BAD_REQUEST
        )
        err_msg = "Ensure this field has no more than 20 characters."
        self.assertEqual(edit_response.data["text"][0], err_msg)
        self.assertEqual(edit_response.data["text"][0].code, "max_length")

        # Try with a too long note
        text = BaseTest.generate_string(20)
        amount = random.randint(1, 90000)
        note = BaseTest.generate_string(51)
        is_archived = bool(random.randint(0, 1))
        data = {
            "text": text,
            "amount": amount,
            "note": note,
            "is_archived": is_archived,
        }

        edit_response2 = self.edit_goal(goal_id, data)
        self.assertEqual(
            edit_response2.status_code, status.HTTP_400_BAD_REQUEST
        )
        err_msg = "Ensure this field has no more than 50 characters."
        self.assertEqual(edit_response2.data["note"][0], err_msg)
        self.assertEqual(edit_response2.data["note"][0].code, "max_length")

        # Try with a string in the amount field
        text = BaseTest.generate_string(20)
        amount = BaseTest.generate_string(20)
        note = BaseTest.generate_string(20)
        is_archived = random.choice([True, False])
        data = {
            "text": text,
            "amount": amount,
            "note": note,
            "is_archived": is_archived,
        }

        edit_response3 = self.edit_goal(goal_id, data)
        self.assertEqual(
            edit_response3.status_code, status.HTTP_400_BAD_REQUEST
        )
        err_msg = "A valid integer is required."
        self.assertEqual(edit_response3.data["amount"][0], err_msg)
        self.assertEqual(edit_response3.data["amount"][0].code, "invalid")

        # Renaming a goal to have the same name as another already existing
        # one will fail
        _, _ = self.create_goal()
        second_list_reponse = self.client.get(goal_list_url, format="json")
        # Get the created goal's id (create API's response does
        # not contain it, yet)
        second_goal_id = second_list_reponse.data["results"][1]["id"]

        data = {"text": goal["text"]}
        edit_second_goal = self.edit_goal(second_goal_id, data)
        self.assertEqual(
            edit_second_goal.status_code, status.HTTP_400_BAD_REQUEST
        )
        err_msg = "A Goal with the same text already exists for this user."
        self.assertEqual(edit_second_goal.data["text"][0], err_msg)
        self.assertEqual(edit_second_goal.data["text"][0].code, "invalid")

    def test_can_have_multiple_goals_with_the_same_note(self):
        self.client.force_login(self.user)

        # Create a goal
        response, goal = self.create_goal()

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            m.Goal.objects.get(created_by=self.user).note, goal["note"]  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        categoriy_list_url = reverse("api:v1:goals")
        response = self.client.get(categoriy_list_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["note"], goal["note"])
        self.assertEqual(response.data["count"], 1)

        create_url = reverse("api:v1:goals")
        text = BaseTest.generate_string(20)
        amount = random.randint(1, 90000)
        note = goal["note"]
        is_archived = bool(random.randint(0, 1))
        data = {
            "text": text,
            "amount": amount,
            "note": note,
            "is_archived": is_archived,
        }
        same_text_goal_response = self.client.post(
            create_url, data, format="json"
        )
        self.assertEqual(
            same_text_goal_response.status_code, status.HTTP_201_CREATED
        )

        categoriy_list_url = reverse("api:v1:goals")
        response = self.client.get(categoriy_list_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["note"], goal["note"])
        self.assertEqual(response.data["results"][1]["note"], goal["note"])
        self.assertEqual(response.data["count"], 2)

        # fmt: off
        self.assertEqual(
            m.Goal.objects.filter(note=goal["note"]).count(), 2  # pylint: disable=E1101; # noqa
        )
        # fmt: on

    def test_cant_see_special_goals(self):
        self.client.force_login(self.user)

        # check_creation_popup = True
        # check_successful_creation = True
        for i in range(4):
            _, _ = self.create_goal()

        goal_list_url = reverse("api:v1:goals")
        list_response = self.client.get(goal_list_url, format="json")
        # Get the created goal's id
        # (create API's response does not contain it, yet)
        goal_id = list_response.data["results"][0]["id"]
        goal2_id = list_response.data["results"][2]["id"]

        new_env = (
            f"{self.user.id},{goal_id},2050-11-01,2021-08-01,5200,5000,350,25;"
            f"{self.user.id},{goal2_id},2050-12-01,2021-09-01,-1,1000,350,20"
        )

        had_env = False
        # Store the original value, if there was any
        if os.getenv("SPECIAL_GOALS"):
            # Inject current user data in the SPECIAL_GOALS env
            original_env = os.getenv("SPECIAL_GOALS")
            had_env = True

        os.environ["SPECIAL_GOALS"] = new_env

        special_goals_url = reverse("api:v1:special_goals")
        special_goals = self.client.get(special_goals_url)
        json_res = special_goals.data

        self.assertEqual(json_res["count"], 0)
        self.assertEqual(len(json_res["results"]), 0)

        # Reset the environment variable after the test if there was any
        if had_env:
            os.environ["SPECIAL_GOALS"] = original_env
        else:
            # otherwise just clean it up
            os.environ.pop("SPECIAL_GOALS")

    # Can update the model with PUT requests
    @skip
    def test_users_can_entirely_edit_goals(self):
        """
        WRITE ME
        """
        pass

    # Update the model with PATCH requests
    # WRITE ME
    @skip
    def test_users_can_partially_edit_goals(self):
        """
        WRITE ME
        """
        pass

    # WRITE ME
    @skip
    def test_can_update_goals_without_changing_text_bug153_test(self):
        """
        WRITE ME
        """
        pass

    @skip
    def test_can_create_special_goals(self):
        # At the moment special goals can only be created by adding data inside
        # app/.env. See docs/env-explaination.txt
        pass

    @skip
    def test_can_not_create_malformed_special_goals(self):
        # At the moment special goals can only be created by adding data inside
        # app/.env. See docs/env-explaination.txt
        # missing fields or malformed json
        pass

    @skip
    def test_can_not_create_special_goals_with_same_name(self):
        # At the moment special goals can not be edited from the API
        pass

    @skip
    def test_can_not_see_other_users_special_goals(self):
        # At the moment special goals can not be edited from the API
        pass

    @skip
    def test_can_not_edit_other_users_special_goals(self):
        # At the moment special goals can not be edited from the API
        pass

    @skip
    def test_can_not_delete_other_users_special_goals(self):
        # At the moment special goals can not be deled from the API
        pass

    def test_can_filter_goals_by_name(self):
        self.client.force_login(self.user)
        _, goal = self.create_goal()

        goal_list_url = reverse("api:v1:goals")
        list_response = self.client.get(goal_list_url, format="json")
        self.assertEqual(list_response.status_code, status.HTTP_200_OK)
        self.assertEqual(list_response.data["count"], 1)
        self.assertEqual(len(list_response.data["results"]), 1)

        _, _ = self.create_goal()
        list_response = self.client.get(goal_list_url, format="json")
        self.assertEqual(list_response.status_code, status.HTTP_200_OK)
        self.assertEqual(list_response.data["count"], 2)
        self.assertEqual(len(list_response.data["results"]), 2)

        filtered_url = (
            reverse("api:v1:goals") + "?" + urlencode({"name": goal["text"]})
        )
        list_response = self.client.get(filtered_url, format="json")
        self.assertEqual(list_response.status_code, status.HTTP_200_OK)
        self.assertEqual(list_response.data["count"], 1)
        self.assertEqual(len(list_response.data["results"]), 1)
        self.assertEqual(
            list_response.data["results"][0]["text"], goal["text"]
        )

    @patch("api.v1.views.goal_views.GoalList")
    @patch("budgets.serializers.GoalSerializer")
    def test_integrity_errors_are_catched_on_create(
        self, mock_cat_serializer, _
    ):
        self.client.force_login(self.user)
        category_response, _ = self.create_goal()
        self.assertEqual(
            category_response.status_code, status.HTTP_201_CREATED
        )

        # Force the serializer to Throw an Integrity error
        serializer = mock_cat_serializer.return_value
        msg = "I AM A MOCKED INTEGRITY ERROR. I HOPE YOU CATCH ME: ROAAAARR!"
        serializer.save.side_effect = IntegrityError(msg)

        # Create a mock request
        ok_data = {
            "text": BaseTest.generate_string(20),
            "amount": random.randint(1, 90000),
            "note": BaseTest.generate_string(20),
            "is_archived": bool(random.randint(0, 1)),
        }

        # Create an instance of the view
        view = GoalList()
        request = self.client.post(reverse("api:v1:goals"), ok_data)

        # Set the request user
        view.request = request
        request.user = self.user

        # Confirm that the mocks works, and that ok_data actually thew
        # an exception and confirm it was correctly handled by the backend
        failed_res = view.perform_create(serializer)
        self.assertEqual(
            failed_res.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR
        )
        msg = (
            "Generic error thrown while creating the Goal. "
            "Refer the web container logs if you're the admin"
        )
        self.assertEqual(failed_res.data["non_field_errors"][0], msg)

    @skip  # This view does not yet catch integrity errors
    @patch("api.v1.views.goal_views.GoalDetail")
    @patch("budgets.serializers.GoalSerializer")
    def test_integrity_errors_are_catched_on_update(
        self, moc_exp_serializer, _
    ):
        self.client.force_login(self.user)
        category_response, _ = self.create_goal()
        self.assertEqual(
            category_response.status_code, status.HTTP_201_CREATED
        )

        goals_url = reverse("api:v1:goals")
        goal_r = self.client.get(goals_url, format="json")
        self.assertEqual(goal_r.data["count"], 1)
        goal_id = goal_r.data["results"][0]["id"]

        ok_data = {
            "text": BaseTest.generate_string(20),
            "amount": random.randint(1, 90000),
            "note": BaseTest.generate_string(20),
            "is_archived": bool(random.randint(0, 1)),
        }
        update_r = self.edit_goal(goal_id, ok_data)
        self.assertEqual(update_r.status_code, status.HTTP_200_OK)

        # Force the serializer to Throw an Integrity error
        serializer = moc_exp_serializer.return_value
        msg = "I AM A MOCKED INTEGRITY ERROR. I HOPE YOU CATCH ME: ROAAAARR!"
        serializer.save.side_effect = IntegrityError(msg)

        # Create a mock request
        ok_data = {
            "text": BaseTest.generate_string(20),
            "amount": random.randint(1, 90000),
            "note": BaseTest.generate_string(20),
            "is_archived": bool(random.randint(0, 1)),
        }

        # Create an instance of the class view
        view = GoalDetail()
        request = self.client.patch(
            reverse("api:v1:goal_detail", kwargs={"pk": goal_id}), ok_data
        )

        # Set the request user
        request.user = self.user
        # Mock the put request
        view.request = request
        view.request.method = "PACTH"

        # Confirm that the mocks works, and that ok_data actually thew
        # an exception and confirm it was correctly handled by the backend
        failed_res = view.perform_update(serializer)
        self.assertEqual(
            failed_res.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR
        )
        msg = (
            "Generic error thrown while updating the Expense. "
            "Refer the web container logs if you're the admin"
        )
        self.assertEqual(failed_res.data["non_field_errors"][0], msg)
        self.fail("copied")
