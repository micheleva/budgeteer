# Copyright: (c) 2021, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from datetime import date
from datetime import timedelta
import logging
import random
from unittest.mock import patch
from unittest import skip

from django.contrib.auth.models import User
from django.db.utils import IntegrityError

from django.urls import reverse
from rest_framework import status
from api.v1.views.income_category_view import IncomeCategoryList
from api.v1.views.income_views import IncomeDetail
from api.v1.tests.test_base import BaseApiTest

from budgets.models import IncomeCategory
from budgets.tests.base import BaseTest


logger = logging.getLogger(__name__)


class IncomeCategoryTest(BaseApiTest):
    """
    Test IncomeCategory model related v1 rest api behaviour
    """

    def setUp(self):  # pylint: disable=C0103; # noqa
        # Instrument the unit tests to fail if they run for too long
        super().setUp()
        self.user = User.objects.create_user(
            username=BaseTest.generate_string(20),
            password=BaseTest.generate_string(20),
        )
        self.user2 = User.objects.create_user(
            username=BaseTest.generate_string(20),
            password=BaseTest.generate_string(20),
        )

    def test_create_and_list_income_categories_and_other_user_can_not_see_them(
        self,
    ):
        """
        Ensure we can create a new income category object, and other users
        can't see it
        """
        self.client.force_login(self.user)

        # Create an income category
        category_response, _ = self.create_income_category()
        category_text = category_response.data["text"]

        self.assertEqual(
            category_response.status_code, status.HTTP_201_CREATED
        )
        # fmt: off
        self.assertEqual(
            IncomeCategory.objects.get(created_by=self.user).text,  # pylint: disable=E1101; # noqa
            category_text,
        )
        # fmt: on
        category_list_url = reverse("api:v1:income_categories")
        response = self.client.get(category_list_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["text"], category_text)
        self.assertEqual(response.data["count"], 1)

        # Another user CANNOT see that category
        self.client.force_login(self.user2)

        response = self.client.get(category_list_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 0)

    def test_can_not_create_income_categories_for_other_users(self):
        """
        Ensure we can NOT create a new income category object linked to
        another user by posting other users' ID in the payload
        """
        self.client.force_login(self.user)

        # Guido asked someone in the IT team to look up Frank's user ID.
        # Guido tries to create an Income category object and have it linked
        # to Frank's user ID as a prank
        self.client.force_login(self.user2)

        # Create an income category
        create_url = reverse("api:v1:income_categories")
        text = BaseTest.generate_string(40)
        # Guido grins while injecting Frank's user id
        data = {"text": text, "created_by": self.user.id}
        res = self.client.post(create_url, data, format="json")
        category_text = res.data["text"]

        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            IncomeCategory.objects.get(created_by=self.user2.id).text,  # pylint: disable=E1101; # noqa
            category_text,
        )
        # fmt: on
        categoriy_list_url = reverse("api:v1:income_categories")
        response = self.client.get(categoriy_list_url, format="json")

        # Guido is sad: the app does not seem vulnerable to said attack.
        # The income category was correctly linked to Guido's account
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["text"], category_text)
        self.assertEqual(response.data["count"], 1)

        # Frank logs in, and he finds no Income Categories, as he created none
        self.client.force_login(self.user)

        response = self.client.get(categoriy_list_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 0)

    def test_same_user_can_not_have_multiple_income_categories_with_the_same_name(
        self,
    ):
        self.client.force_login(self.user)

        # Create an income category
        category_response, _ = self.create_income_category()
        category_text = category_response.data["text"]

        self.assertEqual(
            category_response.status_code, status.HTTP_201_CREATED
        )
        # fmt: off
        self.assertEqual(
            IncomeCategory.objects.get(created_by=self.user).text,  # pylint: disable=E1101; # noqa
            category_text,
        )
        # fmt: on
        categoriy_list_url = reverse("api:v1:income_categories")
        response = self.client.get(categoriy_list_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["text"], category_text)
        self.assertEqual(response.data["count"], 1)

        # Create a category with the same text
        create_url = reverse("api:v1:income_categories")
        text = category_text
        data = {"text": text}
        create_duplicated_response = self.client.post(
            create_url, data, format="json"
        )

        self.assertEqual(
            create_duplicated_response.status_code, status.HTTP_400_BAD_REQUEST
        )
        self.assertEqual(
            create_duplicated_response.data["text"][0].code, "invalid"
        )
        err_msg = (
            "An Income category with this text already exists for this user."
        )
        self.assertEqual(
            str(create_duplicated_response.data["text"][0]), err_msg
        )

        response = self.client.get(categoriy_list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["text"], category_text)
        self.assertEqual(response.data["count"], 1)

    def test_different_users_can_have_multiple_categories_with_the_same_name(
        self,
    ):
        self.client.force_login(self.user)

        # Create an incomecategory
        category_response, _ = self.create_income_category()
        category_text = category_response.data["text"]

        self.assertEqual(
            category_response.status_code, status.HTTP_201_CREATED
        )
        # fmt: off
        self.assertEqual(
            IncomeCategory.objects.get(created_by=self.user).text,  # pylint: disable=E1101; # noqa
            category_text,
        )
        # fmt: on
        categoriy_list_url = reverse("api:v1:income_categories")
        response = self.client.get(categoriy_list_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["text"], category_text)
        self.assertEqual(response.data["count"], 1)

        # Another user can create an income category with the same name
        self.client.force_login(self.user2)

        create_url = reverse("api:v1:income_categories")
        data = {"text": category_text}
        res = self.client.post(create_url, data, format="json")

        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            IncomeCategory.objects.filter(created_by=self.user2)  # pylint: disable=E1101; # noqa
            .get()
            .text,
            category_text,
        )
        # fmt: on
        categoriy_list_url = reverse("api:v1:income_categories")
        response = self.client.get(categoriy_list_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["text"], category_text)
        self.assertEqual(response.data["count"], 1)
        # fmt: off
        self.assertEqual(
            IncomeCategory.objects.filter(text=category_text).count(), 2  # pylint: disable=E1101; # noqa
        )
        # fmt: on

    # Can update the model with PUT requests
    def test_can_entirely_edit_own_categories(self):
        self.client.force_login(self.user)

        # Create an incomecategory
        category_response, _ = self.create_income_category()
        category_id = category_response.data["id"]
        category_text = category_response.data["text"]

        self.assertEqual(
            category_response.status_code, status.HTTP_201_CREATED
        )
        # fmt: off
        self.assertEqual(
            IncomeCategory.objects.get(created_by=self.user).text,  # pylint: disable=E1101; # noqa
            category_text,
        )
        # fmt: ofn
        category_edit_url = reverse(
            "api:v1:income_category", kwargs={"pk": category_id}
        )
        response = self.client.get(category_edit_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["id"], category_id)
        self.assertEqual(response.data["text"], category_text)

        new_text = BaseTest.generate_string(40)
        data = {"text": new_text}
        _ = self.client.put(category_edit_url, data, format="json")

        category_edit_url = reverse(
            "api:v1:income_category", kwargs={"pk": category_id}
        )
        response = self.client.get(category_edit_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["id"], category_id)
        self.assertEqual(response.data["text"], new_text)

    def test_users_can_not_edit_other_users_categories(self):
        self.client.force_login(self.user)

        # Create an incomecategory
        category_response, _ = self.create_income_category()
        category_id = category_response.data["id"]
        category_text = category_response.data["text"]

        self.assertEqual(
            category_response.status_code, status.HTTP_201_CREATED
        )
        # fmt: off
        self.assertEqual(
            IncomeCategory.objects.get(created_by=self.user).text,  # pylint: disable=E1101; # noqa
            category_text,
        )
        # fmt: on
        category_edit_url = reverse(
            "api:v1:income_category", kwargs={"pk": category_id}
        )
        response = self.client.get(category_edit_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["id"], category_id)
        self.assertEqual(response.data["text"], category_text)

        # Another user can NOT edit said income category
        self.client.force_login(self.user2)

        new_text = BaseTest.generate_string(40)
        data = {"text": new_text}
        _ = self.client.patch(category_edit_url, data, format="json")

        category_edit_url = reverse(
            "api:v1:income_category", kwargs={"pk": category_id}
        )
        response = self.client.get(category_edit_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_users_can_not_edit_categories_to_create_duplicate_text_categories(
        self,
    ):
        self.client.force_login(self.user)

        # Create an income category
        category_response, _ = self.create_income_category()
        first_cat_id = category_response.data["id"]
        first_cat_text = category_response.data["text"]

        self.assertEqual(
            category_response.status_code, status.HTTP_201_CREATED
        )
        # fmt: off
        self.assertEqual(
            IncomeCategory.objects.get(created_by=self.user).text,  # pylint: disable=E1101; # noqa
            first_cat_text,
        )
        # fmt: on
        categoriy_list_url = reverse("api:v1:income_categories")
        response = self.client.get(categoriy_list_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["text"], first_cat_text)
        self.assertEqual(response.data["count"], 1)

        second_cat_res, _ = self.create_income_category()
        second_cat_text = second_cat_res.data["text"]

        # Updating the first category, to have its text field matching
        # the second category's will fail
        category_edit_url = reverse(
            "api:v1:income_category", kwargs={"pk": first_cat_id}
        )
        data = {"text": second_cat_text}
        category_update_response = self.client.patch(
            category_edit_url, data, format="json"
        )
        self.assertEqual(
            category_update_response.status_code, status.HTTP_400_BAD_REQUEST
        )
        err_msg = (
            "An Income category with this text already exists for this user."
        )
        self.assertEqual(category_update_response.data["text"][0], err_msg)
        self.assertEqual(
            category_update_response.data["text"][0].code, "invalid"
        )

    @patch("api.v1.views.income_category_view.IncomeCategoryList")
    @patch("budgets.serializers.IncomeCategorySerializer")
    def test_integrity_errors_are_catched_on_create(
        self, mock_cat_serializer, _
    ):
        self.client.force_login(self.user)
        cat_r, _ = self.create_income_category()
        self.assertEqual(cat_r.status_code, status.HTTP_201_CREATED)

        # Force the serializer to Throw an Integrity error
        serializer = mock_cat_serializer.return_value
        msg = "I AM A MOCKED INTEGRITY ERROR. I HOPE YOU CATCH ME: ROAAAARR!"
        serializer.save.side_effect = IntegrityError(msg)

        # Create a mock request
        ok_data = {"text": BaseTest.generate_string(20)}

        # Create an instance of the class view
        view = IncomeCategoryList()
        request = self.client.post(
            reverse("api:v1:income_categories"), ok_data
        )

        # Set the request user
        view.request = request
        request.user = self.user

        # Confirm that the mocks works, and that ok_data actually thew
        # an exception and confirm it was correctly handled by the backend
        failed_res = view.perform_create(serializer)
        self.assertEqual(
            failed_res.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR
        )
        msg = (
            "Generic error thrown while creating the Income Category. "
            "Refer the web container logs if you're the admin"
        )
        self.assertEqual(failed_res.data["non_field_errors"][0], msg)

    # This view is not handling the integration errors correctly:
    #  skip until then
    @skip
    @patch("api.v1.views.income_category_view.IncomeCategoryList")
    @patch("budgets.serializers.IncomeCategorySerializer")
    def test_integrity_errors_are_catched_on_update(
        self, mock_cat_serializer, _
    ):
        self.client.force_login(self.user)
        cat_r, _ = self.create_income_category()
        self.assertEqual(cat_r.status_code, status.HTTP_201_CREATED)

        inc_r, inc_data = self.create_income(cat_r.data["id"])
        self.assertEqual(inc_r.status_code, status.HTTP_201_CREATED)
        self.assertEqual(inc_r.data["category"], cat_r.data["id"])
        self.assertEqual(inc_r.data["note"], inc_data["note"])
        self.assertEqual(inc_r.data["date"], inc_data["date"])
        # fmt: off
        self.assertEqual(
            Income.objects.first(  # pylint: disable=E1101; # noqa
                created_by=self.user
            ).created_by,
            self.user,
        )
        # fmt: on
        expenses_list_url = reverse("api:v1:incomes")
        res = self.client.get(expenses_list_url, format="json")
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data["count"], 1)
        income_id = res.data["results"][0]["id"]

        amount = random.randint(1, 90000)
        note = BaseTest.generate_string(20)
        record_date = (
            date.today() - timedelta(days=random.randint(42, 365))
        ).isoformat()
        cat_r2, _ = self.create_income_category()

        # Confirm update works properly before mocking it
        data = {
            "category": cat_r2.data["id"],
            "amount": amount,
            "note": note,
            "date": record_date,
        }
        update_response, _ = self.replace_all_income_fields(income_id, data)
        self.assertEqual(update_response.status_code, status.HTTP_200_OK)

        incomes_list = reverse("api:v1:incomes")
        second_response = self.client.get(incomes_list, format="json")
        # Assure that since we did an update, and nothing was created
        self.assertEqual(second_response.data["count"], 1)
        self.assertEqual(second_response.status_code, status.HTTP_200_OK)
        self.assertEqual(second_response.data["results"][0]["amount"], amount)
        self.assertEqual(second_response.data["results"][0]["note"], note)
        self.assertEqual(
            second_response.data["results"][0]["date"], record_date
        )

        # Create a mock request
        amount2 = random.randint(1, 90000)
        note2 = BaseTest.generate_string(20)
        record_date2 = (
            date.today() - timedelta(days=random.randint(42, 365))
        ).isoformat()
        cat_r3, _ = self.create_income_category()
        ok_data = {
            "category": cat_r3.data["id"],
            "amount": amount2,
            "note": note2,
            "date": record_date2,
        }

        serializer = mock_cat_serializer.return_value
        msg = "I AM A MOCKED INTEGRITY ERROR. I HOPE YOU CATCH ME: ROAAAARR!"
        serializer.save.side_effect = IntegrityError(msg)

        # Create an instance of the class view
        view = IncomeDetail()
        update_url = reverse("api:v1:incomes", kwargs={"pk": income_id})
        request = self.client.post(update_url, ok_data)

        # Set the request user
        view.request = request
        request.user = self.user

        incomes_list = reverse("api:v1:incomes")
        second_response = self.client.get(incomes_list, format="json")
        # Assure that since we did an update, and nothing was created
        self.assertEqual(second_response.data["count"], 1)
        self.assertEqual(second_response.status_code, status.HTTP_200_OK)
        self.assertEqual(second_response.data["results"][0]["amount"], amount)
        self.assertEqual(second_response.data["results"][0]["note"], note)
        self.assertEqual(
            second_response.data["results"][0]["date"], record_date
        )

        # Confirm that the mocks works, and that ok_data actually thew
        # an exception and confirm it was correctly handled by the backend
        res = view.perform_update(serializer)
        self.assertEqual(
            res.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR
        )
        msg = (
            "Generic error thrown while editing the MonthlyBalance. "
            "Refer the web container logs if you're the admin"
        )
        self.assertEqual(res.data["non_field_errors"][0], msg)
        self.fail("copied from another test")
