# Copyright: (c) 2021, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from decimal import Decimal
import logging
import random
from unittest.mock import patch

from urllib.parse import urlencode

from django.contrib.auth.models import User
from django.db.utils import IntegrityError
from django.urls import reverse
from rest_framework import status

from api.v1.views.budget_views import BudgetDetail
from api.v1.views.budget_views import BudgetList
from api.v1.tests.test_base import BaseApiTest

import budgets.models as m


logger = logging.getLogger(__name__)


class BudgetTest(BaseApiTest):
    def setUp(self):
        # Instrument the unit tests to fail if they run for too long
        super().setUp()
        # fmt: off
        self.user = User.objects.create_user(  # pylint: disable=E1101; # noqa
            username="testuser", password="testpassword"
        )
        self.category = m.Category.objects.create(  # pylint: disable=E1101; # noqa
            text="Test Category", created_by=self.user
        )
        self.savingcategory = m.SavingCategory.objects.create(  # pylint: disable=E1101; # noqa
            text="Monthly Investing",
            created_by=self.user,
        )

        self.user2 = User.objects.create_user(  # pylint: disable=E1101; # noqa
            username="otheruser", password="otherpassword"
        )
        self.category2 = m.Category.objects.create(  # pylint: disable=E1101; # noqa
            text="Test Category", created_by=self.user2
        )
        self.savingcategory2 = m.SavingCategory.objects.create(  # pylint: disable=E1101; # noqa
            text="Monthly Saving",
            created_by=self.user2,
        )
        # fmt: on

    def test_can_create_and_list_budget_with_category(self):
        self.client.force_login(self.user)

        res, data = self.create_budget(
            category_id=self.category.id,
            savingcategory_id=None,
            month=random.randint(1, 12),
            data=None,
        )
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        url = reverse("api:v1:budgets")
        budgets_r = self.client.get(url, format="json")
        self.assertEqual(budgets_r.data["count"], 1)
        self.assertEqual(len(budgets_r.data["results"]), 1)
        self.assertEqual(
            budgets_r.data["results"][0]["category"]["id"], self.category.id
        )
        self.assertEqual(budgets_r.data["results"][0]["year"], data["year"])
        self.assertEqual(budgets_r.data["results"][0]["month"], data["month"])
        # FIXME: Returning Decimals as strings to the frontend is not a great
        # idea, typscript might not be able to handle it
        self.assertEqual(
            float(budgets_r.data["results"][0]["amount"]), data["amount"]
        )

    def test_can_create_and_list_with_savingcategory(self):
        self.client.force_login(self.user)

        res, data = self.create_budget(
            category_id=None,
            savingcategory_id=self.savingcategory.id,
            month=random.randint(1, 12),
            data=None,
        )
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        url = reverse("api:v1:budgets")
        res = self.client.get(url, format="json")
        self.assertEqual(res.data["count"], 1)
        self.assertEqual(len(res.data["results"]), 1)
        self.assertEqual(
            res.data["results"][0]["savingcategory"]["id"],
            self.savingcategory.id,
        )
        self.assertEqual(res.data["results"][0]["year"], data["year"])
        self.assertEqual(res.data["results"][0]["month"], data["month"])
        # FIXME: Returning Decimals as strings to the frontend is not a
        # great idea, typscript might not be able to handle it
        self.assertEqual(
            float(res.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            float(res.data["results"][0]["allocated_amount"]),
            data["allocated_amount"],
        )

    def test_can_filter_budgets_with_savingcategories_by_date(self):
        self.client.force_login(self.user)

        # Can filter Budgets, with a savingcategory, by year AND month
        data = {
            "savingcategory": self.savingcategory.id,
            "year": 2023,
            "month": 1,
            "amount": 500.0,
            "allocated_amount": 0.0,
        }
        url = reverse("api:v1:budgets")
        response = self.client.post(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        data2 = {
            "savingcategory": self.savingcategory.id,
            "year": 2023,
            "month": 2,
            "amount": 501.0,
            "allocated_amount": 0.0,
        }
        url = reverse("api:v1:budgets")
        response = self.client.post(url, data2, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        params = urlencode({"year": 2023, "month": 1})
        filtered_url = url + "?" + params

        budgets_r = self.client.get(filtered_url, format="json")
        self.assertEqual(budgets_r.data["count"], 1)
        self.assertEqual(len(budgets_r.data["results"]), 1)
        self.assertEqual(
            budgets_r.data["results"][0]["savingcategory"]["id"],
            self.savingcategory.id,
        )
        self.assertEqual(budgets_r.data["results"][0]["year"], 2023)
        self.assertEqual(budgets_r.data["results"][0]["month"], 1)
        self.assertEqual(budgets_r.data["results"][0]["amount"], "500.000")

        # The API should returns empty if nothing matches said conditions
        params = urlencode({"year": 2023, "month": 3})
        filtered_url = url + "?" + params

        budgets_r = self.client.get(filtered_url, format="json")
        self.assertEqual(budgets_r.data["count"], 0)
        self.assertEqual(len(budgets_r.data["results"]), 0)

        # Can filter Budgets, with a savingcategory, by year only
        data = {
            "savingcategory": self.savingcategory.id,
            "year": 2023,
            "amount": 502.0,
            "allocated_amount": 0.0,
        }
        url = reverse("api:v1:budgets")
        response = self.client.post(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        data2 = {
            "savingcategory": self.savingcategory.id,
            "year": 2024,
            "amount": 503.0,
            "allocated_amount": 0.0,
        }
        url = reverse("api:v1:budgets")
        response = self.client.post(url, data2, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        params = urlencode({"year": 2024})
        filtered_url = url + "?" + params

        budgets_r = self.client.get(filtered_url, format="json")
        self.assertEqual(budgets_r.data["count"], 1)
        self.assertEqual(len(budgets_r.data["results"]), 1)
        self.assertEqual(
            budgets_r.data["results"][0]["savingcategory"]["id"],
            self.savingcategory.id,
        )
        self.assertEqual(budgets_r.data["results"][0]["year"], 2024)
        self.assertEqual(budgets_r.data["results"][0]["month"], None)
        self.assertEqual(budgets_r.data["results"][0]["amount"], "503.000")

        # The API should returns empty if nothing matches said conditions
        params = urlencode({"year": 2025})
        filtered_url = url + "?" + params

        budgets_r = self.client.get(filtered_url, format="json")
        self.assertEqual(budgets_r.data["count"], 0)
        self.assertEqual(len(budgets_r.data["results"]), 0)

    def test_can_filter_budgets_with_categories_by_date(self):
        self.client.force_login(self.user)

        # Can filter Budgets, with a Category, by year AND month
        data = {
            "category": self.category.id,
            "year": 2023,
            "month": 1,
            "amount": 500.0,
        }
        url = reverse("api:v1:budgets")
        response = self.client.post(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        data2 = {
            "category": self.category.id,
            "year": 2023,
            "month": 2,
            "amount": 501.0,
        }
        url = reverse("api:v1:budgets")
        response = self.client.post(url, data2, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        params = urlencode({"year": 2023, "month": 1})
        filtered_url = url + "?" + params

        budgets_r = self.client.get(filtered_url, format="json")
        self.assertEqual(budgets_r.data["count"], 1)
        self.assertEqual(len(budgets_r.data["results"]), 1)
        self.assertEqual(
            budgets_r.data["results"][0]["category"]["id"], self.category.id
        )
        self.assertEqual(budgets_r.data["results"][0]["year"], 2023)
        self.assertEqual(budgets_r.data["results"][0]["month"], 1)
        self.assertEqual(budgets_r.data["results"][0]["amount"], "500.000")

        # The API should returns empty if nothing matches said conditions
        params = urlencode({"year": 2023, "month": 3})
        filtered_url = url + "?" + params

        budgets_r = self.client.get(filtered_url, format="json")
        self.assertEqual(budgets_r.data["count"], 0)
        self.assertEqual(len(budgets_r.data["results"]), 0)

        # Can filter Budgets, with a Category, by year only
        data = {
            "category": self.category.id,
            "year": 2023,
            "amount": 502.0,
        }
        url = reverse("api:v1:budgets")
        response = self.client.post(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        data2 = {
            "category": self.category.id,
            "year": 2024,
            "amount": 503.0,
        }
        url = reverse("api:v1:budgets")
        response = self.client.post(url, data2, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        params = urlencode({"year": 2024})
        filtered_url = url + "?" + params

        budgets_r = self.client.get(filtered_url, format="json")
        self.assertEqual(budgets_r.data["count"], 1)
        self.assertEqual(len(budgets_r.data["results"]), 1)
        self.assertEqual(
            budgets_r.data["results"][0]["category"]["id"], self.category.id
        )
        self.assertEqual(budgets_r.data["results"][0]["year"], 2024)
        self.assertEqual(budgets_r.data["results"][0]["month"], None)
        self.assertEqual(budgets_r.data["results"][0]["amount"], "503.000")

        # The API should returns empty if nothing matches said conditions
        params = urlencode({"year": 2025})
        filtered_url = url + "?" + params

        budgets_r = self.client.get(filtered_url, format="json")
        self.assertEqual(budgets_r.data["count"], 0)
        self.assertEqual(len(budgets_r.data["results"]), 0)

    def test_filter_budgets_with_categories_passing_month_but_not_year_will_not_filter_anything(
        self,
    ):
        self.client.force_login(self.user)

        data = {
            "category": self.category.id,
            "year": 2023,
            "month": 1,
            "amount": 500.0,
        }
        url = reverse("api:v1:budgets")
        response = self.client.post(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        data2 = {
            "category": self.category.id,
            "year": 2023,
            "month": 2,
            "amount": 501.0,
        }
        url = reverse("api:v1:budgets")
        response = self.client.post(url, data2, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Passing only month, but no year param will make the filter invalid, and thus ignored
        params = urlencode({"month": 2})
        filtered_url = url + "?" + params

        # The filtering is ignored, and all results are returned
        budgets_r = self.client.get(filtered_url, format="json")
        self.assertEqual(budgets_r.data["count"], 2)
        self.assertEqual(len(budgets_r.data["results"]), 2)

    def test_filter_budgets_with_savingcategories_passing_month_but_not_year_will_not_filter_anything(
        self,
    ):
        self.client.force_login(self.user)

        # Can filter Budgets, with a savingcategory, by year AND month
        data = {
            "savingcategory": self.savingcategory.id,
            "year": 2023,
            "month": 1,
            "amount": 500.0,
            "allocated_amount": 0.0,
        }
        url = reverse("api:v1:budgets")
        response = self.client.post(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        data2 = {
            "savingcategory": self.savingcategory.id,
            "year": 2023,
            "month": 2,
            "amount": 501.0,
            "allocated_amount": 0.0,
        }
        url = reverse("api:v1:budgets")
        response = self.client.post(url, data2, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Passing only month, but no year param will make the filter invalid, and thus ignored
        params = urlencode({"month": 2})
        filtered_url = url + "?" + params

        budgets_r = self.client.get(filtered_url, format="json")
        self.assertEqual(budgets_r.data["count"], 2)
        self.assertEqual(len(budgets_r.data["results"]), 2)

    def test_malformed_filters_will_not_throw_errors_but_return_no_items(self):
        self.client.force_login(self.user)

        res, data = self.create_budget(
            category_id=self.category.id,
            savingcategory_id=None,
            month=1,
            data=None,
        )
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        res, data = self.create_budget(
            category_id=self.category.id,
            savingcategory_id=None,
            month=2,
            data=None,
        )
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        # Without filters the list API will return 2 items
        url = reverse("api:v1:budgets")
        budgets_r = self.client.get(url, format="json")
        self.assertEqual(budgets_r.data["count"], 2)
        self.assertEqual(len(budgets_r.data["results"]), 2)

        # Try with strings
        params = urlencode({"year": "not a string", "month": "lol"})
        filtered_url = url + "?" + params

        # The filtering is ignored, and all results are returned
        budgets_r = self.client.get(filtered_url, format="json")
        self.assertEqual(budgets_r.data["count"], 2)
        self.assertEqual(len(budgets_r.data["results"]), 2)

        # Try with invalid integers
        params = urlencode({"year": 1969, "month": 15})
        filtered_url = url + "?" + params

        # The filtering is applied, and nothing matches
        budgets_r = self.client.get(filtered_url, format="json")
        self.assertEqual(budgets_r.data["count"], 0)
        self.assertEqual(len(budgets_r.data["results"]), 0)

    def test_different_users_can_have_the_same_month_year_savingcategory_budgets(
        self,
    ):
        self.client.force_login(self.user)

        data = {
            "savingcategory": self.savingcategory.id,
            "year": 2023,
            "month": 1,
            "amount": 500.0,
            "allocated_amount": 0.0,
        }
        url = reverse("api:v1:budgets")
        response = self.client.post(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            m.Budget.objects.filter(created_by=self.user.id).count(), 1  # pylint: disable=E1101; # noqa
        )
        self.assertEqual(
            m.Budget.objects.filter(created_by=self.user.id)  # pylint: disable=E1101; # noqa
            .first()
            .savingcategory,
            self.savingcategory,
        )
        # fmt: on
        budgets_r = self.client.get(url, format="json")
        self.assertEqual(budgets_r.data["count"], 1)
        self.assertEqual(len(budgets_r.data["results"]), 1)
        self.assertEqual(
            budgets_r.data["results"][0]["savingcategory"]["id"],
            self.savingcategory.id,
        )
        self.assertEqual(budgets_r.data["results"][0]["year"], 2023)
        self.assertEqual(budgets_r.data["results"][0]["month"], 1)
        self.assertEqual(budgets_r.data["results"][0]["amount"], "500.000")

        self.client.force_login(self.user2)
        # fmt: off
        same_text_aside_cat = m.SavingCategory.objects.create(  # pylint: disable=E1101; # noqa
            text=self.savingcategory.text,
            created_by=self.user2,
        )
        # fmt: on
        data = {
            "savingcategory": same_text_aside_cat.id,
            "year": 2023,
            "month": 1,
            "amount": 500.0,
            "allocated_amount": 0.0,
        }
        url = reverse("api:v1:budgets")
        response = self.client.post(url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            m.Budget.objects.filter(created_by=self.user2.id).count(), 1  # pylint: disable=E1101; # noqa
        )
        self.assertEqual(
            m.Budget.objects.filter(created_by=self.user2.id)  # pylint: disable=E1101; # noqa
            .first()
            .savingcategory,
            same_text_aside_cat,
        )
        # fmt: on
        budgets_r = self.client.get(url, format="json")
        self.assertEqual(budgets_r.data["count"], 1)
        self.assertEqual(len(budgets_r.data["results"]), 1)
        self.assertEqual(
            budgets_r.data["results"][0]["savingcategory"]["id"],
            same_text_aside_cat.id,
        )
        self.assertEqual(budgets_r.data["results"][0]["year"], 2023)
        self.assertEqual(budgets_r.data["results"][0]["month"], 1)
        self.assertEqual(budgets_r.data["results"][0]["amount"], "500.000")

    def test_can_not_create_budget_invalid_category(self):
        self.client.force_login(self.user)

        data = {
            "category": 999,  # Non-existent category ID
            "year": 2023,
            "month": 1,
            "amount": 500.0,
            "allocated_amount": 0.0,
        }

        url = reverse("api:v1:budgets")
        res = self.client.post(url, data, format="json")

        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        err_msg = f'Invalid pk "999" - object does not exist.'
        self.assertEqual(res.data["category"][0], err_msg)
        self.assertEqual(res.data["category"][0].code, "does_not_exist")
        # fmt: off
        self.assertEqual(
            m.Budget.objects.filter(created_by=self.user.id).count(), 0  # pylint: disable=E1101; # noqa
        )
        # fmt: on

    def test_can_not_create_budget_invalid_savingcategory(self):
        self.client.force_login(self.user)

        # Perfectly valid data
        original_data = {
            "savingcategory": self.savingcategory.id,
            "year": 2023,
            "month": 1,
            "amount": 500.0,
            "allocated_amount": 0.0,
            "text": "My saving category",
            "is_archived": True,
        }

        res = self.create_budget_from_data(original_data)
        # Confirm the original data can create an entity
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        # Update the text, to avoid error related to already existing saving
        # category with this text
        malformed_data_without_savingcat = self.modify_data(
            original_data, "text", modification_value="My second cat"
        )
        # Try without savingcategory
        malformed_data_without_savingcat = self.modify_data(
            malformed_data_without_savingcat,
            "savingcategory",
            remove_field=True,
        )
        res = self.create_budget_from_data(malformed_data_without_savingcat)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        err = "Either a Category or a Saving Category must be provided."
        self.assertEqual(res.data["non_field_errors"][0], err)
        self.assertEqual(res.data["non_field_errors"][0].code, "invalid")

        # Try with None savingcategory
        malformed_account_none = self.modify_data(
            original_data, "savingcategory", modification_value=None
        )
        res = self.create_budget_from_data(malformed_account_none)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        err = "Either a Category or a Saving Category must be provided."
        self.assertEqual(res.data["non_field_errors"][0], err)
        self.assertEqual(res.data["non_field_errors"][0].code, "invalid")

        # Try with non-existing savingcategory
        malformed_account_not_existing = self.modify_data(
            original_data, "savingcategory", modification_value=999
        )
        res = self.create_budget_from_data(malformed_account_not_existing)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        err_msg = f'Invalid pk "999" - object does not exist.'
        self.assertEqual(res.data["savingcategory"][0], err_msg)
        self.assertEqual(res.data["savingcategory"][0].code, "does_not_exist")
        # fmt: off
        # Confirm we only have the one created with the original_data
        self.assertEqual(
            m.Budget.objects.filter(created_by=self.user.id).count(), 1  # pylint: disable=E1101; # noqa
        )
        # fmt: on

        # Try without 'amount'
        malformed_data_without_amount = self.modify_data(
            original_data, "amount", remove_field=True
        )
        res = self.create_budget_from_data(malformed_data_without_amount)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(res.data["amount"][0], "This field is required.")
        self.assertEqual(res.data["amount"][0].code, "required")

        # Try with 'amount' as text
        malformed_data_with_amount_as_text = self.modify_data(
            original_data, "amount", modification_value="text"
        )
        res = self.create_budget_from_data(malformed_data_with_amount_as_text)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(res.data["amount"][0], "A valid number is required.")
        self.assertEqual(res.data["amount"][0].code, "invalid")

        # Try with 'amount' as None
        malformed_data_with_amount_as_none = self.modify_data(
            original_data, "amount", modification_value=None
        )
        res = self.create_budget_from_data(malformed_data_with_amount_as_none)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(res.data["amount"][0], "This field may not be null.")
        self.assertEqual(res.data["amount"][0].code, "null")

        # Try with 'amount' as a negative number
        malformed_data_with_amount_as_negative = self.modify_data(
            original_data, "amount", modification_value=-100
        )
        res = self.create_budget_from_data(
            malformed_data_with_amount_as_negative
        )
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            str(res.data["amount"][0]), "-100.000 should be more than 0."
        )
        self.assertEqual(res.data["amount"][0].code, "invalid")

        # Update the year, to avoid error related to already existing saving
        # category with this year/year-month
        malformed_data_without_allocated_amount = self.modify_data(
            original_data, "year", modification_value=2024
        )
        # Try without 'allocated_amount'
        malformed_data_without_allocated_amount = self.modify_data(
            malformed_data_without_allocated_amount,
            "allocated_amount",
            remove_field=True,
        )
        res = self.create_budget_from_data(
            malformed_data_without_allocated_amount
        )
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        err = "Saving Budgets requires a 0 or greater allocated amount value."
        self.assertEqual(str(res.data["non_field_errors"][0]), err)
        # Fix this invalid, it should be required
        self.assertEqual(res.data["non_field_errors"][0].code, "invalid")

        # Try with 'allocated_amount' as text
        malformed_data_with_allocated_amount_as_text = self.modify_data(
            original_data, "allocated_amount", modification_value="text"
        )
        res = self.create_budget_from_data(
            malformed_data_with_allocated_amount_as_text
        )
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        err = "A valid number is required."
        self.assertEqual(res.data["allocated_amount"][0], err)
        self.assertEqual(res.data["allocated_amount"][0].code, "invalid")

        # Try with 'allocated_amount' as None
        malformed_data_with_allocated_amount_as_none = self.modify_data(
            original_data, "allocated_amount", modification_value=None
        )
        res = self.create_budget_from_data(
            malformed_data_with_allocated_amount_as_none
        )
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        err = "Saving Budgets requires a 0 or greater allocated amount value."
        self.assertEqual(res.data["non_field_errors"][0], err)
        self.assertEqual(res.data["non_field_errors"][0].code, "invalid")

        # Try with 'allocated_amount' as a negative number
        malformed_data_with_allocated_amount_as_negative = self.modify_data(
            original_data, "allocated_amount", modification_value=-100
        )
        res = self.create_budget_from_data(
            malformed_data_with_allocated_amount_as_negative
        )
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        err = "-100.000 should be more than 0."
        self.assertEqual(str(res.data["allocated_amount"][0]), err)
        self.assertEqual(res.data["allocated_amount"][0].code, "invalid")

        # Try with negative year
        malformed_data_with_year_as_negative = self.modify_data(
            original_data, "year", modification_value=-100
        )
        res = self.create_budget_from_data(
            malformed_data_with_year_as_negative
        )
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        err = "Year must be an integer equal or more than 1970."
        self.assertEqual(str(res.data["year"][0]), err)
        self.assertEqual(res.data["year"][0].code, "invalid")

        # Try with year as string
        malformed_data_with_year_as_string = self.modify_data(
            original_data, "year", modification_value="not an integer"
        )
        res = self.create_budget_from_data(malformed_data_with_year_as_string)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        err = "A valid integer is required."
        self.assertEqual(str(res.data["year"][0]), err)
        self.assertEqual(res.data["year"][0].code, "invalid")

        # Try with year as None
        malformed_data_with_year_as_none = self.modify_data(
            original_data, "year", modification_value=None
        )
        res = self.create_budget_from_data(malformed_data_with_year_as_none)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        err = "This field may not be null."
        self.assertEqual(str(res.data["year"][0]), err)
        self.assertEqual(res.data["year"][0].code, "null")

        # Try with no year
        malformed_data_with_year_as_none = self.modify_data(
            original_data,
            "year",
            remove_field=True,
        )
        res = self.create_budget_from_data(malformed_data_with_year_as_none)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        err = "This field is required."
        self.assertEqual(str(res.data["year"][0]), err)
        self.assertEqual(res.data["year"][0].code, "required")

        # Try with negative month
        malformed_data_with_month_as_negative = self.modify_data(
            original_data, "month", modification_value=-100
        )
        res = self.create_budget_from_data(
            malformed_data_with_month_as_negative
        )
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        err = "Month must be between 1 and 12 (inclusive)."
        self.assertEqual(str(res.data["month"][0]), err)
        self.assertEqual(res.data["month"][0].code, "invalid")

        # Try with an invalid  month number
        malformed_data_with_month_as_negative = self.modify_data(
            original_data, "month", modification_value=13
        )
        res = self.create_budget_from_data(
            malformed_data_with_month_as_negative
        )
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        err = "Month must be between 1 and 12 (inclusive)."
        self.assertEqual(str(res.data["month"][0]), err)
        self.assertEqual(res.data["month"][0].code, "invalid")

        # Try with month as string
        malformed_data_with_month_as_string = self.modify_data(
            original_data, "month", modification_value="not an integer"
        )
        res = self.create_budget_from_data(malformed_data_with_month_as_string)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        err = "A valid integer is required."
        self.assertEqual(str(res.data["month"][0]), err)
        self.assertEqual(res.data["month"][0].code, "invalid")

    def test_creation_fails_with_both_existing_category_and_existing_savingcategory(
        self,
    ):
        self.client.force_login(self.user)

        url = reverse("api:v1:budgets")
        data = {
            "category": self.category.id,
            "savingcategory": self.savingcategory.id,
            "year": 2023,
            "month": 1,
            "amount": 100,
        }
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        err_msg = (
            "Cannot specify both category and savingcategory in"
            " the same budget."
        )
        self.assertEqual(res.data["non_field_errors"][0], err_msg)
        self.assertEqual(res.data["non_field_errors"][0].code, "invalid")
        # fmt: off
        self.assertEqual(
            m.Budget.objects.filter(created_by=self.user.id).count(), 0  # pylint: disable=E1101; # noqa
        )
        # fmt: on

    def test_creation_fails_with_other_user_category(self):
        self.client.force_login(self.user)
        # fmt: off
        category_user2 = m.Category.objects.create(  # pylint: disable=E1101; # noqa
            text="CategoryUser2", created_by=self.user2
        )
        # fmt: on
        url = reverse("api:v1:budgets")
        data = {
            "category": category_user2.id,
            "year": 2023,
            "month": 1,
            "amount": 100,
        }
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        err_msg = f'Invalid pk "{category_user2.id}" - object does not exist.'
        self.assertEqual(res.data["non_field_errors"][0], err_msg)
        self.assertEqual(res.data["non_field_errors"][0].code, "invalid")
        # fmt: off
        self.assertEqual(
            m.Budget.objects.filter(created_by=self.user.id).count(), 0  # pylint: disable=E1101; # noqa
        )
        # fmt: on

    def test_creation_fails_with_other_user_savingcategory(self):
        self.client.force_login(self.user)

        url = reverse("api:v1:budgets")
        data = {
            "savingcategory": self.savingcategory2.id,
            "year": 2023,
            "month": 1,
            "amount": 100,
            "allocated_amount": 1,
        }
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        err_msg = (
            f'Invalid pk "{self.savingcategory2.id}"'
            " - object does not exist."
        )
        self.assertEqual(res.data["non_field_errors"][0], err_msg)
        self.assertEqual(res.data["non_field_errors"][0].code, "invalid")
        # fmt: off
        self.assertEqual(
            m.Budget.objects.filter(created_by=self.user.id).count(), 0  # pylint: disable=E1101; # noqa
        )
        # fmt: on

    def test_creation_with_both_category_and_allocated_amount_provided_fails(
        self,
    ):
        self.client.force_login(self.user)

        data = {
            "category": self.category.id,
            "year": 2023,
            "month": 12,
            "amount": 100.0,
            "allocated_amount": 50.0,
            "created_by": self.user.id,
        }
        url = reverse("api:v1:budgets")
        res = self.client.post(url, data)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn("non_field_errors", res.data)
        msg = (
            "Cannot specify both category and allocated_amount in the"
            " same budget."
        )
        self.assertEqual(str(res.data["non_field_errors"][0]), msg)
        self.assertEqual(res.data["non_field_errors"][0].code, "invalid")

    def test_malformatted_spending_budget_data_creation_results_in_error(self):
        self.client.force_login(self.user)

        url = reverse("api:v1:budgets")
        original_data = {
            "category": self.category.id,
            "savingcategory": self.savingcategory.id,
            "year": random.randint(1970, 2050),
            "month": random.randint(1, 12),
            "amount": round(random.uniform(1, 500), 2),
            "allocated_amount": 0.0,
        }

        # Try with Category and month as a negative integer
        malformed_data = self.modify_data(
            original_data, "savingcategory", remove_field=True
        )
        malformed_data = self.modify_data(
            malformed_data, "allocated_amount", remove_field=True
        )
        malformed_data = self.modify_data(
            malformed_data, "month", modification_value=-1
        )
        res = self.client.post(url, malformed_data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = "Month must be between 1 and 12 (inclusive)."
        self.assertEqual(str(res.data["month"][0]), msg)
        self.assertEqual(res.data["month"][0].code, "invalid")

        # Try with Category and amount as a string
        malformed_data = self.modify_data(
            original_data, "savingcategory", remove_field=True
        )
        malformed_data = self.modify_data(
            malformed_data, "allocated_amount", remove_field=True
        )
        malformed_data = self.modify_data(
            malformed_data, "amount", modification_value="I am not a number"
        )
        res = self.client.post(url, malformed_data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = "A valid number is required."
        self.assertEqual(res.data["amount"][0], msg)
        self.assertEqual(res.data["amount"][0].code, "invalid")

        # Try with Category and amount as None
        malformed_data = self.modify_data(
            original_data, "savingcategory", remove_field=True
        )
        malformed_data = self.modify_data(
            malformed_data, "allocated_amount", remove_field=True
        )
        malformed_data = self.modify_data(
            malformed_data, "amount", modification_value=None
        )
        res = self.client.post(url, malformed_data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = "This field may not be null."
        self.assertEqual(res.data["amount"][0], msg)
        self.assertEqual(res.data["amount"][0].code, "null")

        # Try with Category and amount as a negative number
        malformed_data = self.modify_data(
            original_data, "savingcategory", remove_field=True
        )
        malformed_data = self.modify_data(
            malformed_data, "allocated_amount", remove_field=True
        )
        malformed_data = self.modify_data(
            malformed_data, "amount", modification_value=-42
        )
        res = self.client.post(url, malformed_data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = "-42.000 should be more than 0."
        self.assertEqual(res.data["amount"][0], msg)
        self.assertEqual(res.data["amount"][0].code, "invalid")

        # Try with Category and allocated_amount as a string
        malformed_data = self.modify_data(
            original_data, "savingcategory", remove_field=True
        )
        malformed_data = self.modify_data(
            malformed_data,
            "allocated_amount",
            modification_value="I am not a number",
        )
        res = self.client.post(url, malformed_data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = "A valid number is required."
        self.assertEqual(str(res.data["allocated_amount"][0]), msg)
        self.assertEqual(res.data["allocated_amount"][0].code, "invalid")

        # Try with Category and allocated_amount as a negative number
        malformed_data = self.modify_data(
            original_data, "savingcategory", remove_field=True
        )
        malformed_data = self.modify_data(
            malformed_data, "allocated_amount", modification_value=-42
        )
        res = self.client.post(url, malformed_data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = "-42.000 should be more than 0."
        self.assertEqual(str(res.data["allocated_amount"][0]), msg)
        self.assertEqual(res.data["allocated_amount"][0].code, "invalid")

        # Try with Category and year as None
        malformed_data = self.modify_data(
            original_data, "savingcategory", remove_field=True
        )
        malformed_data = self.modify_data(
            malformed_data, "allocated_amount", remove_field=True
        )
        malformed_data = self.modify_data(
            malformed_data, "year", modification_value=None
        )
        res = self.client.post(url, malformed_data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = "This field may not be null."
        self.assertEqual(res.data["year"][0], msg)

        # Try with Category and year as a String
        malformed_data = self.modify_data(
            original_data, "savingcategory", remove_field=True
        )
        malformed_data = self.modify_data(
            malformed_data, "allocated_amount", remove_field=True
        )
        malformed_data = self.modify_data(
            malformed_data, "year", modification_value="definitely not an int"
        )
        res = self.client.post(url, malformed_data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = "A valid integer is required."
        self.assertEqual(str(res.data["year"][0]), msg)
        self.assertEqual(res.data["year"][0].code, "invalid")

        # Try with Category and year as a negative integer
        malformed_data = self.modify_data(
            original_data, "savingcategory", remove_field=True
        )
        malformed_data = self.modify_data(
            malformed_data, "allocated_amount", remove_field=True
        )
        malformed_data = self.modify_data(
            malformed_data, "year", modification_value=-1
        )
        res = self.client.post(url, malformed_data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = "Year must be an integer equal or more than 1970."
        self.assertEqual(str(res.data["year"][0]), msg)
        self.assertEqual(res.data["year"][0].code, "invalid")

        # Try with category and month as a String
        malformed_data = self.modify_data(
            original_data, "savingcategory", remove_field=True
        )
        malformed_data = self.modify_data(
            malformed_data, "allocated_amount", remove_field=True
        )
        malformed_data = self.modify_data(
            malformed_data, "month", modification_value="definitely not an int"
        )
        res = self.client.post(url, malformed_data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = "A valid integer is required."
        self.assertEqual(str(res.data["month"][0]), msg)
        self.assertEqual(res.data["month"][0].code, "invalid")

        # Try with Saving Category and month as a negative integer
        malformed_data = self.modify_data(
            original_data, "savingcategory", remove_field=True
        )
        malformed_data = self.modify_data(
            malformed_data, "allocated_amount", remove_field=True
        )
        malformed_data = self.modify_data(
            malformed_data, "month", modification_value=-42
        )
        res = self.client.post(url, malformed_data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = "Month must be between 1 and 12 (inclusive)."
        self.assertEqual(str(res.data["month"][0]), msg)
        self.assertEqual(res.data["month"][0].code, "invalid")

        # Try with category and month as a 13
        malformed_data = self.modify_data(
            original_data, "savingcategory", remove_field=True
        )
        malformed_data = self.modify_data(
            malformed_data, "allocated_amount", remove_field=True
        )
        malformed_data = self.modify_data(
            malformed_data, "month", modification_value=13
        )
        res = self.client.post(url, malformed_data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = "Month must be between 1 and 12 (inclusive)."
        self.assertEqual(str(res.data["month"][0]), msg)
        self.assertEqual(res.data["month"][0].code, "invalid")

    def test_malformatted_saving_budget_data_creation_results_in_error(self):
        self.client.force_login(self.user)

        url = reverse("api:v1:budgets")
        original_data = {
            "category": self.category.id,
            "savingcategory": self.savingcategory.id,
            "year": random.randint(1970, 2050),
            "month": random.randint(1, 12),
            "amount": round(random.uniform(1, 500), 2),
            "allocated_amount": 0.0,
        }

        # Try with Saving Category and amount as a negative number
        malformed_data = self.modify_data(
            original_data, "category", remove_field=True
        )
        malformed_data = self.modify_data(
            malformed_data, "amount", modification_value=-42
        )
        res = self.client.post(url, malformed_data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = "-42.000 should be more than 0."
        self.assertEqual(res.data["amount"][0], msg)
        self.assertEqual(res.data["amount"][0].code, "invalid")

        # Try with Saving Category and amount as a string
        malformed_data = self.modify_data(
            original_data, "category", remove_field=True
        )
        malformed_data = self.modify_data(
            malformed_data, "amount", modification_value="I am not a number"
        )
        res = self.client.post(url, malformed_data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = "A valid number is required."
        self.assertEqual(str(res.data["amount"][0]), msg)
        self.assertEqual(res.data["amount"][0].code, "invalid")

        # Try with Saving Category and amount as None
        malformed_data = self.modify_data(
            original_data, "category", remove_field=True
        )
        malformed_data = self.modify_data(
            malformed_data, "amount", modification_value=None
        )
        res = self.client.post(url, malformed_data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = "This field may not be null."
        self.assertEqual(str(res.data["amount"][0]), msg)
        self.assertEqual(res.data["amount"][0].code, "null")

        # Try with Saving Category and amount as a negative number
        malformed_data = self.modify_data(
            original_data, "category", remove_field=True
        )
        malformed_data = self.modify_data(
            malformed_data, "amount", modification_value=-42
        )
        res = self.client.post(url, malformed_data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = "-42.000 should be more than 0."
        self.assertEqual(str(res.data["amount"][0]), msg)
        self.assertEqual(res.data["amount"][0].code, "invalid")

        # Try with Saving Category and allocated_amount as a string
        malformed_data = self.modify_data(
            original_data, "category", remove_field=True
        )
        malformed_data = self.modify_data(
            malformed_data,
            "allocated_amount",
            modification_value="I am not a number",
        )
        res = self.client.post(url, malformed_data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = "A valid number is required."
        self.assertEqual(str(res.data["allocated_amount"][0]), msg)
        self.assertEqual(res.data["allocated_amount"][0].code, "invalid")

        # Try with Saving Category and allocated_amount as a negative number
        malformed_data = self.modify_data(
            original_data, "category", remove_field=True
        )
        malformed_data = self.modify_data(
            malformed_data, "allocated_amount", modification_value=-42
        )
        res = self.client.post(url, malformed_data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = "-42.000 should be more than 0."
        self.assertEqual(str(res.data["allocated_amount"][0]), msg)
        self.assertEqual(res.data["allocated_amount"][0].code, "invalid")

        # Try with Saving Category and year as None
        malformed_data = self.modify_data(
            original_data, "category", remove_field=True
        )
        malformed_data = self.modify_data(
            malformed_data, "year", modification_value=None
        )
        res = self.client.post(url, malformed_data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = "This field may not be null."
        self.assertEqual(res.data["year"][0], msg)

        # Try with Saving Category and year as a String
        malformed_data = self.modify_data(
            original_data, "category", remove_field=True
        )
        malformed_data = self.modify_data(
            malformed_data, "year", modification_value="definitely not an int"
        )
        res = self.client.post(url, malformed_data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = "A valid integer is required."
        self.assertEqual(str(res.data["year"][0]), msg)
        self.assertEqual(res.data["year"][0].code, "invalid")

        # Try with Saving Category and year as a negative integer
        malformed_data = self.modify_data(
            original_data, "category", remove_field=True
        )
        malformed_data = self.modify_data(
            malformed_data, "year", modification_value=-1
        )
        res = self.client.post(url, malformed_data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = "Year must be an integer equal or more than 1970."
        self.assertEqual(str(res.data["year"][0]), msg)
        self.assertEqual(res.data["year"][0].code, "invalid")

        # Try with Saving Category and month as a String
        malformed_data = self.modify_data(
            original_data, "category", remove_field=True
        )
        malformed_data = self.modify_data(
            malformed_data, "month", modification_value="definitely not an int"
        )
        res = self.client.post(url, malformed_data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = "A valid integer is required."
        self.assertEqual(str(res.data["month"][0]), msg)
        self.assertEqual(res.data["month"][0].code, "invalid")

        # Try with Saving Category and month as a negative integer
        malformed_data = self.modify_data(
            original_data, "category", remove_field=True
        )
        malformed_data = self.modify_data(
            malformed_data, "month", modification_value=-42
        )
        res = self.client.post(url, malformed_data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = "Month must be between 1 and 12 (inclusive)."
        self.assertEqual(str(res.data["month"][0]), msg)
        self.assertEqual(res.data["month"][0].code, "invalid")

        # Try with Saving Category and month as a 13
        malformed_data = self.modify_data(
            original_data, "category", remove_field=True
        )
        malformed_data = self.modify_data(
            malformed_data, "month", modification_value=13
        )
        res = self.client.post(url, malformed_data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = "Month must be between 1 and 12 (inclusive)."
        self.assertEqual(str(res.data["month"][0]), msg)
        self.assertEqual(res.data["month"][0].code, "invalid")

    def test_different_users_can_have_the_same_month_year_budget(self):
        self.client.force_login(self.user)

        data = {
            "category": self.category.id,
            "year": 2023,
            "month": 1,
            "amount": 500.0,
        }
        url = reverse("api:v1:budgets")
        res = self.client.post(url, data, format="json")
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            m.Budget.objects.filter(created_by=self.user.id).count(), 1  # pylint: disable=E1101; # noqa
        )
        self.assertEqual(
            m.Budget.objects.filter(created_by=self.user.id)  # pylint: disable=E1101; # noqa
            .first()
            .category,
            self.category,
        )
        # fmt: on
        budgets_r = self.client.get(url, format="json")
        self.assertEqual(budgets_r.data["count"], 1)
        self.assertEqual(len(budgets_r.data["results"]), 1)
        self.assertEqual(
            budgets_r.data["results"][0]["category"]["id"], self.category.id
        )
        self.assertEqual(budgets_r.data["results"][0]["year"], 2023)
        self.assertEqual(budgets_r.data["results"][0]["month"], 1)
        self.assertEqual(budgets_r.data["results"][0]["amount"], "500.000")

        self.client.force_login(self.user2)

        data = {
            "category": self.category2.id,
            "year": 2023,
            "month": 1,
            "amount": 500.0,
        }
        url = reverse("api:v1:budgets")
        res = self.client.post(url, data, format="json")
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            m.Budget.objects.filter(created_by=self.user2.id).count(), 1 # pylint: disable=E1101; # noqa
        )  
        self.assertEqual(
            m.Budget.objects.filter(created_by=self.user2.id)  # pylint: disable=E1101; # noqa
            .first()
            .category,
            self.category2,
        )
        # fmt: on
        budgets_r = self.client.get(url, format="json")
        self.assertEqual(budgets_r.data["count"], 1)
        self.assertEqual(len(budgets_r.data["results"]), 1)
        self.assertEqual(
            budgets_r.data["results"][0]["category"]["id"], self.category2.id
        )
        self.assertEqual(budgets_r.data["results"][0]["year"], 2023)
        self.assertEqual(budgets_r.data["results"][0]["month"], 1)
        self.assertEqual(budgets_r.data["results"][0]["amount"], "500.000")

    def test_can_not_create_duplicated_bugdets(self):
        self.client.force_login(self.user)
        url = reverse("api:v1:budgets")
        res, data = self.create_budget(
            category_id=self.category.id,
            savingcategory_id=None,
            month=-1,
            data=None,
        )
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        budgets_r = self.client.get(url, format="json")
        self.assertEqual(budgets_r.data["count"], 1)
        self.assertEqual(len(budgets_r.data["results"]), 1)
        self.assertEqual(
            budgets_r.data["results"][0]["category"]["id"], self.category.id
        )
        self.assertEqual(budgets_r.data["results"][0]["year"], data["year"])
        self.assertEqual(budgets_r.data["results"][0]["month"], None)
        self.assertEqual(
            float(budgets_r.data["results"][0]["amount"]), data["amount"]
        )

        # Fails to create a Budget with the same category and year (no months)
        res = self.client.post(url, data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = (
            "A Budget for this Category for this year/month (or year)"
            " already exists for this user."
        )
        self.assertEqual(res.data["non_field_errors"][0], msg)

        # Create a Budget with a Category for a given year and month
        data = {
            "category": self.category.id,
            "year": random.randint(1970, 2050),
            "month": random.randint(1, 12),
            "amount": round(random.uniform(1, 500), 2),
        }

        # Succeeds on the first creation
        url = reverse("api:v1:budgets")
        res = self.client.post(url, data, format="json")
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        budgets_r = self.client.get(url, format="json")
        self.assertEqual(budgets_r.data["count"], 2)
        self.assertEqual(len(budgets_r.data["results"]), 2)
        # Since the API might be returning the budgets in a different order
        # from now we sacrifice some readability to future proof this test
        found = False
        for b in budgets_r.data["results"]:
            if (
                b["year"] == data["year"]
                and b["month"] == data["month"]
                and b["category"]["id"] == self.category.id
            ):
                found = True
                # ignore the amount for now
                # self.assertEqual(b['amount'], data['amount'])
                # as we're returning strings
                break
        self.assertTrue(
            found,
            f"Budget {budgets_r.data['results']} did not include an item with"
            f" Category ID:{self.category.id} in the response. Fail!",
        )

        # Fails to create a Budget with the same category and year (no months)
        res = self.client.post(url, data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = (
            "A Budget for this Category for this year/month"
            " (or year) already exists for this user."
        )
        self.assertEqual(res.data["non_field_errors"][0], msg)

        # Create a Budget with a Saving Category for a given year i.e. no month
        data = {
            "savingcategory": self.savingcategory.id,
            "year": random.randint(1970, 2050),
            "amount": round(random.uniform(1, 500), 2),
            "allocated_amount": 0.0,
        }

        # Succeeds on the first creation
        url = reverse("api:v1:budgets")
        res = self.client.post(url, data, format="json")
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        budgets_r = self.client.get(url, format="json")
        self.assertEqual(budgets_r.data["count"], 3)
        self.assertEqual(len(budgets_r.data["results"]), 3)
        # Since the API might be returning the budgets in a different order
        # from now we sacrifice some readability to future proof this test
        found = False
        for b in budgets_r.data["results"]:
            if (
                b["year"] == data["year"]
                and b["month"] == None
                and "savingcategory" in b
                and b["savingcategory"] is not None
                and b["savingcategory"]["id"] == self.savingcategory.id
            ):
                found = True
                # ignore the amount for now, as
                # self.assertEqual(b['amount'], data['amount'])
                # we're returning strings
                break
        self.assertTrue(
            found,
            f"Budget {budgets_r.data['results']} did not include an item with"
            f"savingcategory ID:{self.savingcategory.id} in the response. "
            "Fail!",
        )

        # Fails to create a Budget with the same savingcategory and year (no months)
        res = self.client.post(url, data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = (
            "A Budget for this Saving Category for this year/month"
            " (or year) already exists for this user."
        )
        self.assertEqual(res.data["non_field_errors"][0], msg)

        # Create a Budget with a Saving Category for a given year and month
        data = {
            "savingcategory": self.savingcategory.id,
            "year": random.randint(1970, 2050),
            "month": random.randint(1, 12),
            "amount": round(random.uniform(1, 500), 2),
            "allocated_amount": 0.0,
        }

        # Succeeds on the first creation
        url = reverse("api:v1:budgets")
        res = self.client.post(url, data, format="json")
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        budgets_r = self.client.get(url, format="json")
        self.assertEqual(budgets_r.data["count"], 4)
        self.assertEqual(len(budgets_r.data["results"]), 4)
        # Since the API might be returning the budgets in a different order
        # from now we sacrifice some readability to future proof this test
        found = False
        for b in budgets_r.data["results"]:
            if (
                b["year"] == data["year"]
                and b["month"] == data["month"]
                and b["savingcategory"]["id"] == self.savingcategory.id
            ):
                found = True
                # ignore the amount for now
                # self.assertEqual(b['amount'], data['amount'])
                # as we're returning strings
                break
        self.assertTrue(
            found,
            f"Budget {budgets_r.data['results']} did not include an item with"
            f"savingcategory ID:{self.savingcategory.id} in the response. "
            "Fail!",
        )

        # Fails to create a Budget with the same savingcategory and year
        # (no months)
        res = self.client.post(url, data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = (
            "A Budget for this Saving Category for this year/month"
            " (or year) already exists for this user."
        )
        self.assertEqual(res.data["non_field_errors"][0], msg)

    def test_can_update_budgets_with_category_with_valid_data(self):
        self.client.force_login(self.user)
        url = reverse("api:v1:budgets")

        # Create a Budget with a Category for a given year (i.e. no month)
        res, data = self.create_budget(
            category_id=self.category.id,
            savingcategory_id=None,
            month=-1,
            data=None,
        )
        # Succeeds on the first creation
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        res = self.client.get(url, format="json")
        self.assertEqual(res.data["count"], 1)
        self.assertEqual(len(res.data["results"]), 1)
        self.assertEqual(
            res.data["results"][0]["category"]["id"], self.category.id
        )
        self.assertEqual(res.data["results"][0]["year"], data["year"])
        self.assertEqual(res.data["results"][0]["month"], None)
        self.assertEqual(
            float(res.data["results"][0]["amount"]), data["amount"]
        )

        b_id = res.data["results"][0]["id"]
        new_amount = round(random.uniform(501, 600), 3)
        update_data = {"amount": new_amount}
        res = self.update_budget(b_id, update_data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)

        res = self.client.get(url, format="json")
        self.assertEqual(
            res.data["results"][0]["category"]["id"], self.category.id
        )
        self.assertEqual(res.data["results"][0]["year"], data["year"])
        self.assertEqual(res.data["results"][0]["month"], None)
        self.assertEqual(float(res.data["results"][0]["amount"]), new_amount)

    def test_can_update_budgets_with_savingcategory_with_valid_data(self):
        self.client.force_login(self.user)

        res, data = self.create_budget(
            category_id=None,
            savingcategory_id=self.savingcategory.id,
            month=-1,
            data=None,
        )
        # Succeeds on the first creation
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        url = reverse("api:v1:budgets")
        budgets_r = self.client.get(url, format="json")
        self.assertEqual(budgets_r.data["count"], 1)
        self.assertEqual(len(budgets_r.data["results"]), 1)
        self.assertEqual(
            budgets_r.data["results"][0]["savingcategory"]["id"],
            self.savingcategory.id,
        )
        self.assertEqual(budgets_r.data["results"][0]["year"], data["year"])
        self.assertEqual(budgets_r.data["results"][0]["month"], None)
        # FIXME: Returning Decimals as strings to the frontend is not
        # a great idea typscript might not be able to handle it
        self.assertEqual(
            float(budgets_r.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            float(budgets_r.data["results"][0]["allocated_amount"]),
            data["allocated_amount"],
        )

        b_id = budgets_r.data["results"][0]["id"]
        new_amount = round(random.uniform(501, 600), 3)
        new_allocated_amount = round(random.uniform(501, 600), 3)
        update_data = {
            "amount": new_amount,
            "allocated_amount": new_allocated_amount,
        }
        res = self.update_budget(b_id, update_data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)

        budgets_r = self.client.get(url, format="json")
        self.assertEqual(
            budgets_r.data["results"][0]["savingcategory"]["id"],
            self.savingcategory.id,
        )
        self.assertEqual(budgets_r.data["results"][0]["year"], data["year"])
        self.assertEqual(budgets_r.data["results"][0]["month"], None)
        self.assertEqual(
            float(budgets_r.data["results"][0]["amount"]), new_amount
        )
        self.assertEqual(
            float(budgets_r.data["results"][0]["allocated_amount"]),
            new_allocated_amount,
        )

    def test_when_updating_can_not_change_a_budget_s_category(self):
        self.client.force_login(self.user)
        url = reverse("api:v1:budgets")

        # Create a Budget with a Category for a given year (i.e. no month)
        res, data = self.create_budget(
            category_id=self.category.id,
            savingcategory_id=None,
            month=-1,
            data=None,
        )
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        budgets_r = self.client.get(url, format="json")
        self.assertEqual(budgets_r.data["count"], 1)
        self.assertEqual(len(budgets_r.data["results"]), 1)
        self.assertEqual(
            budgets_r.data["results"][0]["category"]["id"], self.category.id
        )
        self.assertEqual(budgets_r.data["results"][0]["year"], data["year"])
        self.assertEqual(budgets_r.data["results"][0]["month"], None)
        self.assertEqual(
            float(budgets_r.data["results"][0]["amount"]), data["amount"]
        )

        # Update with another category belongnig to this user
        b_id = budgets_r.data["results"][0]["id"]
        # fmt: off
        cat3 = m.Category.objects.create(  # pylint: disable=E1101; # noqa
            text="Trip to the center of Earth",
            created_by=self.user,
        )
        # fmt: on
        new_amount = round(random.uniform(501, 600), 3)
        update_data = {"amount": new_amount, "category": cat3.id}
        res = self.update_budget(b_id, update_data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)

        # The category has not changed indeed
        budgets_r = self.client.get(url, format="json")
        self.assertEqual(budgets_r.data["count"], 1)
        self.assertEqual(len(budgets_r.data["results"]), 1)
        self.assertEqual(
            budgets_r.data["results"][0]["category"]["id"], self.category.id
        )
        self.assertEqual(budgets_r.data["results"][0]["year"], data["year"])
        self.assertEqual(budgets_r.data["results"][0]["month"], None)
        self.assertEqual(
            float(budgets_r.data["results"][0]["amount"]), new_amount
        )

    def test_when_updating_can_not_change_a_budget_s_savingcategory(self):
        self.client.force_login(self.user)

        res, data = self.create_budget(
            category_id=None,
            savingcategory_id=self.savingcategory.id,
            month=-1,
            data=None,
        )
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        url = reverse("api:v1:budgets")
        res = self.client.get(url, format="json")
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data["count"], 1)
        self.assertEqual(len(res.data["results"]), 1)
        self.assertEqual(
            res.data["results"][0]["savingcategory"]["id"],
            self.savingcategory.id,
        )
        self.assertEqual(res.data["results"][0]["year"], data["year"])
        self.assertEqual(res.data["results"][0]["month"], None)
        # FIXME: Returning Decimals as strings to the frontend is not a
        # great idea: typscript might not be able to handle it
        self.assertEqual(
            float(res.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            float(res.data["results"][0]["allocated_amount"]),
            data["allocated_amount"],
        )

        b_id = res.data["results"][0]["id"]
        # fmt: off
        sac3 = m.SavingCategory.objects.create(  # pylint: disable=E1101; # noqa
            text="Monthly Saving",
            created_by=self.user,
        )
        # fmt: on
        new_amount = round(random.uniform(501, 600), 3)
        new_allocated_amount = round(random.uniform(501, 600), 3)
        update_data = {
            "amount": new_amount,
            "allocated_amount": new_allocated_amount,
            "savingcategory": sac3.id,
        }
        res = self.update_budget(b_id, update_data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)

        res = self.client.get(url, format="json")
        self.assertEqual(
            res.data["results"][0]["savingcategory"]["id"],
            self.savingcategory.id,
        )
        self.assertEqual(res.data["results"][0]["year"], data["year"])
        self.assertEqual(res.data["results"][0]["month"], None)
        self.assertEqual(float(res.data["results"][0]["amount"]), new_amount)
        self.assertEqual(
            float(res.data["results"][0]["allocated_amount"]),
            new_allocated_amount,
        )

    def test_can_not_set_allocated_amount_when_updating_budgets_with_category(
        self,
    ):
        self.client.force_login(self.user)
        url = reverse("api:v1:budgets")

        # Create a Budget with a Category for a given year (i.e. no month)
        res, data = self.create_budget(
            category_id=self.category.id,
            savingcategory_id=None,
            month=-1,
            data=None,
        )
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        budgets_r = self.client.get(url, format="json")
        self.assertEqual(budgets_r.data["count"], 1)
        self.assertEqual(len(budgets_r.data["results"]), 1)
        self.assertEqual(
            budgets_r.data["results"][0]["category"]["id"], self.category.id
        )
        self.assertEqual(budgets_r.data["results"][0]["year"], data["year"])
        self.assertEqual(budgets_r.data["results"][0]["month"], None)
        self.assertEqual(
            float(budgets_r.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            budgets_r.data["results"][0]["allocated_amount"], None
        )

        # Update the budget with a new amount, and to have an allocated_amount
        # as well
        b_id = budgets_r.data["results"][0]["id"]
        new_amount = round(random.uniform(501, 600), 3)
        allocated_amount = round(random.uniform(501, 600), 3)
        update_data = {
            "amount": new_amount,
            "allocated_amount": allocated_amount,
        }
        res = self.update_budget(b_id, update_data)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = "Cannot update allocated_amount for Budget having a category."
        self.assertEqual(res.data["non_field_errors"][0], msg)
        self.assertEqual(res.data["non_field_errors"][0].code, "invalid")

        res = self.client.get(url, format="json")
        self.assertEqual(res.data["count"], 1)
        self.assertEqual(len(res.data["results"]), 1)
        self.assertEqual(
            res.data["results"][0]["category"]["id"], self.category.id
        )
        self.assertEqual(res.data["results"][0]["year"], data["year"])
        self.assertEqual(res.data["results"][0]["month"], None)
        self.assertEqual(
            float(res.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            budgets_r.data["results"][0]["allocated_amount"], None
        )

    def test_can_not_remove_savingcategory_when_updating_budgets_with_savingcategory(
        self,
    ):
        self.client.force_login(self.user)
        url = reverse("api:v1:budgets")

        # Create a Budget with a savingcategory for a given year (i.e. no month)
        res, data = self.create_budget(
            category_id=None,
            savingcategory_id=self.savingcategory.id,
            month=-1,
            data=None,
        )
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        budgets_r = self.client.get(url, format="json")
        self.assertEqual(budgets_r.data["count"], 1)
        self.assertEqual(len(budgets_r.data["results"]), 1)
        self.assertEqual(
            budgets_r.data["results"][0]["savingcategory"]["id"],
            self.savingcategory.id,
        )
        self.assertEqual(budgets_r.data["results"][0]["year"], data["year"])
        self.assertEqual(budgets_r.data["results"][0]["month"], None)
        self.assertEqual(
            float(budgets_r.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            float(budgets_r.data["results"][0]["allocated_amount"]),
            data["allocated_amount"],
        )

        # Tries to remove the savingcategory
        b_id = budgets_r.data["results"][0]["id"]
        update_data = {"savingcategory": None}
        res = self.update_budget(b_id, update_data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        if "savingcategory" in res.data:
            self.fail(
                "Budget update body has returned 'category' field."
                " Has the spec changed? Fail."
            )

        res = self.client.get(url, format="json")
        self.assertEqual(res.data["count"], 1)
        self.assertEqual(len(res.data["results"]), 1)
        self.assertEqual(
            res.data["results"][0]["savingcategory"]["id"],
            self.savingcategory.id,
        )
        self.assertEqual(res.data["results"][0]["year"], data["year"])
        self.assertEqual(res.data["results"][0]["month"], None)
        self.assertEqual(
            float(res.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            float(budgets_r.data["results"][0]["allocated_amount"]),
            data["allocated_amount"],
        )

    def test_can_not_remove_category_when_updating_budgets_with_category(self):
        self.client.force_login(self.user)
        url = reverse("api:v1:budgets")

        # Create a Budget with a Category for a given year (i.e. no month)
        res, data = self.create_budget(
            category_id=self.category.id,
            savingcategory_id=None,
            month=-1,
            data=None,
        )
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        budgets_r = self.client.get(url, format="json")
        self.assertEqual(budgets_r.data["count"], 1)
        self.assertEqual(len(budgets_r.data["results"]), 1)
        self.assertEqual(
            budgets_r.data["results"][0]["category"]["id"], self.category.id
        )
        self.assertEqual(budgets_r.data["results"][0]["year"], data["year"])
        self.assertEqual(budgets_r.data["results"][0]["month"], None)
        self.assertEqual(
            float(budgets_r.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            budgets_r.data["results"][0]["allocated_amount"], None
        )

        # Tries to remove the Category
        b_id = budgets_r.data["results"][0]["id"]
        update_data = {"category": None}
        res = self.update_budget(b_id, update_data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        if "category" in res.data:
            self.fail(
                "Budget update body has returned 'category' field."
                " Has the spec changed? Fail."
            )

        res = self.client.get(url, format="json")
        self.assertEqual(res.data["count"], 1)
        self.assertEqual(len(res.data["results"]), 1)
        self.assertEqual(
            res.data["results"][0]["category"]["id"], self.category.id
        )
        self.assertEqual(res.data["results"][0]["year"], data["year"])
        self.assertEqual(res.data["results"][0]["month"], None)
        # NOTE: The new_amount is not applied since the update has failed
        self.assertEqual(
            float(res.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            budgets_r.data["results"][0]["allocated_amount"], None
        )

    def test_can_not_create_duplicated_year_only_budgets_when_updating_budgets_with_category(
        self,
    ):
        self.client.force_login(self.user)

        res, data = self.create_budget(
            category_id=self.category.id,
            savingcategory_id=None,
            month=-1,
            data=None,
        )
        # Succeeds on the first creation
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        url = reverse("api:v1:budgets")
        res = self.client.get(url, format="json")
        self.assertEqual(res.data["count"], 1)
        self.assertEqual(len(res.data["results"]), 1)
        self.assertEqual(
            res.data["results"][0]["category"]["id"], self.category.id
        )
        self.assertEqual(res.data["results"][0]["year"], data["year"])
        self.assertEqual(res.data["results"][0]["month"], None)
        self.assertEqual(
            float(res.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(res.data["results"][0]["allocated_amount"], None)

        # Save this budget id reference for later
        b_id = res.data["results"][0]["id"]

        # Create a Budget with the same Category for the same year
        # (i.e. no month): it should fail
        res, data = self.create_budget(data=data, month=None)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        err = (
            "A Budget for this Category for this year/month (or year)"
            " already exists for this user."
        )
        self.assertEqual(str(res.data["non_field_errors"][0]), err)
        self.assertEqual(res.data["non_field_errors"][0].code, "invalid")

        res = self.client.get(url, format="json")
        self.assertEqual(res.data["count"], 1)
        self.assertEqual(len(res.data["results"]), 1)
        self.assertEqual(
            res.data["results"][0]["category"]["id"], self.category.id
        )
        self.assertEqual(res.data["results"][0]["year"], data["year"])
        self.assertEqual(res.data["results"][0]["month"], None)
        self.assertEqual(
            float(res.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(res.data["results"][0]["allocated_amount"], None)

        new_data = data.copy()
        new_data["year"] += 1
        # Create a second Budget with a Category for a different year
        # (i.e. no month)
        res, new_data_res = self.create_budget(data=new_data, month=None)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        res = self.client.get(url, format="json")
        self.assertEqual(res.data["count"], 2)
        self.assertEqual(len(res.data["results"]), 2)

        # Update the first budget to have the year of the second one
        new_amount = round(random.uniform(501, 600), 3)
        update_data = {"amount": new_amount, "year": new_data["year"]}
        res = self.update_budget(b_id, update_data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        if "year" in res.data:
            self.fail(
                "Budget update body has returned 'year' field. Has the spec changed? Fail!"
            )

        res = self.client.get(url, format="json")
        self.assertEqual(res.data["count"], 2)
        self.assertEqual(len(res.data["results"]), 2)
        for r in res.data["results"]:
            if r["id"] == b_id and r["year"] == new_data["year"]:
                self.fail(
                    "Budget update body was able to update the 'year' field. Has the spec changed? Fail!"
                )

        # Since the API might be returning the budgets in a different order from now
        # we sacrifice some readability to future proof this test
        new_year_budgets_count = 0
        for b in res.data["results"]:
            if b["year"] == new_data["year"]:
                new_year_budgets_count += 1

        if new_year_budgets_count > 1:
            self.fail(
                "Budget update body was able to update the 'year' field. Has the spec changed? Fail!"
            )

    def test_can_not_create_duplicated_year_only_budgets_when_updating_budgets_with_savingcategory(
        self,
    ):
        self.client.force_login(self.user)
        url = reverse("api:v1:budgets")

        data = {
            "savingcategory": self.savingcategory.id,
            "year": random.randint(1970, 2050),
            # Amount should generally be more than allocated_amount
            # (unless a user overbudegts for it)
            "amount": round(random.uniform(43, 500), 3),
            "allocated_amount": Decimal(0.000),
        }

        # Create a Budget with a Category for a given year (i.e. no month)
        res, data = self.create_budget(data=data, month=None)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        res = self.client.get(url, format="json")
        self.assertEqual(res.data["count"], 1)
        self.assertEqual(len(res.data["results"]), 1)
        self.assertEqual(
            res.data["results"][0]["savingcategory"]["id"],
            self.savingcategory.id,
        )
        self.assertEqual(res.data["results"][0]["year"], data["year"])
        self.assertEqual(res.data["results"][0]["month"], None)
        self.assertEqual(
            float(res.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            res.data["results"][0]["allocated_amount"], Decimal(0.000)
        )

        # Save this budget id reference for later
        b_id = res.data["results"][0]["id"]

        # Create a Budget with the same Category for the same year
        # (i.e. no month): it should fail
        res, data = self.create_budget(data=data, month=None)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        err = (
            "A Budget for this Saving Category for this year/month (or year)"
            " already exists for this user."
        )
        self.assertEqual(str(res.data["non_field_errors"][0]), err)
        self.assertEqual(res.data["non_field_errors"][0].code, "invalid")

        res = self.client.get(url, format="json")
        self.assertEqual(res.data["count"], 1)
        self.assertEqual(len(res.data["results"]), 1)
        self.assertEqual(
            res.data["results"][0]["savingcategory"]["id"],
            self.savingcategory.id,
        )
        self.assertEqual(res.data["results"][0]["year"], data["year"])
        self.assertEqual(res.data["results"][0]["month"], None)
        self.assertEqual(
            float(res.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            res.data["results"][0]["allocated_amount"], Decimal(0.000)
        )

        new_data = data.copy()
        new_data["year"] += 1
        # Create a second Budget with a Category for a different year
        # i.e. no month
        res, _ = self.create_budget(data=new_data)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        res = self.client.get(url, format="json")
        self.assertEqual(res.data["count"], 2)
        self.assertEqual(len(res.data["results"]), 2)

        # Update the first budget to have the year of the second one
        new_amount = round(random.uniform(501, 600), 3)
        update_data = {"amount": new_amount, "year": new_data["year"]}
        res = self.update_budget(b_id, update_data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        if "year" in res.data:
            self.fail(
                "Budget update body has returned 'year' field."
                " Has the spec changed? Fail!"
            )

        res = self.client.get(url, format="json")
        msg = (
            "Budget update body was able to update the 'year' field."
            " Has the spec changed? Fail!"
        )
        self.assertEqual(res.data["count"], 2)
        self.assertEqual(len(res.data["results"]), 2)
        for r in res.data["results"]:
            if r["id"] == b_id and r["year"] == new_data["year"]:
                self.fail(msg)

        # Since the API might be returning the budgets in a different order
        # from now we sacrifice some readability to future proof this test
        new_year_budgets_count = 0
        for b in res.data["results"]:
            if b["year"] == new_data["year"]:
                new_year_budgets_count += 1

        msg = (
            "Budget update body was able to update the 'year' field."
            " Has the spec changed? Fail!"
        )
        if new_year_budgets_count > 1:
            self.fail(msg)

    def test_can_not_create_duplicated_year_and_month_budgets_when_updating_budgets_with_category(
        self,
    ):
        self.client.force_login(self.user)

        res, data = self.create_budget(
            category_id=self.category.id,
            savingcategory_id=None,
            month=-1,
            data=None,
        )
        # Succeeds on the first creation
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        url = reverse("api:v1:budgets")
        res = self.client.get(url, format="json")
        self.assertEqual(res.data["count"], 1)
        self.assertEqual(len(res.data["results"]), 1)
        self.assertEqual(
            res.data["results"][0]["category"]["id"], self.category.id
        )
        self.assertEqual(res.data["results"][0]["year"], data["year"])
        self.assertEqual(res.data["results"][0]["month"], None)
        self.assertEqual(
            float(res.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(res.data["results"][0]["allocated_amount"], None)

        # Save this budget id reference for later
        b_id = res.data["results"][0]["id"]

        # Create a Budget with the same Category for the same year/month:
        # it should fail
        res, data = self.create_budget(data=data, month=None)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        err = (
            "A Budget for this Category for this year/month (or year)"
            " already exists for this user."
        )
        self.assertEqual(str(res.data["non_field_errors"][0]), err)
        self.assertEqual(res.data["non_field_errors"][0].code, "invalid")

        res = self.client.get(url, format="json")
        self.assertEqual(res.data["count"], 1)
        self.assertEqual(len(res.data["results"]), 1)
        self.assertEqual(
            res.data["results"][0]["category"]["id"], self.category.id
        )
        self.assertEqual(res.data["results"][0]["year"], data["year"])
        self.assertEqual(res.data["results"][0]["month"], None)
        self.assertEqual(
            float(res.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(res.data["results"][0]["allocated_amount"], None)

        new_data = data.copy()
        new_data["year"] += 1
        new_data["month"] = 12
        # Create a second Budget with a Category for a different
        # year/month combination
        res, new_data_res = self.create_budget(data=new_data, month=None)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        res = self.client.get(url, format="json")
        self.assertEqual(res.data["count"], 2)
        self.assertEqual(len(res.data["results"]), 2)

        # Update the first budget to have the same year and month of the
        # second one
        new_amount = round(random.uniform(501, 600), 3)
        update_data = {
            "amount": new_amount,
            "year": new_data["year"],
        }
        res = self.update_budget(b_id, update_data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        if "year" in res.data:
            self.fail(
                "Budget update body has returned 'year' field."
                " Has the spec changed? Fail!"
            )

        if "month" in res.data:
            self.fail(
                "Budget update body has returned 'month' field."
                " Has the spec changed? Fail!"
            )

        res = self.client.get(url, format="json")
        self.assertEqual(res.data["count"], 2)
        self.assertEqual(len(res.data["results"]), 2)
        for r in res.data["results"]:
            if (
                r["id"] == b_id
                and r["year"] == new_data["year"]
                and r["month"] == new_data["month"]
            ):
                self.fail(
                    "Budget update body was able to update the 'year' and"
                    " the 'month' fields. Has the spec changed? Fail!"
                )

        # Since the API might be returning the budgets in a different order
        # from now we sacrifice some readability to future proof this test
        new_year_budgets_count = 0
        new_month_budgets_count = 0
        for b in res.data["results"]:
            if b["year"] == new_data["year"]:
                new_year_budgets_count += 1
            if b["month"] == new_data["month"]:
                new_month_budgets_count += 1

        if new_year_budgets_count > 1:
            self.fail(
                "Budget update body was able to update the 'year' field."
                " Has the spec changed? Fail!"
            )

        if new_month_budgets_count > 1:
            self.fail(
                "Budget update body was able to update the 'month' field."
                " Has the spec changed? Fail!"
            )

    def test_can_not_create_duplicated_year_and_month_budgets_when_updating_budgets_with_savingcategory(
        self,
    ):
        self.client.force_login(self.user)
        url = reverse("api:v1:budgets")

        data = {
            "savingcategory": self.savingcategory.id,
            "year": random.randint(1970, 2050),
            # Amount should generally be more than allocated_amount
            # (unless a user overbudegts for it)
            "amount": round(random.uniform(43, 500), 3),
            "allocated_amount": Decimal(0.000),
        }

        # Create a Budget with a Category for a given year (i.e. no month)
        res, data = self.create_budget(data=data, month=None)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        res = self.client.get(url, format="json")
        self.assertEqual(res.data["count"], 1)
        self.assertEqual(len(res.data["results"]), 1)
        self.assertEqual(
            res.data["results"][0]["savingcategory"]["id"],
            self.savingcategory.id,
        )
        self.assertEqual(res.data["results"][0]["year"], data["year"])
        self.assertEqual(res.data["results"][0]["month"], None)
        self.assertEqual(
            float(res.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            res.data["results"][0]["allocated_amount"], Decimal(0.000)
        )

        # Save this budget id reference for later
        b_id = res.data["results"][0]["id"]

        # Create a Budget with the same Category for the same year
        # (i.e. no month): it should fail
        res, data = self.create_budget(data=data, month=None)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        err = (
            "A Budget for this Saving Category for this year/month (or year)"
            " already exists for this user."
        )
        self.assertEqual(str(res.data["non_field_errors"][0]), err)
        self.assertEqual(res.data["non_field_errors"][0].code, "invalid")

        res = self.client.get(url, format="json")
        self.assertEqual(res.data["count"], 1)
        self.assertEqual(len(res.data["results"]), 1)
        self.assertEqual(
            res.data["results"][0]["savingcategory"]["id"],
            self.savingcategory.id,
        )
        self.assertEqual(res.data["results"][0]["year"], data["year"])
        self.assertEqual(res.data["results"][0]["month"], None)
        self.assertEqual(
            float(res.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            res.data["results"][0]["allocated_amount"], Decimal(0.000)
        )

        new_data = data.copy()
        new_data["year"] += 1
        new_data["month"] = 12
        # Create a second Budget with a Category for a different year
        # (i.e. no month)
        res, _ = self.create_budget(data=new_data)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        res = self.client.get(url, format="json")
        self.assertEqual(res.data["count"], 2)
        self.assertEqual(len(res.data["results"]), 2)

        # Update the first budget to have the year of the second one
        new_amount = round(random.uniform(501, 600), 3)
        update_data = {
            "amount": new_amount,
            "year": new_data["year"],
            "month": new_data["month"],
        }
        res = self.update_budget(b_id, update_data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        if "year" in res.data:
            self.fail(
                "Budget update body has returned 'year' field."
                " Has the spec changed? Fail!"
            )

        if "month" in res.data:
            self.fail(
                "Budget update body has returned 'month' field."
                " Has the spec changed? Fail!"
            )

        res = self.client.get(url, format="json")
        msg = (
            "Budget update body was able to update the 'year' field."
            " Has the spec changed? Fail!"
        )
        self.assertEqual(res.data["count"], 2)
        self.assertEqual(len(res.data["results"]), 2)
        for r in res.data["results"]:
            if (
                r["id"] == b_id
                and r["year"] == new_data["year"]
                and r["month"] == new_data["month"]
            ):
                self.fail(
                    "Budget update body was able to update the 'year' and"
                    " the 'month' fields. Has the spec changed? Fail!"
                )

        # Since the API might be returning the budgets in a different order
        # from now we sacrifice some readability to future proof this test
        new_year_budgets_count = 0
        new_month_budgets_count = 0
        for b in res.data["results"]:
            if b["year"] == new_data["year"]:
                new_year_budgets_count += 1
            if b["month"] == new_data["month"]:
                new_month_budgets_count += 1

        if new_year_budgets_count > 1:
            self.fail(
                "Budget update body was able to update the 'year' field."
                " Has the spec changed? Fail!"
            )

        if new_month_budgets_count > 1:
            self.fail(
                "Budget update body was able to update the 'month' field."
                " Has the spec changed? Fail!"
            )

    def test_can_not_alter_year_on_update_for_budgets_with_savingcategory(
        self,
    ):
        self.client.force_login(self.user)
        url = reverse("api:v1:budgets")

        # Create a Budget with a savingcategory for a given year
        # (i.e. no month)
        res, data = self.create_budget(
            category_id=None,
            savingcategory_id=self.savingcategory.id,
            month=-1,
            data=None,
        )
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        budgets_r = self.client.get(url, format="json")
        self.assertEqual(budgets_r.data["count"], 1)
        self.assertEqual(len(budgets_r.data["results"]), 1)
        self.assertEqual(
            budgets_r.data["results"][0]["savingcategory"]["id"],
            self.savingcategory.id,
        )
        self.assertEqual(budgets_r.data["results"][0]["year"], data["year"])
        self.assertEqual(budgets_r.data["results"][0]["month"], None)
        self.assertEqual(
            float(budgets_r.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            float(budgets_r.data["results"][0]["allocated_amount"]),
            data["allocated_amount"],
        )

        # Tries to update the year
        b_id = budgets_r.data["results"][0]["id"]
        update_data = {"year": random.randint(1970, 2050)}
        # Make sure the random year is not the same as the original one
        while update_data["year"] == data["year"]:
            update_data["year"] = random.randint(1970, 2050)

        res = self.update_budget(b_id, update_data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        if "year" in res.data:
            self.fail(
                "Budget update body has returned 'year' field."
                " Has the spec changed? Fail!"
            )

        res = self.client.get(url, format="json")
        self.assertEqual(res.data["count"], 1)
        self.assertEqual(len(res.data["results"]), 1)
        self.assertEqual(
            res.data["results"][0]["savingcategory"]["id"],
            self.savingcategory.id,
        )
        self.assertEqual(res.data["results"][0]["year"], data["year"])
        self.assertEqual(res.data["results"][0]["month"], None)
        self.assertEqual(
            float(budgets_r.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            float(budgets_r.data["results"][0]["allocated_amount"]),
            data["allocated_amount"],
        )

    def test_can_not_alter_month_on_update_for_budgets_with_savingcategory(
        self,
    ):
        self.client.force_login(self.user)
        url = reverse("api:v1:budgets")

        # Create a Budget with a SetAdideCategory for a given year and month
        res, data = self.create_budget(
            category_id=None,
            savingcategory_id=self.savingcategory.id,
            month=12,
            data=None,
        )
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        budgets_r = self.client.get(url, format="json")
        self.assertEqual(budgets_r.data["count"], 1)
        self.assertEqual(len(budgets_r.data["results"]), 1)
        self.assertEqual(
            budgets_r.data["results"][0]["savingcategory"]["id"],
            self.savingcategory.id,
        )
        self.assertEqual(budgets_r.data["results"][0]["year"], data["year"])
        self.assertEqual(budgets_r.data["results"][0]["month"], 12)
        self.assertEqual(
            float(budgets_r.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            float(budgets_r.data["results"][0]["allocated_amount"]),
            data["allocated_amount"],
        )

        # Tries to update the month
        b_id = budgets_r.data["results"][0]["id"]
        update_data = {"month": random.randint(1, 12)}
        # Make sure the random month is not the same as the original one
        while update_data["month"] == data["month"]:
            update_data["month"] = random.randint(1, 12)

        res = self.update_budget(b_id, update_data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        if "month" in res.data:
            self.fail(
                "Budget update body has returned 'month' field."
                " Has the spec changed? Fail!"
            )

        res = self.client.get(url, format="json")
        self.assertEqual(res.data["count"], 1)
        self.assertEqual(len(res.data["results"]), 1)
        self.assertEqual(
            res.data["results"][0]["savingcategory"]["id"],
            self.savingcategory.id,
        )
        self.assertEqual(res.data["results"][0]["year"], data["year"])
        self.assertEqual(res.data["results"][0]["month"], 12)
        self.assertEqual(
            float(budgets_r.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            float(budgets_r.data["results"][0]["allocated_amount"]),
            data["allocated_amount"],
        )

    def test_can_not_alter_year_and_month_on_update_for_budgets_with_savingcategory(
        self,
    ):
        self.client.force_login(self.user)
        url = reverse("api:v1:budgets")

        # Create a Budget with a SetAdideCategory for a given year and month
        res, data = self.create_budget(
            category_id=None,
            savingcategory_id=self.savingcategory.id,
            month=12,
            data=None,
        )
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        budgets_r = self.client.get(url, format="json")
        self.assertEqual(budgets_r.data["count"], 1)
        self.assertEqual(len(budgets_r.data["results"]), 1)
        self.assertEqual(
            budgets_r.data["results"][0]["savingcategory"]["id"],
            self.savingcategory.id,
        )
        self.assertEqual(budgets_r.data["results"][0]["year"], data["year"])
        self.assertEqual(budgets_r.data["results"][0]["month"], 12)
        self.assertEqual(
            float(budgets_r.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            float(budgets_r.data["results"][0]["allocated_amount"]),
            data["allocated_amount"],
        )

        # Tries to update the year and the month as well
        b_id = budgets_r.data["results"][0]["id"]
        update_data = {
            "year": random.randint(1970, 2050),
            "month": random.randint(1970, 2050),
        }

        # Make sure the random year is not the same as the original one
        while update_data.get("year") == data["year"]:
            update_data["year"] = random.randint(1970, 2050)

        # Make sure the random month is not the same as the original one
        while update_data.get("month") == data["month"]:
            update_data["month"] = random.randint(1, 12)

        res = self.update_budget(b_id, update_data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        if "month" in res.data:
            self.fail(
                "Budget update body has returned 'month' field. Has the"
                " spec changed? Fail!"
            )

        res = self.client.get(url, format="json")
        self.assertEqual(res.data["count"], 1)
        self.assertEqual(len(res.data["results"]), 1)
        self.assertEqual(
            res.data["results"][0]["savingcategory"]["id"],
            self.savingcategory.id,
        )
        self.assertEqual(res.data["results"][0]["year"], data["year"])
        self.assertEqual(res.data["results"][0]["month"], 12)
        self.assertEqual(
            float(budgets_r.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            float(budgets_r.data["results"][0]["allocated_amount"]),
            data["allocated_amount"],
        )

    def test_can_not_alter_year_on_update_for_budgets_with_category(self):
        self.client.force_login(self.user)
        url = reverse("api:v1:budgets")

        # Create a Budget with a Category for a given year (i.e. no month)
        res, data = self.create_budget(
            category_id=self.category.id,
            savingcategory_id=None,
            month=-1,
            data=None,
        )
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        budgets_r = self.client.get(url, format="json")
        self.assertEqual(budgets_r.data["count"], 1)
        self.assertEqual(len(budgets_r.data["results"]), 1)
        self.assertEqual(
            budgets_r.data["results"][0]["category"]["id"], self.category.id
        )
        self.assertEqual(budgets_r.data["results"][0]["year"], data["year"])
        self.assertEqual(budgets_r.data["results"][0]["month"], None)
        self.assertEqual(
            float(budgets_r.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            budgets_r.data["results"][0]["allocated_amount"], None
        )

        # Tries to update the year
        b_id = budgets_r.data["results"][0]["id"]
        update_data = {"year": random.randint(1970, 2050)}
        # Make sure the random year is not the same as the original one
        while update_data["year"] == data["year"]:
            update_data["year"] = random.randint(1970, 2050)

        res = self.update_budget(b_id, update_data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        if "year" in res.data:
            self.fail(
                "Budget update body has returned 'year' field."
                " Has the spec changed? Fail!"
            )

        res = self.client.get(url, format="json")
        self.assertEqual(res.data["count"], 1)
        self.assertEqual(len(res.data["results"]), 1)
        self.assertEqual(
            res.data["results"][0]["category"]["id"], self.category.id
        )
        self.assertEqual(res.data["results"][0]["year"], data["year"])
        self.assertEqual(res.data["results"][0]["month"], None)
        # NOTE: The new_amount is not applied since the update has failed
        self.assertEqual(
            float(res.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            budgets_r.data["results"][0]["allocated_amount"], None
        )

    def test_can_not_alter_month_on_update_for_budgets_with_category(self):
        self.client.force_login(self.user)
        url = reverse("api:v1:budgets")

        # Create a Budget with a Category for a given year (i.e. no month)
        res, data = self.create_budget(
            category_id=self.category.id,
            savingcategory_id=None,
            month=11,
            data=None,
        )
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        budgets_r = self.client.get(url, format="json")
        self.assertEqual(budgets_r.data["count"], 1)
        self.assertEqual(len(budgets_r.data["results"]), 1)
        self.assertEqual(
            budgets_r.data["results"][0]["category"]["id"], self.category.id
        )
        self.assertEqual(budgets_r.data["results"][0]["year"], data["year"])
        self.assertEqual(budgets_r.data["results"][0]["month"], 11)
        self.assertEqual(
            float(budgets_r.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            budgets_r.data["results"][0]["allocated_amount"], None
        )

        # Tries to update the year
        b_id = budgets_r.data["results"][0]["id"]
        update_data = {"month": random.randint(1, 12)}
        # Make sure the random month is not the same as the original one
        while update_data["month"] == data["month"]:
            update_data["month"] = random.randint(1, 12)

        res = self.update_budget(b_id, update_data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        if "month" in res.data:
            self.fail(
                "Budget update body has returned 'month' field."
                " Has the spec changed? Fail!"
            )

        res = self.client.get(url, format="json")
        self.assertEqual(res.data["count"], 1)
        self.assertEqual(len(res.data["results"]), 1)
        self.assertEqual(
            res.data["results"][0]["category"]["id"], self.category.id
        )
        self.assertEqual(res.data["results"][0]["year"], data["year"])
        self.assertEqual(res.data["results"][0]["month"], 11)
        # NOTE: The new_amount is not applied since the update has failed
        self.assertEqual(
            float(res.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            budgets_r.data["results"][0]["allocated_amount"], None
        )

    def test_can_not_alter_year_and_month_on_update_for_budgets_with_category(
        self,
    ):
        self.client.force_login(self.user)
        url = reverse("api:v1:budgets")

        # Create a Budget with a Category for a given year and month
        res, data = self.create_budget(
            category_id=self.category.id,
            savingcategory_id=None,
            month=11,
            data=None,
        )
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        budgets_r = self.client.get(url, format="json")
        self.assertEqual(budgets_r.data["count"], 1)
        self.assertEqual(len(budgets_r.data["results"]), 1)
        self.assertEqual(
            budgets_r.data["results"][0]["category"]["id"], self.category.id
        )
        self.assertEqual(budgets_r.data["results"][0]["year"], data["year"])
        self.assertEqual(budgets_r.data["results"][0]["month"], data["month"])
        self.assertEqual(
            float(budgets_r.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            budgets_r.data["results"][0]["allocated_amount"], None
        )

        # Tries to update the year and the month as well
        b_id = budgets_r.data["results"][0]["id"]
        update_data = {
            "year": random.randint(1970, 2050),
            "month": random.randint(1970, 2050),
        }

        # Make sure the random year is not the same as the original one
        while update_data["year"] == data["year"]:
            update_data["year"] = random.randint(1970, 2050)

        # Make sure the random month is not the same as the original one
        while update_data["month"] == data["month"]:
            update_data["month"] = random.randint(1, 12)

        res = self.update_budget(b_id, update_data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        if "month" in res.data:
            self.fail(
                "Budget update body has returned 'month' field."
                " Has the spec changed? Fail!"
            )

        if "year" in res.data:
            self.fail(
                "Budget update body has returned 'year' field."
                " Has the spec changed? Fail!"
            )

        res = self.client.get(url, format="json")
        self.assertEqual(res.data["count"], 1)
        self.assertEqual(len(res.data["results"]), 1)
        self.assertEqual(
            res.data["results"][0]["category"]["id"], self.category.id
        )
        self.assertEqual(res.data["results"][0]["year"], data["year"])
        self.assertEqual(res.data["results"][0]["month"], 11)
        # NOTE: The new_amount is not applied since the update has failed
        self.assertEqual(
            float(res.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            budgets_r.data["results"][0]["allocated_amount"], None
        )

    def test_can_not_update_budgets_with_savingcategory_to_have_an_invalid_year(
        self,
    ):
        self.client.force_login(self.user)
        url = reverse("api:v1:budgets")

        # Create a Budget with a savingcategory for a given year i.e. no month
        res, data = self.create_budget(
            category_id=None,
            savingcategory_id=self.savingcategory.id,
            month=-1,
            data=None,
        )
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        budgets_r = self.client.get(url, format="json")
        self.assertEqual(budgets_r.data["count"], 1)
        self.assertEqual(len(budgets_r.data["results"]), 1)
        self.assertEqual(
            budgets_r.data["results"][0]["savingcategory"]["id"],
            self.savingcategory.id,
        )
        self.assertEqual(budgets_r.data["results"][0]["year"], data["year"])
        self.assertEqual(budgets_r.data["results"][0]["month"], None)
        self.assertEqual(
            float(budgets_r.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            float(budgets_r.data["results"][0]["allocated_amount"]),
            data["allocated_amount"],
        )

        # Tries to update the year to an invalid one
        # (we force the year to be equal or more than 1970)
        b_id = budgets_r.data["results"][0]["id"]
        update_data = {"year": 1969}

        res = self.update_budget(b_id, update_data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        if "year" in res.data:
            self.fail(
                "Budget update body has returned 'year' field."
                " Has the spec changed? Fail!"
            )

        res = self.client.get(url, format="json")
        self.assertEqual(res.data["count"], 1)
        self.assertEqual(len(res.data["results"]), 1)
        self.assertEqual(
            res.data["results"][0]["savingcategory"]["id"],
            self.savingcategory.id,
        )
        self.assertEqual(res.data["results"][0]["year"], data["year"])
        self.assertEqual(res.data["results"][0]["month"], None)
        self.assertEqual(
            float(budgets_r.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            float(budgets_r.data["results"][0]["allocated_amount"]),
            data["allocated_amount"],
        )

    # Partial duplicated of test_can_not_alter_year_on_update_for_budgets_with_category()
    def test_can_not_update_budgets_with_category_to_have_an_invalid_year(
        self,
    ):
        self.client.force_login(self.user)
        url = reverse("api:v1:budgets")

        # Create a Budget with a Category for a given year (i.e. no month)
        res, data = self.create_budget(
            category_id=self.category.id,
            savingcategory_id=None,
            month=-1,
            data=None,
        )
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        budgets_r = self.client.get(url, format="json")
        self.assertEqual(budgets_r.data["count"], 1)
        self.assertEqual(len(budgets_r.data["results"]), 1)
        self.assertEqual(
            budgets_r.data["results"][0]["category"]["id"], self.category.id
        )
        self.assertEqual(budgets_r.data["results"][0]["year"], data["year"])
        self.assertEqual(budgets_r.data["results"][0]["month"], None)
        self.assertEqual(
            float(budgets_r.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            budgets_r.data["results"][0]["allocated_amount"], None
        )

        # Tries to update the year to an invalid one
        # (we force the year to be equal or more than 1970)
        b_id = budgets_r.data["results"][0]["id"]
        update_data = {"year": 1969}

        res = self.update_budget(b_id, update_data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        if "year" in res.data:
            self.fail(
                "Budget update body has returned 'year' field."
                " Has the spec changed? Fail!"
            )

        res = self.client.get(url, format="json")
        self.assertEqual(res.data["count"], 1)
        self.assertEqual(len(res.data["results"]), 1)
        self.assertEqual(
            res.data["results"][0]["category"]["id"], self.category.id
        )
        self.assertEqual(res.data["results"][0]["year"], data["year"])
        self.assertEqual(res.data["results"][0]["month"], None)
        self.assertEqual(
            float(res.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            budgets_r.data["results"][0]["allocated_amount"], None
        )

    # Partial duplicate of test_can_not_alter_month_on_update_for_budgets_with_category()
    def test_can_not_update_budgets_with_category_to_have_an_invalid_month(
        self,
    ):
        self.client.force_login(self.user)
        url = reverse("api:v1:budgets")

        # Create a Budget with a Category for a given year (i.e. no month)
        res, data = self.create_budget(
            category_id=self.category.id,
            savingcategory_id=None,
            month=-1,
            data=None,
        )
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        budgets_r = self.client.get(url, format="json")
        self.assertEqual(budgets_r.data["count"], 1)
        self.assertEqual(len(budgets_r.data["results"]), 1)
        self.assertEqual(
            budgets_r.data["results"][0]["category"]["id"], self.category.id
        )
        self.assertEqual(budgets_r.data["results"][0]["year"], data["year"])
        self.assertEqual(budgets_r.data["results"][0]["month"], None)
        self.assertEqual(
            float(budgets_r.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            budgets_r.data["results"][0]["allocated_amount"], None
        )

        # Tries to update the month to an invalid value
        b_id = budgets_r.data["results"][0]["id"]
        update_data = {"month": 42}

        res = self.update_budget(b_id, update_data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        if "month" in res.data:
            self.fail(
                "Budget update body has returned 'month' field."
                " Has the spec changed? Fail!"
            )

        res = self.client.get(url, format="json")
        self.assertEqual(res.data["count"], 1)
        self.assertEqual(len(res.data["results"]), 1)
        self.assertEqual(
            res.data["results"][0]["category"]["id"], self.category.id
        )
        self.assertEqual(res.data["results"][0]["year"], data["year"])
        self.assertEqual(res.data["results"][0]["month"], None)
        self.assertEqual(
            float(res.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            budgets_r.data["results"][0]["allocated_amount"], None
        )

    # Partial duplicated of test_can_not_alter_month_on_update_for_budgets_with_savingcategory()
    def test_can_not_update_budgets_with_savingcategory_to_have_an_invalid_month(
        self,
    ):
        self.client.force_login(self.user)
        url = reverse("api:v1:budgets")

        # Create a Budget with a savingcategory for a given year i.e. no month
        res, data = self.create_budget(
            category_id=None,
            savingcategory_id=self.savingcategory.id,
            month=-1,
            data=None,
        )
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        budgets_r = self.client.get(url, format="json")
        self.assertEqual(budgets_r.data["count"], 1)
        self.assertEqual(len(budgets_r.data["results"]), 1)
        self.assertEqual(
            budgets_r.data["results"][0]["savingcategory"]["id"],
            self.savingcategory.id,
        )
        self.assertEqual(budgets_r.data["results"][0]["year"], data["year"])
        self.assertEqual(budgets_r.data["results"][0]["month"], None)
        self.assertEqual(
            float(budgets_r.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            float(budgets_r.data["results"][0]["allocated_amount"]),
            data["allocated_amount"],
        )

        # Tries to update the month to an invalid one
        b_id = budgets_r.data["results"][0]["id"]
        update_data = {"month": 13}

        res = self.update_budget(b_id, update_data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        if "month" in res.data:
            self.fail(
                "Budget update body has returned 'month' field."
                " Has the spec changed? Fail!"
            )

        res = self.client.get(url, format="json")
        self.assertEqual(res.data["count"], 1)
        self.assertEqual(len(res.data["results"]), 1)
        self.assertEqual(
            res.data["results"][0]["savingcategory"]["id"],
            self.savingcategory.id,
        )
        self.assertEqual(res.data["results"][0]["year"], data["year"])
        self.assertEqual(res.data["results"][0]["month"], None)
        self.assertEqual(
            float(budgets_r.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            float(budgets_r.data["results"][0]["allocated_amount"]),
            data["allocated_amount"],
        )

    def test_can_not_update_budgets_with_category_to_have_an_invalid_amount(
        self,
    ):
        self.client.force_login(self.user)
        url = reverse("api:v1:budgets")

        # Create a Budget with a Category for a given year (i.e. no month)
        res, data = self.create_budget(
            category_id=self.category.id,
            savingcategory_id=None,
            month=-1,
            data=None,
        )
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        budgets_r = self.client.get(url, format="json")
        self.assertEqual(budgets_r.data["count"], 1)
        self.assertEqual(len(budgets_r.data["results"]), 1)
        self.assertEqual(
            budgets_r.data["results"][0]["category"]["id"], self.category.id
        )
        self.assertEqual(budgets_r.data["results"][0]["year"], data["year"])
        self.assertEqual(budgets_r.data["results"][0]["month"], None)
        self.assertEqual(
            float(budgets_r.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            budgets_r.data["results"][0]["allocated_amount"], None
        )

        # Tries to update with an invalid amount
        b_id = budgets_r.data["results"][0]["id"]
        update_data = {"amount": -42}

        res = self.update_budget(b_id, update_data)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = "-42.000 should be more than 0."
        self.assertEqual(str(res.data["amount"][0]), msg)
        self.assertEqual(res.data["amount"][0].code, "invalid")

        res = self.client.get(url, format="json")
        self.assertEqual(res.data["count"], 1)
        self.assertEqual(len(res.data["results"]), 1)
        self.assertEqual(
            res.data["results"][0]["category"]["id"], self.category.id
        )
        self.assertEqual(res.data["results"][0]["year"], data["year"])
        self.assertEqual(res.data["results"][0]["month"], None)
        self.assertEqual(
            float(res.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            budgets_r.data["results"][0]["allocated_amount"], None
        )

    def test_can_not_update_budgets_with_savingcategory_to_have_an_invalid_amount(
        self,
    ):
        self.client.force_login(self.user)
        url = reverse("api:v1:budgets")

        # Create a Budget with a savingcategory for a given year i.e. no month
        res, data = self.create_budget(
            category_id=None,
            savingcategory_id=self.savingcategory.id,
            month=-1,
            data=None,
        )
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        budgets_r = self.client.get(url, format="json")
        self.assertEqual(budgets_r.data["count"], 1)
        self.assertEqual(len(budgets_r.data["results"]), 1)
        self.assertEqual(
            budgets_r.data["results"][0]["savingcategory"]["id"],
            self.savingcategory.id,
        )
        self.assertEqual(budgets_r.data["results"][0]["year"], data["year"])
        self.assertEqual(budgets_r.data["results"][0]["month"], None)
        self.assertEqual(
            float(budgets_r.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            float(budgets_r.data["results"][0]["allocated_amount"]),
            data["allocated_amount"],
        )

        # Tries to update the year to an invalid one
        # (we force the year to be equal or more than 1970)
        b_id = budgets_r.data["results"][0]["id"]
        update_data = {"amount": -42}

        res = self.update_budget(b_id, update_data)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = "-42.000 should be more than 0."
        self.assertEqual(str(res.data["amount"][0]), msg)
        self.assertEqual(res.data["amount"][0].code, "invalid")

        res = self.client.get(url, format="json")
        self.assertEqual(res.data["count"], 1)
        self.assertEqual(len(res.data["results"]), 1)
        self.assertEqual(
            res.data["results"][0]["savingcategory"]["id"],
            self.savingcategory.id,
        )
        self.assertEqual(res.data["results"][0]["year"], data["year"])
        self.assertEqual(res.data["results"][0]["month"], None)
        self.assertEqual(
            float(budgets_r.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            float(budgets_r.data["results"][0]["allocated_amount"]),
            data["allocated_amount"],
        )

    def test_can_not_update_budgets_with_savingcategory_to_have_an_invalid_savingamount(
        self,
    ):
        self.client.force_login(self.user)
        url = reverse("api:v1:budgets")

        # Create a Budget with a savingcategory for a given year i.e. no month
        res, data = self.create_budget(
            category_id=None,
            savingcategory_id=self.savingcategory.id,
            month=-1,
            data=None,
        )
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        budgets_r = self.client.get(url, format="json")
        self.assertEqual(budgets_r.data["count"], 1)
        self.assertEqual(len(budgets_r.data["results"]), 1)
        self.assertEqual(
            budgets_r.data["results"][0]["savingcategory"]["id"],
            self.savingcategory.id,
        )
        self.assertEqual(budgets_r.data["results"][0]["year"], data["year"])
        self.assertEqual(budgets_r.data["results"][0]["month"], None)
        self.assertEqual(
            float(budgets_r.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            float(budgets_r.data["results"][0]["allocated_amount"]),
            data["allocated_amount"],
        )

        # Tries to update the year to an invalid one
        # (we force the year to be equal or more than 1970)
        b_id = budgets_r.data["results"][0]["id"]
        update_data = {"allocated_amount": -42}

        res = self.update_budget(b_id, update_data)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = "-42.000 should be more than 0."
        self.assertEqual(str(res.data["allocated_amount"][0]), msg)
        self.assertEqual(res.data["allocated_amount"][0].code, "invalid")

        res = self.client.get(url, format="json")
        self.assertEqual(res.data["count"], 1)
        self.assertEqual(len(res.data["results"]), 1)
        self.assertEqual(
            res.data["results"][0]["savingcategory"]["id"],
            self.savingcategory.id,
        )
        self.assertEqual(res.data["results"][0]["year"], data["year"])
        self.assertEqual(res.data["results"][0]["month"], None)
        self.assertEqual(
            float(budgets_r.data["results"][0]["amount"]), data["amount"]
        )
        self.assertEqual(
            float(budgets_r.data["results"][0]["allocated_amount"]),
            data["allocated_amount"],
        )

    @patch("api.v1.views.budget_views.BudgetList")
    @patch("budgets.serializers.BudgetSerializer")
    def test_integrity_errors_are_catched_on_create(
        self, mock_cat_serializer, _
    ):
        self.client.force_login(self.user)
        res, data = self.create_budget(
            category_id=self.category.id,
            savingcategory_id=None,
            month=random.randint(1, 12),
            data=None,
        )
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        # Force the serializer to Throw an Integrity error
        serializer = mock_cat_serializer.return_value
        msg = "I AM A MOCKED INTEGRITY ERROR. I HOPE YOU CATCH ME: ROAAAARR!"
        serializer.save.side_effect = IntegrityError(msg)

        # Create a mock request
        ok_data = {
            "year": random.randint(1970, 2050),
            # Amount should generally be more than allocated_amount
            # (unless a user overbudegts for it)
            "category": self.category.id,
            "amount": round(random.uniform(43, 500), 3),
        }

        # Create an instance of the view
        view = BudgetList()
        request = self.client.post(reverse("api:v1:budgets"), ok_data)

        # Set the request user
        view.request = request
        request.user = self.user

        # Confirm that the mocks works, and that ok_data actually thew an
        # exception  and confirm it was correctly handled by the backend
        failed_res = view.perform_create(serializer)
        self.assertEqual(
            failed_res.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR
        )
        msg = (
            "Generic error thrown while creating the Budget. "
            "Refer to the web container logs if you're the admin."
        )
        self.assertEqual(failed_res.data["non_field_errors"][0], msg)

    @patch("api.v1.views.budget_views.BudgetDetail")
    @patch("budgets.serializers.BudgetSerializerAllowOnlyAmountsUpdate")
    def test_integrity_errors_are_catched_on_update(
        self, moc_exp_serializer, _
    ):
        self.client.force_login(self.user)
        res, data = self.create_budget(
            category_id=self.category.id,
            savingcategory_id=None,
            month=-1,
            data=None,
        )
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        url = reverse("api:v1:budgets")
        budgets_r = self.client.get(url, format="json")
        self.assertEqual(budgets_r.data["count"], 1)
        self.assertEqual(len(budgets_r.data["results"]), 1)
        b_id = budgets_r.data["results"][0]["id"]

        # Update goes in correctly
        new_amount = round(random.uniform(501, 600), 3)
        update_data = {"amount": new_amount}
        res = self.update_budget(b_id, update_data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)

        # Force the serializer to Throw an Integrity error
        serializer = moc_exp_serializer.return_value
        msg = "I AM A MOCKED INTEGRITY ERROR. I HOPE YOU CATCH ME: ROAAAARR!"
        serializer.save.side_effect = IntegrityError(msg)

        # Create a mock request
        ok_data = {"amount": new_amount + 1}
        res = self.update_budget(b_id, update_data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)

        # Create an instance of the class view
        view = BudgetDetail()
        request = self.client.patch(
            reverse("api:v1:budget", kwargs={"pk": b_id}), ok_data
        )

        # Set the request user
        request.user = self.user
        # Mock the put request
        view.request = request
        view.request.method = "PACTH"

        # Confirm that the mocks works, and that ok_data actually thew an
        # exception and confirm it was correctly handled by the backend
        failed_res = view.perform_update(serializer)
        self.assertEqual(
            failed_res.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR
        )
        msg = (
            "Generic error thrown while updating the Budget. "
            "Refer the web container logs if you're the admin"
        )
        self.assertEqual(failed_res.data["non_field_errors"][0], msg)
