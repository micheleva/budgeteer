import random
from datetime import datetime, timedelta
from unittest import skip

from rest_framework import status
from rest_framework.reverse import reverse
from django.contrib.auth.models import User

from api.v1.tests.test_base import BaseApiTest
from budgets.models import Obligation, ObligationPaymentChange


class ObligationPaymentChangeTest(BaseApiTest):
    """
    Test ObligationPaymentChange model related v1 rest api behaviour
    """

    def setUp(self):  # pylint: disable=C0103; # noqa
        # Instrument the unit tests to fail if they run for too long
        super().setUp()
        # fmt: off
        self.user = User.objects.create_user(
            username=BaseApiTest.generate_string(20),
            password=BaseApiTest.generate_string(20),
        )
        self.user2 = User.objects.create_user(
            username=BaseApiTest.generate_string(20),
            password=BaseApiTest.generate_string(20),
        )
        # fmt: on
        self.client.force_login(self.user)
        # fmt: off
        current_year = datetime.now().year

        start_date = f"{current_year}-01-01"
        end_date = f"{current_year}-12-31"
        self.obligation = Obligation.objects.create(  # pylint: disable=E1101; # noqa
            name=BaseApiTest.generate_string(20),
            total_amount="1000.00",
            obligation_type="variable",
            monthly_payment="100.00",
            start_date=start_date,
            payment_day=15,
            end_date=end_date,
            created_by=self.user,
        )
        # fmt: on

    def generate_valid_change_date(self):
        start_date = datetime.strptime(self.obligation.start_date, "%Y-%m-%d")
        end_date = datetime.strptime(self.obligation.end_date, "%Y-%m-%d")
        return start_date + timedelta(
            days=random.randint(1, (end_date - start_date).days)
        )

    def create_obligation_payment_change(self):
        create_url = reverse("api:v1:obligationpaymentchanges")
        change_date = self.generate_valid_change_date()
        data = {
            "obligation": self.obligation.id,
            "new_monthly_payment": str(BaseApiTest.generate_number()),
            "change_date": change_date.strftime("%Y-%m-%d"),
        }
        return (self.client.post(create_url, data, format="json"), data)

    @skip
    def test_can_not_create_payment_change_for_fixed_obligation(self):
        # TODO: Test already existing change date
        self.fail("implement me")

    def test_create_and_list_obligation_payment_changes(self):
        """
        Ensure we can create a new obligation payment change and list it
        """
        # Create an obligation payment change
        self.client.force_login(self.user)
        change_response, data = self.create_obligation_payment_change()
        print(f"change_response.data: {data}", flush=True)
        self.assertEqual(change_response.status_code, status.HTTP_201_CREATED)
        change_id = change_response.data["id"]

        # List obligation payment changes
        list_url = reverse("api:v1:obligationpaymentchanges")
        response = self.client.get(list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["id"], change_id)
        self.assertEqual(len(response.data["results"]), 1)

    def test_create_obligation_payment_change_invalid_data(self):
        """
        Ensure we cannot create an obligation payment change with invalid data
        """
        create_url = reverse("api:v1:obligationpaymentchanges")
        valid_data = {
            "obligation": self.obligation.id,
            "new_monthly_payment": "200.00",
            "change_date": "2023-06-01",
        }

        # Test invalid 'obligation'
        invalid_obligations = [None, "", "invalid", 99999]
        for invalid_obligation in invalid_obligations:
            malformed_data = self.modify_data(
                valid_data, "obligation", invalid_obligation
            )
            response = self.client.post(
                create_url, malformed_data, format="json"
            )
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertIn("obligation", response.data)

        # Test invalid 'new_monthly_payment'
        invalid_payments = [None, "", "invalid", -100, "100.00.00"]
        for invalid_payment in invalid_payments:
            malformed_data = self.modify_data(
                valid_data, "new_monthly_payment", invalid_payment
            )
            response = self.client.post(
                create_url, malformed_data, format="json"
            )
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertIn("new_monthly_payment", response.data)

        # Test invalid 'change_date'
        invalid_dates = [None, "", 123, 45.67, "2023/01/01", "invalid"]
        for invalid_date in invalid_dates:
            malformed_data = self.modify_data(
                valid_data, "change_date", invalid_date
            )
            response = self.client.post(
                create_url, malformed_data, format="json"
            )
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertIn("change_date", response.data)

        # Test 'change_date' after the end date of the obligation
        invalid_change_date = (
            datetime.strptime(self.obligation.end_date, "%Y-%m-%d")
            + timedelta(days=1)
        ).strftime("%Y-%m-%d")
        malformed_data = self.modify_data(
            valid_data, "change_date", invalid_change_date
        )
        response = self.client.post(create_url, malformed_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn("non_field_errors", response.data)

    def test_update_obligation_payment_change(
        self,
    ):  # Refactor me out to complete #46
        """
        Ensure we can update an obligation payment change
        """
        print(
            "test_update_obligation_payment_change is flaky."
            "See https://gitlab.com/micheleva/budgeteer/-/jobs/9102890135"
            " Address it to close #46"
        )
        change_response, data = self.create_obligation_payment_change()
        # print(f"change_response.data: {change_response.data}", flush=True)
        if "id" not in change_response.data:
            # Helps to debug the flaky test
            print(
                "==========ID MISSING!====================="
                "Agumenting the output of api.v1.tests.test_obligation_paym"
                "ent_changes.ObligationPaymentChangeTest.test_update_obligat"
                "ion_payment_change to debug the flaky test",
                flush=True,
            )
            print(f"change_response.data: {change_response.data}", flush=True)
            print(f"data: {data}", flush=True)

        change_id = change_response.data["id"]
        update_url = reverse(
            "api:v1:obligationpaymentchange_detail", args=[change_id]
        )

        updated_data = data.copy()
        new_amount = f"{BaseApiTest.generate_number():.2f}"
        # Ensure new_amount has two decimal places to match the endpoint
        # response format
        while new_amount == updated_data["new_monthly_payment"]:
            new_amount = f"{BaseApiTest.generate_number():.2f}"
            updated_data["new_monthly_payment"] = new_amount

        # Ensure new_amount has two decimal places to match the endpoint
        if (
            "." in updated_data["new_monthly_payment"]
            and len(updated_data["new_monthly_payment"].split(".")[1]) < 2
        ):
            updated_data["new_monthly_payment"] += "0"

        # Calculate the diff of days between the obligation.start_date and the
        # obligation.end_date
        diff_days = (
            datetime.strptime(self.obligation.end_date, "%Y-%m-%d")
            - datetime.strptime(self.obligation.start_date, "%Y-%m-%d")
        ).days

        # Generate a new valid change_date different from the original one
        # **AND** between the obligation.start_date and the obligation.end_date
        new_change_date = (
            datetime.strptime(self.obligation.start_date, "%Y-%m-%d")
            + timedelta(days=random.randint(1, diff_days))
        ).strftime("%Y-%m-%d")

        while (
            new_change_date == updated_data["change_date"]
            or new_change_date <= self.obligation.start_date
            or new_change_date >= self.obligation.end_date
        ):
            new_change_date = BaseApiTest.generate_date(120, 30).strftime(
                "%Y-%m-%d"
            )
            print(
                f"It seems the new_change_date ({new_change_date}) is not valid",
                flush=True,
            )

        updated_data["change_date"] = new_change_date

        response = self.client.put(update_url, updated_data, format="json")
        if response.status_code != status.HTTP_200_OK:
            # Helps to debug the flaky test
            print(
                "Agumenting the output of api.v1.tests.test_obligation_paym"
                "ent_changes.ObligationPaymentChangeTest.test_update_obligat"
                "ion_payment_change to debug the flaky test",
                flush=True,
            )
            print(f"response data: {response.data}", flush=True)
            print(f"updated (posted) data: {updated_data}", flush=True)
            print(
                f"self.obligation.start_date: {self.obligation.start_date}",
                flush=True,
            )
            print(f"original data: {data}", flush=True)

        self.assertEqual(
            response.status_code, status.HTTP_200_OK
        )  # <== Originally Flaky line
        self.assertEqual(
            response.data["new_monthly_payment"],
            updated_data["new_monthly_payment"],
        )

    def test_update_obligation_payment_change_with_same_change_date(self):
        """
        Ensure we can update an obligation payment change keeping the same chage date
        This is to make sure we're enforcing the unique_together constraint, but
        also to make sure we can update the instance without changing the change_date
        """
        change_response, data = self.create_obligation_payment_change()
        change_id = change_response.data["id"]

        update_url = reverse(
            "api:v1:obligationpaymentchange_detail", args=[change_id]
        )

        # Update only the amount, and keep the same change_date
        updated_data = data.copy()
        new_amount = f"{BaseApiTest.generate_number():.2f}"
        # Since the endpoint returns the amount with 2 decimal places, we need to have
        # two decimal places in the new_amount to make sure self.assertEqual works
        # Otherwise when the new_amount has only one decimal place, the self.assertEqual will fail
        # as it will compare X.0 with X.00
        while new_amount == data["new_monthly_payment"]:
            new_amount = f"{BaseApiTest.generate_number():.2f}"
        updated_data["new_monthly_payment"] = new_amount

        response = self.client.put(update_url, updated_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["new_monthly_payment"], new_amount)

    def test_delete_obligation_payment_change(self):
        """
        Ensure we can delete an obligation payment change
        """
        change_response, _ = self.create_obligation_payment_change()
        change_id = change_response.data["id"]

        delete_url = reverse(
            "api:v1:obligationpaymentchange_detail", args=[change_id]
        )
        response = self.client.delete(delete_url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        # fmt: off
        self.assertFalse(
            ObligationPaymentChange.objects.filter(  # pylint: disable=E1101; # noqa
                id=change_id
            ).exists()
        )
        # fmt: on

    def test_retrieve_obligation_payment_change(self):
        """
        Ensure we can retrieve an obligation payment change
        """
        change_response, _ = self.create_obligation_payment_change()
        change_id = change_response.data["id"]

        retrieve_url = reverse(
            "api:v1:obligationpaymentchange_detail", args=[change_id]
        )
        response = self.client.get(retrieve_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["id"], change_id)
        self.assertEqual(response.data["obligation"], self.obligation.id)

    # Flickery test
    def test_permissions(self):
        """
        Ensure only the creator can update or delete the obligation payment
        change
        """
        self.client.force_login(self.user)
        change_response, _ = self.create_obligation_payment_change()
        # ERROR: test_permissions (api.v1.tests.test_obligation_payment_changes.ObligationPaymentChangeTest.test_permissions)
        # Ensure only the creator can update or delete the obligation payment
        # ----------------------------------------------------------------------
        # Traceback (most recent call last):
        #   File "/usr/lib64/python3.12/unittest/case.py", line 58, in testPartExecutor
        #     yield
        #   File "/usr/lib64/python3.12/unittest/case.py", line 634, in run
        #     self._callTestMethod(testMethod)
        #     ^^^^^^^^^^^^^^^^^
        #   File "/usr/lib64/python3.12/unittest/case.py", line 589, in _callTestMethod
        #     if method() is not None:
        #     ^^^^^^^^^^^^^^^^^
        #   File "xxx/api/v1/tests/test_obligation_payment_changes.py", line 261, in test_permissions
        #     change_id = change_response.data["id"]
        #     ^^^^^^^^^^^^^^^^^
        # KeyError: 'id'

        change_id = change_response.data["id"]

        # Another user trying to update the obligation payment change
        self.client.force_login(self.user2)
        update_url = reverse(
            "api:v1:obligationpaymentchange_detail", args=[change_id]
        )
        response = self.client.put(
            update_url, {"new_monthly_payment": "500.00"}, format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # Another user trying to delete the obligation payment change
        response = self.client.delete(update_url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_update_obligation_payment_change_invalid_data(self):
        """
        Ensure we cannot update an obligation payment change with invalid data
        """
        change_response, data = self.create_obligation_payment_change()
        change_id = change_response.data["id"]
        update_url = reverse(
            "api:v1:obligationpaymentchange_detail", args=[change_id]
        )

        valid_data = data.copy()

        # Test invalid 'obligation'
        invalid_obligations = [None, "", "invalid", 99999]
        for invalid_obligation in invalid_obligations:
            malformed_data = self.modify_data(
                valid_data, "obligation", invalid_obligation
            )
            response = self.client.put(
                update_url, malformed_data, format="json"
            )
            self.assertEqual(response.status_code, status.HTTP_200_OK)
            self.assertIn("obligation", response.data)
            self.assertEqual(response.data["obligation"], data["obligation"])

        # Test invalid 'new_monthly_payment'
        invalid_payments = [None, "", "invalid", -100, "100.00.00"]
        for invalid_payment in invalid_payments:
            malformed_data = self.modify_data(
                valid_data, "new_monthly_payment", invalid_payment
            )
            response = self.client.put(
                update_url, malformed_data, format="json"
            )
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertIn("new_monthly_payment", response.data)

        # Test invalid 'change_date'
        invalid_dates = [None, "", 123, 45.67, "2023/01/01", "invalid"]
        for invalid_date in invalid_dates:
            malformed_data = self.modify_data(
                valid_data, "change_date", invalid_date
            )
            response = self.client.put(
                update_url, malformed_data, format="json"
            )
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertIn("change_date", response.data)

        # Test 'change_date' after the end date of the obligation
        invalid_change_date = (
            datetime.strptime(self.obligation.end_date, "%Y-%m-%d")
            + timedelta(days=1)
        ).strftime("%Y-%m-%d")
        malformed_data = self.modify_data(
            valid_data, "change_date", invalid_change_date
        )
        response = self.client.put(update_url, malformed_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertIn("non_field_errors", response.data)

    @skip
    def test_update_obligation_payment_change_with_already_existing_data(self):
        # TODO: Confirm it fails if we try to edit a second payment change object, to have the same date as the second one, on the same obligaiton obj
        self.fail("implement me")
