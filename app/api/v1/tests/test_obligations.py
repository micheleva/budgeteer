import random

from rest_framework import status
from rest_framework.reverse import reverse
from django.contrib.auth.models import User

from api.v1.tests.test_base import BaseApiTest
from budgets.models import Obligation, ObligationPaymentChange


class ObligationTest(BaseApiTest):
    """
    Test Obligation model related v1 rest api behaviour
    """

    def setUp(self):  # pylint: disable=C0103; # noqa
        # Instrument the unit tests to fail if they run for too long
        super().setUp()
        # fmt: off
        self.user = User.objects.create_user(
            username=BaseApiTest.generate_string(20),
            password=BaseApiTest.generate_string(20),
        )
        self.user2 = User.objects.create_user(
            username=BaseApiTest.generate_string(20),
            password=BaseApiTest.generate_string(20),
        )
        # fmt: on
        self.client.force_login(self.user)

    def create_obligation(self):
        create_url = reverse("api:v1:obligations")
        # Between 2 years ago and 1 year ago
        start_date = BaseApiTest.generate_date(730, 365)
        # Within the last year
        end_date = BaseApiTest.generate_date(364, 0)
        data = {
            "name": BaseApiTest.generate_string(20),
            "total_amount": f"{BaseApiTest.generate_number():.2f}",
            "obligation_type": "regular",
            "monthly_payment": f"{BaseApiTest.generate_number():.2f}",
            "start_date": start_date.strftime("%Y-%m-%d"),
            "payment_day": random.randint(1, 28),
            "end_date": end_date.strftime("%Y-%m-%d"),
        }
        # Make sure the monthly_payment is smaller than total_amount
        while data["monthly_payment"] >= data["total_amount"]:
            # data['monthly_payment'] = f"{float(data['monthly_payment']) - 1:.2f}"
            data["total_amount"] = f"{BaseApiTest.generate_number():.2f}"
            data["monthly_payment"] = f"{BaseApiTest.generate_number():.2f}"
            # If the values are too little, generate new random values
            if (
                float(data["monthly_payment"]) <= 1
                or float(data["total_amount"]) <= 1
            ):
                data["total_amount"] = f"{BaseApiTest.generate_number():.2f}"
                data[
                    "monthly_payment"
                ] = f"{BaseApiTest.generate_number():.2f}"
        return (self.client.post(create_url, data, format="json"), data)

    def test_create_and_list_obligations_and_other_user_can_not_see_them(self):
        """
        Ensure we can create a new obligation object and other users
        can't see it
        """
        # Create an obligation
        self.client.force_login(self.user)
        obligation_response, _ = self.create_obligation()
        self.assertEqual(
            obligation_response.status_code, status.HTTP_201_CREATED
        )
        obligation_id = obligation_response.data["id"]

        # List obligations
        list_url = reverse("api:v1:obligations")
        response = self.client.get(list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["id"], obligation_id)
        self.assertEqual(len(response.data["results"]), 1)

        # Another user CANNOT see that obligation
        self.client.force_login(self.user2)
        response = self.client.get(list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 0)

    def test_create_obligation_invalid_data(self):
        """
        Ensure we cannot create an obligation with invalid data
        """
        self.client.force_login(self.user)
        create_url = reverse("api:v1:obligations")
        valid_data = {
            "name": "Valid Name",
            "total_amount": "1000.00",
            "obligation_type": "regular",
            "monthly_payment": "100.00",
            "start_date": "2023-01-01",
            "payment_day": 15,
            "end_date": "2023-12-31",
        }

        # Test without required fields
        fields = [
            "name",
            "monthly_payment",
            "start_date",
            "payment_day",
            "end_date",
        ]
        msg = "This field is required."
        for field in fields:
            malformed_data = self.modify_data(
                valid_data, field, remove_field=True
            )
            response = self.client.post(
                create_url, malformed_data, format="json"
            )
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertIn(field, response.data)
            self.assertEqual(response.data[field][0], msg)

        # Test with invalid values
        invalid_data_cases = {
            "total_amount": "invalid",
            "obligation_type": "invalid",
            "monthly_payment": "invalid",
            "payment_day": 32,
            "end_date": "2022-01-01",  # End date before start date
        }
        for field, invalid_value in invalid_data_cases.items():
            malformed_data = self.modify_data(valid_data, field, invalid_value)
            response = self.client.post(
                create_url, malformed_data, format="json"
            )
            self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
            if field == "end_date":
                field = "non_field_errors"
            self.assertIn(field, response.data)

    def test_update_obligation(self):
        """
        Ensure we can update an obligation
        """
        self.client.force_login(self.user)
        obligation_response, data = self.create_obligation()
        obligation_id = obligation_response.data["id"]

        update_url = reverse("api:v1:obligation_detail", args=[obligation_id])
        updated_data = data.copy()
        updated_data["name"] = "Updated Obligation Name"

        response = self.client.patch(update_url, updated_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["name"], "Updated Obligation Name")

    def test_delete_obligation(self):
        """
        Ensure we can delete an obligation
        """
        self.client.force_login(self.user)
        obligation_response, _ = self.create_obligation()
        obligation_id = obligation_response.data["id"]

        delete_url = reverse("api:v1:obligation_detail", args=[obligation_id])
        response = self.client.delete(delete_url)
        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        # fmt: off
        self.assertFalse(
            Obligation.objects.filter(id=obligation_id).exists()  # pylint: disable=E1101; # noqa
        )
        # fmt: on

    def test_retrieve_obligation(self):
        """
        Ensure we can retrieve an obligation and its changes
        """
        self.client.force_login(self.user)
        obligation_response, _ = self.create_obligation()
        obligation_id = obligation_response.data["id"]

        # fmt: off
        change = ObligationPaymentChange.objects.create(  # pylint: disable=E1101; # noqa
            obligation_id=obligation_id,
            new_monthly_payment=f"{BaseApiTest.generate_number():.2f}",
            change_date=BaseApiTest.generate_date(365, 0).strftime("%Y-%m-%d"),
        )
        # fmt: on

        retrieve_url = reverse(
            "api:v1:obligation_detail", args=[obligation_id]
        )
        response = self.client.get(retrieve_url)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["id"], obligation_id)
        self.assertEqual(
            response.data["changes"][0]["new_monthly_payment"],
            change.new_monthly_payment,
        )

    def test_permissions(self):
        """
        Ensure only the creator can view, update or delete the obligation
        """
        obligation_response, _ = self.create_obligation()
        obligation_id = obligation_response.data["id"]

        # Another user trying to update the obligation
        self.client.force_login(self.user2)
        update_url = reverse("api:v1:obligation_detail", args=[obligation_id])
        response = self.client.patch(
            update_url, {"name": "New Name"}, format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

        # Another user trying to delete the obligation
        response = self.client.delete(update_url)
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
