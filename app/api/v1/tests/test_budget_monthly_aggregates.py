# Copyright: (c) 2021, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
import logging

from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status

import budgets.models as m

from api.v1.tests.test_base import BaseApiTest


logger = logging.getLogger(__name__)


class MonthlyBudgetSumAPITest(BaseApiTest):
    def setUp(self):
        # Instrument the tests to fail if they take too long
        super().setUp()
        # fmt: off
        self.user = User.objects.create_user(  # pylint: disable=E1101; # noqa
            username="testuser", password="testpass"
        )
        self.category = m.Category.objects.create(  # pylint: disable=E1101; # noqa
            text="Cat1", created_by=self.user
        )
        self.other_user = User.objects.create_user(  # pylint: disable=E1101; # noqa
            username="otheruser", password="otherpass"
        )
        self.other_category = m.Category.objects.create(  # pylint: disable=E1101; # noqa
            text="Cat1_OtherUser",
            created_by=self.other_user,
        )
        # fmt: on

    def create_budget(self, category, year, month, amount, user):
        if category.created_by.id != user.id:
            self.fail(
                "Wrong usage! You are passing a category belonging"
                "to another user!"
            )
        # fmt: off
        return m.Budget.objects.create(  # pylint: disable=E1101; # noqa
            category=category,
            created_by=user,
            year=year,
            month=month,
            amount=amount,
        )
        # fmt: on

    def test_no_budgets_for_user(self):
        self.client.force_login(self.user)
        url = reverse("api:v1:budget-yearly-aggregate", kwargs={"year": 2023})
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {"results": [], "count": 0})

    def test_sum_amounts_for_year(self):
        self.create_budget(self.category, 2023, 1, 100, self.user)
        self.create_budget(self.category, 2023, 2, 150, self.user)
        # fmt: off
        cat2 = m.Category.objects.create(  # pylint: disable=E1101; # noqa
            text="Cat2", created_by=self.user
        )
        # fmt: on
        self.create_budget(cat2, 2023, 1, 50, self.user)

        self.client.force_login(self.user)
        url = reverse("api:v1:budget-yearly-aggregate", kwargs={"year": 2023})
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        expected_data = [
            {"category": self.category.id, "sum_amount": 250},
            {"category": cat2.id, "sum_amount": 50},
        ]
        self.assertEqual(response.data["results"], expected_data)
        self.assertEqual(response.data["count"], 2)

    def test_non_integer_year_returns_404(self):
        self.client.force_login(self.user)
        url = "api/v1/budgets/yearly-aggregate/invalid"
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_aggregate_separately_for_multiple_categories(self):
        self.create_budget(self.category, 2023, 1, 100, self.user)
        # fmt: off
        cat2 = m.Category.objects.create(  # pylint: disable=E1101; # noqa
            text="Cat2", created_by=self.user
        )
        # fmt: on
        self.create_budget(cat2, 2023, 1, 150, self.user)

        self.client.force_login(self.user)
        url = reverse("api:v1:budget-yearly-aggregate", kwargs={"year": 2023})
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        expected_data = [
            {"category": self.category.id, "sum_amount": 100},
            {"category": cat2.id, "sum_amount": 150},
        ]
        self.assertEqual(response.data["results"], expected_data)
        self.assertEqual(response.data["count"], 2)

    def test_no_items_for_other_users(self):
        self.create_budget(self.other_category, 2023, 1, 75, self.other_user)

        self.client.force_login(self.user)
        url = reverse("api:v1:budget-yearly-aggregate", kwargs={"year": 2023})
        response = self.client.get(url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {"results": [], "count": 0})

    def test_budgets_for_categories_of_other_users_do_not_appear(self):
        self.client.force_login(self.user)
        self.create_budget(self.category, 2023, 1, 75, self.user)
        self.create_budget(self.other_category, 2023, 1, 100, self.other_user)

        url = reverse("api:v1:budget-yearly-aggregate", kwargs={"year": 2023})
        response = self.client.get(url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        expected_data = [{"category": self.category.id, "sum_amount": 75}]
        self.assertEqual(response.data["results"], expected_data)
        self.assertEqual(response.data["count"], 1)
