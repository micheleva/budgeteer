# Copyright: (c) 2021, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from datetime import date
from datetime import timedelta
from decimal import Decimal
import logging
import random
from unittest.mock import patch
from unittest import skip
from urllib.parse import urlencode

from django.contrib.auth.models import User
from django.db.utils import IntegrityError
from django.urls import reverse
from rest_framework import status

from api.v1.tests.test_base import BaseApiTest
from api.v1.views.asset_views import AssetsCreate

from budgets.models import Account
from budgets.tests.base import BaseTest


logger = logging.getLogger(__name__)


def _create_users_and_accounts():
    """
    Creates two users and two accounts.

    This function generates two users with random usernames and passwords,
    and creates two accounts associated with each user.

    Returns:
        tuple: A tuple containing the created User and Account objects in the
               following order: (user1, user2, account1, account2).
    """
    user = User.objects.create_user(
        username=BaseTest.generate_string(20),
        password=BaseTest.generate_string(20),
    )
    user2 = User.objects.create_user(
        username=BaseTest.generate_string(20),
        password=BaseTest.generate_string(20),
    )
    account = Account.objects.create(
        text="Account X",
        credit_entity="Broker 1",
        created_by=user,
    )
    account2 = Account.objects.create(
        text="Account Y",
        credit_entity="Broker 1",
        created_by=user2,
    )
    return user, user2, account, account2


class AssetTest(BaseApiTest):
    """
    Test Asset model related v1 rest api behaviour
    """

    def setUp(self):  # pylint: disable=C0103; # noqa
        # Instrument the unit tests to fail if they run for too long
        super().setUp()
        (
            self.user,
            self.user2,
            self.account,
            self.account2,
        ) = _create_users_and_accounts()

    def test_can_create_and_list_assets(self):
        self.client.force_login(self.user)

        # Create Asset linked to the above created account
        res, data = self.create_asset(self.account.id)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            Asset.objects.filter(created_by=self.user).count(), 1  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        assets_list_url = reverse("api:v1:assets")
        r = self.client.get(assets_list_url, format="json")
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(r.data["count"], 1)

        self.assertEqual(r.data["results"][0]["text"], data["text"])
        self.assertEqual(
            r.data["results"][0]["account"]["id"], self.account.id
        )
        self.assertEqual(r.data["results"][0]["amount"], data["amount"])
        self.assertEqual(
            r.data["results"][0]["foreign_currency"], data["foreign_currency"]
        )
        self.assertEqual(
            r.data["results"][0]["current_local_value"],
            data["current_local_value"],
        )
        self.assertEqual(
            r.data["results"][0]["current_foreign_value"],
            data["current_foreign_value"],
        )
        self.assertEqual(
            r.data["results"][0]["bought_price_foreign"],
            data["bought_price_foreign"],
        )
        self.assertEqual(
            r.data["results"][0]["bought_price_local"],
            data["bought_price_local"],
        )
        self.assertEqual(
            r.data["results"][0]["net_loss_local"], data["net_loss_local"]
        )
        self.assertEqual(
            r.data["results"][0]["net_loss_foreign"], data["net_loss_foreign"]
        )
        self.assertEqual(
            Decimal(r.data["results"][0]["rate"]), Decimal(data["rate"])
        )
        self.assertEqual(
            r.data["results"][0]["record_date"], data["record_date"]
        )

        res, data = self.create_asset(self.account.id)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            Asset.objects.filter(created_by=self.user).count(), 2  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        assets_list_url = reverse("api:v1:assets")
        r2 = self.client.get(assets_list_url, format="json")
        self.assertEqual(r2.status_code, status.HTTP_200_OK)
        self.assertEqual(r2.data["count"], 2)
        self.assertEqual(r2.data["results"][1]["text"], data["text"])
        self.assertEqual(
            r2.data["results"][1]["account"]["id"], self.account.id
        )
        self.assertEqual(r2.data["results"][1]["amount"], data["amount"])
        self.assertEqual(
            r2.data["results"][1]["foreign_currency"], data["foreign_currency"]
        )
        self.assertEqual(
            r2.data["results"][1]["current_local_value"],
            data["current_local_value"],
        )
        self.assertEqual(
            r2.data["results"][1]["current_foreign_value"],
            data["current_foreign_value"],
        )
        self.assertEqual(
            r2.data["results"][1]["bought_price_foreign"],
            data["bought_price_foreign"],
        )
        self.assertEqual(
            r2.data["results"][1]["bought_price_local"],
            data["bought_price_local"],
        )
        self.assertEqual(
            r2.data["results"][1]["net_loss_local"], data["net_loss_local"]
        )
        self.assertEqual(
            r2.data["results"][1]["net_loss_foreign"], data["net_loss_foreign"]
        )
        self.assertEqual(
            Decimal(r2.data["results"][1]["rate"]), Decimal(data["rate"])
        )
        self.assertEqual(
            r2.data["results"][1]["record_date"], data["record_date"]
        )

    def test_can_filter_listed_assets_by_account(self):
        self.client.force_login(self.user)

        # Create Asset linked to the above created account
        res, _ = self.create_asset(self.account.id)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        assets_list_url = reverse("api:v1:assets")
        a1_r = self.client.get(assets_list_url, format="json")
        self.assertEqual(a1_r.status_code, status.HTTP_200_OK)
        self.assertEqual(a1_r.data["count"], 1)
        # fmt: off
        other_acc = Account.objects.create(  # pylint: disable=E1101; # noqa
            text="Account Y",
            credit_entity="Broker X",
            created_by=self.user,
        )
        # fmt: on
        res, data = self.create_asset(other_acc.id)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        a2_r = self.client.get(assets_list_url, format="json")
        self.assertEqual(a2_r.status_code, status.HTTP_200_OK)
        self.assertEqual(a2_r.data["count"], 2)
        self.assertEqual(len(a2_r.data["results"]), 2)

        mb_filtered_url = (
            assets_list_url + "?" + urlencode({"account_id": self.account.id})
        )
        response = self.client.get(mb_filtered_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)
        self.assertEqual(
            response.data["results"][0]["account"]["text"], self.account.text
        )
        self.assertEqual(
            response.data["results"][0]["account"]["id"], self.account.id
        )

        mb_filtered_url = (
            assets_list_url + "?" + urlencode({"account_id": other_acc.id})
        )
        response = self.client.get(mb_filtered_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)
        self.assertEqual(
            response.data["results"][0]["account"]["text"], other_acc.text
        )
        self.assertEqual(
            response.data["results"][0]["account"]["id"], other_acc.id
        )

    def test_can_not_see_other_users_assets(self):
        self.client.force_login(self.user)

        res, _ = self.create_asset(self.account.id)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            Asset.objects.filter(created_by=self.user).count(), 1  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        assets_list_url = reverse("api:v1:assets")
        response = self.client.get(assets_list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 1)

        # User2 can not see user1's asset
        self.client.force_login(self.user2)
        assets_list_url = reverse("api:v1:assets")
        response = self.client.get(assets_list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 0)

    def test_creating_an_asset_with_a_newer_date_will_prevent_old_assets_to_be_shown(
        self,
    ):
        """
        - create an asset in the past
        - confirm the api returns it
        - create a different text asset, with a newer date
        - confirm the api does NOT return the original asset
          but only the newest one if filtering with the original asset
        """
        self.client.force_login(self.user)

        res, data = self.create_asset(self.account.id)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            Asset.objects.filter(created_by=self.user).count(), 1  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        assets_list_url = reverse("api:v1:assets")
        response = self.client.get(assets_list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(len(response.data["results"]), 1)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(
            response.data["results"][0]["record_date"], data["record_date"]
        )

        # Let's create another asset, that has a more recent date
        amount = random.randint(1, 42)
        asset_text = BaseTest.generate_string(60)
        foreign_currency = random.choice(["USD", "EUR"])
        current_foreign_value = amount * random.randint(2, 42)
        rate = random.randint(88, 160)
        current_local_value = current_foreign_value * rate
        increase_perc = random.uniform(0.1, 0.2)
        bought_price_foreign = current_foreign_value * increase_perc
        bought_price_local = current_local_value * increase_perc
        net_loss_foreign = bought_price_foreign - current_foreign_value
        net_loss_local = bought_price_local - current_foreign_value

        # Generate a random date for user2 that is not overlapping with the
        # date already registered in assets beloging to user1
        # i.e. we want to avoid this test to be flaky, and randomly fail as the
        # random.int() has returned the same values for both user1 and user2
        today = date.today()

        future_date = today + timedelta(days=random.randint(42, 365))
        new_record_date = future_date.isoformat()

        while new_record_date in response.data:
            future_date = today + timedelta(days=random.randint(42, 365))
            new_record_date = future_date.isoformat()

        future_date_asset = {
            "account": self.account.id,
            "amount": amount,
            "text": asset_text,
            "foreign_currency": foreign_currency,
            "current_local_value": current_local_value,
            "current_foreign_value": current_foreign_value,
            "bought_price_local": bought_price_local,
            "bought_price_foreign": bought_price_foreign,
            "net_loss_local": net_loss_local,
            "net_loss_foreign": net_loss_foreign,
            "rate": rate,
            "record_date": new_record_date,
        }

        res3, _ = self.create_asset_from_data(future_date_asset)
        self.assertEqual(res3.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            res3.data["record_date"], future_date_asset["record_date"]
        )

        # Confirm both assets' dates are returned
        assets_list_url = reverse("api:v1:assets_dates")
        response = self.client.get(assets_list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 2)
        self.assertIn(data["record_date"], response.data)
        self.assertIn(new_record_date, response.data)

        assets_list_url = reverse("api:v1:assets")
        response = self.client.get(assets_list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Confirm that only the asset with a more recent date is shown
        query_params = urlencode({"date": new_record_date})
        assets_list_url = f"{reverse('api:v1:assets')}?{query_params}"
        response = self.client.get(assets_list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(len(response.data["results"]), 1)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(
            response.data["results"][0]["record_date"], future_date.isoformat()
        )

    def test_can_not_create_assets_for_other_users(self):
        """
        Ensure we can NOT create a new asset object linked to another user,
        by posting other users' ID in the payload
        """
        self.client.force_login(self.user)

        amount = random.randint(1, 42)
        asset_text = BaseTest.generate_string(60)
        foreign_currency = random.choice(["USD", "EUR"])
        current_foreign_value = amount * random.randint(2, 42)
        rate = random.randint(88, 160)
        current_local_value = current_foreign_value * rate
        increase_perc = random.uniform(0.1, 0.2)
        bought_price_foreign = current_foreign_value * increase_perc
        bought_price_local = current_local_value * increase_perc
        net_loss_foreign = bought_price_foreign - current_foreign_value
        net_loss_local = bought_price_local - current_foreign_value
        today = date.today().isoformat()

        data = {
            "account": self.account.id,
            "amount": amount,
            "text": asset_text,
            "foreign_currency": foreign_currency,
            "current_local_value": current_local_value,
            "current_foreign_value": current_foreign_value,
            "bought_price_local": bought_price_local,
            "bought_price_foreign": bought_price_foreign,
            "net_loss_local": net_loss_local,
            "net_loss_foreign": net_loss_foreign,
            "rate": rate,
            "record_date": today,
            "created_by": self.user2.id,
        }

        res, _ = self.create_asset_from_data(data)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        # The created assets has been assigned to user1's account
        assets_list_url = reverse("api:v1:assets_dates")
        response = self.client.get(assets_list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 1)
        self.assertIn(data["record_date"], response.data)

        # User2 can not see any assets in his account
        self.client.force_login(self.user2)
        assets_list_url = reverse("api:v1:assets_dates")
        response = self.client.get(assets_list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 0)

    def test_creating_malformed_assets_throw_an_error(self):
        self.client.force_login(self.user)

        # Let's create another asset, that has a more recent date
        amount = random.randint(1, 42)
        asset_text = BaseTest.generate_string(60)
        foreign_currency = random.choice(["USD", "EUR"])
        current_foreign_value = amount * random.randint(2, 42)
        rate = random.randint(88, 160)
        current_local_value = current_foreign_value * rate
        # Only allow increase to avoid negative bought prices
        increase_perc = random.uniform(0.1, 0.3)
        bought_price_foreign = current_foreign_value * increase_perc
        bought_price_local = current_local_value * increase_perc
        net_loss_foreign = bought_price_foreign - current_foreign_value
        net_loss_local = bought_price_local - current_foreign_value

        # Generate a random date for user2 that is not overlapping with
        # the date already registered in assets beloging to user1
        # i.e. we want to avoid this test to be flaky, and randomly fail as
        # the random.int() has returned the same values for both user1 and
        # user2
        today = date.today()

        original_data = {
            "account": self.account.id,
            "amount": amount,
            "text": asset_text,
            "foreign_currency": foreign_currency,
            "current_local_value": current_local_value,
            "current_foreign_value": current_foreign_value,
            "bought_price_local": bought_price_local,
            "bought_price_foreign": bought_price_foreign,
            "net_loss_local": net_loss_local,
            "net_loss_foreign": net_loss_foreign,
            "rate": rate,
            "record_date": today,
        }

        # Try without account
        malformed_data_without_account = self.modify_data(
            original_data, "account", remove_field=True
        )
        res, _ = self.create_asset_from_data(malformed_data_without_account)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(res.data["account"][0], "This field is required.")
        # NICE to have: Alter the view to throw the correct exception
        self.assertEqual(res.data["account"][0].code, "invalid")

        # Try with None account
        malformed_account_none = self.modify_data(
            original_data, "account", modification_value=None
        )
        res, _ = self.create_asset_from_data(malformed_account_none)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(res.data["account"][0], "This field is required.")
        # NICE to have: Alter the view to throw the correct exception
        self.assertEqual(res.data["account"][0].code, "invalid")

        # Try without 'amount'
        malformed_data_without_amount = self.modify_data(
            original_data, "amount", remove_field=True
        )
        res, _ = self.create_asset_from_data(malformed_data_without_amount)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(res.data["amount"][0], "This field is required.")
        self.assertEqual(res.data["amount"][0].code, "required")

        # Try with non-existing account
        malformed_account_none = self.modify_data(
            original_data, "account", modification_value=34789789
        )
        res, _ = self.create_asset_from_data(malformed_account_none)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            res.data["account"][0],
            'Invalid pk "34789789" - object does not exist.',
        )
        self.assertEqual(res.data["account"][0].code, "does_not_exist")

        # Try with 'amount' as text
        malformed_data_with_amount_as_text = self.modify_data(
            original_data, "amount", modification_value="text"
        )
        res, _ = self.create_asset_from_data(
            malformed_data_with_amount_as_text
        )
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(res.data["amount"][0], "A valid number is required.")
        self.assertEqual(res.data["amount"][0].code, "invalid")

        # Try with 'amount' as None
        malformed_data_with_amount_as_none = self.modify_data(
            original_data, "amount", modification_value=None
        )
        res, _ = self.create_asset_from_data(
            malformed_data_with_amount_as_none
        )
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(res.data["amount"][0], "This field may not be null.")
        self.assertEqual(res.data["amount"][0].code, "null")

        # Try with 'amount' as 0
        malformed_data_with_amount_as_zero = self.modify_data(
            original_data, "amount", modification_value=0
        )
        res, _ = self.create_asset_from_data(
            malformed_data_with_amount_as_zero
        )
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(res.data["amount"][0], "0.0 should be more than 0.")
        self.assertEqual(res.data["amount"][0].code, "invalid")

        # Try with 'amount' as a negative number
        malformed_data_with_amount_as_negative = self.modify_data(
            original_data, "amount", modification_value=-100
        )
        res, _ = self.create_asset_from_data(
            malformed_data_with_amount_as_negative
        )
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            res.data["amount"][0], "-100.0 should be more than 0."
        )
        self.assertEqual(res.data["amount"][0].code, "invalid")

        # Test without 'text'
        malformed_data_without_text = self.modify_data(
            original_data, "text", remove_field=True
        )
        res, _ = self.create_asset_from_data(malformed_data_without_text)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(res.data["text"][0], "This field is required.")
        self.assertEqual(res.data["text"][0].code, "invalid")

        # Test with 'text' as None
        malformed_data_without_text = self.modify_data(
            original_data, "text", modification_value=None
        )
        res, _ = self.create_asset_from_data(malformed_data_without_text)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(res.data["text"][0], "This field may not be null.")
        self.assertEqual(res.data["text"][0].code, "null")

        # Test with 'text' as too long (61 chars)
        malformed_data_with_long_text = self.modify_data(
            original_data, "text", modification_value="a" * 61
        )
        res, _ = self.create_asset_from_data(malformed_data_with_long_text)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            res.data["text"][0],
            "Ensure this field has no more than 60 characters.",
        )
        self.assertEqual(res.data["text"][0].code, "max_length")

        # Test with no 'foreign_currency'
        malformed_data_no_foreign_currency = self.modify_data(
            original_data, "foreign_currency", remove_field=True
        )
        res, _ = self.create_asset_from_data(
            malformed_data_no_foreign_currency
        )
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            res.data["foreign_currency"][0], "This field is required."
        )
        # NICE to have: Alter the view to throw the correct exception
        self.assertEqual(res.data["foreign_currency"][0].code, "invalid")

        # Test with 'foreign_currency' as None
        malformed_data_with_foreign_currency_as_none = self.modify_data(
            original_data, "foreign_currency", modification_value=None
        )
        res, _ = self.create_asset_from_data(
            malformed_data_with_foreign_currency_as_none
        )
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            res.data["foreign_currency"][0], "This field may not be null."
        )
        self.assertEqual(res.data["foreign_currency"][0].code, "null")

        # Test with 'current_local_value' as a string
        malformed_current_local_as_string = self.modify_data(
            original_data, "current_local_value", modification_value="string"
        )
        res, _ = self.create_asset_from_data(malformed_current_local_as_string)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            res.data["current_local_value"][0], "A valid number is required."
        )
        self.assertEqual(res.data["current_local_value"][0].code, "invalid")

        # Test with 'current_local_value' as None
        malformed_current_local_as_none = self.modify_data(
            original_data, "current_local_value", modification_value=None
        )
        res, _ = self.create_asset_from_data(malformed_current_local_as_none)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            res.data["current_local_value"][0], "This field may not be null."
        )
        self.assertEqual(res.data["current_local_value"][0].code, "null")

        # Test with 'current_local_value' as 0... it seems we do allow 0 atm :(

        # Test with 'current_local_value' as a negative number
        malformed_current_local_negative = self.modify_data(
            original_data, "current_local_value", modification_value=-100
        )
        res, _ = self.create_asset_from_data(malformed_current_local_negative)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            res.data["current_local_value"][0], "-100.0 should be more than 0."
        )
        self.assertEqual(res.data["current_local_value"][0].code, "invalid")

        # Test with no 'current_foreign_value'
        malformed_data_no_current_foreign_value = self.modify_data(
            original_data, "current_foreign_value", remove_field=True
        )
        res, _ = self.create_asset_from_data(
            malformed_data_no_current_foreign_value
        )
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            res.data["current_foreign_value"][0], "This field is required."
        )
        # NICE to have: Alter the view to throw the correct exception
        self.assertEqual(res.data["current_foreign_value"][0].code, "invalid")

        # Test with 'current_foreign_value' as a string
        malformed_data_with_current_foreign_value_as_string = self.modify_data(
            original_data, "current_foreign_value", modification_value="string"
        )
        res, _ = self.create_asset_from_data(
            malformed_data_with_current_foreign_value_as_string
        )
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            res.data["current_foreign_value"][0], "A valid number is required."
        )
        self.assertEqual(res.data["current_foreign_value"][0].code, "invalid")

        # Test with 'current_foreign_value' as None
        malformed_data_with_current_foreign_value_as_none = self.modify_data(
            original_data, "current_foreign_value", modification_value=None
        )
        res, _ = self.create_asset_from_data(
            malformed_data_with_current_foreign_value_as_none
        )
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            res.data["current_foreign_value"][0], "This field may not be null."
        )
        self.assertEqual(res.data["current_foreign_value"][0].code, "null")

        # Test with 'current_foreign_value' as 0... it seems we do allow 0 atm

        # Test with 'current_foreign_value' as a negative number
        malformed_data_with_current_foreign_value_as_negative = (
            self.modify_data(
                original_data, "current_foreign_value", modification_value=-100
            )
        )
        res, _ = self.create_asset_from_data(
            malformed_data_with_current_foreign_value_as_negative
        )
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            res.data["current_foreign_value"][0],
            "-100.0 should be more than 0.",
        )
        self.assertEqual(res.data["current_foreign_value"][0].code, "invalid")

        # Test with no 'bought_price_local'
        malformed_data_no_bought_price_local = self.modify_data(
            original_data, "bought_price_local", remove_field=True
        )
        res, _ = self.create_asset_from_data(
            malformed_data_no_bought_price_local
        )
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            res.data["bought_price_local"][0], "This field is required."
        )
        # NICE to have: Alter the view to throw the correct exception
        self.assertEqual(res.data["bought_price_local"][0].code, "invalid")

        # Test with 'bought_price_local' as a string
        malformed_data_with_bought_price_local_as_string = self.modify_data(
            original_data, "bought_price_local", modification_value="string"
        )
        res, _ = self.create_asset_from_data(
            malformed_data_with_bought_price_local_as_string
        )
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            res.data["bought_price_local"][0], "A valid number is required."
        )
        self.assertEqual(res.data["bought_price_local"][0].code, "invalid")

        # Test with 'bought_price_local' as None
        malformed_data_with_bought_price_local_as_none = self.modify_data(
            original_data, "bought_price_local", modification_value=None
        )
        res, _ = self.create_asset_from_data(
            malformed_data_with_bought_price_local_as_none
        )
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            res.data["bought_price_local"][0], "This field may not be null."
        )
        self.assertEqual(res.data["bought_price_local"][0].code, "null")

        # Test with 'bought_price_local' as 0... it seems we do allow 0 atm :-/

        # Test with 'bought_price_local' as a negative number
        malformed_data_with_bought_price_local_as_negative = self.modify_data(
            original_data, "bought_price_local", modification_value=-100
        )
        res, _ = self.create_asset_from_data(
            malformed_data_with_bought_price_local_as_negative
        )
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            res.data["bought_price_local"][0], "-100.0 should be more than 0."
        )
        self.assertEqual(res.data["bought_price_local"][0].code, "invalid")

        # Test with no 'bought_price_foreign'
        malformed_data_no_bought_price_foreign = self.modify_data(
            original_data, "bought_price_foreign", remove_field=True
        )
        res, _ = self.create_asset_from_data(
            malformed_data_no_bought_price_foreign
        )
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            res.data["bought_price_foreign"][0], "This field is required."
        )
        # NICE to have: Alter the view to throw the correct exception
        self.assertEqual(res.data["bought_price_foreign"][0].code, "invalid")

        # Test with 'bought_price_foreign' as a string
        malformed_data_with_bought_price_foreign_as_string = self.modify_data(
            original_data, "bought_price_foreign", modification_value="string"
        )
        res, _ = self.create_asset_from_data(
            malformed_data_with_bought_price_foreign_as_string
        )
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            res.data["bought_price_foreign"][0], "A valid number is required."
        )
        self.assertEqual(res.data["bought_price_foreign"][0].code, "invalid")

        # Test with 'bought_price_foreign' as None
        malformed_data_with_bought_price_foreign_as_none = self.modify_data(
            original_data, "bought_price_foreign", modification_value=None
        )
        res, _ = self.create_asset_from_data(
            malformed_data_with_bought_price_foreign_as_none
        )
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            res.data["bought_price_foreign"][0], "This field may not be null."
        )
        self.assertEqual(res.data["bought_price_foreign"][0].code, "null")

        # Test with 'bought_price_foreign' as 0... it seems we do allow 0 atm

        # Test with 'bought_price_foreign' as a negative number
        malformed_data_with_bought_price_foreign_as_negative = (
            self.modify_data(
                original_data, "bought_price_foreign", modification_value=-100
            )
        )
        res, _ = self.create_asset_from_data(
            malformed_data_with_bought_price_foreign_as_negative
        )
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            res.data["bought_price_foreign"][0],
            "-100.0 should be more than 0.",
        )
        self.assertEqual(res.data["bought_price_foreign"][0].code, "invalid")

        # Test with no 'net_loss_foreign'
        malformed_data_no_net_loss_foreign = self.modify_data(
            original_data, "net_loss_foreign", remove_field=True
        )
        res, _ = self.create_asset_from_data(
            malformed_data_no_net_loss_foreign
        )
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            res.data["net_loss_foreign"][0], "This field is required."
        )
        # NICE to have: Alter the view to throw the correct exception
        self.assertEqual(res.data["net_loss_foreign"][0].code, "invalid")

        # Test with 'net_loss_foreign' as a string
        malformed_data_with_net_loss_foreign_as_string = self.modify_data(
            original_data, "net_loss_foreign", modification_value="string"
        )
        res, _ = self.create_asset_from_data(
            malformed_data_with_net_loss_foreign_as_string
        )
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            res.data["net_loss_foreign"][0], "A valid number is required."
        )
        self.assertEqual(res.data["net_loss_foreign"][0].code, "invalid")

        # Test with 'net_loss_foreign' as 0: it is totally possible to
        # have no gain or no loss => skip

        # Test with 'net_loss_foreign' as None
        malformed_data_with_net_loss_foreign_as_none = self.modify_data(
            original_data, "net_loss_foreign", modification_value=None
        )
        res, _ = self.create_asset_from_data(
            malformed_data_with_net_loss_foreign_as_none
        )
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            res.data["net_loss_foreign"][0], "This field may not be null."
        )
        self.assertEqual(res.data["net_loss_foreign"][0].code, "null")

        # Test with 'net_loss_foreign' as a negative number: this could be zero
        # if we are loosing money => skip

        # Try# Test with no 'net_loss_local'
        malformed_data_no_net_loss_local = self.modify_data(
            original_data, "net_loss_local", remove_field=True
        )
        res, _ = self.create_asset_from_data(malformed_data_no_net_loss_local)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            res.data["net_loss_local"][0], "This field is required."
        )
        self.assertEqual(res.data["net_loss_local"][0].code, "invalid")

        # Test with 'net_loss_local' as a string
        malformed_data_with_net_loss_local_as_string = self.modify_data(
            original_data, "net_loss_local", modification_value="string"
        )
        res, _ = self.create_asset_from_data(
            malformed_data_with_net_loss_local_as_string
        )
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            res.data["net_loss_local"][0], "A valid number is required."
        )
        # NICE to have: Alter the view to throw the correct exception
        self.assertEqual(res.data["net_loss_local"][0].code, "invalid")

        # Test with 'net_loss_local' as None
        malformed_data_with_net_loss_local_as_none = self.modify_data(
            original_data, "net_loss_local", modification_value=None
        )
        res, _ = self.create_asset_from_data(
            malformed_data_with_net_loss_local_as_none
        )
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            res.data["net_loss_local"][0], "This field may not be null."
        )
        self.assertEqual(res.data["net_loss_local"][0].code, "null")

        # Test with 'net_loss_local' as 0: it is totally possible to have no
        #      gain or no loss => skip
        # Test with 'net_loss_local' as a negative number: this could be zero
        #  if we are loosing money => skip

        # Test with no 'record_date'
        malformed_data_no_record_date = self.modify_data(
            original_data, "record_date", remove_field=True
        )
        res, _ = self.create_asset_from_data(malformed_data_no_record_date)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(res.data["record_date"][0], "This field is required.")
        self.assertEqual(res.data["record_date"][0].code, "required")

        # Test with 'record_date' as a string
        malformed_data_with_record_date_as_string = self.modify_data(
            original_data, "record_date", modification_value="string"
        )
        res, _ = self.create_asset_from_data(
            malformed_data_with_record_date_as_string
        )
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        err_msg = (
            "Date has wrong format. Use one of these formats instead:"
            " YYYY-MM-DD."
        )
        self.assertEqual(res.data["record_date"][0], err_msg)
        self.assertEqual(res.data["record_date"][0].code, "invalid")

        # Test with 'record_date' as a malformed date (e.g., '2012-99-99')
        malformed_data_with_malformed_date = self.modify_data(
            original_data, "record_date", modification_value="2012-99-99"
        )
        res, _ = self.create_asset_from_data(
            malformed_data_with_malformed_date
        )
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        err_msg = (
            "Date has wrong format. Use one of these formats instead:"
            " YYYY-MM-DD."
        )
        self.assertEqual(res.data["record_date"][0], err_msg)
        self.assertEqual(res.data["record_date"][0].code, "invalid")

        # Test with 'record_date' as None
        malformed_data_with_record_date_as_none = self.modify_data(
            original_data, "record_date", modification_value=None
        )
        res, _ = self.create_asset_from_data(
            malformed_data_with_record_date_as_none
        )
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            res.data["record_date"][0], "This field may not be null."
        )
        self.assertEqual(res.data["record_date"][0].code, "null")

        # Test with 'rate' as a string
        malformed_rate_as_string = self.modify_data(
            original_data, "rate", modification_value="string"
        )
        res, _ = self.create_asset_from_data(malformed_rate_as_string)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(res.data["rate"][0], "A valid number is required.")
        self.assertEqual(res.data["rate"][0].code, "invalid")

        # Test with 'rate' as a negative number : this is not enforced yet
        # malformed_rate_as_negative = self.modify_data(original_data, 'rate',
        #                                               modification_value=-100)
        # res, _ = self.create_asset_from_data(malformed_rate_as_negative)
        # self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        # self.assertEqual(res.data['rate'][0],
        #                  '-100.0 should be more than 0.')
        # self.assertEqual(res.data['rate'][0].code, 'invalid')

        # Test with 'rate' as None: Null is currently allowed
        # malformed_data_with_record_date_as_none = self.modify_data(original_data,
        #                                                            'rate',
        #                                                            modification_value=None)
        # res, _ = self.create_asset_from_data(malformed_data_with_record_date_as_none)
        # self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        # self.assertEqual(res.data['record_date'][0], 'This field may not be null.')
        # self.assertEqual(res.data['record_date'][0].code, 'null')

        # Confirm the original data was actually working
        res, _ = self.create_asset_from_data(original_data)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        assets_list_url = reverse("api:v1:assets")
        response = self.client.get(assets_list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 1)
        self.assertEqual(response.data["count"], 1)

    # WRITE ME
    @skip
    def test_regression_202_null_rate_will_not_throw_django_db_utils_Integrity_error(
        self,
    ):
        # Create an Assets with rate field null, and confirm the backend
        # handles it, and does not
        # throw error 5XX
        pass

    # WRITE ME
    @skip
    def test_can_not_delete_own_assets(self):
        # Use delete on all routes related to Assets
        pass

    # WRITE ME
    @skip
    def test_can_not_delete_other_users_assets(self):
        # Use delete on all routes related to Assets
        pass

    def test_get_assets_dates_returns_no_data_for_users_without_assets(self):
        self.client.force_login(self.user)

        assets_list_url = reverse("api:v1:assets_dates")
        response = self.client.get(assets_list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data), 0)

    def test_get_assets_dates_returns_data_for_users_with_assets(self):
        self.client.force_login(self.user)

        res, data = self.create_asset(self.account.id)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            Asset.objects.filter(created_by=self.user).count(), 1  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        # Create Asset with a past date:
        # we confirm that some tests are accidentally suceeding because we
        # use today() everywhere
        res2, data2 = self.create_asset(self.account.id, date_in_the_past=True)
        self.assertEqual(res2.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            Asset.objects.filter(created_by=self.user).count(), 2  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        assets_list_url = reverse("api:v1:assets_dates")
        response = self.client.get(assets_list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(len(response.data), 2)
        self.assertIn(data["record_date"], response.data)
        self.assertIn(data2["record_date"], response.data)

    def test_get_assets_dates_does_not_return_other_users_data(
        self,
    ):  # Refactor me out to complete #46
        (
            local_user,
            local_user2,
            local_account,
            local_account2,
        ) = _create_users_and_accounts()

        self.client.force_login(local_user)
        res, data = self.create_asset(local_account.id, date_in_the_past=True)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            Asset.objects.filter(created_by=local_user).count(), 1  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        # Create Asset with a past date:
        # we must confirm that some tests are NOT accidentally suceeding
        # because we use today() everywhere
        res, data2 = self.create_asset(local_account.id, date_in_the_past=True)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            Asset.objects.filter(created_by=local_user).count(), 2  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        response = self.client.get(
            reverse("api:v1:assets_dates"), format="json"
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        print(
            "test_get_assets_dates_does_not_return_other_users_data"
            " might have been sharing the user with the previous test(s),"
            " hence the flakiness."
            " See https://gitlab.com/micheleva/budgeteer/-/jobs/9102546626",
            flush=True,
        )
        self.assertEqual(len(response.data), 2)  # <== Previously flaky line

        # The second user does not get to see local_user's assets date
        self.client.force_login(local_user2)
        assets_list_url = reverse("api:v1:assets_dates")
        response = self.client.get(assets_list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        self.assertEqual(len(response.data), 0)
        self.assertNotIn(data["record_date"], response.data)
        self.assertNotIn(data2["record_date"], response.data)

        amount = random.randint(1, 42)
        asset_text = BaseTest.generate_string(60)
        foreign_currency = random.choice(["USD", "EUR"])
        current_foreign_value = amount * random.randint(2, 42)
        rate = random.randint(88, 160)
        current_local_value = current_foreign_value * rate
        increase_perc = random.uniform(0.1, 0.2)
        bought_price_foreign = current_foreign_value * increase_perc
        bought_price_local = current_local_value * increase_perc
        net_loss_foreign = bought_price_foreign - current_foreign_value
        net_loss_local = bought_price_local - current_foreign_value

        # Generate a random date for local_user2 that is not overlapping with the
        #  date already registered in assets beloging to local_user
        # i.e. we want to avoid this test to be flaky, and randomly fail as
        # the random.int() has returned the same values for both local_user and
        # local_user2
        new_record_date = (
            date.today() - timedelta(days=random.randint(42, 365))
        ).isoformat()
        while new_record_date in response.data:
            new_record_date = (
                date.today() - timedelta(days=random.randint(42, 365))
            ).isoformat()

        user2_data = {
            "account": local_account2.id,
            "amount": amount,
            "text": asset_text,
            "foreign_currency": foreign_currency,
            "current_local_value": current_local_value,
            "current_foreign_value": current_foreign_value,
            "bought_price_local": bought_price_local,
            "bought_price_foreign": bought_price_foreign,
            "net_loss_local": net_loss_local,
            "net_loss_foreign": net_loss_foreign,
            "rate": rate,
            "record_date": new_record_date,
        }

        self.client.force_login(local_user2)
        res, _ = self.create_asset_from_data(user2_data)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        response = self.client.get(reverse("api:v1:assets"), format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        # Confirm local_user2 can only see 1 asset
        self.assertEqual(len(response.data["results"]), 1)
        self.assertEqual(response.data["count"], 1)

        self.assertNotIn(
            data["record_date"], response.data["results"][0]["record_date"]
        )
        self.assertNotIn(
            data2["record_date"], response.data["results"][0]["record_date"]
        )
        self.assertIn(
            user2_data["record_date"],
            response.data["results"][0]["record_date"],
        )

        # Confirm that local_user can only see their original two assets
        self.client.force_login(local_user)
        response = self.client.get(reverse("api:v1:assets"), format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(len(response.data["results"]), 2)
        self.assertEqual(response.data["count"], 2)
        for a in response.data["results"]:
            self.assertNotIn(a["record_date"], new_record_date)

    def test_get_assets_dates_returns_in_desc_order(self):
        logger.warning("test_get_assets_dates_returns_in_desc_order")

    # WRITE ME
    @skip
    def test_create_assets_with_the_same_data_does_create_multiple_objects(
        self,
    ):
        # Assets does not work as Dividends, let's make sure nobody changed
        # the spec!
        # Currently posting the same data will create 2 different Assets
        # but posting the same data will NOT create a second Dividend
        pass

    def test_create_assets_with_non_existing_accounts_throws_error_400(self):
        self.client.force_login(self.user)
        acc_r, _ = self.create_account()
        self.assertEqual(acc_r.status_code, status.HTTP_201_CREATED)

        # Create a mock request
        amount = random.randint(1, 42)
        asset_text = BaseTest.generate_string(60)

        current_foreign_value = amount * random.randint(2, 42)
        rate = random.randint(88, 160)
        current_local_value = current_foreign_value * rate

        increase_perc = random.uniform(0.1, 0.2)

        bought_price_foreign = current_foreign_value * increase_perc
        bought_price_local = current_local_value * increase_perc
        record_date = (
            date.today() - timedelta(days=random.randint(420, 3650))
        ).isoformat()

        ok_data = {
            "account": 9999999,
            "amount": amount,
            "text": asset_text,
            "foreign_currency": random.choice(["USD", "EUR"]),
            "current_local_value": current_local_value,
            "current_foreign_value": current_foreign_value,
            "bought_price_local": bought_price_local,
            "bought_price_foreign": bought_price_foreign,
            "net_loss_local": (bought_price_local - current_foreign_value),
            "net_loss_foreign": (bought_price_foreign - current_foreign_value),
            "record_date": record_date,
            "rate": rate,
        }

        res = self.client.post(reverse("api:v1:asset_create"), ok_data)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        err_msg = 'Invalid pk "9999999" - object does not exist.'
        self.assertEqual(res.data["account"][0], err_msg)
        self.assertEqual(res.data["account"][0].code, "does_not_exist")

    def test_create_assets_with_malformed_data_throws_error_400(self):
        self.client.force_login(self.user)
        acc_r, _ = self.create_account()
        self.assertEqual(acc_r.status_code, status.HTTP_201_CREATED)

        # Create a mock request
        amount = -1
        asset_text = BaseTest.generate_string(60)

        current_foreign_value = amount * random.randint(2, 42)
        rate = random.randint(88, 160)
        current_local_value = current_foreign_value * rate

        increase_perc = random.uniform(0.1, 0.2)

        bought_price_foreign = current_foreign_value * increase_perc
        bought_price_local = current_local_value * increase_perc
        record_date = (
            date.today() - timedelta(days=random.randint(420, 3650))
        ).isoformat()

        ok_data = {
            "account": 9999999,
            "amount": amount,
            "text": asset_text,
            "foreign_currency": random.choice(["USD", "EUR"]),
            "current_local_value": current_local_value,
            "current_foreign_value": current_foreign_value,
            "bought_price_local": bought_price_local,
            "bought_price_foreign": bought_price_foreign,
            "net_loss_local": (bought_price_local - current_foreign_value),
            "net_loss_foreign": (bought_price_foreign - current_foreign_value),
            "record_date": record_date,
            "rate": rate,
        }

        res = self.client.post(reverse("api:v1:asset_create"), ok_data)
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        # FIXME: why is this error not "This field is required" ???
        err_msg = 'Invalid pk "9999999" - object does not exist.'
        self.assertEqual(res.data["account"][0], err_msg)
        self.assertEqual(res.data["account"][0].code, "does_not_exist")

    @patch("api.v1.views.asset_views.AssetsList")
    @patch("budgets.serializers.AssetSerializer")
    def test_integrity_errors_are_catched_on_create(
        self, mock_cat_serializer, _
    ):
        self.client.force_login(self.user)
        acc_r, _ = self.create_account()
        self.assertEqual(acc_r.status_code, status.HTTP_201_CREATED)

        # Confirm assets can be create before the view is mocked
        res, _ = self.create_asset(acc_r.data["id"], date_in_the_past=False)
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        # Force the serializer to Throw an Integrity error
        serializer = mock_cat_serializer.return_value
        msg = "I AM A MOCKED INTEGRITY ERROR. I HOPE YOU CATCH ME: ROAAAARR!"
        serializer.save.side_effect = IntegrityError(msg)

        # Create a mock request
        amount = random.randint(1, 42)
        asset_text = BaseTest.generate_string(60)

        current_foreign_value = amount * random.randint(2, 42)
        rate = random.randint(88, 160)
        current_local_value = current_foreign_value * rate

        increase_perc = random.uniform(0.1, 0.2)

        bought_price_foreign = current_foreign_value * increase_perc
        bought_price_local = current_local_value * increase_perc
        record_date = (
            date.today() - timedelta(days=random.randint(420, 3650))
        ).isoformat()

        ok_data = {
            "account": acc_r.data["id"],
            "amount": amount,
            "text": asset_text,
            "foreign_currency": random.choice(["USD", "EUR"]),
            "current_local_value": current_local_value,
            "current_foreign_value": current_foreign_value,
            "bought_price_local": bought_price_local,
            "bought_price_foreign": bought_price_foreign,
            "net_loss_local": (bought_price_local - current_foreign_value),
            "net_loss_foreign": (bought_price_foreign - current_foreign_value),
            "record_date": record_date,
            "rate": rate,
        }
        # Create an instance of the view
        view = AssetsCreate()
        request = self.client.post(reverse("api:v1:asset_create"), ok_data)

        # Set the request user
        view.request = request
        request.user = self.user

        # Confirm that the mocks works, and that ok_data actually thew an
        #  exception and confirm it was correctly handled by the backend
        failed_res = view.perform_create(serializer)
        self.assertEqual(
            failed_res.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR
        )
        msg = (
            "Generic error thrown while creating the Asset. "
            "Refer the web container logs if you're the admin"
        )
        self.assertEqual(failed_res.data["non_field_errors"][0], msg)


from django.test import TestCase
from django.contrib.auth.models import User
from rest_framework.test import APIClient
from rest_framework import status
from budgets.models import Asset  # Adjust according to your project structure

from django.test import TestCase
from django.contrib.auth.models import User
from rest_framework.test import APIClient
from rest_framework import status
from budgets.models import (
    Asset,
    Account,
)  # Adjust according to your project structure

from django.test import TestCase
from django.contrib.auth.models import User
from rest_framework.test import APIClient
from rest_framework import status
from budgets.models import (
    Asset,
    Account,
)  # Adjust according to your project structure


class BulkDeleteAssetsTestCase(BaseApiTest):
    def setUp(self):
        self.client = APIClient()
        self.user = User.objects.create_user(
            username="testuser", password="testpass"
        )
        self.client.force_login(self.user)
        # Create an account for the assets
        self.account = Account.objects.create(  # pylint: disable=E1101; # noqa
            text="Account X",
            credit_entity="Broker 1",
            created_by=self.user,
        )
        # Create three assets using the API
        for i in range(3):
            res, _ = self.create_asset(self.account.id)
            self.assertEqual(res.status_code, status.HTTP_201_CREATED)
            # fmt: off
            self.assertEqual(
                Asset.objects.filter(created_by=self.user).count(), i+1  # pylint: disable=E1101; # noqa
            )
        # Fetch the list of assets to get their IDs
        assets_list_url = reverse("api:v1:assets")

        response = self.client.get(assets_list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 3)

        # Store the IDs of the created assets
        self.asset_ids = [asset["id"] for asset in response.data["results"]]

    def test_bulk_delete_success(self):
        data = {
            "ids": [self.asset_ids[0], self.asset_ids[1]]
        }  # Use the IDs stored in setUp
        response = self.client.post(
            "/api/v1/assets/delete", data, format="json"
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {"records_deleted": 2})
        self.assertFalse(Asset.objects.filter(id=self.asset_ids[0]).exists())
        self.assertFalse(Asset.objects.filter(id=self.asset_ids[1]).exists())

        # Check that one asset still exists
        remaining_assets = Asset.objects.filter(id__in=self.asset_ids).count()
        self.assertEqual(remaining_assets, 1)  # Only one asset should remain

    def test_bulk_delete_invalid_ids(self):
        data = {"ids": "not_a_list"}
        response = self.client.post(
            "/api/v1/assets/delete", data, format="json"
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.data, {"error": "Invalid or empty 'ids' list."}
        )

    def test_bulk_delete_empty_ids(self):
        data = {"ids": []}
        response = self.client.post(
            "/api/v1/assets/delete", data, format="json"
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.data, {"error": "Invalid or empty 'ids' list."}
        )

    def test_bulk_delete_no_matching_assets(self):
        data = {"ids": [999]}  # Assuming this ID does not exist
        response = self.client.post(
            "/api/v1/assets/delete", data, format="json"
        )

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)

    def test_bulk_delete_partially_existing_assets(self):
        data = {
            "ids": [self.asset_ids[0], 9999]
        }  # One existing and one non-existing ID
        response = self.client.post(
            "/api/v1/assets/delete", data, format="json"
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {"records_deleted": 1})
        # self.assertFalse(Asset.objects.filter(id=self.asset_ids[1]).exists())
        # self.assertTrue(Asset.objects.filter(id=self.asset_ids[2]).exists())
        assets_list_url = reverse("api:v1:assets")

        response = self.client.get(assets_list_url, format="json")
        # Extract the asset ids from the response
        res_asset_ids = [asset["id"] for asset in response.data["results"]]
        self.assertIn(self.asset_ids[1], res_asset_ids)
        self.assertIn(self.asset_ids[2], res_asset_ids)
        self.assertEqual(response.data["count"], 2)
