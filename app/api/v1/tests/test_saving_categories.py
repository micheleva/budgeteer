# Copyright: (c) 2021, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
import logging

from unittest.mock import patch
from unittest import skip

from django.contrib.auth.models import User
from django.db.utils import IntegrityError
from django.urls import reverse
from rest_framework import status

from api.v1.tests.test_base import BaseApiTest
from api.v1.views.saving_category_views import SavingCategoryList
from api.v1.views.saving_category_views import SavingCategoryDetails

import budgets.models as m
from budgets.tests.base import BaseTest

logger = logging.getLogger(__name__)


class SavingCategoryTest(BaseApiTest):
    """
    Test SavingCategory model related v1 rest api behaviour
    """

    def setUp(self):  # pylint: disable=C0103; # noqa
        # Instrument the unit tests to fail if they run for too long
        super().setUp()
        # fmt: off
        self.user = User.objects.create_user(  # pylint: disable=E1101; # noqa
            username=BaseTest.generate_string(20),
            password=BaseTest.generate_string(20),
        )
        self.user2 = User.objects.create_user(  # pylint: disable=E1101; # noqa
            username=BaseTest.generate_string(20),
            password=BaseTest.generate_string(20),
        )
        # fmt: on

    def test_create_and_list_savingcategories(self):
        """
        Ensure we can create a new SavingCategory object, and other users
        can't see it
        """
        self.client.force_login(self.user)

        # Create an SavingCategory
        response, data = self.create_savingcategory()
        # fmt: off
        sac = m.SavingCategory.objects.filter(  # pylint: disable=E1101; # noqa
            created_by=self.user.id
        ).first()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            m.SavingCategory.objects.filter(created_by=self.user.id).count(),  # pylint: disable=E1101; # noqa
            1,
        )
        # fmt: on
        self.assertEqual(sac.text, data["text"])

        # Same user (the owner) can see that SavingCategory
        list_url = reverse("api:v1:savingcategories")
        r = self.client.get(list_url, format="json")

        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(r.data["count"], 1)
        self.assertEqual(r.data["results"][0]["text"], data["text"])

    def test_different_user_can_have_the_same_SavingCategory_text(self):
        self.client.force_login(self.user)

        # Create an SavingCategory
        response, data = self.create_savingcategory()
        # fmt: off
        sac = m.SavingCategory.objects.filter(  # pylint: disable=E1101; # noqa
            created_by=self.user.id
        ).first()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            m.SavingCategory.objects.filter(created_by=self.user.id).count(),  # pylint: disable=E1101; # noqa
            1,
        )
        # fmt: on
        self.assertEqual(sac.text, data["text"])

        list_url = reverse("api:v1:savingcategories")
        r = self.client.get(list_url, format="json")
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(r.data["count"], 1)
        self.assertEqual(r.data["results"][0]["text"], data["text"])

        self.client.force_login(self.user2)

        create_url = reverse("api:v1:savingcategories")
        data = {"text": data["text"]}
        self.client.post(create_url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # fmt: off
        sac2 = m.SavingCategory.objects.filter(  # pylint: disable=E1101; # noqa
            created_by=self.user.id
        ).first()
        self.assertEqual(
            m.SavingCategory.objects.filter(  # pylint: disable=E1101; # noqa
                created_by=self.user2.id
            ).count(),
            1,
        )
        # fmt: on
        self.assertEqual(sac2.text, data["text"])

        r = self.client.get(list_url, format="json")
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(r.data["count"], 1)
        self.assertEqual(r.data["results"][0]["text"], data["text"])

    def test_can_not_create_savingcategories_for_other_users(self):
        """
        Ensure we can NOT create a new SavingCategory object linked to
        another user by posting other users' ID in the payload
        """
        self.client.force_login(self.user)

        # Another user tries to create an SavingCategory object
        # and have it linked to the initial user's ID
        self.client.force_login(self.user2)

        # Try to inject first user ownserhip using an SavingCategory
        # belonging to the intial user: the SavingCategory creation should
        # succeed ignoring created_by
        create_list_url = reverse("api:v1:savingcategories")
        text = BaseTest.generate_string(20)
        data = {"text": text, "created_by": self.user.id}
        res = self.client.post(create_list_url, data, format="json")

        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            m.SavingCategory.objects.get(created_by=self.user2.id).text,  # pylint: disable=E1101; # noqa
            text,
        )
        # fmt: on
        # The SavingCategory object is attatched to the currently logged
        # in user (other_user)
        response = self.client.get(create_list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["text"], text)
        self.assertEqual(response.data["count"], 1)

        # The initial user logs in, and finds no savingcategories, as this
        # user created none
        self.client.force_login(self.user)
        response = self.client.get(create_list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 0)

    def test_malformed_savingcategories_creation_will_fail(self):
        self.client.force_login(self.user)

        # Try with None text
        create_url = reverse("api:v1:savingcategories")
        text = None
        data = {"text": text}
        res = self.client.post(create_url, data, format="json")

        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(res.data["text"][0], "This field may not be null.")
        self.assertEqual(res.data["text"][0].code, "null")

        # Try with too long text
        text = BaseTest.generate_string(41)
        data = {"text": text}
        res = self.client.post(create_url, data, format="json")

        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        err_msg = "Ensure this field has no more than 40 characters."
        self.assertEqual(res.data["text"][0], err_msg)
        self.assertEqual(res.data["text"][0].code, "max_length")

        # Try with an empty string
        data = {"text": ""}
        res = self.client.post(create_url, data, format="json")

        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(res.data["text"][0], "This field may not be blank.")
        self.assertEqual(res.data["text"][0].code, "blank")

    # Can update the model with PUT requests
    def test_users_can_entirely_edit_savingcategories(self):
        self.client.force_login(self.user)

        # Create a SavingCategory
        r, data = self.create_savingcategory()
        self.assertEqual(r.status_code, status.HTTP_201_CREATED)

        s_cats = self.get_savingcategories()
        self.assertEqual(len(s_cats.data["results"]), 1)
        self.assertEqual(s_cats.data["count"], 1)

        c = s_cats.data["results"][0]
        c_id = c["id"]
        c_arch = c["is_archived"]

        new_text = BaseTest.generate_string(20)
        data = {"text": new_text, "is_archived": not c_arch}
        r = self.update_savingcategories(c_id, data)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(r.data["text"], new_text)
        self.assertEqual(r.data["is_archived"], not c_arch)

        # Assert we have not accidentally created new categories
        s_cats = self.get_savingcategories()
        self.assertEqual(len(s_cats.data["results"]), 1)
        self.assertEqual(s_cats.data["count"], 1)

        # Assert the update correctly updated the model
        self.assertEqual(s_cats.data["results"][0]["text"], new_text)
        self.assertEqual(s_cats.data["results"][0]["is_archived"], not c_arch)

    # Can update the model with PATCH requests
    def test_users_can_partially_edit_savingcategories(self):
        self.client.force_login(self.user)

        # Create a SavingCategory
        r, data = self.create_savingcategory()
        self.assertEqual(r.status_code, status.HTTP_201_CREATED)

        s_cats = self.get_savingcategories()
        self.assertEqual(len(s_cats.data["results"]), 1)
        self.assertEqual(s_cats.data["count"], 1)

        s_cat = s_cats.data["results"][0]
        c_id = s_cat["id"]
        c_arch = s_cat["is_archived"]

        # Can update only the text
        new_text = BaseTest.generate_string(20)
        data = {
            "text": new_text,
        }
        r = self.partially_update_savingcategories(c_id, data)
        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(r.data["text"], new_text)
        self.assertEqual(r.data["is_archived"], c_arch)

        # Assert we have not accidentally created new categories
        s_cats = self.get_savingcategories()
        self.assertEqual(len(s_cats.data["results"]), 1)
        self.assertEqual(s_cats.data["count"], 1)

        # Assert the update correctly updated the model
        self.assertEqual(s_cats.data["results"][0]["text"], new_text)
        self.assertEqual(s_cats.data["results"][0]["is_archived"], c_arch)

        # Can update only the is_archived
        sec_data = {
            "is_archived": not c_arch,
        }
        res = self.partially_update_savingcategories(c_id, sec_data)
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data["text"], new_text)
        self.assertEqual(res.data["is_archived"], not c_arch)

        # Assert we have not accidentally created new categories
        res2 = self.get_savingcategories()
        self.assertEqual(len(res2.data["results"]), 1)
        self.assertEqual(res2.data["count"], 1)

        # Assert the update correctly updated the model
        self.assertEqual(res2.data["results"][0]["text"], new_text)
        self.assertEqual(res2.data["results"][0]["is_archived"], not c_arch)

    @skip
    def test_can_not_edit_saving_categories_to_be_malformed(self):
        self.fail("IMPLEMENT ME")
        self.fail(
            "validate all possible combination of editing category malformed requests"
        )

    def test_can_not_create_duplicated_text_savingcategories(self):
        self.client.force_login(self.user)

        create_url = reverse("api:v1:savingcategories")
        text = BaseTest.generate_string(20)
        data = {"text": text}
        res = self.client.post(create_url, data, format="json")
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        # Create another SetAside Category, and confirm it fails
        data = {"text": text}
        res = self.client.post(create_url, data, format="json")

        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = "A SavingCategory with the text already exists for this user."
        self.assertEqual(res.data["non_field_errors"][0], msg)
        self.assertEqual(res.data["non_field_errors"][0].code, "invalid")

    def test_users_can_not_see_other_users_savingcategories(self):
        self.client.force_login(self.user)
        # Create an SavingCategory
        response, data = self.create_savingcategory()
        # fmt: off
        sac = m.SavingCategory.objects.get(  # pylint: disable=E1101; # noqa
            created_by=self.user.id
        )
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            m.SavingCategory.objects.filter(created_by=self.user.id).count(),  # pylint: disable=E1101; # noqa
            1,
        )
        # fmt: on
        self.assertEqual(sac.text, data["text"])

        # Same user (the owner) can see that SavingCategory
        list_url = reverse("api:v1:savingcategories")
        response = self.client.get(list_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(response.data["results"][0]["text"], data["text"])

        # Another user CANNOT see that SavingCategory
        self.client.force_login(self.user2)
        response = self.client.get(list_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 0)
        self.assertEqual(len(response.data["results"]), 0)

    # Can update the model with PUT requests
    def test_users_can_not_entirely_edit_other_users_savingcategories(self):
        self.client.force_login(self.user)

        # Create a SavingCategory
        r, data = self.create_savingcategory()
        self.assertEqual(r.status_code, status.HTTP_201_CREATED)

        s_cats = self.get_savingcategories()
        self.assertEqual(len(s_cats.data["results"]), 1)
        self.assertEqual(s_cats.data["count"], 1)

        c = s_cats.data["results"][0]
        c_id = c["id"]
        c_arch = c["is_archived"]
        c_purp = c["text"]

        # User 2 can not entirely update other users' models
        self.client.force_login(self.user2)
        new_text = BaseTest.generate_string(20)
        data = {"text": new_text, "is_archived": not c_arch}
        r = self.update_savingcategories(c_id, data)
        self.assertEqual(r.status_code, status.HTTP_404_NOT_FOUND)

        # User 1 can still see the original data
        self.client.force_login(self.user)
        # Assert user2 did not accidentally create new saving categories
        recent_cats = self.get_savingcategories()
        self.assertEqual(len(recent_cats.data["results"]), 1)
        self.assertEqual(recent_cats.data["count"], 1)

        # Assert the update failed, and the data has not changed
        self.assertEqual(recent_cats.data["results"][0]["text"], c_purp)
        self.assertEqual(recent_cats.data["results"][0]["is_archived"], c_arch)

    # Can update the model with PATCH requests
    def test_users_can_not_partially_edit_other_users_savingcategories(self):
        self.client.force_login(self.user)

        # Create an SavingCategory
        r, data = self.create_savingcategory()
        self.assertEqual(r.status_code, status.HTTP_201_CREATED)

        s_cats = self.get_savingcategories()
        self.assertEqual(len(s_cats.data["results"]), 1)
        self.assertEqual(s_cats.data["count"], 1)

        s_cat = s_cats.data["results"][0]
        c_id = s_cat["id"]
        c_arch = s_cat["is_archived"]
        c_purp = s_cat["text"]

        # Can not update  text fields of other users' models
        self.client.force_login(self.user2)
        new_text = BaseTest.generate_string(20)
        data = {
            "text": new_text,
        }
        r = self.partially_update_savingcategories(c_id, data)
        self.assertEqual(r.status_code, status.HTTP_404_NOT_FOUND)

        # User 1 can still see the original data
        self.client.force_login(self.user)
        # Assert user2 did not accidentally create new saving categories
        recent_cats = self.get_savingcategories()
        self.assertEqual(recent_cats.status_code, status.HTTP_200_OK)
        self.assertEqual(len(recent_cats.data["results"]), 1)
        self.assertEqual(recent_cats.data["count"], 1)

        # Assert the update failed, and the data has not changed
        self.assertEqual(recent_cats.data["results"][0]["text"], c_purp)
        self.assertEqual(recent_cats.data["results"][0]["is_archived"], c_arch)

        # Can not update is_archived field of other users' models
        self.client.force_login(self.user2)
        sec_data = {
            "is_archived": not c_arch,
        }
        r_up = self.partially_update_savingcategories(c_id, sec_data)
        self.assertEqual(r_up.status_code, status.HTTP_404_NOT_FOUND)

        # User 1 can still see the original data
        self.client.force_login(self.user)
        # Assert user2 did not accidentally create new saving categories
        recent_cats = self.get_savingcategories()
        self.assertEqual(recent_cats.status_code, status.HTTP_200_OK)
        self.assertEqual(len(recent_cats.data["results"]), 1)
        self.assertEqual(recent_cats.data["count"], 1)

        # Assert the update correctly updated the model
        self.assertEqual(recent_cats.data["results"][0]["text"], c_purp)
        self.assertEqual(recent_cats.data["results"][0]["is_archived"], c_arch)

    def test_can_not_delete_own_savingcategories(self):
        self.client.force_login(self.user)

        # Create a SavingCategory
        r, _ = self.create_savingcategory()
        self.assertEqual(r.status_code, status.HTTP_201_CREATED)

        s_cats = self.get_savingcategories()
        self.assertEqual(len(s_cats.data["results"]), 1)
        self.assertEqual(s_cats.data["count"], 1)

        c = s_cats.data["results"][0]
        c_id = c["id"]
        c_arch = c["is_archived"]
        c_purp = c["text"]

        # # User 2 can not entirely update other users' models
        # self.client.force_login(self.user2)
        r = self.delete_savingcategories(c_id)
        self.assertEqual(r.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # Assert user2 did not accidentally create new saving categories
        recent_cats = self.get_savingcategories()
        self.assertEqual(len(recent_cats.data["results"]), 1)
        self.assertEqual(recent_cats.data["count"], 1)

        # Assert the update failed, and the data has not changed
        self.assertEqual(recent_cats.data["results"][0]["text"], c_purp)
        self.assertEqual(recent_cats.data["results"][0]["is_archived"], c_arch)

    def test_can_not_delete_other_users_savingcategories(self):
        self.client.force_login(self.user)

        # Create a SavingCategory
        r, _ = self.create_savingcategory()
        self.assertEqual(r.status_code, status.HTTP_201_CREATED)

        s_cats = self.get_savingcategories()
        self.assertEqual(len(s_cats.data["results"]), 1)
        self.assertEqual(s_cats.data["count"], 1)

        c = s_cats.data["results"][0]
        c_id = c["id"]
        c_arch = c["is_archived"]
        c_purp = c["text"]

        # # User 2 can not entirely update other users' models
        self.client.force_login(self.user2)
        r = self.delete_savingcategories(c_id)
        self.assertEqual(r.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # User 1 can still see the original data
        self.client.force_login(self.user)
        recent_cats = self.get_savingcategories()
        # Assert user2 did not accidentally create new saving categories
        self.assertEqual(len(recent_cats.data["results"]), 1)
        self.assertEqual(recent_cats.data["count"], 1)

        # Assert the update failed, and the data has not changed
        self.assertEqual(recent_cats.data["results"][0]["text"], c_purp)
        self.assertEqual(recent_cats.data["results"][0]["is_archived"], c_arch)

    @patch("api.v1.views.saving_category_views.SavingCategoryList")
    @patch("budgets.serializers.SavingCategorySerializer")
    def test_integrity_errors_are_catched_on_create(
        self, mock_cat_serializer, _
    ):
        self.client.force_login(self.user)

        # Create an SavingCategory
        response, _ = self.create_savingcategory()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Force the serializer to Throw an Integrity error
        serializer = mock_cat_serializer.return_value
        msg = "I AM A MOCKED INTEGRITY ERROR. I HOPE YOU CATCH ME: ROAAAARR!"
        serializer.save.side_effect = IntegrityError(msg)

        # Create a mock request
        ok_data = {"text": BaseTest.generate_string(20)}
        # Create an instance of the view
        view = SavingCategoryList()
        request = self.client.post(reverse("api:v1:savingcategories"), ok_data)

        # Set the request user
        view.request = request
        request.user = self.user

        # Confirm that the mocks works, and that ok_data actually thew
        # an exception and confirm it was correctly handled by the backend
        failed_res = view.perform_create(serializer)
        self.assertEqual(
            failed_res.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR
        )
        msg = (
            "Generic error thrown while creating the SavingCategory. "
            "Refer the web container logs if you're the admin"
        )
        self.assertEqual(failed_res.data["non_field_errors"][0], msg)

    @patch("api.v1.views.saving_category_views.SavingCategoryDetails")
    @patch("budgets.serializers.SavingCategorySerializer")
    def test_integrity_errors_are_catched_on_update(
        self, mock_cat_serializer, _
    ):
        self.client.force_login(self.user)

        # Create an SavingCategory
        response, _ = self.create_savingcategory()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        s_cats = self.get_savingcategories()
        self.assertEqual(len(s_cats.data["results"]), 1)
        self.assertEqual(s_cats.data["count"], 1)

        c = s_cats.data["results"][0]
        c_id = c["id"]
        c_arch = c["is_archived"]

        # Force the serializer to Throw an Integrity error
        serializer = mock_cat_serializer.return_value
        msg = (
            "I AM A MOCKED INTEGRITY ERROR for PATCH. I HOPE YOU CATCH ME"
            ": ROAAAARR!"
        )
        serializer.save.side_effect = IntegrityError(msg)

        # Create a mock request for PATCH
        ok_data = {"text": BaseTest.generate_string(20)}
        # Create an instance of the view
        view = SavingCategoryDetails()
        patch_url = reverse("api:v1:savingcategory", kwargs={"pk": c_id})
        request = self.client.patch(patch_url, ok_data)

        # Set the request user
        view.request = request
        request.user = self.user

        # Confirm that the mocks works, and that ok_data actually thew
        # an exception and confirm it was correctly handled by the backend
        failed_res = view.perform_update(serializer)
        self.assertEqual(
            failed_res.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR
        )
        msg = (
            "Generic error thrown while updating the SavingCategory. "
            "Refer the web container logs if you're the admin"
        )
        self.assertEqual(failed_res.data["non_field_errors"][0], msg)

        # Force the serializer to Throw an Integrity error
        serializer = mock_cat_serializer.return_value
        msg = (
            "I AM A MOCKED INTEGRITY ERROR for PUT. I HOPE YOU CATCH ME"
            ": ROAAAARR!"
        )
        serializer.save.side_effect = IntegrityError(msg)

        # Create a mock request for PUT
        ok_data = {
            "text": BaseTest.generate_string(20),
            "is_archived": not c_arch,
        }
        # Create an instance of the view
        view = SavingCategoryDetails()
        patch_url = reverse("api:v1:savingcategory", kwargs={"pk": c_id})
        request = self.client.put(patch_url, ok_data)

        # Set the request user
        view.request = request
        request.user = self.user

        # Confirm that the mocks works, and that ok_data actually thew
        # an exception and confirm it was correctly handled by the backend
        failed_res = view.perform_update(serializer)
        self.assertEqual(
            failed_res.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR
        )
        msg = (
            "Generic error thrown while updating the SavingCategory. "
            "Refer the web container logs if you're the admin"
        )
        self.assertEqual(failed_res.data["non_field_errors"][0], msg)
