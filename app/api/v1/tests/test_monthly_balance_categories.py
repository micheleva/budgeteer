# Copyright: (c) 2021, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
import logging
import random
from unittest.mock import patch
from unittest import skip
from urllib.parse import urlencode

from django.contrib.auth.models import User
from django.db.utils import IntegrityError
from django.urls import reverse
from rest_framework import status

from api.v1.tests.test_base import BaseApiTest
from api.v1.views.monthly_balance_category_views import (
    MonthlyBalanceCategoryDetail,
)
from api.v1.views.monthly_balance_category_views import (
    MonthlyBalanceCategoryList,
)

from budgets.models import MonthlyBalanceCategory
from budgets.tests.base import BaseTest

logger = logging.getLogger(__name__)


class MonthlyBalanceCategoryTest(BaseApiTest):
    """
    Test MonthlyBalanceCategory model related v1 rest api behaviour
    """

    def setUp(self):  # pylint: disable=C0103; # noqa
        # Instrument the unit tests to fail if they run for too long
        super().setUp()
        self.user = User.objects.create_user(
            username=BaseTest.generate_string(20),
            password=BaseTest.generate_string(20),
        )
        self.user2 = User.objects.create_user(
            username=BaseTest.generate_string(20),
            password=BaseTest.generate_string(20),
        )

    def test_create_and_list_monthly_balance_categories(self):
        """
        Ensure we can create a new monthly balance category object,
        and other users can't see it
        """
        self.client.force_login(self.user)

        # Create a monthly balance category
        category_response, _ = self.create_monthly_balance_category()
        category_text = category_response.data["text"]
        # fmt: off
        created_cat = (
            MonthlyBalanceCategory.objects.get()  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        self.assertEqual(
            category_response.status_code, status.HTTP_201_CREATED
        )
        self.assertEqual(created_cat.text, category_text)

        monthly_balance_categoriy_list_url = reverse(
            "api:v1:monthly_balance_categories"
        )
        response = self.client.get(
            monthly_balance_categoriy_list_url, format="json"
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["text"], category_text)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)

        # Another user CANNOT see that monthly balance category
        self.client.force_login(self.user2)

        response = self.client.get(
            monthly_balance_categoriy_list_url, format="json"
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 0)
        self.assertEqual(len(response.data["results"]), 0)

    def test_can_filter_monthly_balance_categories_by_name(self):
        self.client.force_login(self.user)

        # Create a monthly balance category
        category_response, _ = self.create_monthly_balance_category()
        category_text = category_response.data["text"]
        # fmt: off
        created_cat = (
            MonthlyBalanceCategory.objects.get()  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        self.assertEqual(
            category_response.status_code, status.HTTP_201_CREATED
        )
        self.assertEqual(created_cat.text, category_text)

        url = reverse("api:v1:monthly_balance_categories")
        mb_filtered_url = url + "?" + urlencode({"name": category_text})
        response = self.client.get(mb_filtered_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["text"], category_text)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)

        url = reverse("api:v1:monthly_balance_categories")
        mb_filtered_url = (
            url + "?" + urlencode({"name": BaseTest.generate_string(40)})
        )
        response = self.client.get(mb_filtered_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 0)
        self.assertEqual(len(response.data["results"]), 0)

    def test_create_and_retrieve_single_monthly_balance_categories(self):
        self.client.force_login(self.user)

        # Create a monthly balance category
        cat_r, _ = self.create_monthly_balance_category()
        category_text = cat_r.data["text"]
        category_id = cat_r.data["id"]

        self.assertEqual(cat_r.status_code, status.HTTP_201_CREATED)

        detail_url = reverse(
            "api:v1:monthly_balance_category", kwargs={"pk": category_id}
        )
        res = self.client.get(detail_url, format="json")
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data["text"], category_text)

        mc_cat_url = reverse("api:v1:monthly_balance_categories")
        response = self.client.get(mc_cat_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)
        self.assertEqual(response.data["results"][0]["text"], category_text)

    # WRITE ME <==
    @skip
    def test_update_monthly_balance_categories(self):
        pass

    # WRITE ME <==
    @skip
    def test_updating_monthly_balance_category_with_malformed_data_will_fail(
        self,
    ):
        # in particular confirm that non existing categories
        # (or other users' categories) will fail
        pass

    # WRITE ME <==
    @skip
    def test_updating_monthly_balance_category_to_created_duplicated_data_will_fail(
        self,
    ):
        pass

    def test_can_not_create_monthly_balance_category_for_other_users(self):
        """
        Ensure we can NOT create a new monthly balance object linked to
        another user by posting other users' ID in the payload
        """
        # Another user tries to create an Expense object and have it linked to
        # the initial user's ID
        self.client.force_login(self.user2)

        # Try to inject first user ownserhip using a monthly balance category
        # belonging to the intial user: the monthly balance category creation
        # should fail hard
        create_list_url = reverse("api:v1:monthly_balance_categories")
        text = BaseTest.generate_string(20)
        is_foreign_currency = False
        data = {
            "text": text,
            "is_foreign_currency": is_foreign_currency,
            "created_by": self.user.id,
        }
        res = self.client.post(create_list_url, data, format="json")

        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            MonthlyBalanceCategory.objects.get(  # pylint: disable=E1101; # noqa
                created_by=self.user2.id
            ).text,
            text,
        )
        # fmt: on
        # The monthly balance object is attatched to the currently logged in
        # user (other_user)
        response = self.client.get(create_list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["text"], text)
        self.assertEqual(response.data["count"], 1)

        # The initial user logs in, and finds no MonthlyBalanceCategories,
        # as this user created none
        self.client.force_login(self.user)
        response = self.client.get(create_list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 0)

    def test_malformed_request_do_not_create_monthly_balance_categories(self):
        """
        Ensure we can not create new monthly balance category objects if the
        post data is malformed
        """
        self.client.force_login(self.user)

        create_url = reverse("api:v1:monthly_balance_categories")

        # Leave out Text
        is_foreign_currency = False
        data = {"is_foreign_currency": is_foreign_currency}
        response = self.client.post(create_url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(response.data["text"][0].code, "required")

        # Leave out is_foreign_currency
        text = text = BaseTest.generate_string(20)
        data = {"text": text}
        response = self.client.post(create_url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(response.data), 1)
        self.assertEqual(
            response.data["is_foreign_currency"][0].code, "required"
        )

        # Use too long string in Text (41 chars)
        text = text = BaseTest.generate_string(41)
        data = {"text": text}
        response = self.client.post(create_url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.data["text"][0].code, "max_length")
        self.assertEqual(
            response.data["is_foreign_currency"][0].code, "required"
        )

        # Use None in Text
        data = {"text": None}
        response = self.client.post(create_url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.data["text"][0].code, "null")
        self.assertEqual(
            response.data["is_foreign_currency"][0].code, "required"
        )

        # Use string in is_foreign_currency
        is_foreign_currency = "I am surely not a boolean"
        data = {"is_foreign_currency": is_foreign_currency}
        response = self.client.post(create_url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.data["text"][0].code, "required")
        self.assertEqual(
            response.data["is_foreign_currency"][0].code, "invalid"
        )

        # Use float in is_foreign_currency
        is_foreign_currency = 4.2
        data = {"is_foreign_currency": is_foreign_currency}
        response = self.client.post(create_url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.data["text"][0].code, "required")
        self.assertEqual(
            response.data["is_foreign_currency"][0].code, "invalid"
        )

        # Use None in is_foreign_currency
        is_foreign_currency = None
        data = {"is_foreign_currency": is_foreign_currency}
        response = self.client.post(create_url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(response.data), 2)
        self.assertEqual(response.data["is_foreign_currency"][0].code, "null")
        self.assertEqual(response.data["text"][0].code, "required")

    # NOTE: this is a regression test, as we've removed the ability to
    # delete monthly balances categories
    def test_regression_212_can_not_delete_own_monthly_balance_categories(
        self,
    ):
        self.client.force_login(self.user)

        # Create a monthly balance category
        category_response, _ = self.create_monthly_balance_category()
        category_text = category_response.data["text"]
        # fmt: off
        created_cat = MonthlyBalanceCategory.objects.get(  # pylint: disable=E1101; # noqa
            created_by=self.user.id
        )
        # fmt: on
        self.assertEqual(
            category_response.status_code, status.HTTP_201_CREATED
        )
        self.assertEqual(created_cat.text, category_text)

        to_delete_id = created_cat.id
        res, _ = self.delete_monthly_balance_category(to_delete_id)
        self.assertEqual(res.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        monthly_balance_categoriy_list_url = reverse(
            "api:v1:monthly_balance_categories"
        )
        response = self.client.get(
            monthly_balance_categoriy_list_url, format="json"
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)
        self.assertEqual(response.data["results"][0]["text"], category_text)
        self.assertEqual(response.data["results"][0]["id"], created_cat.id)

    def test_can_not_delete_other_users_monthly_balance_categories(self):
        self.client.force_login(self.user)

        # Create a monthly balance category
        category_response, _ = self.create_monthly_balance_category()
        category_text = category_response.data["text"]
        # fmt: off
        created_cat = MonthlyBalanceCategory.objects.get(  # pylint: disable=E1101; # noqa
            created_by=self.user.id
        )
        # fmt: on
        self.assertEqual(
            category_response.status_code, status.HTTP_201_CREATED
        )
        self.assertEqual(created_cat.text, category_text)

        # Another user CANNOT see that monthly balance category
        self.client.force_login(self.user2)

        to_delete_id = created_cat.id
        res, _ = self.delete_monthly_balance_category(to_delete_id)
        self.assertEqual(res.status_code, status.HTTP_405_METHOD_NOT_ALLOWED)

        # The original user can still see their own monthly balance category
        self.client.force_login(self.user)
        monthly_balance_categoriy_list_url = reverse(
            "api:v1:monthly_balance_categories"
        )
        response = self.client.get(
            monthly_balance_categoriy_list_url, format="json"
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["text"], created_cat.text)
        self.assertEqual(response.data["results"][0]["id"], created_cat.id)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)

    @patch(
        "api.v1.views.monthly_balance_category_views.MonthlyBalanceCategoryList"
    )
    @patch("budgets.serializers.MonthlyBalanceCategorySerializer")
    def test_integrity_errors_are_catched_on_create(
        self, mock_cat_serializer, _
    ):
        self.client.force_login(self.user)
        cat_r, _ = self.create_monthly_balance_category()
        self.assertEqual(cat_r.status_code, status.HTTP_201_CREATED)

        # Force the serializer to Throw an Integrity error
        serializer = mock_cat_serializer.return_value
        msg = "I AM A MOCKED INTEGRITY ERROR. I HOPE YOU CATCH ME: ROAAAARR!"
        serializer.save.side_effect = IntegrityError(msg)

        # Create a mock request
        ok_data = {
            "text": BaseTest.generate_string(20),
            "is_foreign_currency": random.choice([True, False]),
        }

        # Create an instance of the class view
        view = MonthlyBalanceCategoryList()
        request = self.client.post(
            reverse("api:v1:monthly_balance_categories"), ok_data
        )

        # Set the request user
        view.request = request
        request.user = self.user

        # Confirm that the mocks works, and that ok_data actually thew an
        # exception and confirm it was correctly handled by the backend
        failed_res = view.perform_create(serializer)
        self.assertEqual(
            failed_res.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR
        )
        msg = (
            "Generic error thrown while creating the Monthly Balance Category."
            " Refer the web container logs if you're the admin"
        )
        self.assertEqual(failed_res.data["non_field_errors"][0], msg)

    @skip
    @patch(
        "api.v1.views.monthly_balance_category_views.MonthlyBalanceCategoryDetail"
    )
    @patch("budgets.serializers.MonthlyBalanceCategorySerializer")
    def test_integrity_errors_are_catched_on_update(
        self, mock_cat_serializer, _
    ):
        self.client.force_login(self.user)
        cat_r, _ = self.create_monthly_balance_category()
        self.assertEqual(cat_r.status_code, status.HTTP_201_CREATED)

        monthly_balance_categoriy_list_url = reverse(
            "api:v1:monthly_balance_categories"
        )
        response = self.client.get(
            monthly_balance_categoriy_list_url, format="json"
        )

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)
        cat_id = response.data["results"][0]["id"]
        # TODO: update it
        # TODO: confirm update had code 200

        # Force the serializer to Throw an Integrity error
        serializer = mock_cat_serializer.return_value
        msg = "I AM A MOCKED INTEGRITY ERROR. I HOPE YOU CATCH ME: ROAAAARR!"
        serializer.save.side_effect = IntegrityError(msg)

        # TODO: update it AGAIN

        # Create a mock request
        ok_data = {
            "text": BaseTest.generate_string(20),
            "is_foreign_currency": random.choice([True, False]),
        }

        # Create an instance of the class view
        view = MonthlyBalanceCategoryDetail()
        edit_url = reverse(
            "api:v1:monthly_balance_category", kwargs={"pk": cat_id}
        )
        request = self.client.post(edit_url, ok_data)

        # Set the request user
        view.request = request
        request.user = self.user

        # Confirm that the mocks works, and that ok_data actually thew an
        # exceptionand confirm it was correctly handled by the backend
        res = view.perform_update(serializer)
        self.assertEqual(
            res.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR
        )
        msg = (
            "Generic error thrown while editing the MonthlyBalance. "
            "Refer the web container logs if you're the admin"
        )
        self.assertEqual(res.data["non_field_errors"][0], msg)

    @skip
    # @patch('api.v1.views.monthly_balance_category_views.MonthlyBalanceCreateOrUpdate')
    @patch("budgets.serializers.MonthlyBalanceSerializerForCreateUpdate")
    def test_integrity_errors_are_catched_on_create_without_using_pk(
        self, mock_cat_serializer, _
    ):
        self.client.force_login(self.user)
        cat_r, _ = self.create_monthly_balance_category()
        self.assertEqual(cat_r.status_code, status.HTTP_201_CREATED)
        self.fail("i need to be written")

    @skip
    # @patch('api.v1.views.monthly_balance_category_views.MonthlyBalanceCreateOrUpdate')
    @patch("budgets.serializers.MonthlyBalanceSerializerForCreateUpdate")
    def test_integrity_errors_are_catched_on_update_without_using_pk(
        self, mock_cat_serializer, _
    ):
        self.client.force_login(self.user)
        cat_r, _ = self.create_monthly_balance_category()
        self.assertEqual(cat_r.status_code, status.HTTP_201_CREATED)
        self.fail("i need to be written")
