# Copyright: (c) 2021, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from datetime import date
import logging
import random
from unittest.mock import patch
from unittest import skip


from django.contrib.auth.models import User
from django.db.utils import IntegrityError
from django.urls import reverse
from rest_framework import status

from api.v1.tests.test_base import BaseApiTest
from api.v1.views.account_views import AccountDetail
from api.v1.views.account_views import AccountList

import budgets.models as m
from budgets.tests.base import BaseTest

logger = logging.getLogger(__name__)


class AccountTest(BaseApiTest):
    """
    Test account model related v1 rest api behaviour
    """

    def setUp(self):  # pylint: disable=C0103; # noqa
        self.user = User.objects.create_user(
            username=BaseTest.generate_string(20),
            password=BaseTest.generate_string(20),
        )
        self.user2 = User.objects.create_user(
            username=BaseTest.generate_string(20),
            password=BaseTest.generate_string(20),
        )

    def test_create_and_list_accounts(self):
        """
        Ensure we can create a new account object, and other users can't see it
        """
        self.client.force_login(self.user)

        # Create an account
        response, data = self.create_account()
        acc = m.Account.objects.filter(  # pylint: disable=E1101; # noqa
            created_by=self.user.id
        ).first()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            m.Account.objects.filter(created_by=self.user.id).count(), 1  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        self.assertEqual(acc.text, data["text"])
        self.assertEqual(acc.credit_entity, data["credit_entity"])
        self.assertEqual(acc.is_foreign_currency, data["is_foreign_currency"])

        # Same user (the owner) can see that account
        list_url = reverse("api:v1:accounts")
        r = self.client.get(list_url, format="json")

        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(r.data["count"], 1)
        self.assertEqual(r.data["results"][0]["text"], data["text"])
        self.assertEqual(
            r.data["results"][0]["credit_entity"], data["credit_entity"]
        )
        self.assertEqual(
            r.data["results"][0]["is_foreign_currency"],
            data["is_foreign_currency"],
        )

    def test_can_not_create_accounts_for_other_users(self):
        """
        Ensure we can NOT create a new account object linked to another user
        by posting other users' ID in the payload
        """
        self.client.force_login(self.user)

        # Another user tries to create an Account object and have it linked
        # to the initial user's ID
        self.client.force_login(self.user2)

        # Try to inject first user ownserhip using an Account belonging
        # to the intial user:
        # the Account creation should succeed ignoring created_by
        create_list_url = reverse("api:v1:accounts")

        text = BaseTest.generate_string(20)
        credit_entity_text = BaseTest.generate_string(20)
        is_foreign_currency = random.choice([True, False])
        data = {
            "text": text,
            "is_foreign_currency": is_foreign_currency,
            "credit_entity": credit_entity_text,
            "created_by": self.user.id,
        }
        res = self.client.post(create_list_url, data, format="json")

        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            m.Account.objects.get(created_by=self.user2.id).text, text  # pylint: disable=E1101; # noqa
        )

        # fmt: on
        # The Account balance object is attatched to the currently logged in
        # user (other_user)
        response = self.client.get(create_list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["text"], text)
        self.assertEqual(response.data["count"], 1)

        # The initial user logs in, and finds no Accounts, as this user
        # created none
        self.client.force_login(self.user)
        response = self.client.get(create_list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 0)

    def test_malformed_account_creation_will_fail(self):
        self.client.force_login(self.user)

        # Try with None text
        create_url = reverse("api:v1:accounts")
        text = None
        credit_entity = BaseTest.generate_string(20)
        data = {
            "text": text,
            "credit_entity": credit_entity,
            "is_foreign_currency": False,
        }
        res = self.client.post(create_url, data, format="json")

        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(res.data["text"][0], "This field may not be null.")
        self.assertEqual(res.data["text"][0].code, "null")

        # Try with too long text
        text = BaseTest.generate_string(21)
        credit_entity = BaseTest.generate_string(20)
        data = {
            "text": text,
            "credit_entity": credit_entity,
            "is_foreign_currency": False,
        }
        res = self.client.post(create_url, data, format="json")

        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        err_msg = "Ensure this field has no more than 20 characters."
        self.assertEqual(res.data["text"][0], err_msg)
        self.assertEqual(res.data["text"][0].code, "max_length")

        # Try with None credity_entity
        text = BaseTest.generate_string(20)
        credit_entity = None
        data = {
            "text": text,
            "credit_entity": credit_entity,
            "is_foreign_currency": False,
        }
        res = self.client.post(create_url, data, format="json")

        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            res.data["credit_entity"][0], "This field may not be null."
        )
        self.assertEqual(res.data["credit_entity"][0].code, "null")

        # Try with too long credit_entity
        text = BaseTest.generate_string(20)
        credit_entity = BaseTest.generate_string(21)
        data = {
            "text": text,
            "credit_entity": credit_entity,
            "is_foreign_currency": False,
        }
        res = self.client.post(create_url, data, format="json")

        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        err_msg = "Ensure this field has no more than 20 characters."
        self.assertEqual(res.data["credit_entity"][0], err_msg)
        self.assertEqual(res.data["credit_entity"][0].code, "max_length")

        # Try with a string for is_foreign_currency
        text = BaseTest.generate_string(20)
        credit_entity = BaseTest.generate_string(20)
        is_foreign_currency = BaseTest.generate_string(20)
        data = {
            "text": text,
            "credit_entity": credit_entity,
            "is_foreign_currency": is_foreign_currency,
        }
        res = self.client.post(create_url, data, format="json")

        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            res.data["is_foreign_currency"][0], "Must be a valid boolean."
        )
        self.assertEqual(res.data["is_foreign_currency"][0].code, "invalid")

    # Can update the model with PUT requests
    # WRITE ME
    @skip
    def test_users_can_entirely_edit_accounts(self):
        pass

    # Can update the model with PATCH requests
    # WRITE ME
    @skip
    def test_users_can_partially_edit_accounts(self):
        pass

    def test_can_update_accounts_without_changing_text_bug153_test(self):
        self.client.force_login(self.user)

        # Create an account
        response, data = self.create_account()

        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            m.Account.objects.filter(created_by=self.user.id).count(), 1  # pylint: disable=E1101; # noqa
        )
        self.assertEqual(
            m.Account.objects.get(
                created_by=self.user.id
            ).text, data["text"]  # pylint: disable=E1101; # noqa
        )
        self.assertEqual(
            m.Account.objects.get(
                created_by=self.user.id
            ).credit_entity, data["credit_entity"]  # pylint: disable=E1101; # noqa
        )
        self.assertEqual(
            m.Account.objects.get(
                created_by=self.user.id
            ).is_foreign_currency,  # pylint: disable=E1101; # noqa
            data["is_foreign_currency"],
        )
        # fmt: on
        # Same user (the owner) can see that account
        list_url = reverse("api:v1:accounts")
        response = self.client.get(list_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(response.data["results"][0]["text"], data["text"])
        self.assertEqual(
            response.data["results"][0]["credit_entity"], data["credit_entity"]
        )
        self.assertEqual(
            response.data["results"][0]["is_foreign_currency"],
            data["is_foreign_currency"],
        )
        acc_id = response.data["results"][0]["id"]

        # Update all fields but 'text' will not trigger a
        # "An Account with the same text AND credit_entity already exists
        # for this user." i.e. not BUGFIX-253 regression
        new_credit_entity = BaseTest.generate_string(20)
        new_is_foreign_currency = not data["is_foreign_currency"]
        update_data = {
            "credit_entity": new_credit_entity,
            "is_foreign_currency": new_is_foreign_currency,
        }
        # Can update the model with PATCH requests
        update_res = self.partial_update_account(acc_id, update_data)
        self.assertEqual(update_res.status_code, status.HTTP_200_OK)
        self.assertEqual(update_res.data["id"], acc_id)
        self.assertEqual(update_res.data["credit_entity"], new_credit_entity)
        self.assertEqual(
            update_res.data["is_foreign_currency"], new_is_foreign_currency
        )

        r2 = self.client.get(list_url, format="json")
        self.assertEqual(r2.status_code, status.HTTP_200_OK)
        self.assertEqual(r2.data["count"], 1)
        self.assertEqual(r2.data["results"][0]["id"], acc_id)
        self.assertEqual(r2.data["results"][0]["text"], data["text"])
        self.assertEqual(
            r2.data["results"][0]["credit_entity"], new_credit_entity
        )
        self.assertEqual(
            r2.data["results"][0]["is_foreign_currency"],
            new_is_foreign_currency,
        )

    def test_users_can_not_see_other_users_accounts(self):
        self.client.force_login(self.user)

        # Create an account
        response, data = self.create_account()

        acc = m.Account.objects.get(
            created_by=self.user.id
        )  # pylint: disable=E1101; # noqa
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            m.Account.objects.filter(created_by=self.user.id).count(),
            1,  # pylint: disable=E1101; # noqa
        )
        self.assertEqual(acc.text, data["text"])
        self.assertEqual(acc.credit_entity, data["credit_entity"])
        self.assertEqual(acc.is_foreign_currency, data["is_foreign_currency"])

        # Same user (the owner) can see that account
        list_url = reverse("api:v1:accounts")
        response = self.client.get(list_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(response.data["results"][0]["text"], data["text"])
        self.assertEqual(
            response.data["results"][0]["credit_entity"], data["credit_entity"]
        )
        self.assertEqual(
            response.data["results"][0]["is_foreign_currency"],
            data["is_foreign_currency"],
        )

        # Another user CANNOT see that account
        self.client.force_login(self.user2)
        response = self.client.get(list_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 0)

    # Can update the model with PUT requests
    # WRITE ME
    @skip
    def test_users_can_not_entirely_edit_other_users_accounts(self):
        pass

    # WRITE ME
    @skip
    def test_users_can_not_partially_edit_other_users_accounts(self):
        pass

    @skip
    def test_can_not_delete_other_users_accounts(self):
        # We do even yet allow own's accounts deletion, hence this case can not be written atm
        pass

    @patch("api.v1.views.account_views.AccountList")
    @patch("budgets.serializers.AccountSerializer")
    def test_integrity_errors_are_catched_on_create(
        self, mock_cat_serializer, _
    ):
        self.client.force_login(self.user)

        # Create an account
        response, _ = self.create_account()
        acc = m.Account.objects.filter(  # pylint: disable=E1101; # noqa
            created_by=self.user.id
        ).first()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        # Force the serializer to Throw an Integrity error
        serializer = mock_cat_serializer.return_value
        msg = "I AM A MOCKED INTEGRITY ERROR. I HOPE YOU CATCH ME: ROAAAARR!"
        serializer.save.side_effect = IntegrityError(msg)

        # Create a mock request
        ok_data = {
            "text": BaseTest.generate_string(20),
            "credit_entity": BaseTest.generate_string(20),
            "is_foreign_currency": False,
        }
        # Create an instance of the view
        view = AccountList()
        request = self.client.post(reverse("api:v1:accounts"), ok_data)

        # Set the request user
        view.request = request
        request.user = self.user

        # Confirm that the mocks works, and that ok_data actually thew an
        # exception and confirm it was correctly handled by the backend
        failed_res = view.perform_create(serializer)
        self.assertEqual(
            failed_res.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR
        )
        msg = (
            "Generic error thrown while creating the Account. "
            "Refer the web container logs if you're the admin"
        )
        self.assertEqual(failed_res.data["non_field_errors"][0], msg)

    @skip  # This view does not yet catch integrity errors
    @patch("api.v1.views.account_views.AccountDetail")
    @patch("budgets.serializers.AccountSerializer")
    def test_integrity_errors_are_catched_on_update(
        self, mock_cat_serializer, _
    ):
        self.client.force_login(self.user)

        # Create an account
        response, data = self.create_account()
        acc = m.Account.objects.filter(  # pylint: disable=E1101; # noqa
            created_by=self.user.id
        ).first()
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)

        list_url = reverse("api:v1:accounts")
        response = self.client.get(list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        acc_id = response.data["results"][0]["id"]

        # Confrim we can update before mocking
        ok_data = {
            "text": BaseTest.generate_string(20),
            "credit_entity": BaseTest.generate_string(20),
            "is_foreign_currency": not data["is_foreign_currency"],
        }
        update_r = self.partial_update_account(acc_id, ok_data)
        self.assertEqual(update_r.status_code, status.HTTP_200_OK)
        self.assertEqual(update_r.data["id"], acc_id)
        self.assertEqual(
            update_r.data["credit_entity"], ok_data["credit_entity"]
        )
        self.assertEqual(
            update_r.data["is_foreign_currency"],
            ok_data["is_foreign_currency"],
        )

        list_url = reverse("api:v1:accounts")
        list_r = self.client.get(list_url, format="json")
        self.assertEqual(list_r.status_code, status.HTTP_200_OK)
        self.assertEqual(list_r.data["count"], 1)
        self.assertEqual(list_r.data["results"][0]["id"], acc_id)
        self.assertEqual(list_r.data["results"][0]["text"], ok_data["text"])
        self.assertEqual(
            list_r.data["results"][0]["credit_entity"],
            ok_data["credit_entity"],
        )
        self.assertEqual(
            list_r.data["results"][0]["is_foreign_currency"],
            ok_data["is_foreign_currency"],
        )

        # Force the serializer to Throw an Integrity error
        serializer = mock_cat_serializer.return_value
        msg = "I AM A MOCKED INTEGRITY ERROR. I HOPE YOU CATCH ME: ROAAAARR!"
        serializer.save.side_effect = IntegrityError(msg)

        # Create a mock request
        ok_data = {
            "text": BaseTest.generate_string(20),
            "credit_entity": BaseTest.generate_string(20),
            "is_foreign_currency": not data["is_foreign_currency"],
        }
        # Create an instance of the view
        view = AccountDetail()
        request = self.client.post(
            reverse("api:v1:account", kwargs={"pk": acc_id}), ok_data
        )

        # Set the request user
        view.request = request
        request.user = self.user

        # Confirm that the mocks works, and that ok_data actually thew an
        # exception and confirm it was correctly handled by the backend
        failed_res = view.perform_update(serializer)
        self.assertEqual(
            failed_res.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR
        )
        msg = (
            "Generic error thrown while creating the Account. "
            "Refer the web container logs if you're the admin"
        )
        self.assertEqual(failed_res.data["non_field_errors"][0], msg)
        self.fail("copied")
