# Copyright: (c) 2021, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from datetime import date
from datetime import datetime, timedelta
import logging
import random
from unittest.mock import patch
from unittest import skip
from urllib.parse import urlencode

from django.contrib.auth.models import User
from django.db.utils import IntegrityError
from django.urls import reverse

from rest_framework import status
from rest_framework.exceptions import MethodNotAllowed

from api.v1.tests.test_base import BaseApiTest
from api.v1.views.monthly_balance_views import MonthlyBalanceCreateOrUpdate
from api.v1.views.monthly_balance_views import MonthlyBalanceList

from budgets.serializers import MonthlyBalanceSerializerWithNestedCategory
import budgets.models as m

from budgets.tests.base import BaseTest


logger = logging.getLogger(__name__)


class MonthlyBalanceTest(BaseApiTest):
    """
    Test MonthlyBalance model related v1 rest api behaviour
    """

    def setUp(self):  # pylint: disable=C0103; # noqa
        # Instrument the unit tests to fail if they run for too long
        super().setUp()
        # fmt: off
        self.user = User.objects.create_user(  # pylint: disable=E1101; # noqa
            username=BaseTest.generate_string(20),
            password=BaseTest.generate_string(20),
        )
        self.user2 = User.objects.create_user(  # pylint: disable=E1101; # noqa
            username=BaseTest.generate_string(20),
            password=BaseTest.generate_string(20),
        )
        # fmt: on

    def test_serializer(self):
        self.client.force_login(self.user)

        # Create a category
        category_response, _ = self.create_monthly_balance_category()
        category_id = category_response.data["id"]

        # Can not create monthly balance using MonthlyBalanceSerializerWithNestedCategory
        data = {
            "category": category_id,
            "amount": random.randint(1, 90000),
            "date": date.today().isoformat(),
        }
        context = {
            "request": type("Request", (), {"user": self.user})(),
            "user_id": self.user.id,
        }
        serializer = MonthlyBalanceSerializerWithNestedCategory(
            data=data, context=context
        )

        with self.assertRaises(MethodNotAllowed) as context:
            serializer.is_valid(raise_exception=True)
            serializer.save()
            msg = (
                "Creation via this serializer is not allowed."
                "Use MonthlyBalanceSerializerForCreation instead!"
            )
            self.assertIn(msg, context.exception.detail)
            self.assertIn("method_not_allowed", context.exception.code)

    def test_serializer_validate_date(self):
        logger.warning("implement test_serializer_validate_date")

    def test_create_and_list_monthly_balances(self):
        """
        Ensure we can create a new monthly balance object, and that we can
        list it as well
        """
        self.client.force_login(self.user)

        # Create a category
        category_response, _ = self.create_monthly_balance_category()
        category_id = category_response.data["id"]

        balance_response, _ = self.create_monthly_balance(category_id)
        balance_amount = balance_response.data["amount"]
        balance_date = balance_response.data["date"]

        self.assertEqual(balance_response.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            m.MonthlyBalance.objects.get(created_by=self.user).amount,  # pylint: disable=E1101; # noqa
            balance_amount,
        )
        self.assertEqual(
            m.MonthlyBalance.objects.get(  # pylint: disable=E1101; # noqa
                created_by=self.user
            ).date.isoformat(),
            balance_date,
        )
        # fmt: on
        monthly_balance_url = reverse("api:v1:monthly_balances")
        response = self.client.get(monthly_balance_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["date"], balance_date)
        self.assertEqual(response.data["results"][0]["amount"], balance_amount)
        self.assertEqual(
            response.data["results"][0]["category"]["id"], category_id
        )
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)

    def test_can_not_see_other_users_monthly_balances(self):
        """
        Ensure users can not see other users monthly balance objects
        """
        self.client.force_login(self.user)

        # Create a category
        category_response, _ = self.create_monthly_balance_category()
        category_id = category_response.data["id"]

        balance_response, _ = self.create_monthly_balance(category_id)
        balance_amount = balance_response.data["amount"]
        balance_date = balance_response.data["date"]

        self.assertEqual(balance_response.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            m.MonthlyBalance.objects.get().amount, balance_amount  # pylint: disable=E1101; # noqa
        )
        self.assertEqual(
            m.MonthlyBalance.objects.get().date.isoformat(), balance_date  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        monthly_balance_url = reverse("api:v1:monthly_balances")
        response = self.client.get(monthly_balance_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["date"], balance_date)
        self.assertEqual(response.data["results"][0]["amount"], balance_amount)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)

        # Another user CANNOT see that monthly balance
        self.client.force_login(self.user2)

        response = self.client.get(monthly_balance_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 0)
        self.assertEqual(len(response.data["results"]), 0)

    def test_can_not_create_monthly_balance_for_other_users(self):
        """
        Ensure we can NOT create a new monthly balance object linked to
        another user by posting other users' ID in the payload
        """
        self.client.force_login(self.user)

        # Guido asked someone in the IT team to look up Frank's user ID.
        # Guido tries to create an Monthly Balance object and have it linked
        # to Frank's user ID as a prank
        self.client.force_login(self.user2)

        # Create a category
        category_response, _ = self.create_monthly_balance_category()
        category_id = category_response.data["id"]

        # Create an monthly balance object
        create_url = reverse("api:v1:monthly_balances")
        # Guido grins while injecting Frank's user id
        is_foreign_currency = random.choice([True, False])
        record_date = date.today().isoformat()
        amount = random.randint(1, 90000)
        data = {
            "amount": amount,
            "category": category_id,
            "date": record_date,
            "is_foreign_currency": is_foreign_currency,
            "created_by": self.user.id,
        }

        res = self.client.post(create_url, data, format="json")
        category_amount = res.data["amount"]

        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            m.MonthlyBalance.objects.get(  # pylint: disable=E1101; # noqa
                created_by=self.user2.id
            ).amount,
            category_amount,
        )
        # fmt: on
        mb_list = reverse("api:v1:monthly_balances")
        response = self.client.get(mb_list, format="json")

        # Guido is sad: the app does not seem vulnerable to said attack.
        # The Monthly balance was correctly linked to Guido's account
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data["results"][0]["amount"], category_amount
        )
        self.assertEqual(
            response.data["results"][0]["category"]["id"], category_id
        )
        self.assertEqual(response.data["count"], 1)

        # Frank logs in, and he finds no Monthly Balance, as he created none
        self.client.force_login(self.user)

        response = self.client.get(mb_list, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 0)

    def test_create_and_list_monthly_balances_using_filters(self):
        """
        Ensure we can create new monthly balance objects, and the result
        matches the filter conditions
        """
        self.client.force_login(self.user)

        # Create a category
        category_response, _ = self.create_monthly_balance_category()
        first_category_id = category_response.data["id"]
        first_categoty_text = category_response.data["text"]

        # Current month, first category
        balance_response, _ = self.create_monthly_balance(first_category_id)
        balance_amount = balance_response.data["amount"]
        first_balance_date = balance_response.data["date"]
        # fmt: off
        mb = m.MonthlyBalance.objects.get()  # pylint: disable=E1101; # noqa
        # fmt: on
        self.assertEqual(balance_response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(mb.amount, balance_amount)
        self.assertEqual(mb.date.isoformat(), first_balance_date)

        monthly_balance_url = reverse("api:v1:monthly_balances")
        response = self.client.get(monthly_balance_url, format="json")
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)

        # Create a second monthly balance that has a different date
        # will check for this item absence when using filtering API
        # Past month, first category
        create_url = reverse("api:v1:monthly_balances")
        past_amount = random.randint(1, 90000)
        past_record_date = "2005-01-01"
        data = {
            "category": first_category_id,
            "amount": past_amount,
            "date": past_record_date,
        }
        _ = self.client.post(create_url, data, format="json"), data
        self.client.get(monthly_balance_url, format="json")

        # Since 'api:v1:monthly_balances' returns monthly balances for a
        # given month we need to use the orm in order to confirm we have two
        # fmt: off
        self.assertEqual(
            m.MonthlyBalance.objects.all().count(), 2  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        # Create a third monthly balance that has a different category:
        # will check for this item absence when using filtering API
        # Same month, second
        second_category_response, _ = self.create_monthly_balance_category()
        second_category_text = second_category_response.data["text"]
        second_category_id = second_category_response.data["id"]

        third_balance_response, _ = self.create_monthly_balance(
            second_category_id
        )
        _ = third_balance_response.data["amount"]
        _ = third_balance_response.data["date"]

        # fmt: off
        self.assertEqual(
            m.MonthlyBalance.objects.all().count(), 3  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        # use first filtered url /api/v1/monthly_balances?date=xxxx
        filtered_url = (
            monthly_balance_url + "?" + urlencode({"date": first_balance_date})
        )
        r2 = self.client.get(filtered_url, format="json")
        self.assertEqual(r2.status_code, status.HTTP_200_OK)
        self.assertEqual(r2.data["count"], 2)

        self.assertEqual(
            r2.data["results"][0]["category"]["id"], first_category_id
        )
        self.assertEqual(
            r2.data["results"][0]["category"]["text"], first_categoty_text
        )
        self.assertEqual(r2.data["results"][0]["date"], first_balance_date)

        # FIXME: improve the following comment's wording
        # Since we filter by date, the second results should be the third item
        # that uses the second category
        self.assertEqual(
            r2.data["results"][1]["category"]["text"], second_category_text
        )
        self.assertEqual(
            r2.data["results"][1]["category"]["id"], second_category_id
        )
        self.assertEqual(r2.data["results"][1]["date"], first_balance_date)

        # use first filtered url:
        # /api/v1/monthly_balances?date=<date-in-the-past>
        filtered_url = (
            monthly_balance_url + "?" + urlencode({"date": past_record_date})
        )
        r3 = self.client.get(filtered_url, format="json")
        self.assertEqual(r3.status_code, status.HTTP_200_OK)
        self.assertEqual(r3.data["count"], 1)

        # The result should be the second object, that uses the first category
        # but a date in the past
        self.assertEqual(
            r3.data["results"][0]["category"]["id"], first_category_id
        )
        self.assertEqual(
            r3.data["results"][0]["category"]["text"], first_categoty_text
        )
        self.assertEqual(r3.data["results"][0]["amount"], past_amount)
        self.assertEqual(r3.data["results"][0]["date"], past_record_date)

    def test_can_not_create_two_monthly_balances_for_the_same_category_with_the_same_date(
        self,
    ):  # pylint: disable=R0914;# noqa
        self.client.force_login(self.user)

        # Create a monthly balance category
        cat_res, _ = self.create_monthly_balance_category()
        cat_id = cat_res.data["id"]

        create_url = reverse("api:v1:monthly_balances")
        amount = random.randint(1, 90000)
        record_date = "2023-09-02"  # Any date not ending in 01
        record_date_ending_in_01 = record_date[0 : len(record_date) - 2] + "01"
        data = {"category": cat_id, "amount": amount, "date": record_date}
        res = self.client.post(create_url, data, format="json")
        balance_amount = res.data["amount"]
        _ = res.data["date"]

        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            m.MonthlyBalance.objects.get().amount, balance_amount  # pylint: disable=E1101; # noqa
        )
        self.assertNotEqual(
            m.MonthlyBalance.objects.get().date.isoformat(), record_date  # pylint: disable=E1101; # noqa
        )
        self.assertEqual(
            m.MonthlyBalance.objects.get().date.isoformat(),  # pylint: disable=E1101; # noqa
            record_date_ending_in_01,
        )
        # fmt: on
        monthly_balance_url = reverse("api:v1:monthly_balances")
        response = self.client.get(monthly_balance_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["date"], "2023-09-01")
        self.assertEqual(response.data["results"][0]["amount"], balance_amount)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)

        # Try to create another monthly balance for the same date & category
        amount = random.randint(1, 90000)
        data = {"category": cat_id, "amount": amount, "date": record_date}
        res = self.client.post(create_url, data, format="json")

        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        err_msg = (
            "A MonthlyBalance with the same date and category already"
            " exists for this user."
        )
        self.assertEqual(res.data["non_field_errors"][0], err_msg)
        self.assertEqual(res.data["non_field_errors"][0].code, "invalid")

        # Confirm the second request did not create anything
        response = self.client.get(monthly_balance_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["date"], "2023-09-01")
        self.assertEqual(response.data["results"][0]["amount"], balance_amount)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)

    def test_can_not_create_monthly_balance_without_category(self):
        self.client.force_login(self.user)

        create_url = reverse("api:v1:monthly_balances")
        amount = random.randint(1, 90000)
        record_date = "2023-09-02"
        data = {"amount": amount, "date": record_date}
        res = self.client.post(create_url, data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(res.data["category"][0], "This field is required.")

        monthly_balance_url = reverse("api:v1:monthly_balances")
        response = self.client.get(monthly_balance_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 0)
        self.assertEqual(len(response.data["results"]), 0)

    def test_can_edit_monthly_balances(self):  # pylint: disable=R0914;# noqa
        self.client.force_login(self.user)

        category_response, _ = self.create_monthly_balance_category()
        category_id = category_response.data["id"]

        balance_response, _ = self.create_monthly_balance(category_id)
        balance_amount = balance_response.data["amount"]
        balance_date = balance_response.data["date"]

        self.assertEqual(balance_response.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            m.MonthlyBalance.objects.get().amount, balance_amount  # pylint: disable=E1101; # noqa
        )
        self.assertEqual(
            m.MonthlyBalance.objects.get().date.isoformat(), balance_date  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        monthly_balances_url = reverse("api:v1:monthly_balances")
        response = self.client.get(monthly_balances_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["date"], balance_date)
        self.assertEqual(response.data["results"][0]["amount"], balance_amount)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)
        mb_id = response.data["results"][0]["id"]

        new_amount = random.randint(1, 90000)
        new_data = {"amount": new_amount}
        update_res = self.edit_monthly_balance(mb_id, new_data)

        update_res = self.client.get(monthly_balances_url, format="json")
        self.assertEqual(update_res.status_code, status.HTTP_200_OK)
        self.assertEqual(update_res.data["results"][0]["date"], balance_date)
        self.assertEqual(update_res.data["results"][0]["amount"], new_amount)
        self.assertEqual(
            update_res.data["results"][0]["category"]["id"], category_id
        )
        self.assertEqual(update_res.data["count"], 1)
        self.assertEqual(len(update_res.data["results"]), 1)

    # WRITE ME
    @skip
    def test_create_and_retrieve_single_monthly_balance(self):
        pass

    # WRITE ME
    @skip
    def test_update_monthly_balance(self):
        pass

    def test_can_not_update_a_monthly_balance_to_have_a_date_not_ending_in_01(
        self,
    ):
        self.client.force_login(self.user)

        category_response, _ = self.create_monthly_balance_category()
        category_id = category_response.data["id"]

        balance_response, _ = self.create_monthly_balance(category_id)
        balance_amount = balance_response.data["amount"]
        balance_date = balance_response.data["date"]

        self.assertEqual(balance_response.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            m.MonthlyBalance.objects.get().amount, balance_amount  # pylint: disable=E1101; # noqa
        )
        self.assertEqual(
            m.MonthlyBalance.objects.get().date.isoformat(), balance_date  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        monthly_balance_url = reverse("api:v1:monthly_balances")
        response = self.client.get(monthly_balance_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["date"], balance_date)
        self.assertEqual(response.data["results"][0]["amount"], balance_amount)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)
        mb_id = response.data["results"][0]["id"]

        t2 = timedelta(days=random.randint(1, 100))
        new_record_date = (
            datetime.strptime(balance_date, "%Y-%m-%d") - t2
        ).strftime("%Y-%m-%d")

        new_data = {"date": new_record_date}
        update_res = self.edit_monthly_balance(mb_id, new_data)

        monthly_balance_url = reverse("api:v1:monthly_balances")
        update_res = self.client.get(monthly_balance_url, format="json")
        self.assertEqual(update_res.status_code, status.HTTP_200_OK)
        self.assertEqual(update_res.data["results"][0]["date"], balance_date)
        self.assertEqual(
            update_res.data["results"][0]["amount"], balance_amount
        )
        self.assertEqual(
            update_res.data["results"][0]["category"]["id"], category_id
        )
        self.assertEqual(update_res.data["count"], 1)
        self.assertEqual(len(update_res.data["results"]), 1)

    def test_can_not_update_a_monthly_balance_to_have_malformed_data_in_the_date_field(
        self,
    ):
        self.client.force_login(self.user)

        category_response, _ = self.create_monthly_balance_category()
        category_id = category_response.data["id"]

        balance_response, _ = self.create_monthly_balance(category_id)
        balance_amount = balance_response.data["amount"]
        balance_date = balance_response.data["date"]

        self.assertEqual(balance_response.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            m.MonthlyBalance.objects.get().amount, balance_amount  # pylint: disable=E1101; # noqa
        )
        self.assertEqual(
            m.MonthlyBalance.objects.get().date.isoformat(), balance_date  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        monthly_balance_url = reverse("api:v1:monthly_balances")
        response = self.client.get(monthly_balance_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["date"], balance_date)
        self.assertEqual(response.data["results"][0]["amount"], balance_amount)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)
        mb_id = response.data["results"][0]["id"]

        t2 = timedelta(days=random.randint(1, 100))
        _ = (datetime.strptime(balance_date, "%Y-%m-%d") - t2).strftime(
            "%Y-%m-%d"
        )

        # Try to corrupt the data with a string
        new_data = {"date": "i am surely not a date, but a string"}
        update_res = self.edit_monthly_balance(mb_id, new_data)

        monthly_balance_url = reverse("api:v1:monthly_balances")
        list_res = self.client.get(monthly_balance_url, format="json")
        self.assertEqual(list_res.status_code, status.HTTP_200_OK)
        self.assertEqual(list_res.data["results"][0]["date"], balance_date)
        self.assertEqual(list_res.data["results"][0]["amount"], balance_amount)
        self.assertEqual(
            list_res.data["results"][0]["category"]["id"], category_id
        )
        self.assertEqual(list_res.data["count"], 1)
        self.assertEqual(len(list_res.data["results"]), 1)

        # Try to corrupt the data with an integer
        new_data = {"date": 1}
        update_res = self.edit_monthly_balance(mb_id, new_data)
        self.assertEqual(update_res.status_code, status.HTTP_200_OK)

        list_res = self.client.get(monthly_balance_url, format="json")
        self.assertEqual(list_res.status_code, status.HTTP_200_OK)
        self.assertEqual(list_res.data["results"][0]["date"], balance_date)
        self.assertEqual(list_res.data["results"][0]["amount"], balance_amount)
        self.assertEqual(
            list_res.data["results"][0]["category"]["id"], category_id
        )
        self.assertEqual(list_res.data["count"], 1)
        self.assertEqual(len(list_res.data["results"]), 1)

        # Try to corrupt the data with an object
        new_data = {"date": {"key": "value"}}
        update_res = self.edit_monthly_balance(mb_id, new_data)
        self.assertEqual(update_res.status_code, status.HTTP_200_OK)

        list_res = self.client.get(monthly_balance_url, format="json")
        self.assertEqual(list_res.status_code, status.HTTP_200_OK)
        self.assertEqual(list_res.data["results"][0]["date"], balance_date)
        self.assertEqual(list_res.data["results"][0]["amount"], balance_amount)
        self.assertEqual(
            list_res.data["results"][0]["category"]["id"], category_id
        )
        self.assertEqual(list_res.data["count"], 1)
        self.assertEqual(len(list_res.data["results"]), 1)

        # Try to corrupt the data with None
        new_data = {"date": None}
        update_res = self.edit_monthly_balance(mb_id, new_data)
        self.assertEqual(update_res.status_code, status.HTTP_200_OK)

        list_res = self.client.get(monthly_balance_url, format="json")
        self.assertEqual(list_res.status_code, status.HTTP_200_OK)
        self.assertEqual(list_res.data["results"][0]["date"], balance_date)
        self.assertEqual(list_res.data["results"][0]["amount"], balance_amount)
        self.assertEqual(
            list_res.data["results"][0]["category"]["id"], category_id
        )
        self.assertEqual(list_res.data["count"], 1)
        self.assertEqual(len(list_res.data["results"]), 1)

    # NOTE: in the serializer used in MonthlyBalanceDetail category and date
    # fields are read_only to prevent users to update them (we force users to
    # delete andrecreate new monthly balance ON PURPOSE)
    def test_can_not_update_category_when_updating_monthly_balance(self):
        self.client.force_login(self.user)

        category_response, _ = self.create_monthly_balance_category()
        category_id = category_response.data["id"]

        sec_cat_res, _ = self.create_monthly_balance_category()
        sec_cat_id = sec_cat_res.data["id"]

        balance_response, _ = self.create_monthly_balance(category_id)
        balance_amount = balance_response.data["amount"]
        balance_date = balance_response.data["date"]

        self.assertEqual(balance_response.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            m.MonthlyBalance.objects.get().amount, balance_amount  # pylint: disable=E1101; # noqa
        )
        self.assertEqual(
            m.MonthlyBalance.objects.get().date.isoformat(), balance_date  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        monthly_balance_url = reverse("api:v1:monthly_balances")
        response = self.client.get(monthly_balance_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["date"], balance_date)
        self.assertEqual(response.data["results"][0]["amount"], balance_amount)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)
        mb_id = response.data["results"][0]["id"]

        new_data = {"category": sec_cat_id}
        update_res = self.edit_monthly_balance(mb_id, new_data)

        monthly_balance_url = reverse("api:v1:monthly_balances")
        update_res = self.client.get(monthly_balance_url, format="json")
        self.assertEqual(update_res.status_code, status.HTTP_200_OK)
        self.assertEqual(update_res.data["results"][0]["date"], balance_date)
        self.assertEqual(
            update_res.data["results"][0]["amount"], balance_amount
        )
        self.assertEqual(
            update_res.data["results"][0]["category"]["id"], category_id
        )
        self.assertEqual(update_res.data["count"], 1)
        self.assertEqual(len(update_res.data["results"]), 1)

    # NOTE: in the serializer used in MonthlyBalanceDetail category and date
    # fields are read_only to prevent users to update them (we force users to
    # delete and recreate  new monthly balance ON PURPOSE)
    def test_can_not_update_date_when_updating_monthly_balance(self):
        self.client.force_login(self.user)

        category_response, _ = self.create_monthly_balance_category()
        category_id = category_response.data["id"]

        balance_response, _ = self.create_monthly_balance(category_id)
        balance_amount = balance_response.data["amount"]
        balance_date = balance_response.data["date"]

        self.assertEqual(balance_response.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            m.MonthlyBalance.objects.get().amount, balance_amount  # pylint: disable=E1101; # noqa
        )
        self.assertEqual(
            m.MonthlyBalance.objects.get().date.isoformat(), balance_date  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        monthly_balance_url = reverse("api:v1:monthly_balances")
        response = self.client.get(monthly_balance_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["date"], balance_date)
        self.assertEqual(response.data["results"][0]["amount"], balance_amount)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)
        mb_id = response.data["results"][0]["id"]

        t2 = timedelta(days=random.randint(1, 100))
        new_record_date = (
            datetime.strptime(balance_date, "%Y-%m-%d") - t2
        ).strftime("%Y-%m-%d")
        new_record_date_01 = (
            new_record_date[0 : len(new_record_date) - 2] + "01"
        )

        new_data = {"date": new_record_date_01}
        update_res = self.edit_monthly_balance(mb_id, new_data)

        monthly_balance_url = reverse("api:v1:monthly_balances")
        update_res = self.client.get(monthly_balance_url, format="json")
        self.assertEqual(update_res.status_code, status.HTTP_200_OK)
        self.assertEqual(update_res.data["results"][0]["date"], balance_date)
        self.assertEqual(
            update_res.data["results"][0]["amount"], balance_amount
        )
        self.assertEqual(update_res.data["count"], 1)
        self.assertEqual(len(update_res.data["results"]), 1)

    def test_can_not_edit_other_users_monthly_balances(
        self,
    ):  # pylint: disable=R0914;# noqa
        self.client.force_login(self.user)

        category_response, _ = self.create_monthly_balance_category()
        category_id = category_response.data["id"]

        balance_response, _ = self.create_monthly_balance(category_id)
        balance_amount = balance_response.data["amount"]
        balance_date = balance_response.data["date"]

        self.assertEqual(balance_response.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            m.MonthlyBalance.objects.get().amount, balance_amount  # pylint: disable=E1101; # noqa
        )
        self.assertEqual(
            m.MonthlyBalance.objects.get().date.isoformat(), balance_date  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        monthly_balances_url = reverse("api:v1:monthly_balances")
        response = self.client.get(monthly_balances_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["date"], balance_date)
        self.assertEqual(response.data["results"][0]["amount"], balance_amount)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)
        mb_id = response.data["results"][0]["id"]

        self.client.force_login(self.user2)
        new_amount = random.randint(1, 90000)
        new_data = {"amount": new_amount}
        update_res = self.edit_monthly_balance(mb_id, new_data)

        self.assertEqual(update_res.status_code, status.HTTP_404_NOT_FOUND)

        # Confirm the initial user can edit said URL
        self.client.force_login(self.user)
        update_res = self.edit_monthly_balance(mb_id, new_data)

        monthly_balances_url = reverse("api:v1:monthly_balances")
        response = self.client.get(monthly_balances_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["date"], balance_date)
        self.assertEqual(response.data["results"][0]["amount"], new_amount)
        self.assertEqual(
            response.data["results"][0]["category"]["id"], category_id
        )
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)

    def test_can_not_edit_monthly_balances_to_attach_them_to_other_users(
        self,
    ):  # pylint: disable=R0914;# noqa
        self.client.force_login(self.user2)

        category_response, _ = self.create_monthly_balance_category()
        category_id = category_response.data["id"]

        balance_response, _ = self.create_monthly_balance(category_id)
        balance_amount = balance_response.data["amount"]
        balance_date = balance_response.data["date"]

        self.assertEqual(balance_response.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            m.MonthlyBalance.objects.get().amount, balance_amount  # pylint: disable=E1101; # noqa
        )
        self.assertEqual(
            m.MonthlyBalance.objects.get().date.isoformat(), balance_date  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        monthly_balance_url = reverse("api:v1:monthly_balances")
        response = self.client.get(monthly_balance_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["date"], balance_date)
        self.assertEqual(response.data["results"][0]["amount"], balance_amount)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)
        mb_id = response.data["results"][0]["id"]

        t2 = timedelta(days=random.randint(1, 100))
        _ = (datetime.strptime(balance_date, "%Y-%m-%d") - t2).strftime(
            "%Y-%m-%d"
        )

        # Try to attach the object to the initial user
        new_data = {"created_by": self.user.id}
        _ = self.edit_monthly_balance(mb_id, new_data)
        # fmt: off
        self.assertEqual(
            m.MonthlyBalance.objects.get().created_by.id, self.user2.id  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        # Confirm the object has been attatched to the second_user instead
        monthly_balance_url = reverse("api:v1:monthly_balances")
        list_res = self.client.get(monthly_balance_url, format="json")
        self.assertEqual(list_res.status_code, status.HTTP_200_OK)
        self.assertEqual(list_res.data["results"][0]["date"], balance_date)
        self.assertEqual(list_res.data["results"][0]["amount"], balance_amount)
        self.assertEqual(
            list_res.data["results"][0]["category"]["id"], category_id
        )
        self.assertEqual(list_res.data["count"], 1)
        self.assertEqual(len(list_res.data["results"]), 1)

        # Confirm the initial user sees no objects
        self.client.force_login(self.user)
        list_res = self.client.get(monthly_balance_url, format="json")
        self.assertEqual(list_res.status_code, status.HTTP_200_OK)
        self.assertEqual(len(list_res.data["results"]), 0)
        self.assertEqual(list_res.data["count"], 0)

    def test_can_not_edit_monthly_balances_to_use_other_users_categories(
        self,
    ):  # pylint: disable=R0914;# noqa
        self.client.force_login(self.user)

        cat_res, _ = self.create_monthly_balance_category()
        init_cat_id = cat_res.data["id"]

        self.client.force_login(self.user2)

        category_response, _ = self.create_monthly_balance_category()
        category_id = category_response.data["id"]

        balance_response, _ = self.create_monthly_balance(category_id)
        balance_amount = balance_response.data["amount"]
        balance_date = balance_response.data["date"]

        self.assertEqual(balance_response.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            m.MonthlyBalance.objects.get().amount, balance_amount  # pylint: disable=E1101; # noqa
        )
        self.assertEqual(
            m.MonthlyBalance.objects.get().date.isoformat(), balance_date  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        monthly_balance_url = reverse("api:v1:monthly_balances")
        response = self.client.get(monthly_balance_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["date"], balance_date)
        self.assertEqual(response.data["results"][0]["amount"], balance_amount)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)
        mb_id = response.data["results"][0]["id"]

        t2 = timedelta(days=random.randint(1, 100))
        _ = (datetime.strptime(balance_date, "%Y-%m-%d") - t2).strftime(
            "%Y-%m-%d"
        )

        # Try to attach the object to the initial user
        new_data = {"category": init_cat_id}
        _ = self.edit_monthly_balance(mb_id, new_data)

        monthly_balance_url = reverse("api:v1:monthly_balances")
        list_res = self.client.get(monthly_balance_url, format="json")
        self.assertEqual(list_res.status_code, status.HTTP_200_OK)
        self.assertEqual(list_res.data["results"][0]["date"], balance_date)
        self.assertEqual(list_res.data["results"][0]["amount"], balance_amount)
        self.assertEqual(
            list_res.data["results"][0]["category"]["id"], category_id
        )
        self.assertEqual(list_res.data["count"], 1)
        self.assertEqual(len(list_res.data["results"]), 1)

    def test_can_not_edit_a_monthly_balances_to_have_the_same_category_and_the_same_date_as_another_monthly_balance(
        self,
    ):  # pylint: disable=R0914;# noqa
        self.client.force_login(self.user)

        # Create a monthly balance category
        cat_res, _ = self.create_monthly_balance_category()
        cat_id = cat_res.data["id"]

        sec_cat_res, _ = self.create_monthly_balance_category()
        sec_cat_id = sec_cat_res.data["id"]

        create_url = reverse("api:v1:monthly_balances")
        amount = random.randint(1, 90000)
        record_date = date.today().isoformat()
        record_date_ending_in_01 = record_date[0 : len(record_date) - 2] + "01"
        data = {"category": cat_id, "amount": amount, "date": record_date}
        res = self.client.post(create_url, data, format="json")

        balance_id = res.data["id"]
        balance_amount = res.data["amount"]
        balance_date = res.data["date"]

        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            m.MonthlyBalance.objects.get().amount, balance_amount  # pylint: disable=E1101; # noqa
        )
        self.assertEqual(
            m.MonthlyBalance.objects.get().date.isoformat(),  # pylint: disable=E1101; # noqa
            record_date_ending_in_01,
        )
        # fmt: on
        # Create a second Monthly Balance object with different date and
        # different category
        sec_amount = random.randint(1, 90000)
        sec_record_date = (
            datetime.strptime(balance_date, "%Y-%m-%d")
            - timedelta(days=random.randint(1, 100))
        ).strftime("%Y-%m-%d")
        sec_record_date_ending_in_01 = (
            sec_record_date[0 : len(sec_record_date) - 2] + "01"
        )
        data = {
            "category": sec_cat_id,
            "amount": sec_amount,
            "date": sec_record_date,
        }
        sec_res = self.client.post(create_url, data, format="json")
        sec_balance_id = sec_res.data["id"]
        _ = sec_res.data["amount"]
        _ = sec_res.data["date"]

        self.assertEqual(sec_res.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            m.MonthlyBalance.objects.exclude(id=balance_id)[0].amount,  # pylint: disable=E1101; # noqa
            sec_amount,
        )
        self.assertEqual(
            m.MonthlyBalance.objects.exclude(id=balance_id)[  # pylint: disable=E1101; # noqa
                0
            ].date.isoformat(),
            sec_record_date_ending_in_01,
        )
        # fmt: on
        monthly_balance_url = reverse("api:v1:monthly_balances")
        response = self.client.get(monthly_balance_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 2)
        self.assertEqual(len(response.data["results"]), 2)

        # Try to update both date and category of the SECOND monthly balance
        # object, to match the FRIST one
        update_data = {
            "category": cat_id,
            "amount": amount,
            "date": record_date_ending_in_01,
        }
        _ = self.edit_monthly_balance(sec_balance_id, update_data)
        # fmt: off
        mb = m.MonthlyBalance.objects.exclude(id=balance_id)[  # pylint: disable=E1101; # noqa
            0
        ]
        # fmt: on
        self.assertEqual(mb.amount, amount)
        # Assert the category of the second object has NOT changed
        self.assertNotEqual(mb.category.id, cat_id)
        self.assertEqual(mb.category.id, sec_cat_id)
        # Assert the date of the second object has NOT changed
        self.assertNotEqual(mb.date.isoformat(), record_date_ending_in_01)
        self.assertEqual(mb.date.isoformat(), sec_record_date_ending_in_01)

        # Create a monthly balance without a non-existing category

    def test_can_not_edit_monthly_balance_to_have_malformed_category(self):
        self.client.force_login(self.user)

        category_response, _ = self.create_monthly_balance_category()
        category_id = category_response.data["id"]

        balance_response, _ = self.create_monthly_balance(category_id)
        balance_amount = balance_response.data["amount"]
        balance_date = balance_response.data["date"]
        # fmt: off
        mb = m.MonthlyBalance.objects.get()  # pylint: disable=E1101; # noqa
        self.assertEqual(balance_response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            mb.amount, balance_amount
        )
        self.assertEqual(
            mb.date.isoformat(), balance_date
        )
        # fmt: on
        amount = random.randint(1, 90000)

        # Update with None category
        non_existing_cat = {
            "amount": amount,
            "date": balance_date,
            "category": None,
        }
        res = self.edit_monthly_balance(mb.id, non_existing_cat)
        # This route uses a serializer that has category and date read only:
        # it can only update the amount, hence the status.HTTP_200_OK
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data["id"], mb.id)
        self.assertEqual(res.data["amount"], amount)
        self.assertEqual(res.data["date"], mb.date.isoformat())
        self.assertEqual(res.data["category"]["id"], mb.category.id)
        self.assertEqual(res.data["category"]["text"], mb.category.text)

        # Update with non existing category
        non_existing_cat = {
            "amount": amount,
            "date": balance_date,
            "category": 545433,
        }
        res = self.edit_monthly_balance(mb.id, non_existing_cat)
        # This route uses a serializer that has category and date read only:
        # it can only update the amount, hence the status.HTTP_200_OK
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data["id"], mb.id)
        self.assertEqual(res.data["amount"], amount)
        self.assertEqual(res.data["date"], mb.date.isoformat())
        self.assertEqual(res.data["category"]["id"], mb.category.id)
        self.assertEqual(res.data["category"]["text"], mb.category.text)

        # Post without a category
        non_existing_cat = {
            "amount": amount,
            "date": balance_date,
        }
        res = self.edit_monthly_balance(mb.id, non_existing_cat)
        # This route uses a serializer that has category and date read only:
        # it can only update the amount, hence the status.HTTP_200_OK
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data["id"], mb.id)
        self.assertEqual(res.data["amount"], amount)
        self.assertEqual(res.data["date"], mb.date.isoformat())
        self.assertEqual(res.data["category"]["id"], mb.category.id)
        self.assertEqual(res.data["category"]["text"], mb.category.text)

    def test_using_malformed_date_in_get_request_param_returns_no_data(
        self,
    ):  # pylint: disable=R0914;# noqa
        self.client.force_login(self.user)

        # Create a category
        category_response, _ = self.create_monthly_balance_category()
        category_id = category_response.data["id"]

        balance_response, _ = self.create_monthly_balance(category_id)
        balance_amount = balance_response.data["amount"]
        balance_date = balance_response.data["date"]

        self.assertEqual(balance_response.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            m.MonthlyBalance.objects.get().amount, balance_amount  # pylint: disable=E1101; # noqa
        )
        self.assertEqual(
            m.MonthlyBalance.objects.get().date.isoformat(), balance_date  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        monthly_balance_url = reverse("api:v1:monthly_balances")
        r = self.client.get(monthly_balance_url, format="json")

        self.assertEqual(r.status_code, status.HTTP_200_OK)
        self.assertEqual(r.data["results"][0]["date"], balance_date)
        self.assertEqual(r.data["results"][0]["amount"], balance_amount)
        self.assertEqual(r.data["results"][0]["category"]["id"], category_id)
        self.assertEqual(r.data["count"], 1)
        self.assertEqual(len(r.data["results"]), 1)

        # Try malformed end param
        malformed_date = "I am definitely not a valid date value"
        filtered_url = (
            monthly_balance_url + "?" + urlencode({"date": malformed_date})
        )
        r2 = self.client.get(filtered_url, format="json")
        self.assertNotEqual(r2.status_code, 500)
        self.assertNotEqual(r2.status_code, 400)
        self.assertEqual(r2.status_code, 200)
        self.assertEqual(len(r2.data["results"]), 0)
        self.assertEqual(r2.data["count"], 0)

    def test_can_not_create_a_monthly_balances_with_dates_not_ending_in_01(
        self,
    ):  # pylint: disable=R0914;# noqa
        self.client.force_login(self.user)

        # Create a monthly balance category
        cat_res, _ = self.create_monthly_balance_category()
        cat_id = cat_res.data["id"]

        # Create a monthly balance that does not end with -01
        create_url = reverse("api:v1:monthly_balances")
        amount = random.randint(1, 90000)
        record_date = "2023-09-02"  # any date that does not end in -01
        data = {"category": cat_id, "amount": amount, "date": record_date}
        res = self.client.post(create_url, data, format="json")
        balance_amount = res.data["amount"]
        _ = res.data["date"]

        # fmt: off
        mb = m.MonthlyBalance.objects.get()  # pylint: disable=E1101; # noqa
        # fmt: on
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        self.assertEqual(mb.amount, balance_amount)
        self.assertNotEqual(mb.date.isoformat(), record_date)
        self.assertEqual(mb.date.isoformat(), "2023-09-01")

        monthly_balance_url = reverse("api:v1:monthly_balances")
        response = self.client.get(monthly_balance_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["date"], "2023-09-01")
        self.assertEqual(response.data["results"][0]["amount"], balance_amount)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)

    def test_create_malformed_monthly_balances_fails(
        self,
    ):  # pylint: disable=R0914;# noqa
        """
        Ensure we can not create a new monthly balance object without a
        category
        """
        self.client.force_login(self.user)

        # Create a category
        category_response, _ = self.create_monthly_balance_category()
        category_id = category_response.data["id"]

        # Create a monthly balance without a category
        create_url = reverse("api:v1:monthly_balances")
        amount = random.randint(1, 90000)
        record_date = date.today().isoformat()
        record_date = record_date[0 : len(record_date) - 2] + "01"

        data_wihtout_category = {"amount": amount, "date": record_date}
        r_no_cat = self.client.post(
            create_url, data_wihtout_category, format="json"
        )

        self.assertEqual(r_no_cat.status_code, status.HTTP_400_BAD_REQUEST)

        self.assertEqual(
            r_no_cat.data["category"][0], "This field is required."
        )
        self.assertEqual(r_no_cat.data["category"][0].code, "invalid")

        # Create a monthly balance without category set to None
        create_url = reverse("api:v1:monthly_balances")
        amount = random.randint(1, 90000)
        record_date = date.today().isoformat()
        record_date = record_date[0 : len(record_date) - 2] + "01"

        data_with_category_none = {
            "amount": amount,
            "date": record_date,
            "category": None,
        }
        r_none_cat = self.client.post(
            create_url, data_with_category_none, format="json"
        )
        self.assertEqual(r_none_cat.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            r_none_cat.data["category"][0], "This field is required."
        )
        self.assertEqual(r_none_cat.data["category"][0].code, "invalid")

        # Create a monthly balance without a non-existing category
        create_url = reverse("api:v1:monthly_balances")
        amount = random.randint(1, 90000)
        record_date = date.today().isoformat()
        record_date = record_date[0 : len(record_date) - 2] + "01"

        non_existing_cat = {
            "amount": amount,
            "date": record_date,
            "category": 545433,
        }
        r = self.client.post(create_url, non_existing_cat, format="json")
        self.assertEqual(r.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            r.data["category"][0],
            'Invalid pk "545433" - object does not exist.',
        )
        self.assertEqual(r.data["category"][0].code, "does_not_exist")

        # Another user CANNOT see that monthly balance
        monthly_balance_url = reverse("api:v1:monthly_balances")
        self.client.force_login(self.user2)

        response = self.client.get(monthly_balance_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 0)
        self.assertEqual(len(response.data["results"]), 0)

        # Create a monthlybalance with another user's category
        other_user_cat_response, _ = self.create_monthly_balance(category_id)
        self.assertEqual(
            other_user_cat_response.status_code, status.HTTP_400_BAD_REQUEST
        )
        # TODO: check whether DJANGO throws the same error on missing foreign
        # keys for other fields
        self.assertEqual(
            other_user_cat_response.data["category"][0],
            "This field is required.",
        )
        self.assertEqual(
            other_user_cat_response.data["category"][0].code, "invalid"
        )

    def test_can_delete_own_monthly_balances(self):
        self.client.force_login(self.user)

        # Create a category
        category_response, _ = self.create_monthly_balance_category()
        category_id = category_response.data["id"]

        balance_response, _ = self.create_monthly_balance(category_id)
        balance_amount = balance_response.data["amount"]
        balance_date = balance_response.data["date"]

        self.assertEqual(balance_response.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            m.MonthlyBalance.objects.get().amount, balance_amount  # pylint: disable=E1101; # noqa
        )
        self.assertEqual(
            m.MonthlyBalance.objects.get().date.isoformat(), balance_date  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        monthly_balance_url = reverse("api:v1:monthly_balances")
        response = self.client.get(monthly_balance_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["date"], balance_date)
        self.assertEqual(response.data["results"][0]["amount"], balance_amount)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)
        mb = response.data["results"][0]

        # Can delete own monthly balance
        del_res, _ = self.delete_monthly_balance(mb["id"])
        self.assertEqual(del_res.status_code, status.HTTP_204_NO_CONTENT)

        response = self.client.get(monthly_balance_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 0)
        self.assertEqual(len(response.data["results"]), 0)

    def test_can_not_delete_other_users_monthly_balances(self):
        self.client.force_login(self.user)

        # Create a category
        category_response, _ = self.create_monthly_balance_category()
        category_id = category_response.data["id"]

        balance_response, _ = self.create_monthly_balance(category_id)
        balance_amount = balance_response.data["amount"]
        balance_date = balance_response.data["date"]

        self.assertEqual(balance_response.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            m.MonthlyBalance.objects.get().amount, balance_amount  # pylint: disable=E1101; # noqa
        )
        self.assertEqual(
            m.MonthlyBalance.objects.get().date.isoformat(), balance_date  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        monthly_balance_url = reverse("api:v1:monthly_balances")
        response = self.client.get(monthly_balance_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["date"], balance_date)
        self.assertEqual(response.data["results"][0]["amount"], balance_amount)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)
        mb = response.data["results"][0]

        # Another user CANNOT see that monthly balance
        self.client.force_login(self.user2)

        response = self.client.get(monthly_balance_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 0)
        self.assertEqual(len(response.data["results"]), 0)

        # Another user CANNOT delete that monthly balance
        del_res, _ = self.delete_monthly_balance(mb["id"])
        self.assertEqual(del_res.status_code, status.HTTP_404_NOT_FOUND)
        self.client.logout()

        # Hence the original user can still see their object, as the delete
        # failed
        self.client.force_login(self.user)
        monthly_balance_url = reverse("api:v1:monthly_balances")
        response = self.client.get(monthly_balance_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["date"], balance_date)
        self.assertEqual(response.data["results"][0]["amount"], balance_amount)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)

    def test_create_and_list_monthly_balance_without_pk(self):
        """
        Test MonthlyBalanceCreateOrUpdate class: create a monthly balance
        through the MonthlyBalanceCreateOrUpdate class, and confirm the entity
        has been created
        """
        self.client.force_login(self.user)

        # Create an income category
        category_response, _ = self.create_monthly_balance_category()
        category_text = category_response.data["text"]
        self.assertEqual(
            category_response.status_code, status.HTTP_201_CREATED
        )
        # fmt: off
        cat_id = m.MonthlyBalanceCategory.objects.get(  # pylint: disable=E1101; # noqa
            created_by=self.user
        ).id
        # fmt: on
        data = {
            "amount": random.randint(1, 90000),
            "date": date.today().isoformat(),
            "category": cat_id,
        }

        create_update_url = reverse(
            "api:v1:monthly_balance_category_create_or_update"
        )
        response = self.client.post(create_update_url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data["amount"], data["amount"])
        self.assertEqual(response.data["category"], cat_id)
        # Monthly balances always end in '-01'
        date_formatted = data["date"][0 : len(data["date"]) - 2] + "01"
        self.assertEqual(response.data["date"], date_formatted)

        mb_url = reverse("api:v1:monthly_balances")
        response = self.client.get(mb_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)
        self.assertEqual(response.data["results"][0]["date"], date_formatted)
        self.assertEqual(response.data["results"][0]["amount"], data["amount"])
        self.assertEqual(response.data["results"][0]["category"]["id"], cat_id)

    def test_update_monthly_balance_without_pk(self):
        """
        Test MonthlyBalanceCreateOrUpdate class: edit an existing Monthly
        Balance witout using its primary key, but by using its date and
        category_id
        """
        self.client.force_login(self.user)

        # Create a category
        category_response, _ = self.create_monthly_balance_category()
        cat_id = category_response.data["id"]

        balance_response, _ = self.create_monthly_balance(cat_id)
        balance_amount = balance_response.data["amount"]
        balance_date = balance_response.data["date"]

        self.assertEqual(balance_response.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            m.MonthlyBalance.objects.get().amount, balance_amount  # pylint: disable=E1101; # noqa
        )
        self.assertEqual(
            m.MonthlyBalance.objects.get().date.isoformat(), balance_date  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        mb_url = reverse("api:v1:monthly_balances")
        response = self.client.get(mb_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)

        data = {
            "amount": random.randint(1, 90000),
            "date": balance_date,
            "category": cat_id,
        }

        create_update_url = reverse(
            "api:v1:monthly_balance_category_create_or_update"
        )
        # Note: we use POST for updates as well...
        response = self.client.post(create_update_url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)

        response = self.client.get(mb_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        # Confrim we did not accidentally create new entities
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)
        self.assertEqual(response.data["results"][0]["date"], balance_date)
        self.assertEqual(response.data["results"][0]["amount"], data["amount"])
        self.assertEqual(response.data["results"][0]["category"]["id"], cat_id)

    def test_updating_monthly_balance_wihtout_using_pk_with_malformed_data_will_fail(
        self,
    ):
        """
        Test MonthlyBalanceCreateOrUpdate class: edit an existing Monthly
        Balance witout using its primary key, but by using its date and
        category_id
        """
        self.client.force_login(self.user)

        # Create a category
        category_response, _ = self.create_monthly_balance_category()
        cat_id = category_response.data["id"]

        balance_response, _ = self.create_monthly_balance(cat_id)
        balance_amount = balance_response.data["amount"]
        balance_date = balance_response.data["date"]

        self.assertEqual(balance_response.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            m.MonthlyBalance.objects.get().amount, balance_amount  # pylint: disable=E1101; # noqa
        )
        self.assertEqual(
            m.MonthlyBalance.objects.get().date.isoformat(), balance_date  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        mb_url = reverse("api:v1:monthly_balances")
        response = self.client.get(mb_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)

        data = {
            "amount": random.randint(1, 90000),
            "date": balance_date,
            "category": cat_id,
        }

        # Try with negative amount
        bad_data = data
        bad_data["amount"] = -1
        create_update_url = reverse(
            "api:v1:monthly_balance_category_create_or_update"
        )
        # Note: we use POST for updates as well...
        response = self.client.post(create_update_url, bad_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.data["amount"][0], "-1 should be more than 0."
        )
        self.assertEqual(response.data["amount"][0].code, "invalid")

        # Try with None amount
        bad_data["amount"] = None
        create_update_url = reverse(
            "api:v1:monthly_balance_category_create_or_update"
        )
        # Note: we use POST for updates as well...
        response = self.client.post(create_update_url, bad_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.data["amount"][0], "This field may not be null."
        )
        self.assertEqual(response.data["amount"][0].code, "null")

        # Try with malformed date
        bad_data = data
        bad_data["date"] = "2022-42-42"
        create_update_url = reverse(
            "api:v1:monthly_balance_category_create_or_update"
        )
        # Note: we use POST for updates as well...
        response = self.client.post(create_update_url, bad_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        msg = (
            "Date has wrong format. Use one of these formats instead"
            ": YYYY-MM-DD."
        )
        self.assertEqual(response.data["date"][0], msg)
        self.assertEqual(response.data["date"][0].code, "invalid")

        # Try with None date
        bad_data["date"] = None
        create_update_url = reverse(
            "api:v1:monthly_balance_category_create_or_update"
        )
        # Note: we use POST for updates as well...
        response = self.client.post(create_update_url, bad_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.data["date"][0], "This field may not be null."
        )
        self.assertEqual(response.data["date"][0].code, "null")

        # Try with number as date
        bad_data["date"] = 42
        create_update_url = reverse(
            "api:v1:monthly_balance_category_create_or_update"
        )
        # Note: we use POST for updates as well...
        response = self.client.post(create_update_url, bad_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        msg = (
            "Date has wrong format. Use one of these formats instead"
            ": YYYY-MM-DD."
        )
        self.assertEqual(response.data["date"][0], msg)
        self.assertEqual(response.data["date"][0].code, "invalid")

        # Try with non-existing category
        bad_data = data
        bad_data["category"] = random.randint(80000, 90000)
        create_update_url = reverse(
            "api:v1:monthly_balance_category_create_or_update"
        )
        # Note: we use POST for updates as well...
        response = self.client.post(create_update_url, bad_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        msg = (
            err_msg
        ) = f"Invalid pk \"{bad_data['category']}\" - object does not exist."
        self.assertEqual(response.data["category"][0], msg)
        self.assertEqual(response.data["category"][0].code, "does_not_exist")

        # Try with another user category
        self.client.force_login(self.user2)
        category_response, _ = self.create_monthly_balance_category()
        cat_id2 = category_response.data["id"]

        self.client.force_login(self.user)
        bad_data = data
        bad_data["category"] = cat_id2
        create_update_url = reverse(
            "api:v1:monthly_balance_category_create_or_update"
        )
        # Note: we use POST for updates as well...
        response = self.client.post(create_update_url, bad_data, format="json")
        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            response.data["category"][0], "This field is required."
        )
        self.assertEqual(response.data["category"][0].code, "invalid")

    # WRITE ME
    @skip
    def test_integrity_errors_are_catched_on_create(
        self, mock_cat_serializer, _
    ):
        pass

    # WRITE ME
    @skip
    @patch("api.v1.views.monthly_balance_views.MonthlyBalanceList")
    @patch("budgets.serializers.MonthlyBalanceSerializerForCreation")
    def test_integrity_errors_are_catched_on_update(
        self, mock_cat_serializer, _
    ):
        # <=========== 7 extra line of coverage
        pass

    @patch("api.v1.views.monthly_balance_views.MonthlyBalanceList")
    @patch("budgets.serializers.MonthlyBalanceSerializerWithNestedCategory")
    def test_integrity_errors_are_catched_on_getting_details(
        self, mock_cat_serializer, _
    ):
        self.client.force_login(self.user)

        # Create a category
        cat_r, _ = self.create_monthly_balance_category()
        cat_id = cat_r.data["id"]
        cat_text = cat_r.data["text"]

        balance_response, _ = self.create_monthly_balance(cat_id)
        mb_amount = balance_response.data["amount"]
        mb_date = balance_response.data["date"]
        # fmt: off
        mb_id = m.MonthlyBalance.objects.get(  # pylint: disable=E1101; # noqa
            created_by=self.user
        ).id

        self.assertEqual(balance_response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            m.MonthlyBalance.objects.get(created_by=self.user).amount,  # pylint: disable=E1101; # noqa
            mb_amount,
        )
        self.assertEqual(
            m.MonthlyBalance.objects.get(  # pylint: disable=E1101; # noqa
                created_by=self.user
            ).date.isoformat(),
            mb_date,
        )
        # fmt: on
        category_edit_url = reverse(
            "api:v1:monthly_balances", kwargs={"pk": mb_id}
        )
        response = self.client.get(category_edit_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["category"]["id"], cat_id)
        self.assertEqual(response.data["category"]["text"], cat_text)
        self.assertEqual(response.data["date"], mb_date)
        self.assertEqual(response.data["amount"], mb_amount)

        cat_r2, _ = self.create_monthly_balance_category()
        cat_id2 = cat_r2.data["id"]

        # Force the serializer to Throw an Integrity error
        serializer = mock_cat_serializer.return_value
        msg = "I AM A MOCKED INTEGRITY ERROR. I HOPE YOU CATCH ME: ROAAAARR!"
        serializer.save.side_effect = IntegrityError(msg)

        # Create a mock request
        record_date = (
            date.today() - timedelta(days=random.randint(42, 365))
        ).isoformat()
        ok_data = {
            "amount": random.randint(1, 90000),
            "date": record_date[0 : len(record_date) - 2] + "01",
            "category": cat_id2,
        }
        # Create an instance of the view
        view = MonthlyBalanceList()
        request = self.client.post(reverse("api:v1:monthly_balances"), ok_data)

        # Set the request user
        view.request = request
        request.user = self.user

        # Confirm that the mocks works, and that ok_data actually thew an
        # exception and confirm it was correctly handled by the backend
        failed_res = view.perform_create(serializer)
        self.assertEqual(
            failed_res.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR
        )
        msg = (
            "Generic error thrown while creating the MonthlyBalance. "
            "Refer the web container logs if you're the admin"
        )
        self.assertEqual(failed_res.data["non_field_errors"][0], msg)

    # WRITE ME
    @skip
    def test_can_not_create_monthly_balance_category_for_other_users_without_pk(
        self,
    ):
        """
        WRITE ME
        """
        pass

    # WRITE ME
    @skip
    def test_can_not_update_monthly_balance_category_for_other_users_without_pk(
        self,
    ):
        """
        WRITE ME
        """
        pass

    @patch("api.v1.views.monthly_balance_views.MonthlyBalanceCreateOrUpdate")
    @patch("budgets.serializers.MonthlyBalanceSerializerForCreateUpdate")
    def test_integrity_errors_are_catched_on_create_without_using_pk(
        self, mock_cat_serializer, _
    ):
        self.client.force_login(self.user)

        # Create an income category
        cat_r, _ = self.create_monthly_balance_category()
        self.assertEqual(cat_r.status_code, status.HTTP_201_CREATED)
        # fmt: off
        cat_id = m.MonthlyBalanceCategory.objects.get(  # pylint: disable=E1101; # noqa
            created_by=self.user
        ).id
        # fmt: on
        data = {
            "amount": random.randint(1, 90000),
            "date": date.today().isoformat(),
            "category": cat_id,
        }

        create_update_url = reverse(
            "api:v1:monthly_balance_category_create_or_update"
        )
        response = self.client.post(create_update_url, data, format="json")
        self.assertEqual(response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(response.data["amount"], data["amount"])
        self.assertEqual(response.data["category"], cat_id)
        # Monthly balances always end in '-01'
        date_formatted = data["date"][0 : len(data["date"]) - 2] + "01"
        self.assertEqual(response.data["date"], date_formatted)

        mb_url = reverse("api:v1:monthly_balances")
        response = self.client.get(mb_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)
        self.assertEqual(response.data["results"][0]["date"], date_formatted)
        self.assertEqual(response.data["results"][0]["amount"], data["amount"])
        self.assertEqual(response.data["results"][0]["category"]["id"], cat_id)

        cat_r2, cat_data = self.create_monthly_balance_category()
        self.assertEqual(cat_r2.status_code, status.HTTP_201_CREATED)
        # fmt: off
        cat_id2 = m.MonthlyBalanceCategory.objects.get(  # pylint: disable=E1101; # noqa
            created_by=self.user,
            text=cat_data["text"],
            is_foreign_currency=cat_data["is_foreign_currency"],
        ).id
        # fmt: on
        # Force the serializer to Throw an Integrity error
        serializer = mock_cat_serializer.return_value
        msg = "I AM A MOCKED INTEGRITY ERROR. I HOPE YOU CATCH ME: ROAAAARR!"
        serializer.save.side_effect = IntegrityError(msg)

        # Create a mock request
        ok_data = {
            "amount": random.randint(1, 90000),
            "date": date.today().isoformat(),
            "category": cat_id2,
        }
        # Create an instance of the view
        view = MonthlyBalanceCreateOrUpdate()
        request = self.client.post(
            reverse("api:v1:monthly_balance_category_create_or_update"),
            ok_data,
        )

        # Set the request user
        view.request = request
        request.user = self.user

        # Confirm that the mocks works, and that ok_data actually thew
        # an exception  and confirm it was correctly handled by the backend
        failed_res = view.perform_create(serializer)
        self.assertEqual(
            failed_res.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR
        )
        msg = (
            "Generic error thrown while creating or updating the"
            " MonthlyBalance. Refer the web container logs if you're the admin"
        )
        self.assertEqual(failed_res.data["non_field_errors"][0], msg)

    # WRITE ME
    @skip
    def test_integrity_errors_are_catched_on_update_without_using_pk(
        self, mock_cat_serializer, _
    ):
        pass
