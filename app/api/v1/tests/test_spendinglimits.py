# Copyright: (c) 2024, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

import logging
import random
from unittest.mock import patch
from datetime import date, timedelta
from django.db.utils import IntegrityError
from django.contrib.auth.models import User
from django.urls import reverse
from rest_framework import status

from api.v1.tests.test_base import BaseApiTest
from budgets.tests.base import BaseTest

from budgets.serializers import (
    SpendingLimitSerializer,
    SpendingLimitPartialUpdateSerializer,
)
import budgets.models as m
from api.v1.views.spending_limit_views import SpendingLimitDetail
from api.v1.views.spending_limit_views import SpendingLimitList

from rest_framework import status
from django.urls import reverse
from budgets.serializers import SpendingLimitSerializer

logger = logging.getLogger(__name__)

from rest_framework.test import APIRequestFactory


class SpendingLimitTest(BaseApiTest):  # pylint: disable=R0904; # noqa
    """
    Test SpendingLimit model related v1 rest API behavior
    """

    def setUp(self):  # pylint: disable=C0103; # noqa
        # Instrument the unit tests to fail if they run for too long
        super().setUp()
        self.user = User.objects.create_user(
            username=BaseTest.generate_string(20),
            password=BaseTest.generate_string(20),
        )
        self.user2 = User.objects.create_user(
            username=BaseTest.generate_string(20),
            password=BaseTest.generate_string(20),
        )
        self.factory = APIRequestFactory()

    def test_serializer(self):
        text = BaseTest.generate_string(40)
        category = m.Category.objects.create(text=text, created_by=self.user)

        # Test the serializer with valid data first
        valid_data = {
            "category": category,
            "amount": random.randint(1, 90000),
            "currency": "JPY",
            "days": random.randint(1, 30),
            "text": "Monthly spending limit",
            "created_by": self.user,
        }

        spending_limit = m.SpendingLimit.objects.create(**valid_data)

        # Create a mock request, as the serializer checks whether this is
        # a PATCH request, or a POST one
        request = self.factory.patch(
            f"/api/v1/spending_limits/{spending_limit.id}/"
        )
        request.user = self.user

        # Pass the request in the context
        serializer = SpendingLimitSerializer(
            instance=spending_limit, context={"request": request}
        )

        self.assertEqual(serializer.data["category_text"], text)

        # Test the serializer with invalid data
        invalid_data = {
            # "category": None,
            "amount": random.randint(-100, -1),
            "currency": BaseTest.generate_string(4),
            "days": random.randint(-10, -1),
            "text": BaseTest.generate_string(151),
        }

        for field, invalid_value in invalid_data.items():
            malformed_data = self.modify_data(valid_data, field, invalid_value)
            serializer = SpendingLimitSerializer(
                data=malformed_data, context={"request": request}
            )
            self.assertFalse(serializer.is_valid())
            self.assertIn(field, serializer.errors)

    def test_partial_update_serializer(self):
        text = BaseTest.generate_string(40)
        category = m.Category.objects.create(text=text, created_by=self.user)
        # Test the serializer with valid data first
        valid_data = {
            "category": category,
            "amount": random.randint(1, 90000),
            "currency": "JPY",
            "days": random.randint(1, 30),
            "text": "Monthly spending limit",
            "created_by": self.user,
        }
        spending_limit = m.SpendingLimit.objects.create(**valid_data)
        request = self.factory.patch(
            f"/api/v1/spending_limits/{spending_limit.id}/"
        )
        request.user = self.user

        # Pass the request in the context
        serializer = SpendingLimitPartialUpdateSerializer(
            instance=spending_limit, context={"request": request}
        )
        self.assertEqual(serializer.data["category"], category.id)

        # Confirm we can partially update the model without some of the fields
        fields = [
            "text",
            "amount",
            "currency",
            "days",
            "text",
        ]
        msg = "This field is required."
        for field in fields:
            malformed_data = self.modify_data(
                valid_data, field, remove_field=True
            )
            malformed_data = self.modify_data(
                malformed_data, "category", category.id
            )
            # Change at least one field to make the data different
            if field != "text":
                new_text = BaseTest.generate_string(40)
                while new_text == malformed_data["text"]:
                    new_text = BaseTest.generate_string(40)
                malformed_data["text"] = new_text
            else:
                new_amount = random.randint(1, 90000)
                while new_amount == malformed_data["amount"]:
                    new_amount = random.randint(1, 90000)
                malformed_data["amount"] = new_amount

            serializer = SpendingLimitPartialUpdateSerializer(
                instance=spending_limit,
                data=malformed_data,
                context={"request": request},
            )
            self.assertTrue(serializer.is_valid())
            self.assertNotIn(field, serializer.errors)
            self.assertNotIn(msg, serializer.errors)

        # Test with invalid data
        invalid_data = {
            "category": "I am definitely not a category",
            "amount": random.randint(-100, -1),
            "currency": BaseTest.generate_string(4),
            # TODO: check empty currency
            "days": random.randint(-10, -1),
            "text": BaseTest.generate_string(151),
            # TODO Check empty text
        }

        for field, invalid_value in invalid_data.items():
            malformed_data = self.modify_data(valid_data, field, invalid_value)
            serializer = SpendingLimitPartialUpdateSerializer(
                data=malformed_data, context={"request": request}
            )
            self.assertFalse(serializer.is_valid())
            self.assertIn(field, serializer.errors)

    def test_can_create_spending_limit(self):
        """
        Ensure we can create a new SpendingLimit object
        """
        self.client.force_login(self.user)
        category_response, _ = self.create_category()
        (
            spending_limit_response,
            spending_limit_input,
        ) = self.create_spending_limit(category_response.data["id"])
        self.assertEqual(
            spending_limit_response.status_code, status.HTTP_201_CREATED
        )
        self.assertEqual(
            spending_limit_response.data["category"],
            category_response.data["id"],
        )
        self.assertEqual(
            spending_limit_response.data["text"], spending_limit_input["text"]
        )
        # fmt: off
        self.assertEqual(
            m.SpendingLimit.objects.first().created_by,  # pylint: disable=E1101; # noqa
            self.user,
        )
        # fmt: on
        spending_limit_list_url = reverse("api:v1:spending_limits")
        response = self.client.get(spending_limit_list_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)
        for key in spending_limit_input:
            self.assertEqual(
                response.data["results"][0][key], spending_limit_input[key]
            )

        if "errors" in spending_limit_response.data:
            self.fail(
                "SpendingLimit creation response's body included at least one error!"
            )

    def test_can_list_spending_limits(self):
        """
        Ensure we can list SpendingLimit objects
        """
        self.client.force_login(self.user)
        category_response, _ = self.create_category()
        _, spending_limit_input = self.create_spending_limit(
            category_response.data["id"]
        )

        spending_limit_list_url = reverse("api:v1:spending_limits")
        response = self.client.get(spending_limit_list_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)
        for key in spending_limit_input:
            self.assertEqual(
                response.data["results"][0][key], spending_limit_input[key]
            )

    def test_can_not_see_other_users_spending_limits(self):
        """
        Ensure that SpendingLimits created by user1 are not listed when logged in as user2
        """
        self.client.force_login(self.user)  # Log in as user1
        category_response, _ = self.create_category()
        (
            spending_limit_response,
            spending_limit_input,
        ) = self.create_spending_limit(category_response.data["id"])
        self.assertEqual(
            spending_limit_response.status_code, status.HTTP_201_CREATED
        )

        # List the SpendingLimits and confirm the created one is listed
        spending_limit_list_url = reverse("api:v1:spending_limits")
        spending_limit_list_response = self.client.get(
            spending_limit_list_url, format="json"
        )
        self.assertEqual(
            spending_limit_list_response.status_code, status.HTTP_200_OK
        )
        self.assertEqual(spending_limit_list_response.data["count"], 1)
        for key in spending_limit_input:
            self.assertEqual(
                spending_limit_list_response.data["results"][0][key],
                spending_limit_input[key],
            )

        self.client.logout()

        # Log in as user2 and check that user1's SpendingLimits are not visible
        self.client.force_login(self.user2)
        spending_limit_list_response = self.client.get(
            spending_limit_list_url, format="json"
        )

        self.assertEqual(
            spending_limit_list_response.status_code, status.HTTP_200_OK
        )
        self.assertEqual(spending_limit_list_response.data["count"], 0)
        self.assertEqual(len(spending_limit_list_response.data["results"]), 0)

    def test_can_update_spending_limit(self):
        """
        Ensure we can update an existing SpendingLimit object with a dynamically generated new amount,
        and confirm the updated value is listed correctly.
        """
        self.client.force_login(self.user)
        category_response, _ = self.create_category()
        self.assertEqual(
            category_response.status_code, status.HTTP_201_CREATED
        )
        category_response2, _ = self.create_category()
        self.assertEqual(
            category_response2.status_code, status.HTTP_201_CREATED
        )
        (
            spending_limit_response,
            spending_limit_input,
        ) = self.create_spending_limit(category_response.data["id"])
        self.assertEqual(
            spending_limit_response.status_code, status.HTTP_201_CREATED
        )
        # Generate a new random amount different from the original one
        new_amount = random.randint(1000, 10000)
        while new_amount == spending_limit_input["amount"]:
            new_amount = random.randint(1000, 10000)
        new_days = random.randint(1, 28)
        while new_days == spending_limit_input["days"]:
            new_days = random.randint(1000, 10000)
        new_currency = "EUR"
        while new_currency == spending_limit_input["currency"]:
            new_currency = random.choice(["USD", "EUR", "JPY"])
        new_text = "Updated spending limit"
        while new_text == spending_limit_input["text"]:
            new_text = BaseTest.generate_string(40)
        update_data = {
            "amount": new_amount,
            "currency": new_currency,
            "days": new_days,
            "text": new_text,
        }
        update_url = reverse(
            "api:v1:spending_limit",
            args=[spending_limit_response.data["id"]],
        )
        response = self.client.patch(
            update_url, data=update_data, format="json"
        )
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["amount"], new_amount)
        self.assertEqual(response.data["currency"], new_currency)
        self.assertEqual(response.data["days"], new_days)
        self.assertEqual(response.data["text"], new_text)

        # Now, list the objects and confirm the amount has been updated
        spending_limit_list_url = reverse("api:v1:spending_limits")
        list_response = self.client.get(spending_limit_list_url, format="json")
        self.assertEqual(list_response.status_code, status.HTTP_200_OK)
        self.assertEqual(list_response.data["count"], 1)
        self.assertEqual(len(list_response.data["results"]), 1)
        for key in update_data:
            self.assertEqual(
                list_response.data["results"][0][key], update_data[key]
            )

    def test_can_partially_update_spending_limit(self):
        # Create an initial spending limit
        initial_data = {
            "category": m.Category.objects.create(
                text="Initial Category", created_by=self.user
            ),
            "amount": 1000,
            "currency": "USD",
            "days": 30,
            "text": "Initial limit",
            "created_by": self.user,
        }
        spending_limit = m.SpendingLimit.objects.create(**initial_data)

        # Create a mock PATCH request
        request = self.factory.patch("/")
        request.user = self.user

        # Test partial update
        update_data = {
            "amount": 2000,
            "text": "Updated limit",
        }
        serializer = SpendingLimitPartialUpdateSerializer(
            instance=spending_limit,
            data=update_data,
            partial=True,
            context={"request": request},
        )
        self.assertTrue(serializer.is_valid())
        updated_spending_limit = serializer.save()

        # Check that the fields were updated correctly
        self.assertEqual(updated_spending_limit.amount, 2000)
        self.assertEqual(updated_spending_limit.text, "Updated limit")
        # Check that other fields remain unchanged
        self.assertEqual(
            updated_spending_limit.category, initial_data["category"]
        )
        self.assertEqual(
            updated_spending_limit.currency, initial_data["currency"]
        )
        self.assertEqual(updated_spending_limit.days, initial_data["days"])

    def test_can_delete_spending_limit(self):
        """
        Ensure we can delete a SpendingLimit object
        """
        self.client.force_login(self.user)
        category_response, _ = self.create_category()
        spending_limit_response, data = self.create_spending_limit(
            category_response.data["id"]
        )

        spending_limit_list_url = reverse("api:v1:spending_limits")
        response = self.client.get(spending_limit_list_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)
        for key in data:
            self.assertEqual(response.data["results"][0][key], data[key])

        delete_url = reverse(
            "api:v1:spending_limit",
            args=[spending_limit_response.data["id"]],
        )
        response = self.client.delete(delete_url)

        self.assertEqual(response.status_code, status.HTTP_204_NO_CONTENT)
        self.assertEqual(m.SpendingLimit.objects.count(), 0)
        response = self.client.get(spending_limit_list_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 0)
        self.assertEqual(len(response.data["results"]), 0)

    def test_cannot_create_spending_limit_with_other_users_category(self):
        """
        Ensure that a user cannot create a SpendingLimit using another user's category ID
        """
        self.client.force_login(self.user)  # Log in as user1
        (
            category_response,
            _,
        ) = self.create_category()  # Create category as user1
        self.assertEqual(
            category_response.status_code, status.HTTP_201_CREATED
        )

        # List the categories and confirm the created one is listed
        category_list_url = reverse("api:v1:categories")
        res = self.client.get(category_list_url, format="json")
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data["count"], 1)
        self.assertEqual(
            res.data["results"][0]["id"],
            category_response.data["id"],
        )

        self.client.logout()

        # Try to create a SpendingLimit using user1's category while logged in as user2
        self.client.force_login(self.user2)
        res2, _ = self.create_spending_limit(category_response.data["id"])

        self.assertEqual(res2.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            res2.data["category"][0],
            f"Invalid pk \"{category_response.data['id']}\" - object does not exist.",
        )
        self.assertEqual(res2.data["category"][0].code, "invalid")

        self.assertIn("category", res2.data)

    def test_cannot_update_spending_limit_with_other_users_category(self):
        """
        Test that a user cannot fully update (PUT request) a SpendingLimit
        by assigning it another user's category
        """
        self.client.force_login(self.user)  # Log in as user1
        category_res, _ = self.create_category()  # Create category as user1
        spending_limit_response, limit_data = self.create_spending_limit(
            category_res.data["id"]
        )  # Create spending limit
        self.assertEqual(
            spending_limit_response.status_code, status.HTTP_201_CREATED
        )

        # List the SpendingLimits and confirm the created one is listed
        spending_limit_list_url = reverse("api:v1:spending_limits")
        spending_limit_list_response = self.client.get(
            spending_limit_list_url, format="json"
        )
        self.assertEqual(
            spending_limit_list_response.status_code, status.HTTP_200_OK
        )
        self.assertEqual(spending_limit_list_response.data["count"], 1)
        self.assertEqual(
            spending_limit_list_response.data["results"][0]["id"],
            spending_limit_response.data["id"],
        )

        self.client.logout()

        # Create another category as user2
        self.client.force_login(self.user2)
        user2_category_res, _ = self.create_category()
        self.assertEqual(
            user2_category_res.status_code, status.HTTP_201_CREATED
        )
        self.client.logout()

        # Try to update user1's SpendingLimit to assign user2's category while logged in as user1
        self.client.force_login(self.user)
        new_amount = random.randint(1000, 10000)
        while new_amount == limit_data["amount"]:
            new_amount = random.randint(1000, 10000)
        new_days = random.randint(1, 28)
        while new_days == limit_data["days"]:
            new_days = random.randint(1, 28)
        new_currency = "EUR"
        while new_currency == limit_data["currency"]:
            new_currency = random.choice(["USD", "EUR", "JPY"])
        new_text = "Updated spending limit"
        while new_text == limit_data["text"]:
            new_text = BaseTest.generate_string(40)
        update_data = {
            "amount": new_amount,
            "currency": new_currency,
            "days": new_days,
            "text": new_text,
            "category": user2_category_res.data["id"],
        }
        update_url = reverse(
            "api:v1:spending_limit",
            args=[spending_limit_response.data["id"]],
        )
        res = self.client.put(update_url, data=update_data, format="json")

        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(
            res.data["category"][0],
            f"Invalid pk \"{user2_category_res.data['id']}\" - object does not exist.",
        )
        self.assertEqual(res.data["category"][0].code, "invalid")

        # List the SpendingLimits and confirm the category has not been updated
        spending_limit_list_url = reverse("api:v1:spending_limits")
        res = self.client.get(spending_limit_list_url, format="json")
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data["count"], 1)
        self.assertEqual(len(res.data["results"]), 1)
        self.assertEqual(
            res.data["results"][0]["category"],
            category_res.data["id"],
        )

    def test_cannot_partially_update_spending_limit_with_other_users_category(
        self,
    ):
        """
        Ensure that a user cannot partially update (PATCH request) a SpendingLimit
        by assigning it another user's category
        """
        self.client.force_login(self.user)  # Log in as user1
        (
            category_response,
            _,
        ) = self.create_category()  # Create category as user1
        spending_limit_response, _ = self.create_spending_limit(
            category_response.data["id"]
        )  # Create spending limit
        self.assertEqual(
            spending_limit_response.status_code, status.HTTP_201_CREATED
        )

        # List the SpendingLimits and confirm the created one is listed
        spending_limit_list_url = reverse("api:v1:spending_limits")
        spending_limit_list_response = self.client.get(
            spending_limit_list_url, format="json"
        )
        self.assertEqual(
            spending_limit_list_response.status_code, status.HTTP_200_OK
        )
        self.assertEqual(spending_limit_list_response.data["count"], 1)
        self.assertEqual(
            spending_limit_list_response.data["results"][0]["id"],
            spending_limit_response.data["id"],
        )

        self.client.logout()

        # Create another category as user2
        self.client.force_login(self.user2)
        user2_category_response, _ = self.create_category()
        self.assertEqual(
            user2_category_response.status_code, status.HTTP_201_CREATED
        )
        self.client.logout()

        # Try to update user1's SpendingLimit to assign user2's category while logged in as user1
        self.client.force_login(self.user)
        update_data = {"category": user2_category_response.data["id"]}
        update_url = reverse(
            "api:v1:spending_limit",
            args=[spending_limit_response.data["id"]],
        )
        response = self.client.patch(
            update_url, data=update_data, format="json"
        )

        self.assertEqual(response.status_code, status.HTTP_400_BAD_REQUEST)

        # List the SpendingLimits and confirm the category has not been updated
        spending_limit_list_url = reverse("api:v1:spending_limits")
        response = self.client.get(spending_limit_list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 1)
        self.assertEqual(len(response.data["results"]), 1)
        self.assertEqual(
            response.data["results"][0]["category"],
            category_response.data["id"],
        )

    def test_cannot_delete_other_users_spending_limit(self):
        """
        Ensure that a user cannot delete another user's SpendingLimit
        """
        self.client.force_login(self.user)  # Log in as user1
        category_response, _ = self.create_category()
        spending_limit_response, _ = self.create_spending_limit(
            category_response.data["id"]
        )
        self.assertEqual(
            spending_limit_response.status_code, status.HTTP_201_CREATED
        )

        # List the SpendingLimits and confirm the created one is listed
        spending_limit_list_url = reverse("api:v1:spending_limits")
        spending_limit_list_response = self.client.get(
            spending_limit_list_url, format="json"
        )
        self.assertEqual(
            spending_limit_list_response.status_code, status.HTTP_200_OK
        )
        self.assertEqual(spending_limit_list_response.data["count"], 1)
        self.assertEqual(
            spending_limit_list_response.data["results"][0]["id"],
            spending_limit_response.data["id"],
        )

        self.client.logout()

        # Try to delete user1's SpendingLimit while logged in as user2
        self.client.force_login(self.user2)
        delete_url = reverse(
            "api:v1:spending_limit",
            args=[spending_limit_response.data["id"]],
        )
        response = self.client.delete(delete_url)

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.client.logout()
        self.client.force_login(self.user)
        spending_limit_list_response = self.client.get(
            spending_limit_list_url, format="json"
        )
        self.assertEqual(
            spending_limit_list_response.status_code, status.HTTP_200_OK
        )
        self.assertEqual(spending_limit_list_response.data["count"], 1)
        self.assertEqual(
            spending_limit_list_response.data["results"][0]["id"],
            spending_limit_response.data["id"],
        )

    @patch("api.v1.views.spending_limit_views.SpendingLimitDetail")
    @patch("budgets.serializers.SpendingLimitSerializer")
    def test_integrity_errors_are_catched_on_create(
        self, mock_cat_serializer, _
    ):
        self.client.force_login(self.user)
        cat_r, _ = self.create_category()
        self.assertEqual(cat_r.status_code, status.HTTP_201_CREATED)

        spl_res, inc_data = self.create_spending_limit(cat_r.data["id"])
        self.assertEqual(spl_res.status_code, status.HTTP_201_CREATED)
        self.assertEqual(spl_res.data["category"], cat_r.data["id"])
        self.assertEqual(spl_res.data["amount"], inc_data["amount"])
        self.assertEqual(spl_res.data["currency"], inc_data["currency"])
        self.assertEqual(spl_res.data["days"], inc_data["days"])
        self.assertEqual(spl_res.data["text"], inc_data["text"])
        # fmt: off
        self.assertEqual(
            m.SpendingLimit.objects.first().created_by, self.user  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        expenses_list_url = reverse("api:v1:spending_limits")
        response = self.client.get(expenses_list_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data["results"][0]["category_text"], cat_r.data["text"]
        )
        self.assertEqual(
            response.data["results"][0]["amount"], inc_data["amount"]
        )
        self.assertEqual(
            response.data["results"][0]["currency"], inc_data["currency"]
        )
        self.assertEqual(response.data["results"][0]["days"], inc_data["days"])
        self.assertEqual(response.data["results"][0]["text"], inc_data["text"])
        self.assertEqual(response.data["count"], 1)

        # Force the serializer to Throw an Integrity error
        serializer = mock_cat_serializer.return_value
        msg = "I AM A MOCKED INTEGRITY ERROR. I HOPE YOU CATCH ME: ROAAAARR!"
        serializer.save.side_effect = IntegrityError(msg)

        # Create an instance of the class view
        view = SpendingLimitList()
        create_url = reverse("api:v1:spending_limits")
        # fmt: off
        cat = m.Category.objects.filter(  # pylint: disable=E1101; # noqa
            created_by=self.user
        ).get()
        # fmt: on
        ok_data = {
            "category": cat.id,
            "amount": random.randint(1, 90000),
            "note": BaseTest.generate_string(20),
            # Gererate a random date that is between 42 and 365 days ago
            "date": (
                date.today() - timedelta(days=random.randint(42, 365))
            ).isoformat(),
        }

        def mocked_get_data(*args, **_):
            if args[0] == "category":
                return cat
            elif args[0] == "amount":
                return ok_data["amount"]
            elif args[0] == "currency":
                return ok_data["currency"]
            elif args[0] == "days":
                return ok_data["days"]
            elif args[0] == "text":
                return ok_data["text"]
            return ok_data["date"]

        serializer.validated_data.get.side_effect = mocked_get_data

        request = self.client.post(create_url, ok_data, format="json")

        # Set the request user
        view.request = request
        request.user = self.user

        # Confirm that the mocks works, and that ok_data actually thew
        # an exception and confirm it was correctly handled by the backend
        failed_res = view.perform_create(serializer)
        self.assertEqual(
            failed_res.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR
        )
        msg = (
            "Generic error thrown while creating the SpendingLimit. "
            "Refer the web container logs if you're the admin."
        )
        self.assertEqual(failed_res.data["non_field_errors"][0], msg)

    @patch("api.v1.views.spending_limit_views.SpendingLimitList")
    @patch("budgets.serializers.SpendingLimitSerializer")
    def test_integrity_errors_are_catched_on_update(
        self, mock_cat_serializer, _
    ):
        self.client.force_login(self.user)
        cat_r, _ = self.create_category()
        spl_res, spl_data = self.create_spending_limit(cat_r.data["id"])
        self.assertEqual(spl_res.data["category"], cat_r.data["id"])
        self.assertEqual(spl_res.data["amount"], spl_data["amount"])
        self.assertEqual(spl_res.data["currency"], spl_data["currency"])
        self.assertEqual(spl_res.data["days"], spl_data["days"])
        self.assertEqual(spl_res.data["text"], spl_data["text"])
        # fmt: off
        self.assertEqual(
            m.SpendingLimit.objects.filter(created_by=self.user)  # pylint: disable=E1101; # noqa
            .first()
            .created_by,
            self.user,
        )
        # fmt: on
        expenses_list_url = reverse("api:v1:spending_limits")
        res = self.client.get(expenses_list_url, format="json")
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data["count"], 1)
        income_id = res.data["results"][0]["id"]

        amount = random.randint(1, 90000)
        while amount == spl_data["amount"]:
            amount = random.randint(1, 90000)
        currency = random.choice(["USD", "EUR", "JPY"])
        while currency == spl_data["currency"]:
            currency = random.choice(["USD", "EUR", "JPY"])
        days = random.randint(1, 28)
        while days == spl_data["days"]:
            days = random.randint(1, 28)
        text = BaseTest.generate_string(40)
        while text == spl_data["text"]:
            text = BaseTest.generate_string(40)
        cat_r2, _ = self.create_category()

        # Confirm update works properly before mocking it
        data = {
            "category": cat_r2.data["id"],
            "amount": amount,
            "currency": currency,
            "days": days,
            "text": text,
        }
        update_response, _ = self.replace_all_spendinglimit_fields(
            income_id, data
        )
        self.assertEqual(update_response.status_code, status.HTTP_200_OK)

        incomes_list = reverse("api:v1:spending_limits")
        second_response = self.client.get(incomes_list, format="json")
        # Assure that since we did an update, and nothing was created
        self.assertEqual(second_response.data["count"], 1)
        self.assertEqual(second_response.status_code, status.HTTP_200_OK)
        self.assertEqual(second_response.data["results"][0]["amount"], amount)
        self.assertEqual(
            second_response.data["results"][0]["currency"], currency
        )
        self.assertEqual(second_response.data["results"][0]["days"], days)
        self.assertEqual(second_response.data["results"][0]["text"], text)
        self.assertEqual(
            second_response.data["results"][0]["category"], cat_r2.data["id"]
        )

        # Create a mock request
        amount2 = random.randint(1, 90000)
        note2 = BaseTest.generate_string(20)
        record_date2 = (
            date.today() - timedelta(days=random.randint(42, 365))
        ).isoformat()
        cat_r3, _ = self.create_category()
        ok_data = {
            "category": cat_r3.data["id"],
            "amount": amount2,
            "note": note2,
            "date": record_date2,
        }
        # fmt: off
        cat_r3_obj = m.Category.objects.filter(  # pylint: disable=E1101; # noqa
            created_by=self.user, id=cat_r3.data["id"]
        ).get()
        # fmt: on
        serializer = mock_cat_serializer.return_value
        msg = "I AM A MOCKED INTEGRITY ERROR. I HOPE YOU CATCH ME: ROAAAARR!"
        serializer.save.side_effect = IntegrityError(msg)

        def mocked_get_data(*args, **_):
            if args[0] == "category":
                return cat_r3_obj
            elif args[0] == "amount":
                return ok_data["amount"]
            elif args[0] == "note":
                return ok_data["note"]
            return ok_data["date"]

        serializer.validated_data.get.side_effect = mocked_get_data

        # Create an instance of the class view
        view = SpendingLimitDetail()
        update_url = reverse("api:v1:spending_limit", kwargs={"pk": income_id})
        request = self.client.post(update_url, ok_data)

        # Set the request user
        view.request = request
        request.user = self.user

        # Confirm that the mocks works, and that ok_data actually thew
        # an exception and confirm it was correctly handled by the backend
        failed_res = view.perform_update(serializer)
        self.assertEqual(
            failed_res.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR
        )
        msg = (
            "An error occurred while updating the SpendingLimit. "
            "Refer the web container logs if you're the admin."
        )
        self.assertEqual(failed_res.data["non_field_errors"][0], msg)
