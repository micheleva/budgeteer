# Copyright: (c) 2021, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
import logging
from unittest.mock import patch
from urllib.parse import urlencode

from django.contrib.auth.models import User
from django.db.utils import IntegrityError
from django.urls import reverse
from rest_framework import status

from api.v1.tests.test_base import BaseApiTest
from api.v1.views.category_views import CategoryList

import budgets.models as m
from budgets.tests.base import BaseTest


logger = logging.getLogger(__name__)


class CategoryTest(BaseApiTest):
    """
    Test Category model related v1 rest api behaviour
    """

    def setUp(self):  # pylint: disable=C0103; # noqa
        # Instrument the unit tests to fail if they run for too long
        super().setUp()
        # fmt: off
        self.user = User.objects.create_user(
            username=BaseTest.generate_string(20),
            password=BaseTest.generate_string(20),
        )
        self.user2 = User.objects.create_user(
            username=BaseTest.generate_string(20),
            password=BaseTest.generate_string(20),
        )
        # fmt: on

    def test_create_and_list_categories_and_other_user_can_not_see_them(self):
        """
        Ensure we can create a new category objectand other users can't see it
        """
        self.client.force_login(self.user)

        # Create a category
        category_response, _ = self.create_category()
        category_text = category_response.data["text"]

        self.assertEqual(
            category_response.status_code, status.HTTP_201_CREATED
        )
        # fmt: off
        self.assertEqual(
            m.Category.objects.get(created_by=self.user).text,  # pylint: disable=E1101; # noqa
            category_text,
        )
        # fmt: on
        categoriy_list_url = reverse("api:v1:categories")
        response = self.client.get(categoriy_list_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["text"], category_text)
        self.assertEqual(response.data["count"], 1)

        # Another user CANNOT see that category
        self.client.force_login(self.user2)

        response = self.client.get(categoriy_list_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 0)

    def test_can_not_create_categories_for_other_users(self):
        """
        Ensure we can NOT create a new category object linked to another user
        by posting other users' ID in the payload
        """
        self.client.force_login(self.user)

        # Guido asked someone in the IT team to look up Frank's user ID.
        # Guido tries to create a Category object and have it linked to
        # Frank's user ID as a prank
        self.client.force_login(self.user2)

        # Create a category
        create_url = reverse("api:v1:categories")
        text = BaseTest.generate_string(40)
        # Guido grins while injecting Frank's user id
        data = {"text": text, "created_by": self.user.id}
        res = self.client.post(create_url, data, format="json")
        category_text = res.data["text"]

        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            m.Category.objects.get(  # pylint: disable=E1101; # noqa
                created_by=self.user2.id
            ).text,
            category_text,
        )
        # fmt: on
        categoriy_list_url = reverse("api:v1:categories")
        response = self.client.get(categoriy_list_url, format="json")

        # Guido is sad: the app does not seem vulnerable to said attack.
        # The category was correctly linked to Guido's account
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["text"], category_text)
        self.assertEqual(response.data["count"], 1)

        # Frank logs in, and he finds no Categories, as he created none
        self.client.force_login(self.user)

        response = self.client.get(categoriy_list_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 0)

    def test_create_and_list_categories_using_filters(self):
        """
        Ensure we can create new category objects
        and then using thos object's data in the filtered URL
        the result will be propertly filtered
        """
        self.client.force_login(self.user)

        # Create a category
        category_response, _ = self.create_category()
        category_text = category_response.data["text"]
        self.assertEqual(
            category_response.status_code, status.HTTP_201_CREATED
        )
        # fmt: off
        self.assertEqual(
            m.Category.objects.get(  # pylint: disable=E1101; # noqa
                created_by=self.user
            ).text,
            category_text,
        )
        # fmt: on
        category_list_url = reverse("api:v1:categories")
        response = self.client.get(category_list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["text"], category_text)
        self.assertEqual(response.data["count"], 1)

        second_category_response, _ = self.create_category()
        second_text = second_category_response.data["text"]

        # use first filtered url /api/v1/categories?name=xxxx
        filtered_url = (
            category_list_url + "?" + urlencode({"name": category_text})
        )
        second_response = self.client.get(filtered_url, format="json")
        self.assertEqual(second_response.status_code, status.HTTP_200_OK)
        self.assertEqual(second_response.data["count"], 1)
        self.assertEqual(
            second_response.data["results"][0]["text"], category_text
        )
        self.assertNotEqual(
            second_response.data["results"][0]["text"], second_text
        )

        # filter by a subset of the category text, and by the category.txt in
        # all caps
        fil_url = (
            category_list_url + "?" + urlencode({"name": category_text[:-1]})
        )
        second_subset_response = self.client.get(fil_url, format="json")
        self.assertEqual(
            second_subset_response.status_code, status.HTTP_200_OK
        )
        self.assertEqual(second_subset_response.data["count"], 1)
        self.assertEqual(
            second_subset_response.data["results"][0]["text"], category_text
        )
        self.assertNotEqual(
            second_subset_response.data["results"][0]["text"], second_text
        )

        # filter by the category.txt in all caps
        fil_url2 = (
            category_list_url
            + "?"
            + urlencode({"name": category_text.upper()})
        )
        second_subset_response = self.client.get(fil_url2, format="json")
        self.assertEqual(
            second_subset_response.status_code, status.HTTP_200_OK
        )
        self.assertEqual(second_subset_response.data["count"], 1)
        self.assertEqual(
            second_subset_response.data["results"][0]["text"], category_text
        )
        self.assertNotEqual(
            second_subset_response.data["results"][0]["text"], second_text
        )

    def test_same_user_can_not_have_multiple_categories_with_the_same_name(
        self,
    ):
        self.client.force_login(self.user)

        # Create a category
        category_response, _ = self.create_category()
        category_text = category_response.data["text"]

        self.assertEqual(
            category_response.status_code, status.HTTP_201_CREATED
        )
        # fmt: off
        self.assertEqual(
            m.Category.objects.get(created_by=self.user).text,
            category_text,  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        categoriy_list_url = reverse("api:v1:categories")
        response = self.client.get(categoriy_list_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["text"], category_text)
        self.assertEqual(response.data["count"], 1)

        # Create a category with the same text
        create_url = reverse("api:v1:categories")
        text = category_text
        is_archived = False
        data = {
            "text": text,
            is_archived: is_archived,
        }
        create_duplicated_response = self.client.post(
            create_url, data, format="json"
        )

        self.assertEqual(
            create_duplicated_response.status_code, status.HTTP_400_BAD_REQUEST
        )
        self.assertEqual(
            create_duplicated_response.data["text"][0].code, "invalid"
        )
        err = "A Category with this text already exists for this user."
        self.assertEqual(str(create_duplicated_response.data["text"][0]), err)

        response = self.client.get(categoriy_list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["text"], category_text)
        self.assertEqual(response.data["count"], 1)

    def test_different_users_can_have_multiple_categories_with_the_same_name(
        self,
    ):
        self.client.force_login(self.user)

        # Create a category
        category_response, _ = self.create_category()
        category_text = category_response.data["text"]

        self.assertEqual(
            category_response.status_code, status.HTTP_201_CREATED
        )
        # fmt: off
        self.assertEqual(
            m.Category.objects.get(created_by=self.user).text,
            category_text,  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        categoriy_list_url = reverse("api:v1:categories")
        response = self.client.get(categoriy_list_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["text"], category_text)
        self.assertEqual(response.data["count"], 1)

        # Another user can create a category with the same name
        self.client.force_login(self.user2)

        create_url = reverse("api:v1:categories")
        text = category_text
        is_archived = False
        data = {
            "text": text,
            is_archived: is_archived,
        }
        res = self.client.post(create_url, data, format="json")
        # fmt: off
        c_text = (
            m.Category.objects.filter(created_by=self.user2)  # pylint: disable=E1101; # noqa
            .get()
            .text
        )
        # fmt: on
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        self.assertEqual(c_text, category_text)

        categoriy_list_url = reverse("api:v1:categories")
        response = self.client.get(categoriy_list_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["results"][0]["text"], category_text)
        self.assertEqual(response.data["count"], 1)
        # fmt: off
        c_count = m.Category.objects.filter(  # pylint: disable=E1101; # noqa
            text=category_text,
        ).count()
        # fmt: on
        self.assertEqual(c_count, 2)

    # Can update the model with PUT requests
    def test_can_entirely_edit_own_categories(self):
        self.client.force_login(self.user)

        # Create a category
        category_response, _ = self.create_category()
        category_text = category_response.data["text"]
        category_id = category_response.data["id"]

        self.assertEqual(
            category_response.status_code, status.HTTP_201_CREATED
        )
        # fmt: off
        c_text = m.Category.objects.get(  # pylint: disable=E1101; # noqa
            created_by=self.user
        ).text
        # fmt: on
        self.assertEqual(c_text, category_text)

        category_edit_url = reverse(
            "api:v1:category_detail", kwargs={"pk": category_id}
        )
        response = self.client.get(category_edit_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["id"], category_id)
        self.assertEqual(response.data["text"], category_text)
        self.assertEqual(response.data["is_archived"], False)

        new_text = BaseTest.generate_string(20)
        data = {
            "text": new_text,
            "is_archived": True,
        }
        _ = self.client.put(category_edit_url, data, format="json")

        category_edit_url = reverse(
            "api:v1:category_detail", kwargs={"pk": category_id}
        )
        response = self.client.get(category_edit_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["id"], category_id)
        self.assertEqual(response.data["text"], new_text)
        self.assertEqual(response.data["is_archived"], True)

    def test_users_can_partially_edit_categories(self):
        self.client.force_login(self.user)

        # Create a category
        category_response, _ = self.create_category()
        category_text = category_response.data["text"]
        category_id = category_response.data["id"]

        self.assertEqual(
            category_response.status_code, status.HTTP_201_CREATED
        )
        # fmt: off
        self.assertEqual(
            m.Category.objects.get(created_by=self.user).text,  # pylint: disable=E1101; # noqa
            category_text,
        )
        # fmt: on
        category_edit_url = reverse(
            "api:v1:category_detail", kwargs={"pk": category_id}
        )
        response = self.client.get(category_edit_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["id"], category_id)
        self.assertEqual(response.data["text"], category_text)
        self.assertEqual(response.data["is_archived"], False)

        # Edit only text
        new_text = BaseTest.generate_string(20)
        data = {"text": new_text}
        _ = self.client.patch(category_edit_url, data, format="json")

        category_edit_url = reverse(
            "api:v1:category_detail", kwargs={"pk": category_id}
        )
        response = self.client.get(category_edit_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["id"], category_id)
        self.assertEqual(response.data["text"], new_text)
        self.assertEqual(response.data["is_archived"], False)

        # Edit only is_archived
        data = {"is_archived": True}
        category_update_2_response = self.client.patch(
            category_edit_url, data, format="json"
        )
        self.assertEqual(
            category_update_2_response.status_code, status.HTTP_200_OK
        )
        self.assertEqual(category_update_2_response.data["id"], category_id)
        self.assertEqual(category_update_2_response.data["text"], new_text)
        self.assertEqual(category_update_2_response.data["is_archived"], True)

        category_edit_url = reverse(
            "api:v1:category_detail", kwargs={"pk": category_id}
        )
        response = self.client.get(category_edit_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["id"], category_id)
        self.assertEqual(response.data["text"], new_text)
        self.assertEqual(response.data["is_archived"], True)

        # Edit only is_archived, but also has the original text in the patch
        # body
        data = {"text": new_text, "is_archived": True}
        category_update_3_response = self.client.patch(
            category_edit_url, data, format="json"
        )
        self.assertEqual(
            category_update_3_response.status_code, status.HTTP_200_OK
        )

        category_edit_url = reverse(
            "api:v1:category_detail", kwargs={"pk": category_id}
        )
        response = self.client.get(category_edit_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["id"], category_id)
        self.assertEqual(response.data["text"], new_text)
        self.assertEqual(response.data["is_archived"], True)

    def test_user_can_not_entirely_edit_other_users_categories(self):
        self.client.force_login(self.user)

        # Create a category
        category_response, _ = self.create_category()
        category_text = category_response.data["text"]
        category_id = category_response.data["id"]

        self.assertEqual(
            category_response.status_code, status.HTTP_201_CREATED
        )
        # fmt: off
        self.assertEqual(
            m.Category.objects.get(created_by=self.user).text,  # pylint: disable=E1101; # noqa
            category_text,
        )
        # fmt: on
        category_edit_url = reverse(
            "api:v1:category_detail", kwargs={"pk": category_id}
        )
        response = self.client.get(category_edit_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["id"], category_id)
        self.assertEqual(response.data["text"], category_text)
        self.assertEqual(response.data["is_archived"], False)

        # Another user CANNOT edit that category
        self.client.force_login(self.user2)

        new_text = BaseTest.generate_string(20)
        data = {
            "text": new_text,
            "is_archived": True,
        }
        _ = self.client.patch(category_edit_url, data, format="json")

        category_edit_url = reverse(
            "api:v1:category_detail", kwargs={"pk": category_id}
        )
        response = self.client.get(category_edit_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(str(response.data["detail"]), "Not found.")
        self.assertEqual(response.data["detail"].code, "not_found")

    # Update the model with PATCH requests
    def test_users_can_not_partially_edit_other_users_categories(self):
        self.client.force_login(self.user)

        # Create a category
        category_response, _ = self.create_category()
        category_text = category_response.data["text"]
        category_id = category_response.data["id"]

        self.assertEqual(
            category_response.status_code, status.HTTP_201_CREATED
        )
        # fmt: off
        self.assertEqual(
            m.Category.objects.get(created_by=self.user).text,  # pylint: disable=E1101; # noqa
            category_text,
        )
        # fmt: on
        category_edit_url = reverse(
            "api:v1:category_detail", kwargs={"pk": category_id}
        )
        response = self.client.get(category_edit_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["id"], category_id)
        self.assertEqual(response.data["text"], category_text)
        self.assertEqual(response.data["is_archived"], False)

        # Another user CANNOT edit that category
        self.client.force_login(self.user2)

        # Edit only text
        new_text = BaseTest.generate_string(20)
        data = {"text": new_text}
        _ = self.client.patch(category_edit_url, data, format="json")

        category_edit_url = reverse(
            "api:v1:category_detail", kwargs={"pk": category_id}
        )
        response = self.client.get(category_edit_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(str(response.data["detail"]), "Not found.")
        self.assertEqual(response.data["detail"].code, "not_found")

        # Edit only is_archived
        new_text = BaseTest.generate_string(20)
        data = {
            "is_archived": True,
        }
        _ = self.client.patch(category_edit_url, data, format="json")

        category_edit_url = reverse(
            "api:v1:category_detail", kwargs={"pk": category_id}
        )
        response = self.client.get(category_edit_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_404_NOT_FOUND)
        self.assertEqual(str(response.data["detail"]), "Not found.")
        self.assertEqual(response.data["detail"].code, "not_found")

    @patch("api.v1.views.category_views.CategoryList")
    @patch("budgets.serializers.CategorySerializer")
    def test_integrity_errors_are_catched_on_create(
        self, mock_cat_serializer, _
    ):
        self.client.force_login(self.user)
        category_response, _ = self.create_category()
        self.assertEqual(
            category_response.status_code, status.HTTP_201_CREATED
        )

        # Force the serializer to Throw an Integrity error
        serializer = mock_cat_serializer.return_value
        msg = "I AM A MOCKED INTEGRITY ERROR. I HOPE YOU CATCH ME: ROAAAARR!"
        serializer.save.side_effect = IntegrityError(msg)

        # Create a mock request
        text = BaseTest.generate_string(20)
        is_archived = False
        ok_data = {"text": text, is_archived: is_archived}

        # Create an instance of the view
        view = CategoryList()
        request = self.client.post(reverse("api:v1:categories"), ok_data)

        # Set the request user
        view.request = request
        request.user = self.user

        # Confirm that the mocks works, and that ok_data actually thew an
        # exception and confirm it was correctly handled by the backend
        failed_res = view.perform_create(serializer)
        self.assertEqual(
            failed_res.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR
        )
        msg = (
            "Generic error thrown while creating the Category. "
            "Refer the web container logs if you're the admin"
        )
        self.assertEqual(failed_res.data["non_field_errors"][0], msg)
