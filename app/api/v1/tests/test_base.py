# Copyright: (c) 2021, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from datetime import date
from datetime import timedelta
from datetime import datetime
import logging
import random
import signal
import string

from django.shortcuts import resolve_url
from django.urls import reverse
from rest_framework.test import APITestCase

import budgets.models as m
from budgets.tests.base import BaseTest
from decimal import Decimal, getcontext


logger = logging.getLogger(__name__)


class BaseApiTest(APITestCase):
    """
    Base class for API testing including models creation utilities
    """

    # Static timeout for all tests
    TIMEOUT = 10

    def timeout_handler(self, signum, frame):
        """
        Raise a TimeoutError when a test takes too long
        """
        raise TimeoutError(
            f"Infinite loop or slow test detected: intentionally timing out! Signal number {signum} frame: {frame}"
        )

    def setUp(self):
        """
        Override the setUp method to apply signal.alarm to every test
        """
        super().setUp()

        # Set the signal handler and alarm before each test method
        signal.signal(signal.SIGALRM, self.timeout_handler)
        signal.alarm(self.TIMEOUT)  # Set alarm to go off after TIMEOUT seconds

    def tearDown(self):
        signal.alarm(0)  # Disable the alarm after each test
        super().tearDown()

    # Helper function for test_creating_malformed_assets_throw_an_error
    def modify_data(
        self,
        original_data,
        field_to_modify,
        modification_value=None,
        remove_field=False,
    ):
        malformed_data = original_data.copy()
        if remove_field:
            if field_to_modify in malformed_data:
                del malformed_data[field_to_modify]
        else:
            malformed_data[field_to_modify] = modification_value
        return malformed_data

    @staticmethod
    def generate_string(length=10):
        """Generate a random string of a given length"""
        char = string.ascii_lowercase
        return "".join(random.choice(char) for i in range(length))

    @staticmethod
    def generate_number(max_digits=10, decimal_places=2):
        """Generate a random decimal number"""
        getcontext().prec = max_digits
        return Decimal(random.uniform(0, 90000)).quantize(
            Decimal(10) ** -decimal_places
        )

    @staticmethod
    def generate_date(start_days_ago=365, end_days_ago=0):
        """
        Generate a random date between `start_days_ago` and `end_days_ago`
        days ago
        """
        return datetime.now() - timedelta(
            days=random.randint(end_days_ago, start_days_ago)
        )

    def create_account(self):
        create_url = reverse("api:v1:accounts")
        text = BaseTest.generate_string(20)
        credit_entity = BaseTest.generate_string(20)
        data = {
            "text": text,
            "credit_entity": credit_entity,
            "is_foreign_currency": False,
        }
        return (self.client.post(create_url, data, format="json"), data)

    # Can update the model with PATCH requests
    def partial_update_account(self, acc_id, data):
        """
        Update the model with PATCH requests
        """
        update_url = reverse("api:v1:account", kwargs={"pk": acc_id})
        return self.client.patch(update_url, data, format="json")

    def create_dividend(self, account_id):
        create_url = reverse("api:v1:dividends_create")
        asset_text = BaseTest.generate_string(20)
        local_amount = random.randint(1, 90000)
        rate = random.randint(88, 170)
        foreign_amount = local_amount * rate
        currency = random.choice(["USD", "EUR"])
        record_date = date.today().isoformat()
        data = {
            "account": account_id,
            "asset_text": asset_text,
            "local_amount": local_amount,
            "foreign_amount": foreign_amount,
            "currency": currency,
            "rate": rate,
            "record_date": record_date,
        }
        return (self.client.post(create_url, data, format="json"), data)

    def create_bulk_expenses(self, user):
        text = BaseTest.generate_string(20)
        # fmt: off
        cat= m.Category.objects.create(  # pylint: disable=E1101; # noqa
            created_by=user,
            text=text,
            is_archived=False,
            )
        # fmt: on
        expenses = []
        for _ in range(5000):
            amount = self.generate_number(max_digits=10, decimal_places=2)
            note = BaseTest.generate_string(20)
            data = {
                "category": cat,
                "amount": amount,
                "note": note,
                "date": date.today().isoformat(),
                "created_by": user,
            }
            expense = m.Expense(**data)
            expenses.append(expense)
        # fmt: off
        m.Expense.objects.bulk_create(expenses)  # pylint: disable=E1101; # noqa
        # fmt: on

    def delete_all_dividends(self):
        delete_url = reverse("api:v1:dividends_delete")
        return self.client.delete(delete_url, format="json")

    def create_dividend_from_data(self, data):
        create_url = reverse("api:v1:dividends_create")
        return (self.client.post(create_url, data, format="json"), data)

    def create_asset(
        self, account_id, date_in_the_past=False, target_date=None
    ):
        create_url = reverse("api:v1:asset_create")
        amount = random.randint(1, 42)
        asset_text = BaseTest.generate_string(60)

        current_foreign_value = amount * random.randint(2, 42)
        rate = random.randint(88, 160)
        current_local_value = current_foreign_value * rate

        increase_perc = random.uniform(0.1, 0.2)

        bought_price_foreign = current_foreign_value * increase_perc
        bought_price_local = current_local_value * increase_perc

        if target_date is not None:
            record_date = target_date
        else:
            if date_in_the_past:
                # Gererate a random date that is between 420 and 3650 days ago
                record_date = (
                    date.today() - timedelta(days=random.randint(420, 3650))
                ).isoformat()
            else:
                record_date = date.today().isoformat()

        data = {
            "account": account_id,
            "amount": amount,
            "text": asset_text,
            "foreign_currency": random.choice(["USD", "EUR"]),
            "current_local_value": current_local_value,
            "current_foreign_value": current_foreign_value,
            "bought_price_local": bought_price_local,
            "bought_price_foreign": bought_price_foreign,
            "net_loss_local": (bought_price_local - current_foreign_value),
            "net_loss_foreign": (bought_price_foreign - current_foreign_value),
            "record_date": record_date,
            "rate": rate,
        }
        return (self.client.post(create_url, data, format="json"), data)

    def create_category_from_data(self, data):
        create_url = reverse("api:v1:categories")
        return (self.client.post(create_url, data, format="json"), data)

    def create_asset_from_data(self, data):
        create_url = reverse("api:v1:asset_create")
        return (self.client.post(create_url, data, format="json"), data)

    def create_category(self):
        create_url = reverse("api:v1:categories")
        text = BaseTest.generate_string(20)
        is_archived = False
        data = {
            "text": text,
            is_archived: is_archived,
        }
        return (self.client.post(create_url, data, format="json"), data)

    def create_expense(self, category_id, date_in_the_past=False):
        create_url = reverse("api:v1:expenses")
        amount = self.generate_number(max_digits=10, decimal_places=2)
        note = BaseTest.generate_string(20)
        if date_in_the_past:
            today = date.today()
            # Gererate a random date that is between 42 and 365 days ago
            more_than_six_months_ago = today - timedelta(
                days=random.randint(42, 365)
            )
            record_date = more_than_six_months_ago.isoformat()
        else:
            record_date = date.today().isoformat()

        data = {
            "category": category_id,
            "amount": amount,
            "note": note,
            "date": record_date,
        }
        return (self.client.post(create_url, data, format="json"), data)

    def delete_expense(self, expense_id):
        delete_url = resolve_url("api:v1:expense", expense_id)
        return (self.client.delete(delete_url, format="json"), expense_id)

    def replace_all_expense_fields(self, expense_id, data):
        """
        Update the model with PUT requests
        """
        delete_url = resolve_url("api:v1:expense", expense_id)
        return (self.client.put(delete_url, data, format="json"), expense_id)

    def partial_update_expense(self, expense_id, data):
        """
        Update the model with PATCH requests
        """
        patch_url = resolve_url("api:v1:expense", expense_id)
        return (self.client.patch(patch_url, data, format="json"), expense_id)

    def create_monthly_balance_category(self):
        create_url = reverse("api:v1:monthly_balance_categories")
        text = BaseTest.generate_string(20)
        is_foreign_currency = False
        data = {
            "text": text,
            "is_foreign_currency": is_foreign_currency,
        }
        return (self.client.post(create_url, data, format="json"), data)

    def edit_monthly_balance_category(self, monthly_balance_id, post_data):
        edit_url = reverse(
            "api:v1:monthly_balance_category",
            kwargs={"pk": monthly_balance_id},
        )
        return self.client.patch(edit_url, post_data, format="json")

    def delete_monthly_balance_category(self, monthly_balance_category_id):
        delete_url = resolve_url(
            "api:v1:monthly_balance_category", monthly_balance_category_id
        )
        return (
            self.client.delete(delete_url, format="json"),
            monthly_balance_category_id,
        )

    def create_monthly_balance(
        self, monthly_category_id, date_in_the_past=False, target_date=None
    ):
        create_url = reverse("api:v1:monthly_balances")
        amount = random.randint(1, 90000)

        if target_date is not None:
            record_date = target_date
        else:
            if date_in_the_past:
                # Gererate a random date that is between 420 and 3650 days ago
                record_date = (
                    date.today() - timedelta(days=random.randint(420, 3650))
                ).isoformat()
            else:
                record_date = date.today().isoformat()

            record_date = record_date[0 : len(record_date) - 2] + "01"

        data = {
            "category": monthly_category_id,
            "amount": amount,
            "date": record_date,
        }
        return (self.client.post(create_url, data, format="json"), data)

    def edit_monthly_balance(self, monthly_balance_id, post_data):
        edit_url = reverse(
            "api:v1:monthly_balances", kwargs={"pk": monthly_balance_id}
        )
        return self.client.patch(edit_url, post_data, format="json")

    def delete_monthly_balance(self, monthly_balance_id):
        delete_url = resolve_url("api:v1:monthly_balances", monthly_balance_id)
        return (
            self.client.delete(delete_url, format="json"),
            monthly_balance_id,
        )

    def create_income_category(self):
        create_url = reverse("api:v1:income_categories")
        text = BaseTest.generate_string(40)
        data = {"text": text}
        return (self.client.post(create_url, data, format="json"), data)

    def create_income_from_data(self, data):
        create_url = reverse("api:v1:incomes")
        return (self.client.post(create_url, data, format="json"), data)

    def create_income(self, category_id, date_in_the_past=False):
        amount = random.randint(1, 90000)
        note = BaseTest.generate_string(20)
        if date_in_the_past:
            today = date.today()
            # Gererate a random date that is between 42 and 365 days ago
            more_than_six_months_ago = today - timedelta(
                days=random.randint(42, 365)
            )
            record_date = more_than_six_months_ago.isoformat()
        else:
            record_date = date.today().isoformat()

        data = {
            "category": category_id,
            "amount": amount,
            "note": note,
            "date": record_date,
        }
        return self.create_income_from_data(data)

    def delete_income(self, income_id):
        delete_url = reverse("api:v1:income", kwargs={"pk": income_id})
        return self.client.delete(delete_url, format="json")

    # Update the model with PUT requests
    def replace_all_income_fields(self, income_id, data):
        """
        Update the model with PUT requests
        """
        pur_url = resolve_url("api:v1:income", income_id)
        return (self.client.put(pur_url, data, format="json"), income_id)

    # Update the model with PATCH requests
    def partial_update_income(self, income_id, data):
        """
        Update the model with PATCH requests
        """
        patch_url = resolve_url("api:v1:income", income_id)
        return (self.client.patch(patch_url, data, format="json"), income_id)

    def create_goal(self):
        create_url = reverse("api:v1:goals")
        text = BaseTest.generate_string(20)
        amount = random.randint(1, 90000)
        note = BaseTest.generate_string(20)
        is_archived = random.choice([True, False])
        data = {
            "text": text,
            "amount": amount,
            "note": note,
            "is_archived": is_archived,
        }
        return (self.client.post(create_url, data, format="json"), data)

    def get_goal(self, goal_id):
        get_url = resolve_url("api:v1:goal_detail", goal_id)
        return self.client.get(get_url, format="json")

    def delete_goal(self, goal_id):
        delete_url = resolve_url("api:v1:goal_detail", goal_id)
        return self.client.delete(delete_url, format="json")

    def edit_goal(self, goal_id, data):
        patch_url = resolve_url("api:v1:goal_detail", goal_id)
        return self.client.patch(patch_url, data, format="json")

    def create_savingcategory(self):
        create_url = reverse("api:v1:savingcategories")
        text = BaseTest.generate_string(20)
        data = {"text": text}
        return (self.client.post(create_url, data, format="json"), data)

    def create_savingcategory_from_data(self, data):
        create_url = reverse("api:v1:savingcategories")
        return self.client.post(create_url, data, format="json")

    def get_savingcategories(self):
        create_url = reverse("api:v1:savingcategories")
        return self.client.get(create_url, format="json")

    # Update the model with PATCH requests
    def partially_update_savingcategories(self, c_id, data):
        """
        Update the model with PATCH requests
        """
        patch_url = reverse("api:v1:savingcategory", kwargs={"pk": c_id})
        return self.client.patch(patch_url, data, format="json")

    def update_savingcategories(self, c_id, data):
        """
        Update the model with PUT requests
        """
        put_url = reverse("api:v1:savingcategory", kwargs={"pk": c_id})
        return self.client.put(put_url, data, format="json")

    def delete_savingcategories(self, c_id):
        delete_url = resolve_url("api:v1:savingcategory", c_id)
        return self.client.delete(delete_url, format="json")

    def create_budget(
        self, category_id=None, savingcategory_id=None, month=None, data=None
    ):
        if data is None:
            data = {
                "year": random.randint(1970, 2050),
                # Amount should generally be more than allocated_amount
                # (unless a user overbudegts for it)
                "amount": round(random.uniform(43, 500), 3),
                "allocated_amount": round(random.uniform(0, 42), 3),
            }
            # NOTE: -1 simply does not set the 'month' key in the POST body
            # that's because 'month': None is a legal value, and we want to
            # distinguish between them
            if month != -1:
                data["month"] = month

            if category_id:
                data["category"] = category_id
                del data["allocated_amount"]
            else:
                data["savingcategory"] = savingcategory_id

        url = reverse("api:v1:budgets")
        res = self.client.post(url, data, format="json")
        return (res, data)

    def create_budget_from_data(self, data):
        create_url = reverse("api:v1:budgets")
        return self.client.post(create_url, data, format="json")

    def update_budget(self, b_id, data):
        update_url = reverse("api:v1:budget", kwargs={"pk": b_id})
        return self.client.patch(update_url, data, format="json")

    def create_spending_limit(self, c_id):
        create_url = reverse("api:v1:spending_limits")
        data = {
            "category": c_id,
            "amount": random.randint(1, 90000),
            "currency": random.choice(["USD", "EUR", "JPY"]),
            "days": random.randint(1, 28),
            "text": BaseTest.generate_string(20),
        }
        return (self.client.post(create_url, data, format="json"), data)

    def create_spending_limit_from_data(self, data):
        create_url = reverse("api:v1:spending_limits")
        return self.client.post(create_url, data, format="json")

    # Update the model with PUT requests
    def replace_all_spendinglimit_fields(self, income_id, data):
        """
        Update the model with PUT requests
        """
        put_url = resolve_url("api:v1:spending_limit", income_id)
        return (self.client.put(put_url, data, format="json"), income_id)
