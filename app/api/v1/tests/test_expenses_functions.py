from django.contrib.auth import get_user_model
from django.urls import reverse
from budgets.tests.base import BaseTest

from budgets.models import Account
from api.v1.tests.test_base import BaseApiTest


class TestMonthlyBalancesAndAssetsView(BaseApiTest):
    """
    Test Expenses model related v1 rest api behaviour
    """

    def setUp(self):
        self.user = get_user_model().objects.create_user(
            username=BaseTest.generate_string(20),
            password=BaseTest.generate_string(20),
        )
        self.client.force_authenticate(user=self.user)

        # Create test account
        self.account = Account.objects.create(
            text="Test Broker Account",
            credit_entity="Broker",
            created_by=self.user,
        )

    def test_assets_only_no_monthly_balances(self):
        """
        Test that the view returns data when user has only assets
        and no monthly balances
        """
        self.client.force_login(self.user)

        # This user only has assets, no monthly balances
        res, data = self.create_asset(self.account.id)
        self.assertEqual(res.status_code, 201)

        response = self.client.get(
            reverse("api:v1:monthly_balances_and_assets")
        )
        self.assertEqual(response.status_code, 200)
        self.assertNotEqual(
            len(response.data),
            0,
            "Response should not be empty when user has assets but no monthly balances",
        )

        first_item = response.data[0]
        self.assertIn("id", first_item)
        self.assertIn("amount", first_item)
        self.assertIn("category_id", first_item)
        self.assertIn("category_text", first_item)
        self.assertTrue(first_item["is_investing"])
        self.assertEqual(first_item["category_text"], "Test Broker Account")
        self.assertEqual(first_item["category_id"], self.account.id)
        self.assertEqual(first_item["amount"], data["current_local_value"])

    def test_no_assets_no_monthly_balances(self):
        """
        Test that the view handles the case when user has no data at all
        """
        self.client.force_login(self.user)

        # This user has no assets, no monthly balances
        response = self.client.get(
            reverse("api:v1:monthly_balances_and_assets")
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(
            len(response.data),
            0,
            "Response should be empty when user has no assets and no monthly balances",
        )
