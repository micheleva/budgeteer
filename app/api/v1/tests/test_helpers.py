# Copyright: (c) 2021, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from datetime import datetime
from datetime import date

import logging

from unittest import skip
from unittest import TestCase

from dateutil.relativedelta import relativedelta
from django.contrib.auth.models import User
from django.shortcuts import resolve_url
from django.urls import reverse
from rest_framework import status

from api.v1.views.helpers import _positive_int
from api.v1.views.helpers import get_days_in_year
from api.v1.views.helpers import calculate_daily_stats

from api.v1.views.views import assets_monthly_combined
from api.v1.views.views import get_assets_formatted_dates


from api.v1.tests.test_base import BaseApiTest

import budgets.models as m
from budgets.tests.base import BaseTest
from budgets.views_utils import current_month_boundaries


logger = logging.getLogger(__name__)


# FIXME: if this month has no monthly balance we should at least display the last entry we do have
# for each category or not? Unify the behaviour of assets and monthly balance: assets only grab
# latest while monthly balance has one entry per month... should we grab the most recent month... ?
# sometimes monthly balance goes to 0... what if no entry for that month for a given monthly balance
# account? I believe that 0 is OK, the rest has to be figured out
# see https://stackoverflow.com/questions/6186962/sql-query-to-show-nearest-date/6187063
def _monthly_balances_to_assets(filters, user_id, nest_account_object=True):
    class MimicAsset(dict):
        """
        As the serializer expects a class, we mimic the Asset model
        """

        def __init__(self, *args, **kwargs):
            super(MimicAsset, self).__init__(*args, **kwargs)

        def pop(self, key):
            return super(MimicAsset, self).pop(key, None)

        def __getattr__(self, key):
            return self.get(key, None)

        def __setattr__(self, key, value):
            self[key] = value

        def __delattr__(self, key):
            if key in self:
                del self[key]

        def __setitem__(self, key, value):
            super(MimicAsset, self).__setitem__(key, value)

    # NOTE: filters already contains the user.id and the date to filter for
    m_b = (
        m.MonthlyBalance.objects.select_related("category")
        .filter(**filters)
        .order_by("id")
    )
    if not m_b.exists():
        return []

    if m.Asset.objects.count() > 0:
        highest_asset_id = m.Asset.objects.latest("id").id + 1
    else:
        # Let's leave 1000 id of buffer for possible concurrent entries from
        # other users
        highest_asset_id = 1000

    if m.Account.objects.count() > 0:
        highest_account_id = m.Account.objects.latest("id").id + 1
    else:
        # Let's leave 1000 id of bufffer for possible concurrent entries from
        #  other users
        highest_account_id = 1000

    m_b_as_assets = []

    for index, _ in enumerate(m_b, start=0):
        my_asset = MimicAsset()
        my_asset.id = highest_asset_id + index  # pylint: disable=W0201; # noqa
        my_asset.amount = m_b[index].amount  # pylint: disable=W0201; # noqa

        # We use a non-existent account id on purpose, to avoid collision
        # with existing Account ids
        if nest_account_object:
            my_asset["account"] = {}
            my_asset["account"]["id"] = highest_account_id + m_b[index].id
            my_asset["account"]["text"] = m_b[index].category.text
        else:
            my_asset["account_id"] = highest_account_id + m_b[index].id
            my_asset["account_text"] = m_b[index].category.text

        my_asset.text = "Cash(USD)"  # pylint: disable=W0201; # noqa
        my_asset.foreign_currency = "USD"  # pylint: disable=W0201; # noqa

        my_asset.current_foreign_value = m_b[
            index
        ].amount  # pylint: disable=W0201; # noqa
        my_asset.current_local_value = int(
            m_b[index].amount
        )  # pylint: disable=W0201; # noqa

        my_asset.bought_price_foreign = m_b[
            index
        ].amount  # pylint: disable=W0201; # noqa
        my_asset.bought_price_local = int(
            m_b[index].amount
        )  # pylint: disable=W0201; # noqa

        my_asset.net_loss_foreign = 0  # pylint: disable=W0201; # noqa
        my_asset.net_loss_local = 0  # pylint: disable=W0201; # noqa

        if not m_b[index].category.is_foreign_currency:
            my_asset.text = "Cash(JPY)"  # pylint: disable=W0201; # noqa
            my_asset.foreign_currency = "JPY"  # pylint: disable=W0201; # noqa
            my_asset.current_foreign_value = 0  # pylint: disable=W0201; # noqa
            my_asset.bought_price_foreign = 0  # pylint: disable=W0201; # noqa

        my_asset.record_date = m_b[index].date
        m_b_as_assets.append(my_asset)

    return m_b_as_assets


class HelperTest(BaseApiTest):
    """
    Test related to utilities funcs
    """

    def test_get_assets_formatted_dates(self):
        # For users with no assets, this func will return 0 dates
        user = User.objects.create_user(
            username=BaseTest.generate_string(20),
            password=BaseTest.generate_string(20),
        )
        res = get_assets_formatted_dates(user.id)
        self.assertEqual(len(res), 0)

        # For users with only 1 asset on 1 single day, this func will
        # return 1 date
        # TODO write me

        # For users with only multiple assets on 1 single day, this func will
        # eturn 1 date
        # TODO write me

        # For users with multiple assets on multiple days, this func will
        # return n date where n is the number of DIFFERENT days in which the
        # user has assets recorded for

        # TODO write me
        # TODO make the dates span over 5 years, and create at
        # least 20 assets for 500 days

    def test_monthly_balances_to_assets_returns_empty_if_no_monthly_balances(
        self,
    ):
        user = User.objects.create_user(
            username=BaseTest.generate_string(20),
            password=BaseTest.generate_string(20),
        )
        filters = {"created_by": user, "date": current_month_boundaries()[0]}
        mb_to_as = _monthly_balances_to_assets(filters, user)
        self.assertEqual(len(mb_to_as), 0)

    def test_monthly_balances_to_assets_returns_correct_data_if_monthly_balances_exists(
        self,
    ):
        self.user = User.objects.create_user(
            username=BaseTest.generate_string(20),
            password=BaseTest.generate_string(20),
        )
        self.client.force_login(self.user)

        # create 3 monthly budget categories
        cats = {}
        months = {}
        for i in range(4):
            res, _ = self.create_monthly_balance_category()
            cats[res.data["id"]] = res.data
            # Make sure we have some foreign currency accounts as well
            if i % 2 == 0:
                data = {"is_foreign_currency": True}
                res = self.edit_monthly_balance_category(res.data["id"], data)
                self.assertEqual(res.status_code, status.HTTP_200_OK)
                cats[res.data["id"]] = res.data

        start_date = datetime(2020, 1, 1)
        end_date = start_date + relativedelta(months=3)
        current_date = start_date

        # Create 1 monthly balance per category, over 3 months
        while current_date <= end_date:
            current_date = current_date.replace(day=1)
            for _, val in cats.items():
                res, data = self.create_monthly_balance(
                    val["id"], target_date=current_date.strftime("%Y-%m-%d")
                )
                self.assertEqual(res.status_code, status.HTTP_201_CREATED)
                if current_date.strftime("%Y-%m-%d") in months.keys():
                    months[current_date.strftime("%Y-%m-%d")] += res.data[
                        "amount"
                    ]
                else:
                    months[current_date.strftime("%Y-%m-%d")] = res.data[
                        "amount"
                    ]
            current_date += relativedelta(months=1)

        res = self.client.get(
            reverse("api:v1:monthly_balances"), format="json"
        )
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data["count"], 16)
        self.assertEqual(len(res.data["results"]), 16)

        for key, val in months.items():
            filters = {"created_by": self.user, "date": key}
            mb_to_as = _monthly_balances_to_assets(filters, self.user)
            self.assertEqual(len(mb_to_as), 4)

    def test_monthly_balances_to_assets_returns_correct_nested_data_if_monthly_balances_exists_and_nested_is_true(
        self,
    ):
        self.user = User.objects.create_user(
            username=BaseTest.generate_string(20),
            password=BaseTest.generate_string(20),
        )
        self.client.force_login(self.user)

        # NOTE: increase coverage, and check that the Monthly Balances (converted to assets) fake IDs
        #       and the related fake account IDs are not colliding with any other object
        acc_r, data = self.create_account()
        self.assertEqual(acc_r.status_code, status.HTTP_201_CREATED)
        for i in range(3):
            res, data = self.create_asset(
                acc_r.data["id"], date_in_the_past=False
            )
            self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        # create 3 categories
        cats = {}
        months = {}
        for i in range(4):
            res, _ = self.create_monthly_balance_category()
            cats[res.data["id"]] = res.data
            # Make sure we have some foreign currency accounts as well
            if i % 2 == 0:
                data = {"is_foreign_currency": True}
                res = self.edit_monthly_balance_category(res.data["id"], data)
                self.assertEqual(res.status_code, status.HTTP_200_OK)
                cats[res.data["id"]] = res.data

        start_date = datetime(2020, 1, 1)
        end_date = start_date + relativedelta(months=3)
        current_date = start_date

        # Create 1 monthly balance per category, over 3 months
        while current_date <= end_date:
            current_date = current_date.replace(day=1)
            for _, val in cats.items():
                res, data = self.create_monthly_balance(
                    val["id"], target_date=current_date.strftime("%Y-%m-%d")
                )
                self.assertEqual(res.status_code, status.HTTP_201_CREATED)
                if (
                    current_date.strftime("%Y-%m-%d") in months.keys()
                ):  # pylint: disable=C0201; # noqa
                    months[current_date.strftime("%Y-%m-%d")] += res.data[
                        "amount"
                    ]
                else:
                    months[current_date.strftime("%Y-%m-%d")] = res.data[
                        "amount"
                    ]
            current_date += relativedelta(months=1)

        res = self.client.get(
            reverse("api:v1:monthly_balances"), format="json"
        )
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data["count"], 16)
        self.assertEqual(len(res.data["results"]), 16)

        for key, val in months.items():
            filters = {"created_by": self.user, "date": key}
            mb_to_as = _monthly_balances_to_assets(
                filters, self.user, nest_account_object=True
            )
            self.assertEqual(len(mb_to_as), 4)
            for item in mb_to_as:
                self.assertNotIn("account_text", item)
                self.assertNotIn("account_id", item)
                self.assertIn("account", item)
                self.assertIn("id", item["account"])
                self.assertIn("text", item["account"])

    def test_leap_year_divisible_by_400(self):
        # Test for a leap year divisible by 400
        result = get_days_in_year(1600)
        self.assertEqual(result, 366)

    def test_leap_year_divisible_by_4_not_by_100(self):
        # Test for a leap year divisible by 4 but not by 100
        result = get_days_in_year(2024)
        self.assertEqual(result, 366)

    def test_regular_year_not_divisible_by_4(self):
        # Test for a year that is not divisible by 4
        result = get_days_in_year(2023)
        self.assertEqual(result, 365)

    def test_non_leap_year_divisible_by_100_not_by_400(self):
        # Test for a year divisible by 100 but not by 400
        result = get_days_in_year(1900)
        self.assertEqual(result, 365)

    def test_non_leap_year_divisible_by_4_and_by_100(self):
        # Test for a year divisible by both 4 and 100 but not by 400
        result = get_days_in_year(1800)
        self.assertEqual(result, 365)

    def test_positive_integer(self):
        """
        Test case for the _positive_int function with various scenarios.
        """
        # Normal usage
        int_str = "42"
        strict = True
        cutoff = None
        try:
            res = _positive_int(int_str, strict, cutoff)
            self.assertEqual(res, 42)
        except ValueError as e:
            self.fail(e)

        # using zero with non strict, will be OK
        int_str = "0"
        strict = False
        cutoff = None
        try:
            res = _positive_int(int_str, strict, cutoff)
            self.assertEqual(res, 0)
        except ValueError as e:
            self.fail(e)

        # using zero with strict, will throw a ValueError exception
        int_str = "0"
        strict = True
        cutoff = None
        with self.assertRaises(ValueError):
            res = _positive_int(int_str, strict, cutoff)
            self.assertEqual(res, 0)

        # using negtive int, even without strict, will throw
        # a ValueError exception
        int_str = "-1"
        strict = None
        cutoff = None
        with self.assertRaises(ValueError):
            res = _positive_int(int_str, strict, cutoff)

        # using negtive int, with strict will throw a ValueError exception
        int_str = "-1"
        strict = True
        cutoff = None
        with self.assertRaises(ValueError):
            res = _positive_int(int_str, strict, cutoff)

        # using None, with strict will throw a TypeError exception
        int_str = None
        strict = True
        cutoff = None
        with self.assertRaises(TypeError):
            res = _positive_int(int_str, strict, cutoff)

        # using string, with strict will throw a ValueError exception
        int_str = "i am surely not an int"
        strict = True
        cutoff = None
        with self.assertRaises(ValueError):
            res = _positive_int(int_str, strict, cutoff)

        # cutoff, even with strict false, will return the smallest number
        int_str = "1337"
        strict = False
        cutoff = 42
        try:
            res = _positive_int(int_str, strict, cutoff)
            self.assertEqual(res, 42)
        except ValueError as e:
            self.fail(e)

        # cutoff will return the smallest number
        int_str = "1337"
        strict = True
        cutoff = 42
        try:
            res = _positive_int(int_str, strict, cutoff)
            self.assertEqual(res, 42)
        except ValueError as e:
            self.fail(e)

        # cutoff will throw a TypeError if cutoff is a string
        int_str = "1337"
        strict = False
        cutoff = "i am not an int"
        with self.assertRaises(TypeError):
            _ = _positive_int(int_str, strict, cutoff)

        # cutoff will throw a TypeError if cutoff is a string
        int_str = "1337"
        strict = True
        cutoff = "i am not an int"
        with self.assertRaises(TypeError):
            _ = _positive_int(int_str, strict, cutoff)

        # cutoff will not be used if is None
        int_str = "42"
        strict = False
        cutoff = None
        try:
            _ = _positive_int(int_str, strict, cutoff)
            self.assertEqual(res, 42)
        except ValueError as e:
            self.fail(e)

        # cutoff will not be used if is None even with strict mode on
        int_str = "42"
        strict = True
        cutoff = None
        try:
            _ = _positive_int(int_str, strict, cutoff)
            self.assertEqual(res, 42)
        except ValueError as e:
            self.fail(e)

    def test_regression_266_assets_monthly_combined_return_one_item_with_one_account(
        self,
    ):
        self.user = User.objects.create_user(
            username=BaseTest.generate_string(
                20
            ),  # pylint: disable=W0201; # noqa
            password=BaseTest.generate_string(20),
        )
        self.client.force_login(self.user)
        res, acc1 = self.create_account()
        acc1_id = res.data["id"]
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            m.Account.objects.filter(created_by=self.user.id).count(), 1  # pylint: disable=E1101; # noqa
        )
        acc = m.Account.objects.filter(  # pylint: disable=E1101; # noqa
            created_by=self.user.id
        ).first()
        # fmt: on
        self.assertEqual(acc.text, acc1["text"])
        self.assertEqual(acc.credit_entity, acc1["credit_entity"])
        self.assertEqual(acc.is_foreign_currency, acc1["is_foreign_currency"])

        for _ in range(3):
            res, _ = self.create_asset(acc1_id, date_in_the_past=False)
            self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        # fmt: off
        self.assertEqual(
            m.Asset.objects.filter(created_by=self.user).count(), 3  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        # create 10 different text assets, in the same acccount, for the same month
        # and confirm that they are indeed combined into one aggregate
        ass = assets_monthly_combined(self.user)
        self.assertEqual(len(ass), 1)

    def test_regression_266_assets_monthly_combined_return_one_item_per_account_with_multiple_accounts(
        self,
    ):
        self.user = User.objects.create_user(
            username=BaseTest.generate_string(
                20
            ),  # pylint: disable=W0201; # noqa
            password=BaseTest.generate_string(20),
        )
        self.client.force_login(self.user)
        accounts = []
        for _ in range(3):
            res, _ = self.create_account()
            self.assertEqual(res.status_code, status.HTTP_201_CREATED)
            acc_id = res.data["id"]
            accounts.append(acc_id)
            # Create multiple assets for this account
            for _ in range(5):
                res, _ = self.create_asset(acc_id)
                self.assertEqual(res.status_code, status.HTTP_201_CREATED)

        # create 10 different text assets for the same month, and confirm that we get only
        #  one summary per account
        ass = assets_monthly_combined(self.user)
        self.assertEqual(len(ass), 3)

    def test_monthly_balances_and_assets_returns_no_data_if_no_monthly_balances_nor_assets(
        self,
    ):
        self.user = User.objects.create_user(
            username=BaseTest.generate_string(
                20
            ),  # pylint: disable=W0201; # noqa
            password=BaseTest.generate_string(20),
        )
        self.client.force_login(self.user)

        get_d = resolve_url("api:v1:monthly_balances_and_assets")
        res = self.client.get(get_d, format="json")
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(len(res.data), 0)

    def test_monthly_balances_and_assets_returns_data_if_there_are_monthly_balances_but_no_assets(
        self,
    ):
        self.user = User.objects.create_user(
            username=BaseTest.generate_string(
                20
            ),  # pylint: disable=W0201; # noqa
            password=BaseTest.generate_string(20),
        )
        self.client.force_login(self.user)

        # create 3 monthly budget categories
        cats = {}
        months = {}
        for i in range(4):
            res, _ = self.create_monthly_balance_category()
            cats[res.data["id"]] = res.data
            # Make sure we have some foreign currency accounts as well
            if i % 2 == 0:
                data = {"is_foreign_currency": True}
                res = self.edit_monthly_balance_category(res.data["id"], data)
                self.assertEqual(res.status_code, status.HTTP_200_OK)
                cats[res.data["id"]] = res.data

        start_date = datetime(2020, 1, 1)
        end_date = start_date + relativedelta(months=3)
        current_date = start_date

        # Create 1 monthly balance per category, over 3 months
        while current_date <= end_date:
            current_date = current_date.replace(day=1)
            for _, val in cats.items():
                res, data = self.create_monthly_balance(
                    val["id"], target_date=current_date.strftime("%Y-%m-%d")
                )
                self.assertEqual(res.status_code, status.HTTP_201_CREATED)
                if (
                    current_date.strftime("%Y-%m-%d") in months.keys()
                ):  # pylint: disable=C0201; # noqa
                    months[current_date.strftime("%Y-%m-%d")] += res.data[
                        "amount"
                    ]
                else:
                    months[current_date.strftime("%Y-%m-%d")] = res.data[
                        "amount"
                    ]
            current_date += relativedelta(months=1)

        get_d = resolve_url("api:v1:monthly_balances_and_assets")
        res = self.client.get(get_d, format="json")
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(len(res.data), 16)
        for item in res.data:
            self.assertIn("id", item)
            self.assertIn("amount", item)
            self.assertIn("category_id", item)
            self.assertIn("category_text", item)
            self.assertIn("category_is_foreign_currency", item)
            self.assertIn("date", item)

    def test_monthly_balances_and_assets_returns_correct_number_of_results_if_there_are_monthly_balances_and_assets(
        self,
    ):
        self.user = User.objects.create_user(
            username=BaseTest.generate_string(
                20
            ),  # pylint: disable=W0201; # noqa
            password=BaseTest.generate_string(20),
        )
        self.client.force_login(self.user)

        # create 3 monthly budget categories
        cats = {}
        months = {}
        for i in range(4):
            res, _ = self.create_monthly_balance_category()
            cats[res.data["id"]] = res.data
            # Make sure we have some foreign currency accounts as well
            if i % 2 == 0:
                data = {"is_foreign_currency": True}
                res = self.edit_monthly_balance_category(res.data["id"], data)
                self.assertEqual(res.status_code, status.HTTP_200_OK)
                cats[res.data["id"]] = res.data

        start_date = datetime(2020, 1, 1)
        end_date = start_date + relativedelta(months=3)
        current_date = start_date

        res, _ = self.create_account()
        acc_id = res.data["id"]
        # Create 1 monthly balance per category, over 3 months
        while current_date <= end_date:
            current_date = current_date.replace(day=1)
            _, _ = self.create_asset(
                acc_id,
                date_in_the_past=False,
                target_date=current_date.strftime("%Y-%m-%d"),
            )
            for _, val in cats.items():
                res, data = self.create_monthly_balance(
                    val["id"], target_date=current_date.strftime("%Y-%m-%d")
                )
                self.assertEqual(res.status_code, status.HTTP_201_CREATED)
                if (
                    current_date.strftime("%Y-%m-%d") in months.keys()
                ):  # pylint: disable=C0201; # noqa
                    months[current_date.strftime("%Y-%m-%d")] += res.data[
                        "amount"
                    ]
                else:
                    months[current_date.strftime("%Y-%m-%d")] = res.data[
                        "amount"
                    ]
            current_date += relativedelta(months=1)

        get_d = resolve_url("api:v1:monthly_balances_and_assets")
        res = self.client.get(get_d, format="json")
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        # 4 accounts * 4 months = 16 monthly balances + 1 asset * 4 months = 4 assets = 16 + 4 = 20
        self.assertEqual(len(res.data), (16 + 4))
        for item in res.data:
            self.assertIn("id", item)
            self.assertIn("amount", item)
            self.assertIn("category_id", item)
            self.assertIn("category_text", item)
            self.assertIn("category_is_foreign_currency", item)
            self.assertIn("date", item)

    def test_custom_pagination_page_size_behaviour(self):
        """
        Test the behavior of custom pagination page size.

        This method tests the behavior of the custom pagination page size feature in the API.
        - verify the default page size is 500
        - verify a custom page size n (lesser than 500) is returned, if page=n is passed
        - verify the whole data is returned if huge_page=yes is passed
        """
        user = User.objects.create_user(
            username=BaseTest.generate_string(20),
            password=BaseTest.generate_string(20),
        )
        self.client.force_login(user)

        # Create some test data
        self.create_bulk_expenses(user)

        url = resolve_url("api:v1:expenses")
        params = {"format": "json"}
        res = self.client.get(url, params)

        # Get the default page size
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(len(res.data["results"]), 500)
        self.assertEqual(res.data["count"], 5000)

        # Request exactly the 42 items
        params = {"format": "json", "page": 42}
        res = self.client.get(url, params)

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(len(res.data["results"]), 42)
        self.assertEqual(res.data["count"], 5000)

        # Request all the data using the huge_page param
        params = {"huge_page": "yes", "format": "json"}
        res = self.client.get(url, params)

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(len(res.data["results"]), 5000)
        self.assertEqual(res.data["count"], 5000)


class TestCalculateDailyStats(TestCase):
    def setUp(self):
        # Set up some test data
        self.today = date(2025, 1, 16)
        self.current_year = 2025
        self.oldest_dividend_date = date(2023, 7, 1)

    def test_empty_queryset(self):
        result = calculate_daily_stats(
            [], self.oldest_dividend_date, self.current_year, self.today
        )
        self.assertEqual(result, [])

    def test_single_year_data(self):
        queryset = [{"year": 2025, "local_total": 1000}]
        result = calculate_daily_stats(
            queryset, self.oldest_dividend_date, self.current_year, self.today
        )
        self.assertEqual(len(result), 2)  # One for the year, one for the total
        self.assertEqual(result[0]["year"], 2025)
        self.assertEqual(result[0]["local_total"], 1000)
        self.assertEqual(result[0]["days"], 15)  # 15 days from Jan 1 to Jan 16
        self.assertAlmostEqual(
            result[0]["local_per_day"], 66.67, places=2
        )  # 1000 / 15 ≈ 66.67

    def test_multiple_years_data(self):
        queryset = [
            {"year": 2023, "local_total": 500},
            {"year": 2024, "local_total": 1000},
            {"year": 2025, "local_total": 250},
        ]
        result = calculate_daily_stats(
            queryset, self.oldest_dividend_date, self.current_year, self.today
        )
        self.assertEqual(len(result), 4)  # Three years plus total
        self.assertEqual(result[0]["year"], 2023)
        self.assertEqual(result[1]["year"], 2024)
        self.assertEqual(result[2]["year"], 2025)
        self.assertEqual(result[3]["year"], -1)  # Total

    def test_oldest_dividend_year(self):
        queryset = [{"year": 2023, "local_total": 500}]
        result = calculate_daily_stats(
            queryset, self.oldest_dividend_date, self.current_year, self.today
        )
        self.assertEqual(result[0]["days"], 184)  # July 1 to Dec 31 (184 days)

    def test_leap_year(self):
        leap_year = 2024
        queryset = [{"year": leap_year, "local_total": 1000}]
        result = calculate_daily_stats(
            queryset, self.oldest_dividend_date, self.current_year, self.today
        )
        self.assertEqual(result[0]["days"], 366)  # 2024 is a leap year

    def test_total_calculation(self):
        queryset = [
            {"year": 2023, "local_total": 500},
            {"year": 2024, "local_total": 1000},
            {"year": 2025, "local_total": 250},
        ]
        result = calculate_daily_stats(
            queryset, self.oldest_dividend_date, self.current_year, self.today
        )
        total = result[-1]
        self.assertEqual(total["year"], -1)
        self.assertEqual(total["local_total"], 1750)
        self.assertEqual(
            total["days"], 184 + 366 + 15
        )  # 2023 (partial) + 2024 (leap) + 2025 (partial)
        # 184 days in 2023 (Jul 1 to Dec 31), 366 days in 2024 (leap year), 15 days in 2025 (Jan 1 to Jan 15)

    def test_zero_days_edge_case(self):
        # Test for the first day of the year
        first_day = date(2025, 1, 1)
        queryset = [{"year": 2025, "local_total": 100}]
        result = calculate_daily_stats(
            queryset, self.oldest_dividend_date, self.current_year, first_day
        )
        self.assertEqual(result[0]["days"], 0)
        self.assertEqual(result[0]["local_per_day"], 0)
