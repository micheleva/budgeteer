# Copyright: (c) 2021, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from datetime import date
from datetime import datetime
from datetime import timedelta
import logging

import random
from unittest.mock import patch
from unittest import skip
from urllib.parse import urlencode

from django.contrib.auth.models import User
from django.db.utils import IntegrityError
from django.urls import reverse
from rest_framework import status
from api.v1.views.income_views import IncomeDetail
from api.v1.views.income_views import IncomeList
from api.v1.tests.test_base import BaseApiTest

import budgets.models as m
from budgets.tests.base import BaseTest

logger = logging.getLogger(__name__)


class IncomeTest(BaseApiTest):
    """
    Test Income model related v1 rest api behaviour
    """

    def setUp(self):  # pylint: disable=C0103; # noqa
        # Instrument the unit tests to fail if they run for too long
        super().setUp()
        self.user = User.objects.create_user(
            username=BaseTest.generate_string(20),
            password=BaseTest.generate_string(20),
        )
        self.user2 = User.objects.create_user(
            username=BaseTest.generate_string(20),
            password=BaseTest.generate_string(20),
        )

    def test_can_create_incomes(self):
        """
        Ensure we can create a new income object
        """
        self.client.force_login(self.user)
        category_response, _ = self.create_income_category()
        income_response, inc_data = self.create_income(
            category_response.data["id"]
        )

        self.assertEqual(income_response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(
            income_response.data["category"], category_response.data["id"]
        )
        self.assertEqual(income_response.data["note"], inc_data["note"])
        self.assertEqual(income_response.data["date"], inc_data["date"])
        # fmt: off
        self.assertEqual(
            m.Income.objects.first().created_by, self.user  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        expenses_list_url = reverse("api:v1:incomes")
        response = self.client.get(expenses_list_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data["results"][0]["category_text"],
            category_response.data["text"],
        )
        self.assertEqual(response.data["results"][0]["note"], inc_data["note"])
        self.assertEqual(response.data["results"][0]["date"], inc_data["date"])
        self.assertEqual(response.data["count"], 1)

        if "errors" in income_response.data:
            self.fail(
                "Income creation response's body included at least one error!"
            )

    def test_can_not_create_incomes_for_other_users(self):
        """
        Ensure we can NOT create a new income object linked to another user
        # by posting other users' ID in the payload
        """
        self.client.force_login(self.user)
        # Create a category for the initial user
        init_cat_res, _ = self.create_income_category()
        init_cat_id = init_cat_res.data["id"]

        # Another user tries to create an Expense object and have it linked to
        # the initial user's ID
        self.client.force_login(self.user2)

        # Try to inject first user ownserhip using a category belonging to the
        # intial user: the income creation should fail hard
        create_url = reverse("api:v1:incomes")
        amount = random.randint(1, 90000)
        note = BaseTest.generate_string(20)
        record_date = date.today().isoformat()

        data = {
            "category": init_cat_id,
            "amount": amount,
            "note": note,
            "date": record_date,
            "created_by": self.user.id,
        }
        res = self.client.post(create_url, data, format="json")

        try:
            # fmt: off
            orm_exp = m.Income.objects.get(  # pylint: disable=E1101; # noqa
                created_by=self.user2.id
            )
            # fmt: on
            self.assertEqual(res.status_code, status.HTTP_201_CREATED)
            logger.critical(
                (
                    "ERROR! The income ownership logic on creation has a bug! "
                    "Posting 'created_by' allows injections in other accounts."
                    " FIX ME!"
                )
            )
        # fmt: off
        except m.Income.DoesNotExist:  # pylint: disable=E1101; # noqa
            self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
            self.assertEqual(len(res.data["category"]), 1)
            err_msg = f'Invalid pk "{init_cat_id}" - object does not exist.'
            self.assertEqual(res.data["category"][0], err_msg)
            self.assertEqual(res.data["category"][0].code, "invalid")
        # fmt: on
        # Try to inject first user ownserhip using a category belonging to the
        # other user: creation should suceed
        # However the income.created_by should be self.user2.id
        cat_res, _ = self.create_income_category()
        cat_id = cat_res.data["id"]

        create_url = reverse("api:v1:incomes")
        amount = random.randint(1, 90000)
        note = BaseTest.generate_string(20)
        record_date = date.today().isoformat()

        data = {
            "category": cat_id,
            "amount": amount,
            "note": note,
            "date": record_date,
            "created_by": self.user.id,
        }
        res = self.client.post(create_url, data, format="json")
        # fmt: off
        orm_exp = m.Income.objects.get(  # pylint: disable=E1101; # noqa
            created_by=self.user2.id
        )
        # fmt: on
        self.assertEqual(res.status_code, status.HTTP_201_CREATED)
        self.assertEqual(orm_exp.note, note)
        self.assertEqual(orm_exp.amount, amount)
        self.assertEqual(orm_exp.created_by.id, self.user2.id)

        income_list_url = reverse("api:v1:incomes")
        res = self.client.get(income_list_url, format="json")

        #  The income was correctly linked to the other user account
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data["results"][0]["note"], note)
        self.assertEqual(res.data["results"][0]["amount"], amount)
        self.assertEqual(res.data["count"], 1)

        # The initial user has no incomes, as expected
        self.client.force_login(self.user)

        res = self.client.get(income_list_url, format="json")

        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data["count"], 0)

    def test_can_not_created_malformed_incomes(self):
        self.client.force_login(self.user)
        create_url = reverse("api:v1:incomes")
        amount = random.randint(1, 90000)
        note = BaseTest.generate_string(20)
        record_date = date.today().isoformat()

        # Try without category
        data = {"amount": amount, "note": note, "date": record_date}
        res = self.client.post(create_url, data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(res.data["category"]), 1)
        self.assertEqual(res.data["category"][0], "This field is required.")
        # NOTE: we raise serializers.ValidationError({"category":
        # ['This field is required.']}), hence its code is 'invalid',
        # not 'required'
        self.assertEqual(res.data["category"][0].code, "invalid")

        cat_res, _ = self.create_income_category()
        category_id = cat_res.data["id"]

        # Try with too long text
        long_note = BaseTest.generate_string(151)
        data = {
            "category": category_id,
            "amount": amount,
            "note": long_note,
            "date": record_date,
        }
        res = self.client.post(create_url, data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(res.data["note"]), 1)
        self.assertEqual(
            res.data["note"][0],
            "Ensure this field has no more than 150 characters.",
        )
        self.assertEqual(res.data["note"][0].code, "max_length")

        # Try with no amount
        data = {"category": category_id, "note": note, "date": record_date}
        res = self.client.post(create_url, data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(res.data["amount"]), 1)
        self.assertEqual(res.data["amount"][0], "This field is required.")
        self.assertEqual(res.data["amount"][0].code, "required")

        # Try with negative amount
        data = {
            "category": category_id,
            "amount": -42,
            "note": note,
            "date": record_date,
        }
        res = self.client.post(create_url, data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(res.data["amount"]), 1)
        self.assertEqual(res.data["amount"][0], "-42 should be more than 0.")
        self.assertEqual(res.data["amount"][0].code, "invalid")

        # Try with malfomed date
        data = {
            "category": category_id,
            "amount": amount,
            "note": note,
            "date": "89019-13-32",
        }
        res = self.client.post(create_url, data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(res.data["date"]), 1)
        err_msg = (
            "Date has wrong format. Use one of these formats instead:"
            " YYYY-MM-DD."
        )
        self.assertEqual(res.data["date"][0], err_msg)
        self.assertEqual(res.data["date"][0].code, "invalid")

        # Try with boolean instead of a date
        data = {
            "category": category_id,
            "amount": amount,
            "note": note,
            "date": True,
        }
        res = self.client.post(create_url, data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(res.data["date"]), 1)
        err_msg = (
            "Date has wrong format. Use one of these formats instead:"
            " YYYY-MM-DD."
        )
        self.assertEqual(res.data["date"][0], err_msg)
        self.assertEqual(res.data["date"][0].code, "invalid")

        # Try with a string instead of a date
        data = {
            "category": category_id,
            "amount": amount,
            "note": note,
            "date": "I am surely not a correctly formatte date",
        }
        res = self.client.post(create_url, data, format="json")
        self.assertEqual(res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(len(res.data["date"]), 1)
        err_msg = (
            "Date has wrong format. Use one of these formats instead:"
            " YYYY-MM-DD."
        )
        self.assertEqual(res.data["date"][0], err_msg)
        self.assertEqual(res.data["date"][0].code, "invalid")

        data = {
            "category": category_id,
            "amount": amount,
            "note": note,
            "date": record_date,
        }
        return (self.client.post(create_url, data, format="json"), data)

    # TODO: write me
    @skip
    def test_creating_incomes_with_malformed_date_will_not_throw_error_500(
        self,
    ):
        pass
        # self.fail('write me!')

    def test_get_income_using_malformed_date_will_not_throw_error_500(self):
        self.client.force_login(self.user)

        get_incomes_url = reverse("api:v1:incomes")
        malformed_date = "I am definitely not a valid date value"

        # Try malformed start param
        filtered_url = (
            get_incomes_url + "?" + urlencode({"start": malformed_date})
        )
        response = self.client.get(filtered_url, format="json")
        self.assertNotEqual(response.status_code, 500)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(len(response.data["start"]), 1)
        self.assertEqual(
            response.data["start"][0],
            "Invalid start date format. Use 'YYYY-MM-DD'.",
        )
        self.assertEqual(response.data["start"][0].code, "invalid")

        # Try malformed end param
        filtered_url = (
            get_incomes_url + "?" + urlencode({"end": malformed_date})
        )
        response = self.client.get(filtered_url, format="json")
        self.assertNotEqual(response.status_code, 500)
        self.assertEqual(response.status_code, 400)
        self.assertEqual(len(response.data["end"]), 1)
        self.assertEqual(
            response.data["end"][0],
            "Invalid date end format. Use 'YYYY-MM-DD'.",
        )
        self.assertEqual(response.data["end"][0].code, "invalid")

        malformed_page_number = (
            "I am definitely not a page number, but a string!"
        )
        filtered_url = (
            get_incomes_url + "?" + urlencode({"page": malformed_page_number})
        )
        response = self.client.get(filtered_url, format="json")
        self.assertNotEqual(response.status_code, 500)
        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.data["detail"], "Invalid page.")
        self.assertEqual(response.data["detail"].code, "not_found")

    def test_can_not_use_other_people_category_incomes_in_income_creation(
        self,
    ):
        """
        Ensure we can create a new category object, and other users
        can't see it
        """
        self.client.force_login(self.user)

        # Create a category using the wrong field name
        cat_res, _ = self.create_income_category()
        self.create_expense(cat_res.data["id"])

        # Another user CANNOT use that category
        self.client.force_login(self.user2)

        other_expense_response, _ = self.create_income(cat_res.data["id"])

        self.assertEqual(
            other_expense_response.status_code, status.HTTP_400_BAD_REQUEST
        )
        self.assertEqual(len(other_expense_response.data["category"]), 1)
        err_msg = (
            f"Invalid pk \"{cat_res.data['id']}\" - object does not exist."
        )
        self.assertEqual(other_expense_response.data["category"][0], err_msg)
        self.assertEqual(
            other_expense_response.data["category"][0].code, "invalid"
        )

    def test_error_is_returned_if_category_is_missing(self):
        """
        Ensure we get an error if 'category' field is missing from
        the post body
        """
        self.client.force_login(self.user)

        # Create an income category
        cat_res, _ = self.create_income_category()

        create_url = reverse("api:v1:incomes")
        amount = random.randint(1, 90000)
        note = BaseTest.generate_string(20)
        record_date = record_date = date.today().isoformat()

        data = {
            "bad-field": cat_res.data["id"],
            "amount": amount,
            "note": note,
            "date": record_date,
        }
        other_expense_response, _ = (
            self.client.post(create_url, data, format="json"),
            data,
        )

        self.assertEqual(
            other_expense_response.status_code, status.HTTP_400_BAD_REQUEST
        )
        self.assertEqual(len(other_expense_response.data["category"]), 1)
        self.assertEqual(
            other_expense_response.data["category"][0],
            "This field is required.",
        )
        # NOTE: we raise serializers.ValidationError({"category":
        # ['This field is required.']})
        # hence its code is 'invalid', not 'required'
        self.assertEqual(
            other_expense_response.data["category"][0].code, "invalid"
        )

    def test_deleted_income_is_gone(self):
        """
        Ensure we can delete an income
        """
        self.client.force_login(self.user)
        cat_res, _ = self.create_income_category()
        _, _ = self.create_income(cat_res.data["id"])

        # Get the created income's id (create API's response does
        # not contain it, yet)
        incomes_list = reverse("api:v1:incomes")
        response = self.client.get(incomes_list, format="json")
        self.assertEqual(response.data["count"], 1)
        delete_expense_result = self.delete_income(
            response.data["results"][0]["id"]
        )
        self.assertEqual(
            delete_expense_result.status_code, status.HTTP_204_NO_CONTENT
        )

        # If the income was deleted, we should now have 0 expenses left
        incomes_list = reverse("api:v1:incomes")
        response = self.client.get(incomes_list, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 0)

    def test_delete_other_users_incomes_fail_with_404(self):
        self.client.force_login(self.user)
        category_response, _ = self.create_income_category()
        _, _ = self.create_income(category_response.data["id"])

        # Get the created income's id (create API's response does
        # not contain it, yet)
        incomes_list = reverse("api:v1:incomes")
        response = self.client.get(incomes_list, format="json")
        income_id = response.data["results"][0]["id"]

        # The other user CANNOT delete that income
        self.client.force_login(self.user2)

        delete_income_result = self.delete_income(income_id)
        self.assertEqual(
            delete_income_result.status_code, status.HTTP_404_NOT_FOUND
        )

    def test_delete_non_existing_incomes_return_404(self):
        self.client.force_login(self.user)

        # Assure no income exists
        incomes_list = reverse("api:v1:incomes")
        response = self.client.get(incomes_list, format="json")
        self.assertEqual(response.data["count"], 0)

        # Generate a non existing income id, and confirm its deletion fails
        expense_id = random.randint(1, 90000)
        delete_expense_result, _ = self.delete_expense(expense_id)
        self.assertEqual(
            delete_expense_result.status_code, status.HTTP_404_NOT_FOUND
        )

    def test_can_update_all_income_fields_at_once(self):
        self.client.force_login(self.user)
        cat_res, _ = self.create_income_category()
        category_id = cat_res.data["id"]
        _, _ = self.create_income(category_id)

        # Get the created income's id (create API's response does not
        # contain it, yet)
        incomes_list = reverse("api:v1:incomes")
        res = self.client.get(incomes_list, format="json")
        self.assertEqual(res.data["count"], 1)
        income_id = res.data["results"][0]["id"]

        amount = random.randint(1, 90000)
        note = BaseTest.generate_string(20)
        record_date = date.today().isoformat()

        # TODO: we should update cat id as well i.e. create a second
        # income category and replace it
        data = {
            "category": category_id,
            "amount": amount,
            "note": note,
            "date": record_date,
        }
        update_response, _ = self.replace_all_income_fields(income_id, data)
        self.assertEqual(update_response.status_code, status.HTTP_200_OK)

        incomes_list = reverse("api:v1:incomes")
        second_response = self.client.get(incomes_list, format="json")
        # Assure that since we did an update, and nothing was created
        self.assertEqual(second_response.data["count"], 1)
        self.assertEqual(second_response.status_code, status.HTTP_200_OK)
        self.assertEqual(second_response.data["results"][0]["amount"], amount)
        self.assertEqual(second_response.data["results"][0]["note"], note)
        self.assertEqual(
            second_response.data["results"][0]["date"], record_date
        )

    # Update the model with PATCH requests
    def test_can_partially_update_income(self):
        self.client.force_login(self.user)

        cat_res, _ = self.create_income_category()
        category_id = cat_res.data["id"]
        _, _ = self.create_income(category_id)

        # Get the created income's id (create API's response does not
        # contain it, yet)
        incomes_list = reverse("api:v1:incomes")
        response = self.client.get(incomes_list, format="json")

        self.assertEqual(response.data["count"], 1)
        income_id = response.data["results"][0]["id"]

        # Try updating only one field
        # No need to try each field singularly, otherwise we'd
        # be testing django rest frametwork itself
        amount = random.randint(1, 90000)

        data = {"amount": amount}
        update_response, _ = self.partial_update_income(income_id, data)
        self.assertEqual(update_response.status_code, status.HTTP_200_OK)

        incomes_list = reverse("api:v1:incomes")
        fourth_response = self.client.get(incomes_list, format="json")

        # Assure that since we did an update, and nothing was created
        self.assertEqual(fourth_response.data["count"], 1)
        self.assertEqual(fourth_response.status_code, status.HTTP_200_OK)
        self.assertEqual(fourth_response.data["results"][0]["amount"], amount)

    # Update the model with PATCH requests
    def test_can_not_partially_update_income_using_other_user_category(self):
        self.client.force_login(self.user)

        cat_res, _ = self.create_income_category()
        # Another user CANNOT use this category
        category_id = cat_res.data["id"]

        self.client.force_login(self.user2)

        second_category_response, _ = self.create_income_category()
        second_category_id = second_category_response.data["id"]
        _, _ = self.create_income(second_category_id)

        # Get the created expense's id (create API's response does not
        # contain it, yet)
        incomes_list = reverse("api:v1:incomes")
        response = self.client.get(incomes_list, format="json")

        self.assertEqual(response.data["count"], 1)
        income_id = response.data["results"][0]["id"]

        data = {"category": category_id}
        update_res, _ = self.partial_update_income(income_id, data)

        self.assertEqual(update_res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = f'Invalid pk "{category_id}" - object does not exist.'
        self.assertEqual(update_res.data["category"][0], msg)

    def test_can_not_update_income_partially_using_non_existing_category(self):
        self.client.force_login(self.user)
        category_response, _ = self.create_income_category()
        category_id = category_response.data["id"]

        _, _ = self.create_income(category_id)

        # Get the created income's id (create API's response does not
        # contain it, yet)
        incomes_list = reverse("api:v1:incomes")
        response = self.client.get(incomes_list, format="json")

        self.assertEqual(response.data["count"], 1)
        income_id = response.data["results"][0]["id"]

        wrong_cat_id = category_id + random.randint(80000, 90000)
        data = {"category": wrong_cat_id}
        update_res, _ = self.partial_update_income(income_id, data)

        self.assertEqual(update_res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = f'Invalid pk "{wrong_cat_id}" - object does not exist.'
        self.assertEqual(update_res.data["category"][0], msg)
        self.assertEqual(update_res.data["category"][0].code, "does_not_exist")

    # The permission logic check has changed, and if no category is provided
    # then the old category is passed to the validator
    # Can update the model with PUT requests
    def test_can_update_income_entirely_without_category(self):
        self.client.force_login(self.user)
        cat_res, _ = self.create_income_category()
        category_id = cat_res.data["id"]
        _, _ = self.create_income(category_id)

        # Get the created income's id (create API's response does not
        # contain it, yet)
        incomes_list = reverse("api:v1:incomes")
        response = self.client.get(incomes_list, format="json")

        self.assertEqual(response.data["count"], 1)
        expense_id = response.data["results"][0]["id"]

        amount = random.randint(1, 90000)
        note = BaseTest.generate_string(20)
        record_date = date.today().isoformat()

        data = {"amount": amount, "note": note, "date": record_date}
        update_res, _ = self.replace_all_income_fields(expense_id, data)

        self.assertEqual(update_res.status_code, status.HTTP_200_OK)

    # Can update the model with PUT requests
    def test_can_not_update_income_entirely_using_other_user_category(self):
        self.client.force_login(self.user)
        cat_res, _ = self.create_income_category()
        # Another user CANNOT use this category
        category_id = cat_res.data["id"]

        self.client.force_login(self.user2)

        second_cat_res, _ = self.create_income_category()
        second_category_id = second_cat_res.data["id"]
        self.create_income(second_category_id)

        # Get the created income's id (create API's response does not
        # contain it, yet)
        incomes_list = reverse("api:v1:incomes")
        inc_res = self.client.get(incomes_list, format="json")

        self.assertEqual(inc_res.data["count"], 1)
        self.assertEqual(
            inc_res.data["results"][0]["category_id"], second_category_id
        )
        expense_id = inc_res.data["results"][0]["id"]

        amount = random.randint(1, 90000)
        note = BaseTest.generate_string(20)
        record_date = date.today().isoformat()

        data = {
            "category": category_id,
            "amount": amount,
            "note": note,
            "date": record_date,
        }
        update_res, _ = self.replace_all_income_fields(expense_id, data)
        self.assertEqual(update_res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = f'Invalid pk "{category_id}" - object does not exist.'
        self.assertEqual(update_res.data["category"][0], msg)

    # Can update the model with PUT requests
    def test_can_not_update_income_entirely_using_non_existing_category(self):
        self.client.force_login(self.user)
        cat_res, _ = self.create_income_category()
        category_id = cat_res.data["id"]
        self.create_income(category_id)

        # Get the created income's id (create API's response does not
        # contain it, yet)
        incomes_list = reverse("api:v1:incomes")
        inc_res = self.client.get(incomes_list, format="json")

        self.assertEqual(inc_res.data["count"], 1)
        self.assertEqual(
            inc_res.data["results"][0]["category_id"], category_id
        )
        income_id = inc_res.data["results"][0]["id"]

        amount = random.randint(1, 90000)
        note = BaseTest.generate_string(20)
        record_date = date.today().isoformat()

        data = {
            "category": 7549343,
            "amount": amount,
            "note": note,
            "date": record_date,
        }
        update_res, _ = self.replace_all_income_fields(income_id, data)
        self.assertEqual(update_res.status_code, status.HTTP_400_BAD_REQUEST)
        msg = f'Invalid pk "{7549343}" - object does not exist.'
        self.assertEqual(update_res.data["category"][0], msg)

    def test_can_not_update_income_without_category(self):
        self.client.force_login(self.user)
        cat_res, _ = self.create_income_category()
        category_id = cat_res.data["id"]
        self.create_income(category_id)

        # Get the created income's id (create API's response does
        # not contain it, yet)
        incomes_list = reverse("api:v1:incomes")
        inc_res = self.client.get(incomes_list, format="json")

        self.assertEqual(inc_res.data["count"], 1)
        self.assertEqual(
            inc_res.data["results"][0]["category_id"], category_id
        )
        income_id = inc_res.data["results"][0]["id"]

        amount = random.randint(1, 90000)
        note = BaseTest.generate_string(20)
        record_date = date.today().isoformat()

        # Try with None
        data = {
            "category": None,
            "amount": amount,
            "note": note,
            "date": record_date,
        }
        update_res, _ = self.replace_all_income_fields(income_id, data)
        self.assertEqual(update_res.status_code, status.HTTP_400_BAD_REQUEST)
        self.assertEqual(update_res.data["category"][0].code, "invalid")
        self.assertEqual(
            update_res.data["category"][0], "This field is required."
        )

        # Try without Category => perform_update.perform_update() gets
        # the old category if non is passed to prevent to accidentally
        # set it to none
        data = {"amount": amount, "note": note, "date": record_date}
        update_res, _ = self.replace_all_income_fields(income_id, data)
        self.assertEqual(update_res.status_code, status.HTTP_200_OK)
        self.assertEqual(
            inc_res.data["results"][0]["category_id"],
            update_res.data["category"],
        )

    def test_can_filter_incomes_by_income_category_name(self):
        self.client.force_login(self.user)

        category_response, _ = self.create_income_category()
        _, _ = self.create_income(category_response.data["id"])

        category_response2, _ = self.create_income_category()
        _, _ = self.create_income(category_response2.data["id"])

        expenses_list_url = reverse("api:v1:incomes")
        response = self.client.get(expenses_list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 2)
        self.assertEqual(len(response.data["results"]), 2)

        param = urlencode({"category_name": category_response2.data["text"]})
        filtered_url = reverse("api:v1:incomes") + "?" + param
        list_response = self.client.get(filtered_url, format="json")
        self.assertEqual(list_response.status_code, status.HTTP_200_OK)
        self.assertEqual(list_response.data["count"], 1)
        self.assertEqual(len(list_response.data["results"]), 1)
        self.assertEqual(
            list_response.data["results"][0]["category_text"],
            category_response2.data["text"],
        )

    def test_can_filter_incomes_by_income_category_id(self):
        self.client.force_login(self.user)

        category_response, _ = self.create_income_category()
        _, _ = self.create_income(category_response.data["id"])

        category_response2, _ = self.create_income_category()
        _, _ = self.create_income(category_response2.data["id"])

        expenses_list_url = reverse("api:v1:incomes")
        response = self.client.get(expenses_list_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 2)
        self.assertEqual(len(response.data["results"]), 2)

        param = urlencode({"category_id": category_response2.data["id"]})
        filtered_url = reverse("api:v1:incomes") + "?" + param
        list_response = self.client.get(filtered_url, format="json")
        self.assertEqual(list_response.status_code, status.HTTP_200_OK)
        self.assertEqual(list_response.data["count"], 1)
        self.assertEqual(len(list_response.data["results"]), 1)
        self.assertEqual(
            list_response.data["results"][0]["category_id"],
            category_response2.data["id"],
        )

    def test_can_filter_incomes_by_income_note(self):
        self.client.force_login(self.user)

        category_response, _ = self.create_income_category()
        _, inc = self.create_income(category_response.data["id"])

        category_response2, _ = self.create_income_category()
        _, _ = self.create_income(category_response2.data["id"])

        incomes_url = reverse("api:v1:incomes")
        response = self.client.get(incomes_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 2)
        self.assertEqual(len(response.data["results"]), 2)

        filtered_url = (
            reverse("api:v1:incomes") + "?" + urlencode({"note": inc["note"]})
        )
        list_response = self.client.get(filtered_url, format="json")
        self.assertEqual(list_response.status_code, status.HTTP_200_OK)
        self.assertEqual(list_response.data["count"], 1)
        self.assertEqual(len(list_response.data["results"]), 1)
        self.assertEqual(list_response.data["results"][0]["note"], inc["note"])

    def test_can_filter_by_start_and_end(self):
        self.client.force_login(self.user)

        ic, _ = self.create_income_category()
        _, inc = self.create_income(ic.data["id"])
        _, inc2 = self.create_income(ic.data["id"], date_in_the_past=True)
        # The third Income object can not have a completely random date,
        # so we just set it to be before the second Income objects
        # See https://gitlab.com/micheleva/budgeteer/-/issues/5
        ic3_data = {
            "category": ic.data["id"],
            "amount": inc2["amount"],
            "note": inc2["amount"],
            "date": (
                datetime.strptime(inc2["date"], "%Y-%m-%d").date()
                - timedelta(days=2)
            ).isoformat(),
        }
        _, _ = self.create_income_from_data(ic3_data)

        # Without filters we can get all the Income objects
        incomes_url = reverse("api:v1:incomes")
        response = self.client.get(incomes_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 3)
        self.assertEqual(len(response.data["results"]), 3)

        start_param = urlencode({"start": inc2["date"]})
        end_param = urlencode({"end": inc["date"]})
        filtered_url = (
            reverse("api:v1:incomes") + "?" + start_param + "&" + end_param
        )

        lr = self.client.get(filtered_url, format="json")
        self.assertEqual(lr.status_code, status.HTTP_200_OK)
        self.assertEqual(lr.data["count"], 2)
        self.assertEqual(len(lr.data["results"]), 2)

    def test_filtering_by_malformed_start_and_or_end_will_fail(self):
        self.client.force_login(self.user)

        cr, _ = self.create_income_category()
        _, inc = self.create_income(cr.data["id"])

        cr2, _ = self.create_income_category()
        _, inc2 = self.create_income(cr2.data["id"], date_in_the_past=True)

        incomes_url = reverse("api:v1:incomes")
        response = self.client.get(incomes_url, format="json")
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["count"], 2)
        self.assertEqual(len(response.data["results"]), 2)

        start_param = urlencode({"start": inc["date"]})
        end_param = urlencode({"end": inc2["date"]})
        filtered_url = (
            reverse("api:v1:incomes") + "?" + start_param + "&" + end_param
        )

        list_response = self.client.get(filtered_url, format="json")
        self.assertEqual(
            list_response.status_code, status.HTTP_400_BAD_REQUEST
        )
        self.assertEqual(
            list_response.data["non_field_errors"][0].code, "invalid"
        )
        self.assertEqual(
            str(list_response.data["non_field_errors"][0]),
            "Start date must be before end date",
        )

    # WRITE ME
    @skip
    def test_filtering_by_using_start_date_being_after_end_date_will_fail(
        self,
    ):
        pass

    @patch("api.v1.views.income_views.IncomeDetail")
    @patch("budgets.serializers.BasicIncomeSerializer")
    def test_integrity_errors_are_catched_on_create(
        self, mock_cat_serializer, _
    ):
        self.client.force_login(self.user)
        cat_r, cat_data = self.create_income_category()
        self.assertEqual(cat_r.status_code, status.HTTP_201_CREATED)

        income_response, inc_data = self.create_income(cat_r.data["id"])

        self.assertEqual(income_response.status_code, status.HTTP_201_CREATED)
        self.assertEqual(income_response.data["category"], cat_r.data["id"])
        self.assertEqual(income_response.data["note"], inc_data["note"])
        self.assertEqual(income_response.data["date"], inc_data["date"])
        # fmt: off
        self.assertEqual(
            m.Income.objects.first().created_by, self.user  # pylint: disable=E1101; # noqa
        )
        # fmt: on
        expenses_list_url = reverse("api:v1:incomes")
        response = self.client.get(expenses_list_url, format="json")

        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data["results"][0]["category_text"], cat_r.data["text"]
        )
        self.assertEqual(response.data["results"][0]["note"], inc_data["note"])
        self.assertEqual(response.data["results"][0]["date"], inc_data["date"])
        self.assertEqual(response.data["count"], 1)

        # Force the serializer to Throw an Integrity error
        serializer = mock_cat_serializer.return_value
        msg = "I AM A MOCKED INTEGRITY ERROR. I HOPE YOU CATCH ME: ROAAAARR!"
        serializer.save.side_effect = IntegrityError(msg)

        # Create an instance of the class view
        view = IncomeList()
        create_url = reverse("api:v1:incomes")
        # fmt: off
        cat = m.IncomeCategory.objects.filter(  # pylint: disable=E1101; # noqa
            created_by=self.user
        ).get()
        # fmt: on
        ok_data = {
            "category": cat.id,
            "amount": random.randint(1, 90000),
            "note": BaseTest.generate_string(20),
            # Gererate a random date that is between 42 and 365 days ago
            "date": (
                date.today() - timedelta(days=random.randint(42, 365))
            ).isoformat(),
        }

        def mocked_get_data(*args, **_):
            if args[0] == "category":
                return cat
            elif args[0] == "amount":
                return ok_data["amount"]
            elif args[0] == "note":
                return ok_data["note"]
            return ok_data["date"]

        serializer.validated_data.get.side_effect = mocked_get_data

        request = self.client.post(create_url, ok_data, format="json")

        # Set the request user
        view.request = request
        request.user = self.user

        # Confirm that the mocks works, and that ok_data actually thew
        # an exception and confirm it was correctly handled by the backend
        failed_res = view.perform_create(serializer)
        self.assertEqual(
            failed_res.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR
        )
        msg = (
            "Generic error thrown while creating the Income. "
            "Refer the web container logs if you're the admin"
        )
        self.assertEqual(failed_res.data["non_field_errors"][0], msg)

    @patch("api.v1.views.income_views.IncomeList")
    @patch("budgets.serializers.BasicIncomeSerializer")
    def test_integrity_errors_are_catched_on_update(
        self, mock_cat_serializer, _
    ):
        self.client.force_login(self.user)
        cat_r, _ = self.create_income_category()
        self.assertEqual(cat_r.status_code, status.HTTP_201_CREATED)

        inc_r, inc_data = self.create_income(cat_r.data["id"])
        self.assertEqual(inc_r.status_code, status.HTTP_201_CREATED)
        self.assertEqual(inc_r.data["category"], cat_r.data["id"])
        self.assertEqual(inc_r.data["note"], inc_data["note"])
        self.assertEqual(inc_r.data["date"], inc_data["date"])
        # fmt: off
        self.assertEqual(
            m.Income.objects.filter(created_by=self.user)  # pylint: disable=E1101; # noqa
            .first()
            .created_by,
            self.user,
        )
        # fmt: on
        expenses_list_url = reverse("api:v1:incomes")
        res = self.client.get(expenses_list_url, format="json")
        self.assertEqual(res.status_code, status.HTTP_200_OK)
        self.assertEqual(res.data["count"], 1)
        income_id = res.data["results"][0]["id"]

        amount = random.randint(1, 90000)
        note = BaseTest.generate_string(20)
        record_date = (
            date.today() - timedelta(days=random.randint(42, 365))
        ).isoformat()
        cat_r2, _ = self.create_income_category()

        # Confirm update works properly before mocking it
        data = {
            "category": cat_r2.data["id"],
            "amount": amount,
            "note": note,
            "date": record_date,
        }
        update_response, _ = self.replace_all_income_fields(income_id, data)
        self.assertEqual(update_response.status_code, status.HTTP_200_OK)

        incomes_list = reverse("api:v1:incomes")
        second_response = self.client.get(incomes_list, format="json")
        # Assure that since we did an update, and nothing was created
        self.assertEqual(second_response.data["count"], 1)
        self.assertEqual(second_response.status_code, status.HTTP_200_OK)
        self.assertEqual(second_response.data["results"][0]["amount"], amount)
        self.assertEqual(second_response.data["results"][0]["note"], note)
        self.assertEqual(
            second_response.data["results"][0]["date"], record_date
        )

        # Create a mock request
        amount2 = random.randint(1, 90000)
        note2 = BaseTest.generate_string(20)
        record_date2 = (
            date.today() - timedelta(days=random.randint(42, 365))
        ).isoformat()
        cat_r3, _ = self.create_income_category()
        ok_data = {
            "category": cat_r3.data["id"],
            "amount": amount2,
            "note": note2,
            "date": record_date2,
        }
        # fmt: off
        cat_r3_obj = m.IncomeCategory.objects.filter(  # pylint: disable=E1101; # noqa
            created_by=self.user, id=cat_r3.data["id"]
        ).get()
        # fmt: on
        serializer = mock_cat_serializer.return_value
        msg = "I AM A MOCKED INTEGRITY ERROR. I HOPE YOU CATCH ME: ROAAAARR!"
        serializer.save.side_effect = IntegrityError(msg)

        def mocked_get_data(*args, **_):
            if args[0] == "category":
                return cat_r3_obj
            elif args[0] == "amount":
                return ok_data["amount"]
            elif args[0] == "note":
                return ok_data["note"]
            return ok_data["date"]

        serializer.validated_data.get.side_effect = mocked_get_data

        # Create an instance of the class view
        view = IncomeDetail()
        update_url = reverse("api:v1:income", kwargs={"pk": income_id})
        request = self.client.post(update_url, ok_data)

        # Set the request user
        view.request = request
        request.user = self.user

        # Confirm that the mocks works, and that ok_data actually thew
        # an exception and confirm it was correctly handled by the backend
        failed_res = view.perform_update(serializer)
        self.assertEqual(
            failed_res.status_code, status.HTTP_500_INTERNAL_SERVER_ERROR
        )
        msg = (
            "Generic error thrown while updating the Income. "
            "Refer the web container logs if you're the admin"
        )
        self.assertEqual(failed_res.data["non_field_errors"][0], msg)
