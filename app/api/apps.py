# Copyright: (c) 2019, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
# pylint: disable=C0115
from django.apps import AppConfig


class ApiConfig(AppConfig):
    default_auto_field = "django.db.models.AutoField"
    name = "api"
