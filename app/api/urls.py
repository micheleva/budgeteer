# Copyright: (c) 2019, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from django.urls import path, include

app_name = "api"  # pylint: disable=C0103; # noqa
urlpatterns = [
    path("v1/", include("api.v1.urls")),
    path("alpha/", include("api.alpha.urls")),
]
