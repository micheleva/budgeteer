# Copyright: (c) 2024, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
from django.urls import path

from api.alpha import views

app_name = "alpha"  # pylint: disable=C0103; # noqa
urlpatterns = [
    path(
        "asr-enabled",
        views.SpeechToTextEnabledAPIView.as_view(),
        name="speech_to_text_enabled",
    ),
    path(
        "speech-to-text",
        views.SpeechToTextAPIView.as_view(),
        name="speech_to_text",
    ),
    path(
        "speech-to-text/dummy",
        views.DummySpeechToTextAPIView.as_view(),
        name="dummy_speech_to_text_expense",
    ),
    path(
        "exchange-rate",
        views.ExchangeRateAPIView.as_view(),
        name="exchange_rate",
    ),
    path(
        "theme",
        views.ThemeAPIView.as_view(),
        name="theme",
    ),
]
