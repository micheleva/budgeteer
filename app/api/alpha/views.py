# Copyright: (c) 2024, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
# pylint: disable=E1101,W0611,R0901

import logging
import os
import re
import datetime

from django.contrib.auth.mixins import LoginRequiredMixin
from django.conf import settings
from django.core.files.storage import default_storage
from django.core.files.base import ContentFile

from rest_framework import status
from rest_framework.response import Response


from rest_framework.response import Response
from rest_framework.parsers import MultiPartParser
from rest_framework.views import APIView

import json
import wave
import re
from word2number import w2n
from vosk import Model, KaldiRecognizer

import budgets.models as m
from budgets.serializers import CategorySerializer

###############################################################################
# API: Class based
###############################################################################

logger = logging.getLogger(__name__)


def get_asr_enabled():
    """
    Get the value of the ASR_ENABLED environment variable.
    The ASR is enabled by default // Deploying with docker-compose-no-asr.yml sets it to False

    Returns:
        bool: True if ASR is enabled or set to empty, False otherwise
    """
    return settings.ASR_ENABLED


class SpeechToTextEnabledAPIView(LoginRequiredMixin, APIView):
    def get(self, request, format=None):  # pylint: disable=unused-argument
        is_asr_enabled = get_asr_enabled()
        return Response({"enabled": is_asr_enabled})


class SpeechToTextAPIView(LoginRequiredMixin, APIView):
    parser_classes = (MultiPartParser,)

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        if get_asr_enabled():
            self.model = Model(
                lang="en-us"  # TODO: confirm this is actually readning the model from the right place (i.e. the one we download in the vosk-init.sh script)
            )
        else:
            self.model = None

    def infer_from_audio(self, categories, audio_data):
        cat_res = {}
        cat_res["category"] = {}
        cat_res["category"]["inferred_from_first_word"] = None
        cat_res["category"]["inferred_from_existing_categoies"] = None
        cat_res["date"] = None
        cat_res["note"] = None
        cat_res["amount"] = None
        cat_res["text"] = None

        try:
            # Perform speech recognition
            rec = KaldiRecognizer(self.model, audio_data.getframerate())
            rec.SetWords(True)

            while True:
                data = audio_data.readframes(4000)
                if len(data) == 0:
                    break
                if rec.AcceptWaveform(data):
                    pass
            result = json.loads(rec.FinalResult())

            # Capitalize the first letter of the extracted text.
            # The first word is the category, and Categories are like(ly?...): "Food", "Transport", "Entertainment", "Utilities"
            # So we capitalize the first letter to match the category
            result["text"] = result["text"].capitalize()

            # We do not need to return the whole result to the client, as it contains quite a lot of information
            # that the frontend does not need. Returning text field is enough.
            cat_res["text"] = result["text"]
            month_regex = re.search(
                r"(.*?)(date)(.*?)(note)",
                cat_res["text"],
                re.IGNORECASE,
            )

            if (month_regex is not None and len(month_regex.groups())) > 3:
                string_containing_amount = month_regex.groups()[0]
                string_containing_date = month_regex.groups()[2].strip()
            else:
                string_containing_amount = cat_res["text"]
                string_containing_date = cat_res["text"]

            # Handle the case where the audio is empty, or does not have any text that can help to infer the amount
            cat_res["amount"] = self.extract_amount(
                string_containing_amount, cat_res["text"]
            )

            cat_res["date"] = self.extract_date(string_containing_date)
            existing_category = self.is_category_in_text(
                cat_res["text"], categories
            )

            # As per the spec, the first word is the category
            words = cat_res["text"].split(" ")
            if len(words) > 0:
                cat_res["category"]["inferred_from_first_word"] = words[0]
            else:
                cat_res["category"]["inferred_from_first_word"] = None

            # In cat_res["existing_category"] we store the category object (i.e. orm object)
            # Hence we need to use CategorySerializer to serialize it.
            if existing_category:
                cat_res["category"][
                    "inferred_from_existing_categoies"
                ] = CategorySerializer(existing_category).data
            else:
                # Handle the case where the category is not found
                cat_res["category"]["inferred_from_existing_categoies"] = None

            # Extract the note from the text
            cat_res["note"] = self.extract_note(cat_res["text"])

            return cat_res
        except wave.Error as e:
            logger.error(f"Wave module error: {str(e)}")
            return cat_res
        except Exception as e:
            logger.exception(f"Error in speech recognition: {str(e)}")
            return cat_res
        except Exception as e:
            logger.exception(f"Generic Error: {str(e)}")
            return cat_res

    def post(self, request, format=None):
        if not get_asr_enabled():
            return Response(
                {"error": "Speech recognition is disabled"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        audio_file = request.FILES.get("audio")

        if not audio_file:
            return Response(
                {"error": "No audio file provided"},
                status=status.HTTP_400_BAD_REQUEST,
            )

        filters = {"created_by": self.request.user, "is_archived": False}
        categories = m.Category.objects.filter(**filters).order_by("id")
        if not categories.exists():
            return Response(
                {"error": "No categories found"},
                status=status.HTTP_404_NOT_FOUND,
            )

        try:
            # Save the file to disk
            file_name = default_storage.save(
                f"audio_files/{audio_file.name}",
                ContentFile(audio_file.read()),
            )
            file_path = os.path.join(settings.MEDIA_ROOT, file_name)
            audio_file.seek(0)  # Reset file pointer
            audio_data = wave.open(audio_file, "rb")
            res = self.infer_from_audio(categories, audio_data)
            return Response(res)

        except wave.Error as e:
            logger.error(f"Wave module error: {str(e)}")
            return Response(
                {"error": f"Invalid WAV file: {str(e)}"},
                status=status.HTTP_400_BAD_REQUEST,
            )
        except Exception as e:
            logger.exception(f"Error in speech recognition: {str(e)}")
            return Response(
                {"error": f"Speech recognition failed: {str(e)}"},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )

        except Exception as e:
            return Response(
                {"error": f"Speech recognition failed: {str(e)}"},
                status=status.HTTP_500_INTERNAL_SERVER_ERROR,
            )

    def is_category_in_text(self, text, categories):
        for category in categories:
            if category.text.lower() in text.lower():
                return category
        return None

    def extract_amount(self, string_containing_amount, whole_text):
        try:
            return w2n.word_to_num(string_containing_amount)
        except ValueError:
            return None
        except:
            return None

    def extract_note(self, text):
        try:
            # The note field starts after the word "note"
            note_pattern = r"\bnote\b"
            match = re.search(note_pattern, text, re.IGNORECASE)
            if match:
                note_start = match.end()
                return text[note_start:].strip()
            else:
                return ""
        except Exception as e:
            logger.exception(f"Error extracting note: {str(e)}")
            return ""

    def extract_date(self, date_str):
        try:
            date_str = date_str.strip().lower()
            parts = date_str.split()
            if len(parts) > 3:
                return None

            month_str = parts[0]
            # In case the string has the following format: "December twenty first", remove the extra space, and convert it to "December twentyfirst"
            day_str = "".join(parts[1:])
            day_str = day_str.replace(" ", "-")
            date_str = f"{month_str} {day_str}"
        except Exception as e:
            logger.error(f"Error processing date string: {str(e)} {date_str}")
            return None

        # fmt: off
        month_map = {
            "january": 1, "february": 2, "march": 3, "april": 4, "may": 5, "june": 6,
            "july": 7, "august": 8, "september": 9, "october": 10, "november": 11, "december": 12
        }

        def ordinal_to_number(ordinal_word):
            ordinal_map = {
                "first": 1, "second": 2, "third": 3, "fourth": 4, "fifth": 5, "sixth": 6,
                "seventh": 7, "eighth": 8, "ninth": 9, "tenth": 10, "eleventh": 11,
                "twelfth": 12, "thirteenth": 13, "fourteenth": 14, "fifteenth": 15,
                "sixteenth": 16, "seventeenth": 17, "eighteenth": 18, "nineteenth": 19,
                "twentieth": 20, "twentyfirst": 21, "twentysecond": 22, "twentythird": 23,
                "twentyfourth": 24, "twentyfifth": 25, "twentysixth": 26, "twentyseventh": 27,
                "twentyeighth": 28, "twentyninth": 29, "thirtieth": 30, "thirtyfirst": 31,
                # Add the hyphenated versions
                "twenty-first": 21, "twenty-second": 22, "twenty-third": 23, "twenty-fourth": 24,
                "twenty-fifth": 25, "twenty-sixth": 26, "twenty-seventh": 27, "twenty-eighth": 28,
                "twenty-ninth": 29, "thirtieth": 30, "thirty-first": 31,
            }
            return ordinal_map.get(ordinal_word.lower())
            # fmt: on

        try:
            month, day = date_str.split()
            numeric_month = month_map.get(month.lower(), None)
            numeric_day = ordinal_to_number(day)
            if numeric_day is None or month_str is None:
                logger.error(
                    f"Error processing date, numeric_day : {str(numeric_day)}, month_str: {str(month_str)}"
                )
                return None


            current_year = datetime.datetime.now().year

            # Return the date object
            return datetime.date(current_year, numeric_month, numeric_day)
        except ValueError as e:
            # Handle cases where the input is not as expected
            logger.error(
                f"Error: The input format is incorrect. Please provide in 'Month day' format.: {str(e)}"
            )
            return None
        except Exception as e:
            logger.error(
                f"Error: The input format is incorrect. Please provide in 'Month day' format.: {str(e)}"
            )
            return None


# This class is always availabe, even if the ASR is disabled
class DummySpeechToTextAPIView(LoginRequiredMixin, APIView):
    parser_classes = (MultiPartParser,)

    def get(self, request, format=None):
        res = {
            "category": {
                "inferred_from_first_word": "Food",
                "inferred_from_existing_categoies": {
                    "id": 7,
                    "text": "Food",
                    "is_archived": False,
                },
                # "inferred_from_existing_categoies": None,
            },
            "date": "2021-12-12",
            "note": "Food three hundred and seventy eight december twelve this is a note and i know then it's very long",
            "amount": 378,
            "text": "Food three hundred and seventy eight december twelve this is a note and i know then it's very long",
        }
        return Response(res)


class ExchangeRateAPIView(LoginRequiredMixin, APIView):
    def get(self, request, format=None):
        return Response(
            {
                "success": True,
                "data": {
                    "exchange_rate": settings.EXCHANGE_RATE,
                    "last_updated": datetime.datetime.now().isoformat(),
                },
            }
        )


class ThemeAPIView(LoginRequiredMixin, APIView):
    def get(self, request, format=None):
        return Response(
            {
                "success": True,
                "data": {
                    "dark_theme": settings.DARK_THEME,
                    "last_updated": datetime.datetime.now().isoformat(),
                },
            }
        )
