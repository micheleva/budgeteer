# Copyright: (c) 2024, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
# pylint: disable=E1101

from budgets.tests.base import BaseTest
import os
from unittest import mock
from api.alpha.views import get_asr_enabled
from django.test import RequestFactory
from django.contrib.auth.models import User
from rest_framework.test import APIClient
from rest_framework import status
from unittest.mock import patch
from api.alpha.views import SpeechToTextEnabledAPIView
from django.conf import settings


class ASRTestEnabled(BaseTest):
    """
    Unit tests related to get_asr_enabled function
    """

    class ASRTestEnabled(BaseTest):
        """
        Unit tests related to get_asr_enabled function
        """

        @mock.patch.dict(os.environ, {"ASR_ENABLED": "True"})
        def test_asr_enabled_true(self):
            self.assertTrue(get_asr_enabled())

        @mock.patch.dict(os.environ, {"ASR_ENABLED": ""})
        def test_asr_enabled_true_when_empty(self):
            self.assertTrue(get_asr_enabled())

        @mock.patch.dict(os.environ, {}, clear=True)
        def test_asr_enabled_default_with_no_env(self):
            self.assertTrue(get_asr_enabled())

        @mock.patch.dict(os.environ, {"ASR_ENABLED": "False"})
        def test_asr_enabled_false(self):
            self.assertFalse(get_asr_enabled())

        @mock.patch.dict(os.environ, {"ASR_ENABLED": "invalid_value"})
        def test_asr_enabled_invalid_value(self):
            self.assertFalse(get_asr_enabled())

        @mock.patch.dict(os.environ, {"ASR_ENABLED": "True"})
        def test_django_settings_asr_enabled_true(self):
            with self.settings(ASR_ENABLED=os.getenv("ASR_ENABLED", True)):
                self.assertTrue(settings.ASR_ENABLED)

        @mock.patch.dict(os.environ, {"ASR_ENABLED": "False"})
        def test_django_settings_asr_enabled_false(self):
            with self.settings(ASR_ENABLED=os.getenv("ASR_ENABLED", True)):
                self.assertFalse(settings.ASR_ENABLED)

        @mock.patch.dict(os.environ, {}, clear=True)
        def test_django_settings_asr_enabled_default(self):
            with self.settings(ASR_ENABLED=os.getenv("ASR_ENABLED", True)):
                self.assertTrue(settings.ASR_ENABLED)

        @mock.patch.dict(os.environ, {"ASR_ENABLED": "invalid_value"})
        def test_django_settings_asr_enabled_invalid_value(self):
            with self.settings(ASR_ENABLED=os.getenv("ASR_ENABLED", True)):
                self.assertTrue(settings.ASR_ENABLED)


class SpeechToTextEnabledAPIViewTest(BaseTest):
    """
    Unit tests related to SpeechToTextEnabledAPIView class
    """

    def setUp(self):
        self.factory = RequestFactory()
        self.client = APIClient()
        self.user = User.objects.create_user(
            username="testuser", password="testpassword"
        )
        self.client.login(username="testuser", password="testpassword")

    @patch("api.alpha.views.get_asr_enabled")
    def test_get_asr_enabled_true(self, mock_get_asr_enabled):
        mock_get_asr_enabled.return_value = True
        request = self.factory.get("/api/alpha/speech-to-text-enabled/")
        request.user = self.user

        response = SpeechToTextEnabledAPIView.as_view()(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {"enabled": True})

    @patch("api.alpha.views.get_asr_enabled")
    def test_get_asr_enabled_false(self, mock_get_asr_enabled):
        mock_get_asr_enabled.return_value = False
        request = self.factory.get("/api/alpha/speech-to-text-enabled/")
        request.user = self.user

        response = SpeechToTextEnabledAPIView.as_view()(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {"enabled": False})
