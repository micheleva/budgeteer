# Copyright: (c) 2024, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
# pylint: disable=E1101
from django.test import TestCase, RequestFactory
from django.contrib.auth.models import User
from rest_framework.test import APIClient
from rest_framework import status
from api.alpha.views import (
    DummySpeechToTextAPIView,
)


class DummySpeechToTextAPIViewTest(TestCase):
    """
    Unit tests related to DummySpeechToTextAPIView class
    Since we're returning an hardcoded response, we're not testing the logic:
    we just test if the class is still there and working
    """

    def setUp(self):
        self.factory = RequestFactory()
        self.client = APIClient()
        self.user = User.objects.create_user(
            username="testuser", password="testpassword"
        )
        self.client.login(username="testuser", password="testpassword")

    def test_get_dummy_speech_to_text(self):
        request = self.factory.get("/api/alpha/dummy-speech-to-text/")
        request.user = self.user

        response = DummySpeechToTextAPIView.as_view()(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(
            response.data,
            {
                "category": {
                    "inferred_from_first_word": "Food",
                    "inferred_from_existing_categoies": {
                        "id": 7,
                        "text": "Food",
                        "is_archived": False,
                    },
                },
                "date": "2021-12-12",
                "note": "Food three hundred and seventy eight december twelve this is a note and i know then it's very long",
                "amount": 378,
                "text": "Food three hundred and seventy eight december twelve this is a note and i know then it's very long",
            },
        )
