from django.test import TestCase, RequestFactory
from django.contrib.auth.models import User
from rest_framework.test import APIClient
from rest_framework import status
from django.test.utils import override_settings
from api.alpha.views import ExchangeRateAPIView


class ExchangeRateAPIViewTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.client = APIClient()
        self.user = User.objects.create_user(
            username="testuser", password="testpassword"
        )
        self.client.login(username="testuser", password="testpassword")

    @override_settings(EXCHANGE_RATE=1)
    def test_get_exchange_rate(self):
        request = self.factory.get("/api/alpha/exchange-rate")
        request.user = self.user

        response = ExchangeRateAPIView.as_view()(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["success"], True)
        self.assertEqual(response.data["data"]["exchange_rate"], 1)
        self.assertIn("last_updated", response.data["data"])

    @override_settings(EXCHANGE_RATE=200)
    def test_get_exchange_rate_different_value(self):
        request = self.factory.get("/api/alpha/exchange-rate")
        request.user = self.user

        response = ExchangeRateAPIView.as_view()(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["data"]["exchange_rate"], 200)
