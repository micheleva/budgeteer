# Copyright: (c) 2024, Michele Valsecchi <https://gitlab.com/micheleva>
# GNU General Public License v3.0+
# (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)
# pylint: disable=E1101
from django.test import TestCase, RequestFactory
from django.contrib.auth.models import User
from rest_framework.test import APIClient
from rest_framework import status
from unittest.mock import patch
from api.alpha.views import SpeechToTextEnabledAPIView
from api.alpha.views import (
    SpeechToTextEnabledAPIView,
)


class SpeechToTextEnabledAPIViewTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.client = APIClient()
        self.user = User.objects.create_user(
            username="testuser", password="testpassword"
        )
        self.client.login(username="testuser", password="testpassword")

    @patch("api.alpha.views.get_asr_enabled")
    def test_get_asr_enabled_true(self, mock_get_asr_enabled):
        mock_get_asr_enabled.return_value = True
        request = self.factory.get("/api/alpha/speech-to-text-enabled/")
        request.user = self.user

        response = SpeechToTextEnabledAPIView.as_view()(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {"enabled": True})

    @patch("api.alpha.views.get_asr_enabled")
    def test_get_asr_enabled_false(self, mock_get_asr_enabled):
        mock_get_asr_enabled.return_value = False
        request = self.factory.get("/api/alpha/speech-to-text-enabled/")
        request.user = self.user

        response = SpeechToTextEnabledAPIView.as_view()(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data, {"enabled": False})
