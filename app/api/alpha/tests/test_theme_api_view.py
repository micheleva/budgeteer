from django.test import TestCase, RequestFactory
from django.contrib.auth.models import User
from rest_framework.test import APIClient
from rest_framework import status
from django.test.utils import override_settings
from api.alpha.views import ThemeAPIView


class ThemeAPIViewTest(TestCase):
    def setUp(self):
        self.factory = RequestFactory()
        self.client = APIClient()
        self.user = User.objects.create_user(
            username="testuser", password="testpassword"
        )
        self.client.login(username="testuser", password="testpassword")

    @override_settings(DARK_THEME=True)
    def test_get_theme_dark(self):
        request = self.factory.get("/api/alpha/theme")
        request.user = self.user

        response = ThemeAPIView.as_view()(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["success"], True)
        self.assertEqual(response.data["data"]["dark_theme"], True)
        self.assertIn("last_updated", response.data["data"])

    @override_settings(DARK_THEME=False)
    def test_get_theme_light(self):
        request = self.factory.get("/api/alpha/theme")
        request.user = self.user

        response = ThemeAPIView.as_view()(request)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(response.data["data"]["dark_theme"], False)
