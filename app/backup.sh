#!/bin/sh
PGPASSWORD=$POSTGRES_PASSWORD pg_dump -h db -d budgeteer_db -U $POSTGRES_USER -t 'budgets*' -t 'auth_user' --data-only  > data_only.sql