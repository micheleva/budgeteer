import os
import re
from io import StringIO

from django.core.management import call_command
from django.core.management.base import CommandError
from django.test import TestCase
from django.urls import reverse


class SeedDBCommandTestCase(TestCase):
    def test_seed_db_command(self):
        out = StringIO()
        return_code = call_command('seed-db', '--yes-pollute-the-db-with-demo-data-i-ve-checked-the-env-and-its-not-pointing-to-prod-db', stdout=out)
        output = out.getvalue()
        self.assertIn('Successfully seeded the database!!!', output)
        self.assertEqual(return_code, 0)
        self.assertIn('Username: ', output)
        self.assertIn('Password: ', output)
        self.assertIn('User id: ', output)

    def test_seed_db_command_fails_without_needed_args(self):
        out = StringIO()

        with self.assertRaises(CommandError) as context:
            return_code = call_command('seed-db', stdout=out)

        expected_error = 'the following arguments are required: --yes-pollute-the-db-with-demo-data-i-ve-checked-the-env-and-its-not-pointing-to-prod-db'
        output = error_message = str(context.exception)
        self.assertIn(expected_error, output)

    def test_seed_db_command_can_login_with_created_user(self):
        out = StringIO()

        return_code = call_command('seed-db', '--yes-pollute-the-db-with-demo-data-i-ve-checked-the-env-and-its-not-pointing-to-prod-db', stdout=out)
        output = out.getvalue()

        username_pattern = r"Username: (\w+)"
        password_pattern = r"Password: (\w+)"
        user_id_pattern = r"User id: (\d+)"
        env_variables_pattern = r"SPECIAL_GOALS=(.*?)\nSPENDING_LIMITS=(.*?)\n"

        username_match = re.search(username_pattern, output)
        password_match = re.search(password_pattern, output)
        user_id_match = re.search(user_id_pattern, output)
        env_variables_match = re.search(env_variables_pattern, output)

        if username_match:
            username = username_match.group(1)
        else:
            username = None

        if password_match:
            password = password_match.group(1)
        else:
            password = None

        if user_id_match:
            user_id = user_id_match.group(1)
        else:
            user_id = None

        if env_variables_match:
            special_goals_env = env_variables_match.group(1)
            spending_limits_env = env_variables_match.group(2)
        else:
            special_goals_env = None
            spending_limits_env = None

        self.assertIsNotNone(username)
        self.assertIsNotNone(password)
        self.assertIsNotNone(user_id)
        self.assertIsNotNone(special_goals_env)
        self.assertIsNotNone(spending_limits_env)

        # Confirm the user exists
        logged_in = self.client.login(username=username, password=password)
        self.assertTrue(logged_in)

        had_special_goals_env = False
        # Store the original value, if there was any
        if os.getenv("SPECIAL_GOALS"):
            original_special_goals_env = os.getenv("SPECIAL_GOALS")
            had_special_goals_env = True

        # Inject current user data in the SPECIAL_GOALS env
        os.environ["SPECIAL_GOALS"] = special_goals_env

        # Retrive this user's special goals from the api
        special_goals_url = reverse('api:v1:special_goals')
        special_goals = self.client.get(special_goals_url)
        json_res = special_goals.data

        limits = special_goals_env.split(";")
        expected_goal_count = len([limit for limit in limits if limit.startswith(f"{user_id},")])
        expected_goals = [limit for limit in limits if limit.startswith(f"{user_id},")]
        self.assertEqual(json_res['count'], expected_goal_count)

        # Confirm the SPECIAL_GOALS data matches what we get back from the API
        for expected_goal in expected_goals:
            fields = expected_goal.split(",")
            limit_id, goal_id, date, second_date, initial_amount, second_amount, extra_amount, day_of_the_month = fields

            goal_match = None
            for goal in special_goals.data['results']:
                if (
                    goal['goal_id'] == int(goal_id)
                    and goal['date'] == date
                    and goal['second_date'] == second_date
                    and goal['initial_amount'] == int(initial_amount)
                    and goal['second_amount'] == int(second_amount)
                    and goal['extra_amount'] == int(extra_amount)
                    and goal['day_of_the_month'] == int(day_of_the_month)
                ):
                    goal_match = goal
                    break

            # Assert that a matching goal was found in the response data i.e. all goals in the env do appear in the API response
            self.assertIsNotNone(goal_match)

        had_spending_env = False
        # Store the original value, if there was any
        if os.getenv("SPENDING_LIMITS"):
            original_spending_env = os.getenv("SPENDING_LIMITS")
            had_spending_env = True

        # Inject current user data in the SPECIAL_GOALS env
        os.environ["SPENDING_LIMITS"] = spending_limits_env

        # Retrive this user's spending limits from the api
        spending_limits_url = reverse('api:v1:spending_limits')
        special_goals = self.client.get(spending_limits_url)
        json_res = special_goals.data

        spending_limits = spending_limits_env.split(";")
        expected_limits_count = len(spending_limits)
        expected_limits = [limit for limit in spending_limits]
        self.assertEqual(json_res['count'], expected_limits_count)

        for expected_limit in expected_limits:
            fields = expected_limit.split(",")
            amount, currency, days, category_text, item_text = fields

            # Find the corresponding limit in the response data
            limit_match = None
            for limit in json_res['results']:
                if (
                    limit['amount'] == float(amount)
                    and limit['currency'] == currency
                    and limit['days'] == int(days)
                    and limit['category_text'] == category_text
                    and limit['item_text'] == item_text
                ):
                    limit_match = limit
                    break

        # Reset the environment variable after the test if there was any
        if had_special_goals_env:
            os.environ["SPECIAL_GOALS"] = original_special_goals_env
        else:
            # otherwise just clean it up
            os.environ.pop("SPECIAL_GOALS")

        # Reset the environment variable after the test if there was any
        if had_spending_env:
            os.environ["SPENDING_LIMITS"] = original_spending_env
        else:
            # otherwise just clean it up
            os.environ.pop("SPENDING_LIMITS")


# WRITE ME
class WaitForDBCommandTestCase(TestCase):
    pass

# WRITE ME
class WipeIDCommandTestCase(TestCase):
    pass
