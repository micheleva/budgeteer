#!/bin/bash
set -x

MODEL_NAME="vosk-model-small-en-us-0.15"
MODEL_URL="https://alphacephei.com/vosk/models/${MODEL_NAME}.zip"
MODEL_PATH="/vosk-model/${MODEL_NAME}"

echo "Checking if model exists at ${MODEL_PATH}..."
if [ ! -d "$MODEL_PATH" ]; then
    echo "Downloading ${MODEL_NAME}..."
    wget -O model.zip "$MODEL_URL"
    
    echo "Extracting model..."
    unzip -q model.zip
    rm model.zip
    # Need to adjust permissions for the budgeteer user (uid 101, gid 101), in web container, to access the model
    chown 101:101 -R ${MODEL_PATH}
    
    echo "Model downloaded and extracted to ${MODEL_PATH}"
else
    echo "Model already exists at ${MODEL_PATH}"
fi

echo "Download complete. Exiting."