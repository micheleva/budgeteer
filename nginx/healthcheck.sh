#!/bin/sh

if [ "$NGINX_CONF_FILE" == "nginx_https.conf" ]; then
  URL="https://localhost/"
else
  URL="http://localhost/"
fi

wget -q --spider --no-check-certificate --user-agent="health-checker" --header="Host: $SITENAME" "$URL"