#!/bin/sh

# Color error messages
RED='\033[0;31m'
NC='\033[0m' # No Color

# Copy the main Nginx configuration file from the template
mkdir -p  /etc/nginx/conf.d/
cp /host_conf.d/nginx.conf /etc/nginx/nginx.conf
cp /host_conf.d/mime.types /etc/nginx/mime.types
# Cleans up the volume from previous run with different paramters
rm /etc/nginx/conf.d/*conf
cp /host_conf.d/${NGINX_CONF_FILE} /etc/nginx/conf.d/${NGINX_CONF_FILE}

# Sanity check for ssl keys and certs
if [ "$NGINX_CONF_FILE" = "nginx_https.conf" ] && [ ! -f "/host_ssl/fullchain.pem" ] && [ ! -f "/host_ssl/privkey.pem" ]; then
  echo "${RED}ERROR: Required files 'fullchain.pem' and 'privkey.pem' are not in the nginx/keys folder: put them there and re-trigger the build.${NC}"
  exit 1
fi

# Copy SSL files from ssl_volume, so that nginx container can see them
cp /host_ssl/fullchain.pem /etc/nginx/ssl/fullchain.pem
cp /host_ssl/privkey.pem /etc/nginx/ssl/privkey.pem

# Copy useragent whitelist
mkdir -p /etc/nginx/rules
cp /useragents/useragent.rules /etc/nginx/rules/useragent.rules

# Sanity check for User Agent block feature
if [ -z "$UA_BLOCK_FEATURE" ]; then
  echo -e "${RED}ERROR: Required argument 'UA_BLOCK_FEATURE' not provided. Make sure to have an .env file in the same directory as docker-compose.yaml with UA_BLOCK_FEATURE=true or UA_BLOCK_FEATURE=false${NC}"
  exit 1
fi

# Adjust useragent default behaviour based on UA_BLOCK_FEATURE value
# i.e. 1 block, 0 allow
if [ "$UA_BLOCK_FEATURE" = "true" ]; then
  sed -i 's/^[[:space:]]*default[[:space:]]*[^;]*;/    default 1;/' /etc/nginx/rules/useragent.rules
else
  sed -i 's/^[[:space:]]*default[[:space:]]*[^;]*;/    default 0;/' /etc/nginx/rules/useragent.rules
fi

# Allow the nginx container to read the folder contents
chown -R 101:101 /etc/nginx
